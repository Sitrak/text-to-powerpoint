
delete from song_book where _id = '#1' /end

insert into song_book values (
'#1',
'Fihirana FFPM'
) /end

delete from song_chapter where chapter_book = '#1' /end

insert into song_chapter values ('#1-I', 'I. ANDRIAMANITRA RAY', NULL, '#1'),
('#1-I-1', '1. NY FIDERANA SY FISAORANA AZY', '#1-I', '#1'),
('#1-I-2', '2. NY ASAN''NY FAMORONANA', '#1-I', '#1'),
('#1-I-3', '3. NY FAHATSARANY AMIN''NY FITONDRANA', '#1-I', '#1'),
('#1-I-4', '4. NY FAHASOAVANY AMIN''NY FANAVOTANA', '#1-I', '#1'),
('#1-II', 'II. JESOSY KRISTY TOMPO', NULL, '#1'),
('#1-II-1', '1. NY FIHAVIANY SY NY FIVERENANY', '#1-II', '#1'),
('#1-II-2', '2. NY FAHATERAHANY', '#1-II', '#1'),
('#1-II-3', '3. NY FIAINANY', '#1-II', '#1'),
('#1-II-4', '4. NY FIJALIANY - NY FAHAFATESANA', '#1-II', '#1'),
('#1-II-5', '5. NY FITSANGANANY', '#1-II', '#1'),
('#1-II-6', '6. NY FIAKARANY', '#1-II', '#1'),
('#1-II-7', '7. NY FIFONANY HO ANTSIKA', '#1-II', '#1'),
('#1-II-8', '8. NY FANATREHANY', '#1-II', '#1'),
('#1-II-9', '9. NY FIDERANA SY FISAORANA AZY', '#1-II', '#1'),
('#1-III', 'III. NY FANAHY MASINA', NULL, '#1'),
('#1-IV', 'IV. NY TELO IZAY IRAY', NULL, '#1'),
('#1-V', 'V. NY SORATRA MASINA TENIN''ANDRIAMANITRA', NULL, '#1'),
('#1-VI', 'VI. NY FIANGONANA', NULL, '#1'),
('#1-VI-1', '1. FANOMBOHANA SY FIANTSOANA', '#1-VI', '#1'),
('#1-VI-3', '3. NY SAKRAMENTAN''NY FANASAN''NY TOMPO', '#1-VI', '#1'),
('#1-VI-2', '2. NY SAKRAMENTAN''NY BATISA', '#1-VI', '#1'),
('#1-VI-4', '4. NY FIRAISAN''NY MINO SY NY FIARAHANY MIVAVAKA', '#1-VI', '#1'),
('#1-VI-6', '6. FANOKANANA MPITANDRINA', '#1-VI', '#1'),
('#1-VI-5', '5. FANATERAM-BOKATRA', '#1-VI', '#1'),
('#1-VI-7', '7. FANDRAISANA HO MPANDRAY', '#1-VI', '#1'),
('#1-VI-9', '9. FITOKANANA TRANO FIANGONANA', '#1-VI', '#1'),
('#1-VI-8', '8. FAMPIELEZANA NY FILAZANTSARA', '#1-VI', '#1'),
('#1-VI-10', '10. FISAORANA AN''ANDRIAMANITRA NOHO NY MPIASANY', '#1-VI', '#1'),
('#1-VII', 'VII. NY FIAINANA KRISTIANA', NULL, '#1'),
('#1-VI-11', '11. FIRAVANA', '#1-VI', '#1'),
('#1-VII-1', '1. NY ANTSON''NY FILAZANTSARA', '#1-VII', '#1'),
('#1-VII-3', '3. NY FAMELAN-KELOKA - NY FAMONJENA', '#1-VII', '#1'),
('#1-VII-2', '2. NY FIBEBAHANA', '#1-VII', '#1'),
('#1-VII-4', '4. NY FANOLORAN-TENA - NY FANKATOAVANA - NY FANAMASINANA', '#1-VII', '#1'),
('#1-VII-6', '6. NY FIAINANA AO AMIN''I KRISTY', '#1-VII', '#1'),
('#1-VII-5', '5. NY FIOVAM-PO - NY FIAINAM-BAOVAO', '#1-VII', '#1'),
('#1-VII-7', '7. NY FINOANA', '#1-VII', '#1'),
('#1-VII-9', '9. NY FITIAVANA', '#1-VII', '#1'),
('#1-VII-8', '8. NY FANANTENANA', '#1-VII', '#1'),
('#1-VII-10', '10. FIADANANA SY FIFALIANA', '#1-VII', '#1'),
('#1-VII-12', '12. FITARAINANA AMIN''NY MPAMONJY', '#1-VII', '#1'),
('#1-VII-11', '11. ZAVA-TSAROTRA MAHAZO NY KRISTIANA SY FAMPAHEREZANA', '#1-VII', '#1'),
('#1-VII-13', '13. FANGATAHAM-PITAHIANA SY FIAROVANA', '#1-VII', '#1'),
('#1-VII-15', '15. ADY MASINA SY FANDRESENA', '#1-VII', '#1'),
('#1-VII-14', '14. FANOMANANA SY FAMPIERITRERETANA', '#1-VII', '#1'),
('#1-VII-16', '16. FAHAFATESANA - FITSANGANANA AMIN''NY MATY', '#1-VII', '#1'),
('#1-VIII', 'VIII. FOTOANA SAMIHAFA', NULL, '#1'),
('#1-VII-17', '17. NY LANITRA SY NY FIAINANA MANDRAKIZAY', '#1-VII', '#1'),
('#1-VIII-1', '1. FAMPAKARAM-BADY', '#1-VIII', '#1'),
('#1-VIII-3', '3. AMIN''NY MARAINA', '#1-VIII', '#1'),
('#1-VIII-2', '2. FANOKANANA TRANO', '#1-VIII', '#1'),
('#1-VIII-4', '4. AMIN''NY HARIVA', '#1-VIII', '#1'),
('#1-VIII-6', '6. NY HANDEHA LAVITRA', '#1-VIII', '#1'),
('#1-VIII-5', '5. AMIN''NY SAKAFO', '#1-VIII', '#1'),
('#1-VIII-7', '7. NY FIVAVAHANA HO AN''NY TANINDRAZANA', '#1-VIII', '#1'),
('#1-VIII-9', '9. AMIN''NY TAOM-BAOVAO', '#1-VIII', '#1'),
('#1-VIII-8', '8. AMIN''NY FARAN''NY TAONA', '#1-VIII', '#1'),
('#1-IX', 'IX. NY FIVORIAN''NY TANORA', NULL, '#1'),
('#1-XI', 'XI. LITANIA', NULL, '#1'),
('#1-X', 'X. NY HIRAN''NY ANKIZY', NULL, '#1'),
('#1-XII', 'XII. TSANTA', NULL, '#1'),
('#1-XIII', 'XIII. ANTEMA', NULL, '#1') /end

delete from song where song_book = '#1' /end

insert into song values (10011, '1. Andriananahary masina indrindra!', NULL, NULL, '
{1.
Andriananahary masina indrindra!
Ny anjelinao izay mitoetra Aminao.
Mifamaly hoe: Masina indrindra,
Andriananahary, Telo ''zay Iray.}

{2.
Andriananahary masina Indrindra!
Na tsy hita aza izao ny voninahitrao!
Masina indrindra Ianao irery.
Andriananahary, Telo ''zay Iray.}

{3.
Zava-manana aina samy mankalaza
Sady manambara Anao ''zay Tompony izao;
Ianao irery no mitahy azy,
Andriananahary, Telo ''zay Iray.}

{4.
Andriananahary feno hatsarana!
He, ny fitahianao izay Mpanomponao;
Tsara dia tsara ny omenao azy,
Andriananahary, Telo ''zay Iray.}

{5.
Andriananahary masina indrindra!
Izahay mpanomponao ta-hankalaza Anao;
Feno fahendrena, feno fiantrana,
Andriananahary, Telo ''zay Iray.}
', '#1-I-1', '#1') /end

insert into song values (10021, '2. Misaora an''i Zanahary', NULL, NULL, '
{1.
Misaora an''i Zanahary,
''Zao olona tontolo izao;
Dia mankalazà Azy,
Fa anton''izao rehetra izao.}

{2.
Izay zavatra nomeny
Mahafaly, mahasoa;
Toy ny andro fararano
Toy ny andro mipoaka.}

{3.
Dia ampitomboy ny sainay
Hahalala ny teninao,
hahafantatra marina
Izay sitraky ny fonao.}
', '#1-I-1', '#1') /end

insert into song values (10031, '3. Isaorana anie Andriamanintsika', NULL, NULL, '
{1.
Isaorana anie
Andriamanintsika
Fa fitahiam-be
Omeny ho antsika;
Hatreo ambohoka
Ka mandraka antitra,
Isika henika
Ny soan''ny lanitra.}

{2.
Tsy mety tapitra
Na lany ny hareny,
Tsy hita faritra
Ny sisa izay homeny;
Fanahy sambatra
Sy fo miadana
No efa santatra,
Izay ananana.}

{3.
Ny Ray sy Zanaka
Sy ny Fanahy koa,
Derain''ny masina
Ho Tompo iray tokoa;
Andriamanitray
Be fahasoavana
No anateranay
Izay fisaorana.}
', '#1-I-1', '#1') /end

insert into song values (10041, '4. Mamy ny fitianao', NULL, NULL, '
{1.
Mamy ny fitianao,
Ry Jehovah Tomponay!
Foinao ny Zanakao
Ho Mpanavotra anay.
Tsy mba misy tokinay
Afatsy ny Zanakao.
Ka misaotra Anao izahay
Noho ny fitianao .}

{2.
Mamy ny fitianao,
Mahafaly fo tokoa;
Izahay avelanao
Mba hibebaka avokoa.
Lasa lavitra izahay,
Nefa notadiavinao;
Efa ratsy izahay,
Nefa tsy nafoinao.}

{3.
Mamy ny fitianao,
''zay andraisanao anay
Honina ao an-dapanao
Raha ho faty izahay
Noho ny ratsy izay natao,
Loza no anjaranay,
Nefa namboaranao
Paradisa izahay.}

{4.
Mamy re, ry Tomponay!
''zao fitianao izao,
Ka indreto izahay
Avy mba hisaotra Anao.
Raiso re izao saotra izao,
Fa fanati-tsitrapo;
Tsy mba mendrika ho Anao,
Nefa vokatry ny fo.}
', '#1-I-1', '#1') /end

insert into song values (10051, '5. Jehovah, Ray an-danitra', NULL, NULL, '
{1.
Jehovah, Ray an-danitra,
Mpamelona anay
Tsinjovy ny mpanomponao,
Tsarovy izahay.}

{2.
Jehovah, Ray mamindra fo !
Henoy ny hatakay:
Ekeo sy raiso ho Anao
Ny dera entinay.}

{3.
Jehovah, Ray mahari-po !
Vonoy ny helokay ;
Diovy ny fanahinay,
Velomy izahay.}

{4.
Jehovah Ray ! Ny antranao
No itokianay ;
Ambara-pahafatinay
Hisaoranay Ianao.}
', '#1-I-1', '#1') /end

insert into song values (10061, '6. Avia, miondreha', NULL, NULL, '
{1.
Avia, miondreha,
Ry vazantany o!
Fa lehibe Jehovah,
Sy masina tokoa;
Miankohofa tsara,
Manatona Azy ''zao,
Haneho fanajana
Ilay Mpanao anao.}

{2.
Andriananahary,
Manaiky izahay
Fa aminao ny hery
Tsy hay toherina.
Tsy misy maharara
Ny tianao hatao,
Fa Ianao Mpitondra
Izao rehetra izao.}

{3.
Re any lavitra any
Izao ny herinao,
Ka isam-bazan-tany
Hiondrika Aminao;
Mpanjaka sy Mpitsara
Mahery Ianao;
Miadana ny tany
Miankina aminao.}
', '#1-I-1', '#1') /end

insert into song values (10071, '7. Misaora an''Andriamanitra', NULL, NULL, '
{1.
Misaora an''Andriamanitra
Izay nanao ny lanitra
Namorona ny tany koa
Sy ''zay rehetra mahasoa.}

{2.
Derao ny voninahiny,
Ankalazao ny heriny,
Jereo ny fitiavany
Tsy tambonao tononina.}

{3.
Dia avy izahay izao,
Ry Tompo be fitia o!
Miondrika eo imasonao
Midera ny Anaranao .}

{4.
Isaorana mandrakizay
Ianao, Andriamanitray !
Ny haja be indrindra izao
Atolotray mpanomponao.}
', '#1-I-1', '#1') /end

insert into song values (10081, '8. Dera, laza, hery, haja', NULL, NULL, '
{1.
Dera, laza, hery, haja
Ho an''Andriamanitra
Tompon''aina, Zanahary,
Rain''ny voninahitra,
Izy no mpanao antsika
Sady ivelomana;
Fototr''aina, foto-tsoa,
Izy ivavahana.}

{FIV.
Hihira isika hoe: Haleloia!
Dera ho an''Andriamanitra
:,:Dia Haleloia!
Haleloia:,:}

{2.
Ray tsy toha, tsy miova,
Tia sy mamindra fo,
Mahasoa isan''andro,
Izy no isaorana;
Vatolampy tsy mifindra,
Tokin''olombelona;
Fiarova-mahavonjy,
Izy itokiana :
[FIV.]}
', '#1-I-1', '#1') /end

insert into song values (10091, '9. Ry Tompo Zanaharinay!', NULL, NULL, '
{1.
Ry Tompo Zanaharinay!
Manati-tsaotra izahay,
Fa efa novelominao
Ka tratr''izao maraina izao.}

{2.
Ny aizina efa lasa izao,
Mazava no naposakao;
Levony koa ny otanay,
Ka hazavao ny hevitray.}

{3.
Ny asanay hatao anio
Atrehonao ka mba tahio,
Hatao ho voninahitrao,
Handraisanay ny antranao.}

{4.
Derainay Ianao, ry Ray!
Derainay Jeso Tomponay,
Derainay ny Fanahy koa,
Derainay, fa Mpanisy soa.}
', '#1-I-1', '#1') /end

insert into song values (10101, '10. Ry Tomponay Tsitoha o!', NULL, NULL, '
{1.
Ry Tomponay Tsitoha o!
Mpanao izao tontolo izao,
Ankalazainay Ianao
Fa lehibe ny herinao,}

{2.
Ny hiran''ny anjelinao
Manerana ny lanitra,
Manehoeho fatratra
Ny hobin''ny voaavotra;}

{3.
Fa masina, fa masina,
Fa masina i Jehovah!
Ka heniky ny lazanao
Ny tany sy ny lanitrao.}

{4.
Ireo mpitory irakao
Mihoby eo anilanao,
Nahefa fanompoana,
Mihira ny fisaorana}

{5.
Izao olonao rehetra izao
Dia mino sy manaiky Anao
Ho Ray Tsy miova sady to,
Mahery sy mpamindra fo.}

{6.
Ny Zanakao nirahinao
Sy ny Fanahy Masinao
Isaoranay Anao, ry ray,
Fa foto-pamonjena anay!}

{7.
Ry kristy o, Mpanjakanay
Mitovy hery amin-dRay,
Ny fitiavanao anay
No nisoloanao anay.}

{8.
Ny lozam-pahafatesana
Novanao ho fiainana,
Izany no hiainanay
Na eto na rahatrizay.}

{9.
Ao an-kavanan-dRainao ao
No itoeranao izao,
Fa mbola hidina indray
Hitsena sy handray anay.}

{10.
Ka faly fa naiditra
Ho mponina ao an-danitra
''zahay izay navotanao,
Hiombon-dova aminao.}

{11.
Tariho làlana izahay,
Vimbinonao ny dianay,
Ka isan''andro vaky izao
Hambaranay ny lazanao.}

{12.
Vonjeonao, ry Tompo o!
Vonoy ny ratsy ao am-po,
Ka hasoavy izahay
Ambara-pahafatinay.}

{13.
Fa lehibe ny antranao,
Mahery koa Ianao,
Ka dia matoky izahay
Ry Tompo Zanaharinay.}
', '#1-I-1', '#1') /end

insert into song values (10111, '11. Endrey ny voninahitrao', NULL, NULL, '
{1.
Endrey ny voninahitrao,
Ry Ray Tsitoha o!
Endrey ny fiandriananao,
Hajainay eram-po!}

{2.
Ny herinao, ny andronao,
Dia mandrakizay!
Ny lanitra mitsaoka Anao
Ry Andriamanitray!}

{3.
Ny tavanao mamiratra
Ny fahamasinanao
Sy hery tapitr''ohatra,
Ny fahendrenao soa.}

{4.
Endrey, mahatahotra aho izao
Ry Tompo masina o!
Mangovitra eo imasonao
Sao dia lavinao.}

{5.
Na dia ambony Ianao,
Dia nidina amiko
Nitondra fihavanana,
Ka dia mba tiako.}

{6.
Koa aiza re no misy ray
Mitovy aminao;
Mikarakara tsara anay
Izay natsanganao.}

{7.
O, mahagaga Ianao,
Ry Tompo tia anah,
Ny fonay dia hatoky Anao
Ambara-podinay.}
', '#1-I-1', '#1') /end

insert into song values (10121, '12. Jehovah o, derainay Ianao!', NULL, NULL, '
{1.
Jehovah o, derainay Ianao!
Ankalazainay ny Anaranao!
:,:Izao rehetra izao manaja Anao,
Fa Rainay ho mandrakizay:,:}

{2.
Mpanjakanay sy Tompo masina
Omenay eram-po ny hasina
:,:Izao rehetra izao mihoby Anao
Fa Rainay ho mandrakizay:,:}

{3.
Ry Masina manerinerina,
Jehovah be fitia sy marina!
:,:Izao rehetra izao misaotra Anao
Fa Rainay ho mandrakizay:,:}

{4.
Manerana izao tontolo izao
Ny laza sy ny voninahitrao,
:,:Izao rehetra izao hanoa Anao
Fa Rainay ho mandrakizay:,:}

{5.
Andriananahary masina o
Tanteraka ny fiandriananao
:,:Izao rehetra izao mitsaoka Anao
Fa Rainay ho mandrakizay:,:}
', '#1-I-1', '#1') /end

insert into song values (10131, '13. Misaora ny Mpahary', NULL, NULL, '
{1.
Misaora ny Mpahary,
Tsarovinao ny asany!
Misaora ny Mpahary,
Derao ny soa vitany!
Ny helokao rehetra
Dia voavelany,
Fa tsy mba misy fetra
Ny fitiavany,
Ny Zanany nafoiny
Mba ho Mpamonjinao,
Ka hianao nantsoiny
Handray ny aim-baovao.}

{2.
Ny toky izao nomeny
Sy ny fahasoavana,
Maharitra ireny,
Tsy manam-pahataperana;
Avelany tokoa
Ny hatezerany,
Ka tsy mba hahavoa
Antsika olony,
Fa Izy mahalala
Ny toetsika aty,
Ka tena Ray mandala
Antsika zanany.}

{3.
He, toy ny voninkazo
Ny ain''ny olombelona,
Tsy ela dia malazo
Ka tena mora levona,
Fa Jeso manan-kery
Hamoha fasana,
Tsy hisy izay ho very
Ny olo-masina;
Isika vovotany
No namboariny
Hiombom-panjakana
Sy laza aminy.}

{4.
Misaora ny Mpahary,
Ry tany sy ny lanitra!
Ry zava-boahary,
Derao Andriamanitra
Ry irany mahery
Manao ny sitrany,
Ry zanany izay very,
Kanefa hitany,
Misaora Azy ary,
Fa heniky ny soa,
Misaora ny Mpahary,
O, ry fanahiko!}
', '#1-I-1', '#1') /end

insert into song values (10141, '14. O! ry tany rehetra, avia izao', NULL, NULL, '
{1.
O! ry tany rehetra, avia izao
Hidera ny Tompo Mpanavotra anao
Ny Tompo miantso antsika hankao
Anatrehany mba hohasoaviny ao:}

{FIV.
Dera, laza, anie,
Voninahi-dehibe,
Ho Anao, ry Tomponay Tsitoha o!
Amen.}

{2.
He, Jehovah miantra antsika izao
Ka dia manolotra antsika indray
Ny endriny mbamin''ny aina vaovao;
Eny, Izy manaiky tokoa ho Ray
[FIV.]}

{3.
Asandrato ny feo hihoby hoe:
Isaorana mandrakizay Ianao
Ry Tompo mahery, Izay manome
Ho anay olom-bery ny aina vaovao:
[FIV.]}
', '#1-I-1', '#1') /end

insert into song values (10151, '15. Avia hankalaza', NULL, NULL, '
{1.
Avia hankalaza
Ny Tompo lehibe;
Fa feno hery, haja
Jehovah Masina e
Andeha mitambara,
Ry vazan-tany o!
Miankohofa tsara
Hajana Azy izao!}

{2.
Ry Tomponay Tsitoha,
Indreto Izahay,
Manondrika ny loha,
Midera Anao, ry Ray,
Ny fonay sy ny hery,
Fanahy, saina koa,
Omena Anao irery,
Ka anjakao tokoa.}

{3.
Mpanjaka tena hendry
Ianao, ry Tomponay
KA tena mahakendry
''zay hahasoa anay;
Na misy toa mangidy
Ny fitondranao soa,
Ny saina sy safidy
Manaiky anao tokoa.}

{4.
Manerana ny tany
Izao ny lazanao,
Ka be tsy hita lany
Ny voninahitrao;
Koa hira fiderana
Ventesinay izao:
Haleloia, Hosana!
Mba raiso ho Anao.}
', '#1-I-1', '#1') /end

insert into song values (10161, '16. Avia, ry vazantany o!', NULL, NULL, '
{1.
Avia, ry vazantany o!
Sy mponina ao an-danitra
Miraisa feo ka mihirà
Ny fanavontan-dehibe
:,:Zay amonjena marobe
Dia vita re ka miderà:,:}

{2.
Avia, ry fiangonana o!
Mba ansandrato re ny feo
Hidera ny Mpanavotra;
Na aty an''efitra aza izao,
:,: Hirao am-pandehanana
Ny hira soan''i Ziona :,:}

{3.
Avia, ry voavonjy o!
Ka mifalia avokoa,
Fa afa-doza hianareo;
Miraisa feo ka mihirà
:,: Derao, derao fa mendrika
ilay nanavotra anareo :,:}

{4.
Avia, ry olom-bery o!
Izay mandre ny teny soa
Hoe : " Manatona Ahy ''zao " ;
Anio ny Tompo mba fidio
:,: Ny fitiavany valio;
Ny fiderany koa hirao :,:}
', '#1-I-1', '#1') /end

insert into song values (10171, '17. Haja sy voninahitra', NULL, NULL, '
{1.
Haja sy voninahitra
Omena an''Andriamanitra,
Fa Izy ihany no mahefa
:,: Izay sitraky ny fony :,:}

{2.
Masina, tsy azo haratsiana;
Mahery, tsy azo resena;
Marina, tsy azo laingaina;
:,: Hendry tsy azo fitahina :,:}

{3.
Ny am-po toy ny am-bava;
Ny maizina toy ny mazava;
Ny takona toy ny miseho
:,: Tsy misy àry tsy hitany :,:}

{4.
Tsy miova ny fombany;
Lehibe ka tsy oharina;
Tia, tsy azo arovana;
:,: Miantra tsy azo ferana :,:}

{5.
Saotra amam-bavaka atao
Dera sy laza ho azy;
Izy no ivavahana
:,: Fa anton''izao tontolo izao :,:}
', '#1-I-1', '#1') /end

insert into song values (10181, '18. Ny feonay rehetra izao', NULL, NULL, '
{1.
Ny feonay rehetra izao
Dia entinay midera Anao
Ry Andriananaharinay
Na ambany aza izahay
:,: Dia tianao :,:
Ry Andriananaharinay!}

{2.
Ny andronay rehetra izao
Dia entinay manoa Anao,
Ry Tompo sy Mpanjakanay!
Satria izao dia fantatray
:,: Fa tianao :,:
Ry Tompo sy Mpanjakanay!}

{3.
Ny hevitray rehetra izao
Dia enti-mankalaza Anao!
Ry Tompo Mpanasoa anay!
Mpitahy ny fanahinay,
:,: Fa tianao :,:
Ry Tompo Mpanasoa anay!}

{4.
Ny herinay rehetra izao
No iasanay ho Anao
Ry Aronay sy Tanjakay
Miezaka tokoa ''zahay
:,: Fa tianao :,:
Ry Aronay sy Tanjakay!}

{5.
Rehefa alainao izahay
Ka tapitra ny androanay
Hamantana any aminao
Zahay ary an-danitrao,
:,: Fa tianao :,:
Hamantana any aminao.}
', '#1-I-1', '#1') /end

insert into song values (10191, '19. Misaotra Anao ''zahay izao', NULL, NULL, '
{1.
Misaotra Anao ''zahay izao
Jehovah Ray!
Fa efa notohizanao
Ny andronay,
Ka tonga aty an-tranonao,
Indray ''zahay.}

{2.
Misaotra Anao ''zahay izao,
Ry Mpanome!
Ny soa avy aminao
Tsy lany re,
Fa isan''andro misy vao,
Ka maro be.}

{3.
Misaotra Anao ''zahay izao,
Ry Rainay soa!
Ny helokay navelanao,
Nefa izao moa
No mahalaza izay natao?
Fa be tokoa.}

{4.
Misaotra Anao ''zahay izao
Ry mora fo!
Ny mafy tsy sakananao
Tsy mba hanjo,
Fa be ny fanirianao
Anay hadio.}

{5.
Misaotra Anao ''zahay izao,
Ry Tomponay!
Ao aminao an-danitra ao
Honenanay,
Fa misy fitsaharana ao
Mandrakizay.}
', '#1-I-1', '#1') /end

insert into song values (10201, '20. Tsaroanay tokoa izao', NULL, NULL, '
{1.
Tsaroanay tokoa izao
Ny soa be noraisinay,
Tsy tapaka avy taminao
Ry Andriananaharinay.}

{2.
Ny lasa itodihana
Dia mampiseho avokoa
Ny tanam-pitiavana
Miahy sady manasoa.}

{3.
He! Koa ''ty androanay ity,
Izay diavinay izao,
Dia voahodidina ery
Ny fitahiana aminao.}

{4.
Dia tokinay tokoa izay
Fa mbola ho arovanao
Anio, ampitso, rahatrizay,
Izay miankina Aminao.}

{5.
Ka dia mba velomy re
Aty anatinay ny fo
Hahay misaotra Anao anie,
Ry Rainay be famindram-po!}
', '#1-I-1', '#1') /end

insert into song values (10211, '21. Derao Andriamanitra', NULL, NULL, '
{1.
Derao Andriamanitra
RY vazantany efatra,
Fa Izy no mamindra fo
Ka mety manome ny zo.
Naniraka ny Zanany,
Hanavotra ny olony
Ny sandriny tsy toha re,
Fa manan-kery lehibe.
Malaza ny Anarany,
Fa lehibe ny asany.}

{2.
Ny zavatra rehetra e,
Ny ranomasin-dehibe,
Ireo tendrombohitra,
Ny saha feno ahitra,
Ny hazo sy ny vorona
Ny vato sy ny zavona
Ny voninkazo manitra,
Ny endriky ny lanitra
Dia samy tsy mijanona
Midera ny Mpamorona.}

{3.
Ry olona navotana,
Mba mitsangàna faingana,
Ka miderà ny Tomponao,
Izay nanavotra anao,
Fa he, ny voninahiny
Manerana ny taniny.
Tendreo ny valihanao
Ka miventesa hira vao:
RY voavonjy o, derao
Ny Ray Mpamorona anao!}
', '#1-I-2', '#1') /end

insert into song values (10221, '22. Ry Rainay ''zay namorona izao rehetra izao', NULL, NULL, '
{1.
Ry Rainay ''zay namorona
Izao rehetra izao,
Ny hita sy ny takona
Dia asan-tananao.}

{2.
Ny volana aman-kintana,
Ny biby marobe.
Ny ala maitso mavana,
Hitsaoka Anao anie.}

{3.
Ny tany sy ny lanitra,
Ny ranomasina,
Ireo tendrombohitra
Manolo-kasina.}

{4.
Ny ory sy ny mpanana
Dia fototra iray,
Fa samy asan-tanana
Nataonao Tomponay.}

{5.
Dia saotra no atolotray
Anao, ka raisonao,
Fa fanomezan-ko anay
Ny asan-tananao.}
', '#1-I-2', '#1') /end

insert into song values (10231, '23. Tsy misy zava-mahasoa...', NULL, NULL, '
{1.
Tsy misy zava-mahasoa ,
Andriamanitra o!
''Zay tsy nataonao ho anay
Mba hifalianay.}

{2.
Tsy misy singan''ahitra,
Na ravin-kazo koa
Izay tsy amantaranay
Fa hendry Ianao.}

{3.
Ny kintana mazava ery
Maneho avokoa
Fa sitrakao hanafaka
Ny aizina aminay.}

{4.
Izay rehetra tazanay
Dia voninahitrao;
Izay rehetra takona
Milaza izany koa.}
', '#1-I-2', '#1') /end

insert into song values (10241, '24. Ny lanitra ao ambony ao', NULL, NULL, '
{1.
Ny lanitra ao ambony ao,
:,: Andriamanitra o! :,:
Izay rehetra vitanao
Dia mahagaga anay. (in-3)}

{2.
Ny volana aman-kintana,
:,: Ny masoandro koa :,:
Ny tany aman-danitra.
Mitory avokoa. (in-3)}

{3.
Ny andro aman''alina
:,: Milaza aminay :,:
Fa hendry Andriamanitra,
Jehovah Tomponay.(in-3)}

{4.
Ny tenanay, fanahy koa,
:,: Dia avy taminao :,:
Ka mampiseho tsara koa
Ny voninahitra. (in-3)}

{5.
Izay rehetra vitanao
:,: Aty sy any koa :,:
Miara-mankalaza Anao
Fa Tompony tokoa. (in-3)}
', '#1-I-2', '#1') /end

insert into song values (10251, '25. Andriamanitra hendry', NULL, NULL, '
{1.
Andriamanitra hendry
:,: Loharanon-tsaina :,:
:,: manolo-pahendrena
''Zay manaiky Azy :,:}

{2.
Andriamanitra mahay
:,: Izao rehetra izao :,:
:,: Ka Izy no akana saina
Hahahendry antsika :,:}

{3.
Ary isika mila saina
:,: Hanitsy antsika :,:
:,: Amin''ny fianarana
Hanarahana Azy :,:}

{4.
Izy no ihavian''
:,: Ny soa rehetra :,:
:,: Hiainana mandrakizay
Ka Izy no omen-tsaotra :,:}
', '#1-I-3', '#1') /end

insert into song values (10261, '26. Jehovah tsy mba manadino', NULL, NULL, '
{1.
Jehovah tsy mba manadino
Ny mpanompony manoa;
Fa hotsarovany ''reo mino
Nofidiny Ho Azy koa.
Manerinerina ambony
Ny fiandrianany;
Kanefa kosa re ny fony
Mitsinjo ''zay ambany.}

{2.
Izaho dia ravoravo
Ka midera Azy izao;
Fahasoavanao, ry Avo,
No namonjy hatrizao.
Fahavokisan''ny fanahy
Ary fisandratana
No hotantaninao ho ahy:
Ny foko mankalaza.}
', '#1-I-3', '#1') /end

insert into song values (10271, '27. Fitiavana tokoa', NULL, NULL, '
{1.
Fitiavana tokoa
Re, ny Ray an-danitra,
Fa manolo-java-tsoa
Tena mahasambatra,
Ho antsika mpanota,
Ory, osa, rera-po.
Izy masina sy to,
Ta-hanaisotra ny ota
Noho ny fanavotam-be
Vitan''i Jesosy, e!}

{2.
Tsy ho very, ry mahantra
Sy miferin''aina izao,
Fa ny Ray ''zay feno antra,
Aoka mba hinoanao.
Sambatra ny voavela
Heloka tokoa re,
Fa Jesosy manome
Fanavaozam-po sy lela,
Hira fideram-baovao
No ho re an-dàlanao.}

{3.
O, ry Rainay be fitia
Mba ampio izahay
Tsy ho kivy tsy hania
Ao am-pandehananay,
Fa handroso mba hankany
Aminao an-danitra.
Ry Andriamanitra,
Be loza aty an-tany
Fa ny ranomasonay
Faohinao rahatrizay.}
', '#1-I-4', '#1') /end

insert into song values (10281, '28. Zanahary tsy mba tia', NULL, NULL, '
{1.
Zanahary tsy mba tia,
Ny fahaverezanao,
Izy Rainao be fitia,
Tsy mba mahafoy anao.
Ny famoizam-po ario,
Fa Jesosy itokio
Mba hatoninao anio
''lay be Fahasoavana!}

{2.
Izy dia nitady ela,
Vao nahita anao izao
Mifalia, tsy  ho ela
Dia hody ianao.
He, ry ondry saika very,
Ory, osa, feno fery,
Ny mpamonjy manan-kery
Hahasitrana anao!}

{3.
Ny ao an-danitra mifaly,
Raha misy miova fo
Ary any ny anjely
Miramirana avokoa.
Fo marary dia tsaboiny,
Ahiahim-po vonoiny,
Fiadanana lazainy;
Sambatra isika re!}
', '#1-I-4', '#1') /end

insert into song values (10291, '29. Ry foko, mitsangàna izao', NULL, NULL, '
{1.
Ry foko, mitsangàna izao
Ka miderà ny Tomponao,
Andriamanintsika.
Nasehony nyheriny,
Ny fasana levoniny
Tsy hahatàna antsika,
Derao! Hobio!
Haleloia sy hosana, Fanjakana,
Azy Tompom-pahefana.}

{2.
He! Vitanao Mpanavotra
Ny lova mahasambatra:
Ny fasana resenao.
Mahazo toky fatratra,
Ny mitomany sasatra:
Ny lanitra vohanao.
Jeso Kristy
''Zay nitsangana maraina
Tompon''aina:
Andro Paska Arahabaina.}

{3.
Derao ny voninahiny!
Ankalazao ny heriny,
Fa resy re Satana,
Ry tany sy ry lanitra,
Derao Andriamanitra
akaro ny Hosana,
Raiso, Tompo,
Raisonao ny voninahitra
Aman-dera:
Ho anao, ry Zanak''ondry.}
', '#1-I-4', '#1') /end

insert into song values (10301, '30. Paradisako, ry Jeso', NULL, NULL, '
{1.
Paradisako, ry Jeso,
Fifaliako Ianao;
Lanitry ny foko eto
Sy Edenako vaovao!}

{FIV.
Paradisa soan''ny foko
Eto an-tany Ianao;
Fanaperan-tsoako,
Ao an-danitra vaovao!}

{2.
Volafotsy, volamena,
Vatosoako Ianao.
Zava-tsoako sy harena
Miafina ao an-danitra ao.
[FIV.]}

{3.
Zo sy hajako tokoa
Ianao, Mpanavotro,
Haingo sarobidy koa,
Satro-boninahitro,
[FIV.]}

{4.
Zavatra rehetra ho ahy
Ianao, ry Jeso tia;
Fiainan''ny fanahy,
Fiadanako doria
[FIV.]}
', '#1-I-4', '#1') /end

insert into song values (10311, '31. Misaotra Anao, izahay ry Ray', NULL, NULL, '
{1.
Misaotra Anao, izahay ry Ray,
Fa sitrakoa homena ho anay
Ny Zanakao Malalanao;
''Lay Tokanao no he natolotrao.
Atolotray  ny saotranay,
O! Raisonao anie Jehovah Ray.}

{2.
Misaotra Anao, Jesosinay
Ny fitiavanao tsarovanay:
Nalatsakao ny rànao soa
Hanavotra aina ny mpanota voa:
Atolotray ny saotranay,
Ka raisonao e, ry Jesosinay}

{3.
Misaotra Anao, Fanahy koa
Mamelona sy monina ao am-po,
Havaozy ao anty ao
Ho tonga lapanao madio sy vao.
Atolotray ny saotranay
Ka raiso, ry Fanahy tokinay.}
', '#1-I-4', '#1') /end

insert into song values (10321, '32. Misaotra anao ''zahay', NULL, NULL, '
{1.
Misaotra anao ''zahay,
Jehovah Tompo o!
Fa efa naharay
Ny soa taminao,
Ka lalandava no
Hisaoranay Anao
Ho eo am-bavanay
Tokoa ny Teninao.}

{2.
Fa reharehanay
Sy famonjena koa
Ny Tompo tia anay,
Tsy manam-paharoa,
Ny fandresena soa,
Ny fiononam-po
Dia avy taminao
Ka hery manavao.}

{3.
Ny olo-mino Anao
Sy ireo mpanomponao
Na velona na maty,
Dia misaotra Anao.
Ka sambatra tokoa
Fa manana Anao
''Zahay, ry reharehan''
Ny fanahy o!}
', '#1-I-4', '#1') /end

insert into song values (10331, '33. Tompo o, tsy takatray', NULL, NULL, '
{1.
Tompo o, tsy takatray
Ny hasoan''ny antranao;
Zava-mahagaga anay
Ny fitiavanao,
He, mpanota izahay
Nefa hasoavinao;
Avotr''aina azonay;
Misaotra no atao.}

{2.
Tompo o, Mpanjakanay
Fatratra ny herinao
Ts''isy zavatra izay
Mahasakana Anao!
Nefa mora fo Ianao
Fa mandefitra aminay,
Koa dia misaotra Anao
Fa afaka izahay.}

{3.
Tompo Andriamanitra,
Avy izahay izao
Raiso ny fiainanay;
Anao, ka anjakao.
Tompo o, tsy takatray
Ny hasoan''ny antranao;
Zava-mahagaga anay
Ny fitiavanao.}
', '#1-I-4', '#1') /end

insert into song values (10341, '34. Tompo o, Tsitoha', NULL, NULL, '
{1.
Tompo o, Tsitoha,
Zanaharinay
Asan-tananao re
''Zao rehetra izao
Mahagaga indrindra
Ny fahaizanao
Entinao mitondra
sy miasa ao.}

{2.
Ianao, Jehovah
Ao mandrakizay,
Na miovaova
''zao rehetra izao,
Mahagaga indrindra
Ny fitiavanao
Tsy mba mety miova,
Na mandao anay.}

{3.
Ray niandohana
Fony hatrizay;
Mbola hiafarana
Ka rahatrizay.
Raiso moramora
Re ny zanakao,
Raha tonga ny ora
Hiantsoanao.}
', '#1-I-4', '#1') /end

insert into song values (10351, '35. Isaoranay, ry Ray', NULL, NULL, '
{1.
Isaoranay, ry Ray,
Ny fitiavanao;
Fa indro, azonay
:,: Ny taom-baovao :,:
Ry Tompo o! Ambeno
''Zao fifaliana izao
Ka aoka re ho feno
Ny hasoavanao.}

{2.
Ny taona vao lasa
Nipaoka olon-tsoa
Ireny mba nikasa
:,: Mba ho tafita koa :,:
Ny fitondrana anefa
Afeninao, ry Ray!
Ka nony miantefa
Voa sina izahay.}

{3.
Fanahy o, avia
Hitari-dàlana e!
Mandroso izao ny dia,
:,: Hamaky rano be :,:
Ny herinao tsy lefy
NO tanjaky ny fo;
Ka ampy mba ho fefy,
Raha avy ny manjo}

{4.
Itoero, Jeso tia,
Ny sambonay re!
Na tony na maria,
:,: ''Ty ranomasim-be :,:
Raha hianao no hita
Tsy fefika izahay
Ambara-piampita
Ary an-tratran-dRay.}
', '#1-I-4', '#1') /end

insert into song values (10361, '36. Moa ho haiko tantaraina', NULL, NULL, '
{1.
Moa ho haiko tantaraina ny hasoavanao?
Mifamatra tamin''aina ny fitiavanao
Ny vahoaka sesehena
''Zay nahazo famonjena
no hider''Ilay Mpamonjy soa.}

{FIV.
:,: Jesoa no derao :,:
''Zay nataony ambarao
Jeso no derao.}

{2.
Fiadanana nafoin''i Jeso Tomponay,
F''efa tonga ny fotoana
Sitraky ny Ray
Olom-bery nozahany
Ny fatorany vahany
Hajambana koa nalany tsy ho ao.
[FIV.]}

{3.
Hoderaina laladava Ny Anaranao,
Fa ny aizina nisava
Eo imasonao;
Na tsy tian''ny sasany aza
Ny Anaranao malaza,
''Zaho kosa hankalaza Azy re
[FIV.]}
', '#1-I-4', '#1') /end

insert into song values (10371, '37. Mihainoa ry Ziona, o!', NULL, NULL, '
{1.
Mihainoa ry Ziona, o!
Avy ny Mpanjakanao!
Re hatrany lavitra,
Fa hobin''ny olona,
Mifalia hianao,
Indro Izy, ka tsenao!}

{2.
Indro Ilay miakatra
Mila tsy ho fantatra
Ho Mpanjaka marina,
Fa mietry lalina;
Izy no Mpanjakanao,
Ry Ziona ka derao!}

{3.
Mandrosoa, Tompo o!
Mba hanjaka amiko;
Faly re ny olonao
Raha tonga Ianao
O, hosana, Tomponay!
Ry Ilay nandrasanay!}
', '#1-II-1', '#1') /end

insert into song values (10381, '38. He, hosana!', NULL, NULL, '
{1.
He, hosana!
Isaoran''anie ny Mesia,
Izay nelefan''ny Tompo,
Ka Hosana no asandratray.
Ilay nirahinao ry Tompo
No derainay,
Ka hosana no asandratray!}
', '#1-II-1', '#1') /end

insert into song values (10391, '39. Faingàna, ry Mpanjaka!', NULL, NULL, '
{1.
Faingàna, ry Mpanjaka!
Handray ny lovanao;
Faingàna re mba haka
Ny tany ho Anao;
Avia hampifaly
Ny malahelo fo;
Afaho ny mijaly
Sy azon''ny manjo.}

{2.
Avia, fa misento
Aty ny olonao;
Ny fanjakana ento,
Fa Tompo Ianao.
Tsy hisy hitomany
Eo anatrehanao.
Hiadana ny tany
Izay alehanao}

{3.
Ny tendrombohitra avo
Ny lohasaha koa
Ho tonga ravoravo
Sy tretrika avokoa;
Ny be tsy haka an-kery,
Ny kely afa-po
Tsy hisy izay ho very,
Tsy hisy izay hanjo.}

{4.
Mpanjaka sy Mpiaro
Mahery Ianao;
Ny firenena maro
Hitoky Aminao;
Ho avy lalandava
Izao rehetra izao
Hitondra harem-bevava
Ho eo an-tongotrao.}

{5.
Faingàna, ry Mpamonjy,
Ry Tompo be fitia,
Ny olonao tra-bonjy
Miantso hoe : Avia!
Anao ny fiderana
Anao ny laza be,
Anao ny fanjakana
Sy arahaba e!}
', '#1-II-1', '#1') /end

insert into song values (10401, '40.Tonga ny fahazavan''i Ziona', NULL, NULL, '
{1.
Tonga ny fahazavan''i Ziona
Zavo ny tany ''Zay maizina teo
Tapitra koa ny fidradradradrana
Ary ny tany miadana izao.}

{2.
Tonga ny fahazavan''i Ziona
Efa nandrasana izay ela izay,
Izao dia tapitra ny fanompoana,
Ka ampodiana ny babo indray.}

{3.
He, misy rano izay miboiboika
Any an''efitra maina sy lao,
Feno ny  hobiny ny lohasaha,
Manakoako ny hira vaovao.}

{4.
Indro ny tany sy ny nosy rehetra
Dia mankalaza ny Tompo izao,
Ny fanjakany dia tsy manam-petra
Miravoravo ny tany vaovao.}
', '#1-II-1', '#1') /end

insert into song values (10411, '41.Indro Jeso mandondona', NULL, NULL, '
{1.
Indro Jeso mandondona
Eo ambaravaram-po,
Injao ny feony mifona
Mba haneho indrafo,
Aoka Izy hovohàna,
Fa mitondra famelan
Hampahery sy hitana
Ny fanahinao sy fo.}

{2.
Efa novidiny lafo
Mba ho afaka hianao;
Izy hiara-misakafo
Sy hifaly aminao;
Fahasoavana zaraina,
Lova ampanantenaina,
Iray ny vatsy, iray ny aina,
Ndrey, ny hasambaranao!}
', '#1-II-1', '#1') /end

insert into song values (10421, '42.Vohay ny varavaram-po', NULL, NULL, '
{1.
Vohay ny varavaram-po
Ilay Mpanjaka manan-jo.
Mpamonjy izao tontolo izao
No tonga  hanasoa anao!}

{2.
Ny teny fanasana atao
Mitondra tena aim-baovao,
Ny fahoriana manjo
Afahan''ny famindrampo.}

{3.
Mba mihainoa, maneke,
fa sambatra izay mandre,
Ka anjakan''ny Tompo to,
Izay feno antra, mora fo.}

{4.
Ny andron''aina mitatao
No mahafaly fo izao,
Mamiratra sy manome
Fanantenana lehibe.}

{5.
Ny vavahady mba vohay!
Finaritra izay mandray,
Fa tonga Ilay Mpanjaka soa
Hanao ny asany tokoa.}

{6.
Sokafanay ny fo izao,
Ry Jeso, mba hidiranao,
Ny fahasoavanao anie
Hanananay harena be.}

{7.
Atolotray, ry Tompo o,
Mba hahatamana Anao ny fo,
Ny famonjena vitanao
No iankinanay izao.}
', '#1-II-1', '#1') /end

insert into song values (10431, '43.Manenoa, ry farara!', NULL, NULL, '
{1.
Manenoa, ry farara!
Horohoroy, ry tany
Ka miantsoa ny maty,
Fa akaiky ny Mpitsara;
:,: Indro Kristy :,:
Avy amin-drahona!}

{2.
Ny feony no hampifoha
Ny any am-pasana;
Ny teniny no hamory
Ny velona rehetra;
:,: Samy handray :,:
Ny valin''ny nataony.}

{3.
Ny tsy mino hilazany:
"Mialà voaozona,
Mandehana, mitoera
Ao amin''ny maizina"
:,: Ry mpanota! :,:
Any ny anjaranao.}

{4.
Fa ny mino hilazany:
"Avia, notahina,
Mandrosoa, mankanesa
Ho amin''ny mazava"
:,: Io, ry mpino!" :,:
Indro! ny anjaranao.
}
', '#1-II-1', '#1') /end

insert into song values (10441, '44. Indro Jeso, mba voahy', NULL, NULL, '
{1.
Indro Jeso, mba voahy;
Mba vohay!
Avy ho sakaizanay
Ka vohay!
Lay Malala fahizay
Sady sitraky  ny Ray,
Tonga ho anao indray,
Ka vohay!}

{2.
Indro Jeso, mba vohay;
Mba vohay!
Tompo tia ka mba hajao;
Mba vohay!
Honina aminao ety,
Ho sakaiza hatrery,
Ho anao mandrakizay;
Ka vohay!}

{3.
Indro Jeso, mba vohay,
Mba vohay!
Mbola tia ka mba vohay
Ka vohay!
Tena tia ka tsy mandao,
Raha ela sao dia lao;
Moa holavinao izao?
Mba vohay!}

{4.
Eo am-baravaranao;
Mba vohay!
Tsy manao an-ditra izao;
Ka vohay!
He! Mandona mora re,
Na dia Tompo lehibe,
Efa niandry ela be;
Ka vohay!}
', '#1-II-1', '#1') /end

insert into song values (10451, '45.He, tonga ho antsika', NULL, NULL, '
{1.
He, tonga ho antsika
Ny Tompo mora fo
Mpamonjy, havantsika,
Ilay mamindra fo
Andeha mba hifaly
Isika ry zareo
Nahazo Mpampifaly,
Ka manandrata feo!}

{2.
Hajao ny Tompontsika
Mpanjaka lehibe,
Andriamanintsika,
Mpanala rofy be;
Avia, mba hidera,
Ry mino tsara fo!
Avia manatera
Ny saotra ao am-po.}

{3.
Ry Tompo, Zanahary!
Mihira izahay,
Fa tena mahafaly
No tonga ho anay;
Ny andro famonjena
No andro sitrakay;
Fanahy manatena
No ifalianay.}

{4.
ry Tomponay! Mandraisa
Ny haja entinay;
Tsy volamena anefa,
Fa saotra aterinay;
Ry Jeso Tompo soa!
Isaoranay Ianao,
Fa lehibe tokoa
Ny fitiavanao.}
', '#1-II-1', '#1') /end

insert into song values (10461, '46.O! ry Mpanefa ny toky rehetra', NULL, NULL, '
{1.
O! ry Mpanefa ny toky rehetra,
Moa ho ela vao tonga ny fetra?
Tena handrasanay ny fihavianao:
:,: Aza ela, re! :,:}

{2.
He! Izahay, fa vahiny mandalo
Indro manenjika ny fahavalo;
Koa henoy re ny senton''ny olonao:
:,: Aza ela, re! :,:}

{3.
He! Ny voavotrao dia miandry,
Na izahay na ireo nodimandry;
Tompo, tsinjovy ny fasan''ny olonao:
:,: Aza ela, re! :,:}

{4.
Ary ny zavatra eto an-tany
Samy miara-misento ihany,
Ka manantena ny mba hihavianao:
:,: Aza ela, re! :,:}

{5.
Mbola ho ela va Ilay antenaina?
Tsia, fa akaiky izao ny maraina;
Koa andrandrao sy antsoy ny Tompo hoe:
:,: Aza ela, re! :,:}

{6.
"Tsy mba ho ela, fa faingana Aho,
Ary hanafaka ny mitalaho".
"Eny, avia, faingàna, ry Tompo o!"
:,: Aza ela, re! :,:}
', '#1-II-1', '#1') /end

insert into song values (10471, '47. Ry Ziona, mihainoa!', NULL, NULL, '
{1.
Ry Ziona, mihainoa!
Injay feo mafy mampifoha
Ny ampakarina izao.
Mbola alim-be ihany,
Fa ny fotoana no lany,
Ka avy ny malalanao.
Antsoiny hianao,
Alao ny jironao
Ka velomy;
Dia miaingà hivoaka,
Hitsena ny Mpampakatra!}

{2.
He, Ziona ravoravo,
Fa reny izao ny antso avo,
Ka velomy ny hobiny:
Indro, avy Kristy Tompo,
Hanafaka anay mpanompo,
Halainy hampakariny.
He, faly re ny fo!
Avia, Tompo o!
O, Hosana!
Mihaingo izao hitsena Anao,
Ry Kristy, ny malalanao!}

{3.
Hira, hoby, fiderana
Asandratray mba hitsenana
Anao, Jesosy Tompo o!
Tsy mba misy mitomany,
Fa mirana izay mankany
Am-pampakaram-badinao
Mahafinaritra
Ny hiran-danitra!
Haleloia!
Ilaozanay ny trano lay,
Miakatra ny dianay.}
', '#1-II-1', '#1') /end

insert into song values (10481, '48. Hevero ry mpanota o', NULL, NULL, '
{1.
Hevero ry mpanota o,
Ny andro fitsarana!
Hiverina ny Tomponao
Hamaly fahotana;
Ny lanitra hahorona,
Ny tany dia hodorana,
Fa avy Ilay Mahery.}

{2.
Trompetra no ho velona
Hotsofin''ny anjely;
Hisokatra ny fasana,
Hivory izay niely;
Ny maty dia hatsangana,
Isika koa hakarina
Hanatrika ny Tompo.}

{3.
Ny boky hovelarina,
Fa io no manambara
Ny asa izay miafina,
Handraisana anjara
Ka aza mba mihevitra
Hahazo mihodivitra,
Fa fantatry ny Tompo.}

{4.
Ry Tompo o! Hazoninay
Ny tenim-panekena,
Ka mba arovy izahay
Hitana famonjena:
Ny ranao sy ny Fanahinao
No tokinay raha avy izao
Ny andro fitsarana.}
', '#1-II-1', '#1') /end

insert into song values (10491, '49. Mba jereo ny Tompontsika', NULL, NULL, '
{1.
Mba jereo ny Tompontsika
Avy ao an-danitra
Indro avy amin-kery
Ao amin''ny rahona.
:,: Haleloia! :,:
Ambarao ny heriny.}

{2.
''Zay rehetra fahavalo
Efa resiny tokoa;
Mifalia, ry Anjely!
Mihobia, ry olona!
:,: Haleloia! :,:
Ambarao ny lazany.}

{3.
Fony mbola teto Izy,
Satro-tsilo nentiny;
''Zao ny satro-bolamena
Mendrika ny lohany;
:,: Haleloia! :,:
Ambarao ny derany.}

{4.
''Zay rehetra fahavalo
Efa resiny tokoa;
Mifalia, ry anjely!
Mihobia, ry olona!
:,: Haleloia! :,:
Ambarao ny lazany.}
', '#1-II-1', '#1') /end

insert into song values (10501, '50. Mifohaza, ry matory', NULL, NULL, '
{1.
Mifohaza, ry matory.
Fa re ny feon''ny Mpamory
Miantso izao tontolo izao.
Miaingà, ry vovontany,
Fa he! Jesosy avy any,
An-danitra hitsara ano.
Ry mbola velona!
Ry ao am-pasana!
Mitsangàna!
Miandrandrà ny lanitra,
Fa ny aty dia tapitra!}

{2.
Kristy Tomo avy any,
Ka dia mangovitra ny tany
Fa be ny voninahiny
Na ny ratsy na ny tsara
Hanatrika Ilay Mpitsara,
Miandry ny anjarany.
Zarainy roa izao
Ny tafavory ao;
Ka ny tsara
Horaisiny ao aminy,
Ny ratsy dia ariany.}

{3.
Jeso o! izahay mifaly,
Na dia be ny mampijaly
Sy mahaketraka aty:
Ianao mahay manova
Ny fahorianay ho lova
Mahafinaritra ery.
Ry fasa-maizina o!
Ry fahavaloko!
Aiza moa
Ny herinao, sy sabatrao?
Jesosy no Mpandresy izao.}
', '#1-II-1', '#1') /end

insert into song values (10511, '51. Raha tonga anio ny Tompo', NULL, NULL, '
{1.
Raha tonga anio ny Tompo
Amin-kery lehibe,
Ka arahin''ny mpanompo
Sy anjely marobe;}

{FIV.
:,: Moa ho faly hianao? :,:
Ka hidera ny Mpamonjy
''Zay nanavotra aina anao.}

{2.
He! Ho simba sy ho rava
''Zao rehetra àry izao,
Ka hifankahita tava
Ny natao sy ny Mpanao
[FIV.]}

{3.
Maso maro no hijery
''Lay nijaly fahizay
Fa ny mino Azy ihany
No ho faly rahatrizay;
[FIV.]}

{4.
Rehefa tonga re ny Tompo
Mba hitsara izay natao.
Hitomany ny tsy mino
Fa hanatrika Azy ao:
[FIV.]}

{5.
O! avia re, ry Tompo!
Mba hanjaka aminay,
Hahatonga anay mpanompo
Hiara-paly aminao:
O! Avia, o! avia.
Fa ny olonao rehetra
Dia maniry indrindra Anao.}
', '#1-II-1', '#1') /end

insert into song values (10521, '52. Efa akaiky Jeso Tompo', NULL, NULL, '
{1.
Efa akaiky Jeso Tompo
Haka Anao izao!
Indro tamy Kristy Tompo,
Avy ho anao:}

{FIV.
Dia tano mafy tsara
''zay anananao
Lova soa, tsara fara,
Indro ho anao.}

{2.
Efa akaiky ny Mpitsara
Izay hitsara anao:
Jeso Kristy raiso tsara
Mba ho havanao:
[FIV.]}

{3.
Efa akaiky ny Mpamonjy
Ta-handray anao:
Jeso Kristy mahavonjy
No sakaizanao:
[FIV.]}

{4.
Aza mba mangataka andro
Hibebahanao;
He, mazava anio ny andro
Mba hidiranao:
[FIV.]}
', '#1-II-1', '#1') /end

insert into song values (10531, '53. Miposaka ny vonjy', NULL, NULL, '
{1.
Miposaka ny vonjy,
Endrey ny hafaliana!
Jesosy no Mpamonjy,
Andriam-piandanana,
''lay Zanaky ny avo
Dia nidina taty
Hahatonga antsika lavo
Ho zanaka ary,
Manangana olom-bery
Handova lanitra,
Ka dera, laza, hery
Ho an''Andriamanitra!}
', '#1-II-2', '#1') /end

insert into song values (10541, '54. Jeso Mpamonjy, ''lay Tompon''ny aina, no hita', NULL, NULL, '
{1.
Jeso Mpamonjy, ''lay Tompon''ny aina, no hita,
Nidina teto an-tany hamonjy antsika.
O, mba derao!
Izy Mpanavotra anao,
Ka mifalia, ry mino!}

{2.
Indro ny tenin''anjely milaza ny soa!
Teraka Jeso Mpamonjy antsika tokoa :
O, mba derao!
Tonga ny andro vaovao,
Andron''ny Filazantsara}

{3.
Dia iventesonao hira vaovao re, izany!
Tonga Jesosy hamonjy izay mitomany
O, mba derao!
Afaka isika izao!
Izy mpisolo antsika!}

{4.
Tonga ny Tompon''ny tompo
hisolo ny ory
Ka mifohaza, ry olona mbola matory
O, mba derao!
Amin''ny feo vaovao,
Jeso, ''zay Tompo mahery!}
', '#1-II-2', '#1') /end

insert into song values (10551, '55. Ny andron''ny fahasoavana', NULL, NULL, '
{1.
Ny andron''ny fahasoavana
Miposaka ho antsika,
Manerana izao tontolo izao,
Ny tarany efa hita,
Ka ravo ny olom-belona,
Fa lasa ny alin-tsika}

{2.
Niova tery ny alina
Fa teraka ny Mpamonjy,
Mamiratra be ny lanitra,
Miseho izao re ny vonjy,
Ka lasa tokoa ny aizina,
Mihoby ny voavonjy.}

{3.
Na dia ho nahay nihira re
Ny hazo an''ala rehetra,
Ka afa-mikalo ny ravina
Hidera ny Avo indrindra,
Tsy ampy ho fanandratana
Ny Tompon''ny ain-drehetra.}

{4.
Ny feon''ny olonao izao
Misandratra, ry Jehovah
Hidera ny fitiavanao
Sy ny indrafonao soa
Niaro ny fiangonanao
Hatramin''ny fahagola}

{5.
HO any an-tanindrazanay
''zahay re, ry Ray Tsitoha,
Ka hiditra ao an-tranon-dRay
Nomanin''Ilay Mpialoha;
Ny olonao dia ho tafaray
Hidera anao tokoa!}
', '#1-II-2', '#1') /end

insert into song values (10561, '56. Manatona Anao izao', NULL, NULL, '
{1.
Manatona Anao izao,
Ry Jeso ny madinikao,
Ampandrosoy ny dianay,
Fa ta-hahita Anao ''zahay.}

{2.
Hidera ny Anaranao
No zava-tianay hatao;
Miravoravo izahay,
Fa teraka ny Tomponay.}

{3.
Ny lanitra nilaozanao,
Hanavotra ny olonao,
Ho zanak''Andriamanitra,
Hifaly ao an-danitra.}

{4.
Tsy takatray heverina
Ny fitiavan-dalina,
Izay nitiavanao anay,
Ka dia misaotra Anao izahay.}

{5.
Tariho, Tompo, izahay.
Ambininao ny dianay,
Fa samy ta-handray Anao
''zahay mivory eto izao.}

{6.
O, ry Sakaiza maminay,
Mba te-ho olonao izahay
Hipetraka eo anilanao,
Fa mamy ny fitianao.}

{7.
Raha manana Anao izahay,
Tsy hisy hatahoranay
Fa zaza voamarikao
Ho isan''ny arovanao.}

{8.
Raha ao an-danitra vaovao
''zahay sy ny anjelinao
Hisaora sy hidera Anao
Manatrika ny tavanao.}
', '#1-II-2', '#1') /end

insert into song values (10571, '57. Izao Noely mamy ''zao', NULL, NULL, '
{1.
Izao Noely mamy ''zao
No ihobiantsika,
Fa noresen''ny aim-baovao
Ny fahafatesantsika,
Zanak''Andriamanitra
Tompom-boninahitra
Nandao ny lapan-dRainy,
Nateraka ho olona,
Hamonjy sy hamelona
Antsika sombin''ainy!}

{2.
Velomy ny fisaorana,
Na kely monja aza,
Ka aoka izay navotana
Hiara-hankalaza
Ny zanak''Andriamanitra
Niala tao an-danitra
Hanafaka alahelo:
Ry! Voavonjy o! Derao
Jesosy ''lay Mpanjakanao
He, gaina izao ny helo!}

{3.
Izao Andriamanitra
Dia faly amintsika:
Jesosy, Tompon-danitra,
No tonga namantsika:
Ho re hatrany lavitra
Fa foin''Andriamanitra
''lay sitraky ny fony
Hitondra fahoriana,
Hiari-pijaliana,
Ho solon-ny mpikomy.}

{4.
Na dia traty ny manjo,
''zay mampiferin''aina,
Izaho tsy hamoy fo,
Fa manan-kantenaina;
Jesosy dia zokiko,
Ka tsy hohadinoiko,
Fa izy tia ahy;
Ny tenim-piadanany
No hery anohanany
Ny foko izay manahy.}

{5.
Haleloia, fa tapitra
Ny fahoriantsika!
Derao Andriamanitra,
''zay efa tia antsika!
O, ry fanahiko, derao
Jesosy, ''zay Mpamonjinao,
Harena tsy ho lany,
Haleloia, haleloia!
Fa azo ny Mpanavotra,
anao ny fanjakany!}
', '#1-II-2', '#1') /end

insert into song values (10581, '58. Alina masina', NULL, NULL, '
{1.
Alina masina,
Tonga ny Mpanavotra;
Indro teraka ao Betlehema
Ny Mpajakan''i Jerosalema
:,: ''zay Mpamonjy anao. :,:}

{2.
Alina masina,
Ny anjely midina
Ka miara-mihoby hoe:
Indro ny fiadanam-be,
:,: Jeso teraka izao :,:}

{3.
Alina masina,
Nisehoan''ny Zanaka,
Ny mazava niposaka,
Ka ny tahotra afaka;
:,: Mifalia izao. :,:}
', '#1-II-2', '#1') /end

insert into song values (10591, '59. O Mihobia, fa teraka', NULL, NULL, '
{1.
O Mihobia, fa teraka
Jesosy Tompo, Zanaka,
Mesia nantenaina,
Mpamonjy sitraky ny Ray
HO avotra mandrakizay,
Ka mendrika hoderaina!
Renao izao
Ny anjely mampiely
Teny soa:
Kristy teraka tokoa.}

{2.
He! Tonga ny Mpanavotra
Hanafaka ny tahotra
Mamatotra ny tany,
Mitondra fiadanana,
Ka tena mampihavana
Ny lanitra aman-tany.
Indro tonga
Ny Mpamonjy mahavonjy
Ny mpanota
Ka manaisotra ny ota.}

{3.
Omeo voninahitra
Ny Ray, Andriamanitra,
''zay Tompom-pamonjena!
Ny Zanaka no raisonao,
Fa fananganana anao,
Ny lova dia homena.
Amen, Amen!
Teny soa mamy koa,
/sika lavo
Tonga zanaky ny Avo!}
', '#1-II-2', '#1') /end

insert into song values (10601, '60. He faly ny fanahiko', NULL, NULL, '
{1.
He faly ny fanahiko,
Fa tonga ''lay malalako,
Dia Jeso tonga zaza,
Ka mety ho Mpamonjiko,
Na dia tsy mendrika aza
Izaho, ry Jesosy o!
:,: Raiso ho Anao :,:}

{2.
Ry Jesosy Zazakely o,
Iriko mafy Ianao,
Avia re, ry Zaza,
Ny foko itoeranao
Na dia ratsy aza
Ka tsy mba mendrika Anao,
:,: Ry Jesosy o! :,:}

{3.
Endrey ny fitiavanao,
Ry Ray, namoy ny Zanakao;
''zahay, izay fahavalo
Ka ivesaran-keloka,
Avelanao hivalo
Ka hiditra ao an-danitra
:,: Enga ka ho ao! :,:}

{4.
Endrey ny fifaliana
Tsy tratry ny fisainana
Ataon''ireo anjely
Manerana ny lanitra!
Fa any ny Noely
Hitohy tsy ho tapitra,
:,: Efa ka ho ao! :,:}
', '#1-II-2', '#1') /end

insert into song values (10611, '61. Tany Betlehema fahiny', NULL, NULL, '
{1.
Tany Betlehema fahiny,
Nisy Zaza teraka,
Tonga toy ny mpivahiny,
Nefa tena Zanaka.
Izy no Jesosinao,
Ka mba raiso ho anao.}

{2.
Ao ny lapa feno haja
Nisy Azy fahizay;
Saotra, dera, aman-daza
Teo anatrehan-dRay,
Izany no nilaozany
Hamonjeny ny aty.}

{3.
Zaza tsy nanota Izy,
Nefa Zaza toa anao,
Koa raisonao, rankizy,
Mba ho tena havanao;
Ary mba arahonao
Izay fanaony hitanao.}

{4.
Raha sendra mitomany,
Izy hampangina anao,
Izy koa hampahamamy
Izay fifalianao;
Matokia hianao,
Izy no sakaizanao!}

{5.
Nefa mba henoy ry Zaza:
Ao an-danitra Izy izao,
Fa ho avy indray hitsara
An''izao tontolo izao;
Koa mba fidionao
Izy ho Mpamonjinao.}
', '#1-II-2', '#1') /end

insert into song values (10621, '62.He teraka any Betlehema ', NULL, NULL, '
{1.
He teraka :,: any Betlehema :,:
Ny Tompon''i Jerosalema
:,: Haleloia! :,:}

{2.
Ny vilona :,: nandriany :,:
Nihoby ny anjeliny,
:,: Haleloia :,:}

{3.
Ny magy dia :,: nanatitra :,:
Ny miora sy ny hanitra,
:,: Haleloia :,:}

{4.
Voaisotra :,: ny helokao :,:
Fa tonga ny Mpanavotrao
:,: Haleloia :,:}

{5.
Isika dia :,: noraisiny :,:
Hiombon-dova aminy,
:,: Haleloia :,:}

{6.
Isika koa :,: hianatra :,:
Ny fideran''ny lanitra
:,: Haleloia :,:}

{7.
Isika ao :,: an-danitra :,:
Ho velona finaritra,
:,: Haleloia :,: }

{8.
Ho naman''ny :,: anjelinao :,:
Izahay, ry Jeso Tompo o,
:,: Haleloia :,:}

{9.
Hidera ny :,: Anaranao :,:
Manatrika ny tavanao,
:,: Haleloia :,:}
', '#1-II-2', '#1') /end

insert into song values (10631, '63. Tany finaritra', NULL, NULL, '
{1.
Tany finaritra,
Tarafin-danitra;
Mirana ny mpivahiny ao,
Injao fihobiana
Am-pandehanana
Makany ny Paradisa izao.}

{2.
Andro mihelina,
Tao-mitsingerina,
Ifandimbiasan''ny olona,
Tsy lefy hanitra;
Ny soan''ny lanitra,
Mamelon-kira velona.}

{3.
Teny naely,
Hiran''anjely;
O! mifalia, ry reraka!
Fa fiadanana
No andehanana,
Fa ny Mpamonjy teraka!}
', '#1-II-2', '#1') /end

insert into song values (10641, '64. Injany hiran-danitra', NULL, NULL, '
{1.
Injany hiran-danitra
Misasak''alina;
Lokanga volamena e,
Tendren''ny masina.
Ny tany dia nagina re,
Nandre ny hira vao;
Ho an''ny olona anie
Ny fiadanana.}

{2.
Ny hiran''ny anjely re
Tsy mba mitsahatra,
Ary ambony rahona;
Ireo masina
Mitendry ny lokangany
Am-pihobiana!
Ho an''ny olona anie
Ny fiadanana.}

{3.
O! ry mivesatra entana,
Ry malahelo o,
Mba andrandrao ny lanitra,
Henoy ny hira vao!
Ny hiran''ny anjely re
Dia mampiadam-po.
O! mitsahara hianao
Hihaino azy izao!}

{4.
Ny andron''olombelona,
He fandalovana!
Kanefa hisy tany vao
Izay haharitra.
Endrey ny fiadanana
Ary rahatrizay!
Ny hiran''ny anjely re
Hiredona indray!}
', '#1-II-2', '#1') /end

insert into song values (10651, '65. Ry Betlehema kely o!', NULL, NULL, '
{1.
Ry Betlehema kely o!
Sondiran-tory ve?
Kanefa injay miseho ao
Ny hazavana be.
Ny toky sy ny tahotra
Mihaona ankehitrio,
Fa Kristy ''Lay Mpanavotra
No teraka anio.}

{2
Ry Kontana maraina o!
Fitarik''andro soa,
Torio fa tonga aty izao
Ilay Mpanjaka soa!
Ka he! Ny voninahitra
Ho an''Andriamanitra!
Fiadanana tsy tapitra
Ho an''ny olona!}

{3.
Endrey, mangina fatratra
Ny fampidinana
Ny fitahian-danitra
Am-pon''ny olona!
Tsy misy tabataba re,
Na horakoraka,
Kanefa Kristy tonga anie
Am-po mangoraka!}

{4.
Rankizy hendry sy madio,
Tsarovy Kristy izao,
Fa Izy tonga aty anipo,
Mba ho Mpamonjinao,
Na misy aza ny manjo,
Izao noely izao
No saino fatratra ao am-po
Hampionona anao!}

{5.
Ry Zaza masina sy soa,
Ry zanak''i Maria,
Ry Tomponay Mpamonjy koa,
Midina re aty.
Ny feon''ny lanitra no re,
Indray Noely izay,
Nitondra teny soa hoe:
Ianao no avotray!}
', '#1-II-2', '#1') /end

insert into song values (10661, '66. Ry olona o, avia!', NULL, NULL, '
{1.
Ry olona o, avia!
Ry tany, mifalia!
Mba henoy ny teninay;
Teraka ny Tomponay
Ao an-tranon''omby ao
andeha mitsaoka Azy ''zao;
Teraka ny Tompo!
Teraka Jeso!}

{2.
Ry olna o, avia!
Ry tany, mifalia!
Mba henoy, ry havanay!
Misy zaza hitanay,
Zanak''Andriamanitra
Hamoha indray ny lanitra;
Teraka ny Tompo!
Teraka Jeso!}

{3.
Ry Kristiana o, hobio
Fa afaka hianao anio;
Resy izao ny rafinao,
Afaka ota hianao,
Fahasoan-dehibe
No ho anjaranao anie;
Teraka ny Tompo!
Teraka Jeso!}
', '#1-II-2', '#1') /end

insert into song values (10671, '67. Avia, ry mino!', NULL, NULL, '
{1.
Avia, ry mino!
Hifaly sy hihoby,
Avia, avia hankany Betlehema;
Teraka Jesoa
Tompon''ny Anjely
Avia hitsaoka Azy (in-3)
''zay Tomponao!}
{2.
Tompon''ny tompo,
Andriananahary,
Nefa mba zanana-behivavy koa;
Zanak''i Maria,
Nefa Zanahary;
Avia hitsaoka Azy (in-3)
''zay Tomponao!}
{3.
Derao, ry anjely,
Mihobia mafy,
Ry mponina ao an-danitra ao
ambony ao!
Dera sy laza
Atolory Azy;
Avia hitsaoka Azy (in-3)
''zay Tomponao!}
{4.
He, arahaba,
Tonga ety an-tany
Ny Tompo, Mpamonjy sy
Mpanjaka koa,
Tenin''ny Rainy
Teny tonga nofo,
Avia hitsaoka Azy (in-3)
''zay Tomponao!}
', '#1-II-2', '#1') /end

insert into song values (10681, '68. Re tokoa ny feon''anjely', NULL, NULL, '
{1.
Re tokoa ny feon''anjely,
Irak''Andriamanitra;
He! Ny teny mahafaly
Avy ao an-danitra.
Entiny ny teny tsara
Ho ety an-tany izao
Voninahitra aman-dera
Tompo no omena Anao (in-3)
Voninanihtra aman-dera,
Tompo no omena Anao.}

{2.
Teraka any Betlehema
''Lay Mpanjaka manan-jo,
Ka misatro-diadema,
Voninahi-tsy ho lo.
Misy ''zao ety an-tany
Fihavanana tokoa,
Hafalian-tsy ho lany,
Fitahia-mahasoa. (in-3)
Hafalian-tsy ho lany,
Fitahia-mahasoa.}

{3.
Be ny lazanao, ry Tompo!
Tsara ny Anaranao,
Zovy re no tsy hanompo?
Iza no handà Anao?
Hasina sy arahaba
No aterina aminao;
Fanjakana tsy ho rava,
Ry Mpanjaka no Anao! (in-3)
Fanjakana tsy ho rava,
Ry Mpanjaka no Anao!
}
', '#1-II-2', '#1') /end

insert into song values (10691, '69. Zaza Masina sy soa', NULL, NULL, '
{1.
Zaza Masina sy soa,
Nefa ory Ianao,
Ka nanetry tena koa,
Sy nandao ny lapanao.}

{2.
Teraka ory sy nahantra,
Trano tsy nandrianao,
Fa tsy nisy ''zay niantra,
be ny fahorianao.}

{3.
Fihinanam-bilon''omby
No natao fandrianao,
Fa ny trano be tsy omby,
Nisy olo-maro tao.}

{4.
Nofonosin-damban-jaza,
Tonga zazabodo re,
Nefa Tompo manan-daza
Sy Mpanjaka lehibe.}

{5.
Efa halan''olo-maro,
Nefa kosa tiako;
Dia avia, ry Mpamonjy!
Honina ato amiko.}
', '#1-II-2', '#1') /end

insert into song values (10701, '70. Iza moa io Zazakely', NULL, NULL, '
{1.
Iza moa io Zazakely
:,: Ao Betlehema :,:
Ankalazain''ny anjely
Misasaka alim-be?
Injao! Mihira avokoa
Ny mponina ao an-danitra,
Ka iza re io Zaza soa?
Moa Zanak''Andriamanitra?}

{FIV.
Io no Mpanjaka voatendry,
Sady mahery no hendry,
:,: Zanak''Andriamanintsika,
Koa hobio sy ifalio :,:}

{2.
Izy no Jeso Mpanjaka
:,: Ka mihobia! :,:
Izy mamory sy maka,
Avia, mihobia.
Omeo voninahitra,
Ka mitehafa tanana;
Hobio ry tendrombohitra
Sy lohasaha mavana!
[FIV.]}

{3.
Izy no Tompo nahary,
:,: Ka maneke :,:
Zanaky ny Zanahary,
Ka dia maneke.
Ry hazo ao an''ala o!
Samia re manandra-peo;
Ry voninkazo tsara o!
Atero fofo-manitra;
[FIV.]}

{4.
Efitra sy tany foana!
:,: O! Mihirà :,:
Lemaka ary havoana!
Falia, mihirà;
Ry ranomasin-dehibe,
Ry voron-kely tsara feo!
Izao rehetra marobe!
Avia mankalaza re;
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10711, '71. Mihainoa! Mihainoa!', NULL, NULL, '
{1.
Mihainoa! Mihainoa!
Ny feon''anjely sambatra,
Milaza fa vao teraka
Ny Zanak''Andriamanitra,
Mihainoa izao!}

{2.
Mifalia! Mifalia!
Ry zanak''olombelona,
Fa tonga Ilay Mpanavotra
Antsika izay nifatotra;
Mifalia izao!}

{3.
Mba omeo! Mba omeo!
Ny fonao fa angatahiny;
Ny tenany natolony
Anao izay malalany;
Mba omeo izao!}

{4.
Matokia! Matokia!
Ka manaraha Azy izao;
Ilay Mpamonjy be fitia
Nanetry tena ho anao;
Matokia izao!}
', '#1-II-2', '#1') /end

insert into song values (10721, '72. Teraka Jesosy Tomponay', NULL, NULL, '
{1.
Teraka Jesosy Tomponay,
Taitra ny olona fahizay,
Nisy anjely nihira teo,
Mahafinaritra, tsara feo.
Sady milaza soa aminay.
Dia hihira anio izahay}

{FIV.
Taona fahasambaranay,
Volana fifalianay,
Andro firavoravoana Anao,
Jesosy, Zanaka Masina o!}

{2.
Teraka Jesosy be fitia,
Indro kintana tonga teo,
Avy hanoro ny Zaza soa;
Faly ny Magy, fa lo tokoa,
Io no Mpanjaka mandrakizay
Dia nanaja androtr''izay:
[FIV.]}

{3.
Teraka jesosy Zaza soa,
Zaza Malalan''ny ray tokoa;
Tonga hamonjy io Zaza io,
Teraka ao Betlehema anio;
ento ''zahay, Fanhy, hankao.
Andronay mahafaly izao:
[FIV.]}

{4.
Sambatra manana Anao ho Ray,
Faly sy ravo tokoa izahay;
Jesosy Tompo nirahinao,
Tonga ho nofo ny Zanakao;
Teraka ho Mpanavotra anay
Avy hifaly eto ''zahay:
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10731, '73. Indro Zazakely', NULL, NULL, '
{1.
Indro Zazakely,
Zanak''i Maria,
Tompon''ny anjely,
Nefa nankaty,
Io no Zazalahy,
Zanaky ny Ray,
Tonga mba hitahy:
Izy no anay.}

{FIV.
Mihirà, ry tany,
Sy ny mponina eo!
Hafalian-tsy lany,
Manandrata feo!
Jeso no deraina,
Fa Mpanjaka soa;
He! Ny fanjakany
Lehibe tokoa.}

{2.
Zaza nantenaina
Fony ela be,
Sady Tompon''aina
Manan-kery be,
Mihirà, ry tany!
Eny, mifalia;
Hoy ny mpaminany,
Izy no Mesia:
[FIV.]}

{3.
Jeso Zaza hendry,
Masina sy to,
Tompo voatendry,
Mpanadio ny fo,
Aoka mba hijery
''Zao tontolo izao,
Zaza manan-kery,
Indro, mba hajao:
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10741, '74. Tany Betlehema fahiny', NULL, NULL, '
{1.
Tany Betlehema fahiny
Tao an-tranon''omby re;
Nisy Zazakely soa,
Tampofoan-dreniny,
Jeso Kristy Zaza io,
Zanak''i Maria madio.}

{2.
Nahafoy ny Rainy Izy
Sy nandao ny lanitra,
Tsy nandà ny trano ratsy,
Nefa Andriamanitra:
Toe-pahantrana re
an''ny Tompo lehibe.}

{3.
Fony Izy mbola Zaza
Dia nanaiky sy nanoa,
ary tia reny Izy,
Sy nanetry tena koa,
Zaza Kristiana e!
Manaraha Azy re!}

{4.
Ka ho hitantsika Izy
Noho ny fitiavany:
Fa ''Lay Zazakely tsara
No mba Tompon-danitra,
Ka isika zanany
Dia hankany aminy.}

{5.
Nefa tsy mba tranon''omby
Itoerany izao,
Fa nankao an-danitra Izy
Sy mitafy laza ao;
Ka ho sambatra any koa
Tahaka Azy ny zaza soa.}
', '#1-II-2', '#1') /end

insert into song values (10751, '75. Andro malaza', NULL, NULL, '
{1.
Andro malaza sady soa tokoa
''Lay andro ''zay nisehoan''i Jeso;
''Zay nantenain''ny mpaminany koa,
Mba hampiseho fihavanana,}

{FIV.
Mifalia sy mihobia mafy,
Efa tonga ny Mpanavotrao
Avia, ka miderà
Dera io Zaza io,
:,: Hobio, hobio, Fa Tompontsika io :,:}

{2.
Anjely maro no nihoby tao;
Mpiandry ondry nitsaoka Azy koa,
Ny olon-kendry koa nanasina Azy,
Andeha mba hankalaza Azy izao:
[FIV.]}

{3.
Dera sy haja no atolotray
Anao, ry Jeso ''zay teraka anaio!
Ka mba mandraisa ny fanatitray
Ho zava-mani-pofona tokoa:
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10761, '76. Miderà anio, ry mino', NULL, NULL, '
{1.
Miderà anio, ry mino,
Ny Mpanavotra anareo!
Manandrata feo hidera
An''i Kristy, Tompo soa,
Zanaky ny vehivavy,
Nefa Andriananahary,
Teraka ho olona
Mba hanavotra anareo:
Teraka any Betlehema
''Lay Mpanavotra anareo}

{2.
Taitra ny mpiandry ondry,
ary koa nivadi-po,
Tamin''ny anjely maro
Tonga tany aminy.
Fa isika tsy ho taitra,
Fa ho faly sy ho ravo,
Sitraky ny Tompo soa
Nidina ka nankaty:
Teraka any Betlehema
Jeso Kristy, Tompo soa.}

{3.
Aza taitra, matokia,
Manatona, mihainoa,
Mazotoa, ka avia,
Maneke ny Tompo soa.
Arahaba, Tomponay o!
Tompom-pivavahanay,
Teraka ho Tomponay;
Tompon''ny fiainanay;
Arahaba, Tomponay!
Ry Mpanavo-dehibe!}
', '#1-II-2', '#1') /end

insert into song values (10771, '77. Tonga indray re ny Noely', NULL, NULL, '
{1.
Tonga indray re ny Noely,
Andro tasra sady soa,
Jeso Tompon''ny Anjely,
Nefa nietry tena koa,
Indro tonga Zazakely
Ho sakaizanay tokoa,
Sy hamory ''zay miely
Hody amin-dRainy koa.}

{2.
He! Ny fonay ravoravo,
Ry Noely maminay,
Fa ny Zanaky ny Avo
No nomeny ho anay.
Mba hanarina ny lavo
Sady ho fiainanay;
Feno sy mitafotafo
''Zany hasambaranay.}

{3.
O! ry androm-pamonjena,
Arahaba, hianao!
Olo-maro sesehena
No mifaly aminao.
Avy koa mba hitsena
''Lay Mpamonjy soa ''zahay
Ka ny fonay no homena
An''i Kristy Havanay.}
', '#1-II-2', '#1') /end

insert into song values (10781, '78. Be ny voninahitrao', NULL, NULL, '
{1.
Be ny voninahitrao
Jeso Kristy Tompo!
Lanitra nonenano,
Maro no hanompo.
Be ny mahasambatra,
Faly re ny fonao;
Nanam-boninahitra
Tao an-dapan-dRainao.}

{2.
Fa ny voninahitra
Efa nialanao;
Ny haren''ny lanitra
Tsy mba nojerenao.
Teraka ho olona
Ianao nahery,
Ka avy mba hanavotra
Ny mpanota very.}

{3.
Eny, tonga Ianao
Honina amin-tany,
Hahazoan''ny olonao
soa tsy ho lany.
Sarotra ny lalanao,
Be ny zava-mafy,
Loza no nanjo Anao.
Maro no nandrafy.}

{4.
Saotra no aterinay,
Vavaka aman-dera;
Ravo ny fanahinay,
Ka avy mba hihira,
Vitanao ny asanao
Namonjenao aina,
Ka indro ny Anaranao
Velona ao an-tsaina.}
', '#1-II-2', '#1') /end

insert into song values (10791, '79. Sambasamba Zanahary', NULL, NULL, '
{Sambasamba Zanahary
:,: Tamin''ny nampidinanao
Ny Zanakao malalanao
Mba hisolo ny helokay :,:}
', '#1-II-2', '#1') /end

insert into song values (10801, '80. He, ny hoby ao an-danitra', NULL, NULL, '
{1.
He, ny hoby ao an-danitra
Fa Mpamonjy re no teraka
Io no Zanak''Andriamanitra
''Lay tanteraka}

{FIV.
Mifaly (O! mifalia)
Mihobia (Sy mihobia)
Fa tonga ho anao ny Tompo tia
Ka mba hirao (Ka mba hirao)
sy lazao (sy koa lazao)
Ny famonjena azonao.}

{2.
Zava-mahagaga fatratra
Ny fitiavan''Andriamanitra:
Ny mpanota tafasaraka
Tonga sambatra!
[FIV.]}

{3.
O! ry tany izay asandratra
Ka vangian''ny Tompon-danitra
Inona ary no atolotra
Ny Mpanavotra
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10811, '81. Tao an-tsahan''i Betlehema', NULL, NULL, '
{1.
Tao an-tsahan''i Betlehema
Reo mpiandry ondry re,
Novangian''ny serafima
Sy anjely marobe.}

{FIV.
:,: Voninahitra ho an''
Andriamanitra :,:
Amin''ny avo.}

{2.
Njay ny hiran''ny anjely,
''Zay niredona tokoa;
Indro tonga Imanoela,
Zaza masina sy soa.
[FIV.]}

{3.
Nofo notafian''ny Teny,
Lanitra nilaozany;
''Ndao isika izao hankany,
Be tokoa ny lazany.
[FIV.]}

{4.
Voninahitra ao ambony
Ho an''Andriamanitra;
Ka ny hira no redony
Solon-java-manitra.
[FIV.]}
', '#1-II-2', '#1') /end

insert into song values (10821, '82. He, Zaza mahasambatra', NULL, NULL, '
{He, Zaza mahasambatra
No omena ho antsika;
Virjina no niteraka
Ilay Andriantsika,
''zay Zanak''Andriamanitra
Nandao ny voninahitra
Hanavotra ny very;
ka saotra no aterinay,
Ry Jeso Kristy Tomponay,
Mpamonjy manan-kery!}
', '#1-II-2', '#1') /end

insert into song values (10831, '83. Ry Jeso o! Ianao no mahavonjy', NULL, NULL, '
{1.
Ry Jeso o! Ianao no mahavonjy
Izay rehetra hiankina aminao;
Tonga taty hitady sy hamonjy
Izay mpanota very Ianao.}

{FIV.
He! Ry mpanota very aty!
:,: O! mankanesa ho any aminy. :,:}

{2.
Na dia Zana-Janahary aza,
Na dia Tompo aza Ianao,
Tsy mba nitandro hery aman-daza
Fa tonga olona ory Ianao.
[FIV.]}

{3.
Tamin''ny dianao nanaovan-tsoa,
Tsy nahalala sasatra Ianao;
Fa Ianao tsy nanana tokoa
''Zay nametrahanao ny lohanao.
[FIV.]}

{4.
Dia nolatsaina sy nalam-baraka
Sy novonoin''olon-dozabe,
Ka dia maty Ianao Mpanjaka
Ho fanavotana olo-marobe.
[FIV.]}

{5.
Ny fasanao nambenan''ny mahery,
Nefa tsy ela tao ny Tomponay
Velona indray hamonjy olom-bery,
Velon-kanao fifonan-ko anay.
[FIV.]}
', '#1-II-3', '#1') /end

insert into song values (10841, '84. He! Be ny hasoavana', NULL, NULL, '
{1.
He! Be ny hasoavana.
Fa tonga ny ftoana,
Ka Kristy Tompo teraka
Hamonjy sy hanavotra.}

{2.
Ny fahotan''ny olona
Dia lova tamin-drazana;
Isika trany avokoa,
Ka very re ny aina soa.}

{3.
Inty ny Zaza masina e,
Izay neken''ny Rainy hoe:
"Ny Zanako Malalako,
''Zay mamiko sy sitrako."}

{4.
Ny dera eran-danitra
Omeo an''Andriamanitra,
''Zay nahafoy ny Zanany
Hanavotra ny olony.}
', '#1-II-3', '#1') /end

insert into song values (10851, '85. Avelao isika', NULL, NULL, '
{1.
Avelao isika
Izay te ho tia
Ny Mpanavotra antsika,
Hanaraka Azy.}

{2.
Aoka ho fantantsika
Ny fitiavany
Sy ny aton''ny asany
Hiderantsika Azy.}

{3.
Be ny fiantrany,
Nijery ny ory,
Tratran-dRainy nialany
Ho teraka olona.}

{4.
Be ny fiantrany,
Nijaly ka maty
Hamonjeny ny  mpanota,
Ka Izy hisaorana.}
', '#1-II-3', '#1') /end

insert into song values (10861, '86. Jereo ny Mpamonjy', NULL, NULL, '
{1.
Jereo ny Mpamonjy,
Izay miakatra
Ho ao Jerosalema
Hanao ny avotra;
Na dia b ny ratsy
Mikendry Azy ao,
Ny mba hamonjy azy
No tiany hatao.}

{2.
Tsy biby manan-daza
No nitaingenany,
Tsy ny hitady laza
No nihaviany;
Kanefa manan-kery
Handrsy ho anao;
Ny ratrany irery
No aina ho anao.}

{3.
''lay voasatro-tsilo
Noho ny helokao
Dia mety sy maniry
Mba ho Mpanjakanao.
Ny rany sarobidy
Omeny ho anao;
Ny lanitra nihidy
Misokatra aminao.}

{4.
Ampio mba ho haiko
Hatolotra Anao,
Jesosy o, ny aiko
Ho voninahitrao;
Velariko ny foko
Eo anoloanao,
Tsy misy tsy ho foiko
Mba hodiovinao.}

{5.
Hosana! Ry Mpamonjy,
Hiraiko ho Anao,
Fa mamin''ny tra-bonjy
Ny fiderana Anao
Hosana! Tompo tsara!
Soava dia izao!
Handresy ny devoly
No anton-dianao.}
', '#1-II-3', '#1') /end

insert into song values (10871, '87. Nitomany ny Mpamonjy', NULL, NULL, '
{1.
Nitomany ny Mpamonjy,
Fa tsy nety ho trabonjy
Israely olony.}

{2.
Izy dia nanambara
Taminy ny teny tsara
Feno fitiavana.}

{3.
Ka ny jamba, kilemaina,
Moana sy very saina
Dia nositraniny.}

{4.
He, ny soa efa vita,
Nefa nody tsy nahita
Ny ankamaroany.}

{5.
Ary lotika ny fony,
Koa halany ny Ony
Feno rano velon.}

{6.
Ry Mpamonjiko malala!
Aoka aho hahalala
Ny fitomanianao.}

{7.
Ary hahalala koa
Fa ny soa mahavoa
Raha tsy horaisina.}
', '#1-II-3', '#1') /end

insert into song values (10881, '88. ''Lay ora tao Getsemane', NULL, NULL, '
{1.
''Lay ora tao Getsemane
Tsy mba hadino re!
Raha Ianao no tsembodrà,
Ry Tompon''avotra}

{FIV.
:,: Tsahiviko :,:
Ny fahorianao,
Raha tsinjoko ny adibe,
''lay tao Getsemane!}

{2.
Nafoin-dRay irery tao,
Nilaozan-kavanao
Handraisana anay indray
Ho zanaky ny Ray.
[FIV.]}

{3.
Nandroso mba hijaly tao,
Ry Tompo, Ianao,
Fadiranovana sy may
Hanavotra anay.
[FIV.]}

{4.
Fa vinitra Jehovah Ray
Noho ny ditranay;
Ninianay nolavina
Ny didy masina.
[FIV.]}

{5.
Ka Ianao nasiana
Ho solo voina;
Ny dinitrao, ry Tomponay,
Dia valin''ny otanay.
[FIV.]}

{6.
Fa sitrakao ho afaka
Ny olo-meloka;
Endrey ny fitiavanao
Anay navotanao!
[FIV.]}

{7.
Raha mila hangatsia-po
Izaho, Tompo o,
Mba ampisaino ahy re
Ny tao Getsemane
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (10891, '89. He mijaly ny Mpamonjy', NULL, NULL, '
{1.
He mijaly ny Mpamonjy
Ao Getsemane izao.
O! ry te ho voavonjy,
Manaraha Azy ao!
indro, ny Mpanavotrao,
Fa miady ho anao.}

{2.
Ory ao ny Tompon''aina,
Mafy re ny adiny,
Ka misento mitaraina,
Toy ny ra ny dininy.
Ry mpanota, mijere,
Mihevera ka hendre!}

{3.
Indro tonga ny anjely
Mba hampahatanjaka
An''i Jeso, ''zay mijaly
Amim-panaintainana.
Ry Mpamonjy tia o,
Mamindrà fo amiko!}
', '#1-II-4', '#1') /end

insert into song values (10901, '90. Tsy haiko ny milaza re', NULL, NULL, '
{1.
Tsy haiko ny milaza re,
''Lay fahorianao!
Irery tao Getsemane,
Ry Tompo masina o!
Tarihinao ny saiko e!
Ho ao Getsemane,
Hahita ny fitiavanao
Izao tontolo ''zao.}

{FIV.
Fitiavana ahy re,
Ry Tompo masina o!
No niaretanao
Izao mangidy izao!}

{2.
Nangorohoro mafy re
Ny fonao, Jeso tia!
Ny tsemboka ''zay nitete,
Nanjary toy ny ra!
Endrey ny fahorianao
Nangotraka tokoa!
Tsy maintsy nosotroinao
Ilay kapoaka tao!
[FIV.]}

{3.
Ho ahy re, ho ahy re,
No niaretanao
Irery tao Getsemane,
Jeso malala o!
Sitoninao ny foko e
Ho ao Getsemane,
Hahita izay niaretanao,
Ho tena tia Anao;
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (10911, '91. O! fitiavan-dalina', NULL, NULL, '
{1.
O! fitiavan-dalina
Nasehonao ilay alina
''zay namadihana Anao,
Ry Jeso, fototr''aim-baovao.}

{2.
Mangoraka ery ny fo,
Sy ory ny fanahiko,
Raha mba tsaroako izao
Ny loza izay nanjo Anao.}

{3.
Fadiranovana Ianao,
Nangotraka ny adinao;
Ny dinitrao nanjary ra
Noho ny fahoriana.}

{4.
Izany niaretanao
Noho ny fitiavanao,
Ka nentinao ny heloko
''zay tokony ho nentiko.}

{5.
Namita fanekem-baovao
Ho famonjena Ianao,
Nasianao kase izay
Maharitra mandrakizay}

{6.
Misaotra Anao, ry Tompo o!
Fa ravo ny fanahiko
Handray ny fanomezanao
Eo amin''ny latabatrao.}

{7.
Ny teninao inoako,
Ka raisiko ho ao am-po:
Ny zava-tsoa atolotrao
Dia ranao sy ny tenanao.}

{8.
Ry Jeso, amboarinao
Ny fonay ho fonenanao,
Ka Ianao ho tianay
Izao sy ho mandrakizay.}
', '#1-II-4', '#1') /end

insert into song values (10921, '92. Ry voasatro-tsilo', NULL, NULL, '
{1.
Ry voasatro-tsilo,
Ry voakapoka!
Indrisy be ny loaza
Manafotra Anao!
Nitsaohan''ny anjely
Fahiny Ianao,
Fa henatra sy latsa
Anjaranao izao.}

{2.
Atolotro, ry Jeso
Ny foko ho Anao,
Hiaraka hitondra
Ny fahorianao.
Raha henatra sy loza
No ho anjarako,
Dia Ianao, Mpamonjy,
No ifaliako.}

{3.
Tsarovy aho, Tompo,
Raha tratry ny manjo!
Tsy mendrika aho nefa
Mba amindranao fo!
Ny ratranao no misy
Fanasitranana,
Ny ranao ahitana
Ny fiadanana.}
', '#1-II-4', '#1') /end

insert into song values (10931, '93. Nampalahelo mafy re', NULL, NULL, '
{1.
Nampalahelo mafy re
Ny fahorian-dehibe,
''Zay niaretanao.
Nalaim-baraka Ianao,
Niaritra ka tsy nandao
Lay asa masinao.}

{2.
Ny henatra aman-tahotra
Mangeja anay voavotra,
Fa diso izahay.
Avia, Tompo, mamonje,
Meteza izao meneke
Hanavotra indray.}

{3.
Ampio ny olona, Jeso,
Hahay handray Anao ampo
Fa mila vonjy izao.
Monena hanatanjaka
Ny ory sy ny reraka,
Izay manantena Anao.}
', '#1-II-4', '#1') /end

insert into song values (10941, '94. Gologota no jereo', NULL, NULL, '
{1.
Gologota no jereo:
Ao ny hazo fijaliana;
Indro re, mihantona eo
Jeso, sady harabina,
Moa mba fantatrao, ry fo,
Ny fitiavan''i Jesosy?
''Zay nandefitra tokoa,
Ary koa ninia ho resy.}

{2.
Famonjena ho anao
No netezany hihafy,
Ka hijaly toy izao
Sy hiaritra ny mafy.
He, ny rany nitete
Vokatry  ny fanomboana;
Vetivety foana re
Dia ho faty ao ny Tompo.}

{3.
Mitsangàna, miaingà,
Ndeha mankany Gologota:
Ao no latsaka ny ra
Hamonjena ny mpanota
Io no hany mahadio,
Antoka tsy hay ravàna;
Eny, samy afak''io
''Zay rehetra fahotana.}
', '#1-II-4', '#1') /end

insert into song values (10951, '95. Ny hazo nijalianao', NULL, NULL, '
{1.
Ny hazo nijalianao,
Ry Jeso Tomponay;
Maneho ny fitianao,
Dia iankinanay (in-3)
Maneho ny fitianao.}

{2.
Mitazana ny masonay,
Mangoraka ny fo;
Na meloka aza izahay,
Ny teninao ho to. (in-3)
Na meloka aza izahay.}

{3.
Ny avotr''aina vitanao
No itokianay;
Ny entanay voaesotrao,
Ka afaka izahay. (in-3)
Ny entanay voaesotrao.}

{4.
Ny hazo nihomboanao
Dia voninahitray;
Ny nanalam-baraka Anao
No isandratanay! (in-3)
Ny nanalam-baraka Anao}
', '#1-II-4', '#1') /end

insert into song values (10961, '96. Ilay havoana', NULL, NULL, '
{1.
Ilay havoana
Natao hoe: Kalvary
No tany namonoana
Ny Tompo Jesosy;}

{FIV.
O! tsarovy re!
Maty ory tao
Jeso Tompo lehibe
Hanavotra anao.}

{2.
Jereo ny Tomponao,
Tazanonao iry,
Ka andrandrao ny masonao,
Fa io no Kalvary:
[FIV.]}

{3.
hevero Kalvary,
Fa tao ny Tomponao;
Tsarovinao ny nentiny
Mba hamonjena anao:
[FIV.]}

{4.
Tsinjovy Kalvary.
Fa Jeso Tomponao
Nijaly tany raha tety.
Ka maty Izy tao.
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (10971, '97. ''Lay fonao tia tao Gologota', NULL, NULL, '
{1.
''Lay fonao tia tao Gologota
No velona ao an-tsaiko izao,
Ny fonao ''zay mitady mpanota
No varatra indrindra tao.
:,: Ry fo malala, nihafy loatra,
Lavitra ny mpangoraka Anao :,:}

{2.
Indro! Ny lefona koa mamely,
Ka notrobarana Ianao!
He! Malahelo ireo anjely,
Fa naharary re ny fonao!
:,: Io no fitia tsy misy toa azy,
Zaran''izao rehetra izao. :,:}

{3.
Efa hisoka ny atoandro,
Menatra izao tontolo izao;
Tsy ela intsony dia maizina andro
Kalvary ''zay nihantonanao!
:,: Fa izaho tsy mba
hangataka andro
Hampiantrano ny tenanao. :,:}

{4.
Tsy sasatra na valaka koa,
Ny fona tia, ry Tompo o;
Menatra izao ity foko tsy soa
Tsy hahavaly tokoa Anao;
:,: Nefa ampio aho hanaiky,
Hanatontosa ''zay sitrakao. :,:}
', '#1-II-4', '#1') /end

insert into song values (10981, '98. Indro ny Tompo Jesosy', NULL, NULL, '
{1.
Indro ny Tompo Jesosy,
Jereo,
Tena nihafy tokoa!
He! Fa nivesatra eso, jereo,
Mba hamonjena anao;}

{FIV.
:,: Indro Jeso, jereo :,:
Izy izao dia mihevitra anao,
Indro Jeso, jereo.}

{2.
Indro ny Tompo Jesosy,
Jereo,
Efa voahombo tokoa!
He! Fa nijaly tsy meloka teo,
Mba hamonjena anao;
[FIV.]}

{3.
Indro ny lefo-mandoza, Jereo,
Nenti-namely ny Tompo!
He! Fa ny rany nirotsaka teo,
Mba hamonjena anao;
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (10991, '99. Ry fitiavan''i Jesosy', NULL, NULL, '
{1.
Ry fitiavan''i Jesosy
''zay voahombo ho anay,
Ony be sy mahavonjy,
Aoka ho lasanao ''zahay.}

{FIV.
Tompo, Zanakondry tsara
Ampy re ilay ranao,
:,: Tsy helohin''ny Mpitsara,
''zay mpanota afakao. :,:}

{2.
Fa misento ny fanahy
''Zay ety an-tany ''zao;
Malahelo sy manahy,
Sao voan''izay natao:
[FIV.]}

{3.
Ny mpanota dia ho very,
Raha tsy mba miova izao;
Fa hahazo tena hery,
Raha mino tsara Anao:
[FIV.]}

{4.
Enga anie ka mba hiely
Ho an''izao tontolo izao
Ilehy ony mahafaly
Manadio sy manavao.
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (11001, '100. Moa mba efa nisy va', NULL, NULL, '
{1.
Moa mba efa nisy va,
Fahoriana toy izao?
Jeso ''Lay Mpanisy soa,
Tompo sy Mpanjaka koa,
Entina tsy meloka,
entina any Kalvary.}

{2.
He, ny lanivoany
Voatsindron-defona!
Tongotra am-tanany
Voafantsika avokoa.
Eny, fa ny ainy koa,
Foiny tany Kalvary.}

{3.
Mafy ''zay, ry Jeso o!
Mafy nefa zakanao;
Ny fitiavanao anay!
No niaretanao izay!
Menatra aho, Jeso o,
Raha hadino kalvary.}

{4.
Taomy aho, jeso o!
Mba ho any kalvary;
ka ny foko tsy madio
Mba diovinao anio,
Ento aho jeso o!
Ento any Kalvary.}
', '#1-II-4', '#1') /end

insert into song values (11011, '101. Ny hazo fijaliana', NULL, NULL, '
{1.
Ny hazo fijaliana,
Ry Jeso Tomponay
Dia tena fitoriana
Fa tianao ''zahay.}

{2.
Ny hazo fijaliana
Dia manambara izao
Fa be ny fahoriana
Nihatra taminao.}

{3.
Ny hazo fijaliana
No mampiseho koa
Fa tokony hariana
Ny ota mahavoa.}

{4.
Isaoranay dia isaoranay
Ianao ry Tomponay,
Fa be ny fitiavana
Nataonao taminay.}
', '#1-II-4', '#1') /end

insert into song values (11021, '102. Reharehako ny hazo', NULL, NULL, '
{1.
Reharehako ny hazo
Nijalian''i Kristy;
Manda fiarovana avo
Ialofako aty.}

{2.
Azon-doza maro aho,
Tery koa ny lalana,
Tsy ilaozan''i Jesosy
Mpanome fiadanana.}

{3.
Raha lany re ny andro
Mampiadana indray.
Tsy hadinoko ny hazo
Netin''ny Mpamonjinay.}

{4.
Ka anjara aty an-tany
Tsy mba mahavesatra
Noho ny hazo fijalianao,
Hazo mahasambatra. Amen.}
', '#1-II-4', '#1') /end

insert into song values (11031, '103. Ny hazo fijaliana', NULL, NULL, '
{1.
Ny hazo fijaliana
Tsy maintsy hotsarovana!
Henoy ry fanahiko
Ny tenin''ny Mpamonjiko.}

(Vakiteny 1)

{2..
Ny teny voalohany
Mihevitra ny rafiny:
"Mba mamelà ny helony,
Fa jamba ny fanahiny!"}

{3.
Izaho koa, Jeso o!
Dia jamba sady ratsy fo;
Ka mba jereo sy iantrao,
Fa te-ho tonga olona.}

(Vakiteny 2)

{4.
Ilay meloka tanilany
Naniry mba horaisiny,
Nomeny toky tsara io:
"Ho any Paradisa anio"}

{5.
Raha avy ny anjarako,
Ka tapitra ny androko,
Mba raiso aho, Tompo o,
Ho any paradisanao!}

(Vakiteny 3)

{6.
Dia hitany ny reniny
Tomany teo akaikiny,
Toy ny tsindron-defona
Ka feno fisentoana.}

{7.
Dia Jaona no natolony
Ho zanany ho solony
"Mba raisonao, ry Jaona o
Ho reninao ny reniko!"}

{8.
Tsarovy aho, Tompo o!
Raha ao am-pahoriako,
Iraho ny mpianatrao
Hanampy ny mpanomponao!}

(Vakiteny 4)

{9.
Ny teniny fahaefatra
No tena mampangovitra;
Niantso izy, hoe : "Ry Ray,
Nahoana Aho no afoy?"}

{10.
Izao no dia nandaozana
Anao tam-pahoriana:
Hahafaka ny tahotro,
raha togna ny fotoako

(Vakiteny 5)

{11.
Ny otako izay netiny
Nahory ny fanahiny,
Ka nitarainany hoe:
"Mangetaheta Aho re!"}

{12.
Mangidy, ry Jesosy o,
Ny loza niaretanao,
Ka aoka mba ho mamiko
Ny avotra, izay vitanao!}

(Vakiteny 6)

{13.
Hoy izy indray : "Tanteraka!"
He! Teny mahasambatra!
Tanteraka ny asany,
Voavotra ny olony.}

(Vakiteny 7)

{14.
Ny teny farany indray
Nilaza ny rahatrizay:
"Atolotro ny aiko izao,
Ry Ray, ho eo an-tanananao!"}

{15.
Izany dia maiko
Hiorina ao anatiko,
Ka teny hotononiko
Eo amim-pahafatesako!}
', '#1-II-4', '#1') /end

insert into song values (11041, '104. Raha mijery ny hazo', NULL, NULL, '
{1.
Raha mijery ny hazo
Nijalian''i Jesosy
Tsy misy izay milaza
Na izay mirehareha.}

{2.
Aza miavonavona,
Fa indro ny Mpanavotra;
Ny dian''ny Jesosy Kristy
No arahina isan''andro.}

{3.
Indro ny rany mandeha,
Voahombo ny tanany
Nijaly, ka dia maty
Nisolo olo-meloka}

{4.
Raha ahy ''zao tany izao
Sy ny aminy rehetra,
Tsy ho azoko havaly
Ny fitiavan''i Jeso.}

{5.
Ny foko homeko Azy,
Fanahiko atolotro;
Tsy mba mendrika Azy ''zany,
Fa Jeso no mety mandray.}
', '#1-II-4', '#1') /end

insert into song values (11051, '105. Tsinjovy ilay havoana', NULL, NULL, '
{1.
Tsinjovy ilay havoana,
Ary Jerosalema,
Fa io no Gologota
Mba taidio, ange!
Fa tao nivesatra ota
Ny Tompo masina;
Nijaly sy nolavina
Ka novonoina.}

{2.
Tsy takatra heverina
Ny entan-dehibe,
''zay nentin''ny Mpisolo
Ka diniho, re!
Fa nentiny daholo
Ny heloka natao:
Fa tonga Izy hanavao
Izao tontolo izao.}

{3.
Ny helotsika koa izao
Dia nivesarany,
Ka manaraha Azy,
Mba ho mpianany,
Raha tao Gologota Izy,
Ny rany nitete,
Ka vita ho antsika re,
Ny vonjy lehibe.}
', '#1-II-4', '#1') /end

insert into song values (11061, '106. Indro, ny Fitiavana nijaly', NULL, NULL, '
{1.
Indro, ny Fitiavana nijaly
Teo amin''ny hazo fijaliana;
Ary ny ratsy nivonona hifaly
Handresy ny Zanakalahy Tokana.}

{2.
Indro, Ny Fitiavana mahery
Nitondra ny heloky ny olony;
Izy niady hanafaka ny very,
Ny ainy no avotra ka natolony.}

{3.
Izy nijaly mafy tao Gologota,
Voahombo ny tongotra aman-tanany;
Izy nisolo antsika ''zay mpanota,
Ka fadiranovana ny fanahiny.}

{4.
O, ry Mpamonjy, mba hazony aho
Hijery ny Hazo fijalianao!
Olona meloka aho ka afaho
Hialoka amin''ny fitiavanao.}
', '#1-II-4', '#1') /end

insert into song values (11071, '107. Maniry mafy aho', NULL, NULL, '
{1.
Maniry mafy aho, ry Jeso Tompo o,
Hialoka eo ambany hazo fijalianao.
Tsy misy toeran-kafa re
''zay tena mamiko;
Ny hazo fijalianao no fiarovako.}

{2.
Ny Hazo-fijalianao no itazanako
Ny tenanao izay nijaly mba ho avotro;
Ka dia kotsan-dranomasom-pitiavana
Ny tavako, ry Tompo o! Fa afa-keloka.}

{3.
Am-pototry ny hazo izay nijalianao
No itoerako hohazavain''ny tavanao,
Ataoko fatiantoka ny zava-mora lo,
Ny hazo-fijalianao no tombo-dahiko.}
', '#1-II-4', '#1') /end

insert into song values (11081, '108. Ny hazo fijalianao', NULL, NULL, '
{1.
Ny hazo fijalianao,
Ry Jeso Tompo maminay.
Dia an''izao tontolo izao,
Ka famonjena ho anay.}

{2.
Mivelatra eo ka hitanay
Izany fitiavana;
Ka ampianaro izahay
Ho be fitia toa Anao.}

{3.
He! Latsaka eo ho avotray
Ny ranao masina sy soa
Ka mba sasao re izahay
Ho olonao madio tokoa.}

{4.
Voatolotra eo mba ho anay
Ny famonjena vitanao,
Ka mba tariho izahay
Handray  ny fahasoavanao.}

{5.
Mamiratra eo ka tazanay
Ny lanitra fonenanao;
Ka mba tantano izahay
Ho mendrika mponina ao.}

{6.
''Ndrey, tena mahafinaritra
Ny miara-monina aminao;
Ka ampio ''zahay hiaritra
Ny hazo-fijalianao.}
', '#1-II-4', '#1') /end

insert into song values (11091, '109. Nahafoy ny aiko Aho', NULL, NULL, '
{1.
Nahafoy ny aiko Aho
Hanavotako anao.
Ka nalatsako ny rako,
Nisoloako anao.
He! Ny aiko no nomeko
Ry mpanota very e!
Aiza izay afoinao re
Mba ho Ahy Tomponao?}

{2.
Lapan-dRaiko ao ambony,
Voninahi-dehibe,
No nilaozako hamonjy
Sy hanafa-doza anao,
''Zay voafitaky satana
Sy nozoin-doza be;
Aiza izay afoinao re
Mba ho Ahy Tomponao?}

{3.
Soa be sy mahagaga
No atolotro anao,
Famonjena tsy vidina,
Famelana ny otanao,
Fitiavan-tsy voafetra,
Lova ao an-danitra ao;
Aiza izay afoinao re
Mba ho Ahy Tomponao?}

{4.
Indro, Tompo! Fa omeko
Ho Anao ny tenako,
Ka tsy misy izay tsy foiko
''zay rehetra tianao;
Eso, latsa, tsy ahoako,
Fa ny ho mpanomponao
No ho zavatra kendreko,
Raiso re ny zanakao.}
', '#1-II-4', '#1') /end

insert into song values (11101, '110. Nomeko ho anao', NULL, NULL, '
{1.
Nomeko ho anao,
Ny rako masina,
Hanavotra anao,
Izay tsy marina!}

{FIV.
:,: Ka dia mba inona izao
No ahy taminao! :,:}

{2.
Nafoiko ho anao
Ny zava-tsoa be,
Mba hahatonanao
Ny Raiko lehibe:
[FIV.]}

{3.
Izaho Tomponao
Nijaly mafy re,
Hanafaka anao
Mpanota lehibe:
[FIV.]}

{4.
Ny vonjy lehibe
Nataoko ho anao,
Ny rako nitete
Hanadio anao;
[FIV.]}

{5.
Jesosy Tompo o!
Izao no teniko;
Atolotro anao
ny aiko sy ny fo;
:,: Izany no isaorako
Anao Mpamonjiko :,:}

{6.
Ny foko raisonao,
Oneno hatrety,
Ataovy lapanao
Na eto na ary:
:,: Dia tano aho, Tompo o!
Mba ho fanananao. :,:}
', '#1-II-4', '#1') /end

insert into song values (11111, '111. Indro ny Mpamonjy', NULL, NULL, '
{1.
Indro ny Mpamonjy,
Mpanavotra antsika,
Izy no manotnany hoe:
"Tianao va Aho?"}

{2.
Izaho Mpamonjy
Sy Mpisolo anao;
Ny rako manontany hoe:
"Tianao va Aho?"}

{3.
Ny fitiavako
Tsy mety miova,
Ka mbola manontay hoe:
"Tianao va Aho?"}

{4.
Ry Jeso Tompoko!
Malahelo ''zahay,
Ny fitiavanay anao
Mbola tsy toy inona;}

{5.
Aelezo ao am-po
Ny fitiavanao,
Hahazoanay hamaly:
"Tianay Ianao."}
', '#1-II-4', '#1') /end

insert into song values (11121, '112. Tsy ny hahazo zavatra', NULL, NULL, '
{1.
Tsy ny hahazo zavatra
Ary an-danitra
No itiavako Anao
Ry Andriamanitra}

{2.
Tsy noho ny atahorako
Ny fijaliana,
Fa noho ny fitiavanao,
No itiavako.}

{3.
Voahombo, voalefona,
Voalatsa Ianao:
Nahinjitra ny tànanao
Tambony hazo teo.}

{4.
Izaho no niaretanao
Ny fijaliana;
Ny ranao nanavotana ahy
''zay fahavalonao.}

{5.
Ka moa tsy mfitiavana
Havaliko Anao?
Ny foko no atolotro
Anao, ry Tompo o!}

{6.
Ry Jeso Kristy masina o!
Deraiko Ianao,
Fa Ianao Mpanjakako
Sy Andriamanitro!}
', '#1-II-4', '#1') /end

insert into song values (11131, '113. Jeso, Zanahary!', NULL, NULL, '
{1.
Jeso, Zanahary!
Masina sy to,
Nanome ny ainao
Noho ny olonao.}

{2.
Tsy niaro loza
Tamin-tenanao,
Fa mba tonga ory
Noho ny olonao.}

{3.
Nentinao ny ratsy,
Soa nolavinao,
Lapa nialanao,
Noho ny olonao.}

{4.
Jeso Tomponay o!
Tompo Masina,
Manomeza soa
Ny mpanomponao!}

{5.
Mamelà ny ota;
Manova ny fo;
Diovy ny maloto;
Aoko ho olonao.}

{6.
Tompo o, Tsitoha,
Vatolampinay!
Saotra aman-dera
Raiso ho Anao!}
', '#1-II-4', '#1') /end

insert into song values (11141, '114. Jeso no niala aina', NULL, NULL, '
{1.
Jeso no niala aina
Hanavotany anao;
:,: Iza re no tsy hanaiky
Azy izao? :,:}

{2.
Fa ny lapany ambony
Nialany fahizay,
:,: Hamonjeny olom-bery,
Very tokoa. :,:}

{3.
Fa izay manatona Azy
Ka mibebaka ao am-po
:,: Tsy hariany akory,
Ka matokia. :,:}

{4.
Mab avia, ry havanay o!
Maneke ny Tomponao,
:,: Maneke ny Tompon''aina
Ho Tomponao. :,:}

{5.
Iantrao, ry Tomponay o!
Ny mialoka aminao;
:,: Raisonao ''zahay mpanota
Ho olonao. :,:}
', '#1-II-4', '#1') /end

insert into song values (11151, '115. Jeso Tompo lehibe', NULL, NULL, '
{1.
Jeso Tompo lehibe,
Iraky ny Rainy,
Nolatsain''ny marobe,
Tsy mba nohajainy:}

{FIV.
Jeso tia, Jeso tia,
Maty voahombo;
Maty tany Kalvary,
Maty re ny Tompo.}

{2.
Ory loatra fahizay,
Zanak''ondry mora;
Maty mba ho solonay,
Sady voakora:
[FIV.]}

{3.
Nalahelo, be nanjo,
Nefa tsy nanota,
Fa nijaly sitrapo
Tany Gologota:
[FIV.]}

{4.
Jeso no Mpamonjy tia,
Nahafoy ny ainy
Mba hamory ''zay mania
Hody amin-dRainy:
[FIV.]}
', '#1-II-4', '#1') /end

insert into song values (11161, '116. Miaingà, ry fo sy saina', NULL, NULL, '
{1.
Miaingà, ry fo sy saina,
Mba ho any Golgota,
Maty tao ny Tompon''aina,
Vita tao ny avotra,
Ry mahantra, ry mania,
Sady raiki-tahotra,
Ny Mpamonjy be fitia
Hahitanao avotra.}

{2.
Fa ny ainao, ry Mpamonjy
Nisoloanao anay
Hahazoanay ny vonjy
Sy hahafa-trosa anay.
Vatofantsiky ny aiko
Ianao, Mpamonjy o!
Ravoravo izao ny saiko,
Afakao ny heloko.}

{3.
Ny manjo aty an-tany
Tsy mba hatahorako;
Fa izaho dia ho any
Amin''ny Mpanavotro.
Tombon-dahiko tokoa
Ny fahafatesana,
Hahatrarako ny soa,
Dia ny fiainana.}
', '#1-II-4', '#1') /end

insert into song values (11171, '117. O, ry Zanak''ondry!', NULL, NULL, '
{O, ry Zanak''ondry!
He Ianao nijaly,
Ory voahombo,
Anefa tsy namaly;
Ny helokay nalainao,
Ka maty re ny ainao.}

{1. - 2.
Mba iantrao ''zahay, ry Jeso!}

{3.
Ampiadanonao ''zahay, re!
}
', '#1-II-4', '#1') /end

insert into song values (11181, '118. Mba jereo ny Zanakondry', NULL, NULL, '
{1.
Mba jereo ny Zanakondry
Voahombo sady ory,
Nefa Tompo masina.
He! Ny Rainy tsy namaly,
Raha ny Zanany nijaly
Tamim-panaintainana.}

{2.
He, ny Marina nijaly!
Mba hahaizako mifaly,
''zay no niaretany,
O! ry foko mangovita,
Manatona mba hahita
Ny fahafatesany.}

{3.
Eny, ry Mpamonjy tsara,
Mba meteza hanambara
Ny harem-pitiavanao;
Dia hafanao ny foko,
Mba hahaizako hanompo
Sy hanaraka Anao!}

{4.
Aoka aho hitomany
Raha mihevitra izany
Fahoriana nentinao,
Azoko ny famonjena,
O! ry foko, manenena
Noho ny ratsy vitanao.}

{5.
Tompo, manampia ahy
Mba harisika am-panahy,
Hanompoako Anao,
Raha mbola aty an-tany
Aza afoy fa mba tohany
Tsy hisaraka aminao.}
', '#1-II-4', '#1') /end

insert into song values (11191, '119. Ianao ry Tompon''aiko', NULL, NULL, '
{1.
Ianao ry Tompon''aiko,
Maty noho ny otako;
Tsy mba takatry ny saiko,
''zany fijalianao,
Nefa sitrakao nijaly,
Hahazoako mifaly;
Ry Jesosy, Ianao
No isaorako izao.}

{2.
Safotry ny fandatsana
Ianao, Mpanavotra,
handovako fanjakana
feno voninahitra,
Voaratra, voaeso,
Ary voasatro-tsilo
Noho ny hadisoako,
Kao dia isaorako.}

{3.
Tianao ''zahay mpanota
Helokay no nentinao;
Resinao ny tambin''ota,
Izahay nafahanao;
Voaloanao ny trosa,
Izahay nafahanao;
Voaloanao ny trosa,
Izahay mpanota kosa
Tonga olom-potsinao,
Ka derainay Ianao.}
', '#1-II-4', '#1') /end

insert into song values (11201, '120. Ry mino o, avia re!', NULL, NULL, '
{1.
Ry mino o, avia re!
Hiara-mankalaza
''lay Mpanjaka lehibe,
Fa tena mahagaga
Sy sarobidy anie
Ny fahasoavan-dehibe
Nomeny ho antsika.}

{2.
Tsy asa soa vitako
No toiko anefa,
Fa ny Andriamanitro
No tsy nisalasala
Hanafaka ny otako,
Nomeny mba ho avotro
Ny Zanany malala.}

{3.
Jesosy faly eram-po
Hamonjy ahy very,
Ka azoko ny indrafo,
''zay vitan''izy irery;
Izaho tena afa-po,
Tsy misy atahorako,
Izaho tsy ho very.}

{4.
Jesosy Kristy Tompo o!
Izaho ta-hamaly
Ny teny fiantsoanao:
"Mba manatona Ahy!
Izaho dia ho anao,
Ho Ahy kosa hianao."
Ka manampia ahy.}

{5.
Ny ranao soa nitete
No nanadio ahy
Ny ainao sarobidy re
No nanavotana ahy.
Mba hitokiako anie
Ny famonjena lehibe
Natolotrao ho ahy.}

{6.
Isaorana eram-po anie
Ny Tompo Zanahary,
Fa Izy efa nanome
Ny Zanany nijaly,
Ka ny devoly lozabe,
Ny ota sy ny heloka
Ho resy sy ho faty.}
', '#1-II-4', '#1') /end

insert into song values (11211, '121. Ry Jesosy, ampombay', NULL, NULL, '
{1.
Ry Jesosy, ampombay
Fiadanana maharo
Fo sy saina izahay
Ka mba ampahafantaro.
Raha ny holatrao jerena,
Fa Anao ny fandresena}

{2.
Ry Mpandresy, ambarao
Aminay ny vokatr''ady;
Famonjena aim-baovao
No andramanay sahady;
Raha ny tafika miseho,
Fiadanam-po omeo!}
', '#1-II-4', '#1') /end

insert into song values (11221, '122. Hitako izao, sakaiza', NULL, NULL, '
{1.
Hitako izao, sakaiza,
Ny fahasambarana,
Ka ny foko faly, ravo,
Manam-piadanana,
Teo am-pototry ny hazo
Nijalian''ny Tompoko,
Teo aho no nahazo
Famelana heloka.}

{2.
He! Ny indrafo niriko,
Efa azoko tokoa,
Ka Jesosy no fidiko,
Fa Izy fototry ny soa
He! Ny rany sarobidy,
Nanadio ny otako:
''zaho efa voavidy,
Lanitra no lovako.}

{3.
Tena soa mahagaga
''zao nomenao ahy izao,
Fa izaho izay mpanota
Dia nohavaninao;
Ka izaho dia hidera
''zato fitiavanao,
Ary koa hanambara
Jeso o! Ny herinao.}

{4.
Ry Mpamonjy o! Mba raiso
''zao fisaorako izao;
Ka izay rehetra ahy
Dia atolotro Anao.
Raha tonga re ny fetra
Hialako sasatra,
Ento aho hitoetra
Eo anilanao ary !}
', '#1-II-4', '#1') /end

insert into song values (11231, '123. Velona Jesosy', NULL, NULL, '
{1.
Velona Jesosy,
Nitsangana tokoa;
Ka kristiana marobe
mihoby sy mihira hoe:
Dera ho Anao, ry Mpandresy!}

{2.
Velona Jesosy,
Ny helo dia resy,
Tanteraka ny avotra
Ka afaka ny heloka:
Dera ho Anao, ry Mpandresy!}

{3.
Velona Jesosy,
Ka velona isika,
Hiara-monina aminy,
Hohazavain''ny tavany:
Dera ho Anao, ry Mpandresy!}
', '#1-II-5', '#1') /end

insert into song values (11241, '124. Nitsangana ny Tompo', NULL, NULL, '
{Nitsangana ny Tompo,
Neken''ny Ray ny avotra,
ka ravo ny mpanompo,
Misokatra ny lanitra!
Tompon-pandresena,
Nandrava fasana,
Andriam-pamonjena,
Tolory hasina!
Jesosy no Mpanjaka
Amboninahitra,
Hiarahako mizaka
Ny zon''ny lanitra}
', '#1-II-5', '#1') /end

insert into song values (11251, '125. Indro, Jeso velona', NULL, NULL, '
{1.
Indro, Jeso velona,
Afaka ny olom-bery,
Rava re ny fasana,
Dia nivoaka ilay Mahery;
Ka ny horohoron-tany
Dia nahato izany.}

{2.
Resinao ny rafinay,
Ry Jesosy manan-kery,
Ka mpandresy izahay,
Voavonjinao ny very,
Matinao ny helok''ota,
Velona izahay mpanota.}
', '#1-II-5', '#1') /end

insert into song values (11261, '126. Endrey izao fahasambarana', NULL, NULL, '
{1.
Endrey izato fahasambarana:
Jesosy dia velona indray
Ny foko mandre fitsaharana,
Noho Izy natsangan''ny Ray
Tsy any am-pasana intsony,
Fa hitako velona koa;
:,: ''lay Tompon''ny hery ambony,
Mamiratra sy mananjo :,:}

{2.
He, velona Izy! Hafarany
Izaho izao hampandre
Ireo izay tia ny anarany
Ny fampiononana be.
Anjely no mendri-kilaza;
Izaho, mpanompo tsy soa,
:,: Nirahin''ny Tompo malaza,
Ka ravo sy tretrika koa! :,:}

{3.
Endrey, izato fahasambarana!
Fa velona mandrakizay
Ny Tompo ''zay nifankazarana,
Ka faly ny foko indray!
Ny fasana izay nampisaona
No fampiononana koa,
:,: Fa tao izahay tafahaona
Ka ravo dia ravo ny fo! :,:}
', '#1-II-5', '#1') /end

insert into song values (11271, '127. Tahaka ny masoandro miseho', NULL, NULL, '
{1.
Tahaka ny masoandro miseho,
Sady mamiratra tsara izao;
Toy  ny heriny izay mahaleo,
Na dia takon''ny rahona tao.
Tahaka izany Jesosy Mpamonjy
Izy nitsangana velona indray
Ry olombelona izay voavonjy
Mba miderà Azy mandrakizay}

{2.
Saotra aman-dera sy laza sy hery,
Aoka ho Azy hatramin''izao,
Izy nanafaka ny olom-bery,
Indro ny fasana vaky sy lao,
Izy nanafaka ny vavahady,
Aiza, ry fahafatesana o!
Aiza ny herinao sahy miady
Amin''ny mino sy amin''ny to?}

{3.
Jeso Mpamonjy o! Mba hasaovy
Ka mba arovy ny mino anao,
Ka ny fanahinao aoka ho avy
Mba hanazava ny fonay izao;
Ary tantao izahy tsy hania,
Fa Ianao no mpiaro anay!
O! ry Mpamonjy anay, mba avia;
Ka mba vonjeo, fa miandry izahay.}

{4.
Jeso, izahay te-hisaotra tokoa
Noho ny soa nomenao anay:
Teraka teto nitondra soa,
Dia ny fiainana mandrakizay.
Saotra omenay anao izay nijaly,
Nefa natsangan''ny Rainao indray,
Rahatrizay izahay dia hifaly
Any an-danitra mandrakizay!}
', '#1-II-5', '#1') /end

insert into song values (11281, '128. Efa velona ny Tompo', NULL, NULL, '
{1.
Efa velona ny Tompo
Fa nitsangana tokoa;
Resiny ny fahavalo,
''Lay mpihaza aina soa;
:,: Velona ny Tomponao,
Resiny ny rafinao :,:}

{2.
Matokia, ry mpanota,
An''i Jeso Avotrao,
Fa ny ota nahadiso
Afaka tokoa izao;
:,: Velona ny Tomponao,
Afany ny helokao :,:}

{3.
Eny fa nitsangana ela
Jeso Kristy Tomponao,
Ka namoha vavahady
Any mba hidiranao;
:,: Velona ny Tomponao,
Ka efa nialoha anao. :,:}
', '#1-II-5', '#1') /end

insert into song values (11291, '129. Avy naraina...', NULL, NULL, '
{1.
Avy naraina
:,: ''lay Avotr''aina, :,:
Natsangan-dRay,
Resy ny ota,
:,: ka ny mpanota, :,:
Mihoby indray,
Avy naraina sns.}

{2.
Hira hiraina
:,: Ny Tompon''aina :,:
Isaoranay;
Ory ny fony
:,: mba hampitony :,:
Ny sentonay.
Hira hiraina sns.}

{3.
Faly ny very,
:,: tsy manan-kery :,:
Ny helokay;
Jesosy Tompo
:,: dia mampanompo :,:
Ny rafinay.
Faly ny very sns.}

{4.
Hisy maraina
:,: ny Tompon''aina :,:
Ho avy indray;
Izy hizara
:,: Ny lova tsara :,:
Homena anay.
Hisy maraina sns.}
', '#1-II-5', '#1') /end

insert into song values (11301, '130. Hobio, ry olombelona', NULL, NULL, '
{1.
Hobio, ry olombelona!
Jesosy dia velona,
Ny fasana dia rava;
Misokatra ny lanitra,
Ny aizina sy tahotra,
Dia resin''ny mazava,
Jeso Kristy
No deraina sy hajaina.
Fa Mpamonjy,
Tompon''aina mahavonjy.}

{2.
He! Misy làlana indray
Ho ao an-danitra, izay
Miandry ao ammbony;
Fa vitan''ny Mpanavotra
Ny vonjy mahasambatra,
Ka sitraky ny fony
Handray izay
Mitomany, mba ho any
Amin-dRayiny;
Fa izany dia hainy.}
', '#1-II-5', '#1') /end

insert into song values (11311, '131. Tonga ny andro malaza', NULL, NULL, '
{1.
Tonga ny andro malaza, izay mahafaly,
Indro, nandresy Jesosy ilay vao nijaly;
Velona indray Izy,
natsagan''ny Ray,
Aoka isika hifaly!}

{2.
Efa nahery Jesosy, Ka resy Satana;
Menatra mafy ireo fahavalo nitana
Ka mba zahao: rava ny fasana izao,
Rava, fa tsy nahatàna!}

{3.
Helo sy fahafatesana, resy tokoa,
Ary ny tambin''ota dia efa naloa;
Jeso, ''zay maty ka velona indray,
Tompon''aina tokoa!}

{4.
Mba mihobia, ry olona mino rehetra;
An''i Jesosy ny hery izay tsy voafetra;
Izy nanao lalana tsara vaovao;
Resy ny ratsy rehetra.}

{5.
Faly isika fa manana fanatenana,
Mbola hiantso Jesosy hoe:
" Mitsangàna! "
Amin''izay, velona mandrakizay,
Ny nodimandry tra-bonjy.}
', '#1-II-5', '#1') /end

insert into song values (11321, '132. Indro fa velona', NULL, NULL, '
{1.
Indro fa velona
Jesosy Tompon''aina;
Foana ny fasana,
Resy izao Satana,
Iza indray no hitomany,
Raha mahare izany?
Ry foko sasatra,
Manahy fatratra,
Heveronao izao
Ho afaka ny sentonao.}

{2.
Indro fa velona
Jesosy Tompon''aina;
Izy nitsangana;
Ravo ny fo sy saina.
Jeso Tompo nolatsaina.
Tonga loharanon''aina.
Ry havana andrandrao
Ny lanitra izao:
Jesosy velona,
Ka azo ny fiainana.}
', '#1-II-5', '#1') /end

insert into song values (11331, '133. Efa namangy ny olony', NULL, NULL, '
{1.
Efa namangy ny olony
Jeso ''zay Tompon''ny aina.
Lasa ny alina maizina,
:,: Tonga indray ny maraina :,:}

{2.
Maty ny Tompo, nalevina,
Nidina nita ny ony,
Nefa nitsangana velona,
:,: Koa tsy maty intsony :,:}

{3.
Ory ny fon''ny sakaizany,
Aiza Ilay nantenaina?
Hitany nefa tsy fantany,
:,: Jeso, ''zay Tompon''aina :,:}

{4.
Teniny tsy mba mivadika
Amin''ny olony ory,
Tonga ny Tompo nitsidika
:,: /reo mpianany vory :,:}

{5.
Tonga ny taom-pamangiana,
Tondraka ny famonjena,
Velona ny fihobiana,
:,: Aina vaovao no nomena :,:}

{6.
Teraka ny fifaliana,
Tonga ny Filazantsara,
Andron''ny fitomaniana
:,: Mody ho Pasaka tsara :,:}
', '#1-II-5', '#1') /end

insert into song values (11341, '134. Mifaly tokoa ny foko izao', NULL, NULL, '
{1.
Mifaly tokoa ny foko izao,
Fa misy manambara
Ny Filazantsara
:,: O, mba raisonao! :,:}

{2.
Nitsangana Jeso, Haleloia re!
Henoy teny soa,
Ka dia minoa
:,: Fa marina e! :,:}

{3.
He! Rava ny fasana, ka mijere,
Fa efa niala
Ny Tompo malala,
:,: Fa velona e! :,:}

{4.
Ny fahafatesana resy tokoa,
Ka tsy mba mijaly
Ny mino fa faly
:,: Sy tretrika am-po :,:}

{5.
Misaotra, m idera Anao izahay,
Ry Jeso Mpamonjy,
Fa vita ny vonjy
:,: Ho mandrakizay :,:}

{6.
Haleloia! Tsara ny Pasaka e!
Ny Tompon''ny aina
Izao hoderaina,
:,: Haleloia re! :,:}
', '#1-II-5', '#1') /end

insert into song values (11351, '135. Foana ny fasanao', NULL, NULL, '
{1.
Foana ny fasanao,
Jeso Tompon''aina!
To tokoa ny teninao
Ka notontosaina.}

{2.
Foana ny fasanao,
Tompo manan-kery,
Gaga teo ny rafinao,
Resy ''lay mahery.}

{3.
Foana ny fasanao,
Fa efa novohana!
Maito ny fatoranao,
Tsy mba nahatana.}

{4.
Foana ny fasanao.
Zanaky ny Avo!
Aina no nasehonao.
Zava-maharavo.}
', '#1-II-5', '#1') /end

insert into song values (11361, '136. Ry Jeso Tompon''aina!', NULL, NULL, '
{1.
Ry Jeso Tompon''aina!
Nalevina Ianao,
Kanefa, he, deraina,
Fa velona ao izao.}

{2.
Ny fasana nivoha
Noho ny herinao,
Fa tsy mba nahatoha
Ny voninahitrao.}

{3.
Ireo miaramila
Nahery sy nahay
Dia tsy mba nahatana
Anao Mpanjakanay.}

{4.
Nitsangana tokoa
Ho tena santatray
Ianao, ry Tompo soa
Sy Kapiteninay!}
', '#1-II-5', '#1') /end

insert into song values (11371, '137. Vita izao ny fandresena', NULL, NULL, '
{1.
Vita izao ny fandresena
Haleloia, haleloia
Azo koa ny famonjena,
Haleloia;
Resy re ny fasana
Sy ny fahafatesana
Fahavalo farany,
Fa Jeso nitsangana,
Tonga fanafahana;
Haleloia (in-3)}

{2.
To izao izay nambara:
Haleloia, haleloia
Fa ny fatotra nivaha:
Haleloia.
Foana ny fasana.
Jeso re nitsangana
Tamin-kery lehibe
Ka torio ry mino e!
Eran-tany mba ho re:
Haleloia (in-3)}

{3.
Dia venteso ny hosana:
Haleloia, haleloia
Aoka tsy hihambahamba:
Haleloia.
Fa efa velona tokoa
Lay Mpamonjy tsy mba roa
Zay nisolo voina anao;
Izy no nisantatra
Ny fiainan-tsambatra:
Haleloia (in-3)}
', '#1-II-5', '#1') /end

insert into song values (11381, '138. Ty no andro maharavo', NULL, NULL, '
{1.
Ty no andro maharavo,
Ora fifalianay;
Lay Manarina ny lavo,
No niady ho anay.
:,: Haleloia, haleloia!
Ho Anao Mpanjakanay. :,:}

{2.
Azonao ny fandresena,
Ry Mpamonjy velona!
Vitanao ny famonjena,
Tafatsangana Ianao.
:,: Haleloia, haleloia!
Ho Anao Mpanjakanay. :,:}

{3.
Resinao ny fahotana:
Voavoha ny fasanao;
Torotoro re Satana;
Afaka ny olona,
:,: Haleloia, haleloia!
Ho Anao Mpanjakanay. :,:}

{4.
Ry Mpandresy fandozana!
Raisonao ny hajanay;
Ry Mpamonjy! Mitantàna,
Tano ho Anao ''Zahay
:,: Haleloia, haleloia!
Ho Anao Mpanjakanay. :,:}
', '#1-II-5', '#1') /end

insert into song values (11391, '139. He! Vita re ny ady izao!', NULL, NULL, '
{1.
He! Vita re ny ady izao!
Ny ota efa resy re,
Ny feo mifaly no atao.
Haleloia!}

{2.
Fahafatesana no tao,
Fa Kristy naharesy izao
Ny fandreseny no derao,
Haleloia!}

{3.
Ny andro nijaliana e!
Nisolo fifaliam-be,
Ka mihirà izay mandre:
Haleloia!}

{4.
Ny gadra voatapany,
Ny efitra efa ravany;
Ka miderà ny heriny:
Haleloia!}

{5.
Ry Tompo o! Ny fiainanao,
Mamelona ny folonao,
Ka hoderaina Ianao,
Haleloia!}
', '#1-II-5', '#1') /end

insert into song values (11401, '140. Haleloia, Haleloia', NULL, NULL, '
{1.
Haleloia, Haleloia!
Miderà ny Tomponao.
Manandrata feo hidera
an''i Jeso Kristy ''zao.
Fa nijaly mafy Izy,
Sady voavono koa.
Nefa indro, velona izy
Ao an-danitra tokoa.}

{2.
Eny, velona ny Tompo
Mba ho santatsika koa,
Ka ho velona ny mino
Sy hitsangana tokoa;
Dia handray ny lova soa,
Satro-bolamena koa.
Ka hidera ny Mpamonjy
Sy ho sambatra avokoa.}

{3.
Haleloia, haleloia,
Haja, hery, saotra anie
No hatolotra ny Tompo,
''Zay Mpandresy lehibe;
Hoderaina lalandava
Fa manjaka manan-jo:
Haleloia, haleloia,
Hoderaina eram-po.}
', '#1-II-5', '#1') /end

insert into song values (11411, '141. Efa resy izao', NULL, NULL, '
{1.
Efa resy izao
na dia ny fasana aza,
Ka asandrato re
ny hira sy ny feo,
Jesosy nampikoy
Lay rafy ''zay mandoza,
Ry fahafatesana o!
Aiza moa ny herinao?}

{2.
Lasa ilay tahotrao,
ny foko ''zay nalemy,
Fa torotoro ''zao
ny fahavalonao,
Tsy nahatohitra
ny sandrin''i Jesosy;
Ry fahafatesana o!
Aiza moa ny herinao?}

{3.
Efa lasa tokoa
ny ozona rehetra,
Nasehon''i Jeso,
miharihary izao,
Ny fitiavany
izay tsy misy fetra.
Ry fahafatesana o!
Aiza moa ny herinao?}

{4.
Kristy no santatra;
ny lalana mankany
Ho ao an-tranon-dRay,
mivoha tsara izao.
Ny fifaliana
manerana ny tany.
Ry fahafatesana o!
Aiza moa ny herinao?}

{5.
Kristy no fiainana:
izay matahotra Azy
Omeny tanjaka,
fanahy fo vaovao,
Ka ataony velona,
mpandresy tahaka Azy
Ry fahafatesana o!
Aiza moa ny herinao?}
', '#1-II-5', '#1') /end

insert into song values (11421, '142. Tafatsangana tokoa', NULL, NULL, '
{1.
Tafatsangana tokoa,
Haleloia!
Jeso Kristy Tompo soa,
Haleloia!
Maty nefa velona,
Haleloia!
Mihirà ry olona,
Haleloia!}

{2.
Foana ny fasana,
Haleloia!
Indro, Jeso velona,
Haleloia!
Toky tsy mamitaka,
Haleloia!
Mihirà ry olona,
Haleloia!}

{3.
Andro lehibe tokoa,
Haleloia!
Andro fihobiana,
Haleloia!
Afaka ny tahotra,
Haleloia!
Mihirà ry olona,
Haleloia!}

{4.
Haja, laza, aterinay,
Haleloia!
Jeso o! Mpanjakanay,
Haleloia!
Raiso re ny saotranay,
Haleloia!
Raiso koa ny tenanay,
Haleloia!}
', '#1-II-5', '#1') /end

insert into song values (11431, '143. Vita izao ny fandresena', NULL, NULL, '
{1.
Vita izao ny fandresena,
Haleloia!
Jeso Kristy no hobiana,
Haleloia!
Azo re ny famonjena,
Haleloia!
Verinay tary Edena,
Haleloia!}

{2.
Ka izao dia arahabaina,
Haleloia!
''Lay Mpanjakanay hajaina,
Haleloia!
Ka na zovy mitaraina,
Haleloia!
Misy vonjy azo maina,
Haleloia!}

{3.
Betlehema niandohana,
Haleloia!
Kalvary niafarana,
Haleloia!
Fasana tsy nahatana,
Haleloia!
Toro teo ''lay menarana,
Haleloia!}

{4.
Saro-bidy re ny rany!
Haleloia!
Ny fitiavany tsy lany,
Haleloia!
Ny fatoranay vahany,
Haleloia!
Ka hobionareo, ry tany,
Haleloia!}
', '#1-II-5', '#1') /end

insert into song values (11441, '144. O! avia, mba hidera', NULL, NULL, '
{1.
O! avia, mba hidera,
Andro Jobily anio;
Ka faingàna, manatera,
Saotra, laza, ka hobio.
Indro tonga ny anjely,
Ka ny vato mikodia,
Tantaraina mba hiely
Velona (in-3) ny Tomponao.}

{2.
Ifalio ny famonjena,
Andro Jobily anio;
Afaka ny firenena,
Voavonjy androtr''io.
Gaga teo ny mpanakora
Sy ny rafy marobe,
Ka miaiky moramora:
Velona (in-3) ny Tomponao.}

{3.
Mba venteso re ny hira,
Andro Jobily anio;
Resy ny mpaniratsira,
Ka ny tahotrao ario,
He! Ny fasana efa foana,
Tsy nahazona Azy tao,
Tsy naharitra hateloana:
Velona (in-3) ny Tomponao.}

{4.
Efa resy ''zao Satana,
Andro Jobily anio:
Ny fatoranao vahana,
Ka hobio sy ifalio!
Lay Mpamonjy nolatsaina,
Velona ho avotrao,
Io no mendrika hoderaina:
Velona (in-3) ny Tomponao.}
', '#1-II-5', '#1') /end

insert into song values (11451, '145. Efa velona tokoa', NULL, NULL, '
{1.
Efa velona tokoa
Ianao, ry Tomponay!
Ka ny herinao tsy toha,
No manjaka hatrizay;
Fa ny fasana, ny ota,
Noresenao avokoa,
:,: Zay no tokinay mpanota,
Tanjakay sy hery koa :,:}

{2.
Efa velona tokoa,
Ianao, ry Tomponay!
Lasa nanamboatra koa,
Fitoerana ho anay;
Tany tsy mba misy ota.
Tsy hidiran''ny manjo.
:,: Tany soa sy madio,
Maharavoravo fo. :,:}

{3.
Efa velona tokoa,
Ianao, ry Tomponay!
Ka misolo vava koa
Sy mifona ho anay;
Fa Fanahinao Omena
Mba hitari-dalana;
:,: He! Ny sentonay dia renao,
Jeso tena Havana. :,:}

{4.
Efa velona tokoa,
Ianao, ry Tomponay!
Ary mbola ho avy koa;
Avy mba handray anay,
Maminay, ry Tompo tsara!
Hiara-monina aminao;
:,: Eny, tena manan-jara
Zay mby eo akaikinao. :,:}
', '#1-II-5', '#1') /end

insert into song values (11461, '146. Anao ny dera', NULL, NULL, '
{1.
Anao ny dera,
ry Zanaky  ny Ray,
Anao ny fandresena,
ry Mpanjakanay.
Ny fasana nivoha
noho ny herinao,
Ny Anaranao Tsitoha
tsy voatana tao.}

{FIV.
Anao ny dera,
ry Zanaky  ny Ray,
Anao ny fandresena,
ry Mpanjakanay.}

{2.
Ianao nandresy,
lao re ny fasana;
Ny fanjakan''ny ota
koa ho lasana.
Ny herin''ny Satana
vato mikodia,
Ny Anaranao nandresy
haharitra doria,
[FIV.]}

{3.
He, ravo aho:
Izy no santatra;
Izaho koa hifoha,
mba ho sambatra.
Ny fandresen''i Jeso,
lovako tokoa,
Ny Anarany Tsitoha
hanangana ahy koa.
[FIV.]}
', '#1-II-5', '#1') /end

insert into song values (11471, '147. Lasa ny Mpanavotsika', NULL, NULL, '
{1.
Lasa ny Mpanavotsika,
Ao an-danitra ary,
Mba hanoman-trano tsara,
Hitoerantsika ao,
:,: Mifalia :,:
Ho tafiditra ao tokoa.}

{2.
Lasa ny Mpisorontsika
Ao an-danitra ary
ho Mpisolo vava koa
Ho an''olo-marobe.
:,: Matokia, :,:
Tsy ho very Ianao.}

{3.
Lasa ny Mpanjakantsika
Ao an-danitra ary,
Mba handray ny fanjakana
Mety ho anjarany,
:,: Mihobia, :,:
Asandrato re ny feo.}
', '#1-II-6', '#1') /end

insert into song values (11481, '148. Ry varavaran-danitra o!', NULL, NULL, '
{1.
Ry varavaran-danitra o!
Ampandrosoy ny Tomponao,
Fa fandresean lehibe,
No efa vitany taty.}

{2.
Moa iza izao Mpanjaka izao?
Mpandresy iza re izao?
Ny lazany manerana
Ny tany sy ny lanitra.}

{3.
Io no Kristy Mpamonjy soa
Nisolo olo-meloka,
Ka satro-boninahitra
No mendrika ny lohany.}

{4.
Anjaranao, ry foko o!
Hanompo sy hanaraka
''Lay Kapiteny lehibe
Nialoha ao an-danitra ao.}

{5.
Ry Kristy o! Mpanavotray!
Irinay mba ho hita koa
Izany voninahitrao
Sy ny fiandriananao.}

{6.
Raha tapitra  ny andronay,
Vohay ny varavaranao,
Ny trano namboarinao
Honenanay mandrakizay.}
', '#1-II-6', '#1') /end

insert into song values (11491, '149. Efa lasa Jeso Tompo', NULL, NULL, '
{1.
Efa lasa Jeso Tompo,
Hanamboatra izao
Fitoera-mahafaly
Iza re no tsy hankao?}

{FIV.
Fa ao ankoatr''i Jordana,
Ao an-danitra ao ambony
Misy fitsarahan-tsara
Ho an''ny madio.}

{2.
Tany tsy idiran''ota,
Tany masina sy soa,
Ka izay rehetra hankany
Dia ho masina avokoa:
[FIV.]}

{3.
Fa izay matoky tena,
Ny tsy mety mino koa,
Tsy avelany hankany,
Fa ho laviny tokoa.
[FIV.]}

{4.
Ny mandao ny fomba ratsy
Ny mahazo fo vaovao,
Ny navotan''i Jesosy,
Dia izy no hankao.
[FIV.]}

{5.
Ray mpanota saiky very!
Mibebaha re anio;
Maniria hovonjena,
ka ny fahotana ario:
[FIV.]}
', '#1-II-6', '#1') /end

insert into song values (11501, '150. He! Tsara ny fonenanao', NULL, NULL, '
{1.
He! Tsara ny fonenanao,
Andriamanitra o!
Ziona tendrombohitrao,
Tanàana masina;
Ka iza no hiakatra ao?
Izay Izay mahitsy fo,
Izay madio tanana,
Izay manao ny to
Ka mino no Mpanavotra, (in-3)
Ireo hiakatra ao.}

{2.
Mba misandrata faingana,
Ry varavarana!
Hidiran''ny Mpanjakanay
Be voninahitra.
Moa iza izao Mpanjaka izao?
Andriamanitra
Mahery sy mandresy koa,
Mpanjaka masina;
He, Izy ''Lay Mpanjakanay (in-3)
Be voninahitra.}

{3.
Miantranoa, Tompo o!
Mba hifalianay;
Voavoatra ny tranonao
Midira aminay;
Jehovah no Mpanjakanay,
Mpanjaka tianay;
Ny hery, voninahitra,
Sy ny fisaorana,
Anao tokoa, ry Tomponay! (in-3)
Mandrakizay doria.}
', '#1-II-6', '#1') /end

insert into song values (11511, '151. Miakara, miakara', NULL, NULL, '
{1.
Miakara, miakara,
Ry Mpanjaka lehibe !
Vitanao ny famonjena
''zay nalehanao taty.
Mihobia ry anjely!
Ka derao ny Tomponao.
Voninahitra sy haja
Atolory Azy izao!}

{2.
Miakara, miakara,
Ry Mpandresy lehibe
Eso, latsa, fahoriana,
No anjaranao taty;
''Zao ny lanitra miandry.
Miandry ny hiverenanao;
Renao ao ny feo manako,
Feno fanajana Anao.}

{3.
Miakara, ry Mpamonjy !
Ravo ny navotanao ;
Izahay aty an-tany,
Faly te hidera Anao;
Raisonao ny fiderana
''Zay atolotray izao:
Haleloia! Haleloia!
Ho Anao Mpanavotray!}
', '#1-II-6', '#1') /end

insert into song values (11512, '151. Miakara, miakara (Zafindraony)', NULL, NULL, '
{1.
Miakara, miakara,
Ry Mpanjaka lehibe !
Vitanao ny famonjena
''zay nalehanao taty.
Mihobia ry anjely!
Ka derao ny Tomponao.
Voninahitra sy haja
Atolory Azy izao!}

{FIV.
Miakara, miakara,
Ry Mpanjaka lehibe !
Vitanao ny famonjena
''zay nalehanao taty.}

{2.
Miakara, miakara,
Ry Mpandresy lehibe
Eso, latsa, fahoriana,
No anjaranao taty;
''Zao ny lanitra miandry.
Miandry ny hiverenanao;
Renao ao ny feo manako,
Feno fanajana Anao.
[FIV.]}

{3.
Miakara, ry Mpamonjy !
Ravo ny navotanao ;
Izahay aty an-tany,
Faly te hidera Anao;
Raisonao ny fiderana
''Zay atolotray izao:
Haleloia! Haleloia!
Ho Anao Mpanavotray!
[FIV.]}
', '#1-II-6', '#1') /end

insert into song values (11521, '152. Niakatra ao an-danitra', NULL, NULL, '
{1.
Niakatra ao :,: an-danitra, :,:
Ny Zanak''Andriamanitra,
:,: Haleloia! :,:}

{2.
Ka monina ao :,: an-danitra :,:
Jesosy Tompon-danitra
:,: Haleloia! :,:}

{3.
''zao zavatra :,: rehetra izao :,:
Ry Jeso o, hazoninao,
:,: Haleloia! :,:}

{4.
Isaoranay, :,: ry Tompo o, :,:
Ny herinao sy indrafo!
:,: Haleloia! :,:}

{5.
Fa ny devloy resinao
:,: he resinao! :,:
Ny ratsy nofongoranao,
:,: Haleloia! :,:}

{6.
Ry tany :,: ry lanitra :,:
Derao Andriamanitra!
:,: Haleloia! :,:}
', '#1-II-6', '#1') /end

insert into song values (11531, '153. Ao an-dapany Jesosy', NULL, NULL, '
{1.
Ao an-dapany Jesosy
Dia derain''ny tapitrisa,
Fa ny lanitra sy tany
Dia samy mankamamy
:,: ''lay Mpandresy mora fo. :,:}

{2.
Jeso Tompo dia ho avy
Ho Mpitsara aty an-tany,
Na ny ratsy na ny tsara
Samy hanana anjara,
:,: Valin''ny natao taty! :,:}

{3.
Ry Mpiantra be fitia,
Iantrao ''zahay mania,
Raiso ho Anao, tahio,
Hafanao, ampifalio,
:,: Ento any aminao! :,:}
', '#1-II-6', '#1') /end

insert into song values (11541, '154. ''zao Jesosy Tompontsika', NULL, NULL, '
{1.
''zao Jesosy Tompontsika
Dia nasandratry ny Ray;
Ao an-tsezam-panjakany
''zay rehetra nialany
:,: Voarainy indray :,:}

{2.
Tsy hadinony anefa
''zay nilaozany taty,
Fa nomeny Fanahy
Mba hitondra sy hitahy
:,: Ny mpianany :,:}

{3.
Ao anilan-dRainy Izy
Manam-boninahitra,
Ka midera Azy any
''zay rehetra anjakany
:,: Eran-danitra:,:}

{4.
Tompom-pitsarana Izy,
Samy mba hatsangany,
Na ny ratsy na ny tsara,
Ka homeny ''zay anjara
:,: Valin''asany :,:}

{5.
He! Hohitantsika Izy,
Dia ny tena endriny,
Ka ny lova soa homena
Izay nandray ny famonjena
:,: Voatendriny :,:}
', '#1-II-6', '#1') /end

insert into song values (11551, '155. Antsezam-panjakany', NULL, NULL, '
{1.
Antsezam-panjakany.
Ambony kintana,
No lapan''i Jesosy
Ilay nasandratra.}

{2.
Aty an-tany iva,
Anilam-pasana,
No ipetrahantsika
Zoim-pahoriana.}

{3.
Kanefa mety ihany
No monina aty,
Hiomana ho any
An-danitra ary.}

{4.
Isika tsy kamboty,
Na dia ilaozany;
Nomeny ny Fanahy
Ho solon-tenany.}

{5.
Ny fahoraintsika
Dia efa maivana,
Ny fahafantesantsika
Dia afa-tsindrona.}

{6.
Ny ain''i Jeso Tompo
Iainantsika aty;
Tsy ela dia hiseho
Indray ny tenany.}

{7.
Ka hira no venteso,
Ry havany izao,
Ny senton''alahelo
Mba atsaharonao!}
', '#1-II-6', '#1') /end

insert into song values (11561, '156. Jeso Mpanjaka tokana', NULL, NULL, '
{1.
Jeso Mpanjaka tokana
Na aiza misy olona;
Ny fanjakany hanerana
Ny tany sy ny lanitra.}

{2.
Vavaka sy fisaorana
Hakarin''ny navotana;
Haharitra tsy tapitra
Hanolo-boninahitra.}

{3.
Ny firenen-drehetra anie
Hidera ny fitiavan-dehibe;
Ny zaza aza hanatitra
Ny hajam-boninahitra.}

{4.
Ny tany izay alehany
Dia feno fitahiany;
Ny voagadra afaka,
Miadana ny reraka.}

{5.
Izao rehetra izao anie
Hanatitra fisaoram-be
Ho an''ny Tompo tokana,
Izay Mpanjakan-danitra.}
', '#1-II-6', '#1') /end

insert into song values (11571, '157. Hobio, ry trabonjy', NULL, NULL, '
{1.
Hobio, ry trabonjy,
Fa indro ny Mpamonjy
Miakatra indray;
Tsinjovy Izy any
Mandray ny fanjakany
Ao ankavanan-dRay.}

{2.
Ny hiran''ny anjely
Manako sy miely
Ary an-danitra;
Injay ny Ray Tsitoha,
Miarahaba koa,
Mandray ny Zanaka.}

{3.
O, miaingà, ry foko,
Fa aza mitoloko
Aty an-tany izao;
Miandrandrà, jereo
Ilay voahombo teo!
Tsitoha Izy ao.}

{4.
Naharitra ny mafy,
Niady sy nihafy
Ny Tomponao taty;
Tahafonao izany,
Fa he! ny fanjakany
Miandry anao ary.}
', '#1-II-6', '#1') /end

insert into song values (11581, '158. Raha afaka entana aho aty', NULL, NULL, '
{1.
Raha afaka entana aho aty
Ka tody any an-danitra ary
Ny hankaiky Ilay Tompo tia,
No hifaliako mandrakizay.}

{FIV.
Satriko re, ny hanome
Saotra tokoa eran''ny fo,
An''Ilay maty fa velona indray
Hatrehiko ao anilan''ny Ray.}

{2.
''Ndrey ny  haren''izao antrany izao,
Izay handovako ilay tany vaovao!
Fa ny hahita ny tavany ao
No hiadanako mandrakizay.
[FIV.]}

{3.
Hava-malala hihaonako ao,
Hobin-trabonjy hiaraha-manao;
Mamy indrindra anefa izao;
Kristy hatrehiko mandrakizay.
[FIV.]}
', '#1-II-6', '#1') /end

insert into song values (11591, '159. Satrohy voninahitra', NULL, NULL, '
{1.
Satrohy voninahitra,
Anjely masina o!
Ilay voasatrotsilo teo,
Dia Jeso, Jeso, Jeso,
Tompo tokana!}

{2.
Ry afa-pahoriana,
Izay ao an-dapany!
Nombany tao an-dàlana,
Ka saotra, laza, hery,
Aoka ho Azy re!}

{3.
Taranaky ny lavo o,
Izay natsangany,
Ekeo Izy eram-po,
Ka saotra, laza, hery,
Aoka ho Azy re!}

{4.
Ry firenena marobe
Ambany lanitra!
Miankohofa, maneke
Fa Jeso, Jeso, Jeso
No Mpanjaka re!}
', '#1-II-6', '#1') /end

insert into song values (11601, '160. Kristy Mpanjaka!', NULL, NULL, '
{1.
Kristy Mpanjaka!
Mendrika hoderaina;
Izy no mitana
Antsika ho velona aina.
Derao, derao
Ilay Mpanjakanao!}

{2.
Kristy Mpanjaka
Mendrika hohajaina;
Izy no mifona
Ho antsika novonjena
Hajao, hajao
Ilay Mpanjakanao!}

{3.
Kristy Mpanjaka,
Mendrika ho hobiana;
Izy no hitsena
Ny olo-notahiana.
Hobio, hobio
Ilay Mpanjakanao!}
', '#1-II-7', '#1') /end

insert into song values (11611, '161. Mivelatra eo imasoko', NULL, NULL, '
{1.
Mivelatra eo imasoko,
Ny fiandrianan-dehibe
Itoeran''ny Mpamonjiko,
Dia Jeso ''Lay nandresy re!
Fa niakatr''Izy ka nandao
''Ty tany fandalovana,
Manjaka ao an-dapany,
Ka feno voninahitra.}

{2.
Anjely no manotrona
Arahin''olo-masina:
Ireo dia mandohalika
Am-panajana fatratra.
Ny Tompo eo afovoany,
Mamiratra ny tavany,
Ka feno hasambarana
Izay mahazo taratra.}

{3.
Kanefa tsy mipetraka
Ho endriky ny lanitra
''Lay Zanak''Andriamanitra
Fa miasa tsy mijanona.
Renao va ny fifonany
Ho an''ny Fiangonany
''Zay novidiny tamin-dRà,
Ka tiany hampakarina?}

{4.
Ekeo, ry Fiangonana o!
''Lay hany Mpanalàlana,
''Zay mandohalika amin-dRay,
Ka mitomany ho anao!
Ry Tompo, aza sasatra,
Ifony lalandava e!
''Zahay izay navotanao,
Fa te-hankao an-danitra.}
', '#1-II-7', '#1') /end

insert into song values (11621, '162. Amen, Jeso no Mpifehy', NULL, NULL, '
{1.
Amen, Jeso no Mpifehy,
Amen, ny Fanahiny.
Amen, Amen, re, ny teny
Mbamin''ny fandraisany,
Amen, ny batisany.
Amen, ny fiainany.}

{2.
Amen, Jeso no Mpanjaka,
Amen, ny Anarany,
Amen, Jeso tonga maka
Ahy ho anjarany,
Amen, Amen ary to
Izy, Izay inoako.}

{3.
Amen, ny fanantenana,
Zavatra maharitra;
Amen re, ny fifindrana
Raha mankao an-danitra!
Amen, ny fiadanana,
Amen, ny fiainana.}

{4.
Aoka isika hihira Amen,
Amen ho mandrakizay!
Jeso, mba valio Amen,
Dia ho faly izahay;
Amen no ho hiranay
Raha mby Aminao izahay.}
', '#1-II-7', '#1') /end

insert into song values (11631, '163. He Jesosy Tokintsika', NULL, NULL, '
{1.
He Jesosy Tokintsika
Tompon''ny fiainana,
Tonga eto amintsika
Olon-kovangiana,
Izy no hitory teny
Soa, mahavelona,
Fa ny ainy no nomeny,
Mba ho solovoina.}

{2.
Andrandraonao ka andraso
Ny Mazava lehibe!
Dia Ilay tsy hita maso,
Mpanazava, Mpanome;
Fa ny ainao dia ho very,
Raha tsy arahinao
Ilay Mpanjaka manan-kery
Izay nanavotra anao.}

{3.
Eto afovoantsika
Ilay Mazava tokana;
Sambatra izay mahita
Azy, fa ho velona.
Ry Jesosy, iresaho
Izahay mpianatrao,
Hafanao sy hatanjaho
Ny finoanay Anao.}
', '#1-II-8', '#1') /end

insert into song values (11641, '164. Jesosy irery ihany', NULL, NULL, '
{1.
Jesosy irery ihany : Filazantsara soa !
Netin''ireo mpanompo, torina aminao
Maizina avoko izao rehetra izao,
Nefa he, ny mazava naposaka aminao!}

{2.
Jesosy irery ihany no ivavahana
Ny Anarany irery no fanavotana,
Ny olona rehetra no handohalika
Ety an-tany ka hatrany an-danitra!}

{3.
Jesosy tsy miova omaly sy anio,
Ary mandrakizay, fa tsy mba mamitaka;
Mifona ho antsika eo ankavanan-dRay,
JESOSY IRERY IHANY re no isaorana!}
', '#1-II-9', '#1') /end

insert into song values (11651, '165. Haleloia! Miderà!', NULL, NULL, '
{1.
Haleloia! Miderà!
:,: Asandrato, re ny feo :,:
Miderà ny Ray Tsitoha
:,: ''zay Mpamorona anareo :,:
Fa Mpanjaka manan-jo
Haleloia! Haleloia!}

{2.
Haleloia! Mihirà!
:,: Lehibe sy masina :,:
''Lay Mpahary anareo.
:,: Ao ny ranomasina :,:
Sy ny biby marobe
''Ndray mihoby : Haleloia!}

{3.
Haleloia! Mifalia!
:,: ''Lay Mpamonjy be fitia :,:
Sy Mpanavotra anareo.
:,: O, ry olom-bery o :,:
Manatona, mihobia!
Haleloia! Haleloia!}

{4.
Raiso re, ry Tompo o!
:,: Ny mpanota tao anay :,:
''Zay miantoraka aminao.
:,: Mba henoy ny vavakay :,:
Ho fanatitra madio
''Zay atolotray anio.}
', '#1-II-9', '#1') /end

insert into song values (11661, '166. Derao ny Tompo lehibe', NULL, NULL, '
{1.
Derao ny Tompo lehibe
Nahary ''zao rehetra izao;
Haleloia, Haleloia!
Ny lanitra sy ny tany koa
Samia midera avokoa
Asandrato izao ny dera!
Haleloia, Haleloia, Haleloia!}

{2.
Ry fo voadio''ny ràny soa.
Ankalazao ny Tompo tia
Haleloia, Haleloia!
Ry voavela heloka o!
Tsarovy re ny indrafo,
Asandrato izao ny dera:
Haleloia, Haleloia, Haleloia!}

{3.
Miraisa fo ka midera
Fa Izy niahy hatrizay,
Haleloia, Haleloia!
Derao ny Ray sy Zanaka
Sy ny Fanahy Masina
Asandrato izao ny dera:
Haleloia, Haleloia, Haleloia!
Amena!}
', '#1-II-9', '#1') /end

insert into song values (11671, '167. Midera ny Anaranao', NULL, NULL, '
{1.
Midera ny Anaranao
''Zahay, ry Tompo o!
Ka hira no ventesinay}

{FIV.
Hoe : haja, saotra, hery,
Mba raiso ho Anao.}

{2.
He ! voahombo Ianao
Hisolo heloka,
Ka mendrika hanaovanay
[FIV.]}

{3.
Endrey ! ny fitiavanao
Endrey ! ny herinao,
Tra-pamonjena izahay,
[FIV.]}

{4.
Raha tonga ao an-danitra
''Zahay mpanomponao,
Hitohy re ny hiranay
[FIV.]}
', '#1-II-9', '#1') /end

insert into song values (11681, '168. Ry Jeso! ny Anaranao', NULL, NULL, '
{1.
Ry Jeso! ny Anaranao
Dia maminay tokoa;
Mihira sy midera Anao
''Zahay navotanao.}

{2.
Tsy takatry ny hevitra
Ny fitiavanao;
Tsy tambonay tononina
Ny hasoavanao.}

{3.
Ny tsara eran-danitra
Sy eran-tany koa,
Tsy azo anoharana
Ny fitahianao.}

{4.
Ho toy ny loharano e,
Ny fonay olonao;
Tsy hanam-pitsaharana e,
Ny hidera Anao!}
', '#1-II-9', '#1') /end

insert into song values (11691, '169. Ry Tompo Andriamanitra', NULL, NULL, '
{1.
Ry Tompo Andriamanitra,
Mpanao izao rehetra izao!
Midera Anao ny lanitra,
Ny tany koa mitsaoka Anao.}

{2.
Ianao ny Ray mandrakizay,
Mpanao izao tontolo izao.
Tongava re ka mba tahio
Ny vavakay atao anio.}

{3.
Ry Kristy o, Mpanavotra,
Mpamonjy be fitia tokoa;
Ianao no Zanaky ny Ray
Ka mendrika haja, saotra koa.}

{4.
Fanahy Masina o, avia,
Sasao ho voadio izahay
Hanoa sy hanara-dia
Ilay Mpamonjy Tomponay.}

{5.
Anao ny voninahitra
Sy dera tsy mitsahatra
Ry Telo Izay Iray, Tsitoha,
Ray, Zanaka, Fanahy koa.}
', '#1-II-9', '#1') /end

insert into song values (11701, '170. Ry Jeso! ny fitiavanao', NULL, NULL, '
{1.
Ry Jeso! ny fitiavana
No ifalianay,
Fa voavona Ianao,
Hisolo voina anay.}

{FIV.
Mba raisonao re
Ny saotra aterinay izao,
Ho fanajana
Anao, Mpamonjy tia!}

{2.
Mahery ny fitiavana,
Ka itokianay ;
Izay mikambana aminao
Dia tsy hangaihay,
[FIV.]}

{3.
Ambony ny fitiavana,
Ka tsy mba takatray;
Kanefa noho ny herinao,
Hiezaka izahay,
[FIV.]}

{4.
Hiezaka izahay, Jeso ;
Ka mba ho tia Anao,
Ka tsy hatahotra eso koa,
Noho ny aminao!
[FIV.]}
', '#1-II-9', '#1') /end

insert into song values (11711, '171. Jeso, Mpanjaka malaza', NULL, NULL, '
{1.
Jeso, Mpanjaka malaza,
No Tompo Mpanjakanay ;
Mahery sy tsara indrindra,
Ka izy no aronay:
Mendrika hankalazaina
Noho ny heriny;
Eran-tany sy lanitra, koa
Ny zo aman-dazany:}

{FIV.
Ry Tompo, Mpanjaka
Mahery o! Mihainoa!
Avy manaiky tokoa ''zahay
Fa Tompon''ny hery rehetra Ianao,
Tsy misy mihitsy ny toa Anao,
Tsy tratry ny saina ny herinao;
Ny fiderana ''zay entinay,
Raiso re!
Ny haja atolotra Anao ety,
Raiso izao!}

{2.
Tompo, Mpandahatra hendry,
Mpanao izao rehetra izao,
Ny zara sy tendry ataony
Dia tsara sy mahasoa;
Mendrika ny hoderaina
Ny fandaharany;
Atolory ho Azy  ny saotra,
Fa Izy tandrifin''io:
[FIV.]}

{3.
Tompo, Mpanjaka Tsitoha,
Kanefa be indrafo ;
Deraina eram-po eran-tsaina,
Deraina mandrakizay;
He! Ny mpanompo voavonjy.
Samy mihoby, hoe:
"Haleloia, ho amin''ny Avo,
Fa Jeso no Tomponay!"
[FIV.]}
', '#1-II-9', '#1') /end

insert into song values (11721, '172. Hoderaiko lalandava', NULL, NULL, '
{1.
Hoderaiko lalandava
Jeso ''Lay Mpanavotro
Noho ny fitiavana ahy,
Maty mba ho soloko.}

{FIV.
Hoderaiko, hoderaiko
Fa ny rany masina
Nentiny hanavotra ahy;
Afaka ny heloko.}

{2.
Holazaiko lalandava
Ny tantara mamiko,
''Zay nitadiavana ahy
Very noho ny  heloko:
[FIV.]}

{3.
Hohiraiko lalandava:
''Zao ny voninahiny,
Fa ny fahafatesana aza
Resy noho ny heriny:
[FIV.]}
', '#1-II-9', '#1') /end

insert into song values (11731, '173. Hosana!', NULL, NULL, '
{1.
Hosana! (in-3)
Hosana no asandratray
Ho an''i Jesosy,
Fa Izy no Mpanjakanay
Sy Tompo tia koa;}

{FIV.
Hosana no atolotray
Anao, ry Tomponay!
:,: Ity no andro Jobily
Ka hifalianay. :,:}

{2.
Hosana! (in-3)
Hosana ho Anao anie,
Ry Kristy Tomponay!
Fa avy Ianao tokoa
Hanjaka aminay!
[FIV.]}

{3.
Hosana! (in-3)
Hosana! Fa ho sambatra
Izay mpanomponao;
Tsy hisy fahoriana
Ety an-taninao!
[FIV.]}

{4.
Hosana! (in-3)
Hosana! Fa miomana
''Zahay vahoakanao
Hitsena Anao Mpanjakanay
Raha avy Ianao!
[FIV.]}

{5.
Hosana! (in-3)
Hosana! Raisonao tokoa,
Ry Tompo tianay,
Ny dera, laza, saotra koa,
Izay aterinay!
[FIV.]}
', '#1-II-9', '#1') /end

insert into song values (11741, '174. Fanahy Masina o!', NULL, NULL, '
{1.
Fanahy Masina o !
Ny hevitray izao
Ovay ho vao;
Vonoy ny otanay
Diovinao ''zahay,
Ka ny fanahinay
Onenonao!}

{2.
Fanahy Masina o !
Tsinjovinao izao
Ny olonao ;
Mpananatra o! Ovay
Ny eritreritray;
Ny hadisoanay
Esorinao!}

{3.
Fanahy hendry o !
Jereo ''zay izao,
Ka hazavao!
Adala izahay,
Mania ny  hevitray:
Dia atoroy izahay,
Henoy ny hatakay,
Ka ny fanahinay
Ononinao!}

{4.
Fanahy Masina o !
Mba asehoy izao
Ny antranao;
Be tahotra izahay,
Henoy ny hatakay,
Ka ny fanahinay
Ononinao.}
', '#1-III', '#1') /end

insert into song values (11751, '175. Avia, Fanahy o!', NULL, NULL, '
{1.
Avia, Fanahy o !
Fanahy Masina o !
Midina eto aminay
Hitari-dàlana !}

{2.
Fa diso làlana
Ka ory izahay ;
Ny sainay dia maizina,
Ny fonay ratsy koa.}

{3.
Midina Ianao,
Ry Mpanazava anay,
Hanaisotra ny maizina
Aty anatinay!}

{4.
Avia, Mpananatra o !
Mombà anay tokoa ;
Ampahatsiarovinao !
Ny teny mahasoa.}

{5.
Ry Mpampianatra o !
Ampionao izahay,
Ka aoko re ho marina
''zay ianaranay !}

{6.
Mitomoera aty
Anatin''ny fonay;
Izany indrindra, Tompo o,
No angatahinay!}
', '#1-III', '#1') /end

insert into song values (11761, '176. Avia, Fanahy Masina o!', NULL, NULL, '
{1.
Avia, Fanahy Masina o !
Midina eto aminay ;
Fenoy ny hasoavanao
Ny fonay noforoninao!}

{2.
Tongava, ry Mpananatra o !
Nomen''Andriamanitra
Ho loharano velona
Hiboiboika ao am-po.}

{3.
Sokafy ny fanahinay
Hidiram-pitiavana ;
Ny tenanay malemy koa,
Ampaherezonao anie!}

{4.
Reseo ny fahavalonay ;
Onony koa ny tahotray ;
Tantano ao an-dàlana,
Hahatraranay ny lanitra!}

{5.
Tahio mba hitiavanay
Ny Ray sy Jeso Zanany,
Sy hitokianay Anao,
Izay Fanahy Masina!}
', '#1-III', '#1') /end

insert into song values (11771, '177. Ry Fanahy Mpanazava!', NULL, NULL, '
{1.
Ry Fanahy Mpanazava !
Mitariha anay izao,
Hianaranay ny teny
''zay nampanoratinao!
Dia hifaly
Izahay tarihinao!}

{2.
Ry Fanahy Mpanazava !
Maminay ny anatrao
Fa matanjaka sy hendry
''zay ampianarinao!
Hatanjaho
Izahay mpianatrao!}

{3.
Ry Fanahy Mpanazava !
Aoka tsy hangia anay
Ny angano sy tantara
''zay hamily dia anay!
Mba sakano
Tsy handavanay Anao!}

{4.
Ry Fanahy Mpanazava !
Ny Baiboly taminao
Dia fanilo sady toky
Hanatonanay Anao!
Mba tsilovy
''zay miankina aminao!}
', '#1-III', '#1') /end

insert into song values (11781, '178. Avia, Fanahy o!', NULL, NULL, '
{1.
Avia, Fanahy o!
Midina Ianao
Hanova ny fo vatonay
Ho tonga fo vaovao.}

{2.
Izay afafinay,
Tondrahan-drano koa,
Tsy mety misy vokatra
Raha tsy ambininao.}

{3.
Fenoy ny herinao
Ny fon''ny olona
Hamelona finoana
Sy fitiavana.}

{4.
Fanahy Masina o!
Tahio ny asa atao.
Ampandrosoy ao am-po
Ny hasoavanao.}
', '#1-III', '#1') /end

insert into song values (11791, '179. Ny Fanahy Masina efa tonga tokoa', NULL, NULL, '
{1.
Ny Fanahy Masina efa
tonga tokoa,
Mba homba ny mpitory
ny Teny mahasoa,
Koa mitsangana izao
andeha ry mahery fo
Hatramin''ny faran''ny tany
torio ny Teny soa.}

{2.
Voasaron''ny aizim-pito
Ny Nosinao;
Ny odi sy ny sampy no
Notompoiny tao;
Ny fahazarana koa,
Nangeja mafy tokoa,
Kanefa tonga ny mazava:
Resy ny aizina ao.}

{3.
Ry fanahy voatafy hery
Ny Tompo anie
Ka efa miandry anao,
Injay miteny hoe:
"Angony, eny antsoy,
Hanatona avokoa
Ny firenena eran-tany
Ho mpianatro tokoa".}

{4.
Tompo tia o,
Ekeko ny antsonao;
Hafoiko ny hareko,
Ny tenako izao,
Ho vavolombelona,
Hijoro, hanafika,
Hitory sy hamafy koa:
Tafio ny herinao.}
', '#1-III', '#1') /end

insert into song values (11801, '180. Ny Tompo, raha vao nanao', NULL, NULL, '
{1.
Ny Tompo, raha vao nanao
Veloma ny taty,
Namela ny Mpananatra
Ho arony.}

{2.
Dia tonga Izy hanome
Ny hery mahasoa,
Dia hery fanadiovana
Ny fo tokoa.}

{3.
Malemy ny fibitsiny,
Kanefa mora re,
Mahazo fiononana
Izay mandre.}

{4.
Tsinjovy, ry Fanahy o!
Ka iantrao izahay;
Amboary ho tempolinao
Ny tenanay.}
', '#1-III', '#1') /end

insert into song values (11811, '181. Fanahy Mpanazava', NULL, NULL, '
{1.
Fanahy Mpanazava,
Ny foko hazavao,
Maniry lalandava
Ny Pentekositrao.
Ny aizina ao am-poko,
Fongàny re ho lao;
Ny foko mitoloko,
Maniry indrindra Anao.}

{2.
Fanahy Mpanadio,
Ny foko mba sasao,
Diovy re anio
Ho tena lapanao;
Ny hivitra misoko
''Zay te-handroaka Anao,
Dia ota te-hijoko,
Esory sy fafao.}

{3.
Fanahy, Mpampahery
''Zay reraka sy voa,
Ny foko re dia tery
Hiantoraka Aminao,
Izay mandrebireby,
Roahy re izao,
Ny fo mitebiteby,
Hiondana Amiano. Amena.}
', '#1-III', '#1') /end

insert into song values (11821, '182. Ny Pentekosta tonga izao', NULL, NULL, '
{1.
Ny Pentekosta tonga izao;
Fanahy o, ny jironao
Velomy ao anatiko,
Hahafantarako ny to.}

{2.
Ampisehoy, Fanahy o!
Fa Jeso no fiainako,
Ka Izy dia làlana
Ho amin''ny fiainana.}

{3.
Dia mba tarihonao ny fo,
Tohano ny fanahiko,
Ampio ny finoako
Hahita ny mpanavotro.}

{4.
Fa Ianao, Fanahy o!
Hanova ny fanahiko,
Ka raiso sy diovinao
Ny foko ho fonenanao.}

{5.
Ny hazo-fijaliana
Anjara hivesarana,
Fa Ianao, ry Heriko,
Mba miaraha amiko.}

{6.
Mba asehoy ho fantatra
Ny soa tsy ho tapitra,
Ny satro-boninahitra
Homen''Andriamanitra.}

{7.
O, ry Fanahy Masina o!
Ampaherezonao ny fo,
Ka hazavao ny lalàko,
Raha avy ny hodiako.}
', '#1-III', '#1') /end

insert into song values (11831, '183. Midina, ry Fanahy o!', NULL, NULL, '
{1.
Midina, ry Fanahy o!
Midira ao anaty fo,
Ampio ny mihafy,
Fa Ianao no Mpanome
Ny to sy fahazavana be;
Ny vavanay sokafy,
Izao hanao
Saotra tsara hanambara
Ny Mpamonjy,
Fa satria voavonjy.}

{2.
Ka raha re ny teninao,
Dia mba avia, hazavao
Ny hevitry ny teny;
Mba ambaraonao aminay
Ny fomban''ny Mpanavotray,
Lazao ny famonjeny,
Izahay, izay
Vory eto, andreheto
Jiron''aina
Hialan''ny aizin-tsaina.}

{3.
Ny maizina onenanay
Tsy mety afaka aminay,
Ka tena mahavery,
Raha Ianao tsy tonga izao
Mitondra filazam-baovao
Sy toky mampahery;
Ovay izahay,
Hamasino ny te-hino,
Ampidiro
eto aminay ny jiro.}

{4.
Mba hafanao re izahay,
Ampio hitiavanay
Indrindra ny Mpamonjy,
Ny rany mba ho avotray,
Ny didiny harahinay,
Hanananay ny vonjy,
Jeso, Jeso
No torio sy bitsiho
Ao an-tsainay,
Mba hamelona ny ainay.}
', '#1-III', '#1') /end

insert into song values (11841, '184. Fanahy Mpanazava anay!', NULL, NULL, '
{Fanahy Mpanazava anay!
Midina eto aminay,
Tarihonao ny sainay;
Ny alahelonay savao,
Ny toetray Havaozinao,
Velomy re ny ainay.
Sento renao
Mba valio ka tahio
Ny mahantra
Ry Fanahin''ny Mpiantra!}
', '#1-III', '#1') /end

insert into song values (11851, '185. O! ry Fanahin''ny Ray ao ambony', NULL, NULL, '
{1.
O! ry Fanahin''ny Ray ao ambony,
Andriamanitra mandrakizay,
Aza avela hisaraka intsony,
Ny olo-mino, fa aoka hiray;
Taomy ho an''i Jesosy Mpamonjy,
Ny mbola maizina, ka hazavao,
Hahafantarany tsara ny vonjy,
Dia ho faly ny mino Anao.}

{2.
O! ry Mpiantoka ny famonjena,
Azo ny antoka tsara izao;
Tonga ny Iraka be fahendrena
Mba hanambara ny sitraponao;
Ka ny lelafo noraisin''ny mino
Sy ny fiteniny tonga vaovao
Dia nahagaga, ka tena nampino
Olona maro hanaraka Anao.}

{3.
Ry Mpampionona o, mba sitrano
Fa voaratran''ny ota izahay;
Ka izay ory mba ampiadano,
Ny malahelo falio indray!
O, mba ovaonao tokoa ny sainay,
Taomy ka ampahereto ny fo;
Ka raha ory arovy ny ainay
Mba tsy  hafoinay intsony ny to.}
', '#1-III', '#1') /end

insert into song values (11861, '186. Fanay, Mpanavao anay', NULL, NULL, '
{1.
Fanay, Mpanavao anay,
Simban''ny ota izahay,
ka amboarinao indray,
Havaozy ny fanahinay.}

{2.
Avia, ry Afo masina,
Ny ratsy aoka ho levona;
Ry loharano velona,
Tondrahonao ny efitra.}

{3.
Ny lohataonanao anie
Mba hampaniry soa be
Sy hampomony ho Anao
Izao tany karankaina izao.}

{4.
Ny efitra sy tany lao,
Hihoby sy hidera Anao,
Fa olom-potsy no ho ao;
Mpanompo teo fa afakao;}

{5.
Ry lohataona maminay!
Midina faingana aminay,
Fanahy, Mpanavao anay,
Niandry ela izahay!}
', '#1-III', '#1') /end

insert into song values (11871, '187. Ny lanitra sokafinao', NULL, NULL, '
{1.
Ny lanitra sokafinao,
Midina ry Mahery
Ampisehoy ny sandrinao,
Ry Tompo manan-kery,
He manakora izao
Ny fahavalonao,
Ka maro no mandà
Ny afo masina
Narehitrao, ry Jeso,}

{2.
O, ry Mpitia olona,
Mpanjaka voahombo!
Ny teninao dia velona,
Mahery sy mitombo;
Ampahafantaronao
Izao tontolo izao,
Fa tsy mba foana
Ny ranao masina
Nalatsakao fahiny.}

{3.
Mba ampiraiso hina re
Ny olom-boafidy,
Ka akambano mafy e,
Fa lova sarobidy,
Ny fitiavana
Madio sy marina
No aoka hanavao
Ny fiangonanao
Handraisanao ny dera.}

{4.
Ny Nosinay, ny tranonay,
Ny vava aman-tsainay,
Dia mba samy havaozinao,
Ry fototry ny ainay!
Ampiononinao
Ny manina Anao!
Avia, ka mamonje,
Avia, mampandrese
Ny olonao mahantra!}
', '#1-III', '#1') /end

insert into song values (11881, '188. Ry Fanahin''aina', NULL, NULL, '
{1.
Ry Fanahin''aina,
mpanazava saina,
Mpampionom-po,
O, mba akekezo
Ireto miambezo
Hamindrana fo!
Hazavao
Ny olonao,
Mba handraisanao tokoa
Fanomezantsoa.}

{2.
Mba ampivavaho,
Hafanao re aho
Hankalaza Anao!
Hataka ny sento
Asandrato, ento,
Ka valionao!
Anehoy
Sy ampinoy
Ny famindram-po mamonjy
Izay te-ho tra-bonjy!}

{3.
O! mba amosahy
Fahazavam-panahy
Ny fisainanay,
Ka ny fo tarafo
Andreheto afo
Ny fanahinay,
Ka ombay
Re izahay,
Raha antsoinao hanambara,
Ny Filazantsara!}

{4.
O, ry Andon''aina!
Tany karankaina
Mba vontosinao!
Dia ho vanom-boa,
Hahavoka-tsoa
Re ny volinao!
Indrafo
Sy teny to
No harena antenaina
Hanavao ny aina.}

{5.
O, mba areheo
Sy ampahereto
Ny finoanay!
Izahay afaho,
Ampahatanjaho
Ny fanahinay!
Tompo o!
Miraiki-po
Aminao ny olom-bery,
Tonga manan-kery.}

{6.
raha mitoloko,
Tano re ny foko
Tsy handao Anao,
ka tsilovy dia,
Tsy haniasia,
Hahatody ao
Aminao
Ny zanakao,
Ao ny lova andrandraina
Fahasambaran''aina.}
', '#1-III', '#1') /end

insert into song values (11891, '189. O, ry Fanahy Masina o', NULL, NULL, '
{1.
O, ry Fanahy Masina o
Mba midira ao anaty fo!
Taomy ny mania
Hody avokoa;
Eny, mitahia,
Manomeza soa
Miantrà ry Tompo o!}

{2.
O, ry Mazava lehibe,
Hazavao ny teny izay ho re!
Asehoy Jesosy,
Ambarao ny vonjy;
Iasao ny Nosy
Mba ho voavonjy,
Miantrà ry Tompo o!}

{3.
Ry Afom-pitiavana o,
Mba levoninao ny lolom-po!
Ampiraiso ny dia
Ireto ondry vitsy
Mba hifankatia
Ho iray mihitsy
Miantrà, ry Tompo o!}

{4.
Ry Mpampifaly tsara o
Mba omeo toky re ny fo!
Mba hanantenanay
Fa ho voanvonjy,
Ary handrenesanay
Noho ny Mpamonjy,
Miantrà, ry Tompo o!}
', '#1-III', '#1') /end

insert into song values (11901, '190. Ry Fanahy Mpanazava!', NULL, NULL, '

{1.
Ry Fanahy Mpanazava !
Miandrandra Anao izahay ;
Aza ela, fa tongava,
Taomy ny fanahinay,
Mba ho fantatray mazava
''zay lazain''ny teninao;
Areheto lalandava
Ao am-po ny afonao!}

{2.
Ry Jesosy ! mitenena
Araka ny fombanao,
Aminay izay manatena
Mba handre ny feonao!
Fo marary sy manahy
Mba tsaboy sy iantrao;
Ny te-hino, fa tsy sahy,
Anehoy ny holatrao!}

{3.
Rainay, Mpanome ny soa,
Mba henoy ny hatakay,
Ka ny tananao tsy toha
Iarovinao anay;
Ny tanora mba tahio,
Ny vaventy hafanao,
Mba ho hitanay anio
Fa aty ny tenanao!}
', '#1-IV', '#1') /end

insert into song values (11911, '191. Ny Teny sy Fanahinao', NULL, NULL, '
{1.
Ny Teny sy Fanahinao,
Ry Ray, mba avelao
Handroso tsara, hanavao
Izao tontolo izao,
Mitombo, mihabe
Ny ratsy izao, odre!
Fa vitsy ny mpanomponao
Izay ifalianao.}

{2.
Vonjeo, ry Jesosy o!
Ny fiangonanao,
Fa misy marobe izao
Izay mody olonao,
Anefa tsy manao
Izay tena sitrakao;
O, ry Mpanjaka lehibe!
Mba mamonje anie.}

{3.
Fanahy o! Mba raisonao
Izao vavakay izao;
Ny fonay mba havaozinao,
Ny maso hazavao,
Hahafantaranay
Izay alehanay,
Mba hamoazanay izao
Izay voa tianao.}
', '#1-IV', '#1') /end

insert into song values (11921, '192. Andriananahary, aba Ray', NULL, NULL, '
{1.
Andriananahary, aba Ray
Izay ianteheranay,
Mba raiso re ny vavakay.
Esory sy vonoy ny helokay,
Vonjeo, re, izahay!}

{2.
Ry Jeso Zanak''Andriamanitra,
Mpitarika ho an-danitra
Mpanolo-tsaina marina,
Mpamonjy tia ka nolavina,
Vonjeo, re, izahay!}

{3.
Ry Mpampionona mandrakizay,
Ampiomano izahay
Handray ny famindramponao,
''Ndrao very amin''ny otanay
izahay.
Vonjeo, re, izahay!}
', '#1-IV', '#1') /end

insert into song values (11931, '193. Tahio, re, ny teninao', NULL, NULL, '
{1.
Tahio, re, ny teninao,
Ankalazao ny Zanakao,
Izay nasandratrao, ry Ray,
Ho kiady sy ho aronay.}

{2.
Ry Jeso o! Mpanjakanay,
Ampandreseo izahay;
Ho menatra ny rafinao,
Raha hitany ny herinao.}

{3.
Ry Mpampionona anay,
Ampikambano ho iray
Ny sampam-piangonana,
Hiray am-pitiavana.}
', '#1-IV', '#1') /end

insert into song values (11941, '194. Andriananahary o', NULL, NULL, '
{1.
Andriananahary o, endrey ny voninahitrao !
Ny tany sy ny lanitra mamelatra ny lazanao,
Ny asanao rehetra e, ny famonjena lehibe
Mitari-dàlana anay hidera ny Anaranao}

{2.
Ry Zanak''ondry masina,
Jesosy ''zay isaoranay !
He, Ianao miantra anay, ka
Nentinao ny otanay
Ny menarana resina, ny fasana vinakinao
Ka Ianao nanokatra ny lanitra hidiranay.}

{3.
O, ry Fanahy Masina, Izay mitarika anay
Hanetry tena sy hanaiky fa mpanota izahay,
Mitaona sy mananatra anay handray ny avotra,
Ka manamasina anay, - mba raiso ny fisaoranay.}

{4.
Ry voavonjy mbamin''ny anjely mankasitraka,
Ny tany sy ny lanitra mikambana hanandratra
Anao, ry Ray sy Zanaka, Anao, Fanahy Masina
Izay maneho famonjena feno fitiavana.}
', '#1-IV', '#1') /end

insert into song values (11951, '195. Andriamanitra Ray', NULL, NULL, '
{1.
Andriamanitra Ray
loharanon''ny aina,
Masina, masina, re,
Ianao ka deraina,
Fa manome fahasoavana be,
Soa tsy tratry ny saina.}

{2.
O, ry Jesosy Mpamonjy,
Mpanjaka mahery,
Tonga taty mba hampody
ny zanka very,
Raiso izao ny fiderana Anao,
Saotra aman-daza sy hery.}

{3.
Ry Mpampionona o,
Mpanazava ny sainay,
Mba ampinoy izahay,
ka havaozy ny ainay.
Mba hazavao ny manantena Anao;
Raiso ny hira hirainay!}
', '#1-IV', '#1') /end

insert into song values (11961, '196. Ry Rainay ao an-danitra o', NULL, NULL, '
{1.
Ry Rainay ao an-danitra o
Izay mendrika ho deraina,
Mahery sy be indrafo,
Tsy takatry ny saina,
Izahay dia tonga zanakao,
Ka faly manana Anao:
Mihoby sy miravo!}

{2.
Izahay miondrika aminao,
Midera Anao tokoa,
Fa be ny fitiavanao,
Ry Ray mpanisy soa;
Anao izao rehetra izao,
Ho vitanao izay tianao,
Andriananahary.}

{3.
Ry Jeso Kristy Tomponay
Mpanjaka manan-kery,
Izay any an-kavanan-dRay,
Mpanavotra ny very,
He, any no ifonanao
Ho an''izay navotanao,
Fa resinao ny ota.}

{4.
Ny ranao no nalatsakao,
Fanati-tsaro-bidy!
Ny ainao no atohanao
Ny olonao mpiady.
Na aty na ao an-danitra
Jesosy Kristy tokana
Ho avotra sy vonjy.}

{5.
Ry Mpampiantra mahay,
Fanahy Mpanazava,
Ampianaro izahay
Handroso lalandava,
Tariho ny fanahinay,
Honenanay rahatrizay
Ary an-danitra Amen!}
', '#1-IV', '#1') /end

insert into song values (11971, '197. Haleloia, deraina', NULL, NULL, '
{1.
Haleloia, deraina
Andriamanitray,
Mpanjaka fototr''aina
Mpikarakara anay
Ny zavaboahary
Dia eo an-tanananao.
Ka tena lavorary
Ny fandaharanao.}

{2.
Haleloia, deraina
Ianao, Mpamonjinay!
Nijaly, noratraina,
Ka maty solonay,
Anefa naharesy
Ny fahavalonay,
Natsangana handresy
Mba ho fiainanay.}

{3.
Haleloia, deraina
Ianao, Fanahy o!
Ny herinao no aina
Hamelona tokoa;
Ianao mikarakara
Ny fiangonanao,
Manova manatsara
Izao tontolo izao.}

{4.
Haleloia, deraina
Ny Telo Izay Iray!
Hobina sy hiraina,
Fa Andriamanitray,
He, Tompo foto-tsoa
Iainanay aty,
Ka ho izany koa
Mandrakizay doria.}
', '#1-IV', '#1') /end

insert into song values (11981, '198. Isaoranay Jehovah', NULL, NULL, '
{1.
Isaoranay Jehovah,
Fa Tompo lehibe
Izay tsy mba manova
Ny fampindraponao,
Miorina aminay izao
Ny fiangona-masinao;
Torina aminay
Ny teny mampiray
Ka ravoravo izao
Izahay midera Anao.}

{2.
Derao Jesosy Tompo,
Izay Mpamonjinao,
Hiraonao re, ry foko,
Ny fitiavany,
Haleloia, Haleloia!
Haleloia, Haleloia!
Jesosy avotray
Dia Zanaky ny Ray,
Ny ainy no voaray,
Hisolo helokay!}

{3.
Derainay, ry Fanahy
Izao ny asanao,
Tsy misy mampanahy,
Izay tarihinao,
Ka anjakao, re, ny fonay,
Arovy ny fiangonanay,
Velomy izahay,
Ataovinao mahay
Manoka-po izao
Handray ny aim-baovao!}
', '#1-IV', '#1') /end

insert into song values (11991, '199. Misaora! Misaora an''i Jehovah', NULL, NULL, '
{1.
Misaora! Misaora an''i Jehovah,
Ry olona aty an-tany o!
Fa lehibe ny indrafony
Ka maharitra mandrakizay.}

{2.
Misaora! Misaora an''i Jesosy,
Ry olona aty an-tany o!
Fa vitany ny famonjena,
Ka misokatra ny lanitra.}

{3.
Misaora! Misaora re ny Fanahy,
Ry olona aty an-tany o!
Fa fahazavana no omeny
Ary fanavaozana am-po.}
', '#1-IV', '#1') /end

insert into song values (12001, '200. Rainay o Masina!', NULL, NULL, '
{1.
Rainay o Masina!
Tomon''ny lanitra,
''Zay tia anay;
kiady sy mandanay
Ho fiarovana,
''Zay hitokianay
Mandrakizay.}

{2.
Jeso o, Tomponay!
Lalana marina
Amin''ny Ray!
Fahamarinana
Ary fiainana
Azonay aminao
Ry Tomponay!}

{3.
Fanahy Masina o!
Irak''i Jesosy
Tonga ety;
Manazavà anay
Amin''ny maizina;
Ny hevi-dratsinay
Esorinao!}

{4.
Andriamanitra o!
Telo fa tokana,
Tompo iray;
Aza mandao anay,
Ory sy reraka
Noho ny heloka
''Zay momba anay.}

{5.
Aza tsaroanao
Ny hadisoanay,
Ry Tompo o!
Fa mamelà anay
Noho ny zanakao
Ka manomeza anay
Ny fo madio.}
', '#1-IV', '#1') /end

insert into song values (12011, '201. Midina, Tomponay!', NULL, NULL, '
{1.
Midina, Tomponay !
Ampionao izahay
Hidera Anao ;
Ray soa tia anay,
Hery sy tanjakay,
''Zay hotompoinay
Mandrakizay.}

{2.
Avia, Jeso o !
Reseo ny rafinay
Ho lavonao;
Arovy izahay
Amin''ny herinao:
Ny fitarainanay
Henoinao.}

{3.
Fanahy Masina o !
Tariho izahay
Hanaiky ''zao;
Mahery Ianao,
Ny fonay anjakao,
Aza ilaozanao
Mpananatra o!}

{4.
Ry Telo ''zay Iray !
Dera mandrakizay
Omena Anao;
Ny voninahitrao
Aoka ho hitanay,
Ka raisonao ''zahay,
Ho aminao.}
', '#1-IV', '#1') /end

insert into song values (12021, '202. Ry Ray malala o!', NULL, NULL, '
{1.
Ry Ray malala o !
Mba raisonao
Ny haja entiko
Hidera Anao.
Ny fitiavanao
No mamiko izao,
Izaho tia Anao,
Ry Raiko o!}

{2.
Ry Jeso tia o !
Mpanavotra,
Ta-hankalaza Anao
Ny vavako;
Ny avo-dehibe
Manafaka ahy e!
Izao dia Anao,
Ry Jeso o!}

{3.
Fanahy Masina o !
Mpananatra,
Mahiratra tokoa
Ny masoko,
Mazava re izao
Ny lalako hankao ;
He! Fifaliam-be
No ahy ''zao.}
', '#1-IV', '#1') /end

insert into song values (12031, '203. Ry Rainay be fitia', NULL, NULL, '
{1.
Ry Rainay be fitia,
Mpamelona anay!
Tariho tsy hania,
Hazoninao ''zahay;
Fa be ny fahavalo
Mitady ny ainay,
Mamely tsy mimalo
''Lay ratsy hatrizay.}

{2.
Ry Zokinay malala!
Ry Jeso havanay!
Toroy mba hahalala
Ny mety izahay;
Ny miaraka aminao re,
Ry Jeso Tompo soa,
No mahasoa ny saina,
Ny fo, fanahy koa.}

{3.
Fanahy Mpanazava
Sy Mpanadio ny fo!
Tomoera lalandava,
Monena ao am-po;
Koa dia ampidino
Ny hasoavanao,
Ovay sy hamasino
Ny toetray izao.}

{4.
Ry Tompo be fiantra!
Ry Telo Zay Iray!
Ny tananao miantra
Atsory aminay;
Rehefa tapitra andro
Ka hody izahay,
Mba raiso re, ry Tompo!
Mba raiso izahay.}
', '#1-IV', '#1') /end

insert into song values (12041, '204. Mamy ny fitianao', NULL, NULL, '
{1.
Mamy ny fitianao,
Ry Jehovah Tompo o!
Nahafoy ny Zanakao
Hanavotanao anay.}

{2.
Maminay ny antranao,
Ry Jeso Mpamonjy o!
Ta-hamonjy Ianao
Sy hanafa-keloka.}

{3.
Mamy ny fitaomanao,
Ry Fanahy Masina o!
Ny fanahinay ovay
Hankatò  ny anatrao.}
', '#1-IV', '#1') /end

insert into song values (12051, '205. Ny teninao rehetra', NULL, NULL, '
{1.
Ny teninao rehetra,
Ry Tompo tia anay !
No tsy mba manam-petra,
Fa tò mandrakizay;
Ho levona avokoa
Izao rehetra izao,
Haharitra tokoa
''zao teninao izao.}

{2.
Ny tenin''ny Baiboly
No sabatra entinay
Handresy ny devoly,
''lay fahavalonay;
Raha misy mampahory,
Ka osa izahay
Ny teninao mahery
Manjary tanjakay.}

{3.
Fa teny mamy koa
Ho an''ny mpakatò
Ka loharano soa
Miboika ao am-po,
Ho fanomezan-toky
Ny azon''ny manjo,
Sy hani-mahavoky
Mamelona ny fo.}

{4.
Fa teny mampisava
Ny aizim-be aty,
Fanilo manazava
Ny dia ho ary;
Manolo-pahendrena,
Mananatra ny fo,
Milaza famonjena
Sy lova tsy ho lò.}

{5.
Ny teny masinao
Dia tsy mba mety lo;
Izay ambarany
Tsy misy tsy ho to,
Na levona aza ka tsy ao
Izao rehetra ary ''zao}
', '#1-V', '#1') /end

insert into song values (12061, '206. Ny teny masinao', NULL, NULL, '
{1.
Ny teny masinao,
Jehovah Tompo o !
Nampanoratinao
Ireo mpanomponao,
Dia tiako ka tanako
Ho lova sy fananako.}

{2.
Ny teny masinao
''zay noteneninao
Milaza amiko
Ny hasoavanao,
Izay harena atolotrao
Ho entiko hidera Anao.}

{3.
Ny teny masinao,
Andriamanitra o !
Maneho ny fitia
Nataon''ny Zanakao,
''zay maty tany Kalvary
Hamonjy ny mpanota ety.}

{4.
Ny teny masinao
No mampianatra
Ny saiko tsy mahay
Mba hahafantatra
Ny zavatra miafina
''zay momba ny hasoavanao.}

{5.
Ny teny masinao
Dia tsy mba mety l|o;
Izay ambarany
Tsy misy tsy ho tò,
Na levona aza ka tsy ao
Izao rehetra àary ''zao.}
', '#1-V', '#1') /end

insert into song values (12071, '207. Mamy ny teninao (maintimolaly)', NULL, NULL, '
{1.
Mamy ny teninao,
Jehovah Masina!
Sy to ny teninao,
Tsy azo ovana;
Ny lanitra ho levona,
Fa tsy simba ny teninao.}

{2.
Madio ny teninao
Ka tsara indrindra,
Dia fahendrena
Ho an''ny adala;
Mahafaka ny maizina
Ka mitondra ny mazava.}

{3.
Soa ny teninao,
Mahatsara ny fo,
Fa ao no hitanay
Misy Mpanavotra;
Jeso, Loharanon''aina
Mahasasa ny meloka.}

{4.
Irin''ny foko koa
Ny teny masina
Noho ny harena
Na fananana be;
Ny teninao no tanako
Ho lovako mandrakizay.}
', '#1-V', '#1') /end

insert into song values (12081, '208. Ny teny masinao', NULL, NULL, '
{1.
Ny teny masinao
TOrina hatrizao;
Maharitra tsy lo
Mamabo ''zay fo,
Fa marina ka tianay
Hiainanay mandrakizay.}

{2.
Ny teny masinao
Mitondra hery vao,
Mandrava ny manjo
Ireo ''zay ory fo;
Ka mendrika ho tanana
Anaty, ho fananana.}

{3.
Ny teny masinao
Dia tsy mba mankaleo;
Fa hery velona
Mamelombelona;
Ka satrinay ho re indray
Ry Andriananaharinay.}

{4.
Ny teny masinao
No feo manorina;
Ny teny masinao
No feo manarina,
Ka tianay hanenika
Ny tany sy ny lanitra.}
', '#1-V', '#1') /end

insert into song values (12091, '209. Jesosy tena kiady', NULL, NULL, '
{1.
Jesosy tena kiady
Sy reherehanay,
Namoha vavahady
Ny tanindrazanay;
Nampiditra ny teny
Hampahalala anay
Ny Ray ny fahendreny,
Mba hianaranay.}

{2.
Na betsaka mifofo
Ny Baibolinay,
Mihaza sy milofo
Moa ketraka izahay?
Jesosy no Mpiaro
Ka moa ho kivy ve?
Vao mainka mihamaro
Ny mpankasitraka e!}

{3.
Mbola Izy no Filoha
Mpitarika anay,
hitory ka hamoha
Ireo namanay;
Vahoaka an-tapitrisa
No mbola hasondrotray,
Hahita Paradisa
Ho zanaky ny Ray.}

{4.
Ry Jeso, hatanjaho
Ny fiangonanao,
Hijoro, ho tena taho
Mamoa mandrakizay.
Ianao anie no sandry
''Zay tena tanjakay,
No sady hany andry
''Zay hijoroanay.}

{5.
Jesosy soa malala!
Tsy foinay Ianao,
Ny fonay dia handalà
''Ty lova taminao.
Fanahy ravoravo
Noho ny teninao,
Ry Zanaky ny Avo,
Izao misaotra Anao.}
', '#1-V', '#1') /end

insert into song values (12101, '210. Zava-tsoa tokoa ny fitsipikao', NULL, NULL, '
{1.
Zava-tsoa tokoa ny fitsipikao
Tompo, Andriamanitra o!
Teny mahasoa, aina, hery koa
Mba ho an''izay manoa.}

{FIV.
Soa tsy manam-paharoa
Ny fitsipikao
Iraovanay, fa harena be
Sarobidy tsy afoy,
Izany didinao;
Famonjena ho an''izay mandre.}

{2.
Maizina izahay, kely fantatray
Nefa ao ny Teninao :
Jiro sy fanilon''ny tongotray
Hakanesana Aminao
[FIV.]}

{3.
Raha alaim-panahy itony zanakao
Indro, ao ny Teninao,
Sabatry ny fo, hanoheranay
Ilay ratsy hatrizay.
[FIV.]}

{4.
Fifaliana, odin''ny tahotray
Ny Baiboly taminao :
Avy ny manjo, nefa kosa, Ray
Ao tokoa ny tavanao.
[FIV.]}

{5.
Ry Jesosy o Mpampianatray !
Maminay ny Teninao ;
Taomy izahay, mba hahaizanay
Hankatò ny didinao.
[FIV.]}
', '#1-V', '#1') /end

insert into song values (12111, '211. Ilay Boky ''zay nodorana', NULL, NULL, '
{1.
Ilay Boky ''zay nodorana,
Dia may nefa tsy levona;
Izay natao ho lavenona,
Lasa vato fanorenana;
Noheverina ho foana
Kanjo mainka koa velona;
''Lay nafenina, nalevina,
No nisandratra nivelatra!
Iza moa no  hahasakana,
Eny, Iza no hanohitra,
Ilay teny voasoratra,
Dia ny Tenin''Andriamanitra
Zao rehetr''izao ho levona,
Fa ny Teny no haharitra.}

{2.
Rasalama nolefonina,
Nisy hafa koa nodorana.
Hantsana no namarinana,
Tora-bato: fahafatesana
Dia tao ireo nampinomina,
Noterena mba hiozona:
Moa izany nampivadika
Kristiana be finonana?
Valin-teny voasoratra
Nentiny nandresy lahatra:
Feo tony, tava mirana,
Feno hery, hasambarana:
"Tsy mba misy hatahorako
Afa-tsy Ianao ry Tompoko"}

{3.
Ilay teny masina sy to
Manambara ny Mpamonjiko
Tena saro-bidy ao am-po,
Ka vakiko sy iainako
Fifaliako, fiarovako,
Fahazavana ao am-dalako;
Koa tanako ho lovako
Sy isaorako ny Tompoko,
Maty aho nefa velona,
Fa Kristy no ahavelomako.
Maty Izy nohomboana
Noho ny fahotana vitako
Nefa Izy dia natsangana
Santatry ny hitsanganako.}
', '#1-V', '#1') /end

insert into song values (12121, '212. Velona sy mahery', NULL, NULL, '
{1.
Velona sy mahery
Ny Tenin''Andriamanitra ;
Ka na dorana aza
Dia tsy ho levona.
Ny Sekoly Alahady
Dia mivelona aminy.}

{2.
Sabatra roa lela
Maranitra, manindrona
Mampisaraka aina
Sy fanahy, tonona ;
Fantany avokoa
Ny am-po miafina.}

{3.
Ho tsaraina isika,
Ka Ilay Teny velona
No hanavaka antsika
Ary koa hanavotra.
Koa minoa ny teny
Teny fahasoavana.}
', '#1-V', '#1') /end

insert into song values (12131, '213. Ry fanahy mangetaheta', NULL, NULL, '
{1.
Ry fanahy mangetaheta,
Mila fitsaharana:
Aza kivy, mahareta,
Sentonao hifarana!
Fa ny tenin''i Jehovah,
Soa mangarangarana;
Loharano tsy miova,
Ao no hasambarana.}

{2.
Ry tontolo mila vonjy,
Te ho afa-patotra,
Ao ny tenin''ny Mpamonjy
Dia Kristy Mpanavotra!
Teny velona sy mamy,
Feno fananarana;
Hotorina eran-tany,
Fa fampihavanana.}

{3.
Ry Mpanjakan''ny mpanjaka!
Ao, tendreo ny lelako
Mba hitory sy hizaka,
Hampandre ny havako,
Dia tahio ny teny reny
Tsy ho very hasina,
Ka hamoa zato heny
Ny Baiboly Masina.}
', '#1-V', '#1') /end

insert into song values (12141, '214. Finaritra, ry Tompo o!', NULL, NULL, '
{1.
Finaritra, ry Tompo o!
Ny mankafy ny teninao
Sy mankatò ny didy.
Ny helony esorinao,
Ny sainy hohavaozinao,
Ka afaka hifidy
Ny lova sarobidy.}

{2.
Ny teninao no rakitra
Sy tokin''ny mpandefitra,
Fa teny manan-kery;
He! Ao dia voasoratra
Ny aminao Mpanavotra,
Fa Ianao irery
No avotry ny very.}

{3.
Ny teninao no vatsinay
Manohana ny dianay
Sakafo mahasoa
Sy ampy hivelomanay,
Ka ao am-parany indray
Ho azonay tokoa
Ny lovan''ny mpanoa.}
', '#1-V', '#1') /end

insert into song values (12151, '215. Ny teny masina sy to', NULL, NULL, '
{1.
Ny teny masina sy to
Nampilazainao, Tompo o!
No manankery mahavoa
Ny fahavalonao tokoa
Ka dia itokianay
Na aiza no alehanay.}

{2.
Mitaona ny fanahinay
Ny teninao Mpanavotray,
Mananatra sy manambara
Ny ratsy ary koa ny tsara;
Mpilaza mahafatra-po
Ny teninao, ry Tompo o!}

{3.
Ny teninao no sabatray
Sy mpampionona anay;
Ny teninao no mofon''aina
Sy mpanazava fo sy saina:
Mpiaro sady tanjakay
Ny teninao, ry Tomponay!}

{4.
Ampinganay ny teninao
Ao amin''izao tontolo izao,
Ka raha levona ny tany,
Ny teny mbola ao ihany,
Dia izy iankinanay
Ho antoky ny avotray.}
', '#1-V', '#1') /end

insert into song values (12161, '216. Ny teninao ry Tompo o!', NULL, NULL, '
{1.
Ny teninao ry Tompo o!
Dia ranon''aina soa
Izay mahalonaka ny fo
Mangetana tokoa.
Jereo ny tanimbolinao,
Fa tena mila io.
Ho feno voa ho Anao
Hatramin''ny anio.}

{2.
Ny teninao dia sabatra
Maranitra tokoa.
Mitsara sy mamantatra
Ny fonay sy ny voa.
Hetseho izao tontolo izao
Handray ny teninao,
O, asehoy ny herinao,
Hamorona aim-baovao.}

{3.
Ny teninao izay tokinay,
Fanilon-dianay,
Noraisinay ho lovanay
Sy fifalianay.
Ampianaro izahay
Hanoa ny didinao,
Ataovy izay hitiavanay
Ny teny masinao.}
', '#1-V', '#1') /end

insert into song values (12171, '217. Ry Jesosy Tomponay!', NULL, NULL, '
{1.
Ry Jesosy Tomponay!
Indreto izahay mivory,
Nefa tsy mahay mandray
Izay ambaranao akory;
Ka iraho ny Fanahy
Mba hanampy sy hitahy.}

{2.
Maizin-tsaina izahay,
Fa taranaka mpanotra;
Simba ny fanahinay,
Tena voasaron''ota,
Tompo tsara o, tongava,
Aposahy ny mazava.}

{3.
Ry Mazava lehibe,
Taominao ny fo malaina,
Tairo mba hifoha re,
Ka handray ny tenin''aina;
Ny fivavakay valio,
Ny ataonay mba tahio.}
', '#1-VI-1', '#1') /end

insert into song values (12181, '218. Mba miangavy, Tompo o!', NULL, NULL, '
{1.
Mba miangavy, Tompo o!
Iraho ny Fanahinao,
Vangio ny mpanomponao
Ampianaro izy izao!}

{2.
Ampaladio ny sofinay
Handraisanay ny teninao,
Tendreo koa ny lelanay,
Hahaizanay hidera Anao.}

{3.
Hiely be ny lazanao
Ambara-pahatonganao
Hitsara izao tontolo izao,
Hampody ny navotanao.}

{4.
Fanahy, Zanaka sy Ray,
Mba raiso ny fisaoranay;
Ka raha hiala aty izahay,
Hidera Anao rahatrizay.}
', '#1-VI-1', '#1') /end

insert into song values (12191, '219. Ry Mpampianatra mahay', NULL, NULL, '
{1.
Ry Mpampianatra mahay,
Fanahy Mpanazava,
Ampianaro izahay
Handroso lalandava.
Ny idrafon''ny Tomponay
Velaro eo imasonay
Ho fantatray mazava.}

{2.
Mivory eto izahay
Handre ny tenin''aina,
Ka hazavao ho fantatray
Ny hevitra lazaina;
Ovay ny sainay sy ny fo
Hamantatra sy hankato
Ny tenin''ny Mpamonjy.}

{3.
Raha hodimandry izahay,
Ka tonga ny anjara,
Bitsiho ao an-tsofinay
Ny tokin''aina tsara:
Ataovy mora hitanay
Ny tavan''ny Mpanavotray,
Izay hery mahavonjy.}
', '#1-VI-1', '#1') /end

insert into song values (12201, '220. Ry Jeso Tompo, Avotray', NULL, NULL, '
{1.
Ry Jeso Tompo, Avotray,
Mitomoera aminay;
Aidino ny Fanahinao,
Tahio izahay izao.}

{2.
Ny sitrakao mba ambarao
Ho fantatray ny  hevitrao;
Ny fonay amboaronao
Hahay handray ny teninao.}

{3.
Ny vavanay hasinonao
Hahaizanay hisaotra Anao.
Fa be ny soa azonay
Nomanao, ry Mpanavotray.}

{4.
Rahatrizay ny olonao
Hihoby sy hidera Anao,
Fa masina, fa masina,
Fa masina i Jehovah.}
', '#1-VI-1', '#1') /end

insert into song values (12211, '221. Mivory eto izahay', NULL, NULL, '
{1.
Mivory eto izahay,
Jesosy Tompo o!
Avia hanazava anay,
:,: Hinoanay Anao. :,:}

{2.
Maniry mafy izahay
Hihaona aminao:
Omeo hosotroinay
:,: Ny rano velona. :,:}

{3.
Ny mofon''aina mba zarao
Ho anay, ry Tompo o!
Tahio re ny irakao,
:,: Ho voninahitrao. :,:}

{4.
Ny fonay dia atolotray
Anao, Jesosy o!
Omeo hery izahay
:,: Hanao ny sitrakao. :,:}

{5.
Raha tapitra ny andronay,
Izay voafetranao,
Dia ento mody izahay
:,: Ho any aminao. :,:}
', '#1-VI-1', '#1') /end

insert into song values (12221, '222. Ry Lela afo Masina', NULL, NULL, '
{Ry Lela afo Masina
Nilatsaka fahiny
Tandohan''ny mpianatra
Sy tao anaty fony,
Miandry fatratra Anao
Izahay mivory eto izao;
Avia, ry Fanahy!
Avia, ry Jesosy o!
Atreho ny mpianatrao
Ho velona am-panahy.}
', '#1-VI-1', '#1') /end

insert into song values (12231, '223. Mba avia, malakia', NULL, NULL, '
{1.
Mba avia, malakia,
Ry mangetaheta o!
Teny tsara no ambara,
Ka henoy eram-po;
Fa Jesosy ranon''aina
No torina sy lazaina.}

{2.
Ny Mpamonjy mahavonjy,
Mahatonga soa be,
Ka tsy maintsy ho tra-bonjy
Izay manatona Azy re
Ka henoinao ny teny
Manambara ny omeny.}

{3.
Mba tariho sy tahio
Aho, ry Jesosy o!
Mba  ho ahy ny Fanahy
Hanazava ao am-po,
Hampahery fo sy saina,
Ka hamelona ny aina.}
', '#1-VI-1', '#1') /end

insert into song values (12241, '224. Tafavory izahay ka manatona Anao', NULL, NULL, '
{1.
Tafavory izahay ka manatona Anao,
Zanahary nahary anay;
Ka mba raiso izao
Izahay zanakao;
Aza lavina re, izahay!}

{2.
Tafavory izahay ka maintso Anano,
Ry Jesosy Mpamonjy anay:
Mihainoa, Tompo o!
Mitenena izao,
Fa mitady Anao Izahay!}

{3.
Tafavory izahay ka miandry Anao,
Ry Fanahy Mpananantra anay;
Aidino ilay
Afonao fahizay,
Areheto aty aminay.}
', '#1-VI-1', '#1') /end

insert into song values (12251, '225. Aty an-tranonao izao', NULL, NULL, '
{1.
Aty an-tranonao izao
No iangonanay
Hitady soa aminao,
Hahafaka ota anay,
Hiantso ny anaranao,
Handinika ny teninao,
Izay mahavelona anay,
Ka mba ambinonao.}

{2.
Na vitsy aza eto izao,
Na be no tonga aty,
Dia aza mba lalovanao,
Atreho izahay!
Raha tonga eto Ianao
Dia ampy izany ho anay:
Ny fofonain''ny Tomponay
No mahavelom-po.}

{3.
Raha misy mangatsia-po,
Mba hafanao, ry Ray;
Iraho ny Fanahinao
Hamelona anay,
Hanetsika ny mafy fo
Ho tonga mpino mpankato
Izay lazain''ny teninao
Hataon''ny zanakao.}

{4.
Izay rehetra tonga aty
Ampirisihonao,
Ka aoka samy hankafy
Ny fanavotanao,
Ny fonay mba sokafinao
Handray ny fananaranao;
Ao aminao ny hery be,
Efao ny sitrakao.}

{5.
Ambininao ny irakao
Hitory teny izao;
Tolorinao ny hevitrao,
Ny fony hafanao;
Sokafy re ny vavany,
Tendreo koa ny lelany
Hilaza tsara aminay
Ny sitrakao, ry Ray.}
', '#1-VI-1', '#1') /end

insert into song values (12261, '226. Tompo o! mba te-hihaona', NULL, NULL, '
{1.
Tompo o ! mba te-hihaona
Aminao ny olonao.
Ka ny oran-dohataona
Ampombay  ny teninao;
Ny Fanahinao iraho
Aminay izay mitalaho;
:,: Mba vokiso zava-tsoa
Ny mangetaheta Anao :,:}
', '#1-VI-1', '#1') /end

insert into song values (12271, '227. Eto afovoanay', NULL, NULL, '
{1.
Eto afovoanay
Ianao, ry Tomposoa
Ka tahio ny vavakay
Mba ho maminao tokoa;
Raisonao ny fideranay,
Ka savao ny fahantranay.}

{2.
Mitoera aminay
Na hariva na maraina;
Aza mahafoy anay
Mandra-pialanay aina;
Ka ny dera tsy ho lany
Dia ho anao ihany.}
', '#1-VI-1', '#1') /end

insert into song values (12281, '228. Tafangona aty indray ', NULL, NULL, '
{Tafangona aty indray
Ny fiangonanao, ry Ray,
Ka dia mba tahio!
Iraho ny Fanahinao
Hitarika ny zanakao,
Hivavaka anio.
Anao izao:
Saotra, laza, dera, haja,
fa nomena
Ho anay ny famonjena.}
', '#1-VI-1', '#1') /end

insert into song values (12291, '229. Jesosy o!', NULL, NULL, '
{1.
Jesosy o!
Mba hasoavinao
Ny asanay, izay atombokay
Amin''ny anaranao;
Ka mba ombay.
Fa satria sitrakao,
Fa ny anaranao
No anton''ny atao.
Dia hotanterahinay,
Mba ho fifalianay
Sy ho voninahitrao.}

{2.
Jesosy o!
Derainay Ianao,
Fa feno famonjena olona
Ny Anara-masinao!
Ny herinao
Tompo o! Manavao
Izao tontolo izao
Ny teny masinao
Dia iankinanay;
Ianao isaoranay
Eto sy rahatr''izay.}

{3.
Jesosy o!
Ny fanorenana
Iainanay sy hahafatesanay
Dia ny Anaranao,
Fa Izy no
Manome toky be,
Ka hitsangananay
Ho velona indray;
Dia mba horaisinao
Izahay navotanao
Amin''ny Anaranao.}
', '#1-VI-1', '#1') /end

insert into song values (12301, '230. Mba trotroy ny olona ory', NULL, NULL, '
{1.
Mba trotroy ny olona ory,
Ry Jesosy Tompo o!
Tsy hirenireny saina,
Fa handray ny teninao;
:,: Olona osa, olona osa,
Ento mora, Tompo o! :,:}

{2.
Jeso, Tomponay Tsitoha!
Taomy ny sakaizanay,
Mba ho tonga mpino koa
Sy hiankina aminao,
:,: Mba tariho, mba tariho
Izy ho mpanomponao! :,:}

{3.
Faly izahay, Jesosy,
Tratr''izao anio izao,
Tafavory ka hihaino
Ny Filazantsaranao,
:,: Raisonao re, raisonao re
Izao fisaoranay izao! :,:}
', '#1-VI-1', '#1') /end

insert into song values (12311, '231. Ry Tomponay Tsitoha', NULL, NULL, '
{1.
Ry Tomponay Tsitoha,
Ilay iray hatrizay,
Avia, hampifoha,
Ny fiangonanay,
Atreho ny mpivory,
Fa manantena Anao;
Fohazy ny matory
Handray ny teninao.}

{2.
Iraho ny Fanahy
Mba hanavao anay
Ka homba sy hitahy
Ny teny inoanay.
Na lehibe na kely,
Ampio sy hafanao
Hazoto hampiely
Aty ny lazanao}

{3.
Ny asam-pamonjena
Izay natombokao,
Ambino tsy hihena,
Tahio sy tontosao!
Ny herin''i Satana
Folahy hatrizao,
Ho afa-pahotana
Izahay navotanao.}

{4.
Arovy ny finoana
Izay naorinao;
Vonoy ny hevi-poana
Mandà ny teninao,
Izahay na be na vitsy,
Hazony tsy hania,
Mba ho Anao mihitsy
Mandrakizay doria.}
', '#1-VI-1', '#1') /end

insert into song values (12321, '232. Ny alahady andronao', NULL, NULL, '
{1.
Ny alahady andronao,
Jesosy Tompo Masina o!
Fotoana nitsangananao
Ka maminay mpianatrao.}

{2.
Anio, mivory izahay,
Tahaka ireo mpianatrao;
Ka enga anie ho hitanay
Ny  hazavan''ny tavanao.}

{3.
Omeo anay, omeo indray
Ny herin''ny Fanahinao;
Havaozy ao anatinay
''Lay Pentekosta fahizay.}
', '#1-VI-1', '#1') /end

insert into song values (12331, '233. O! Ray Malala', NULL, NULL, '
{1.
O! Ray Malala, asehoy
Ny hasoavanao;
Sasao ho afa-keloka
''Zahay ho zanakao:}

{FIV.
Ho mafy ny fiezakay
Handia ny lalanao,
Tsy misy hijanonanay,
Raha tsy an-tranonao.}

{2.
O! Ray Malala, mba henoy
Ny feonay zanakao;
Tantano ao an-dàlana
Hankany aminao:
[FIV.]}

{3.
O! Ray Malala, hitanao
Ny hasambaranay;
Ny avotr''ain''ny Zanakao
No itokianay:
[FIV.]}
', '#1-VI-1', '#1') /end

insert into song values (12341, '234. Mahery Ianao', NULL, NULL, '
{1.
Mahery Ianao,
Jehovah Tompo o!
Mombà anay mpanomponao,
Ka miarova anay.}

{2.
Mpanjaka Ianao
Ry Zanahary o!
Avia, anjakao izao
Ny ao anatinay.}

{3.
Miaro Ianao
Andriamanitra o!
Hidera sy hisaotra Anao
No iangonanay.}

{4.
Miantra Ianao,
Ry Tompo tia o!
Tongava aminay izao
Mba hifalianay.}

{5.
Ny fanomezan-tsoa
''Zay maminay tokoa
Dia Jeso ''lay malalanao
Nafoy ho Avotray.}

{6.
Mba raisonao, ry Ray!
Ny saotra entinay,
Ny dera, laza, hery koa,
Anao Mpanjaka soa.}
', '#1-VI-1', '#1') /end

insert into song values (12351, '235. Tafavory izahay', NULL, NULL, '
{1.
Tafavory izahay
Eto anatrehanao,
Ry Jehovah Tomponay !
Mba hivavaka aminao.}

{2.
Te hianatra izahay
Mba hanao ny sitrakao ;
Mihainoa ny vavakay;
Manampia anay izao.}

{3.
Fa mangataka aminao
Fanampiana izahay ;
Raha tsy ampianao,
Dia ho diso izahay.}

{4.
''zay rehetra anananay
Entinay midera Anao,
Aoka hazoto izahay
Mba hanao ny sitrakao.}

{5.
Aoka handroso izahay
Mba hanompo tsara Anao,
Dia hifaly izahay
Amin''ny fitianao.}
', '#1-VI-1', '#1') /end

insert into song values (12361, '236. Avia, Tomponay!', NULL, NULL, '
{1.
Avia, Tomponay !
Atrehonao ''zahay,
Fiderana no atolotray
Ho Anao, ry Tomponay!
Tongava aty an-tranonay,
Handray ny dera atolotray:
Avia, Tomponay!}

{2.
Avia, Tomponay!
Atrehonao ''zahay;
Enga ka ho tonga olonao
''zao ''zahay rehetra izao!
Ny vavanay hidera Anao,
Ny fonay mba divoninao
Avia, Tomponay!}

{3.
Avia, Tomponay!
Atrehonao ''zahay;
Ampidino ny Fanahinao
Ho aty an-tranonay;
Ampionao koa ny olonao,
Hilaza anio ny teninao:
Avia, Tomponay!}

{4.
Avia, Tomponay!
Atrehonao ''zahay;
Fiderana no atolotray
Ho Anao, ry Avotray!
Mba raisonao ny deranay,
Efao tokoa ny hatakay:
Avia, Tomponay!}
', '#1-VI-1', '#1') /end

insert into song values (12371, '237. Ry Jehovah Tomponay!', NULL, NULL, '
{1.
Ry Jehovah Tomponay!
Tafavory izahay;
Avy hankalaza Anao.
Izahay vahoakanao.}

{2.
Faly, ravo izahay
Noho ny soa azonay;
Tonga mba hisaotra Anao
Izahay voavotrao.}

{3.
Saotra tsara entinay,
Hira no asandratray
Mba ho fiderana Anao,
Rain''izao rehetra izao.
Amena.}
', '#1-VI-1', '#1') /end

insert into song values (12381, '238. Mivory eto izahay', NULL, NULL, '
{1.
Mivory eto izahay
Hiantso ny Anaranao;
Jehovah Masina o!
Dia manomeza anay anio
Ny soa avy aminao,
Ho fitahiana.}

{2.
Ny vavaka tononinay
Miakatra any aminao,
Mpamonjy tia anay!
Mba raiso ao an-danitra
Ho zava-mani-pofona
Izay ho tianao.}

{3.
Ny saotra izay aterinay
Mba raisonao, Jehovah o,
Andriamanitray!
Ny vokatry ny molotray
Dia aoka ho fanatitra
Ankasitrahanao.}

{4.
Ankehitriny, Tompo o!
Omeo ny Fanahinao
Hitoetra aminay,
Ho fanalana aizina,
Sy fanoloran-kevitra
Ho an''ny olonao.}
', '#1-VI-1', '#1') /end

insert into song values (12391, '239. Ry Jeso! Vory izahay', NULL, NULL, '
{1.
Ry Jeso! Vory izahay,
Fa andronao anio.
Dia avy mba hivavaka
Aty an-tranonao.}

{2.
Ka na aiza na aiza ny olonao
No vory toy izao,
Ny herinao mba asehoy,
Efao ny teninao.}

{3.
Ny hira ikambananay
Aty an-tranonao,
Ekeo ho fanandratana
Ny voninahitrao.}

{4.
Ny vavaka tononinay
Eo anatrehanao,
Mba raiso ho fanatitra
Ankasitrahanao.}

{5.
Ny teny fitarainana
Ataon''ny ory koa
Solo fifaliana;
Hisaoranay Anao.}

{6.
Ny teninao halahatra
Hambininao anie,
Hamelom-pitiavana
Am-pon''izay mandre.}
', '#1-VI-1', '#1') /end

insert into song values (12401, '240. Alahady, andro tsara', NULL, NULL, '
{1.
Alahady, andro tsara,
Andro fitsaharanay,
Nitsangananao, ry Tompo!
Andronao, Mpanavotray!
Eny, maminay ny andro,
Andro fiangonanay,
Hiderana Anao, Mpamonjy,
Sy ny voninahitrao.}

{2.
Tovy aminay ny teny
''zay nataonao fahizay,
Tamin''ny Apositoly
Fony vory toy izao;
Raha misy roa na telo
Amin''ny Anarako
Vory mba hiantso Ahy,
Ao no hiangonako}

{3.
Mba soraty am-ponay,
Tompo o ! ny didinao,
Hiasanay saina tsara
Eto anatrehanao,
Mba avia, ry Jeso Tompo!
Tomoera aminay,
Hitandremanay ny andro
''zay natokana ho Anao.}
', '#1-VI-1', '#1') /end

insert into song values (12411, '241. Ry Jeso o! Midina', NULL, NULL, '
{1.
Ry Jeso o! Midina,
Jereo ''zahay izao;
Atreho sao variana,
Ka tsy mahita Anao.
Hazoninao ny saina,
Hijery tsara Anao,
Mba tsy ho very maina,
''Zao fivoriana izao.}

{2.
Ry Jeso! hamasino
Ny toe-piainanay,
Ka dia ampidino
Ny hery ho anay;
Ny bitsika mangina
Ataovinao izao,
Hamelom-paniriana
Hanao ny sitrakao.}

{3.
Ry Jeso o! Monena,
Midira Ianao,
Ny fonay manantena
Handray Anao izao;
Ka izay misalasala
Ampaherezonao,
Dia taomy hahalala
Ny fitiavanao.}

{4.
Ny hatakay mangina
Henoy ka mba valio,
Koa dia mitenena,
Ianao ankehitrio,
Ry Jeso o! Mpanazava
Ny dianay aty,
Omany lalandava
Ny fonay ho ary.}
', '#1-VI-1', '#1') /end

insert into song values (12421, '242. Misaotra Anao ''zahay ry Ray!', NULL, NULL, '
{1.
Misaotra Anao ''zahay ry Ray !
Fa tsy nafenina anay
''zay tena sitrakao tokoa
Mba hahazoanay ny soa}

{2.
Ka mitariha anay izao
Handray ny toro-hevitrao
Izay lazain''ny teninao
Hanaovanay ny sitrakao.}

{3.
Ampahirato izahay
Hahaizanay mahita anio
Ny zava-mahagaga anay
Ao amin''ny lalànao soa.}

{4.
Ampianaro izahay
Ho tia tokoa ny teninao,
Ka hankatò ny didinao,
Hanaiky hotapahinao.}

{5.
Ny asan''ny Fanahinao
Mba tanteraho aminay
Hampiorina ao anatinay
Izay lazain''ny teninao.}
', '#1-VI-1', '#1') /end

insert into song values (12431, '243. Mivory eto izahay', NULL, NULL, '
{1.
Mivory eto izahay
Handre ny teninao
Izay hambara aminay,
Andriamanitra o!}

{2.
Tolory fahendrena, ka
Ampio ny irakao,
Izay hitory aminay
Ny teninao izao}

{3.
Ny fon''izay mihaino ko
Sokafy rahateo,
Ho tia sy hirakitra
Ny teny mahasoa.}
', '#1-VI-1', '#1') /end

insert into song values (12441, '244. ry Jeso! mba henoinao', NULL, NULL, '
{1.
Ry Jeso ! mba henoinao
Ny feonay mpanomponao,
Izay mivavaka aminao
Mitady fitahiana.}

{2.
Vokiso ny fanahinay,
Ny teninao no haninay,
Hahaizanay mamahana
Ny reraka ao an-dàlana.}

{3.
Ampaherezonao izao
Ny tànanay mpanomponao,
Hanohana ny olonao
Miraviravy tànana.}

{4.
Omeo fo mazot ore !
Tolory fahendrena be ;
Tarihonao ny hevitray;
Tendreo koa ny molotray.}

{5.
He ! tsinontsinona izahay
Malemy sady tsy mahay ;
Ka dia ampaherezonao
Hanaovanay ny sitrakao.}
', '#1-VI-1', '#1') /end

insert into song values (12451, '245. Ray o! Asehoinao', NULL, NULL, '
{1.
Ray o! Asehoinao
Aminay ny antranao,
Raisonao ny zanakay,
Atsangano ho Anao.}

{2.
Zanak''Andriamanitra o!
Mba henoy ny hatakay,
Trotroinao ny zanakay,
Taomy ho mpanomponao.}

{3.
Ry Fanahy Masina o!
Mitoera aminay,
Indro re ny zanakay,
Ampianaro, hazavao.}

{4.
Ry Jehovah, ''Zay Iray!
Nefa Telo, dia ny Ray,
Zanaka, Fanahy koa!
Mba tahio ny zanakay.}
', '#1-VI-2', '#1') /end

insert into song values (12461, '246. Zazakely entinay', NULL, NULL, '
{1.
Zazakely entinay
mba hatao batisa izao.
Izy no atokanay
Sy atolotray Anao.}

{2.
Rano tsy mba mahasoa.
Rano tsy mba mahadio;
Ka tahionao izy ''zao,
Jeso o, be indrafo!}

{3.
Ary ny Fanahinao
Dia ataovy ao am-po
Mba  hitarika azy, sao
Hisy loza izay hanjo.}

{4.
Ry Jehovah be fitia!
Ry Jesosy Tomponay!
Ry Fanahy koa, avia
Mba handray ny zanakay!}
', '#1-VI-2', '#1') /end

insert into song values (12471, '247. Ry Tompo o! Tsinjovinao', NULL, NULL, '
{1.
Ry Tompo o! Tsinjovinao
Ny mila famonjena,
Ka hatao batisa izao.
Satria manantena
Ny tany namboarinao,
Ho an''izay mpianatrao
Ho lovany tokoa.}

{2.
Fa Ianao dia tsy mandà
Ny ory sy mpanota,
Izay nandatsahanao ny ra
Hanaisotra ny ota;
Ka tonga izahay izao.
Mitady hodiovinao:
Mba raisonao ny fonay.}

{3.
Mifona, Tompo, izahay
Izay efa voaisa
Ho olonao, izay nandray
Ny soan''ny batisa;
Ampaherezonao ny fo,
Mba tsy hafoinay ny to
Ambara-pahafaty!}
', '#1-VI-2', '#1') /end

insert into song values (12481, '248. Nitondra ny zaza', NULL, NULL, '
{1.
Nitondra ny zaza
Ny reny tao Salema,
Mba hotahin''i Jeso Kristy
Be indrafo;
Novalin''ny mpianatry izy,
Nefa hoy Jesosy hoe :
"Avelao ny zaza hanatona izao."}

{2.
Fa Ahy ny zaza ka tiako indrindra,
Mpiandry zanakondry Aho,
Ka aza mandà ;
Fa raha manatona Ahy izy,
Hotahiko tsara e ;
"Avelao ny zaza hanatona izao."}

{3.
Endrey, mahagaga ny fiantran''i Kristy,
Fa indro, zazakely aza asiany soa;
Avia, manatòna
Ilay Mpiandry zanakondry, ka
"Avelao ny zaza hanatona izao."}

{4.
Ry Jesosy Tompo ! mampanatòna Anao re
Ny zazakely tafavory eto izao;
Izay mandrara mba rarao,
Ho tò anie ny teninao:
"Avelao ny zaza hanatona izao."}
', '#1-VI-2', '#1') /end

insert into song values (12491, '249. Tompon''ny lanitra o! Mba tahio', NULL, NULL, '
{1.
Tompon''ny lanitra o! Mba tahio
Izy izay ta-hanatona Anao,
Ka ta-handray ny batisa anio,
Sady maniry mba ho zanakao.}

{2.
Ampifalio ny fony, izay ory
Sady miaiky ny ota natao,
Ary ny ratsy maniry fongory,
Ka mba omeo ny aina vaovao.}

{3.
Izy manaiky hitana ny soa
Sady mandà ny devoly izao;
Aoka hiseho ho mpino tokoa
Amin''ny fomba sy asa vaovao.}

{4.
Afon''ny lanitra o! Mba avia,
Ka ateraho ny fony izao,
Ary tarihonao mba tsy hania,
Fa te-ho sambatra ao aminao.}

{5.
Andriamanitra Ray be fitia,
Taomy ny maro hanatona Anao,
Ny Fiangonanao, ho be mpitia,
Ho marobe ny ankohonanao!}
', '#1-VI-2', '#1') /end

insert into song values (12501, '250. Atolotray hatao batisa', NULL, NULL, '
{1.
Atolotray hatao batisa
Ny zanakay, Jesosy o!
Irinay izy mba ho isan''ny
zaza tezainao,}

{2.
Izahay izao efa nifidy
Anao handray ny zanakay;
Ka raiso izy fa voafidy
Tamin''ny ranao koa.}

{3.
Soraty izy mba handova
Ny lanitra fonenanao,
Ka ny fanahinao hanova
Ny fony ho Anao.}

{4.
Tahio ny fahazazany
Ho fanorena-marina;
Ampianaro azy izany,
Mivavaka aminao.}

{5.
Tantano izy mandra-pita,
Ho tonga ao an-danitra,
Ka aoka izy mba hahita
Anao an-dàlany.}

{6.
Ka amin''ny fotoana feno,
Handaozany ity tany ity,
Omeo toky, iteneno:
"Ndeha hody, anaka."}
', '#1-VI-2', '#1') /end

insert into song values (12511, '251. Ry Jeso Zana-Janahary', NULL, NULL, '
{1.
Ry Jeso Zana-Janahary
Sy fototry ny aim-baovao!
Mba aniraho ny Fanahy
Izao madinikao izao.}

{2.
Ny fahafatesanao nandresy
No aoka hiombonany
Ny herinao tsy mety resy
No mba ho fiarovany.}

{3.
Ka ny Anaranao hajaina
Dia mba omeo ny zanakay,
Ka io no hialany aina
Sy hitsanganany indray.}

{4.
Tantano izy tsy ho osa,
Raha lavitra ny diany;
Fa raha fony ihany kosa,
Mba raiso ny fanahiny.}

{5.
Ka mba soraty ao am-pony,
Ry Tompo, ny Anaranao,
Ka ny anaran''ny mpanompo
Soraty ao an-tananao.}
', '#1-VI-2', '#1') /end

insert into song values (12521, '252. Mba avelao ny zaza', NULL, NULL, '
{1.
Mba avelao ny zaza
Hanatona Ahy re,
Fa an''ny toa azy
Ny fanjakan''ny Ray;
Izany teny tsara
No ifalianay,
Ka dia tsy rarana
Ny zazakelinay.}

{2.
Ao amin''ny batisa
Trotroiny izahay,
Ka tena voaisa
Ho zanany indray,
Atsangany handova,
Ny fahasambarana,
Fiainan-tsy miova
Sy fiadanana.}

{3.
Sakaizanay malaza,
Ry Jeso, Ianao,
Finaritra ny zaza
Miankina aminao
Ampio, ry Mpamonjy,
Ny ondrikelinao,
Ho tonga voavonjy
Ao anatrehanao.}
', '#1-VI-2', '#1') /end

insert into song values (12531, '253. Avia, ry Mpanavotra', NULL, NULL, '
{1.
Avia, ry Mpanavotra,
Tahio ny fandraisana,
Sakafo namboarinao
Izao hatoninay izao.}

{2.
Ireo zava-masina
Aroso mba hohanina
Ho famelana heloka,
Ho aina sy ho tanjaka.}

{3.
Tsaroanay ny dianao,
Ny ra sy fijalianao,
Ny nisoloanao anay,
Ny nitsangananao indray.}

{4.
Ny tenanao natolotra,
Ny ranao dia nalatsaka,
Sakafo iombonanay
Ho tena sy fanahy iray.}
', '#1-VI-3', '#1') /end

insert into song values (12541, '254. Avia, ry mangetaheta o!', NULL, NULL, '
{1.
Avia, ry mangetaheta o !
Ka aza dia mba mamoy fo,
Fa indro misy rano velona,
Izay omena mba hamelona
Anao mahantra sy variana,
Ry zanak''ondry very làlana.}

{2.
Ny mofon''aina no atolony
Anao manaiky mba ho olony ;
Ny tenany no ho fihinana,
Mba hahazoana fiainana;
Kanefa tsy mba hovidina anie,
Fa tena fanomezan-dehibe.}

{3.
Jesosy Tompo no nanolotra
Ny rany masina ho avotra ;
Izany raiso mba ho tanjaka
Sy toky ho anao, ry reraka;
Ry mpivahiny sasatra ao am-po,
Avia re, ka manaràna fo!}
', '#1-VI-3', '#1') /end

insert into song values (12551, '255. Ny ran''ny Zanakondry latsaka', NULL, NULL, '
{1.
Ny ran''ny Zanakondry latsaka
Namelona Ahy tany Golgota,
Ka io no tena vato fantsika
Maharitra ny tafio-drivotra.}

{2.
Raha voaratra noho ny otako,
Ka lany foana ny heriko
Ny rany masina nalatsaka
No nahitako fahasitranana.}

{3.
Izao hira vao izao no mamiko,
Ka tonga tsy misaraka amiko:
Ny ran''ny Zanakondry no mahay
Manavotra ahy mba ho an''ny Ray.}

{4.
Raha ketraka sy kivy re ny fo,
Velomiko indray ny hirako
Ny amin''i Jesosy sy ny ra-
Ka afaka ery ny tahotra.}
', '#1-VI-3', '#1') /end

insert into song values (12561, '256. Mivoaka ao Golgota', NULL, NULL, '
{1.
Mivoaka ao Golgota
Ilay renirano soa
Iantsoana ny mpanota
Ho any avokoa.
Ny ratran''ny Mpamonjy
No loharanon''io,
Ka ony mahavonjy
Sy tena mahadio.}

{2.
Mamelona ny tany
Ity ony soa ity,
Izay rehetra ombany
Miadana ery,
Vahoaka tapitrisa
Izay misotro ao
Mahita Paradisa
Sy fiainana vaovao.}

{3.
Ny fo izay maratra
Ka entina mankeo,
Ny ory sy mahantra
Izay manantona eo,
Mahita fanafody,
Ka sitrana avokoa,
Ny ota sy ny rofy
Dia afaka tokoa.}

{4.
Fanahy be ahina,
Avia misotro ao,
Fa io no mampangina
Ny alahelonao;
Fanahy o, hatony
Ity ony soa ity,
Fa io no mahatony
Ny faniriana aty.}

{5.
Ianao, ry Tompon''aina!
Isaoranay tokoa,
Nanaiky hiratraina
Handefa ony soa.
Ny rano tao Golgota
No iankinanay,
Fa mahadio ny ota,
Mamelona aina anay.}
', '#1-VI-3', '#1') /end

insert into song values (12571, '257. Ry Jesosy o! Indreto', NULL, NULL, '
{1.
Ry Jesosy o! Indreto,
Izahay nasainao eto
Amin''ny latabatrao,
Mba tolory mofon''aina;
O! omeo ranon''aina
Dia ny rà aman-tenanao.}

{2.
O! ry loharanon''aiko,
Tonga aho, fa ilaiko
Ianao Mpamonjiko,
Raiso ka diovy aho,
Atafio izay mitalaho
Ny fahamarinanao!}

{3.
Ry Mpiandry Tsara indrindra,
Taomy aho mba hifindra
Ao am-balan''ondrinao;
Mamonje ka mitahia
Ireto ondrinao nania,
Ento any aminao!}

{4.
O! ry Fahazavan''ny saiko
Sady tanjaky ny aiko,
Tonga aho, ka vonjeo
Noana ka mitaraina,
Mba vokiso mofon''aina,
Rano velona omeo!}

{5.
Indro, miankohoka aho,
Tompo o! Ka mitalaho
Eto anatrehanao;
Aza mba manary ahy,
Izay misento sy manahy,
Mamelà ny heloko!}

{6.
Ry Mpisolo ny mpanota,
Izay nandoa ny tambin''ota,
Latsa-dra ny tenanao,
Raisiko am-panajana,
Fa nataonao fanasana
Ireto zava-masinao.}
', '#1-VI-3', '#1') /end

insert into song values (12581, '258. Manaraka ny hafatrao', NULL, NULL, '
{1.
Manaraka ny hafatrao,
Tafangona izahay.
Mba hahatsiarovanay
Anao, ry Tomponay.}

{2.
Tsy azo adinoina
Ny fahorianao,
Ny dinitrao ''zay toy ny ra
Tsaroanay izao.}

{3.
Ny hazo-fijalianao
Dia tazanay izao:
Ry Zanak''ondry masina o!
Tsaroanay Ianao.}

{4.
Nijaly mafy Ianao
Nisolo voina anay:
Raha mbola velona izahay
Tsaroanay izao.}

{5.
Raha tapitra ny andronay,
Ka tonga Ianao
Mba haka ny fanahinay,
Tsarovinao ''zahay.}
', '#1-VI-3', '#1') /end

insert into song values (12591, '259. Ny latabatrao, ry Jeso!', NULL, NULL, '
{1.
Ny latabatrao, ry Jeso !
Dia atrehinay izao ;
Ny fanahinay vokiso
Amin''ny fitianao.}

{2.
Tsy nitady zo sy laza
Ianao, ry Tomponay!
Foinao ny ainao aza
Nanavotanao anay}

{3.
Jeso o !  ny ranao soa
No nataonao avotray,
Mba hanafaka avokoa
''zay tsy mahadio anay}

{4.
Manomeza mofon''aina
Tompo o ! ho tanjakay ;
Mihainoa ny mitaraina,
Mamalia ny hatakay}

{5.
Ny fanahinay, ry Tompo !
Raiso, fa atolotray ;
Faly izahay hanompo
Sy  hanao ny sitrakao.}
', '#1-VI-3', '#1') /end

insert into song values (12601, '260. Ry Kristy o, malalako', NULL, NULL, '
{1.
Ry Kristy o, malalako,
Sakafon''ny fanahiko,
Manaraka ny hafatra,
Tsarovako ny tenanao.}

{2.
Mangorakoraka ny fo,
Fa voavono Ianao!
Ka saotra no atolotray
Anao, ry Ilay nalatsa-dra.}

{3.
Ny tenanao ''zay raisiko
Fiainako vaovao tokoa;
Ny ranao ''zay sotroiko
No manadio ny foko koa.}

{4.
Ry Kristy o! Ampitomboy
Ny fitiavako Anao,
Ny toe-piainako fenoy,
Ny tena hamasinanao.}

{5.
Atolotro anio indray
Mba ho Anao mandrakizay
Ny tenako, ny ananako,
Ny heriko, ny asako.}
', '#1-VI-3', '#1') /end

insert into song values (12611, '261. Ankehitrio no isehoanao', NULL, NULL, '
{1.
Ankehitrio no isehoanao,
Ry Jeso Tompo o, izay malalanay!
Ankehitrio no anandramanay
Ny hafaliamben''ny lanitrao.}

{2.
Ampiomany ao am-po ''zahay
Hampahadio ny eritreritray,
Hahatakaranay ny avotrao
Izay nambaranao tao Kalvary.}

{3.
Eto no anolorano anay
Ny mofon''aina, rano velona ;
Eto no ialanay sasatra
Amin''ny adinay sy rafinay.}

{4.
Ry Jeso o ! miandry izahay
Ny fihavian''ny voninahitrao,
Ho toy ny andro vao miposaka,
Ho toy ny rano mahavelona.}

{5.
"Raiso ny tenako" no teninao,
"Ary ny rako ho fiainana"
Am-pitiavana no andraisanay,
Na dia tsy mendrika izahay.}

{6.
Mombà anay, ry Tompo tia o !
Aza miala amin''ny fonay,
Aoka ny tavanao hazava re
Amin''ny lalana alehanay.}
', '#1-VI-3', '#1') /end

insert into song values (12621, '262. Indreto izahay', NULL, NULL, '
{1.
Indreto izahay,
Ry Jeso o!
Manaraka izao
Ny hafatrao,
Ny fahorianao
Tsaroanay izao,
Fa fatratra tokoa
Ny nentinao.}

{2.
Ny mofo haninay,
Ry Jeso o!
Milaza aminay
Ny tenanao;
Ny fanavotanao
Mahafaka anay,
Ka saotra re izao
Atolotray.}

{3.
Misotro ny divay
''Zahay izao
Hahatsiarovanay
Ny antranao;
Ny ra nilatsaka
No manadio anay,
Manafaka tokoa
Ny olonao.}

{4.
Tarihonao ny fo,
Ry Jeso o!
Ho tia sy hankato
Ny didinao;
Ka tsy ny sitrakay,
Fa ny Anao, Jeso,
No aoka ho maminay,
Ry Tompo o!}
', '#1-VI-3', '#1') /end

insert into song values (12631, '263. Mandrenesa, ry sofina', NULL, NULL, '
{1.
Mandrenesa, ry sofina,
Mifalia, ry fo,
Ny feon''ny Filazantsara
Manatona anao.}

{2.
Ry olon''ota rehetra,
Ry ory sy noana,
''Zay maneno ny kibony
Amin-java-poana.}

{3.
Jehovah no mamboatra
Izay mofon''aina,
Ho an''izay rehetra
Mety hihinana.}

{4.
Kristy sy ny fitahiany
Izany mofo izany,
Fa Izy no mamelona
Ny maty mosary}

{5.
Avia, ry mosarena,
Ka manaràna fo;
Ny mofon''aina tsy lany
Fa ampy ho anao!}
', '#1-VI-3', '#1') /end

insert into song values (12641, '264. He! Mahagaga anay', NULL, NULL, '
{1.
He! Mahagaga anay
Ny fitiavana
Nasehonao, ry Jeso o!
Mba ho anay.}

{2.
Ny fijalianao
Ilay tao Golgota,
Indrisy! Lalina tokoa
Ny ngidiny.}

{3.
Na mafy aza izay,
Tsy nadosiranao,
Nataonao tsinontsinona
Ny tenanao.}

{4.
Ny fitiavanao
Fa tsy ny fantsika,
No anton''ny nihantonanao
Tao Golgota.}

{5.
Ny ra nalatsakao
No vidin''ny ainay.
Ny fijaliana nentinao
No avotray.}

{6.
Tsaroanay izao
Derainay avokoa
''Lay fitavanao anay,
Ry Tompo soa!}
', '#1-VI-3', '#1') /end

insert into song values (12651, '265. Ny fitiavanao', NULL, NULL, '
{1.
Ny fitiavanao,
No mahagaga anay,
Ry Zanak''Andriamanitra o!
Ry Jeso Tomponay!}

{2.
Hanavotra aina anay
No nijalianao,
Ka famonjena lehibe
No azonay izao.}

{3.
Ny mofo sy divay,
''Zay haninay izao,
Dia mampiseho aminay
Ny fitiavanao.}

{4.
Avia, Jeso o!
Avia ankehitrio,
Ka ny fanahinay sasao
Ho tsara sy madio.}

{5.
Mampaneke anay
Hanao ny sitrakao,
Koa aoka tsy ho tianay
Izay tsy tianao.}

{6.
Monena aminay,
Ry Tompo be fitia!
Ka aza ilaozanao ''zahay
Mandrakizay doria.}
', '#1-VI-3', '#1') /end

insert into song values (12661, '266. Ry Jeso Tompo, Ianao', NULL, NULL, '
{1.
Ry Jeso Tompo, Ianao
Isaorako tokoa izao,
Ny ranao sy ny tenanao
No nihinanako izao.}

{2.
Ry foko faly, ambarao
Ny soa azonao izao:
Jesosy monina ao am-po,
Endrey ny hafaliako!}
', '#1-VI-3', '#1') /end

insert into song values (12671, '267. He, ny adon''ny maraina', NULL, NULL, '
{1.
He, ny adon''ny maraina
Latsaka ao an-tsaha re,
Ka mamelona ny maina,
''zay maniry rano be!
Ravoravo e, ny tany,
Fa he, ny andro maina efa lany,
Ka mitsahatra ny fahantrany,
Manaroka ny hazo be!}

{2.
Ny fanahy karankaina
Misy rano velona,
Fa Jesosy andon''aina
Manome fiainana:
Velona ny jentilisa,
Ny tany hay dia tonga Paradisa;
Marobe no efa voaisa
Handray ny lova masina!}

{3.
Misy lalana mizotra,
''zay mankao Ziona re;
Any no mitanjozotra
Ny vahiny marobe;
Ka Ziona dia mivoha
Hidiran''ny navotan''ny Tsitoha;
Ao no ivorian''ny nifoha,
Fa an''ny Tompo lehibe.}

{4.
He, mikambana ny mino
Amim-pifaliam-be,
Ka ny hobiny maneno
Tahaka ny rano be!
Ao mitovy avokoa,
Tsy misy tompo sy mpanompo koa,
Eny, razana iray tokoa
Ny mponina ao Ziona, re!}
', '#1-VI-4', '#1') /end

insert into song values (12681, '268. Jesosy no Mpiandry', NULL, NULL, '
{1.
Jesosy no Mpiandry
Ny fiangonanay;
Ny feon''Izy irery
No feo fantatray,
Ka tsy mba vala maro
Ny fiangonanay;
Iray tsy hay zaraina,
Tapahin-Tompo iray.}

{2.
Iray no Vatolampy
''zay iorenanay,
Iray no Fehizoro
''zay ikambananay,
Iray no mofon''aina
Fihinanay ety,
Iray no antenaina
Ho azonay ary.}

{3.
Tabain''ny hevi-diso,
Kanefa mbola iray;
Ny saina tsy mitovy
Tsy mahazara anay;
Fisoko maromaro,
Kanefa vala iray;
Ny Tompo no mpiaro
Ny fiangonanay.}

{4.
Ny teny samy hafa,
Ny fomba tsy miray,
Tsy misy maharava
Ny fikambananay;
Ny fanenjehana aza
Vao mainka mampiray
Sy mahatsara laza
Ny fiangonanay.}

{5.
Ry Jeso! Hatanjaho
Ny fiangonanao,
Ny fony halalaho
Mba hitoeranao;
Izay firaisan-tsaina
Miseho hatrety,
Velomy hanana aina
Hitory hatrary.}
', '#1-VI-4', '#1') /end

insert into song values (12691, '269. He! Tianay ny monina eto', NULL, NULL, '
{1.
He! Tianay ny monina eto,
Fa maminay ny valanao.
Tsy tianay fa mampisento
Ny lavitra ny tranonao.}

{FIV.
He! Tianay ny monina eto,
Fa maminay ny valanao.
Tsy tianay fa mampisento
Ny lavitra ny tranonao.}

{2.
He! Tianay, fa misy aina,
Ry Tomponay! Ny valanao!
Tsy tianay, fa tany maina
Ny lavitra ny tranonao.
[FIV.]}

{3.
Fehezinao, Mpiandry tia!
Milamina ny valanao;
Tsy misy tsy mifankatia
Ny ondry izay tarihinao.
[FIV.]}

{4.
Ny fanimban''ny fahavalo
Dia lavitra ny valanao;
Mitazan-davitra mandalo
Ny rafinao tsy tia Anao.
[FIV.]}

{5.
Ny fototra ''zay mahatsara
Ny valanao, ry Tomponay!
Dia Ianao Mpiandry tsara
No miara-monina aminay.
[FIV.]}
', '#1-VI-4', '#1') /end

insert into song values (12701, '270. Ry Tompo, Izay nanangona', NULL, NULL, '
{1.
Ry Tompo, Izay nanangona
Anay ho vato velona!
Hazony tsy handao Anao
Ny vato voarafitrao;
Fa maro no mandronjina
Handevona.}

{2.
Jesosy Fehizoro o,
Mpifehy mora sady to!
Tohano mafy izahay
Mba tsy hirodana indray,
Fa indro ny mpisompatra
Mandrirotra.}

{3.
Ry vato fampiraisana!
Tariho mba hikambana
Ny vato voapaika re,
Ho tonga trano lehibe,
Tsy zakan''ny mpandevona
Harodana.}

{4.
Ny Kristiana marobe
Mba ampiraiso tsara re,
Hiray am-pitiavana,
Ho velona finoana,
Ka mba hiaraka hanao
Ny asanao.}
', '#1-VI-4', '#1') /end

insert into song values (12711, '271. Mba mivavaha raha vao maraina', NULL, NULL, '
{1.
Mba mivavaha raha vao maraina,
Tsarovinao ilay Mpanao anao
Izay miaro ny fanahy sy aina
Mba ho salama toy izao;}

{FIV.
Tsarovy re ny Tomponao
''zay Mpiaro anao.}

{2.
Rehefa tonga koa ny antoandro
O! miambena tsara hianao,
Fa manangoly saina isan''andro
Ilay devoly rafinao:
[FIV.]}

{3.
Raha alina mangina, ka hatory,
Tsarovy eo ny ota izay natao,
Ka dia ifony mafy sy itsory
Fandrao maningotra anao:
[FIV.]}

{4.
Na aiza na aiza hianao mitoetra
Satana fahavalo ao tokoa,
Ka aza kivy na mba manjoretra,
Fa mivavaha tsy ho voa:
[FIV.]}
', '#1-VI-4', '#1') /end

insert into song values (12721, '272. Faly izahay mandre', NULL, NULL, '
{1.
Faly izahay mandre
Havan-tia nanao hoe:
"Miaraha aminay,
Trano soa halehanay."}

{2.
Efa tonga izahay
Mba hivavaka aminao;
Ry Jehovah Tomponay,
Mitahia ny olonao!}

{3.
Miandrandra Anao ''zahay,
Ka mangataka aminao
Hanomezanao anay
Soa avy aminao.}

{4.
Dia sambatra izahay,
Fa mitoky aminao;
Ka ho tanterahinay
''Zay rehetra sitrakao.}

{5.
Hoderainay Ianao
Andro amana alina,
Mbamin''ny Anaranao,
Foto-piadanana.}
', '#1-VI-4', '#1') /end

insert into song values (12731, '273. He! Vokatra ny tany', NULL, NULL, '
{1.
He! Vokatra ny tany,
Jehovah Tompo o!
Fa be tsy mety lany
Ny hasoavanao:
Ny andro fararano
No ifalianay;
Voataona ao an-trano
Ny vokatra azonay:}

{FIV.
Saotra tsara entinay
Ho Anao tokoa.
Ny olonao rehetra izao
Mifaly avokoa.}

{2.
Ny tany tsy misaona,
Fa sambatra tokoa,
Tsy tapaka isan-taona
Ny fanomezan-tsoa;
Endrey, nahazo maina
Izao rehetra izao;
Ny zava-manana aina
Mifaly aminao:
[FIV.]}

{3.
Tontosa izay nirina,
Nahazo izahay,
Fa misy taona zina
Nataonao ho anay;
Tsy diso isan-taona
Ny fitahianao,
Ny olonao mihaona
Mba hankalaza Anao:
[FIV.]}
', '#1-VI-5', '#1') /end

insert into song values (12741, '274. Tao an-tany karankaina', NULL, NULL, '
{1.
Tao an-tany karankaina
Nisy olo-marobe,
Tsy mba nampodina maina,
Tao Jesosy nanome.}

{2.
Na dia mofo kely aza
Tonga nahavoky ireo,
Fa nitombo mahagaga,
Samy faly izy ireo.}

{3.
Fa ny herinao, ry Jeso,
Izay namonjy fahizay
No miasa lalandava
Hahavoky soa anay.}

{4.
Dia tahio ny fambolena
Hahavokatra tokoa;
Miantrà ny firenena
Tsy ho diso zava-tsoa.}

{5.
Na dia voky fatratra aza,
Mora reraka izahay;
Ao ambony ny mateza
Iankinan''ny ainay.}
', '#1-VI-5', '#1') /end

insert into song values (12751, '275. Raisonao ry Tomposoa', NULL, NULL, '
{1.
Raisonao ry Tomposoa,
Ny fanatitray izao;
Hamasino avokoa
''Zay atolotray izao;}

{FIV.
:,: Raiso re, ho Anao,
Ho fanati-tsitrakao! :,:}

{2.
He, Anao, ry Tompo soa
''Zay rehetra anananay
Ny harena, vola koa,
Taminao, Jehovah Ray.
[FIV.]}

{3.
Na dia kely aza, Tompo,
Ka tsy mendrika Anao
Raiso ho entina hanompo
Sy hanao ny sitrakao.
[FIV.]}

{4.
Indro, Tompo, raiso koa
Tena, fo, fanahinay,
Mba ho fiasana koa
Eo an-tànanao izahay.
[FIV.]}
', '#1-VI-5', '#1') /end

insert into song values (12761, '276. Ry Jehovah Ray Mpahary', NULL, NULL, '
{1.
Ry Jehovah Ray Mpahary,
Sy Mpamelona anay,
Tsapanay fa tsy manary
Ianao Mpitondra anay;
He, mandrotsa-pitahiana
Sesehena Ianao;
Ka dia feno fifaliana
Izahay mpanomponao.}

{2.
Noteneninao ny tany,
Notarihinao izahay,
Ka nitobaka aoka izany
Zao vokatra azonay.
Feno hika isan-taona
Ny fitiavanao anay,
Ka aoreno tsy ho foana,
Tompo o! Ny asanay.}

{3.
O! mba raiso re, ry Tompo,
''Zao fanatitray izao;
Enga anie ka mba hitombo,
Handrosoan''ny hanitrao.
Raiso koa sy hamasino,
Ny fonay hiasanao;
Ny Fanahy ampidino
Hampahery, hanavao.}
', '#1-VI-5', '#1') /end

insert into song values (12771, '277. Indro, Tompo vory eto', NULL, NULL, '
{1.
Indro, Tompo vory eto
Anatrehanao izahay,
Mba  hanatitra ireto
Vokatry ny asanay,}

{FIV.
:,: Raiso re, raiso re!
Ho fanati-tsitrakao. :,:}

{2.
Ianao no mampaniry
Sy mampisy vokatra;
Izahay dia mpitahiry,
Fa Anao ny zavatra.
[FIV.]}

{3.
Ary na tsy mendrika aza,
Aoka tsy holavinao,
Fa ho enti-mankalaza
Ny anaran-tsoanao.
[FIV.]}

{4.
Raiso koa re, ry Tompo,
Ho anao ny tenanay,
Amboary ho mpanompo
Mahafatra-po izahay,
[FIV.]}
', '#1-VI-5', '#1') /end

insert into song values (12781, '278. Mba ampio, ry Tomposoa', NULL, NULL, '
{1.
Mba ampio, ry Tomposoa
Izahay navotanao
Fa mikasa mba hanoa
Sy hanaraka Anao;}

{FIV.
Hitanao fa kely hery,
Izahay mpanomponao,
Tsy hefa raha irery,
Koa mba tarihonao.}

{2.
Izahay manolo-tena
Mba hiasa ho Anao,
Ka maniry, manantena
Fa hanampy Ianao;
[FIV.]}

{3.
Tianay hiara-dia
Aminao rahatrizay
Ka tantano tsy hania
Amin''ny alehanay,
[FIV.]}
', '#1-VI-5', '#1') /end

insert into song values (12791, '279. Raisonao, ry Tomposoa', NULL, NULL, '
{1.
Raisonao, ry Tomposoa,
Izato asan-tananay;
Fa nomenao avokoa
Izay rehetra anananay.
Simba izahay rehetra,
Sady very hatrizay,
Nefa soa tsy misy fetra
No navalinao anay.}

{2.
Ianao nanetry tena,
Tsy nitandro henatra,
Be fiantra ka onena
Ny tra-pahoriana.
Mba tsarovanay izany,
Koa dia tahafinay,
Nefa fiasana ihany
Ao an-tananao izahay.}

{3.
Arosoy sy ampitaro
Aminay ny teninao;
Hazavao ny olo-maro
Izay tsy mahalala Anao!
Ny misaona mba vangio,
Ny kamboty iantrao,
Izahay izay tonga anio
Mba ataovy irakao.}

{4.
Eny, ry Mpamonjy tsara,
Amboary izahay
Mba ho tonga mpanambara
Ny fitianao anay;
Ary dia mba tahio
Izato fikambananay,
Hafanao, ampirisiho,
Hamasino izahay.}
', '#1-VI-5', '#1') /end

insert into song values (12801, '280. Ry Tompom-boninahitra', NULL, NULL, '
{1.
Ry Tompom-boninahitra,
Jereonao izao
Ireto izay milahatra
Ho tena irakao;
Ny fanasana fatratra
Omaninao izao,
Ilàm-pitondra hafatra
Hamory izay mankao.}

{2.
Hitety sampan-dàlana
Sy vohitra izahay,
Hanetra fisakanana
Sy ady mahamay;
Ka aoka hotafiana
Ny fiadianao,
Fa tena fanafihana
Izay hatao izao.}

{3.
O, ry Fanahy Masina,
Monena aminay!
Ka aoka ho fanasina
Tokoa izahay.
Fihino tsy hihemotra,
Ataovy irakao
Manefa zava-tsarotra
Ho voninahitrao}

{4.
Ry Tompom-boninahitra!
Ny voninahitrao
No aoka mba hitaratra
An-tavanay izao.
Ka raha ao an-danitra
Fonena-masinao,
Dia mainka vao hamiratra
Ho voninahitrao.}
', '#1-VI-6', '#1') /end

insert into song values (12811, '281. Vory eto izahay', NULL, NULL, '
{1.
Vory eto izahay,
Hihevitra ny asanao.
Tompo, manatreha anay,
Ka asehoy ny sitrakao.
Aoka Ianao no mba hitarikanay,
Ary ny Fanahinao hamalona aina anay}

{FIV.
Tompo, tahio izahay,
Mba ho voninahitrao ny asa vitanay.}

{2.
Hahavita inona
Ny saina aman-kevitray?
Tena tsinontsinona.
Ry Tompo o! Ny herinay.
Koa mba midina eto afovoanay
Hanana aina masina ny fivorianay!
[FIV.]}

{3.
Ianao no nanome
Ny asa hoheverinay,
Tena adidy lehibe
No voatolotra anay.
Enga anie ny sitrakao irery no hatao!
Aoka tsy ho vanona izay tsy sitrakao!
[FIV.]}

{4.
Areheto aminay
Ny afom-pitiavanao,
Mba hahaizanay miray
Mitory ny Anaranao,
Hahavory marobe hanatona Anao,
Hampamirapiratra ny voninahitrao!
[FIV.]}
', '#1-VI-6', '#1') /end

insert into song values (12821, '282. Ry Jeso, Tompo velona', NULL, NULL, '
(Manaraka ny famankian-teny voalohany)

{1.
Ry Jeso, Tompo velona,
mpiandry sy Mpamelona
Ny fiangona-masinao,
Atreho izahay izao.}

{2.
Indreo*(inty) mpanomponao vaovao
Atokanay, ka rasionao
Hanao ny raharahanao,
Hamahana ny ondrinao.}

(Manaraka ny famankian-teny faharoa)

{3.
Ny tany no nakana anay,
Mihelina ny andronay,
Ka vetivety izahay
Dia hody vovoka indray.}

{4.
Ry Jeso o! Mpitia anay,
Izay Mpanjaka hatrizay,
Endrey ny voninahitrao,
Manerana ny lapanao.}

{5.
Ny deranao asandratra
Hitohy tsy hitsahatra;
Hazava toy ny kintana
Ny tsara fanompoana.}

(Manaraka ny famankian-teny fahatelo)

{6.
Mba raiso ny mpanomponao,
Ka aoka hamboarinao
Ho mpanambara mankato
Ny hafatrao, ry Tompo o!}

{7.
Omeo afo masina
Ny vava mampionona;
Ny sainao sy ny feonao
Omeo ny mpanomponao.}

(Manaraka ny famankian-teny fahaefatra)

{8.
Raha misy tafio-drivotra,
Tohano tsy hatahotra,
Hahery sy hisakana
Ny rafi-piangonana.}

{9.
Ka mba ampirisihonao
Hanaraka ny dianao;
Tantano raha mbola aty,
Handresy ka ho tonga ary.}

(Manaraka ny famankian-teny fahadinmy)

{10.
Mba mamindra fo aminy,
Fa kely ny finoany;
Ny Ranao masina anie
Hanafaka ny helony!}

{11.
Ampio mba haharitra,
Raha mbola aty an''efitra
Raha avy ny hodiany,
Mba hazavao ny lalany!}

{12.
Raha tonga ny harivany,
Ka ao ny ady farany,
Dia raiso ny mpanomponao
Hiala sasatra aminao.}
', '#1-VI-6', '#1') /end

insert into song values (12831, '283. Manan-jara ry Jesosy!', NULL, NULL, '
{1.
Manan-jara ry Jesosy!
Ny mpanomponao,
''zay natokanao hitondra
Ny Anaranao;
Nofidinao ho mpitory
Ny fitiavanao
Nitokianao ho mpamory
Ny vahoakanao.}

{2.
Izao no zara-fanompoako
Voatendrinao,
Ka ny mafy tsy ahoako
Hahefako izao;
Tsy handosi-pahoriana
Ny mpiasanao,
Fa ny hazo fijaliana
Lova taminao.}

{3.
Na ho voky na ho noana
Ny mpanomponao,
Tsy handao ny fanompoana
Nametrahano;
Sitrakao ny fihafiana,
Ka mba tiako koa;
Zakany ny tsy fisiana,
Ka zakaiko koa.}

{4.
Tsy ahiko ny handany,
Holanina koa,
Fa famoizan-tsoa no hany
Ahefana soa;
Na ny saiko na ny aiko,
Andro, vola koa,
Tsy mba misy holalaiko,
Foiko avokoa.}

{5.
Nefa raha izaho irery
Tsy mahefa izao,
Ka mangataka ny hery
Eo an-tananao,
Aoka ho feno ny Fanahy
Ny mpanomponao
Mba ho tena lehilahy/vehivavy
Voahosotrao.}
', '#1-VI-6', '#1') /end

insert into song values (12841, '284. Mangataka aminao ny olonao, ry Tompo!', NULL, NULL, '
{1.
Mangataka aminao ny olonao, ry Tompo!
Mba hitahianao ny havantianay.
Izay atokanay hitondra sy hanompo
Ny Fiangonanao; henoy ny vavakay.}

{2.
Tafio ny herinao, fa tsy mahefa irery,
Fenoy ny hevitrao ho toy ny zanakao;
Mpitady ny mania, mpikaroka ny very,
Hitondra ireo ho tia ny valan''ondrinao.}

{3.
Ny sainy hazavao hahazo fahendrena
Hahay hitondra izao vahoaka be izao;
Ampianaro koa mba tsy hatoky tena,
Tolory saina soa hiankina Aminao.}

{4.
Raha ory, aza afoy, tohano sy onono;
Marary mba tsaboy, mahantra mba jereo,
Apetraka aminao ny ainy sy ny fony,
Ka mba tsinjovinao, arovy sy vonjeo.}
', '#1-VI-6', '#1') /end

insert into song values (12851, '285. Mivory maro eto ''Zahay navotanao', NULL, NULL, '
{1.
Mivory maro eto
''Zahay navotanao.
Ny saotranay no ento
Ho voninahitrao.
Mifaly sy mihoby
Mandray ny irakao;
Ka eto no mitoby
Mangataka aminao.}

{2.
Ry Kristy tena Andry
Sy Vatolampinay!
Ry Lohan''ny mpiandry
Sy Tompo tokinay!
Tohano mba hijoro
Ity mpanomponao,
Tinendry ho mpanoro,
Mpilaza hafatrao.}

{3.
Handray ny asa mafy,
Koa aza ilaozanao;
Hitory, ho mpamafy,
Ka dia mba hazavao.
Vokiso lalandava
Ny saina tianao;
Omeo ny andro lava
Hanao ny asanao.}

{4.
Tahio mba ho salama
Hahefa ny Anao,
Ho iraka mafana
Ampian''ny herinao;
Mandrava manda mafy
Mba hampanjaka Anao,
Koa aoka mba hihafy,
Fa izay no sitrakao.}

{5.
Ny fitaizam-panahy,
Adidy lehibe,
Ampio mba tsy hanahy
Ny asa maro be;
Ka dia tahio ny asa
Hataony ho Anao;
Ny fony no mikasa
Hanoa ny didinao.}
', '#1-VI-6', '#1') /end

insert into song values (12861, '286. Ry Jeso Mpitarika hendry', NULL, NULL, '
{1.
Ry Jeso Mpitarika hendry!
Tsijovy ny olom-boatendry;
Tolory ''zay hevitra hendry
Handinika sy hahakendry.
Ry Tompo Mpanjaka mahery;
Vangio raha ory sy tery,
Tohano raha mahatsiaro fery
Vimbino ndrao saraka irery.}

{2.
Raha miasa dia miasa ny saina.
Kanefa ny tany no maina,
Ka mila ho kivy ny aina,
Tondray fandrao karankaina,
Ny mpiasa izay voafidy
Hamoha ny fo ''zay mihidy,
Hirari-panahy saro-bidy,
Tantano hanaraka didy.}
', '#1-VI-6', '#1') /end

insert into song values (12871, '287. O! miaingà izao, ry miaramila soa', NULL, NULL, '
{1.
O! miaingà izao, ry miaramila soa,
Antsoin''ny Tomponao hiady hianao.
Ny fahavalo ao kenefa mandrosoa,
:,: Mahery re no momba anao. :,:}

{2.
Izao tontolo izao izay feno fahotana
No iantsoana anao mba hotafihinao;
Indreo mihoraka ao ny fehin''i Satana,
:,: Jeso anefa momba anao. :,:}

{3.
Ny aizim-be izay mandrakotra ny tany
No iantsoana anao, ka ndeha mba savao;
Ny Tompo no tafio, alao ny hazavany,
:,: Diovy izao tontolo izao. :,:}

{4.
Ny fahoriam-be izay manarona eto,
No iantsoana anao, ka moa ve handà?
Ampiononinao ny ory sy misento,
:,: O! mazotoa, ka miaingà! :,:}

{5.
Na inona hanjo, mbola ao ny famonjena;
Hahery hianao! Izay no no antenao;
Ampoizonao tokoa izao ny fandresena,
:,: Mahery Ilay arahinao. :,:}

{6.
O! mahereza izao, ry miaramila soa!
Ny hatrotrahanao iareto sy zakao,
Jesosy no jereo, ka matokia tokoa,
:,: Fa Izy re no herinao. :,:}
', '#1-VI-6', '#1') /end

insert into song values (12881, '288. Sambatra izay voafidy', NULL, NULL, '
{1.
Sambatra izay voafidy
Ho mpiasanao,
Ka hanohy ireo adidy
''Zay natombokao.
Ho mpitaiza ny fanahy
Toy ny vitanao
Ho mpiady tena sahy
Tarikao Jeso.}

{2.
Dia mpiasa tsy an-tery
Ka mafana fo,
Te-hanangona ondry very
Marobe tokoa,
Te-hitaona ny tanora
Sy ny zaza koa,
Tsy hifidy asa mora,
Fa ho mpakato.}

{3.
Ianao, ry Jeso Tompo,
No nampianatra
Ny olonao mba hanompo
Tsy mitsahatra,
Tsy handala ny talenta
''zay natolotrao;
Fa ny aina re no fetra
Entina manao.}

{4.
Nefa tsy matoky tena
Fa malemy moa,
Ka dia mila fahendrena
Mila hery koa;
Ho mpiasa mahavita
''Zay anjara ety,
Mandra-pody ao am-pita Aminao ary}
', '#1-VI-6', '#1') /end

insert into song values (12891, '289. Hitsangana izao', NULL, NULL, '
{1.
Hitsangana izao
''Zahay, ry Tompo o!
Mba hanomponay Anao
Ampionao izahay.}

{2.
Ny fahotana be,
Izay vitanay taty,
Dia ibebahanay izao.
Ka mamelà anay.}

{3.
Ny tsy finoanay
No nijanonanay
Tsy nanatonanay Anao
Izay ela be izay.}

{4.
Sasao madio izahay
Asio fo vaovao
Ho ao anatinay izao
Ry Jeso Avotray!}

{5.
Inoanay tokoa
Fa maty Ianao,
Hisolo olo-meloka
No nijalianao.}

{6.
Mba manampia anay
Hiankina aminao
Fa tokinay ny avotrao
Natao tao Kalvary.}
', '#1-VI-7', '#1') /end

insert into song values (12901, '290. Indreo fa mivory izao', NULL, NULL, '
{1.
Indreo fa mivory izao
Zatovo vonon-kanavao
Ilay fanekena tsara;
Mba raisonao, ry Rainay o!
Ny fitarainanay izao,
Omeo toky tsara;
Henoy, fenoy
Aimpanahy mba ho sahy
lalandava
Ho mpanaiky ny mazava.}

{2.
Mandà ny ratsy izy izao,
Manaiky fa ho zanakao
Handia ny lalan''aina;
Ry Ray o! Mba tsarovinao
Ireto voasoratrao
Ao amim-bokin''aina;
Tezao izao
Tsy ho resy, fa handresy,
Ka omeo
Azy hery mahaleo.}

{3.
Nantsanganao ho zanakao
Sy ondry mba  handrasanao,
Ka tena sarobidy;
Arovy re ny ondrinao,
Tantano ny malalanao,
Ry Tompo Izay nifidy
Anay handray
Lova soa tsy miova,
Ka hazony
Tsy handao Anao intsony!}
', '#1-VI-7', '#1') /end

insert into song values (12911, '291. Efa tonga zana-dRay Tsitoha hianao', NULL, NULL, '
{1.
Efa tonga zana-dRay Tsitoha hianao,
Ry tanora, tamin''ny batisa masina;
Izany no tenin''ny Tompo.}

{2.
Koa aza manadino ny batisanao,
Zava-tsoa sarobidy efa azonao;;
Tamin''ny ranon''aina.}

{3.
Famelana heloka sy fahasoavana
Fanavaozana ny fo sy fiadanana;
Samy nom''ny Mpamonjy.}

{4.
Dia arahonao Jesosy, fa
Mpamonjinao;
Aoka hahafaly Azy ny fiainanao;
Tovy izay Sitrapony!}

{5.
Mivavaha, miambena ao an-dàlana!
Misy fahavalo fetsy ta-hamitaka
Ny ondrikelin''ny Tompo.}

{6.
Mahatoky ny Mpamonjy, aza kivy fo,
Izy hampionona, raha avy ny manjo;
Azy ny hery rehetra.}

{7.
Ary raha tapitra ny andronao aty
Endrey ny fifalianao rehefa mby ary;
Amin''ny Tompo Jesosy!}
', '#1-VI-7', '#1') /end

insert into song values (12921, '292. Izahay natao batisa', NULL, NULL, '
{1.
Izahay natao batisa,
Voarain''ny Tomponay,
Ary efa voaisa
Mba ho zanaky ny Ray;
Voavelany tokoa
ny fahadisonay
Nefa mbola mora resy
Mora simba izahay.}

{2.
Na ny asa, na ny teny,
Na ny hevitra ao am-po,
Mbola tsy madio ireny,
Ka diovy, Tompo o!
Mora aminay ny ratsy,
Sarotra aminay ny to;
Mba ampio tsy ho resy
Izahay, ry Tompo o!}

{3.
Raha mbola aty an-tany
Izahay izay zanakao,
Mba ampio tsy ho lany
Ny finoanay Anao.
Ry Jesosy Tompo soa,
Tano mafy izahay,
Mba ho azonay tokoa
Izay anjara lovanay.}
', '#1-VI-7', '#1') /end

insert into song values (12931, '293. Izaho nentina nanatrika', NULL, NULL, '
{1.
Izaho nentina nanatrika
Ny Tompo sy ny fiangonana;
Ny ray sy reny niantoka
Hitaiza sy hitari-dalana.}

{2.
Ka faly aho, fa noraisina
Natao batisa mbola zaza re,
Nanaovam-panekena masina
Ho zanaky ny Tompo lehibe.}
', '#1-VI-7', '#1') /end

insert into song values (12941, '294. Ampio aho, Zanahary o', NULL, NULL, '
{1.
Ampio aho, Zanahary o,
Hanaraka ny "eny" masina.
Izay nataoko taminao, ry To,
Ka tsy mba hisy hivadihana.}

{2.
Ry Tompoko, Izay nanomboka
Ny asa tsara ao anatiko,
Arovy aho ho tanteraka,
Mba tsy ho faty ny finoako.}
', '#1-VI-7', '#1') /end

insert into song values (12951, '295. Miorena mafy tsara', NULL, NULL, '
{1.
Miorena mafy tsara
Amin''ilay vatosoa
Mandrosoa, mifahara,
Mifikira mafy koa.
He! Ny Tompo manambara
Fa ho sambatra tokoa,
Ka ho tsara miafara
Ny mpanompo mahasoa.}

{2.
Raha misy fahoriana
''Zay midona aminao,
Raiso re ho fitahiana
''Zay omen''ny Tomponao;
Fa ny lalam-pijaliana
''Zay alehanao izao
Dia mamelom-paniriana
Hitomboan''ny herinao.}

{3.
Miadia ny ady tsara,
Ho mpandresy hianao;
Tontosay izay anjara
Eo am-pelantananao;
Jeso Tompo no mijery
Sy mahita ny atao,
Koa tsy mba hisy very
''Zay rehetra vitanao.}

{4.
Ry Jesosy! hatanjaho
''Ty mpanolo-tenananao,
Miangavy, mitalaho,
Ka iteneno, Tompo o!
Aoka re ho tena taho
''Zahy mikambana aminao,
Ka ny foko halalaho,
Diovy ho Tempolinao.}
', '#1-VI-7', '#1') /end

insert into song values (12961, '296. Ankehitry,Jereo, ry Ray', NULL, NULL, '
{1.
Ankehitry,
Jereo, ry Ray!
Ny mpivahiny,
Miampy indray;
Ka manomeza hery
Ny fiangonanao,
Mba tsy hiasa irery
Ny olonao.}

{2.
Avia hitahy
Ny olonao;
Ka ny Fanahy
Omeonao;
Ho hery izay handrava
Ny toetra ratsinay,
Ho jiro hanazava
Ny làlana.}

{3.
Ry Tompo tia!
Tarihonao
Hifankatia
Ny olonao,
Mba tsy ho rava intsony
Ny fikambananay,
Hamantana ao ambony
Ny dianay.}
', '#1-VI-7', '#1') /end

insert into song values (12971, '297. Ry havanay malala!', NULL, NULL, '
{1.
Ry havanay malala!
Manolo-tena izao.
Fotoana mahafaly
Iraisan-tsika anio;
:,: Faly ''zahay mandray anao, :,:
Hira-manompo ny Ray,
:,: Jeso anie mandray anao! :,:
Mba ho sakaizany tokoa.}

{2.
Hajao ny fenekena,
Adidinao efao,
Ataovy tsy terena
Izay andraikitrao.
:,: Jereo Jeso ka matokia, :,:
Fa Izy no hiahy anao.
:,: Ka mandrosoa sy mifalia, :,:
Hanefa izay anjaranao.}
', '#1-VI-7', '#1') /end

insert into song values (12981, '298. Aina no fetra ry Tompo Mpanjaka', NULL, NULL, '
{1.
Aina no fetra ry Tompo Mpanjaka,
Aina no fetra anarahana anao,
Ka ny faneva tsy maintsy ho zaka,
Ny fanompoana tsy maintsy atao.}

{FIV.
Indreto izahay, hilanja faneva,
''lay hazo izay nanavotana anay;
An-tsitrapo no anoloranay tena,
Ka tano mba ho Anao izahay.}

{2.
Aina no fetra ho enti-manoa
''lay Zanak''ondry nisolo anay;
Fandavan-tena sy fanaovan-tsoa
No fianarana atolotra anay.
[FIV.]}

{3.
Aina no fetra, ry Tompo malala!
Aina no fetran''ny fitiavanay;
Ka sanatria izay handao na hiala;
Mandrakizay, ho Anao izahay.
[FIV.]}

{4.
Aina no fetra, ny nofo anefa
Mora mirona ho lavo indray,
Ka manampia, ry Mpandresy mahefa!
O! mba tantano, hazòna izahay
[FIV.]}

{5.
Aina no fetra, ka ampio, ry Fanahy!
Aina no fetra, vonjeo e, ry Ray!
Aina no fetra, izahay re no sahy;
Aina no fetra, handresy izahay.
[FIV.]}
', '#1-VI-7', '#1') /end

insert into song values (12991, '299. Tompo mahery Tsitoha', NULL, NULL, '
{1.
Tompo mahery Tsitoha,
Faly ''Zahay mahatsiaro,
Fa Ianao Izay Loha,
Efa niantso olo-maro,
:,: Mba hitohizan''ny asa,
Asa natomboka teto. :,:}

{2.
Raiso ny saotra aman-dera
Noho ny zavatra vita,
Fa Ianao nampahery
Ny mpiasanao izay tafita.
:,: Raiso fa Anao samy irery
Ny fahefana sy hery. :,:}

{3.
Dia hamasino ny asa
Sy ny fiezahana mafy,
Efa nataon''ilay mpiasa
Tamin''ny fo sy ny hafy.
:,: Ho voninahitrao, ry Tompo
Zay niantso azy hanompo. :,:}

{4.
Ary izahay izay mandova
Sy ta-hanohy ny asa,
Aoka ny toetra ho lova
Horaisinay, enti-miasa.
:,: Ho voninahitrao, ry Tompo
''Zay efa niantso mpanompo. :,:}
', '#1-VI-8', '#1') /end

insert into song values (13001, '300. Ny tany izay vangianao', NULL, NULL, '
{1.
Ny tany izay vangianao,
Ry Jeso Masoandronay,
Tsy maintsy lasanao, Jeso,
Mba hanjakanao avokoa.}

{2.
Ny vavaka sy dera koa
Tsy manam-pitsaharana,
Hiakatra aminao tokoa,
Hitady fiadanana.}

{3.
Ireo olona voaforona,
Dia ary ho Anao tokoa;
Hohasoavinao ireo
Hanjary masina avokoa.}

{4.
Ny fo sy tany lasanao
Ho feno fahasambarana,
Ny loza hosakananao,
Ny soa hampidinina.}

{5.
Ny maty hovelominao
Ny ratsy hohavaozina,
Ny very hotadiavinao
Ny diso, re, horaisina.}

{6.
Ry vazan-tany efatra!
Ry Nosy tanindrazanay!
mba raiso Ilay Mpanavotra
Ho Tomponao mandrakizay.}
', '#1-VI-8', '#1') /end

insert into song values (13011, '301. He, sahanao, Mpamonjy o!', NULL, NULL, '
{1.
He, sahanao, Mpamonjy o!
Izao rehetra izao,
Ka be izao ny vokatra ao,
Fa vitsy ny mpanao.}

{2.
Indreto, tonga izahay
hivavaka aminao
Hanolotra ny tenanay
(na Haniraka ny namanay)
Hiasa ho Anao.}

{3.
Ny firenena marobe
Sy nosy mba tahio,
Indrindra fa ny Nosinay,
Ry Tompo o, ahio.}

{4.
Vimbinonao ny irakao,
Ny feony hamamaio,
Indrindra fa omeonao
Ny zotom-po madio.}

{5.
Tahio re ny olonao
Hihaino sy hanoa.
Mba hahitany aminao
Ny antra mahasoa.}
', '#1-VI-8', '#1') /end

insert into song values (13021, '302. Miandry ao ny Tomponao', NULL, NULL, '
{1.
Miandry ao ny Tomponao,
Samia manendry tena,
Fa be ny asa izay atao;
Atreho ny firenena!}

{FIV.
Topazo, maso kely
Ny ondry izay mania
Ilay zandry lasa niala,
Ataovy hoe: modia!}

{2.
Miandry ao ny namanao
Moa ve tsy hovonjena?
Hatony re, dia ilazao;
Kristy no famonjena
[FIV.]}

{3.
Hevero e! ny Nosinao
Ity Madagasikara!
Mba efa aiza izy izao
Ara-pilazantsara?
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13031, '303. O, mandrosoa, ry mpanaraton''i Jeso', NULL, NULL, '
{1.
O, mandrosoa, ry mpanaraton''i Jeso,
''zay no baiko azontsika ka mandrosoa
Jesosy Tompo no mibaiko mandrosoa
Raiso ka arotsahy ny haratonao.}

{FIV.
Miaingà, namako o! Jeso no mibaiko
:,: Mandrosoa, :,: tokoa
Aza kivy koa na dia tsy mahazo
Mandrosoa amin''ny lalina kokoa.}

{2.
O, mandrosoa, ry mpanaraton''i Jeso,
Saina mora kivy aoka ho lavinao ;
Ny mora kivy dia hanenina tokoa;
Raiso ka arotsahy ny haratonao.
[FIV.]}

{3.
O, mandrosoa, ry mpanaraton''i Jeso,
Mbola ao ny hazandrano ho azonao ;
Tsy azonao anio, ampitso mandrosoa,
Raiso ka arotsahy ny haratonao
[FIV.]}

{4.
Mba mitoera, Jeso ao an-tsambonay
Mba ho renay lalandava ny teninao;
Ny baikonao ry Tompo no andrasanay,
Handrotsahanay izao ny haratonay.
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13041, '304. He, ali-maizim-pito e', NULL, NULL, '
{1.
He, ali-maizim-pito e!
Sao very ao ny marobe,
Satana no manjaka ao,
Mivadi-po ny olona ao.}

{FIV.
Avia, ry Masoandronay,
Mba hazavao ny taninay!}

{2.
Fa ny mazava ho anay
Ao aminao, Jehovah Ray!
Mamirapiratra tokoa
Ilay Fitarikandro soa.
[FIV.]}

{3.
Ry Jeso Masoandronay!
Miandry eto izahay;
Mba misehoa Ianao,
Fanilon''ny fanahy o!
[FIV.]}

{4.
Dia hazavao ny taninay,
''Ndrao very eto izahay;
Savao ny aizim-be izao
Hizoranay hankaminao.
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13051, '305. O ! miasà, saikaza', NULL, NULL, '
{1.
O ! miasà, saikaza,
Jeso no herinao ;
Na misy mafy aza,
Izy tsy handao.
Dia matokia tokoa,
Fa Jeso momba anao,
Ka hisy voka-tsoa
Hifalianao.}

{2.
Maro ny andro lasa
''zay tsy nanaovanao,
Ka mbola b ny asa
''zay tsy vitanao,
Koa dia mitsangàna
Hanao tokoa izao,
Hahavitanao tsara
Ny anjaranao.}

{3.
Indro, fa mihariva,
Fohy ny andronao;
Moa tsy mba fantatrao va
Ka tsy hitanao?
Eny, fa manakaiky
Jeso Mpitsara re,
Ary efa ''mby akaiky
Ny hodianao.}

{4.
Rehefa tonga Jeso,
Tompon''ny asanao,
Ary hamaly soa
Ny mpiasany.
Mandrakizay doria
No hifalianao,
Raha mpanompo tsara
Mahatoky e!}
', '#1-VI-8', '#1') /end

insert into song values (13061, '306. Mamafaza voa, ka aza mba malaina', NULL, NULL, '
{1.
Mamafaza voa, ka aza mba malaina,
Fa tsy fantatrao re izay ho vanona ;
Na amin''ny hariva, na amin''ny maraina,
Mamafaza voa tsy mijanona.}

{FIV.
:,: Matokia re, matokia re,
Hisy amboara ho anao anie. :,:}

{2.
Mamafaza voa amin''ny mahantra,
Ka aza mba mifidy izay hasian-tsoa;
Fa ny fo mazoto sady feno antra
Dia homen''ny Tompo valisoa tokoa.
[FIV.]}

{3.
Mamafaza voa amin''ny malemy,
Hatanjaho ''reo izay reraka ao am-po;
Asehoy Jesosy loharanon-kery,
Ao no anovozy ''zay ilain''ny fo:
[FIV.]}

{4.
Miasà, sakaiza! Milofosa tsara
Dieny misy asa azonao atao;
Matahora sao tsy vita ilay anjara
''zay napetraky ny Tompo aminao.
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13071, '307. Iza no hanompo an''I Jeso soa?', NULL, NULL, '
{1.
Iza no hanompo an''I Jeso soa?
Iza no hanaiky, mba ho Azy moa?
He ny hatsarany! He ny antra be!
Izy ta hamonjy ka meteza re.}

{FIV.
Jeso Tomponay o! Indreto izahay,
Avy mba hanaiky, ka mandraisa anay.}

{2.
Iza re no mety, iza re lazao,
Hanome ny fony mba ho Azy izao?
He, miantso mafy Jeso Tompo tia:
" Zay rehetra mety, o! Avia, avia! "
[FIV.]}

{3.
Iza no mba sahy, sahy ao am-po
Ho miaramila fehin''I Jeso?
Iza no hiady ady tsara re,
Fa ny fahavalo dia marobe?
[FIV.]}

{4.
Iza re no hanomba Azy ankehitrio
Mba hamonjy ireny azon''ny manjo?
Iza no hihafy sady hanome
Andro, hery, saina? Mamalia e!
[FIV.]}

{5.
Iza no hazoto mba hiasa moa?
Iza no handroso mba ho hendry koa?
Iza no hanaiky, iza re? lazao!
Mba ho an''i Jeso izao anio izao;
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13081, '308. O! miangà, miasà izao,', NULL, NULL, '
{1.
O! miangà, miasà izao,
Irak''i Jesosy hianao;
Ny masoandro miposaka ao
Mba hanazava ny dianao.
:,: Ka dia mazotoa o, :,:}

{FIV.
O! miangà, miasà, mitoria,
Aza ho kivy, fa mba mihafia
Dia mba lazao ny nataony taty,
Mba hanazava ny maizina aty.}

{2.
O! miangà, miasà izao,
Ka afafazo ny voanao
Ho ao an-tany mahantra ao,
Jeso Mpamonjy no aronao
:,: Hitahy ny asanao :,:
[FIV.]}

{3.
O! miangà, miasà izao,
Aoka hiezaka fatratra;
Jeso hanambina anao aty
Mandra-pahazo ny vokatra
:,: ''Zay valin''ny asanao :,:
[FIV.]}

{4.
O! miangà, miasà izao,
Fa ''mby akaiky ny Tomponao
Mba hanome valisoa anao,
Araka izay voa nafafinao,
:,: Ka dia miasà izao :,:
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13091, '309. Iza no hitantara', NULL, NULL, '
{1.
Iza no hitantara
Ny Tompo Jesosy,
Ilay nandao ny lapa
Ka nidina taty?
Nitafy nofo Izy,
Nanetry tena koa.
Ka maty ho Mpisolo
Izao rehetra izao:
Iza no hitantara (in-3)
Ny Tompo Jesosy?}

{2.
Iza no hitantara
Ny fitiavany?
Fa tia ny mpanota
Mahatra, very koa;
Ka izay manatona Azy
Dia tsy hariany,
Fa raisiny tokoa
Mba ho sakaizany:
Iza no hitantara (in-3)
Ny fitiavany?}

{3.
Iza no hitantara
Ny zava-marobe
''zay raisin''ny voavonjy
Amin''ny alalany?
"Tsy mbola hita maso,
Na ren''ny sofina,
Na takatry ny saina"
Ny hasoavany:
Iza no hitantara (in-3)
Ny hasoavany?}

{4.
Iza no hitantara
Ny soa be ary
He! Lova tsara fara
Hasolo ny aty;
Any an-danitra any
Be haravoana,
Fa ao ny Tompo Jeso
''zay hifaliana:
Iza no hitantara (in-3)
Ny soa be ary?}
', '#1-VI-8', '#1') /end

insert into song values (13101, '310. Misy maro ao an-tsaha mitaraina', NULL, NULL, '
{1.
Misy maro ao an-tsaha mitaraina,
Ka ento ny Jiro mba hankao,
Tsy mba misy atoandro na maraina,
Fa maizina re ny ao;
Indro olo-marobe
No maniry mafy hoe:
Mba midina mankaty.
Itondray Baiboly re
Izy ''reo fa ta-handre
An''i Kristy Tompo soa.}

{FIV.
"Mandehana", hoy ny Tompo,
Mandehana
Ho amin''izao tontolo izao
Mitsangàna àry, eny, mitsangàna
Fa izy no homba anao.}

{2.
Mandehana mba hitarika ny jamba,
Mba ampifalio ny ory fo;
Ny mahantra ilazao fa misy lamba
Tsy miova, tsy mety lo;
Jeso mofon''aina soa,
Sady ranon''aina koa,
Asehoy ho hitany,
Tena Andriamanitra,
Tompo tao an-danitra,
Ambarao ho fantany:
[FIV.]}

{3.
Mandehana hanatanjaka ny osa,
Omeo toky re ny ketraka;
Ny anjaranao, ataovy ho tontosa,
Ka aza dia reraka,
Mandehana hianao,
Ka vitao ny asanao,
Fa ny tambinao ary,
Mbola any ihany re
Ny handraisan-tamby be,
Dia rehefa tsy ety;
[FIV.]}
', '#1-VI-8', '#1') /end

insert into song values (13111, '311. Miely lavitra any', NULL, NULL, '
{1.
Miely lavitra any
Ny olo-marobe;
Koa Iza no hankany;
Hamonjy azy re,
Hanorina sekoly,
Hitory teny koa.
Hamaky ny Baiboly,
Milaza zava-tsoa?}

{2.
Indreny olona ory
Tsinjovy, ry zareo!
Tsy misy ny mpitory,
Koa moa hafoinareo?
Tsarovy Jeso Tompo
Nandao ny lapa soa,
Ka tonga mba  hanompo,
Nanolotra aina koa.}

{3.
Ry Lohan''ny mpiandry!
Tendreo ny irakao,
Fa reraka ka mandry
Ny ondrikelinao;
Iraho ny mpitory
Handeha ankehitrio,
Ka tsy handà akory
Hanao veloma anio.}

{4.
Ny tia famonjena
Miaramila soa;
Izay manolo-tena
No iraka tokoa;
Na lavira ny tany,
Hanendry tena hoe:
"Izaho no hankany,
Iraho aho e!"}
', '#1-VI-8', '#1') /end

insert into song values (13121, '312. Mitoera, ry Jesosy!', NULL, NULL, '
{1.
Mitoera, ry Jesosy!
Ao an-tanimbolinao,
Ka amboary ''zao ny voly
Mba hitombo sy hamoa.
Tomoera,
Iasao ny Nosinay.}

{2.
Mba onenonao ny lasy
Itoeranay izao,
Ka ovay  ny Malagasy
Mba ho tena zanakao.
O! monena;
Hazavao ny Nosinay.}

{3.
Akekezo ireo mpania
Tafaverina aminao,
Ka tantano mba ho tia
Ny miresaka aminao,
O! tariho
Ho Anao ny Nosinay.}

{4.
Dia fohazy ny mpitory
Mba hanao ny asanao,
Ka hamoha ireo matory
Mba handre ny sitrakao,
Hamasino
Ho Anao ny Nosinay.}

{5.
O! Jesosy feno antra!
Dera, laza, ho Anao,
Ny lohalika rehetra
Handohalika aminao,
Mitoera,
Fa Anao ny Nosinay.}
', '#1-VI-8', '#1') /end

insert into song values (13131, '313. Miantrà  ny firenena', NULL, NULL, '
{1.
Miantrà  ny firenena,
Ry Jesosy Tomponay!
Ambarao ny famonjena
''zay nataonao fahizay;
Asehoinao ny hery
Namonjenao marobe;
:,: Ry Mpamonjy olom-bery!
Mamonje indray anie. :,:}

{2.
Mba heveronao ny Nosy
Itoeranay izao;
Iantranao, ry Jesosy,
Ny tsy mahalala Anao;
Iantrao, ry Jesosy,
Ny tsy mahalala Anao;
Iantrao ''reo faritany
Anjakan''ny aizim-be
:,: Mba ho tonga faingana any
Ny mazava lehibe. :,:}

{3.
Be ny tany tsy voasa,
Vitsy ny mpanomponao;
Koa izahay mpiasa
Ao an-tanimbolinao
Mba omeo fo minia
Hankatò ny teninao,
:,: Ka aoka mba hikely aina
Izahay hiasa ao. :,:}

{4.
Mankanesa, ry mpitory!
Ao an-tany lavitra ao,
Be  tsy mahalala akory
An''i Jeso Tomponao,
He, ny fahantrany, indrisy!
He, ny hadalan''ny ao!
:,: Fa ny olona tsy misy
Hampianatra azy izao. :,:}

{5.
Ny harena anomezo
Sy ny fahendrena koa.
Eny, Kristiana! Aelezo
''zay rehetra mahasoa;
Mankanesa re ''zay tia
Ny Mpamonjy lehibe;
:,: Miantrà ka manampia
Ny mahantra marobe. :,:}
', '#1-VI-8', '#1') /end

insert into song values (13141, '314. Injany, misy feo!', NULL, NULL, '
{1.
Injany, misy feo!
Ka iza no tsy mandre?
Ry Kristiana, raiso
Adidy lehibe.
Manerana ny tany
Ny antso avo izao,
Ho an''i Jeso Tompo
Izao tontolo izao.
Mihobia
Sy mitoria.
Ny teny mahafaly
Izay voarainareo
Izao no tena fetra;
Ho an''i Jeso soa.
Ny olona rehetra ho Azy
Avokoa.}

{2.
Tsy ho antsika irery
Ny fahasoavam-be,
Fa ho an''ny olom-bery
Tsy an-kanavaka e!
Ny fiantran''ny Tompo
Tsy misy fetra, koa
Lazaonareo mpanompo
Lay fitiavan-tsoa.
Miasà.
Ka milazà
Ny teny mampionona
Mahafa-po;
Voarainareo ny baiko:
Ho an''i Jeso soa
Ny olona rehetra ho Azy
Avokoa.}

{3.
Ny Tompo no milaza:
"Andeha mitoria,"
Ka na dia sarotra aza
Ny asa, matokia;
Fa tsy mandeha irery
Izay mitondra soa,
Fa omban''Ilay Mahery
Ny irany tokoa,
Ka sahia,
Dia mitoria
Ny famonjena lehibe
Voarainareo;
Voatsangana ny saina;
Ho an''i Jeso soa
Ny olona rehetra ho Azy
Avokoa.}

{4.
Ry Tompo sy Mpanjaka!
Ampio ny olonao
Hanoa sy hanahaka
Ka hankalaza Anao,
Dia aoka hokendreny
Izay katsahinao.
Ka hahita famonjena
Izao rehetra izao,
Ry Tsitoha!
Mba manehoa
Ny hery mahagaga izay
Anananao:
Tongava mba hanjaka,
Ka angony avokoa
Ny olona navotanao,
Ry Tompo soa!}
', '#1-VI-8', '#1') /end

insert into song values (13151, '315. Ry Ziona, mitsangàna', NULL, NULL, '
{1.
Ry Ziona, mitsangàna,
Ka mihazavà izao;
Andrandrao ny fahazavana,
''zay miposaka aminao.}

{2.
Maizina ny firenena,
Amin-tany fantatrao,
Fa tsy manam-pamonjena,
Nefa ankohonanao.}

{3.
Asandrato ny fanilo
Hanazava lavitra,
Mba ho marobe no hino
An''Andrimanitra.}

{4.
Atopazy re ny maso!
Marobe ny zanakao,
Toy ny tany feno hazo
Eo anatrehanao.}

{5.
Ry Jesosy, hotahinao
Anie ny lovanao;
Olo-maro izay irinao,
Taomy mora ho anao.}
', '#1-VI-8', '#1') /end

insert into song values (13161, '316. Maizina ny firenena', NULL, NULL, '
{1.
Maizina ny firenena
Koa diso lalana,
Tsy mahita famonjena,
Tia zava-poana;
Endrey, ny haverezan-tsiny,
Lany foana ny ainy.}

{2.
Lehibe ny aizin''ota,
Takona ny lalana
Handosiran''ny mpanota
Ny fahaverezana;
/ndrisy, very re ny aina!
/jany ny feo mitaraina!}

{3.
Mitsangàna izao, ry mino,
Mandehana faingana,
Ento aminy ny jiro,
Atory ny lalana;
Ianao va tsy onena
Ny tsy manam-pamonjena!}

{4.
Dia ento ny mazava
''zay naseho taminao;
Mazotoa hampisava
''zany aizim-pito ao,
Ka ny namanao mahantra
Itorio ny Mpiantra.}

{5.
Eny, tsara sy mahery
Ny filazantsara re!
Afaka ny olom-bery,
Sambatra ny mahare
Ka mitandrina tokoa
''zany teny mahasoa.}
', '#1-VI-8', '#1') /end

insert into song values (13171, '317. O, ry Rain''ny fahazavana tsy milentika!', NULL, NULL, '
{1.
O, ry Rain''ny fahazavana tsy milentika!
Avy izahay mitondra ny fisaorana;
Aloky ny fasanay no ipetrahanay,
Nefa andro no nasolonao ny alinay.}

{2.
Enga ka handroso tsara ny mazava e,
Mba ho fantatray toko Ilay manao hoe:
"''zaho no fahazavan''izao tontolo izao",
Hanazava sy hanitsy izay alehanao.}

{3.
Ny fitarikandron''aina efa hita re,
Manazava na ny kely na ny lehibe,
Mahasoa, mahahendry, mahasambatra,
Ka akory re ny andro izay hiposaka!}

{4.
Mitsangàna, ry Ziona, ka mihazavà!
Voninahitry ny Tompo no miposaka;
Atopazy manodidina ny masonao,
Ny haren''ny firenena tonga aminao.}
', '#1-VI-8', '#1') /end

insert into song values (13181, '318. Mandrosoa, ry mpanarato', NULL, NULL, '
{1.
Mandrosoa, ry mpanarato,
Aza ketraka izao!
Mandrosoa tsy miato,
Jeso re no herinao.
Ka ny jentilisa maro,
Havana sy tapaka,
Mba toroy ampahafantaro
An''Ilay Mpanjaka!}

{2.
Mandrosoa, ry mpanarato,
Miezaha fatratra,
Ka na iza hisahato,
Aza mba matahotra.
Notolorana adidy
Ianao, ka miasaà!
Na ho mamy na mangidy,
aza fay na mba mandà!}

{3.
Mandrosoa, ry mpanarato,
Ao an-drano lalina!
Misy ve hitora-bato?
Anehoy fitiavana!
Raha kivy ny mpanota
Noho ny haratsiny,
Ento any Gologota
Amin''ny Mpamonjiny.}

{4.
Mandrosoa re no baiko
''zay mifehy ahy izao,
Ka ny tena sy ny aiko
Raiso, Tompo, ho Anao!
He, afoiko manontolo
''zay ananako izao,
Ary raisonao daholo
Mba ho voninahitrao!}
', '#1-VI-8', '#1') /end

insert into song values (13191, '319. Mba iraho ny Fanahy', NULL, NULL, '
{1.
Mba iraho ny Fanahy
Ry Jesosy Tompo o!
Mba hanampy sy hitahy
Ny mpiasa irakao.
Tany mafy no asaina,
Nefa kely izahay,
Marobe no tsy misaina,
Raha itorianay!
Tanteraho re ny toky,
''zay nomenao fahizay,
Ka mombà anay mpitory
Mandra-podimandrinay!}

{2.
O, ry Masoandron''aina,
Hazavao ny namanay,
Mba fafazo andon''aina,
Fa efa toy ny tany hay;
Tapa-kazo no hajainy,
Ka tsy fantany ny To:
Feno aizina ny sainy,
Efa momoka sy lo;
Ny sikidy mahavery
No ataony solonao,
Samy vato, hazo kely
No hajainy toa Anao.}

{3.
Averenonao ireny
Mba ho tonga olonao;
Tadiavo tsy ho very,
Raiso re, fa lovanao!
Entonao ny famonjena
Ka ovay ny fony izao;
Aoka hisy sesehena
Ny hanaiky ho Anao!
Dia hiova re ny tany
Ka hanjary lonaka,
Ary hisy voa mamy
''zay ho tena vokatra}

{4.
Aoka re mba tsy ho rava
Ny mazava hitanay!
Mba handroso lalandava
Ny anjara-asanay.
Mba ho dera aman-daza
Ho Anao, ry Tomponay!
Hatanjaho ny mpiasa,
Dia tsy mba hety fay:
Aoka re ny olo-mino
Mba hamory ho Anao,
Ka hitombo be ny mpino
Hiandany Aminao.}
', '#1-VI-8', '#1') /end

insert into song values (13201, '320. Manana fiorenana', NULL, NULL, '
{1.
Manana fiorenana
Ny fiangonan''i Kristy;
Ela, fa he, tsy levona
Izy na maro na vitsy,
Vavan''ny olo-masina
Mety ho voatampina,
Nefa tsy maty ny feo.}

{2.
Ny fiangona-masina
Trano tsy mety mikoa,
Izy no vato velona
Rafitry ny Ray Tsitoha;
Jeso no fehizorony,
/sika mirafitra aminy,
Monina ao ny Fanahy.}

{3.
Nefa mba misy koa aty
Trano nataon-tanantsika
Ho voninahitr''i Kristy,
Mendrika ho tiantsika;
Fa ao no andrenesana
Ny tenin''Andriamanitra,
Mamin''ny olona mino.}

{4.
Ao ny batisa masina,
''zay tena ranon''ny aina;
Ao ny Mpihaino vavaka,
Tokin''izay mitaraina;
Ao koa ny fandraisana,
Dia ny sakafo masina,
Tena aman-dran''i Jesosy.}

{5.
Enga ka mba ho fantatra
Amin''ny tany rehetra
Ny loharano velona
Sady tsy mety ho ritra!
Raha ny lakolosy velona,
Faly izay mpivavaka,
Fa manantena hihaona.}

{6.
Raiso, ry Kristy, ho anao
Ny fideranay anio!
Ampandrosoy ny teninao,
Ny ondry very vorio
Mandra-pikambany indray,
Fa Ianao no Mpiandry.}
', '#1-VI-9', '#1') /end

insert into song values (13211, '321. Ry Jesosy Tomponay', NULL, NULL, '
{1.
Ry Jesosy Tomponay
Ravoravo izahay;
Tonga izahay izao
Mba hihaona aminao:
Indro misy tranonao
Hitokananay izao.}

{2.
Ka mangataka aminao
Izahay, ry Tompo o!
Aoka mba horaisinao
''zay nataonay ho Anao;
Trano fiangonana,
Trano fivavahana.}

{3.
Aoka mba ho fantatray
Eto ny alehanay,
Fa vahiny izahay,
Ka ny tanindrazanay
Dia any aminao;
Koa mba tarihonao.}

{4.
Aoka ny Anaranao
Mba hohamasininao;
Aoka ny Fanahinao,
Mba ho tonga hanavao
Ny mpanota marobe,
Dia ho voavonjy re!}

{5.
Ka ny fiadananao
Ampombay ny olonao,
Hampikambana ny fo,
Ka ho toy ny iray tam-po,
Mba hifankatiavanay
''zao sy ho rahatrizay}
', '#1-VI-9', '#1') /end

insert into song values (13221, '322. Indro, Tompo, misy trano', NULL, NULL, '
{1.
Indro, Tompo, misy trano
''Zay namboarinay ho Anao,
Angatahanay tsodrano,
Itokananay ho Anao,
Tompo o, mba hamasino
Ho fihaonana aminao,
Ho tempoly tian''ny mino,
Fivavahana aminao.}

{2.
Diovy, Tompo, mba ho trano
Zay hajaina ho Anao,
Ka omeo sy itokano
Mba ho fideràna Anao.
Tompo Masina $o, idiro
Mba ho mendrika Anao
Ka aoka Ianao ho jiro
''Zay hamirapiratra ao.}

{3.
Mba tahio, ry Ray Mpitahy,
Ho tempoly masinao,
Ho Betelan''ny fanahy,
Vavahady ho aminao.
Itoero, mba ho kianja
Mahamanina ny fo,
Mba ho trano mendri-kaja,
Mampivavaka ny fo.}

{4.
Mandrosoa ka midira
Ho Mpanjaka tokana ao
Ka ny vavaka sy hira
Mba tahio ho sitrakao.
Nefa tsy ny trano ihany
No atolotray, ry Ray,
Indro koa mba itokany
Ho Anao ny tenanay.}
', '#1-VI-9', '#1') /end

insert into song values (13231, '323. Vory ny mpanomponao', NULL, NULL, '
{1.
Vory ny mpanomponao
Eto anatrehanao.
Ry Jeso Tomponay!}

{FIV.
He! Faly ny fonay,
Faly, faly, faly,
He! faly ny fonay,
Vory maro toy izao.}

{2.
Mba atrehonao izahay,
Fa mitokana eto ''zao
Ny trano ho Anao:
[FIV.]}

{3.
Eto hitorianay
An''i Jeso Tomponay,
Mpamonjy lehibe:
[FIV.]}

{4.
Eto hanomezanao
Tsara ny Fanahinao
Mba hanadio ny fo:
[FIV.]}

{5.
Eto no hanananao
Laza saotra ho Anao;
Ny maro mba vonjeo:
[FIV.]}

{6.
Aoka re ny tranonay,
Trano iangonanay,
Mba ho tempolinao:
[FIV.]}
', '#1-VI-9', '#1') /end

insert into song values (13241, '324. Ry Andriananaharinay', NULL, NULL, '
{1.
Ry Andriananaharinay,
Isaoranay ny Anaranao
Nambininao ny tranonay
Ka vita tsara ho Anao:}

{FIV.
:,: Fenoy ny hamasinanao
Ny tranonanay ho tranonao :,:}

{2.
Ry Jeso! avy izahay
Hitokana ny tranonao,
Ny trano ''zay naorinay
Ho voninahitrao anie:
[FIV.]}

{3.
Avia, Fanahy Masina o!
Hitoetra eto aminay;
Ny aizinay mba hazavao,
Hananay fo be fitia:
[FIV.]}

{4.
Mba raisonao ny deranay
''Zay enti-mankalaza Anao;
Ny trano itokananay
Ekeo tokoa ho tranonao:
[FIV.]}
', '#1-VI-9', '#1') /end

insert into song values (13251, '325. Ry Jehovah Tomposoa', NULL, NULL, '
{1.
Ry Jehovah Tomposoa
Vory eto izahay,
Ka mifaly avokoa,
Raisonao ny vavakay!
Andro fitokana-trano,
Fifaliam-be izao,
Ampidino ny tsodrano
Avy ao an-danitrao.}

{2.
Ry Jesosy! Mandrosoa,
Fa Anao ny tranonay,
Hiderana Anao tokoa,
Ka henoy ny hatakay!
Nefa tsy vatan-trano
NO honenanao izao,
Fa ny fo no itokano,
Itoero, hafanao.}

{3.
Ry Fanahy Mpanazava!
Anjakao tokoa ''zahay,
Dia hazava lalandava
Ny fanahin-janakao;
Ka fenoy fanay mino
Ho tempolinao tokoa;
Hatrizao dia hamasiino
Ka hitombo sy hanoa}
', '#1-VI-9', '#1') /end

insert into song values (13261, '326. Atreho ny mpanomponao', NULL, NULL, '
{1.
Atreho ny mpanomponao
Izay mivory eto izao,
Ka efa nametrahanao
Adidy masina hatao.}

{2.
Ianao ''zay Tompon''ezaka
Nifidy anay tsy mendrika,
Ka efa nampirisika
Handroso tsy hiherika.}

{3.
''Ndrisy mpanompo tsy manoa
''Zahay izay nofinidinao,
Mpiasa tsy mba mahasoa,
Tompo o! Mifona aminao.}

{4.
Na dia kely aza izao
No azonay atolotra,
Aoka re mba ho sitrakao
Haroso ho fanatitra.}

{5.
Dia saotra no omena Anao
Ka raiso re, ry Tomponay,
Ny fonay koa onenonao
Sy anjakao mandrakizay.}
', '#1-VI-10', '#1') /end

insert into song values (13271, '327. Isaoranay Anao', NULL, NULL, '
{1.
Isaoranay Anao,
Ry Tomponay Tsitoha,
Ny nanomezanao
Mpiasa mahasoa.
Ka nony vao nandre
Lay antso taminao
Dia roso ka nandeha
Nanao ny asanao.}

{2.
Natokany ho Anao
Ny taona maro be;
Ilay fitiavanao,
Nataony rehareha.
Na nisy ny manjo,
Tsy kivy re satria
Ianao ilay ao am-po
Nanao hoe: "Matokia".}

{3.
Izay nataony anie
Hambininao tokoa,
Hitombo be dia be
Hitera-boka-tsoa.
Ianao ''Lay Tompo tia
No angatahanay;
Tongava, manampia
''Zay vitany ho anay.}

{4.
Ka mba tsinjovinao,
Sikino hery indray
Ity mpanomponao,
Anio sy rahatrizay.
''Lay teny taminao
Ho aminy anie:
Mpanompo sitrakao
Satria nahefa be.}
', '#1-VI-10', '#1') /end

insert into song values (13281, '328. Mba tahio Jeso o!', NULL, NULL, '
{1.
Mba tahio Jeso o!
Hira, hataka sy teny,
Mba tarihonao ny fo
Hanarahanay ireny;
Ianao no Tompon''aina,
Sady mandrika hoderaina}

{2.
Izahay mpanomponao
Hody ao an-tokantrano,
Faly manana Anao,
Ka vimbino sy tantano;
Aza avela ho irery
Izahay, fandrao ho very.}

{3.
Saotra no atolotray,
Raha mbola aty an-tany;
Ao an-danitra indray
Dia dera tsy ho lany
No hatao sy hohiraina
Ho Anao, ry Tompon''aina.}
', '#1-VI-11', '#1') /end

insert into song values (13291, '329. Saotra, dera, no homena', NULL, NULL, '
{1.
Saotra, dera, no homena
Ho Anao Mpanavotray,
Ianao nanetry tena
Mba hanandratra anay.
Mba tariho, Tompo o!
Ny fanahy sy ny fo;
Tianay ny hiaradia
Aminao ka tsy hania.}

{2.
Raha ory sy mijaly
Eto aza izahay,
Tsy ho kivy, fa hifaly
Amin''ny anjaranay.
Ianao, ry Tomponay,
No mba iankinanay,
Ka inoanay homena
Ho anay  ny famonjena}
', '#1-VI-11', '#1') /end

insert into song values (13301, '330. Sambatra ny mankato', NULL, NULL, '
{1.
Sambatra ny mankato
''zay lazain''ny Tompon''aina,
Sy manaiky eram-po
Fa hanolotra ny saina,
Ka mitandrina ny didy
Toy  ny zava-tsarobidy}

{2.
Sambatra izay mandà
Raha taomina hanota,
Ka na dia halatsa-dra
Tsy mba mety resin''ota,
Tsy ekeny ny mpanimba,
Na ireo mpanazimba.}

{3.
Sambatra ny olona
''zay misaina ao am-pony
Andro aman''alina
Ny lazainao, ry Mpamonjy;
Fa ataonao toy ny hazo
Maitso tsara, tsy malazo.}
', '#1-VI-11', '#1') /end

insert into song values (13311, '331. Mba soraty ao am-poko', NULL, NULL, '
{Mba soraty ao am-poko
Jeso, ny Anaranao;
Mba arovy tsy haloto
Ny kasen''ny tananao.
Ianao, Jesosy o!
No ho voninahitro;
Ianao, ry voahombo,
No Mpanjakako sy Tompo.}
', '#1-VI-11', '#1') /end

insert into song values (13321, '332. Finaritra ny olonao', NULL, NULL, '
{1.
Finaritra ny olonao,
Jesosy Tompo o;
Fa manana ny aim-baovao,
Ka ravo eram-po,
Ny famonjena vitanao
Dia efa voaray izao
Ny fifaliany aty
Hitohy hatrary.}

{2.
Izaho va, Jesosy o,
Mba isain''ny Anao?
Ny solika fitaizako
Moa misy va izao?
Hevero, ry fanahiko,
Ka aza mety afa-po
Ambara-pahafantatrao
Fa Jeso no anao.}
', '#1-VI-11', '#1') /end

insert into song values (13331, '333. Hody, Tompo, izahay', NULL, NULL, '
{1.
Hody, Tompo, izahay
Miaraha aminay,
Ka tahio ny teninao
Mba hamorona aim-baovao,
Sao mivory foana
Izahay mpiangona.}

{2.
Marobe ny rafinay
Ta-hamandrika anay;
Tompo, miarova anay,
Sao dia very izahay;
Mitoera aminay
Ao an-tokantranonay.}

{3.
Efa renay taminao
Fa anay ny lanitrao,
Efa namboarinao
Mba ho an''ny olonao;
Ka tariho, Tompo o,
Hody ao ny zanakao!}
', '#1-VI-11', '#1') /end

insert into song values (13341, '334. Sambatra ny sofiko', NULL, NULL, '
{1.
Sambatra ny sofiko
Fa mandre ny teny
Tenin''i Jesosiko,
Tenim-pamonjeny.
He! Izaho tiany
Mba hiara-dia
Sy hianatra aminy
Ary tsy hania.}

{2.
Sarotra ny làlako
Sady mampanahy.
Nefa ny Mpamonjiko
Mahatoky ahy;
Vetivety foana
Dia ho avy Izy
Amim-boninahitra,
Ka handray ny Azy.}
', '#1-VI-11', '#1') /end

insert into song values (13351, '335. Hody ''zahay Zanahary!', NULL, NULL, '
{1.
Hody ''zahay Zanahary!
Tahio tsara izahay;
Ampifalionao ny fonay
Amin''ny fitianao;
:,: Ampifalio :,:
''Zahay ety an-tany.}

{2.
Mankasitraka Anao ''zahay
Noho ny teny soa
''zay nampianaranao
Saina hahasoa anay,
:,: Tomoera :,:
Aminay isan''andro.}

{3.
Raha ho faly izahay
Ka handao ity tany,
Dia ampifalio izahay,
Ento ho an-danitra;
:,: Dia faly :,:
Izahay mandrakizay.}
', '#1-VI-11', '#1') /end

insert into song values (13361, '336. Ampodio izahay, Jehovah', NULL, NULL, '
{1.
Ampodio izahay, Jehovah
Amin''ny fitianao ;
Na aiza na aiza no haleha,
Aoka ho vimbininao:
:,: O! ambino, :,:
Izahay, mpanomponao.}

{2.
Fa ny teny renay any
Aoka ho tadidy koa,
Ka mba hiasanay saina
Hahazoanay ny soa
:,: Isan''andro :,:
''zay hiainanay aty.}
', '#1-VI-11', '#1') /end

insert into song values (13371, '337. Ry Jehovah, Tompo o', NULL, NULL, '
{1.
Ry Jehovah, Tompo o!
Tompo masina sy to,
Azonay ny teninao
Mampandre ny antranao
Aoka harahinay
Ka ho masina aminay,
:,: Mba hanao ny sitrakao
Izahay mpanomponao :,:}

{2.
HOdy izahay izao
Amin''ny Anaranao;
Mitoera aminay
Ao an-tokantranonay,
Mba ho velona aminao
Izahay navotanao;
:,: Be ny fahavalonay,
Ka arovy izahay :,:}
', '#1-VI-11', '#1') /end

insert into song values (13381, '338. Arovy aho, Tompo', NULL, NULL, '
{Arovy aho, Tompo,
Atolotro izao
Ny tenako hanompo,
Sy hankalaza Anao.
Omeo ahy hery
Sy faharentam-po,
Arovy tsy ho very
Izay anjarakao.}
', '#1-VI-11', '#1') /end

insert into song values (13391, '339. Ry Jesosy mba ampio aho ho madio fo', NULL, NULL, '
{Ry Jesosy mba ampio aho ho madio fo,
Raha mivily ahitsio hanarahako ny to.
Mba ampaherezo aho
hanompoako Anao,
Raha ory iresaho mba hifikitra aminao.}
', '#1-VI-11', '#1') /end

insert into song values (13401, '340. Tompo o, mba hasaovy', NULL, NULL, '
{Tompo o, mba hasaovy
Sy harovy izahay;
Tompo o, mba aposahy
Aminay  ny tavanao;
Tompo, mitodiha anay,
Dia hiadana izahay.
Ray sy Zanaka, Fanahy.
Mitahia anay, Amen!}
', '#1-VI-11', '#1') /end

insert into song values (13411, '341. Tompo o! Ampitandremo', NULL, NULL, '
{1.
Tompo o! Ampitandremo
''zay lazainao izahay;
Hazavao ka mba omeo
Hery ny fanahinay;
Ka ny fo ampionony
Tsy hitebiteby intsony;
:,: Mba diovy hamasino
Izahay, ry Tompo o! :,:}

{2.
Ka rehefa hodimandry
Izahay, ry Tomponay,
Tsy ho taitra, fa hiandry
Ny handraisanao anay.
Ry Jesosy Tompon''aina,
Ianao no antenaina,
:,: Ianao no hanazava
Ny hodianay ary :,:}
', '#1-VI-11', '#1') /end

insert into song values (13421, '342. Ny fahasoavan''i Jesosy Kristy', NULL, NULL, '
{Ny fahasoavan''i Jesosy Kristy
Sy ny fitiavan''Andriamanitra
Ary ny firaisina amin''ny Fanahy Masina..
Ho amintsika rehetra anie.
Amen!}
', '#1-VI-11', '#1') /end

insert into song values (13431, '343. Ry Jeso Tompo soa', NULL, NULL, '
{Ry Jeso Tompo soa,
Ry vatolampiko!
Esory avokoa
Ny hevi-dratsiko.
Omeo ahy hery,
Arovy tsy ho very
Ny ondrikelinao
Mba hifalianao;
Jesosy o, henoy re, henoy.}
', '#1-VI-11', '#1') /end

insert into song values (13441, '344. Fiadanana be', NULL, NULL, '
{Fiadanana be,
Hasambarana re,
No miandry antsika ao an-danitra e,
Ka andeha handao
''zao tontolo izao,
Ka hitady tokoa ''lay tany vaovao
Miaingà hody ao,
Miaingà hatrizao
Mba modia izao,
Ao amin''i Jesosy!}
', '#1-VI-11', '#1') /end

insert into song values (13451, '345. Mba tarihonao, ry Raiko', NULL, NULL, '
{Mba tarihonao, ry Raiko,
Ny fanahy sy  ny foko,
mba ho any aminao
O! ry loharanon''aiko,
''zao ihany no ilaiko,
Dia ny hahita anao.}
', '#1-VI-11', '#1') /end

insert into song values (13461, '346. Ny teninao, Jehovah o!', NULL, NULL, '
{1.
Ny teninao, Jehovah o!
Voatory aminay,
Tahio tsy ho levona,
Velomina anie!}

{2.
Izay voatory tsy anay,
Fa avy aminao,
Ka dia ny fanambinanao
NO mba ilainay koa.}

{3.
Fa tsy mba ampy ho anay
Ny toriteninay,
Ka tsy ho tokinay ireo,
Raha tsy ambininao.}

{4.
Dia ampitoeronao izao
Ho eto aminay
Ny herin''ny Fanahinao
Mba hahasoa anay.}
', '#1-VI-11', '#1') /end

insert into song values (13471, '347. Derao Andriamanitra', NULL, NULL, '
{Derao Andriamanitra
Fa Izy Loharanon-tsoa,
Derao ny Tompo tokana,
Ray, Zanaka, Fanahy koa}
', '#1-VI-11', '#1') /end

insert into song values (13481, '348. Andriamanitra Tahio ny mihaino', NULL, NULL, '
{1.
Andriamanitra
Tahio ny mihaino
Ny tsy manao vava-poana
Hahazo ny iriny.
Andriamanitra
Ho masoandrony;
Ka ny Fahany Masina
Mpitaridalana.}

{2.
Ny Anaran''i Jeso
No voninahiny;
Ny hamarinan''i Kristy
No fihaingoany.
Tiavo Jesosy,
Ry mino ny teny!
Katsaho fihavanana,
Ry tsy mino rehetra!}
', '#1-VI-11', '#1') /end

insert into song values (13491, '349. Dera re, laza anie !', NULL, NULL, '
{1.
Dera re, laza anie !
Ho an''Andriamanitra !}

{2.
Aoka ny tany re,
Mba hiadana anie !}

{3.
Ary ny olony
Aoka hankasitrahany !}
', '#1-VI-11', '#1') /end

insert into song values (13501, '350. Ianao ry Ray no mibaiko', NULL, NULL, '
{1.
Ianao ry Ray no mibaiko
Hanaovanay ny asanao
Ka ny handà dia zava-tsy haiko
Na sarotra aza ny atao
Ny fitoriana lalaina
Natomboky ny mpianatrao
Dia tsy azo atao very maina
Fa he, tohizanay izao.}

{2.
Fa mbola adidin''ny mazava
Mitondra ny fanala izao,
Hiainga, handeha hampisava
Ny aizina ao an''ala ao.
''Reo rahalahy sy rahavavy
Dia be tsy mahalala Anao
Koa indro, Tompo, aho, avy
Hitory ny Anaranao.}

{3.
Kanefa tsy ny eto ihany
No anirahanao anay,
Fa ny any lavitra any
Dia samy mbola adidinay.
Hiezaka, hikely aina,
Hamafy koa ny teninao
''Zay hany tena mofon''aina,
Ho an''izao tontolo izao.}
', '#1-VII-1', '#1') /end

insert into song values (13511, '351. Tsy hangina re ny hoby noho hianao ry Ziona ', NULL, NULL, '
{1.
Tsy hangina re ny hoby noho hianao ry Ziona ;
Fa miposaka aminao ny fahazvana soa;
Ny fanilom-pamonjena, indro fa torina
Hanazava ny olona tokoa,}

{FIV.
Mivoaha, asandrato, ny fanevasoanareo
Tsenao re ny mpanjaka fa avy aminao
Ny famonjeny sy ny lova izay atolony anao
Satria hianao dia tsy foiny.}

{2.
Voninahi-panjakana isaloranao,
Sy anaram-baovao no iantsoana anao ;
Fampakaram-bady, fety, anaingàna anao,
Mba ho an''I Kristy, ''lay Tomponay.
[FIV.]}

{3.
O ! Ry mpiambina tinendry eo ambony manda,
Aza avelanao hangina re ny feonao;
Ka hijoro izao ny mino fa tsy hisy banga
Eo an-tobin''i Jeso Tomponao.
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13521, '352. Fantatrao va ny Mpamonjy', NULL, NULL, '
{1.
Fantatrao va ny Mpamonjy
Ilay Tompon-danitra?
Jeso, Kristy sy Mpanjaka,
Zanak''Andriamanitra,
Lay nandao ny lapan-dRainy
Feno voninahitra
Tompo tonga mba hanompo
Noho ny fitiavana.}

{2.
Eny, zava-mahagaga
Fietrena lalina:
Saro-bidy ka tsy foiny
''Sika olombelona.
''Lay Mpisorona Ambony
''Zao no lasa sorona,
Ka manolotra ny ainy
Mba ho fanavotana.}

{3.
Ao ny lanitra mivoha
Miantso anao hiakatra:
Aoka tsy hiroa saina,
Manapaha hevitra,
he mitsotra moramora
Mba handray ny tànany;
Mitodiha: izao no ora,
Maneke ny zanany.}
', '#1-VII-1', '#1') /end

insert into song values (13531, '353. Mba sainonao ange', NULL, NULL, '
{1.
Mba sainonao ange,
Sao tsy mba fantatrao,
Ny zava-dehibe
nataoko ho anao;
:,: Hevero ka lazao, lazao
Izay mba vitanao :,:}

{2.
Ny lapako tary,
Ny fahefako tao,
Nilaozako taty
Hanavotra aina anao:
:,: Hevero, ka lazao, lazao
Izay mba foinao :,:}

{3.
Heveronao ange,
Ny nisoloana anao;
Nangidy indrindra anie
Ny nahazoana anao;
:,: Hevero, ka lazao, lazao
Izay mba nentinao :,:}

{4.
Endrey! Ny aiko koa
Nomeko ho anao;
Tsy misy zava-tsoa
Tsy foiko ho anao:
:,: Hevero, ka lazao, lazao
Izay mba taminao. :,:}

{5.
Ry Tompo be fitia!
Endrey, ''ty vitanao;
Izao ''zay mpania
Tsy mahavaly ''zao:
:,: Ny aiko no mba raisonao
Hanao ny sitrakao. :,:}
', '#1-VII-1', '#1') /end

insert into song values (13541, '354. Ny Mpamonjy no mitady', NULL, NULL, '
{1.
Ny Mpamonjy no mitady
Fihavanana aminao
Izy no manambitamby
Hanatonanao izao;
Soa manjary fandatsana
Ny fifonana aminao,
Ka ny mety mihavana
Amin''ny Mpamojy izao.}

{2.
Dieny misy hafanana
Ka mangoraka ny fo,
Manatòna ny Mpamonjy
Sao dia mangatsiaka indray.
''zao no andro famonjena,
Ka vonjeo sao neninao;
Ry mpanota, miverena
''zao ankehitriny izao.}

{3.
Sao ity no toriteny
Fara-fanasana anao ;
Soa anio no fotoana
Fara-ahavelomanao;
Ka aza mba misalasala,
Sao manjary neninao,
Raha tsy mety mahalala
''zao fotoana mety izao.}

{4.
/ndrisy ! be ny fahotako,
Be ny ratsy vitako ;
Sao tsy raisin''ny Mpamonjy
Ny mpanota tahaka izao?
Miverena, fa ny rany
Ampy hahadio anao;
Manatòna hosasany
''zao ankehitriny ''zao.}
', '#1-VII-1', '#1') /end

insert into song values (13551, '355. Ry namako mahantra', NULL, NULL, '
{1.
Ry namako mahantra
Manaram-po izao!
Ny ota no mandratra,
Sao simba hianao;
Izay anao rehetra
Ho lasa avokoa,
Fa tsy mba misy fetra,
Ho levona avokoa.}

{2.
Ny Tompo be fiantra
Mangora-po tokoa
Mahita anao mahantra
Fa tsy mahita soa;
Natolony ny ainy
Ho vidin''ny ainao,
Nalatsany ny rany
Mba hanadio anao.}

{3.
Ny Tompo malahelo,
Tsy foiny hianao,
Ny feo mamelovelo
Mibitsika aminao;
Onena anao ny fony
Ka mitaraina hoe:
Ny ota ajanony,
Mba ajanony re!}
', '#1-VII-1', '#1') /end

insert into song values (13561, '356. Ry mpanota! Mihevera', NULL, NULL, '
{1.
Ry mpanota! Mihevera;
Fa miantso Kristy izao;
Izy ta-handray mpanota,
Manatona hianao
:,: Mihainao, mihainoa,
Ka ario ny ditranao. :,:}

{2.
Ry Mpanota! Miverena
Ao am-balan''i Jeso;
aza dia mirenireny,
Sao iharan-doza izao;
:,: O! modia, O! modia,
Ialao ny ditranao. :,:}

{3.
Ry Mpanota! Manatona
Dieny misy antso ''zao,
Fa ho avy ny fotoana
Mba hanenenanao:
:,: O! avia, O! avia,
''zao ankehitriny ''zao. :,:}

{4.
Ry Mpanota! Matokia,
Tompo mora fo Jeso;
Izy dia tena tia
''zay mibebaka tokoa;
:,: Matokia, matokia,
Mibebaha re izao. :,:}

{5.
Ry sakaizan''ny mpanota,
Ento aho ho Anao;
He! Ny tenako rehetra
No atolotro izao;
:,: Raisonao re, raisonao re
Ho fanati-tsitrakao. :,:}
', '#1-VII-1', '#1') /end

insert into song values (13571, '357. Efa tonga aty izao', NULL, NULL, '
{1.
Efa tonga aty izao
Teny fiainana ;
Raiso mba ho fiainanao,
Teny fiainana.
He! Ny teny tsara,
Teny manan-kery,}

{FIV.
Tenin''ny Ray,
Tsara tokoa,
Teny fiainana.}

{2.
Io no Filazantsara soa,
Teny fiainana ;
Teny mamy indrindra koa,
Teny fiainana,
Raisonao ny teny,
Teny soa nomeny:
[FIV.]}

{3.
Ry mpanota ! henoy izao,
Teny fiainana ;
Fa fiainana ho anao,
Teny fiainana.
Kristy no nomena
Mba ho famonjena:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13581, '358. He! Ry namako mahantra', NULL, NULL, '
{1.
He! Ry namako mahantra,
Indro Jeso Tompo tia,
Lay Mpamonjy feno antra,
No mitady ny mania,
Olom-bery tsy mba foiny,
Ka na diso aza re
:,: Mbola tiany sy antsoiny
Hanam-pifaliam-be. :,:}

{2.
He! Fa tsy mba misy fetra
Ny homena lova soa
Fa ny olona rehetra
Samy tiany avokoa;
Nefa izao be hataka andro
Dia henanina tokoa;
:,: Misy fetra ny fotoana
Azo amindrana fo. :,:}

{3.
Manatona ankehitriny,
Ry mpanota ory fo!
Mab hesoriny ny tsiny
Ka hadio indray ny fo;
Fa ny ota hosasany
Dia ho afaka avokoa,
:,: ''Zany re no fikasany
Ka tsy maintsy to tokoa :,:}

{4. Fa izay mandà ny teny
Sy maditra, mafy fo,
Hampangain''ny teny reny
Fa tsy nety nankatò;
Jeso Tompo no hitsara
Sy hamaly ny natao,
:,: Nefa kosa ny anjara
Dia miankina aminao.:,:}
', '#1-VII-1', '#1') /end

insert into song values (13591, '359. Tsy renao va ny feon''ny Ray?', NULL, NULL, '
{1.
Tsy renao va ny feon''ny Ray?
Ry malahelo fo!
Miantso hoe : "Avia, Avia,
Modia hianao?"}

{2.
Ny Rainao be fitiavana
Hifaly aminao,
Ka hanome malalaka,
Raha hody hianao.}

{3.
Na diso aza hianao,
Mitebiteby koa,
Ny Rainao ta-handray anao
Ho sambatra tokoa.}

{4.
Modia an-tranonao ary,
Mahantra hianao;
Tsy misy zava-tsoa aty
Hahafa-po anao.}

{5.
Ny voro-damba izay anao
Hesorina aminao,
Ka lamba tsara sy madio
No hitafianao.}

{6.
Mba maneke ny teniny
Ankehitriny izao,
Fandrao dia hisy loza re
Hahatra aminao.}
', '#1-VII-1', '#1') /end

insert into song values (13601, '360. Mba hevero, ry mpanota!', NULL, NULL, '
{1.
Mba hevero, ry mpanota!
Ny nataon''ny Tomponao;
Izy tsy mba manana ota
Nefa tonga solonao;
He! Ny lanitra nafoiny
Mba hidinany taty;
Olom-bery no antsoiny,
Mba handova ny ary.}

{2.
Mba hevero, ry mpanota!
Fa nijaly Jeso tia,
Maty tany Golgota
Mba hamory ny mania.
Indro latsa-dra ny Tompo,
Nefa tsy mba diso re!
Fa nisolo ny mpanompo
''Zay nanohitra ela be.}

{3.
Mba hevero, ry mpanota:
Alatsaho ao am-po:
Mahevery re ny ota
Raha tsy voadio ny fo,
Mangataha ankehitriny,
Dia hodiovin''i Jeso,
Fa izany no iriny
Mba handraisanao ny soa.}

{4.
Mba hevero, ry mpanota!
Ny fifonana aminao
Hahafoizanao ny ota
Tena fahavalonao.
Manatona hovonjena
Sao hiharan-doza be!
Manatona, fa homena
Fifaliab-dehibe.}

{5.
Mba hevero, ry mpanota!
Inona ary no hatao?
Za mety resin''ota,
Tiavo Jeso Tomponao;
Ny lalàny no araho,
Sitrapony tontosay;
Nefa Izy angataho
Homba anao mandrakizay.}
', '#1-VII-1', '#1') /end

insert into song values (13611, '361. Manatona, ry malala!', NULL, NULL, '
{1.
Manatona, ry malala!
An''i Jeso Tomponao
Raiso re ny famonjena
:,: Tsy ho faty hianao. :,:}

{2.
Indro Jeso, fa miandry
Mba handray anao izao;
Miverena, aza kivy,
:,: Mazotoa, sy matokia. :,:}

{3.
Aza mba mantaka andro,
Sao dia tratra hariva ety;
Tahotra sy horohoro
:,: Hampivadi-po anao. :,:}

{4.
Ao ny tany soa homeny
Mampifaly fo ery;
Sady tsy mba misy ratsy,
:,: Mandrosoa ho tonga ary. :,:}
', '#1-VII-1', '#1') /end

insert into song values (13621, '362. Tsy fantatrao va ''lay namonjy anay?', NULL, NULL, '
{1.
Tsy fantatrao va ''lay namonjy anay?
Ry havako ory izao,
Mitady anao ''lay namonjy anay,
Maniry hamonjy anao;}

{FIV.
Mihainoa, ny hamonjy anao
Manao hoe: "Manatona anio,
Na dia be aza ny ratsy natao,
Dia mety ho afaka izao."}

{2.
Fa tonga taty ''lay namonjy anay,
Ka nampijaliana tokoa;
Ny rany izay namonjeny anay
Hamono ny helokao koa:
[FIV.]}

{3.
Misento anao ''lay namonjy anay,
Ry olo-mahantra aty!
Tomany anao ''lay namonjy anay,
Fandrao ho very ary:
[FIV.]}

{4.
Henoinao ''zao ny hamonjy anao,
Hiadam-pinaritra am-po:
Faingàna handray ny homeny anao,
Dia lova tsy mety ho lo:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13631, '363. Ry olo-mahantra', NULL, NULL, '
{1.
Ry olo-mahantra,
Mihainoa izao,
Saino tsara ny teny
''zay lazaina aminao;}

{FIV.
Ry olo-mahantra!
Modia, modia!}

{2.
Fa ela dia ela
Ianao no nania,
Tsy nahita ny tsara,
Tsy nahazo ny soa:
[FIV.]}

{3.
Tsy misy mpiantra
Mba hitsinjo anao,
Afa-tsy ny mpamonjy,
''zay mitady anao:
[FIV.]}

{4.
Tsarovy ny Rainao
Malahelo anao
Ka miandry tokoa
Hampandroso anao:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13641, '364. Ry mpanota! Ry mpanota!', NULL, NULL, '
{1.
Ry mpanota! Ry mpanota!
Raiso Jeso Tomponao,
Aza mba misalasala,
Manatona Azy ''zao
Ka ekeo ny feon''i Jeso
''Zay nifona ho anao;
Teo ambony hazo avo
Maty Izy ho anao.}

{2.
Indro Jeso, ry mpanota!
Faly hohatoninao,
Sady Tompo tsy mandatsa,
Fa mamindra fo tokoa,
Ka miteny moramora
Ta-hamela heloka;
Manatona, ry mpanota!
Mba handray ny avotrao.}

{3.
Ry mpanota! Ry mpanota!
Misy vonjy ho anao,
Ka toneo ny alahelo
Mampahory fo anao;
Aza mety resin''ota
Fa jereo ny Tomponao;
Izy no handresy tsara
''Zay miady aminao.}

{4.
Ianao tsy manan-toky
Ho mpamela heloka,
Afa-tsy ny Tompo Jeso,
''Lay nanavotra aina anao;
Ka aza mba misalasala,
Ry mpanota very o!
Fa azonao ny lova tsara;
Raiso Jeso Tomponao.}
', '#1-VII-1', '#1') /end

insert into song values (13651, '365. He! Ry mpanota ana, sasatra', NULL, NULL, '
{1.
He! Ry mpanota ana, sasatra,
Mitondra fahoriana lehibe;
Jeso, Mpanavotra olo-meloka,
No mahavonjy ny fanahinao}

{FIV.
Matokia ny Mpamela helokao,
Aza kivy, tsy ho faty hianao;
:,: Matokia re :,:
Matokia, Jeso tsy handao anao.}

{2.
Indro ny fitiavan-dehibe!
Ny ainy tsy nataony ho zavatra,
Hamonjy ny mpanota meloka,
Sy manana entana mavesatra:
[FIV.]}

{3.
Mba mahareta, Jeso aminao,
Ny tanany mahery hiambina,
Aza malaky resin-tahotra,
Fa Izy tsy hiala aminao:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13661, '366. Andeha, ry hava-malala!', NULL, NULL, '
{1.
Andeha, ry hava-malala!
Andeha hiaraka izao,
Hitady fonenana tsara
Honenana mandrakizay,
Ao amin''ny tanin''ny Tompo
Ny fahasambarana be,
Ny ota tsy miditra akory,
Tsy misy ny loza manjo.}

{FIV.
Dia aoka hiaraka isika,
Ry hava-malala izao!
Hitady fonenana tsara
Honenana mandrakizay.}

{2.
Ny trano dia maro tokoa,
Mifaly ny mponina ao,
Tanteraka so any aleha,
Fitia omena ny ao;
Miara-midera ny Tompo,
Mpanavotra be indrafo;
Miara-mandova ny soa,
Mitafy lamba madio:
[FIV.]}

{3.
Na ety ny lalana aza,
Na mao ny zava-manjo,
Dia aza matahotra akory,
Mandroso no mety hatao;
Anjely tsy tambo isaina
Hirahin-kanampy anao;
Mpanompon''ny Avo Indrindra
Hitoby hiaro anao:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13671, '367. Ry zanako mania!', NULL, NULL, '
{1.
Ry zanako mania!
Henoinao izao.
Izaho be fitia
Mangora-po anao;
Ry Zanako! Modia,
Sao maty hianao,
Fa be ny bibidia;
Modia re!}

{2.
Ny zavatry ny tany
Sao mahasimba anao
Harena mety lany
Tsy mahavonjy anao;
Izaho irery ihany
No hiadananao;
Ka mbola manotany:
Tsy hody va?}

{3.
Mamiratra jerena
Ny lalan-tsitrapo,
Fa ankaso-bolamena,
Mamitaka ny fo;
Ny sitrapon''ny tena
Mamery ny ainao,
Ario ka miverena,
Modia re!}

{4.
Izao Tompon''aina,
Nijaly ho anao,
Ka, he, nanolotra aina
Mba hamonjena anao;
Ny rako mitaraina,
Modia hianao;
Ka aza mba malaina
Modia re!}
', '#1-VII-1', '#1') /end

insert into song values (13681, '368. Manatona, ry mpanota', NULL, NULL, '
{1.
Manatona, ry mpanota
Ory, jamba, very koa!
Ianao simbàn''ny ota,
Indro, ao ny Tompo soa;
Ry tsy mendrika hovonjena,
Misy ''zay miantra anao;
Fifaliam-be homena,
Famelana helokao}

{2.
Mba henoy  ny teny tsara
''Zay lazaina aminao,
Sao very ny anjara
Soa tadiavinao;
Feno antra, manan-kery
Jeso no mitaona anao;
Ka ny mino tsy ho very
Manatona re izao.}

{3.
Ny miato tsy mba tsara,
Ny halainao ario;
Sao helohin''ny Mpitsara,
Manatona re anio;
He! Miandry ny Mpamonjy,
Mba faingàna hianao.
Raha te-ho voavonjy,
Aza ela hianao.}
', '#1-VII-1', '#1') /end

insert into song values (13691, '369. Faingàna, ry hava-malala', NULL, NULL, '
{1.
Faingàna, ry hava-malala,
Hatony ny Tompo izao,
Ny fo sy ny feo asandrato,
Dera ''Lay nanavotra anao.
Ny avotra sy ny famonjena,
''Zany vitany tao Kalvary,
No fanantenan''ny mpanota
Hahazo fiainana ary.}

{2.
He Izy mitondra irery
Ny mafy, nisolo anao,
Ka maty hamonjy ny very;
Ny rany nandriaka tao,
Niaritra latsa sy eso,
Ny hombo sy ny lefona koa;
Nivesatra ota Izy teto
Hanananao fiainana soa.}

{3.
Endrey! Ny fitiavan''i Jeso,
Ry olo-mahantra, jereo!
Nijaly hamonjy, hevero,
Ny ota mahazo ekeo;
Ekeo, fa ny fo mihamafy,
Dia loza manenjika anao;
Faingàna hamonjy ny ainao.
Hararaoty ny andro ''zao.}

{4.
Ny masom-panahy atopazo,
Ny lanitra no andrandrao;
Ny Tompo maniry hamonjy,
Henoy, fa miantso anao,
Faingàna, ry hava-malala!
Modia an-tranon''ny Ray;
Miandry anao re ny lova,
Fiainana mandrakizay.}
', '#1-VII-1', '#1') /end

insert into song values (13701, '370. Mba henoy ny feon''i Jeso', NULL, NULL, '
{1.
Mba henoy ny feon''i Jeso,
Ry mpanota very o!
Atsaharo re ny ota,
''Zay mamery aina anao;
Jeso Tompo no miantso,
Aoka tsy ho lavinao;
Dieny mbola re ny antso,
Manatona hianao.}

{2.
''Zao ny tenin''ny Mpamonjy
''Zay lazainy aminao:
"Manatona Ahy, Vonjy
Sy Mpamela helokao;
''Zaho, Tompo feno antra,
Hanome anao ny soa,
Ka helokao rehetra
Dia ho afako tokoa."}

{3.
"Izaho koa no vavahady
''zay hidiranao izao,
Sady Lalan-toratady
''zay mankao an-danitra ao;
''zaho dia Mofon''aina
Sady Ranon''aina koa;
Tsy mba misy miala maina
''zay rehetra tia tokoa."}

{4.
"Indro, efa maty Aho
Hanavotako anao;
Moa tsy hatolotra Ahy
Ny fanahinao izao?
Ry mpanota! Mihevera,
Aoka izay ny ditranao,
Manatona, manatona
Ahy, Jeso Avotrao."}
', '#1-VII-1', '#1') /end

insert into song values (13711, '371. Mankanesa ry mpanota', NULL, NULL, '
{Mankanesa ry mpanota,
Ory, jamba sy mahantra;
Henoy Jeso miantso hoe:
Mankanesa aty amiko,
Feno antra manankery,
Sady tia mba hamonjy
Izay mino tsy ho very,
Hoy ny tenin''ny Mpamonjy,
Raha miandry hihatsara
Ianao izay manota,
Dia tsy maintsy very anjara,
Lova tsy mety miova,
Tsy ny marina no anarina
Haka izay ody aina,
Fa ny meloka no antsoiny
Handray an''i Kristy hovonjeny,
Faingàna, ry olom-bery,
Anio no andro famonjena,
Raiso Kristy ''zay mahavonjy
Amin''ny fahaverezana.}
', '#1-VII-1', '#1') /end

insert into song values (13721, '372. Ry tany o, ry tany o!', NULL, NULL, '
{1.
Ry tany o, ry tany o!
Ry tany o, endre!
Ny Tompo masina sy to
Miantso mafy e!
Mifohaza, ry mpanota,
Mivaloza faingana,
Mba afoizonao ny ota,
Raiso ny fiainana!}

{2.
Manakatra ny lanitra
Ny haratsiana;
Jehovah dia vinitra,
Fa Izy masina,
Ry mpanota, mijanona
Amin''ny alehanao!
Miverena, manatona
Ny Mpamonjy, Avotrao.}

{3.
Jehovah dia masina,
Ka marina sy to,
Kanefa manamarina
Ny ta-hadio fo,
Miovà ka manenena,
fa Andriamanitra
Manome ny famonjena
Noho ny Mpanavotra.}
', '#1-VII-1', '#1') /end

insert into song values (13731, '373. Moa isaoranao ny Tompo', NULL, NULL, '
{1.
Moa isaoranao ny Tompo
Noho ny nidinany,
Tonga teto mba hanompo
Sy Handà ny tenany?
Be no tsy mihevitra
An''Ilay Mpanavotra,
Ka mihambo ny ho tsara,
Ka ho ratsy fiafara.}

{2.
Tena marina tokoa
Fa amin''ny finoana
No andraisana ny soa,
Dia ny fiainana;
nefa raha arianao
Ny fiainana vaovao,
Dia ho very ny anjara
''zay omen''ny Tompo tsara.}

{3.
Fa tsy mety tsy akory
Ny fanaon''ny sasany,
''zay sondriana matory,
Tsy mitsapa farany!
Sady tsy mba ta-handao
''zao tontolo ratsy ''zao,
Fa manompo Tompo roa
Ka manary lova soa.}

{4.
Efa tonga ny mazava,
Moa mino hianao?
Moa fantatrao fa rava
Ny fiainan-dratsinao?
Moa Jeso va izao
No mitoetra aminao?
Moa azonao tokoa
Ny mazava mahasoa?}

{5.
Voatolotra tokoa
Ny mazava ho anao,
Raha lavinao ny soa
Ka ny ratsy tianao,
Dia very foana
Ny fahasoavana,
Ary mbola raiki-trosa
Ianao ka ho tra-doza}

{6.
Tsy mba tianao ho very,
Ry Jesosy, izahay,
Ry sakaizanay mahery,
Manampia re anay,
Aoka hialoha any
Ianao, Mpanavotray,
Izahay hiara-dia
Aminao ka tsy hania.}
', '#1-VII-1', '#1') /end

insert into song values (13741, '374. Hevero, ry andevom-pahotana', NULL, NULL, '
{1.
Hevero, ry andevom-pahotana,
Ny halozan''ny fanompoanao;
Katsaho, re! ''lay manam-pahefana,
Hanapaka ireo gadranao,
Ho be ny fiadananao
Raha olona miova fo tokoa hianao.}

{2.
Hevero, fa miantra ny Mpanjaka,
Dia Jeso Kristy ''lay Mpanavotrao,
Fa be fitia Izy ka te-haka
Anao ho Azy, hifalianao;
Ny fo sy sainao ho vaovao
Rehefa raisinyy ho Azy hianao.}

{3.
Hevero, fa fifalian''ota
Ho faingan-dasa, tsy maharitra,
Fa fijaliana no tambin''ny ota
Ka mafy sady tsy mba tapitra
Izay no andosironao
Ry olona miova fo tokoa izao.}
', '#1-VII-1', '#1') /end

insert into song values (13751, '375. Raha mangoraka ny fo!', NULL, NULL, '
{1.
Raha mangoraka ny fo!
Ao ny Ray be indrafo;
He! Antsoiny hianao,
Moa tsy hody va izao?}

{FIV.
:,: O! modia, O! modia,
Fa miandry Kristy ''zao. :,:}

{2.
Miverena ankehitrio,
Tsy ampitso, fa anio;
Aza mba manary dia,
Fa modia anio, modia
[FIV.]}

{3.
''Zay rehetra maminao
Tsy hahafa-po anao::
Jeso no anjara soa
Hiadananao tokoa.
[FIV.]}

{4.
Manatona, matokia,
Mandrosoa, mifalia;
Kristy no fiainanao
Sady voninahitrao
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13761, '376. Mpanota ory sy mahantra', NULL, NULL, '
{1.
Mpanota ory sy mahantra
Henoy ny teny ho anao;
Jesosy Tompo be fiantra
Nisolo voina anao,
Ka Izy feno indrafo
Mitaona anao hiova fo.}

{2.
Jesosy Tompo sy Mpamonjy
Miantso ny anaranao:
"Mpanota very, mila vonjy,
Minoa Ahy hianao!
Ny sainao aoka ho vaovao,
Ny fomba ratsy ialao!"}

{3.
Jesosy koa mampahery
Ny sasatra ao an-dàlana;
Ny rany soa manan-kery
Hahafaka ny entana;
Jesosy tsy mandroaka
Ny ory ''zay manatona.}

{4.
ka itokionao ny teny
Naloaky ny vavany!
Ny mino Azy hovonjeny
Hanatrika ny tavany,
Sakaizako mahantra o!
Ario ny famoizam-po.}
', '#1-VII-1', '#1') /end

insert into song values (13771, '377. Tsy mba hisy tonga ary an-danitra', NULL, NULL, '
{1.
Tsy mba hisy tonga ary an-danitra,
Raha tsy mandrombaka tokoa;
Ny mpiady tsara izay maharitra
No handray anjara lovasoa.
Ety ary tery ny vavahady ao,
Kely sy mideza ny lalana mankao;
Nefa aza kivy, ry havako izao,
Fa izay malaina dia ho very.}

{2.
Maro no mitady hisakana anao,
Koa mba tohero, ry sakaiza;
Mba reseo mafy ny fahavalonao,
Fetsy izy ireo ka mahaiza,
''zay rehetra mamin''izao tontolo izao
Dia avelao, fa mamandrika anao;
Aoka mba Jesosy no hifalianao:
Eny, hifalianao tokoa.}

{3.
Satroboninahitra tsy ho azonao
Raha tsy katsahinao tokoa;
Fa hevero kosa ny fifalianao,
Raha tonga ao an-dapa soa,
Fa hahita maso ny Tompo hianao,
Ka hifaly amin''Andriamanitrao,
Koa miomàna re, ry sakaiza o!
Mba ho faly ao am-pitsarana.}

{4.
He, ny tany soa tsy mba ho hitanao,
Marina tokoa anie izany,
Ka ny ao an-danitra tsy ho azonao,
Raha fatra-pankafy ny tany;
Raha tsy matoky ny Tompo hianao
Dia helo afo no ho anjaranao,
Kao miverena, fa ny Mpanavotrao
Vonona handray anao mihitsy.}

{5.
Dia miangavy, mangataka aminao;
Mba modia, ry mpirenireny!
Fa ny Tomponao ta-handray anao izao,
Ianao tsy sitrany ho very.
Izy dia mananty ny tanany izao,
Ka ny fonao simba mba atolorinao
Hosoloan''i Jeso fo tsara sy vaovao;
''zany mba hevero, ry sakaiza!}
', '#1-VII-1', '#1') /end

insert into song values (13781, '378. Ho aiza, ho aiza izao hianao', NULL, NULL, '
{1.
Ho aiza, ho aiza izao hianao,
Ry havako izay mbola tia
Ny fomban''ny nofo manimba
anao?
Avia, ry olo-mania!
Henoy, fa miantso ny Tompo!}

{2.
Ho aiza, ho aiza izao hianao,
Ry havako mbola tamana
Ao amin''izao fivelomana izao,
Ka fatra-pikiry hitana
Izay zavatra halan''ny Tompo?}

{3.
Hevero fa tsy mba mahafaka
anao,
Ka tsy mba heken''ny Mpitsara
Ny fahamarinana ao aminao;
Ny an''i Jesosy no tsara,
Ka dia katsaho ny azy!}

{4.
Ekeo ny tenin''ny Tompo izao,
Inoy ny toky nomeny,
Ka dia ho hita ny làlam-baovao,
Izay voatoron''ny teny:
Jesosy no làlana tsara.}
', '#1-VII-1', '#1') /end

insert into song values (13791, '379. Torotoro va ny fonao', NULL, NULL, '
{1.
Torotoro va ny fonao,
Ry mpanota re izao?
Ka ifonanao tokoa
Va ny hadisoanao?
Ianao va mitomany
Ka maniry mba ho any
Amin''i Jeso, Izay nandany,
Re ny ainy ho anao?}

{2.
Ny Fanahin''ny Mpamonjy
Moa nangatahinao
Hampandray anao ny vonjy
Hahamasina anao?
Moa fantatrao tokoa
Fa ny rany mahasoa
Dia avotra naloa
Mba hafafa-troasa anao!}

{3.
Maro no manaiky ihany
Araka ny vavany,
Fa hanoa ny Mpamonjy,
Ka ho tena havany,
Nefa kosa mbola tia
Fomba ratsy, ka mania,He, ny fiantsoana
Dia tsy handray fitia
Raha ao am-parany.}

{4.
Koa dia miangavy
Aho, ry sakaizako,
Mahereza mba hitady
Fiadanana ao am-po,
Mba hatony ny Mpamonjy,
Angatahonao ny vonjy!
Dia tsy maintsy ho tra-bonjy:
Jeso no mamindra fo.}
', '#1-VII-1', '#1') /end

insert into song values (13801, '380. Mba mifohaza re, henoy izao', NULL, NULL, '
{1.
Mba mifohaza re, henoy izao
Ny hafatra ataon''ny Tomponao,
Ny Tompo, Izay Mpanjaka
manankery,
Mikatsaka anao, ry olom-bery!}

{2.He, ny fiantsoana
Dondoniny ny fo, ka hianao
Mandrindrina ny varavaranao!
Tandremo sao ho sasatra ny fony,
Sao lasa Izy ka tsy avy intsony.}

{3.
Fa indro izy mbola aty izao
Mitady hampiverina anao,
Ka aza lavina ny Tompo tsara,
Sao hianao ho ratsy fiafara.}

{4.
Faingàna àry, ka ampandrosoy
Ilay Sakaiza Izay nahafoy
Ny ainy ho anao mpanota very,
Tsy misy Mpamonjy afa-tsy Izy
irery.}

{5.
Ifony aminy ny helokao,
Hariany ka tsy ho hitanao,
Ka hianao, izay mendrika ho
voa,
Hatsangany ho zana-dRay Tsitoha.}

{6.
Ka raha renao ao anaty fo
Ny fiadanana, izay mahato
Ny famelàna ny helokao rehetra
Dia aoka mba handrasanao ny fetra.}

{7.
Akaiky re ny fahafahana,
Ka mba zakao ny hatrotrahana,
Ho avy faingana Jesosy Tompo
Ka dia ho afaka ady ny mpanompo.}

{8.
Rehefa hitanao ny Tomponao
Dia afaka ny hasasaranao,
Hijanona ny sento sy taraina
Fa hobin-danitra no hohiraina.}
', '#1-VII-1', '#1') /end

insert into song values (13811, '381. He, ny fiantsoana', NULL, NULL, '
{1.
He, ny fiantsoana
Feno fitiavana,
Teny masina sy to,
Ho anao, ry ory fo.}

{2.
Hoy Jesosy Tomponao
"Manatona Ahy re,"
"Ry miasa fatratra"
"Sy mavesatra entana!"}

{3.
"Fa Izaho hanome"
"Fitsaharan-dehibe"
"Sy fiainana vaovao"
"Hahasambatra anao"}

{4.
Ry Mpamonjy tsara o!
Saotra no atolotro;
Ka ampio aho izao,
Fa manaiky ho Anao.}
', '#1-VII-1', '#1') /end

insert into song values (13821, '382. Ry olo-matory o! Mba mifohaza', NULL, NULL, '
{1.
Ry olo-matory o! Mba mifohaza,
Fa indro Kristy
Ilay Fahazavana ''zay tena malaza,
Dia tonga aty,
Ka aza matory
Fa mba atolory
Ny fonao izao
Mba hanazavany anao.}

{2.
Aty dia matevina sady mahana
Ny zavona be;
Ny sento; taraina sy fitomaniana
Dia misy anie,
Ry olo-mahantra,
Jereo ny Mpiantra,
Henoinao izao:
Jesosy nanaovtra anao!}

{3.
Aty dia mangetana sady be ady,
Ka maro no voa;
Tsy tambo isaina ireo mitady
Fiadanana am-po.
Dia aza tomany,
Fa eto ihany
Ilay manome
Ny fahasoavana be!}

{4.
O, miandrandrà ka jereo
ao ambony,
Fa atoandro izao,
Falia tokoa zaza, tahafo ny fony,
Ny Tompo derao,
Na misy ny onja
Sy ota mandoza,
Dia mbola derao
Jehovah Mpiaro anao!}

{5.
O, mendrika hankalazaina Jehovah
Izay manome
Ny làlana sy fahamarinana koa
Sy fiainana be.
Tsilovy, ry Kristy,
Ny làla-mahitsy
Izorako ''zao,
Tantano re ny zanakao!}
', '#1-VII-1', '#1') /end

insert into song values (13831, '383. O, ry mpanota aza mandà', NULL, NULL, '
{1.
O, ry mpanota aza mandà,
Jeso miantso ka miovà;
Mba manatòna Azy izao,
Izy hamonjy anao,}

{FIV.
He, mivoha izao ny lanitra,
Ry mpirenireny lavitra;
Indro jesosy, Izy no ao,
Aiza no hisy anao?}

{2.
Be ahiahy va hianao
No tsy mandroso faingana izao?
Dia tapaho ny hevitrao,
Ka miovà hatrizao.
[FIV.]}

{3.
Mba miverena re hatrizao,
Ka hararaoty ny androano,
Fa ny aty ho simba sy ho lao,
Ka mba modia izao.
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13841, '384. Ry olom-bery, tsy ho very', NULL, NULL, '
{1.
Ry olom-bery, tsy ho very
Foana ny andronao,
Raha mby an-tsainao,
Fa ny ainao, tsy fanananao,
Ny hery misy, nefa indrisy,
Resim-pahalainana!
Aty an-tany, mora lany
Ny fiainana.
Mba mazotoa, ry havako,
Ka aoka hoheverinao
Ny andro lasa, sao ny asa
Tsy ho vitanao.}

{2.
Ka manenena, miambena,
Mivavaha hianao!
Efao ny ady, ao ny kiady
Voninahitrao.
Mba ahitsio ankehitrio
Ny dia sisa hatrizao,
Fa be ny simban''ny mpanimba
Ka migadra izao.
Sakaiza o, fantaronao,
Fa nijalian''ny Tomponao
Ny fahotàna tsy ialàna,
Ilalaovanao.}

{3.
Dia mba ampio sy tahio
Izahay, ry Tomponay,
Fa mba hiady ka hitady
Vonjy ho anay.
Homenao soa  ny manoa
Amin''ny fotoanao.
O, ry Mpanjaka, avia haka
Ny malalanao.
Ny olonao dia ta-hankao
An-danitra fonenanao.
Ry Jeso tia, mba avia
Faingàna izao!}
', '#1-VII-1', '#1') /end

insert into song values (13851, '385. Ny Mpamonjy be fitia', NULL, NULL, '
{1.
Ny Mpamonjy be fitia
No miantso hoe: Avia!
Ry mahantra, raiso izao,
Famonjena ho anao.}

{2.
Ho ny Tompo: Ry mpanota,
Manatona, fa ny ota
Voavela, anaka,
Afaka ny heloka.}

{3.
Teny mamy, re, izany,
Faly ireo mitomany,
Ka araho hatrizao,
Ka ho sambatra hianao.}

{4.
Ry Jesosy, /zay Mpiantra,
/zaho ory sy mahantra,
Ka ampio aho izao,
Fa Mpamonjy Ianao!}

{5.
Efa azoko ny teny
Ka hazoniko ireny;
Ianao, Jesosy o,
Ampy hiankinako.}
', '#1-VII-1', '#1') /end

insert into song values (13861, '386. He, Jesosy Tompo soa', NULL, NULL, '
{1.
He, Jesosy Tompo soa
No miantso mafy hoe:
"Manatona avokoa
Ry mpanota marobe!
Fa izay manatona Ahy
Tsy mba horoahiko;
Aza manana ahiahy,
Raisonao ny teniko."}

{2.
Aza mba misalasala
Ka mihevitra hoe:
Sao ny Tompo tsy hamela
Ahy efa diso be.
Tsia, tsia, ry mpanota,
Matokia marina,
Anenenonao ny ota,
Jeso hanamarina.}

{3.
He, ny rany saro-bidy
No mahafa-keloka
Ny mpanota voavidy
Ka miaiky heloka.
Eny, afaka ny revo
Noho ny Mpanavotra,
Ny devoly manandevo
Efa resy lahatra.}

{4.
Ny Mpamonjy mampifidy,
Tsy manao an-keriny,
Ka antsika ny hifidy
Sy handray ny teniny.
Koa raha misy very,
Dia tsy avy aminy;
Ny manao ny mahavery
Dia tsy mba maminy.}
', '#1-VII-1', '#1') /end

insert into song values (13871, '387. Ampihetsehin''inona', NULL, NULL, '
{1.
Ampihetsehin''inona
izao ireo vahoaka,
Ary iza no arahiny
No dia samy maika ery?
:,: Ry jamba o, fantaronao,
Jeso mandalo anao izao. :,:}

{2.
Jesosy Iraky ny Ray
Dia sady mety no mahay
Manafaka ny rofinao
Sy mampahiratra anao;
:,: Koa mitsangàna hianao,
Jeso mandalo anao izao. :,:}

{3.
O, ry mivesatra entana,
Hatony ny Mpanafaka!
Ry zaza very hatrizay,
Modia ao an-tranon-dRay.
:,: Ry jamba o, heveronao
Jeso mandalo anao izao. :,:}

{4.
Ny làlan-tsarotrao aty
Tsy misy tsy nalehany,
Ny rofy nositraniny,
Ny maty novelominy.
:,: Ny jamba faly nahare:
Indro, Jesosy mandalo re! :,:}

{5.
Ny indrafony mbola injao,
Mitety izao tontolo izao;
Ny tranontsika alehany,
Ny fo mihidy doniny.
:,: Ampandrosoy Izy hoe:
Jeso o, aza mandalo re! :,:}

{6.
Fa raha tsy ahoanao
/zao fandalovany izao,
Ka nony efa farany
Vao tia ny hovangiany,
:,: Indrisy! Tsy ho hitanao
Jeso /zay efa nandalo tao :,:}
', '#1-VII-1', '#1') /end

insert into song values (13881, '388. Tsy maintsy hateraka indray hianao', NULL, NULL, '
{1.
Tsy maintsy hateraka indray hianao
Izay ta-handova
Ny lanitra sy ny fiainam-baovao,
Izay tsy miova
Ny havan''ny Tompo ihany
No tena hahazo izany.}

{2.
Ny fo vatolampy esory izao,
Fa tsy hovonjena;
Katsaho ny fo feno aina vaovao
Fa io no ekena
Horaisina any ambony,
Ka tsy hitomany intsony.}

{3.
Ny Tompo mitondra ny hery vaovao;
Fanahy sy afo,
Izay mampiova ny fonao izao
Ho tonga fon nofo;
Ny olona lasan-ko babo
Ho afaka sady hiravo.}

{4.
Ry Masina o, ateraho indray
Izay mitaraina,
Velomy ny maty hifoha, handray
Ilay tena aina;
Mba raiso, ry Ray be fitia,
Ny zanaka adala nania!}
', '#1-VII-1', '#1') /end

insert into song values (13891, '389. Ny perla madio, ny perla vaovao', NULL, NULL, '
{1.
Ny perla madio, ny perla vaovao
No tonga hampanan-karena ny very.
Ny perla madio katsaho izao;
Ario ny ota, izay mahavery,
Tapaho ny gadra, izay mahamy!
Fa sao ho anjaranao mandrakizay.}

{2.
Ny ota nataonao, ry havana e,
Miteraka gadra, izay mahafaty;
Kanefa maro miezaka re,
Hahazo ny voan''ny ota, ka maty!
Reseo ny ota, reseo izao,
Katsaho indrindra ny perla
vaovao!}

{3.
Avia, mpanota, faingàna izao,
Faingàna ka dia hahazo hijery
Ny perla madio, ny perla vaovao,
/zay tonga hampanan-karena ny very.
Ry Jeso, /zay perla madio vaovao,
Avia hampanan-karena izao!}
', '#1-VII-1', '#1') /end

insert into song values (13901, '390. He, Jesosy Tomponao', NULL, NULL, '
{1.
He, Jesosy Tomponao
Malahelo, mitomany,
Fa izao tontolo izao
mbola mihomehy ihany,
Ka ho ratsy fiafara,
Fa mandà ny Tompo tsara!}

{2.
Indro, tevana no ao,
Fa tsy hitan''ny mpanota,
/zay mandihy faly izao
Sy manaram-po manota.
Ry adala, miverena,
Sao ho very famonjena.}

{3.
Mba heveronao izao
Fa ny tenanao ho faty;
Koa ny fifonanao
Aza ataonao matimaty!
He, ny Tompo mitomany!
Sa tsy azonao izany?}

{4.
Latsa-dranomaso be
Sy onena ny Mpamonjy;
Koa mifohaza re
Manatona ho tra-bonjy;
Fa Jesosy be fitia
Mitomany ny mania.}
', '#1-VII-1', '#1') /end

insert into song values (13911, '391. Mpanota o, mba mihevera tsara', NULL, NULL, '
{1.
Mpanota o, mba mihevera tsara,
Fa mora tapitra ny andronao
Hevero, fa ho avy ny Mpitsara,
Handinika ny raharahanao.}

{2.
Akaiky /zao ny fararanon''aina,
/zay hijinjana izao tontolo izao,
Ka dia tsarovy, fa ny Tompon''aina
Hanolotra /zay ho anjaranao.}

{3.
Izay mamafy araka ny nofo
Hijinja fanimbana lehibe;
Izay mamafy tasra amin-joto
Homen''ny Tompo fitsaharam-be.}

{4.
Izany fitsaharana izany
Dia vokatry ny ran''ny Tomponao,
Izay nalatsany taty an-tany
Hamonjy sy hanavotra anao.}

{5.
Henoy ny Mpamonjy be fitia,
/zay voahombo noho ny helokao!
Miantso mafy Izy hoe: Avia
Handray ny lanitra ho lovanao.}
', '#1-VII-1', '#1') /end

insert into song values (13921, '392. O, mba raiso izao', NULL, NULL, '
{1.
O, mba raiso izao,
Misy lovanao ao,
Indro! Jeso miandry anao!
Fa ny helokao be
Dia navelany e,
Ka fiainana omeny anao;
Novidiny hianao,
Ka ekeo hatrizao
/zay omen''i Jesosy anao.
O, mba raiso izao,
Misy lovanao ao,
Indro! Jeso miandry anao!}

{2.
Moa hadinonao va
Fa olo-mandà
Ho tra-nenina rahatrizay?
/zay ataonao aty
Hotsaraina ary
Ka tsy azo ovana indray.
Ny Mpamonjy anao
No hatony izao,
Ary Izy hihaino anao.
Moa hadinonao va
Fa ny olo-mandà
Ho tra-nenina rahatrizay?}

{3.
/zao tontolo izao
Dia mamitaka anao,
Ka afoizo fandrao manindao.
He, Jesosy nandao
/zao tontolo izao
Ary izy miantso anao.
Izy tonga indray
Ao an-dapan''ny Ray,
Ka ho velona mandrakizay.
/zao tontolo izao
Dia mamitaka anao,
Ka afoizo fandrao manindao.}
', '#1-VII-1', '#1') /end

insert into song values (13931, '393. Misy havana ao ala-trano ao', NULL, NULL, '
{1.
Misy havana ao ala-trano ao,
Vohay! Vohay!
Ka indro, Izy miandry ao,
Vohay! Vohay!
Ny lohany lenan''ny ando /zao,
Vohay! Vohay!
Mangataka hampandrosoinao.
Vohay! Vohay!}

{2.
Mikasa hanao soa aminao,
Vohay! Vohay!
Maniry hamelona anao,
Vohay! Vohay!
Ny rany nomeny ho avotrao,
Vohay! Vohay!
Ny fonao omeo ho Azy izao,
Vohay! Vohay!}

{3.
Ka Jeso Kristy no anarany,
Vohay! Vohay!
Ho hitanao ny hatsarany
Vohay! Vohay!
Ho tiany mandrakizay hianao
Vohay! Vohay!
Ka aoka ho raisinao Izy izao
Vohay! Vohay!}
', '#1-VII-1', '#1') /end

insert into song values (13941, '394. O! avia, ry mpanota!', NULL, NULL, '
{1.
O! avia, ry mpanota!
O! avia hianao,
Fa miandry Kristy Tompo,
Mba handray anao izao:}

{FIV.
:,: Manatona, manatona,
Manatona Azy ''zao! :,:}

{2.
O, avia ry mahantra!
Ho finaritra izao,
Fa akaiky ny Mpiantra
Hampifaly fo anao:
[FIV.]}

{3.
O, avia ry malemy!
Aza kivy hianao,
Fa ny vavakao ho ren''i
Jeso Kristy herinao:
[FIV.]}

{4.
O, avia sao ho very,
Ditra mba esorinao;
Ny mpamonjy no mijery
Sady ta-handray anao:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13951, '395. Modia, modia, o, ry Mpanota o!', NULL, NULL, '
{1.
Modia, modia, o, ry Mpanota o!
Tsy mba hitsangana va
Mba hiadananao?
Tsy hadosiranao va
/zao fomba ratsy izao?
Indro mitady anao
Andriamanitra,
:,: Ka ta hampiditra anao
Any an-danitra :,:
Moa tsy tianao va
Ny mba ho velona?
Koa dia aza mandà
Azy Mpamelona anao
Jeso miantso anao,
Ka mba henoinao,
:,: Raiso ny teniny /zao,
Mba hemonjena anao. :,:}

{FIV.
O, ry mpanota, miovà
:,: Fa ho sambatra tokoa,
Raha mody hianao. :,:}

{2.
Modia, modia, o, ry Mpanota o!
Tsy mbola azonao va
Ny fiantsoany?
Sa hianao ta-handà
Ny fitiavany?
Mba mankanesa aty,
Indro mivoha izao
:,: Tranon''ny Rainao ery,
Ka mba idironao :,:
O, mba modia hianao,
Sao dia ho neninao,
Sao dia handatsa anao
Ny fikirizanao;
Koa katsaho izao
Ny voninahitra
:,: Efa miandry anao
Any an-danitra :,:
[FIV.]}
', '#1-VII-1', '#1') /end

insert into song values (13961, '396. Aizan''ny lanitra hianao', NULL, NULL, '
{1.
Aizan''ny lanitra hianao,
Ry zanak''olona o?
Moa mba efa miditra ao,
Sa ao ivelany?}

{2.
Lapan''Andriamanitra ao,
Kao tsy misy mankao
Afa-tsy /zay mandray fo vaovao:
Olona masina.}

{3.
Any ny masina marobe
Velona mandrakizay,
Ka fiderana tsara no re
Eran''ny lanitra.}

{4.
Tapitra irina /zay tonga ao:
Sambatra mandrakizay;
Any ny Paradisa vaovao,
/zay hiadanana.}

{5.
Aizan''ny lanitra hianao,
Ry zanak''olona o?
O, mba hevero /zay toetrao
Mbamin''ny asanao!}
', '#1-VII-1', '#1') /end

insert into song values (13971, '397. Ry olona mbola tamana amin-dratsy', NULL, NULL, '
{1.
Ry olona mbola tamana amin-dratsy,
Hevero ny loza hihatra aminao,
Fa indro Satana zay lohan''ny ratsy
Mandefa tsipika hamono anao,
Kanefa ny Tompo, Izay Zanahary,
No tonga hamonjy, fa tsy manary;
Avia, ry olo-mahantra, hihary
Ny tena harena tsy mety ho lo.}

{2.
Jesosy mamoha anao, ry matory,
Fa Izy maniry hitarika anao.
Ilaozy ny ota, fa he, mampahory,
Henoy ny feo miantso anao!
Jereo /lay kintana, Izay tsy ho rava,
Fa indro ny aizina dia hisava,
Araho Jesosy, fa tena mazava
Izay hanazava tokoa anao.}

{3.
Ny Tompo mamelona ny efa maty,
Ka aoka hitsangana re hianao!
Faio tokoa izay mahafaty,
Katsaho indrindra ny aina vaovao!
Jesosy Mpamonjy no mba itafio,
Ka dia tandremo fa lamba madio!
Ry olo-mahantra, mba raiso anio
Jesosy, Izay mahavonjy anao.}

{4.
Jesosy Mpamonjy mitondra ny teny
Izay hahavelona mandrakizay;
Ka dia henoy ny toky nomeny,
Ry olo-maniry ho amin''ny Ray!
Faingàna tokoa, ry efa mifoha
Fa indro ny lanitra efa mivoha,
Ka faly indrindra ny Tompo Tsitoha
Hitsena ny zanany velona indray.}
', '#1-VII-1', '#1') /end

insert into song values (13981, '398. Iza no hiara-dia', NULL, NULL, '
{1.
Iza no hiara-dia
Ho any an-danitra,
Ka hanaiky sy ho tia
An''Andriamanitra?
Ry mikatsaka ny To,
Atolorinao ny fo;
/zao tontolo izao ario,
Ka ny lanitra fidio.}

{2.
Sao ho lany amin''ota
Ny anjara andronao,
Ka ny helo valin''ota
No hanjary lovanao;
Miovà re hatrizao,
Miverana hianao:
Tadiavonao tokoa
Ny Mpamonjy Tompo soa.}

{3.
Mba avia, mianara
/zay fitondrantena vao;
Mikeleza aina tsara,
Ka reseo ny nofonao,
Aza mba mamindra fo
Amin-dratsy ao am-po,
Fa ongoty avokoa
/zay misakana ny soa.}

{4.
Tsy mba misy lalan-kafa
Hiakaranao ho ao,
Koa andeha manahafa
Ny nataon''ny Tomponao.
Tsy mba mahavonjy anie
Ny fahaizan-dehibe;
Raha ny nofo no hajaina,
Dia ho very re ny aina.}

{5.
Dia ny lanitra katsaho
Ho anjara lovanao;
Ny Mpamonjy angataho
Ho mitarika anao,
Fa izay mpianany
Dia hotantanany,
Iza no hiara-dia?
Mitsangàna ka avia!}
', '#1-VII-1', '#1') /end

insert into song values (13991, '399. Ny sento ajanony', NULL, NULL, '
{1.
Ny sento ajanony,
Ry malahelo o!
Ka aza misy intsony
Izay hamoy fo!
Fa indro ny Mpamonjy
Dia ao anilanao,
Ka tonga mba hamonjy
Sy hitondra anao.}

{2.
Jesosy no nitory
Ny teny soa re,
Ny rera-po sy ory
Antsoiny mba handre;
Minoa fa ny ota
Dia voavelany,
Sakaizan''ny mpanota
No anarany.}

{3.
Miposaka ny taona
Ankasitrahana,
Ka be ny voataona
Am-pitiavana.
Hevero, ry mahantra,
Izay anjaranao!
Jesosy be fiantra
No Mpamonjinao.}
', '#1-VII-1', '#1') /end

insert into song values (14001, '400. Tompo Tsitoha, izahay mifona', NULL, NULL, '
{1.
Tompo Tsitoha, izahay mifona,
Mba mihainoa, o, mba manatona!
Ka mamindra fo aminay mpanota,
Ho afaka ota.}

{2.
Raha averina aminay mpanota
/zay mendrika ho anay ho valin''ota,
Dia ho anjaranay re ny hijaly,
Fa tsy hifaly.}

{3.
Indro /zahay fa vovoka sy tany,
Reraka, osa, sady mitomany,
Tratry ny mafy mapiferinaina,
Ka mitaraina.}

{4.
Noho Jesosy Zanaka malala,
Izay nijaly solonay adala,
Dia mba vonjeo izahay mahantra,
Ry be fiantra.}

{5.
Ray be fitia, tokin''ny mahantra,
Mba manampia anay, ry be fiantra,
Fa miandrandra izahay mijery
Anao irery!}
', '#1-VII-2', '#1') /end

insert into song values (14011, '401. Izaho nivily, ka simba ny fo', NULL, NULL, '
{1.
Izaho nivily, ka simba ny fo,
Nafoiko ny lalana tery,
Izaho nanary ny soa sy to,
Ka resin''ilay mahavery.}

{2.
Ka nony adala sy mafy ny fo,
Dia tonga ny filazantsara
Naneho ny teny mazava sy to,
Dia teny izay manambara:}

{3.
Fa tonga ny Tompon''ny lanitra izao
Hamonjy izay olom-bery;
Modia anio, ry havako o,
Araho ny lalana tery.}

{4.
Jesosy nijaly nisolo anao;
Ny fony, ny ainy nijaly;
Ario ny ratsy, fidio izao
Ny soa izay mahafaly.}

{5.
Izaho fahiny nahery nandà,
Tsy nety nifidy ny soa,
Fa noho ny herin''ny afo sy rà,
Nanaiky ny foko favoa.}

{6.
Izaho izao dia tretrika am-po,
Fa afaka tsara sy sahy
Hiala amin-dratsy, hitady ny to,
Ka mino fa Jeso no ahy.}

{7.
Ry Jeso Mpamonjy, arovy ny fo,
Ahiko ny fandrika maro!
Izaho mikasa ao anatin''ny fo
Hanaraka Anao, ry Mpiaro.}
', '#1-VII-2', '#1') /end

insert into song values (14021, '402. Ory sady mitomany', NULL, NULL, '
{Ory sady mitomany
Aho, ry Jesosy o,
Fa tsaroako izany
Vesatry ny heloko:
Koa mba todihonao.
Raiso ho Mpianatrao,
Ny fanahiko sitrano,
Atsanganonao ka tano!}
', '#1-VII-2', '#1') /end

insert into song values (14031, '403. Ry Jeso Tompo tsara o !', NULL, NULL, '
{Ry Jeso Tompo tsara o !
He, resy lahatra aho izao,
Fa be ny ratsy vitako
Mandifotra ny tenako.
Milela-paladia re,
Fa efa diso lehibe;
Koseho* tsy ho hitanao
Ny mariky ny otako,
Ampiasao ny indrafo
Ka mamelà ny heloko!

(*na: Esory)}
', '#1-VII-2', '#1') /end

insert into song values (14041, '404. Ny fasika amoron''ny rano', NULL, NULL, '
{1.
Ny fasika amoron''ny rano
Tsy tambo isaina, fa maro;
Ny kintana an-danitra koa
Tsy azo isaina avokoa.}

{2.
Ny otako toraka izany,
Ny isany tsy hita lany;
Izaho nanary ny soa,
Ka dia mahantra tokoa.}

{3.
Indrisy, izaho nania,
Ka very ny valim-pitia;
Ny foko tsy mety mangina,
Fa maro ny heloka ahina.}

{4.
Izaho maniry hahazo
Ny lova izay tsy halazo;
Iriko ho tafafindra
Ho amin''ny avo indrindra.}

{5.
Ry Jeso Mpamonjy mahery,
Variana aho fa very,
Ka noho ny ranao madio,
Ny heloko re mba ario.}
', '#1-VII-2', '#1') /end

insert into song values (14051, '405. Jesosy Kristy Tompo o!', NULL, NULL, '
{1.
Jesosy Kristy Tompo o!
Mba mamindrà fo e!
Fa raha tsy hatoninao,
Mahantra aho re!}

{2.
Mifona sy mangataka
Izaho, Tompo o,
Noho ny ranao latsaka,
Vonoy ny heloko.}

{3.
Ny famonjena vitanao,
Tsy hain''ny hafa atao;
Ny teny sy ny herinao
Dia ampy hanavao.}

{4.
Ho heriko ny indrafo
Sy ny fitiavanao,
Ka faly ny fanahiko,
Fa velon-ko Anao.}

{5.
Ny hazo-fijaliana
Anjarako aty,
Fa satro-pifaliana
Ho azoko ary.}
', '#1-VII-2', '#1') /end

insert into song values (14061, '406. Manenina aho, Ray masina o!', NULL, NULL, '
{1.
Manenina aho, Ray masina o!
Fa simba ny tsirin''ny aina;
Izay nambolenao tanatin''ny fo
Dia lanin''ny ota ka maina.}

{2.
Indrisy ny fahadisoako, re!
Fa maro tsy tambo isaina,
Kanefa, ry Tompoko, mba
mamonje
Ity ory, izay mitaraina!}

{3.
Havaozy indray ny fanahiko izao
Andrao lasa ho babon''ny helo;
Ka ny fivalozako mba raisonao,
Vonjeo ny fo malahelo!}
', '#1-VII-2', '#1') /end

insert into song values (14071, '407. 0, ry Raiko, /zay malala', NULL, NULL, '
{1.
0, ry Raiko, /zay malala,
Tonga aho zanakao,
/zay nania, fa adala,
Ka mifona aminao.
Mamelà ny fahotako
Teo anatrehanao;
Ary ny fahadalako
Aza mba tsarovanao!}

{2.
He, izaho nanantena
/zao fiainana izao,
Ka nandà ny famonjena,
/zay lazain''ny teninao;
Kanjo hitako tokoa,
Fa izao tontolo izao
Tsy mba manan-java-tsoa
Hampiadana ny ao.}

{3.
Enga aho, izay mahantra,
Mba ho isan''ny Anao;
Ianao no Ray Mpiantra,
Miantrà ny zanakao,
Ary noho ny MpamonjY,
/zay nanavotra ahy re,
Raiso aho ho tra-bonjy,
Ho Anao, ry Ray! Amen!}
', '#1-VII-2', '#1') /end

insert into song values (14081, '408. Jehovah, Ray mahari-po', NULL, NULL, '
{1.
Jehovah, Ray mahari-po,
''zay tia sy mamindra fo,
O, mihainoa ny hatako;
Mba mamindra fo amiko.}

{2.
Izaho izay mangataka
Tsy azo hamarinina ;
Indrisy! Be ny tsiniko:
Mba mamindrà fo amiko!}

{3.
Tsy tamboko isaina re
Ny fahotako marobe,
Ka be tokoa ny tahotro;
Mba mamindrà fo amiko!}

{4.
He ! Misy hanatonako
Anao, Mpandray mpibebaka
Izaho diso fatratra:
Mba mamindrà fo amiko!}

{5.
Fa Jeso no Anarana
''zay entiko mangataka
Ny hamelanao heloka:
Mba mamindrà fo amiko!}

{6.
Tsinjovy re ny zanakao
Miverina aminao izao
Ka mitaraina aminao:
Mba mamindrà fo amiko!}

{7.
O ! mba jereo ka iantrao
Izaho miandrandra Anao ;
Ny heloko vonoinao,
Fa mampisaraka aminao.}
', '#1-VII-2', '#1') /end

insert into song values (14091, '409. Ry zaza mpiodina, mba miverena ! ', NULL, NULL, '
{1.
" Ry zaza mpiodina, mba miverena ! "
Antsoin''ny Ray.
Isika hamaly am-po tsy terena;
"Indreto izahay
Miaiky fa ratsy,
Maratran''ny rafy,
Sitrano izahay,
Fa simba ny toetranay."}

{2.
Indrisy, nanota ka mendrika ho faty
Sy very izahay!
Ny razanay dia nanompo ny maty
Ka narahinay,
Ry Ray o, jereo
Ka dia vonjeo,
Mba ho zanakao
''zahay mitanondrika izao".}

{3.
"Sitraniko ny fihemoranareo,
Ry zanako o !
Tsy ampanjomboniko aminaore
Ny tavako koa."
Izany irinay
Ho re ao am-ponay,
Fa Ray Ianao
Mampionona ny olonao.}
', '#1-VII-2', '#1') /end

insert into song values (14101, '410. Ry Jeso o! Jereo', NULL, NULL, '
{1.
Ry Jeso o! Jereo,
''Ndrao maty, Ka vonjeo;
Ry Kristy, Vonivatonay!
Mba raiso izahay.
Mahantra izahay.
Ka ory ny fonay;
Ry Tompo be fiantra o!
Mba miantrà anay.}

{2.
Mpanavotra o! Jereo,
''Ndrao very, ka vonjeo;
Ry Kristy, Orimbatonay !
Avoty izahay.
Mpanota izahay,
Maloto ny fonay;
Ry Zanak'' Andriamanitra o!
Vonoy ny otanay.}

{3.
Mpanjaka o! Jereo,
''Ndrao lavo, ka vonjeo;
Ry Kristy, Harambatonay !
Arovy izahay.
Malemy izahay,
Ka kivy ny fonay;
Ry Tomponay mahery o!
Tantano izahay.}
', '#1-VII-2', '#1') /end

insert into song values (14111, '411. Ry Zanahary Tomponay', NULL, NULL, '
{1.
Ry Zanahary Tomponay,
 Mpanao izao rehetra izao !
Tsinjovy re ny olonao,
Ka miantrà ny lovanao.}

{2.
Ry Tokinay sy Tanjakay !
Mba mitalaho izahay;
Tsarovy re ny zanakao,
Ka mamelà ny helokay.}

{3.
Ry Tomponay, Mpamonjy soa !
Tariho izahay izao
Hanompo sy hanoa Anao,
Ho voninahitrao tokoa.}

{4.
Rehefa hody izahay,
Mba raiso ny fanahinay
Handray ny hasambarana
Atolotrao, Mpanavotray.}
', '#1-VII-2', '#1') /end

insert into song values (14121, '412. Jehovah Tompo o !', NULL, NULL, '
{1.
Jehovah Tompo o !
Indreto izahay
Mifona aminao ;
Fa be ny helokay ;}

{FIV.
Henoy izao, ry Ray !
Henoy ankehitrio,
Henoy ny vavakay,
Henoy ka mba valio.
Ry Andriananaharinay
Mijere,
Jesoa Kristy Tomponay
Mamonje}

{2.
Vonjeo, vonjeo, ry Ray !
Vonjeo ny olonao ;
Mifona izahay,
Isao ho zanakao,
[FIV.]}

{3.
Mpanota izahay,
Mba mamelà anay ;
Maloto ny fonay,
Sasao ho voadio;
[FIV.]}

{4.
Jereo ny Zanakao
Misolo vava anay ;
Ilay Malalanao
Mifona ho anay:
[FIV.]}
', '#1-VII-2', '#1') /end

insert into song values (14131, '413. Ry Mpamonjy, Tompo soa!', NULL, NULL, '
{1.
Ry Mpamonjy, Tompo soa!
Avy izahay izao,
Mba hanaiky sy hanoa,
Ka hiverina aminao.
Ianao no ranon''aina
Mahavelona ny fo;
Ianao no antenaina
Mba hanala ny manjo.}

{2.
/ndreto izahay mpanota
Efa diso taminao,
Mamonje sy manavota
Ny fanahinay izao,
Ianao no mahavonjy
/zay miankina aminao;
Koa raiso, ry Mpamonjyl
Ny fifonanay izao.}

{3.
Ry Jesosy be fitia!
Mba tantano izahay,
Fa ho lavo sy hania
Raha tsy hazoninao.
Tsy mba misy mety very
Eo am-pelatananao.
Fa aminao no misy hery
Mahatanjaka ny fo,}

{4.
Tompo o! Mba tanteraho
/zay rehetra mahasoa,
Ka ny otanay afaho,
Fa manimba anay tokoa.
Mampianara anay haniry
Mba hanao ny sitrakao,
Hahazoanay ny hery
Eo am-pelatananao.}
', '#1-VII-2', '#1') /end

insert into song values (14141, '414. Jeso, Vatolampinay!', NULL, NULL, '
{1.
Jeso, Vatolampinay!
O! arovy izahay,
Ka ny fonay mba sasao,
Manafaha anay izao,
Mba tsy hisy izay hanjo
Olon-tsy mahitsy fo.}

{2.
Tsy ny asa izay atao
Mahato ny didinao;
Tsy ny hazotoam-po
No mahazo manadio;
Tsy ny ranomasonay
No mahafa-trosa anay.}

{3.
Tsinontsinona izahay,
Sady be ny helokay;
Ry Mpiaro lehibe,
Jeso Tompo! Mamonje,
Fa miankina aminao
Izahay mpanomponao.}

{4.
Tomoera aminay
Raha ho faty izahay.
Ary raha hiakatra ao
Mba hiseho aminao,
Jeso, Vatolampinay!
O! arovy izahay.}
', '#1-VII-2', '#1') /end

insert into song values (14151, '415. Mifona aminao izahay', NULL, NULL, '
{1.
Mifona aminao izahay,
Andriamanitra o !
Mba mamelà ny helokay,
:, : Fa diso izahay :, :}

{2.
Mivavaka aminao izahay,
Ry Jeso Tompo o!
Avoty ny ranao izahay,
:,: Fa very izahay :,:}

{3.
Mangataka aminao izahay,
Fanahy Masina o !
Velomy ny fanahinay,
:,: Fa maty izahay :,:}

{4.
Jehova o ! Izay Iray,
Kanelo Telo koa,
Mba atokàny izahay,
:,: Ho olonao tokoa :,:}
', '#1-VII-2', '#1') /end

insert into song values (14161, '416. Mamindrà fo amiko', NULL, NULL, '
{1.
Mamindrà fo amiko,
Ry Jehovah Tompoko!
Fa miantra Ianao.
Mamelà ny heloko.}

{2.
lza no mamindra fo
Toa an''Andriamanitra ?
lza no mpanavotra
Afa-tsy ny Zanany ?}

{3.
Jeso Kristy no mahay
Mamonjy ny mpanota
Izay miankina aminy
Ka tsy mandoka tena.}

{4.
Aza mahafoy ahy,
Ry Jehovah Tompoko!
Mamindrà fo amiko,
Mamelà ny heloko.}
', '#1-VII-2', '#1') /end

insert into song values (14171, '417. Nandao Anao ela aho Raiko o !', NULL, NULL, '
{1.
Nandao Anao ela aho Raiko o !
Izao dia te-hody aho,
Maniry hiankina an-tratranao,
Izao dia te-hody aho,}

{FIV.
:,: Inty tamy aho, Raiko malala o,
Raiso ny tanako. :,:}

{2.
Maniry hanana fiadanam-po,
Izao dia te-hody aho,
Sy mba hivesatra ny zioganao
Izao dia te-hody aho,
[FIV.]}

{3.
Itambesaran''ny ota natao
Izao dia te-hody aho,
Matoky anefa fa horaisinao
Izao dia te-hody aho,
[FIV.]}

{4.
Raha tsy mendrika ny ho zanakao,
Izao dia te-hody aho,
Ataovy ho isan''ny mpanomponao
Izao dia te-hody aho,
[FIV.]}

{5.
Indro fa tazako ny sandrinao,
Izao dia te-hody aho,
Mitsotra mba hisakambina re,
Izao dia te-hody aho,
[FIV.]}
', '#1-VII-2', '#1') /end

insert into song values (14181, '418. Amorony fo madio', NULL, NULL, '
{1.
Amorony fo madio
Aho ry Jehovah o!
Ka havaozinao anio
Ny fanahiko izao.
Be ny ratsy vitako,
Ka ny heloko sasao;
Mamindrà fo amiko,
Ry Jehovah Tompo o!}

{2.
Amorony fo madio
Aho ry Jehovah o!
Ka diovy ankehitrio
Ny fiainako izao
Simba ny fanahlko,
Ka amboary ho vaovao;
Aza esorina amiko
Ny fanahy Masinao.}

{3.
Amorony fo madio
Aho, ry Jehovah o!
Ka ny foko ahitsio,
Hamasino ho Anao.
Taminao dia taminao
No mba nanotàko, Ray;
Ka ny heloko avelao,
Ny fanahiko ovay.}

{4.
Amorony fo madio
Aho, ry Jehovah o!
Ka ny fo omeko anio,
Raiso ho fanananao:
Fa mangorakoraka
Noho ny ratsy izay natao;
Fo miaiky heloka,
Ka mba raiso, Tompo o!}
', '#1-VII-2', '#1') /end

insert into song values (14191, '419. Ry Raiko o! Ry Raiko o!', NULL, NULL, '
{1.
Ry Raiko o! Ry Raiko o!
Efa nanota teo imasonao
Ity zanaka mahantranao
Ninia niala teo an-tranonao
Mangovitra ny feoko
Torovana ny foko
Kanefa tazako Ianao
Ka mamelà, mamelà, Raiko o!}

{2.
RyRaiko o! Ry Raiko o!
Ho tanterahiko ny anjarako:
Mandao ny làlako taloha.
Izay nanalavitra ahy taminao
Tratrariko ny eo aloha
Ilaozako ny lasa
Inoako ny famindramponao
Ka mamelà, mamelà, Raiko o!}

{3.
RyRaiko o! Ry Raiko o!
Atsory amiko ny tànanao
Hamelona ny heriko
Fa reraka aho, ka tantanonao,
Ny fitiavanao, Ray o l
Fanilo sady toky
Andraisako fiadanam-po,
Ka mamelà, mamelà, Raiko o!}

{4.
RyRaiko o! Ry Raiko o!
Ho olom-baovao ity zanakao
Raha tonga eo akaikinao
Fa hosasan''i Jeso Tompo tia
Tsy tapitra ny antranao,
Fa manana amby ampy
Dia manantena aho izao
Ka mamelà, mamelà, Raiko o!}
', '#1-VII-2', '#1') /end

insert into song values (14201, '420. Ry Raiko feno antra', NULL, NULL, '
{1.
Ry Raiko feno antra,
Inty aho fa ory;
Mijaly sy mahantra,
ka tsy mahatraka akory.}

{FIV.
Jesosy no nilonjy,
Ny ota vitanao;
Ny rany mahavonjy,
Nomeny ho anao.}

{2.
Nandao ny tavanao,
Nikaro-java-tsoa;
Korana sy Ialao,
Sambo-drivotra avokoa,
[FIV.]}

{3.
Izaho fahavalo
Fa tsy narianao,
Ny loza natakalo
Hasambaram-baovao.
[FIV.]}

{4.
Hanompo no iriko
Fa zanaka nania,
Ry hany itokiako
Mba ho Tompoko doria.
[FIV.]}
', '#1-VII-2', '#1') /end

insert into song values (14211, '421. Andriamanitra o, Ray Mpamindra fo', NULL, NULL, '
{1.
Andriamanitra o, Ray Mpamindra fo
Avy mankalaza Anao tokoa izahay
N''inon''inona midona, zavatra manjo,
Eo ambany elatrao matoky izahay.}

{FIV.
Mamindrà fo amiko,
Andriamanitra o!
Avelao ny heloko Jeso
Mpamonjy o!
Hazavao ny lalako Fanahy
Masino o!
Fa izay no fahasambaran'' ny
fanahiko.}

{2.
Jeso Kristy Tompo sy Zokinay,
Miaraka Aminao afa-tahotra tokoa.
Hetraketraky ny tany tsy hanimba anay,
/zay mialoka Aminao ho ampoky
ny soa.
[FIV.]}

{3.
Ry Fanahy Masina Mpanavao,
Hazavao re, ny ao am-poko
miafina ao,
Ka ny làlana ombako mba tarihonao
Ny ota mahazatra mba levony koa
ho lao.
[FIV.]}
', '#1-VII-2', '#1') /end

insert into song values (14221, '422. Ry Andriananaharinay!', NULL, NULL, '
{1.
Ry Andriananaharinay!
Ny hamasinanao.
Dia tena lehibe tokoa.
Raho te hahita Anao izahay,
Tsy maintsy tsara fo.}

{2.
Ny mason''ny anjelinao
Mahita Anao tokoa,
Ka tsy mba man jambe no ireo
Na dia mamirapiratra
Ny hamasinanao.}

{3.
Fa tsy nanota taminao
Akory izy ireo,
Tsy meloka, tsy menatra,
Ka misy hahazoany
Hijery tsara anao.}

{4.
Fa izahay, maloto fo,
Raha te-hahita Anao,
Tsy mana-mahamendrika,
Fa zanak''olombelona;
AHoana no hatao?}

{5.
Ry Andriananaharinay!
Ny ran''ny zanakao;
Ny asan''ny Fanahy koa,
No zava-mahamendrika
Anay hahita Anao.}
', '#1-VII-3', '#1') /end

insert into song values (14231, '423. He! Maharavoravo', NULL, NULL, '
{1.
He! Maharavoravo
Jeso ny fitiavanao
Izaho dia mpanota lavo.
Nefa Jeso, nareninao}

{FIV.
Mamin''ny foko, loatra
Ny fanavotana vitanao
He! Ny foko tsy afa-manoatra
Jeso hihira ho Anao.}

{2.
Izaho mpanoto adala
No nanehoam-pitiavan-tsoa
Ny heloko, Jeso malala,
Navelanao tokoa tokoa.
[FIV.]}

{3.
Endrey ny hafaliako;
Ny heloko navelanao:
He! Maivana ny fahoriako
Noho ny fanavotanao.
Mamin''ny foko, sns.}
', '#1-VII-3', '#1') /end

insert into song values (14241, '424. Tsaroako dia tsaroako', NULL, NULL, '
{1.
Tsaroako dia tsaroako,
Raiko o! Raiko be fitia,
Fa ny fahadisoako
Nodiovinao satria
Noho Jeso Mpamonjy soa
Inty izaho fa afaka
He azoko tokoa, tokoa,
Ny famelana heloka.}

{2.
Ny aizim-pahotàna.
Raiko o! Raiko be fitia.
Misolo hazavana
Mamiratra ery.
Ny làlako mahitsy izao
Mitory olona afaka
Noho ny hery vaovao:
Ny famelanao heloka.}

{3.
Endrey, ny hafaliako.
Raiko o, Raiko be fitla
Fa maivana ny diako
Avy any Kalvary.
Ny tahotaho-poko tao
Tsy reko intsony, afaka;
An-tanako tokoa izao
Ny famelàna heloka.}

{4.
Deraiko lalandava.
Raiko o, Raiko be fitla
Ny vavako hilaza
Ny vitanao tety.
Fa mbola misy marobe
Maniry te-ho afaka,
Ho azy ireny koa ame
Ny famelàna heloka.}
', '#1-VII-3', '#1') /end

insert into song values (14251, '425. Ny ratsy efa tiako', NULL, NULL, '
{1.
Ny ratsy efa tiako,
Tsy tahotro, tsy henatro;
Fa zava-bao no hitako,
Te hijery izany aho.}

{2.
Ao ny hazo fijaliana
Ao ny Tompo efa maty;
Izy no nijery ahy
Tsy tezitra, fa niantra.}

{3.
Hoy ny fijeriny ahy,
"Ianao mamono ahy"
Dia hitako mahantra aho
Ny heloko namono Azy.}

{4.
Aiza no handosirako
Ndrao hidiran-doza aho?
Iza no hamonjy ahy,
Feno ota, feno loto?}

{5.
Nijery ahy indray Izy;
"Avelako ny helokao"
Fa ny ràko no manadio
Hahazoanao fiainana.}

{6.Maty Kristy,
Maty i Jeso! be fitia
Faly malahelo aho
Sambatra, manana ody aina}
', '#1-VII-3', '#1') /end

insert into song values (14261, '426. Nanaton''Anao, Mpanavotra o', NULL, NULL, '
{1.
Nanaton''Anao, Mpanavotra o,
Ny olo-mahantra fahiny
Ny rofiny be, ny seton''ny fo
Naneho fa soa no niriny.
Tsy nety ho sasatra Ianao,
Tsy nisy nolavina akory,
Fa samy nandray ny indrafonao
Ka ravo ny olona ory.}

{2.
Manatona Anao, ry be indrafo,
Ny olo-mahantra anio,
Fa be siasia sy reraka am-po,
Ka raisonao ankehitrio!
Ny maty omeo aina vaovao,
Ny sasatra ampiadano,
Ny masonay jamba mba hazavao,
Ny rofin''ny fonay sitrano}

{3.
Ka rahatrizay hanatona Anao
Ny tsy ho farofy intsony,
Fa hisy ny Alahady vaovao,
Ka sitrana tsara ny fony,
Izay tany soa misy Anao
No hisy ny olonao koa,
Hahita ny voninahitrao ao,
Ry zoky malala tsitoha.}
', '#1-VII-3', '#1') /end

insert into song values (14271, '427. Ho vavolombelonao', NULL, NULL, '
{1.
Ho vavolombelonao
N''aiza n''aiza no haleha
Izay no faniriana vao
Sy hataoko rehareha
Ka raha asa no atao
Inty tokoa fa vonona aho
(ho vonona aho)
Tsy hiala na handao
Tompo o iraho aho.}

{2.
Ho vavolombelonao
Eran-tany rha ilaina
Toy ny loharano vao
Izay mamelombelona aina.
Hanasoa ny tany hay
Efa mihamaina taho
(o! Mihamaina)
Tompo o, inty indray
Alefaso, vonona aho.}

{3.
Ho vavolombelonao
Ao anaty fitsapana,
Ka hijoro ho Anao,
Na fefana, na raràna,
Tsy hiahotra ny dia,
Fa Ianao naniraka ahy
(naniraka ahy)
Tsy hiahotra aho satria
Amiko Ianao Mpitahy.}
', '#1-VII-4', '#1') /end

insert into song values (14281, '428. Reko ''zao ry Tompo', NULL, NULL, '
{1.
Reko ''zao ry Tompo
Izany antsonao !
Miantso izany mpanompo
Mba ho irakao!}

{FIV.
Inty! Inty! Inty aho Jeso
Inty! Inty! Iraho aho Jeso
Ekeko re ny ho miaramilanao
Ho vonona hanao ny sitrakao!
Ka iraho!}

{2.
O ! diovy Tompo
Izany molotro !
Ka ny fo madio hanompo
No atolotro
[FIV.]}

{3.
Tsy handeha irery,
Fa hiaraka Aminao !
Ka dia mba omeo ny hery
Sy Fanahinao
[FIV.]}

{4.
Reko izao ny toky
Hoe : "Momba ahy Ianao,
Ry Jesosy Tompo sy Zoky
Vonona aho" izao.
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14291, '429. /nty ny alonao, Jeso Tompo o', NULL, NULL, '
{1.
/nty ny alonao, Jeso Tompo o,
Fa nandre ny antsonao.
He, maniry ny mba hotarihinao
Ho akaikinao kokoa}

{FIV.
Mba tariho aho Jeso Tompo o.
Ho akaikinao kokoa
Mba tariho aho Jeso Tompo o.
Mba ho anao tokoa.}

{2.
Tsy ny sitrako, fa ny Sitrakao
No iriko mba hatao
Fa navotanao ny mpanomponao
Ka manaiky ho Anao.
[FIV.]}

{3.
Raha mitokana mba hivavaka,
Ka miresaka Ammao,
Vevombelona ny fanahiko,
Tafakambana Aminao.
[FIV.]}

{4.
Raha avy re izay hodiako,
Hody Aminao Indray,
Ao ny saiko vao tena afa-po
Tonga ao an-tranon-dRay.
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14301, '430. Endrika sariaka ', NULL, NULL, '
{1.
Endrika sariaka no
Anompoako Anao Jehovah Ray,
Fa tapaka ny hevitro;
Izaho dia olonao mandrakizay.
He! Ny foko afa-jaly
Tena ravo, sady falifaly,}

{FIV.
Ka: endrika sariaka no
Anompoako Anao Jehovah Ray.}

{2.
Fa tsaroan''izany saiko,
Mandra-maty ho tadidiko,
Ny nanavotanao ny aiko,
Ilay fanahy sarobidiko.
Fa nomena fahendrena
Izay ekena fa ny famonjena.
[FIV.]}

{3.
Be ny zavatra mihatra
Mampahory sy mandrera-po;
Kanefa ireny tsy handratra
Ny arovanao, ry Ilay Tsitoha.
Fa ny fony tena tony
Tsy mba kivy, tsy mba taitra intsony
[FIV.]}

{4.
Misy ve ny mampanahy
Sy ny zava-manahirana ?
He, tsy kivy ny fanahy
Fa hijoro, ka ho mirana.
Fa Jehovah, Ray mahery,
Miandany sady koa mijery,
[FIV.]
}
', '#1-VII-4', '#1') /end

insert into song values (14311, '431. Na be no mahareraka', NULL, NULL, '
{1.
Na be no mahareraka
Tsy mety kivy ny mino
Ny Tsitoha no heriny
Ny lehibe no tohany.}

{2.
Ny hani-mandrakariva
Mnfo avy any an-danitra
Ny sotroin''isan''andro
Dia rano mahavelona}

{3.
Manda afo no arony
Finoana no ampingany
Vavaka no fofon''ainy
Odiaina no tànany.}

{4.
Ho lavorary ny diany,
Ho resy ny fahavalony
Dia sitraka no hitany
Voninahitra ho azony}

{5.
Mahereza ''lay malala
Aza ketraka hanompo
Lasa ny Mpitari-dàlana
Hanamboatra ho anao.}
', '#1-VII-4', '#1') /end

insert into song values (14321, '432. Mandra-pihavin''ny Tompo, mivavaha', NULL, NULL, '
{1.
Mandra-pihavin''ny Tompo, mivavaha
Ny teny tsara nomeny tadidio
Ny adidinao tontosay sy efao
Mijery ny ataonao ny Ray}

{FIV.
Raha mpanompo tsara hianao
He! Ny Tompo faly aminao.
Ka tanteraho ireo adidinao
Mba ho mpanompo mahasoa.}

{2.
Mandra-pihavin''ny Tompo, miasà
Ampitomboy ireo talenta azonao
Vola, fahaizana, aina, hery koa
No anompoy ny Tompo Tsitoha
[FIV.]}

{3.
Moa tianao va Ilay Mpamonjy
Tompo soa
Ny ràny anie nanavotany anao!
Ny hafany tanteraho sy hajao
Valio ny fanavontany anao
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14331, '433. Mba taominao ny fo izao', NULL, NULL, '
{1.
Mba taominao ny fo izao.
Ry Jeso be Iitia
Hanarahako Anao
Ka mba tsy hania.}

{2.
Mba taominao ny fo izao.
Hamoizako ny tany.
Fa izao rehetra izao
Dia mora lany.}

{3.
Mba taominao ny fo izao.
Hanaraka ny dia,
/zay nomban''ny olonao
Tsy naniasia.}

{4.
Mba taominao ny fo izao.
Hahaizako hifaly,
Fa aty ny olonao
Ory sy mijaly.}

{5.
Mba taominao ny fo izao.
Ho any amin-dRainao,
Mba ho reko hatrizao
Ny hafin''ny ainao.}
', '#1-VII-4', '#1') /end

insert into song values (14341, '434. Ny foko ry Mpamonjy', NULL, NULL, '
{1.
Ny foko ry Mpamonjy,
Atolotro Anao,
Maloto ka diovy.
Mba ho fonenanao
Ny ranao, izay nalatsaka.
No aoka mba hanafaka
/zao fahotako be izao.}

{2.
Ny fahoriam-poko
Atolotro Anao,
Fa he, mampitoloko,
Manindry mafy ao;
O, ry nijaly an-tsitrapo,
Tohàny ny fanahiko
Mba tsy hamoy fo izao}

{3.
Ny fifaliam-poko
Atolotro Anao,
Arovy tsy haloto,
Diovy ho Anao,
Ampio ho tanteraka,
Ho tonga ao an-danitra
Hanatrika ny tavanao !}

{4.
Ny tenako rehetra
Atolotro Anao.
Anao tsy misy fetra.
Anao, re, hatrizao
Ka raiso ho fanananao
Hanompo sy hanajo Anao
Ry Tompo sy Mpanjaka o !}
', '#1-VII-4', '#1') /end

insert into song values (14351, '435. Iza no hanoa ny Tompo lehibe ?', NULL, NULL, '
{1.
Iza no hanoa ny Tompo lehibe ?
Iza no hanaja Azy tsara re?
Iza no hitady olon-kafa koa
Mba hanaiky sy handray ny Tompo soa?
Indreto, Jeso Tompo! Izahay izao,
Avy mba hanompo, avy ho Anao.}

{2.
Tsy an-tery re no hilatsahana
Tony ny olom-bery nandevozina;
Ta-hanompo tsara izahay izao
Ta-hamely koa izay toherinao;
Eny, Jeso Tompo! Ta-hanao tokoa
Izahay mpanompo izay nasian-tsoa.}

{3.
Na dia ta-hanompo aza izahay,
Be ny fahavalo iadianay,
Izahay malemy feno tahotra,
Mba tsy horeseny, manampia anay
Kapiteny sahy! Entonao izao
Mba hitomban-dahy ny mpiadinao.}

{4.
Mafy ren ny ady, maro be ny voa,
Misy tsy mahandry, be ny maty koa;
Izahay mpanompo miandrandra Anao;
Tompo izay tsy toha! Mba herezonao;
Tompo manan-kery! Hatanjahonao
Izahay, sao very ka afaka aminao.}
', '#1-VII-4', '#1') /end

insert into song values (14361, '436. Ny foko ry Jehovah o!', NULL, NULL, '
{1.
Ny foko ry Jehovah o!
Mba ento ho Anao
Diovy ho fonenanao,
Ho tsara sy vaovao.}

{2.
Fanahin''Andriamanitra o!
Midina ato koa,
Ovay ny saiko mba hadio
Sy masina tokoa.}

{3.
Ny aiko no navotanao,
Ry Jeso be fitia!
Mba hanompoako Anao,
Ry Tompoko! avia.}

{4.
Ry sofiko! mba mihainoa;
Ry maso! mijere;
Ry tongotra aman-tanako!
Hajao ny Tompo e.}

{5.
Ry tena sy fanahy koa!
Avia hankato
Ny tenin''ny Mpanavotra,
''Zay masina sy to.}
', '#1-VII-4', '#1') /end

insert into song values (14371, '437. Ry Jeso Kristy Tompo', NULL, NULL, '
{1.
Ry Jeso Kristy Tompo
O! Ampio ny olonao!
Ampianaro mba hanompo
Sy hanao ny sitrakao;
Malemy aho re
Ka omeo ny herinao
Tsy hivily, tsy hania,
Fa hanaradia Anao.}

{FIV.
O! vonjeo, O! vonjeo
Ka tantano sy jereo;
Aza afoy izahay,
Fa Ianao no avotray.}

{2.
Ny foko mba diovy,
Ry Mpamonjy lehibe!
Ka ambeno sy arovy,
Sao dia very aho re;
Iriko indrindra koa
Ny hitombo sy hamoa.
Ka ny voka-tsoa rehetra
Ho Anao, ry Jeso o!
[FIV.]}

{3.
Mba raisonao, ry Tompo!
Ho Anao tokoa izao
''zay anananay rehetra,
Ka anarano fo izao,
Fa maty hianao
Noho ny fitiavanao,
Ka tsy hainay hadinoina
Ny fitiavanao anay;
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14381, '438. Avia, Jehovah Tomponay', NULL, NULL, '
{1.
Avia, Jehovah Tomponay,
Handray ny vavakay,
Fa mitaraina izahay,
Valio ny hatakay.}

{FIV.
Avia, avia, handray ny fonay,
Avia, avia, ry Tomponay.}

{2.
Savao ny alahelom-po,
Mba hifalianay;
Na mafy aza ny manjo,
Matoky izahay;
[FIV.]}

{3.
Avia, ka taomy izahay,
Ho tonga zanakao;
Mba averenonao indray,
Fa te-ho olonao;
[FIV.]}

{4.
Manolo-tena izahay
Mba ho mpanomponao
Ny fonay no atolotray,
Ka raiso ho Anao;
[FIV.]}

{5.
Rah avy ny hodianay
Ho any aminao,
Mba raiso ny fanahinay
HO ao an-dapanao;
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14391, '439. Mba taomy aho e ', NULL, NULL, '
{1.
Mba taomy aho e,
Ry tompo o!
Ho tonga zanakao
Madio am-po.
Mba taomy aho e,
Ho tia anao tokoa!
Ny fo atolotro
Mba raisonao.}

{2.
Tariho aho e,
Ry Jeso o!
Aza ilaozanao,
Fa be manjo.
Trotroy an-tratranao
Ny ondrikelinao,
Ka ento mody izao
Ilay nania.}

{3.
Diovy aho e,
Fanahy o!
Diniho sy hazavao
Ny toetro.
Diovy aho e,
Ny fo ovay ho vao,
Ka ny fanahiko
Onenonao.}
', '#1-VII-4', '#1') /end

insert into song values (14401, '440. Ry Kristy o, Mpanjakako!', NULL, NULL, '
{1.
Ry Kristy o, Mpanjakako!
Ny foko ho Anao;
Ny feoko hasandratro
Hihira ho Anao.}

{2.
Mba raiso ny fanahiko
Hanao ny sitrakao;
Ny tenako, ny heriko,
Dia ento ho Anao.}

{3.
Ny tongotro, ny tanako,
Hiasa ho Anao;
ny androko, ny volako,
Omeko ho Anao.}

{4.
Izaho dia tsy mendrika
Anao, ry Tompo o!
Kanefa re, mba raisanao
Ny foko ho Anao.}
', '#1-VII-4', '#1') /end

insert into song values (14411, '441. Raiso aho, Tompo o !', NULL, NULL, '
{1.
Raiso aho, Tompo o !
Fa te ho mpanomponao ;
Raiso koa ny androko,
Ka anarano fo izao.}

{2.
Raiso re ny tanako
Mba hanao ny asanao;
Raiso koa ny tongotro
Ho fanirakirakao.}

{3.
Raiso ny fahaizako
Ento mba hanaja Anao;
Raiso koa ny volako
Ho Anao tokoa izao.}

{4.
Raiso re ny vavako
Hampiseho ny hafatrao;
Raiso koa ny feoko
Ho mpanandratra Anao.}

{5.
Raiso ny fanahiko
Diovy ho tempolinao;
Raiso aho Tompo o!
Ho Anao mihitsy ''zao.}

{6.
Ho anao manokana,
Ry Mpamonjy, Tompo soa!
Ho Anao dia Anao tokoa.
Ho Anao mandrakizay.}
', '#1-VII-4', '#1') /end

insert into song values (14421, '442. Endrey! ''ty fahotana', NULL, NULL, '
{1.
Endrey! ''ty fahotana
Mamely ao am-po,
O! ady mampahana,
Ka mafy ny manjo;}

{FIV.
:,: Vonjeo, ry Mpiantra!
/zaho zanakao
Fa tery sy mahantra
Miady lava izao. :,:}

{2.
Ry Antoky ny aiko!
''Zay mahafatra-po
Ry toky antenaiko!
''Zay tanjaky ny fo;
Ny tena sy fanahy.
Apetraka aminao
Ka ataovy lehilahy,
Mpandresy toa Anao.
[FIV.]}

{3.
Izaho no Mpamonjy,
Ka matokia tokoa.
Izaho mahavonjy,
Koa antenao ny Soa;
Ny sandriko mahery
No iankinonao
Ianao dia tsy ho very.
Minoa hianao.
[FIV.]}

{4.
Ry Tompo manan-kery!
Ny fitiavanao
Fitia feno hery,
Ka raisiko izao;
Ny dera aman-daza
No mendrika ho Anao
Ny foko hankalaza,
Ny voninahitrao.
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14431, '443. Raiso ho Anao ny foko ', NULL, NULL, '
{1.
Raiso ho Anao ny foko,
Ry Jehovah Raiko o!
Mitoera aty ampoko
Mba ho tompo-trano ao
:,: O, ry Raiko! O! ry Raiko!
Mitoera ao izao. :,:}

{2.
Raiso ho Anao ny foko,
Ry Jesosy Tompo o!
Tsy mba vola, fa ny foko,
No atolotro Anao
:,: Raiso, Tompo! Raiso, Tompo!
Fa fanati-tsitrapo. :,:}

{3.
Raiso ho Anao ny foko,
Ry Fanahy Masina o!
Hazavao ny ao am-poko,
Diovy ho tempolinao;
:,: indro, raiso! Indro, raiso!
Itokàny ho Anao. :,:}

{4.
Tsy harindriko ny foko.
Fa vohako ho anao;
Ka midira aty am-poko,
Manjakà irery ao;
:,: eny, Tompo! Eny, Tompo!
Raiso aho ho Anao. :,:}
', '#1-VII-4', '#1') /end

insert into song values (14441, '444. Ry Raiko feno antra !', NULL, NULL, '
{FIV.
Ry Raiko feno antra !
Inty ny zanakao,
Ny foko dia mahantra
Raha lavitra aminao;}

{1.
Fa tany karankaina
Izao tontolo izao
Ka reraka ny aina
Mihataka aminao.
[FIV.]}

{2.
Nivezivezy aho
Nikaro-java-tsoa,
Izay rehetra hita
Nandramana avokoa;
Nandany taona maro
Nanaran-tsitrapo,
Kanefa tsy nahazo
Izay mahafa-po.
[FIV.]}

{3.
Mamitaka mihitsy
Izao tontolo izao,
Ny mamy dia mangidy,
Izay tena soa tsy ao;
Koa aza mba adalainy
Intsony re, ny fo!
Andeha mba ho hendry
Hatramin''ny anio.
[FIV.]}

{4.
Ry Jeso, Mofon''aina
Sy ranon''aina koa!
Ianao no mahatony
Izay filan''ny fo;
Ianao no mahavoky
Ianao no manome
Izay irin''ny foko,
Dia hasambaram-be
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14451, '445. Jeso mora fo indrindra', NULL, NULL, '
{1.
Jeso mora fo indrindra
Tsara sady mahasoa,
Tsy miova tsy mifindra,
Tompo be fitia tokoa.
Mba iriko ho toa Azy
Tsy mba mety soso-po
Ka hionona ny foko
Raha avy ny manjo.}

{2.
Jeso tia, nanetry tena
Sy nandefitra tokoa,
Nefa Tompo be harena
Sy Mpanjaka foto-tsoa.
Mba iriko ho toa Azy
''Zay ninia ho ory koa;
Ka ny sainy no fidiko,
Fa fiainana tokoa.}

{3.
He! Manolo-tena aho,
Ho Anao, ry Tompo soa!
Ka ny heloko rehetra
Mba sasanao avokoa.
Enga ka omenao ahy
Fo madio sy vaovao,
Mba hahaizako manefa
''Zay rehetra sitrakao!}
', '#1-VII-4', '#1') /end

insert into song values (14461, '446. Atolotro Anao ny tenako, ry Raiko !', NULL, NULL, '
{1.
Atolotro Anao ny tenako, ry Raiko !
Fa Ianao no tena loharanon-tsoa;
Izao tontolo ''zao manintona ny saiko
Hiataka aminao, ho babony tokoa.}

{2.
Atolotro Anao ny tenako, ry Raiko!
Ho eo an-tananao hosakambininao,
Hohatanjahinao ny tena sy ny saiko,
Hisikina tokoa hiady ho Anao.}

{3.
Atolotro Anao ny tenako, ry Raiko!
Fa izany ''ndrindra re no hetahetako;
Ny ran''ny Zanakao mitory ao an-tsaiko
Mba hanolorako Anao ny tenako.}
', '#1-VII-4', '#1') /end

insert into song values (14471, '447. Ianao ry Kristy Tompo', NULL, NULL, '
{1.
Ianao ry Kristy Tompo,
No hasiniko izao,
Avy aho fa mpanompo,
Hiankohoka Aminao,
Raiso re ny dera, saotra
Tena mendrika ho Anao;
He! Ny foko faly loatra,
Faly mba hanaja Anao}

{2.
''Zao no tena faniriako:
Ny ho velona aminao,
Ka hitoeranao ny foko,
Fa tsy holalovanao;
Eny, Tompo o! Monena,
Mba monena ao tokoa;
''Zay no tena famonjena,
''Zay no mahafatra-po}

{3.
Tsy ho orin-java-tsoa
''Zay rehetra itoeranao,
Tena sambatra tokoa,
Raha ny fo no lapanao,
Mandrosoa, fa vohako
Ho Anao ny foko ''zao;
Eny, Tompo, fa vohako,
Koa midira Ianao.}
', '#1-VII-4', '#1') /end

insert into song values (14481, '448. Ry Tompo o! Tariho', NULL, NULL, '
{1.
Ry Tompo o! Tariho
Ny ondrikelinao,
Tantano sy vimbino
Hanao ny sitrakao;
Raho misy hafaliana,
Dia taomy mba ho tia;
Raho sendra fahoriana,
Sakano tsy hania,}

{FIV.
Ry Jeso o tazano,
Ny lalanay Izao,
Tariho sy tantano
Ho ao am-balanao;
Anio dia mikasa
Hanolo-tena koa
Fa nenina ny lasa
Hiezaka tokoa.}

{2.
Anio ho hafaliako,
Ny foko lasanao,
Ny maminao no tiako,
Hatao ny sitrakao ,
Ny androko rehetra,
Atolotro Anao;
Anao tsy misy letra,
Ka raiso, anjakao:
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14491, '449. ''Njay reko Tompo o!', NULL, NULL, '
{1.
''Njay reko Tompo o!
''Njay re ny feonao
Miantso ahy Ianao
hankany Kalvary;}

{FIV.
Tompo lehibe!
Raiso aho e;
Diovy ny ra soanao
Izay tao Kalvary,}

{2.
Malemy aho e,
Ka mitaraina hoe:
Vonjeo ny aiko Tompo o!
Vonjeo ny olonao:
[FIV.]}

{3.
Ny fiadanam-po
Mandresy ny manjo;
Esorinao ny tahotro,
Ry Tompo o vonjeo:
[FIV.]}

{4.
Mpanota tsy mandà,
Ry Tompo! mamelà;
Ny fahotako lehibe
Ifonako tokoa:
[FIV.]}

{5.
Ny foko tsy madio
Mba diovinao anio;
Maloto aho, Tompo o!
Sasao ho voadio:
[FIV.]}

{6.Ny foko no sasao,
Ka diovinao izao;
Ny fahotako marobe
Levony avokoa:
[FIV.]}
', '#1-VII-4', '#1') /end

insert into song values (14501, '450. Jeso Kristy be fiantra!', NULL, NULL, '
{1.
Jeso Kristy be fiantra!
Mba jereo izahay
Very amin''ota,}

{2.
Ianao Mpiandry tsara,
Raisonao izahay
Very toy ny ondry,}

{3.
Fombanay ny tia ratsy,
Mba ovay ho vaovao
Ny fonay ''zay ratsy,}

{4.
Zazabodo sy mahantra
Izahay raha aty,
Tsy mba manan-kery,}

{5.
Ianao no vatolampy,
Tanjakay, Avotray,
Hery sy Ampinga,}
', '#1-VII-4', '#1') /end

insert into song values (14511, '451. Tompo o! Tsy takatry ny saiko', NULL, NULL, '
{1.
Tompo o! Tsy takatry ny saiko
Ny fitiavanao tsy nohajaiko
Kanjo io no hany antenaiko
Mba hamonjy ahy ''zao
(hamonjy''zao)}

{FIV.
(Feo 1)
Tompo o voababonao
Zanak''Andriamanitra
Mamy re!
''Zany fitiavanao ry Jeso o!

(Feo 2)
Tompo o dia resinao
ny foko ''lay nandà
Voababonao tokoa tokoa
Ka nasandratrao ho
zanak''Andriamanitra
Ho tarihinao an-danitra.
Mamy dia mamy re
tsaroan''nyaiko ''zao
/zany fitiavanao ry Jeso o!}

{2.
Ny fandavan-tenanao tsaroako
Fa fandavan-tena nahitako
Ny fitiavanao izay inoako
Fa mambomba ahy izao.
(Mambomba izao)
[FIV.]}

{3.
Ry Jesosy, tsy mba misy haiko
Afa-tsy manolotra nyaiko
Eo an-tananao ''zay andrandraiko
Hampijoro ahy izao.
(Hijoro izao)
[FIV.]}
', '#1-VII-5', '#1') /end

insert into song values (14521, '452. Tompo o ! Ovay ny foko', NULL, NULL, '
{1.
Tompo o ! Ovay ny foko
Mba ho mora anarina
Toy ny an''ny zazakely
Tsotra sady marina,
:,: Tsy mialona, tsy maditra,
Fa manao ny sitrakao. :,:}

{2.
Zaza, ka tsy te-hifidy
Izay homenao ahy anio,
Ny ho azoko ampitso
Dia ankinina aminao;
:,: Koa tsy manahy aho
Fa efa niantohanao. :,:}

{3.
Zaza, tsy maharo tena,
Fa miankina amin-dray.
Tsy mahery, tsy mba hendry,
Ary tsy mahay mandeha;
:,: Ianao no Raiko tia,
Raiko be fahari-po. :,:}
', '#1-VII-5', '#1') /end

insert into song values (14531, '453. Mihirà, ry mponina ao an-danitra', NULL, NULL, '
{1.
Mihirà, ry mponina ao an-danitra,
Tonga izao ny very fahizay,
Ka ny Rainy faly sy tinaritra
mba handray ny zanany indray;}

{FIV.
Mihobia re ny lanitra,
Ka derao Andriamanitra
Eny mihobia, re, ry mina o,
Ka hirao ny fihiram-baovao!}

{2.
Mihirà, ry mponina ao an-danitra,
Fa ilay very dia hita izao,
Voaray ho zanak''Andriamanitra,
Efa tonga olona vaovao.
[FIV.]}

{3.
Mihirà, ry mponina ao an-danitra
Lehibe ny fifalianay,
Ka derao izao Andriamanitra,
Fa ny maty velon. indray!
[FIV.]}
', '#1-VII-5', '#1') /end

insert into song values (14541, '454. Veloma no apetrako', NULL, NULL, '
{1.
Veloma no apetrako,
Fa tapaka ny hevitro,
Handao ny fahazarako,
Fa Kristy no harahiko,}

{FIV.
Veloma hianao,
Ry izao tontolo izao!
Izaho tsy tamana aty,
Veloma hianao!}

{2.
Ny làlan''ota hitako
Namono ny fanahiko,
fa he, ery mba tazako
Ilay tany soa mamiko,
[FIV.]}

{3.
Mpanota o, nahoana
No tsy miaraka aminay?
Fa mbola misy toerana
Izay ho anao sy ho anay.
[FIV.]}

{4.
Veloma, fa ao aloha ao
No tany izay honenako,
Ka maika aho mba hankao
Handray ny fitsaharako.
[FIV.]}

{5.
Jerosalema tazako,
Ka velona ny hi rako,
Fa any no honenako
Hidera ny Mpanjakako,
Ho any aho izao,
Ho any aho izao,
Izaho tsy tamana aty,
Ho any aho izao!}
', '#1-VII-5', '#1') /end

insert into song values (14551, '455. Ny androm-pahasoavana', NULL, NULL, '
{1.
Ny androm-pahasoavana
Maneho zava-baovao;
Mahazo fahasambarana
Izao tontolo izao.
Ravoravo ny efitra maina
mbamin''ny tany karankaina;
He, torina ny teny soa,
Ka dia afaka avokoa
Ny olom-bery maniry ho velona.}

{2.
Ka miandrandra ny lanitra
Ny malahelo izao:
Mihoby izy fa sambatra
Nahazo aina vaovao.
He, vahana ireo migadra,
Afaka ny midradradradra,
Mitsambikina ny mandringa,
Faingana izy sady kinga,
Ary mihoby ny lelan''ny moana.}

{3.
Ny very no tadiavina
Mba hody amin''ny Ray;
Ny maty dia velomina
Ka ravo mandrakizay.
Izy dia mifady ny ota,
Noho ny lova tsy ho tonta,
Sady manaraka Azy Tompo,
Izay voaeso sy voahombo,
Ambara-podiny any an-danitra.}
', '#1-VII-5', '#1') /end

insert into song values (14561, '456. He, sambatra ny navelanao', NULL, NULL, '
{1.
He, sambatra ny navelanao
Ny helony, ry Jehovah!
Ny otany voasaronao,
Ka mba isaina koa;
He, sambatra ny mahitsy fo,
Fa azy ny lovasoa.}

{2.
Ny fo nony nampanginiko,
Ny heriko nihala ny;
Ny ota izay nafeniko
No dia nahatonga izany,
Ka ketraka ny fanahiko,
Nisento sy nitomany.}

{3.
Ny otako dia nambarako,
Niantso ny Tompo aho,
Niaiky ny fahadisoako,
Nivalo sy nitalaho,
Ka dia nomeny indrafo,
Mba ho afa-keloka aho.}

{4.
Jehovah no fialofako
Sy manda miaro ahy;
Ny rano, na dia tondraka,
Dia tsy hahatratra ahy;
Ny hobin''ny afa-keloka
No ampihirainao ahy.}

{5.
Ry olon''Andriamanitra,
Vonjeo ny andro soa,
Ny otanao izay miafina,
Itsorinao avokoa;
Raho mita ilay ony maizina,
Dia aronao ny Tsitoha.}
', '#1-VII-6', '#1') /end

insert into song values (14571, '457. Jeso hany iriko', NULL, NULL, '
{1.
Jeso hany iriko,
Loharanon''aiko,
Sy sakaiza soa!
Mamiko indrindra
Ny ho tafafindra
Any Aminao,
Na dia toa takona,
Ianao no ao an-tsaiko,
Sady tohan''aiko.}

{2.
Loza tsy misava,
Sento lalandava
No mitranga izao;
Notrotroiny aho,
Ka ny fahavalo
Tsy mahazo ao.
Na be aza ny mania,
Tsy ho kivy ny fanahy,
Jeso mbola ahy.}

{3.
Ny haren''ny tany,
Na dia tsara ihany,
Tsy ahoako;
Ianao irery
Lova tsy ho very,
Reharehako!
Ka na fahoriam-be
No mihatra amin''aiko,
Ianao tsy foiko.}

{4.
Raha ao am-poko
Ianao, ry Tompo,
Ka manjaka ao,
Ireo ahiahy,
Nahavalaka ahy,
Dia afakao.
Na mavesatra entana,
Ianao no antenaiko
Sady Tohan''aiko.}
', '#1-VII-6', '#1') /end

insert into song values (14581, '458. Tompo tia o, midina', NULL, NULL, '
{1.
Tompo tia o, midina,
Mitoera aminay.
Ka fenoy fifaliana
Ao anatin''ny fonay.
Ianao, ry be fiantra,
Tomponay Mpamindra fo,
No Mpamangy ny mahantra,
Ka midira ao am-po!}

{2.
Aoka re ny fofon''ainao
Hahavelona anay,
Hanahafanay ny sainao
Hampiadana anay.
Ny fitiavanay ny ota
Mba vonoy, ry Tomponay,
Manafaha anay mpanota,
Ry Andriamanitrayl}

{3.
Ny mpanomponao ampio
Mba handraisany Anao,
Aza afoy fa mba tahio,
Aoka ho tempolinao,
Ianao no hotompoinay,
Ry Andriamanitra,
Ianao no hoderainay
Hatrary an-danitra.}

{4.
Tanteraho ao am-ponay
Ny fiainana vaovao.
Ny fanahy sy ny sainay
Mba fenoy ny herinao.
Amboary sy havaozy
Isan''andro izahay
Mandra-pahatonga any
Aminao mandrakizay.}
', '#1-VII-6', '#1') /end

insert into song values (14591, '459. Jeso Loharanon''aina', NULL, NULL, '
{1.
Jeso Loharanon''aina,
Jeso Masoandronay!
lzao ny fonay mitaraina,
Mba velomy izahay !
O, savao ny zavon-tany
Ao am-pandehananay,
Dia Ianao ihany
No ho Masoandronay.}

{2.
Jeso, Mofon''aina soa,
Mba vokiso izahay,
Jeso, Làlana tokoa,
Mba tariho izahay!
Alfa sy Omega tsara
Ianao, Mpamonjinay;
Teny soa manambara:
Jeso no fiainanay.}
', '#1-VII-6', '#1') /end

insert into song values (14601, '460. Ario ny tahotrao', NULL, NULL, '
{1.
Ario ny tahotrao,
Ry olona osa fo !
Izay lazain''ny Tomponao
Tsy hisy tsy ho to;
Ny fisentoanao
Dia reny avokoa;
Ny feon''ny fitarainanao
Valiany tokoa.}

{2.
Mpanjaka lehibe
No itokianao;
Ny tanany mahery re
No hamonjena anao;
Tsy misy mahalà
''Zay tiany hatao:
Handositra amin-tahotra
Ny fahavalonao.}

{3.
Ny fitondrany soa
No iankinonao;
Koa aoka hofidiny koa
Izay halehanao;
Na dia tsy takatrao
Ny sainy lalina.
Izay tendreny ho anao
Ekeo ho marina.}

{4.
Ry Tompo tsara o!
Mba mijere anay,
Na reraka aza re ny fo
Tohanonao ''zahay:
Ovay ny hevitray,
Ny fonay hazavao,
Izay atoronao anay
Hizoranay izao.}
', '#1-VII-6', '#1') /end

insert into song values (14611, '461. Aoka ho an''Andriamanitra', NULL, NULL, '
{1.
Aoka ho an''Andriamanitra
Ny fanendrena ny lalanao.
Fa hovimbininy hianao
Amin''ny lalan-tsarotra;
Fa toky tsy mamitaka
Ny Tenin''Andriamanitra.}

{2.
Fa inona no azonao
Amin''ny fiahiana?
Tsy afaka ory hianao
Noho ny fisentoana;
Na be henehina hianao,
Ho be ny alahelonao.}

{3.
Lefero kely, maneke
Izay sitrak''Andriamanitra,
Ny zavatra tinendriny
Tsy maintsy mahasoa anao;
Isika nofinidiny,
Ka tsy foiny indray.}

{4.
Fa fantatr''Andriamanitra
Izay andro hifaliana
Isika no hodioviny
Amin''ny fahoriana:
Tsy maintsy hofantenana
Vao afa-pahoriana.}

{5.
Dia aza atao fa hianao
Afoin'' Andriamanitra;
Fa Izy dia mananatra
Ny zaza izay tiany,
Ka dia mba manomana azy
Handova ny an-danitra.}

{6.
Misaora ny Anarany,
Ka aza ketraka hianao
Fa ho tanteraka aminao
Ny soa lazain''ny Teniny;
Fa toky tsy mamitaka
Ny tenin''Andriamanitra.}
', '#1-VII-6', '#1') /end

insert into song values (14621, '462. Eo an-tananao ry Raiko !', NULL, NULL, '
{1.
Eo an-tananao ry Raiko !
No apetrako izao,
Ny fiainako rehetra,
Mba tahio ny zanakao;
Marobe ny fahavalo
Ta-hamely ahy e;
Ry Jehovah Ray mpiaro,
Mba arovinao anie.}

{2.
Raha mbola eto aho,
Zaza be fisiasia,
Tia ratsy noho ny soa,
Ka mandà ny Tompo tia;
Aza mba manary ahy,
Ry Mpamonjy tsy mandao!
Aza manalavitra ahy,
''Ndrao dia takona aminao.}

{3.
Na ho lava, na ho fohy
No hiainako ety,
Tsy mba sitrako akory
Ny  handà ''zay tendrinao;
Raha tonga anio ny andro
''Zay handaozako ity,
Eo an-tananao no tiako,
Raiso aho ho ary.}

{4.
Eo an-tànanao, ry Raiko!
No apetrako izao
Aina, tena sy fanahy,
Mba vonjeo ny zanakao;
Ampianaro hahazaka
Fitondrana tianao;
Ilazao mba hahalala
Fa izao no sitrakao.}

{5.
Eny, Raiko! Mba valio
''Zao fivavako izao,
E, jereo, ka mba tahio
''Zaho miandrandra Anao;
Mba sasao sy ranao soa,
Dia hadio aho ''zao;
Ry Mpamonjy! Mihainoa
Ny mpanomponao izao.}
', '#1-VII-6', '#1') /end

insert into song values (14631, '463. Jehovah no Ampinga', NULL, NULL, '
{1.
Jehovah no Ampinga
Sy Masoandro koa
Hiaro amin-doza
Sy hanazava koa.}

{2.
Ka tsy handresy ahy
''Zay fahavaloko;
Na ota na devoly
Ho resy avokoa.}

{3.
Fa raha alaim-panahy,
Na misy ''zay manjo,
Hanatona Azy aho
Hakako hery koa.}

{4.
Rehefa tapitra andro,
Ka tonga amiko
''Zay hialako aina,
Dia tsy ho tahotro.}

{5.
Ny lanitra haleha
Sy hitoerako;
Hidera an''i Jeso
MandrakIzay doria.}
', '#1-VII-6', '#1') /end

insert into song values (14641, '464. Jeso Tompo manan-kery', NULL, NULL, '
{1.
Jeso Tompo manan-kery
Miarova anay;
Ry Mpitady ondry very,
Mamonje anay.}

{2.
Jeso Tompo be fitia!
Mitsinjova anay;
Ny fanahinay mania,
Miantrà anay.}

{3.
Jeso Tompo be fiantra!
Mijere anay;
Ny fanahinay mahantra,
Mitahia anay.}

{4.
Jeso Tompo mahalala!
Mananara anay;
Ny fanahinay adala,
Mitariha anay.}
', '#1-VII-6', '#1') /end

insert into song values (14651, '465. Ry Jeso o, ry Jeso!', NULL, NULL, '
{1.
Ry Jeso o, ry Jeso !
Ny foko lasanao
Ka Ianao irery
No Tompoko izao
Tsy manan-tahotra aho
Ianao no aroko
Ny lalako mazava
Ianao faniloko.}

{FIV.
Ry Jeso ô, ry Jeso !
Ny foko lasanao
Ka Ianao irery
No Tompoko izao.}

{2.
Mitaona dia mitaona
Ny zava-poana;
Ankaso-bolamena
Mamirampiratra;
Anefa, ry Mpamonjy !
Izay ampianao
Dia haharesy tsara
Izao tontolo izao,
[FIV.]}

{3.
Ny nofoko malemy,
Ka be ny sitrapo;
Be koa ny fitsapana
Mahazo ahy ''zao;
Anefa, Jesoa tia!
Raha re ny feonao,
Mahazo hery aho
Hanao ny sitrakao,
[FIV.]}

{4.
Ry Jeso ô, ry Jeso !
Mba tovy amiko
''Lay teny maharavo
Hoe "sakaizako";
Iriko lalandava
Hiaraka Aminao,
Mba hahitako tsara
Ny fitiavanao.
[FIV.]}

{5.
Ny dianao jereko
Ety an-dàlana,
Ambara-pahatratra
''Lay fitsaharana;
Omeo hery aho
Hamompo tsara Anao;
Fa fantatrao, ry Jeso !
Ny foko lasanao.
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14661, '466. Ry Jeso o! Ry Jeso o!', NULL, NULL, '
{1.
Ry Jeso o! Ry Jeso o!
ny fonay tia Anao
Ka manomàna anay izao
Ho any aminao
FIV:
Fa sambatra avokoa
''Zay tonga aminao
''Zahay maniry koa
Ho eo ankilanao.}

{2.
Ry Jeso o! Ry Jeso o!
Ampionao izahay
Hihevitra ny lanitrao,
Izay hodianay:
[FIV.]}

{3.
Ry Jeso o! Ry Jeso o!
Maloto izahay,
Tsy mendrika ny lanitrao,
Diovinao ''zahay:
[FIV.]}

{4.
Ry Jeso o! Ry Jeso o!
ny fahavalonay
Mamely ny fanahinay,
Reseo fa rafinao:
[FIV.]}

{5.
Ry Jeso o! Ry Jeso o!
Sao reraka izahay
Miandry ny hodianay,
Tohanonao ''zahay:
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14671, '467. Mba manampia anay', NULL, NULL, '
{1.
Mba manampia anay,
Ry Jeso Tompo soa.
Ka dia ny fiainanay
Hambinina avokoa.}

{2.
ny tadiavinay
Dia fihavanana,
Tsy hisarahana aminao,
Izay Fiainana.}

{3.
Ampionao izahay
Mba hahalala Anao,
Ka aoka mba harahinay
ny nodiavinao.}

{4.
Dia mankalaza Anao
''Zahay, ry Tompo o!
Fa, he, ny fitahianao
Dia tena mahasoa.}
', '#1-VII-6', '#1') /end

insert into song values (14681, '468. Irinay Jeso tia', NULL, NULL, '
{1.
Irinay Jeso tia
Mpanota izahay,
Ka Misy alahelo
Aty anatinay;
Irinay ny Mpamonjy
Hiseho ho anay,
Hanafaka ny ota
Mba hifalianay.}

{2.
Irinay Jeso tia
Mba hanazava anay,
Fa maizina ny sainay,
Ka diso izahay;
Irinay Jeso Kristy
Hanolo-tsaina anay,
Irinay Jeso hendry
Mba hianaranay.}

{3.
lrinay Jeso tia
Ho tonga aminay,
Hiantra sy hiaro
Sy hanadio anay;
Na dia malemy aza
Ka reraka izahay,
Rehefa voavonjy,
Matanjaka izahay.}

{4.
Ry Tomponay malala!
Ry Jeso tianay!
Ry Havanay mahery!
Ry Rahalahinay!
Vonjeo izahay mpanota
Miankina aminao,
Tahio izahay mahantra
Mivavaka aminao.}
', '#1-VII-6', '#1') /end

insert into song values (14691, '469. Jeso o, Mpitia anay!', NULL, NULL, '
{1.
Jeso o, Mpitia anay!
Maminay ny teninao,
Osa ny fanahinay,
Fa mahery Ianao;
FIV:
:,: Mamy, ry Jeso!:,:
ny fitiavanao.}

{2.
Jeso o, Mpitia anay!
Mahadio Ianao;
Ka ny halotoanay,
Aoka ho voasasanao;
[FIV.]}

{3.
Jeso o, Mpitia anay!
Tompon''aina Ianao;
Maro ny aretinay,
Nefa samy afakao;
[FIV.]}

{4.
Jeso o, Mpitia anay!
Aoka hotarihinao;
Aza mba mandao anay,
Mandra-pahafatinay;
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14701, '470. Ny foko, ry Mpamonjy', NULL, NULL, '
{1.
Ny foko, ry Mpamonjy,
Mangetaheta Anao,
Maniry lalandava
Hikambana aminao.}

{FIV.
Tsy afaka ao am-poko
Ny faniriana Anao;
Oneno re, ny foko
Ho tena lapanao!}

{2.
Ny androm-pahoriana,
Raha lapanao ny fo,
Manjary fitahiana,
Izay manadio ny fo:
[FIV.]}

{3.
Ny ora fifaliana,
Raha ao anilanao,
Mamelom-paniriana
Hanao ny sitrakao:
[FIV.]}

{4.
Ho lao ny fahotako
Raho akaikezinao,
Tsy misy hadalako
Izay tsy afakao:
[FIV.]}

{5.
Finaritra ny saiko
Raha ipetrahanao,
Fa reraka ny aiko
Raha halavirinao:
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14711, '471. He sambatra aho, Jeso, raha miaraka aminao', NULL, NULL, '
{1.
He sambatra aho, Jeso, raha miaraka aminao,
Fa ny làlanao dia lavi-pahoriana;
Ny ali-mampahory, ny raho-mitatao,
Eo anilanao dia mody fitahiana,
Tano aho Jeso, hiaraka aminao,
Izay làlana ombako e! mba hazavao.}

{2.
Izao no faniriako manentsika ny fo
Ho sakaizano mandrakizay doria;
Na inona mihatra, na inona manjo,
Ianao no antenaiko hitantana ny dia.
O! avia, Jeso, tano aho e!
Tantanonao ny diako, Tompo mamonje!}

{3.
Izay sakaizanao, Jeso, no ampoky ny soa,
Feno haravoana sy lavitra manjo;
Izay rehetra ilaina anananao tokoa,
Raha miaraka aminao, mionona ny fo.
Sambatra aho, Jeso, tretrika tokoa,
Ravo ny fanahiko, toloranao ny soa.}

{4.
Inona no mamiko mihoatra noho Ianao?
Tompo manan-kery sy loharanon-tsoa?
Izay zavatra ilaiko azoko aminao
Koa onenonao ny foko.
Holapanao tokoa.
Manjakà, Jeso, ato am-poko Ianao
Izay fahotako e! ry Tompo, avelao.}
', '#1-VII-6', '#1') /end

insert into song values (14721, '472. Iriko e, ry Jeso!', NULL, NULL, '
{1.
Iriko e, ry Jeso!
Hitovy aminao.
Izay tsy manan-keloka,
Fa masina tokoa.
Iriko e, ry Jeso!
Ho tonga toa Anao,
Namonjy sy nanafaka
Fanahy, aina koa.}

{2.
Iriko, ry Mpamonjy!
Mba ho sakaizanao,
Hiaraka ory aminao
Ka tsy handao Anao;
Iriko, ry Mpamonjy!
Hanara-dia Anao;
Ny hazofijaliana
Ho entiko izao.}

{3.
Iriko, ry Mpandresy!
Hiaraka aminao,
Ho eo an-toby lehibe,
Ho miaramilanao;
Iriko, ry Mpandresyl
Hanaraka Anao,
Mitondra ny fanevanao,
Handray ny herinao.}

{4.
Iriko, ry Mpanjaka!
Ho ao an-dapanao
Hiara-paly aminao,
Fa tonga zanakao;
Iriko, ry Mpanjaka !
Ho eo anilanao
Handray ny lova atolotrao,
Mpanjaka be fitia!}
', '#1-VII-6', '#1') /end

insert into song values (14731, '473. Mombà anay ry Jeso Tompo o!', NULL, NULL, '
{1.
Mombà anay ry Jeso Tompo o!
Fa maizina ny alina aty;
Tsy misy vonjy raha tsy aminao;
Mpamonjy tia o! Mombà anay.}

{2.
Mandalo koa ny andronay ety,
Ka mihalo ny fifalianay;
Miovaova izao rehetra izao;
Ry tsy miova o! Mombà anay.}

{3.
Aza mba mivahiny aminay,
Aza mihevitra ny helokay;
Aoka ho be ny !amindranao fo;
Ry Jeso Tompo o! Mombà anay.}

{4.
Tsy mahaleo tena izahay,
Fa mora resy, mora kivy koa;
Ry Jeso Tompo! Aoka honenanao
Ety an-dasin''ny mpanomponao.}

{5.
Ny teninao no fifalianay;
Ny masonao mitari-dalana;
Ary ny herinao no tanjakay;
Sakaiza tsara o! Mombà anay.}

{6.
Izay ombanao afa-tahotra,
Tsy mety kivy na mamoy fo;
Tsy misy fahoriana izay
Haninona ny fo arovanao.}

{7.
Rehefa tonga ny fotoana
Hitanay ''lay ony maizina,
Aoka ny tavanao hazava ao,
Amin''izany re mombà anay.}
', '#1-VII-6', '#1') /end

insert into song values (14741, '474. Jesosy o! Ianao no faleha!', NULL, NULL, '
{1.
Jesosy o! Ianao no faleha!
Mba hankanesana amin''ny Ray.
Ka mitariha anay Mba handeha
Amin''ny lalana marina indray.}

{2.
Fa Ianao no Mpamonjy
voatendry,
Zanaka mandrakizay Ianao;
Ka Ianao no Mpitarika hendry,
Làlana amin'' ny Ray Ianao.}

{3.
Diso ny tsara, ka dia nizotra
Tamin''ny làlana ratsy izahay;
Koa mamerena anay mba hizotra
Amin''ny làlana tsara indray.}

{4.
Fa Ianao no mitondra ho any
Amin''ny lanitra fonenanao;
Ary tsy misy mahazo mankany
Afa-tsy amin''ny alàlanao.}

{5.
Maro ny zavatra izay mampahory,
Fa Ianao no Mpiaro anay;
Aoka izahay tsy ho reraka akory,
Ry Mpanalàlana amin'' ny Ray !}
', '#1-VII-6', '#1') /end

insert into song values (14751, '475. Jeso Mpamonjy Mpiandry tokoa!', NULL, NULL, '
{1.
Jeso Mpamonjy Mpiandry tokoa!
Ampivereno hanaraka Anao;
Ondry mania, manary ny soa,
Aza avela hiala aminao.}

{2.
Taomin''ny ratsy, fitahin-tSatana,
Mila ho lasan-ko babo izahay,
Fa Ianao no mahery mitàna
Tsy hahavery ny ondry iray.}

{3,
Varivariana, salasalaina,
Be ahiahy, jereo izahay;
Ampianaro ny tsy hary saina,
Ampitsaharo ny fanianay.}

{4.
Aina nomenao hiso lony very,
Ary ny ranao nanavotra anay,
Fa Ianao no nitondra irery
Fampijalian-kanitsy anay.}

{5.
Fa ny fiainan-tsy hita resenao
Velona mandrakizay Ianao;
Dia fiainana an-danitra omenao
Izay ondry mety manaraka Anao.}
', '#1-VII-6', '#1') /end

insert into song values (14761, '476. Ny eo akaikinao, ry Ray', NULL, NULL, '
{1.
Ny eo akaikinao, ry Ray,
No mamiko izao,
Hanjary tena tafaray
Ny foko sy ny Anao
Soa re, ry Raiko o!
Raha eo anilanao;
Ny hazo fijaliana
Ho tonga maivana.}

{2.
Ny eo akaikinao, ry Ray,
No herin-janakao;
Ianao irery no mahay
Manampy ahy izao.
Soa re, ry Raiko o!
Raha eo anilanao;
Ny ota tsy hanapaka
Ny olona afaka.}

{3.
Ny eo akaikinao, ry Ray,
No mandako tokoa;
Ny ratsy dia tsy hahay
Handrombaka ahy koa,
Soa re, ry Raiko o!
Raha eo anilanao;
Ny ora izay manjombona
Ho fiadanana.}

{4.
Ny eo akaikinao, ry Ray,
No fifaliako izao,
Ny tanako dia hifandray
Amin''ny tànanao.
Soa re, ry Raiko o!
Raha eo anilanao;
Fenoinao fanambinantsoa
Ilay kapoakako.}
', '#1-VII-6', '#1') /end

insert into song values (14771, '477. Oram-be mivatravatra', NULL, NULL, '
{1.
Oram-be mivatravatra
Arotsahy Tompo o!
Fa mangetaheta loatra
lzahay mpanamponao
O! valio, O! valio,
Mitaraina, ka valio.}

{2.
He ! Malazo re ny voly
Izay maniry tsy mamoa;
Nefa raha omenao aina,
Dia ho vokatra tokoa.
O! vangio, O! vangio,
Sao dia maty, ka vonieo.}

{3.
Tovy eto, ry Mpamonjy!
Izay nambara fahizay,
Ka iraho ny Fanahy
Amin-kery lehibe,
O! henoy, O! henoy,
Ka valio ny vavakay.}

{4.
He! Toa kivy ka manahy
lzahay mpiasanao;
Aza ela, ry Fanahy!
Asehoy ny herinao,
O! avia,O! avia,
Misehoa, Fanahy o!}
', '#1-VII-6', '#1') /end

insert into song values (14781, '478. Ny hirako izao', NULL, NULL, '
{1.
Ny hirako izao,
Ray be fitia,
Ny ho akaikinao
Bebe kokoa,
Izay, re, no hirako,
Mandrapahafatiko;
:,: Akaikinao:,:}

{2.
Raha mba sarotra
Ny diako
Ka zary maizina
Ny làlako,
Ry Ray! Akaikinao
No tian''ny zanakao:
:,: Akaikinao:,:}

{3.
Ao no hiseho re
Ny tohatra,
Mba hanatonako
Ny lanitra,
Anjely miantso ahy
Akaikinao, ry Ray!
:,: Akaikinao :,:}

{4.
Ny fahoriana
Sy loza koa,
Raha eo anilanao,
Dia mody soa,
Ka dia tano aho
Ho ao akaikinao:
:,: Akaikinao :,:}

{5.
Na dia hilentika
Ny androko,
Ary izao hita izao
Hilaozako,
Mbola ho hirako,
Ry Andriamanitro!
:,: Akaikinao :,:}
', '#1-VII-6', '#1') /end

insert into song values (14791, '479. Jeso no anjara soa ', NULL, NULL, '
{1.
Jeso no anjara soa
Izay mahafa-po tokoa
Soa tsy manam-paharoa
Ahy Jeso: Ampy izay}

{FIV.
Ahy Jeso hatr''ety,
Ahy Jeso hatr''ary
Ahy Jeso zoky tia
Mandrakizay doria}

{2.
Vola sy harena be
Maro tia tokoa anie;
Nefa mamiko ny hoe:
Ahy Jeso: Ampy izay.
[FIV.]}

{3.
Fahefana, laza soa
Mahafatifaty koa;
Nefa izao no tena soa:
Ahy Jeso: Ampy izay.
[FIV.]}

{4.
Eny, izao rehetra izao
Tsy hamoizako Anao,
Ry Jeso Izay tsy mandao!
Ahy Jeso: Ampy izay.
[FIV.]}

{5.
Jeso, aina, hery, zo;
Jeso, lova, tsy mba lo;
Jeso, fianinam-po;
Ahy Jeso: Ampy izay.
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14801, '480. Mamin''ny foko, ry Jeso o !', NULL, NULL, '
{1.
Mamin''ny foko, ry Jeso o !
Ny famonjena izay vitanao;
Ny hadisoako navelanao
Tena afaka aho mandrakizay.}

{FIV.
Jeso malala o!
Mpamonjy soa
Indro ny aiko raiso ho Anao.
Velon-kanompo,
Velon-kanoa,
Vonon-kanao izay sitrakao.}

{2.
Tano ny foko, ry Jeso tia!
Zaza tsy mahasaraka Anao;
Mbola malemy, mora mania,
Tano hifikitra ao aminao.
[FIV.]}

{3.
Aoka ny foko sy ny fonao
Hiara-mitepo tena iray;
Aoka ny saiko sy ny sainao
Hiray safidy mandrakizay
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14811, '481. Ry Tompo be fitia!', NULL, NULL, '
{1.
Ry Tompo be fitia!
Izaho zanakao
Tantano tsy hania
Hiaraka aminao.
Fa mamiko tokoa
Ny eo akaikinao;
Tsy orin-java-tsoa
Izay sakaizanao.}

{2.
Harena be an-tany
Mamilivily fo,
Fa tiana mora lany
Ka tsy mampionom-po;
Izany aniriako
Hifikitra aminao ;
Atolotro ny foko
Ho eo an-tànanao.}

{3.
Fa raha izaho irery
Voasakan-tsitrapo,
Mandeha manirery,
Ka maro no manjo;
Izany no ilàko
Hiaraka aminao,
Vimbino aho, Raiko!
Ho eo an-tànanao.}

{4.
Tsy hitako mihitsy
Izay mahafa-po,
Tsy misy ety akory
Izay tsy mety lo;
Fa ao am-paradisa
No feno zava-tsoa
Tsy mahadisadisa,
Fa sambatra tokoa.}
', '#1-VII-6', '#1') /end

insert into song values (14821, '482. Jehovah o ! Ianao nandinika ahy ', NULL, NULL, '
{1.
Jehovah o ! Ianao nandinika ahy,
Ka fantatrao ny toe-piainako;
:,: Ny fo sy saina mbamin''ny fanahy,
Tsy miafina aminao ry Tompoko.:,:}

{2.
Ka na mipetraka aho, na mandeha,
Izay rehetra ataoko hitanao,
:,: He, fantatrao ny lalana haleha,
Ny androko dia eo an-tananao.:,:}

{3.
Na velona aho, eny, maty aza,
Ny tenako dia eo imasonao,
:,: Ka toerana aiza, lalana mankaiza
No hialako tsy ho hitanao?:,:}

{4.
Ry Tompo! Tena mahagaga loatra
Ny asanao sy ny fisainanao;
:,: tsy misy saina mety mahatratra
Ny fahendrenao sy ny herinao.:,:}

{5.
Izao no hany zavatra iriko:
Velaro indrindra ny fitiavanao,
:,: Hahaizako hametraka ny foko
Hionona eo am-pela-tananao.:,:}
', '#1-VII-6', '#1') /end

insert into song values (14831, '483. Zanahariko Tsitoha', NULL, NULL, '
{1.
Zanahariko Tsitoha,
Tompo, Hery sy fitia;
Ray hatramin''ny taloha,
Ahy anio, ka ho doria.
Manjakà mandrakizay
Ao am-poko, Tompo soa.
Dera, laza Anao ry Ray!
Zanaka, Fanahy koa.}

{2.
Ny finoako Anao,
Hatanjaho, mba hamoa
Hiasako ny asanao,
Hamafazako ny voa.
Aoka ny Fanahinao
Jiro mba hitarika ahy
Ka apetrako aminao
Na ny tena na fanahy.}

{3.
Antenaiko, Ianao, Jeso
/zay Mpitsara lehibe
Hanamarina ahy ao,
Na mpanota aza re.
Dia hiezaka aho ety
Mba hanao izay mendrika
Hamindranao fo ary
Fa voavela heloka.}

{4.
Ray o, ny fitiavanao
Afindrao ho toetrako,
Mba hitiavako Anao,
Ary koa ny namako,
Ho mpanompo aho re !
Ho mpitory koa Anao!
Mba hanjaka an-tany e,
Tompo, ny fitiavanao.}
', '#1-VII-6', '#1') /end

insert into song values (14841, '484. Ireo didinao, Jehovah Ray', NULL, NULL, '
{1.
Ireo didinao, Jehovah Ray;
Izay tianao hankatoavinay,
Dia rehareha sy voninahitra,
Satria filamatry ny ao an-danitra
Ilaina mba hiseho fa tsy hiery,
Ho fisintonana ireo ''zay very
Fa antoky ny fiainanay,
Hatramin''izao, mandrakizay.}

{2.
Ny fitsipikao, Tomponay,
Izay nomenao mba harahinay,
Dia mampifantina izay anjara soa,
Ka manamboatra sy manatsara,
Mampisy endrika itony tena
Sy manazava ny firenena.
Ka antoky ny fiainanay,
Hatramin''izao, mandrakizay.}

{3.
Ireo teninao, Jesosinay,
No hany zavatra ivelomanay,
Fa tanjaka tokoa, tena hery be
Hahafahanay ny làlana mideza
re,
Sakafo mahavelona ny aina
Sy mampirava tokoa ny saina,
Tana antoky ny fiainanay,
Hatramin''izao, mandrakizay.}
', '#1-VII-6', '#1') /end

insert into song values (14851, '485. Moa Jesosy ve tsy ahy?', NULL, NULL, '
{1.
Moa Jesosy ve tsy ahy?
Andriamanitra mahery.
Koa nahoana no hanahy?
Izy miambina mijery,
Fahavalo na dia maro,
Tsy mba mahakasika ahy
Izy no ampinga miaro
Koa izaho tsy hanahy.}

{2.
Moa Jesosy ve tsy ahy?
Ilay Mpanolotsaina hendry
Koa tsy kivy fa ho sahy
Amin-java-tsoa kinendry,
Tsy manaonao foana
Fa tarihin-Kapiteny,
Tsy manahy satria nahoana?
Ao ny saina izay nameny.}

{3.
Moa Jesosy ve tsy ahy
lIay Jesosy mahagaga,
Mpanasitrana, Mpitahy
Ka ahoako izay hitranga?
Ranomasina aza tao
Nampangininy ka tony;
Hainy mihitsy ny manao
Hahato ny sitrapony.}

{4.
Moa Jesosy ve tsy ahy?
Rain''ny mandrakizay, eny!
Velon-toky ny fanahy
Izay nekeny, novonjeny,
Koa na ory sy mijaly
Aza aho ety an-tany,
Vetivety dia hifaly:
Mampaninona izany?}

{5.
Moa Jesosy ve tsy ahy?
lIay Andriampiadanantsika.
Andriamanitra Mpiahy,
Fitahiana feno hika,
Famonjena efa vita
Hofikiriko hatrany,
Fahasoavana hampita
Any am-paradisa any.}
', '#1-VII-6', '#1') /end

insert into song values (14861, '486. Inona no hahasaraka ahy', NULL, NULL, '
{1.
Inona no hahasaraka ahy
Amin''ny fitiavanao, Jeso?
Fahoarina ve, sa ahiahy?
Fahantrana, sa famoizam-po?}

{FIV.
Tsia, ry Jeso Tompo, be fitia,
Izaho tsy mivadika Aminao!
Fa mandrakizay doria doria,
Izaho dia hanaraka Anao!}

{2.
Inona no hahasaraka ahy
Amin''ny fitiavanao, Jeso?
Zo sy laza sy fakampanahy?
Sa harena, zava-mora lo?
[FIV.]}

{3.
Inona no hahasaraka ahy
Amin''ny fitiavanao, Jeso?
Sabatra, sy fanenjeha-mafy?
Sa mosary, ady be manjo?
[FIV.]}

{4.
Inona no  hahasaraka ahy
Amin''ny fitiavanao, Jeso?
Faty ve no hampivadika ahy,
Loza, hery, fahefana koa?
[FIV.]}

{5.
Eny, izao rehetra izao, ry Tompo,
Tsy mba hahasaraka Aminao!
Na inona hanjo ity mpanompo,
Dia hijoro, tsy handao Anao!
Velona aho, Tompo, eny maty.
Dia tsy mivadika Aminao!
Ianao no velona ao anaty,
Ka doria, ny foko ho Anao!}
', '#1-VII-6', '#1') /end

insert into song values (14871, '487. Fanavaozan''izay mahagaga tokoa', NULL, NULL, '
{1.
Fanavaozan''izay mahagaga tokoa
Nomen''i Jeso ahy re,
Fahazavan''ny fanahy izay mahafa-po
Nomen''i Jeso ahy re,}

{FIV.
:,: Hatram''izao dia endrey:,:
Ny fitepon''ny foko izay ravo tokoa
Onenan''ny Tompo Jeso.}

{2.
Ny fiainako be siasia taloha
Voaova mihitsy izao
Eny, ny fahotako, izay nahavoa
Voasasany koa izao.
[FIV.]}

{3.
Ny finoako, ny fanantenako dia be,
Ny tokiko fatratra koa,
Ny fisalasalana dia levona re,
Ny saiko mandroso ho soa.
[FIV.]}

{4.
Tonga mamirapiratra fatratra ery
Ny aloky ny faty re!
Fa dia tsinjoko ilay vavahady ary
Mankao an-tanan-dehibe.
[FIV.]}

{5.
Tsy miato, mizotra mankany tokoa
Ny diako hatramin''izao
Hovantaniko any Ilay Tompo soa,
Jesosy malala no ao;
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14881, '488. Na inona na inona hitranga, Jeso!', NULL, NULL, '
{1.
Na inona na inona hitranga, Jeso!
Na ny lanitra ho manga, Jeso !
Na ho maloka mangina, Jeso !
Dia ho feno fifaliana, Jeso !}

{FIV.
Ny fotoana dia handalo, Jeso!
Fa tsy hisy hampimalo, Jeso!
Raha miorina ao am-poko, Jeso!
Ilay fitiavanao inoako, Jeso!}

{2.
Raha jereko ny andro lasa, Jeso!
Tao ny tsiky, tao ny ratra, Jeso!
Nefa Ianao, Ilay Mpampahery, Jeso!
No nambomba sy nijery, Jeso!
[FIV.]}

{3.
Ny ho avy antenaiko, Jeso!
Mbola ho fifalian''ny saiko, Jeso!
Ny hasasarako hisava, Jeso!
Ny harivako hazava, Jeso!
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14891, '489. Tena sambatra sy maha-te ho tia', NULL, NULL, '
{1.
Tena sambatra sy maha-te ho tia
Izay masina, madio fisainana
Ra miara-dalana, mifankatia
Amin''Andriamanitra Fiainana.}

{FIV.
Masina tokoa re ny Tompo,
Tianay mba ho masina
Saina tena sy fanahy
Ka fiainan-danitra tokoa
No santarin''izay
Mino sy manoa.}

{2.
Indro, mampahery, mahatoky koa
Raha mahitsy ny fijery, soa ny fo;
Fa ny fo madio sy masina tokoa:
Paradisa an-tany, feno zava-tsoa
[FIV.]}

{3.
Koa halaviro ny fitaoman-dratsy,
Ny fisaikazana tsy mba masina;
Fa ny tia ny Tompo, dia olon-tsoa mitafy
Fahadiovan''ny Fanahy Masina.
[FIV.]}

{4.
Miaraha lalana amin''i Jesosy
Fa ny fahavalo ta-hamotraka;
Mijoroa, reseo ny toetra mampihosina
Ao anaty loto sy ny fotaka.
[FIV.]}
', '#1-VII-6', '#1') /end

insert into song values (14901, '490. Ry Kristiana, andrandrao', NULL, NULL, '
{1.
Ry Kristiana, andrandrao
Ny Tompo sy Mpanjakanao,
Fa Izy no Mpiaro;
Ka aza taitra hianao,
Na dia manakora izao
Ny fahavalo maro;
Omban''nyTompo hianao,
Ka aza atahoranao
Ny rahon''ny mpamely.}

{2.
Fa indro tsy rendrehana
Ilay Mpiambi-masina,
Izay Tokin''Israely;
Fantaro marina izao:
Izay ataon''ny rafinao
Ao ka ho azy izany;
Fa andrandrao ny lanitra,
Anao Andriamanitra,
Anao ny fanjakany.
}
', '#1-VII-7', '#1') /end

insert into song values (14911, '491. Inoantsika marina', NULL, NULL, '
{1.
Inoantsika marina
Ny Mpamorona Tsitoha,
Isika dia natsangana
Mba ho zanany tokoa,
Hanina sahaza omeny,
Ary be ny famonjeny,
Amin''ireo loza maro
Dia Izy no Mpiaro,
Inoantsika ao am-po
Fa Izy marina sy to.}

{2.
Jesosy Kristy Zanany
No inoantsika koa;
Isika dia fananany,
Fa navotany tokoa.
Izy Zanak''i Maria,
Dia rahalahy tia;
Maty ka natao ho very,
Fa nitsangana amin-kery,
Niakatra ao an-dapany
Hitsara ao am-parany.}

{3.
Ka ny Fanahy Masina
No inoantsika koa,
Ny fiangona-masina
No naoriny tokoa;
Ao isika dia mahita
Famonjena efa vita;
Afaka ny helotsika,
Fo vaovao no raisintsika;
Isika dia hatsangana
Ho velona ao an-danitra.}
', '#1-VII-7', '#1') /end

insert into song values (14921, '492. Izaho mino ny Mpamorona', NULL, NULL, '
{1.
Izaho mino ny Mpamorona,
Mpanao ny tany sy ny lanitra;
Izaho manam-panorenana,
Fa manan-dRay ary an-danitra.}

{2.
Izaho mino ny Mpanayotra,
Ny rany masina no mamiko,
Ny momba Azy dia sambatra,
Ny diany no mba harahiko.}

{3.
Inoako ny Mpanamasina
Mpananatro sy Masoandroko ;
Izaho omeny afo masina,
Mba ho madio ny fiainako.}
', '#1-VII-7', '#1') /end

insert into song values (14931, '493. Ny loharanom-pamonjena', NULL, NULL, '
{1.
Ny loharanom-pamonjena
He, hitako tokoa izao,
Dia Jeso Kristy lIay nomena
Hamonjy izao tontolo izao ,
Ny rany dia fantatro
Fa ampy hiankinako.}

{2.
Ny otantsika no nanery
Ny Tompontsika hankaty
Isika hitany ho very,
Izany no nidinany
Nitondra fahoriana
Sy hazo-fijaliana.}

{3.
Ny nialan''ny Tompo aina
Hanaisotra ny heloka
Dia tsy mba takatry ny saina,
Fa hitan''ny finoana.
Ny rany no miantso hoe:
He, famindrampo lehibe!}

{4.
Izay no mampionona ahy,
Ka afaka ny tahotro;
Na dia ory ny Fanahy,
Jesosy no Mpanayotro;
Na Misy mampanahy fo,
Ny Raiko dia be indrafo.}

{5.
Na very aza ny harena,
Ka be ny fahoriana,
Izaho mbola manantena
Hahita fifaliana;
Jesosy, Izay fananako,
Dia Tompo feno indrafo.}

{6.
Izaho tsy matoky tena,
Na ny fahamarinako;
Jesosy foto-pamonjena
No tena itokiako.
Ny Raiko no isaorako,
Fa Izy no be indrafo.}

{7.
Izay sitraky ny Ray malala
No aoka mba ho sitrako,
Fa Izy tena mahalala
Hametra izay anjarako;
Izay ataony tiako,
Fa hitako ny indrafo.}

{8.
Izany fototra izany
No toky izay hotanako
Ambara-pitako ho any
An-koatry ny fasako.
Ka any no hisaorako
Ilay Fototry ny indrafo.}
', '#1-VII-7', '#1') /end

insert into song values (14941, '494. 0, ry zanahary', NULL, NULL, '
{1.
0, ry zanahary,
Izay nahary ahy,
Be ny herinao!
Manan-toky aho
Fa tsy maty taho,
Manana Anao;
Afa-po ny zanakao,
Ianao no Ray Mpiahy,
Ka iza no hanahy.}

{2.
Ianao no Raiko,
Tena fototr''aiko,
Sady heriko.
Ianao no aro,
Tsy ho reraka aho,
Tsy ho kivy fo ;
Ao ambany elatrao
No ataoko fiereko;
Ao tsy Misy sento.}

{3.
Latsaka isan''andro
Toy ny ranonando
Ny fitianao.
Raha alaim-panahy,
Ka manafotra ahy
Re ny otako,
Dia ho tafatsangana
Hanayao ny herin''aiko
Ianao, ry Raiko. .}

{4.
Ary raha lany
Toy ny zavan-tany
Ny fiainako,
Misy onin''aina
Tsy mba mety maina
Ao an-danitrao;
Ao hitoerako,
Ao an-tranonao, ry Raiko.
Ao no antenaiko.}
', '#1-VII-7', '#1') /end

insert into song values (14951, '495. Jeso dia tokiko', NULL, NULL, '
{1.
Jeso dia tokiko,
Fa Mpamonjiko mahery
Sady tena Zokiko,
Tsy hamela ahy irery
ao amorom-pasako,
Fa hiaraka amiko.}

{2.
Izy tenany nandre
Ny hangidin''ny fitàna
IIay ony mainty be
Vokatry ny fahotàna;
Koa tsy hilaozany
Irery ao ny olony.}

{3.
Izaho hody vovoka,
Fa ny androko ho lany,
Nefa dia hatsangana
Sy hakarina hankany
Amim-boninahitra,
Izay tsy mety tapitra.}

{4.
Dia ho tafatambatra
Ny Fanahy sy ny tena,
Ary samy sambatra,
Fa nahazo famonjena,
Izay Jesosy, havako,
Vao ho tena hitako.}

{5.
Izao manjo aty izao
Tsy mba Misy ao akory,
Fa finaritra ny ao,
Tapitra ny mampahory,
Ka ny fahalovana
Hody fifaliana.}

{6.
Eto aho reraka,
Any Misy hazon''aina;
Vita sy tanteraka
Izay rehetra nantenaina;
Ao no hahatrarako
Ny fahasambarako.}
', '#1-VII-7', '#1') /end

insert into song values (14961, '496. Jeso iankinako', NULL, NULL, '
{1.
Jeso iankinako,
Izy no Mpanayotro;
Kristy itokiako
Amin-tsaina sy ny fo;
Kristy ifaliako
Izy marina sy to.}

{2.
Nahafoy ny lanitra
Ianao, Mpanayotra;
Raha ny vahoakanao
Nobaboin''ny rafinao,
Ianao nangoraka,
Ry Kristy Mpanafaka.}

{3.
Fahoriam-be ery
No nanjo Anao taty,
Fa ny fahavalonao
Dia nanenjika Anao;
Nefa novalianao
Indrafo ny rafinao .}

{4.
He, nijaly Ianao
Noho ny fitiavanao,
Ka ny famindramponao
No nahafatesanao;
Ianao nasandratra,
Fa nijaly fatratra.}

{5.
Saro-bidy izahay,
Fa ny ranao avotray,
Izay nomenao ho anay,
Mba hanafaka anay;
Tonga olom-potsinao
Izahay navotanao.}

{6.
Ravoravo izahay,
Ry Mpamonjy maminay,
Ka ny fanayotanao
An''izao tontolo izao
Sy ny fitiavanao
No isaoranay Anao.}
', '#1-VII-7', '#1') /end

insert into song values (14971, '497. Ny tany sy ny lanitra tsy manampetra', NULL, NULL, '
{1.
Ny tany sy ny lanitra tsy manampetra
Hiaraka amin''ny mino rehetra,
Manaiky fa Jeso no aina hamonjy
Ny olona ory izay mila vonjy.}

{2.
Na dia manenjika ahy ny ratsy
Tsy lany ny hery, fa Jeso mamatsy,
Dia Izy no tanako mafy takoa,
Ka tsy ialako fa ,Tompo Tsitoha.}

{3.
Jesosy sakaizako dia miahy
Ny Azy, ka afaka ny ahiahy,
Ny fo malahelo ovany ho faly,
Ka tretrika indray ireo olo-mijaly.}

{4.
Jesosy fidiko ho Tompo tokoa,
Efaiko ny asa sy hevitra soa,
Izay halan''ny Tompo tsy tiako
intsony,
Ka tena iraisako tsara ny fony.}

{5.
Izay tanako mafy sy mamy hambara
Rehefa ho faty, ka ao ny anjara
Jesosy, Jesosy irery ihany
No aina, harena tsy mety ho lany.}
', '#1-VII-7', '#1') /end

insert into song values (14981, '498. Ahy va Jesosy Tompon''aina?', NULL, NULL, '
{1.
Ahy va Jesosy Tompon''aina?
Moa ahy va ny lanitra?
Eny, matokia ry fo sy saina,
Fa eken''Andriamanitra.}

{2.
Eny, zokiko Jesosy Tompo,
Fahagagana lehibe ery,
Tsy antsoiny hoe mpanompo,
Fa ataony tena havany.}

{3.
Toa mila hampisalasala
Io tena fahagagana io
Ary lavin''ny tsy mahalala
Nefa azon''ny finoako}

{4.
0, mba mifalia, ry Fanahy,
Fa anao lIay Mpanayotra!
Ka misaora, aza mba manahy,
Fa ho azonao ny lanitra.}

{5.
Iombonana ny lova any,
Fa lray ny Ray an-danitral
Ka harena be dia be izany,
Hasambarana maharitra.}
', '#1-VII-7', '#1') /end

insert into song values (14991, '499. Inona, re, no atakaloko', NULL, NULL, '
{1.
Inona, re, no atakaloko,
An''i Jesosy Izay mpisoloko?
Ny zavatra izay ho simba va sy mora levona.
Raha mbola hazoniko ny To,
Ka ts maty ny jiro ao am-po,
Jesosy tsy soloako, na inona na inona.}

{2.
Indro, Jesosy no fiainako,
Izy no manda fiarovako;
Izao efa maty, nefa novelominy indray,
Ka na dia mahantra aza aty,
Ka tsy mbola nafindra ho ary,
Jesosy no anjarako, ka ampy ahy re, izay.}

{3.
O, ry Jesosy Tompo tsara o!
Aza mamela ny fanahiko
Hisaraka aminao, fa tano aho ho mpanomponao.
Mba velomy ny fitiavako
Ka tohano ny fandehanako
Mba hanarahako ny lalana izay nalehanao.}
', '#1-VII-7', '#1') /end

insert into song values (15001, '500. He! manda fiarovana', NULL, NULL, '
{1.
He! manda fiarovana
Andriamanitsika,
Ampinga mahatohana
Izay manjo antsika.
Mamely fatratra.
Satana sompatra,
Mpandoza hatrizay,
Mpamitaka mahay
Tsy manampaharoa.}

{2.
Malemy, tsinontsinona,
Fa mora resy isika;
Fa Misy voahasina
Miady ho antsika;
Jesosy re izay,
Ilay iraky ny Ray,
Mpitarika mahay,
Mpamonjy hatrizay
Sy Tompom-pandresena.}

{3.
Na dia ta-handevona
Ireo devoly maro,
Tsy Misy atahorana,
Jesosy no Mpiaro.
IIay rain''ny maizina
Dia nohelohina,
Ka tsy ahoana,
Fa teny tokana
Dia mahalavo azy.}

{4.
Kendreny hofongorana
Ny teny tanantsika,
Kanefa tsy ho foana,
Ny Tompo momba antsika
Na dia ho fongana
Ny laza ananana,
Harena, havana
Tsy mampaninona,
Ny lanitra antsika.}
', '#1-VII-7', '#1') /end

insert into song values (15011, '501. Namely mafy loatra', NULL, NULL, '
{1.
Namely mafy loatra
Ny henatra aman-tahotra;
Tsy, afaka ny aizim-po,
Fa mbola tao ny heloko.}

{2.
Nandroso aho nivavaka;
Ry Jeso o, Mpanayotra!
Izao no angatahiko;
Mba mamelà ny heloko.}

{3.
Izaho tsy hiahotra;
Ny tenako atolotro,
Izay rehetra ananako
Anao, Mpamela heloka.}

{4.
Ny tahotra dia levona
Ariako ny henatra,
Satria mandre ny feonao hoe:
Voavelako ny helokao.}

{5.
Endrey! ny hafaliako
Izay velona ao anatiko,
Ka sahiko ny hanao hoe:
Voavelanao ny heloko.}
', '#1-VII-7', '#1') /end

insert into song values (15021, '502. Finaritra ny vorona ao an''ala', NULL, NULL, '
{1.
Finaritra ny vorona ao an''ala
Sy ao an-karam-bato avobe;
Na dia betsaka ny mpankahala
Mivonon-kahafaty azy re;
Matoky izy, tsy misalasala,
Fa manam-piarovan-dehibe.}

{2.
Jesosy Tompoko no harambato
Sy ala mafy ikirizako;
Na mafy aza sady tsy miato
Izay ataon''ny fahavaloko,
Mamely ahy ary misahato,
Jesosy re no fiarovako.}

{3.
Izay rehetra ta-hamely ahy,
Mikendry, nefa sasa-poana;
Izaho dia ravo am-panahy,
Tsy azony atao ahoana,
Fa Misy manda manodidina ahy,
Izay miaro sy manohana.}
', '#1-VII-7', '#1') /end

insert into song values (15031, '503. Ry Jeso Tompo tsara o!', NULL, NULL, '
{1.
Ry Jeso Tompo tsara o!
Tsy azoko valiana
Izato fitiavanao
Manafa-pahoriana.
He, sambatra aho Tompo o!
Fa manana Anao izao,
Izay tsy mba mety sosotra,
Na faly hampitahotra,
Ka Ianao no tokiko
Sy foto-pifaliako.}

{2.
He, isam-pahoriana
Izay niantsoako Anao,
Dia efa novaliana
Ny hataka izay natao.
Ny heloko navelanao,
Ny foko nohavaozinao,
Ny loza nosakananao,
Ny ngidiny nesorinao;
Ny aim-baovao iainako,
Ka velona ny heriko.}

{3.
Ka amin''izao sisa izao,
Mikasa aho, Tompo o!
Hanao izay nohafaranao ,
Hanaraka Anao am-po.
Satana dia holaviko,
Hafoiko izao tontolo izao,
Rafesiko ny nofoko,
Fa Ianao, Mpamonjy o!
Mpanayotra tsy foiko,
No sisa hotompoiko.}
', '#1-VII-7', '#1') /end

insert into song values (15041, '504. Ambarao ry mpivahiny', NULL, NULL, '
{1.
" Ambarao ry mpivahiny,
Ny alehanao izao!"
"Fiakarana mideza
Ihoaranay anio."
Jeso Tompo no miantso
Mba ho ao an-tany soa,
:,: Mba ho ao an-tany soa;
Jeso no alehanay.:, :.}

{2.
"Lozanao, ry mpivahiny,
Làlana tsy fantatrao! "
"Tsia, fa anjely maro
No miaraka aminay;
Jeso Tompo no mitondra,
Tsy hanjo anay ny loza,
:,:Tsy hanjo anay ny loza;
Izy no ampinganay" :,:}

{3.
"Inona, ry mpivahiny,
No ho azonao ary?"
"Jeso Kristy be fitia
Vonon-kanome anay
Lamba fotsy, zava-tsoa,
Ranon''aina tsy ho lany,
:,: Ranon''aina tsy ho lany!
Jeso no hatoninay":, :}

{4.
"Tianay, ry mpivahiny,
Mba hiaraka aminao!"
"Mandrosoa, ry sakaiza!
Miaraha aminay;
Fa izay mitady tsara
Tsy mba lavin''i Jesosy,
Tsy mba lavin''i Jesosy,
Izy no hidiranay":,:}
', '#1-VII-7', '#1') /end

insert into song values (15051, '505. Mpivahiny! Mba lazao', NULL, NULL, '
{1.
"Mpivahiny! Mba lazao:
Zovy moa hianao?"
"Izaho namanao ity,
Fa miandry Ilay ary."
"Mpivahiny! Mba lazao:
Iza no andrasanao?"
"Jeso, Havako ary,
No andrasako aty."}

{2.
"Mpivahiny! Mba lazao:
Aiza no halehanao?"
"Làlana ety avokoa
Sady sarotra tokoa. "
"Mpivahiny! Mba lazao:
Iza no arahina?"
"Jeso, Zoky be fitia,
Tsy mamela mba hania."}

{3.
"Mpivahiny! Mba lazao :
Aiza no hodianao?
"Ao an-danitra madio
No aleha ankehitrio ."
"Mpivahiny! Mba lazao:
Iza no handray anao?"
"Jeso, Ilay Mpanjaka soa,
Tompo,Havako tokoa."}
', '#1-VII-7', '#1') /end

insert into song values (15061, '506. Tsy misy miadana ka afa-po', NULL, NULL, '
{1.
Tsy misy miadana ka afa-po
Izay monina eto an-tany;
Fa izay tia sy mino ny
Tompo Jeso
No hahazo ny soa tsy lany.}

{FIV.
Mba irio, mba irio,
Ny hananana an''i Jeso.}

{2.
Raha misy mahory mahazo ety
Jeso no hanala izany;
Tsarovy ny avotra tao
Kalvary,
Fa nalatsany tany ny rany
[FIV.]}

{3.
Izao maniry hanaraka
Anao,
Ry Tompo Mpanafaka ota!
Fa fantatro tsara hatramin''zao
Ny fitiavanao ahy mpanota.
[FIV.]}
', '#1-VII-7', '#1') /end

insert into song values (15071, '507. Ranomasi-mampahory', NULL, NULL, '
{1.
Ranomasi-mampahory
No alehantsika izao;
Fa ny mino dia hitody,
Koa maharitra izy izao;}

{FIV.
:,:O! andeha isika hody
mba ho ao an-tranon-dRay.,:,:}

{2.
Jeso Tompo eo am-pita
Ka miantso hoe: Avia!
Izy koa no mampita
Ka manao hoe: Matokia!
[FIV.]}

{3.
Maro efa tonga any
Ka mifaly avokoa;
Mbola be ny fiantrany,
Misy ho antsika koa.
[FIV.]}

{4.
Kristy koa no sambon''aina
Izay mitondra ho ary,
Famonjena azo maina,
Ka ilaozy re ity.
[FIV.]}
', '#1-VII-7', '#1') /end

insert into song values (15081, '508. Ny sambokelinay ety', NULL, NULL, '
{1.
Ny sambokelinay ety,
Mitety rano be,
Misononoka ho ery
An-dafin''ony be. (in-3)}

{2.
Ny rivo-doza tsy hanjo,
Fa Jeso aminay;
Ny alon-drano mankato
Ilay Maherinay. (in-3)}

{3.
Mamaky onja izahay,
Fa tsy ho faty ao;
Ny fitodihan-tsambonay,
He! Tazanay izao. (in-3)}

{4.
Irinay ny ho tonga ao,
Hilaozanay ity;
Mitsena anay tokoa izao
Ny Zokinay ary. (in-3)}
', '#1-VII-7', '#1') /end

insert into song values (15091, '509. Jeso Tompo! Vatolampy', NULL, NULL, '
{1.
Jeso Tompo! Vatolampy
Vato voafantina,
Mba ho fanorena-mafy
zay tsy mety levona;
:,: Vato mafy :,:
Dia itokianay.}

{2.
Jeso Tompo! Vatolampy
Voazaha toetra;
Vato tsy mba misy tsiny,
Sady tsara endrika;
:,: Vato soa :,:
Dia ifalianay.}

{3.
Jeso Tompo! Vatolampy
Vato fehizoronay;
Efa nampiray fanahy
Ny fitianao anay.
:,: Fehizoro, :,:
Vato fampiraisana.}

{4.
Jeso Tompo! Vatolampy
Tsy miova Ianao;
Manan-jara ny matoky
Ny tsy fivadihanao,
:,: Fa ho faly :,:
Ka tsy hanan-kenatra.}
', '#1-VII-7', '#1') /end

insert into song values (15101, '510. Jeso ny hamarinanao', NULL, NULL, '
{1.
Jeso ny hamarinanao
No lamba hitafianay,
Amin''ny andro farany,
Izany hifalianay.}

{2.
Raha hitsangana izahay
Handray ny lova aminao,
Dia tsy ho resy lahatra,
Fa Kristy no Mpanavotra.}

{3.
Io lamba io no mbola vao,
Na levona ''zao tany izao
Haharitra tsy rovitra,
Havaozin''Andriamanitra.}

{4.
Ny hery sy ny antranao
Toroy izao tontolo ''zao;
Ampandrosoy ny teninao,
Ampanekeo ny rafinao.}

{5.
Ny maty hahare ny feo
Hiantso azy ho velona;
Jeso! Ny hamarinanao
No lamba hitafianay.}
', '#1-VII-7', '#1') /end

insert into song values (15111, '511. Jeso no sakaiza tena tia anay', NULL, NULL, '
{1.
Jeso no sakaiza tena tia anay,
Izy ta hamonjy olombelona;
Aoka ho tiantsika ny Mpanayotra,
Fa Izy no sakaiza mahatehotia.}

{2.
Jeso no Sakaiza ifaliana.
Izy nahafoy ny voninahitra,
Mba hananantsika fanayotana
Sy handraisantsika ny fiainana.}

{3.
Jeso no Sakaiza itokiana,
Izy mahavaly sy mahanatra;
Aoka hinoantsika ny Mpanayotra,
Hahazoantsika fiadanana.}

{4.
Jeso no Sakaiza ivavahana.
Izy mahavonjy ny mpangataka;
Aoka hinoantsika ny Mpanayotra.
Mba handraisantsika
fanampiana.}

{5.
Jeso no Sakaiza fiarovana,
Izy maharesy ny mpanohitra;
Iza no hanimba izay arovany?
Aoka hisaorantsika ny
Mpanayotra.}

{6.
Jeso o, Sakaiza! Raisonao izao
Ny fisaoran-tsara Izay izay atolotray
Izahay mpanota te-hedera Anao.
Raiso re ny dera izay aterinay.}
', '#1-VII-7', '#1') /end

insert into song values (15121, '512. He ! Matoky aho Tompo', NULL, NULL, '
{1.
He ! Matoky aho Tompo,
Azoko ny teninao;
Teny soa mahafaly,
Tena avy aminao;}

{FIV.
He! Matoky aho Tompo,
Azoko ny teninao;
Teny soa mahafaly,
No omena ahy izao.}

{2.
Tahotra amana ahiahy
Dia ariako izao;
Fa nomenao toky aho
Sady to ny teninao.
[FIV.]}

{3.
Izay rehetra fahotana
Entiko hesorinao;
Ianao no mahadio,
Ka diovy aho izao.
[FIV.]}

{4.
Mahasasa, mahadio,
Mahavonjy Ianao,
Ka mandray sy mahatondra
Tsara ho an-tranonao.
[FIV.]}

{5.
Jeso Tompo! Avy aho,
Avy hankalaza Anao;
Raisonao re izay omeko:
Vola, tena, aina koa.
[FIV.]}
', '#1-VII-7', '#1') /end

insert into song values (15131, '513. Jeso Sakaizanay', NULL, NULL, '
{1.
Jeso Sakaizanay,
Jeso anay ;
Sady tsy mahafoy,
Jeso anay;
Raha ny olona
Mety mandao anay,
Jeso no tsy mandao,
Jeso anay.}

{2.
Jeso Sakaizanay,
Jeso anay;
Izy no Avotray,
Jeso anay;
Maty ny Tomponay
Noho ny helokay;
Afaka izahay,
Jeso anay.}

{3.
Jeso Sakaizanay,
Jeso anay;
Izy no aronay;
Jeso anay;
Reraka izahay
Noho ny adinay,
Jeso no herinay,
Jeso anay.}

{4.
Jeso Sakaizanay,
Jeso anay;
Izy no Lalanay,
Jeso anay;
Lalana marina
Hahatongavanay
Any an-danitra,
Jeso anay.}

{5.
Jeso Sakaizanay,
Jeso anay;
Manana izahay.
Jeso anay;
Jeso Sakaizanay
No hovantaninay
Any an-danitra,
Jeso anay.}
', '#1-VII-7', '#1') /end

insert into song values (15141, '514. Ry Jehovah Tomponay!', NULL, NULL, '
{1.
Ry Jehovah Tomponay!
Olom-bery izahay,
Nefa tsy mba foinao,
Fa vonjen''ny Zanakao:
FIV.
Zanahary, Aba, Ray,
Jeso Kristy havanay.}

{2.
Lehibe ny indrafo;
Iza re no nanampo
Famonjena taminao?
Kanjo azonay izao:
[FIV.]}

{3.
Gaga izahay izao!
Be fitia Ianao;
Avotr''aina azonay,
Hody aminao izahay.
[FIV.]}

{4.
Jeso o! Mpamonjy soa
Sy Mpamindro fo tokoa;
Avy ny navotanao
Ka midera Anao lzao:
[FIV.]}
', '#1-VII-7', '#1') /end

insert into song values (15151, '515. Tsara lova, ry Jeso o!', NULL, NULL, '
{1.
Tsara lova, ry Jeso o!
Ny mpanota voavotrao,
Fa fonenana soa
Amboarinao ao,
Hanefanao ny teninao:}

{FIV.
Ho amin''io tany io tokoa
No izoran''ny dianay;
Jeso Tompo no ao,
Sady efa manao
Trano soa hipetrahanay}

{2.
Tsara lalana, Jeso o!
Ny vahiny tarihinao;
Raha misy manjo,
Tsy mba kivy ny fo
Fa tohanan''ny herinao:
[FIV.]}

{3.
Tsara vatsy, ry Jeso o!
Ny mpandeha fahananao,
Fa ny teninao soa
Mofon''aina tokoa
Iveloman''ny olonao:
[FIV.]}
', '#1-VII-8', '#1') /end

insert into song values (15161, '516. An-dafin'' ny efitra lava sy maina', NULL, NULL, '
{1.
An-dafin'' ny efitra lava sy maina
No Misy ilay tany izay mamin''ny
fo,
Ilay tany izay efa nampanantenaina
Ho lovan''ny mino izay mankato.
Iriko indrindra ny mba ho tafita,
Tsy mety miadana mandrapahahita.}

{2.
Anefa ny lala-mankany no mafy,
Ny rafiko maro manotrika ao;
Ny nofo indrindra no tena
mpandrafy,
Ka tery ny lalan-kaleha izao!
Anefa tsaroako tsara ny teny
Milaza fa Misy fitsaharana eny.}

{3.
Ka aza mba ketraka, ry mpivahiny,
Hevero fa Jeso no momba anao;
IIay nampangina ny loza fahiny
Dia mbola mahay mampandresy
anao!
Andrasonao kely, ho lany ny fetra,
Ho ringana ny fahavalo rehetra.}

{4.
Izaho hiezaka mafy takoa
Hahazo ny loka ao alohako ao;
Ny tafio-drivotra mba mahasoa,
Fa hahatongavako faingana ao;
Indray mipi-maso ny eto an-tany,
Ny any an-danitra tsy hita lany.}
', '#1-VII-8', '#1') /end

insert into song values (15171, '517. Ny alina ho lasa', NULL, NULL, '
{1.
Ny alina ho lasa
Na ela aza re,
Hipoaka ny andro
Mahafinaritra:
Ny zavona hisava
An-tendrombohitra ao,
Ny vavahady tsara
He, tsinjoka izao.}

{2.
Mba sarotra tokoa
Ny fiakarana e;
Mamely mafy koa
Ny rivo-doza be;
Fa he ny mpivahiny
Izay efa trotraka
Tantanan''ny Mpamonjy
Tsy ho solafaka.}

{3.
Ny famonjen''i Kristy,
Ny fanayotany,
No laharano soa
Mangarangarana;
Nandramako sahady
Ny rano velona,
Ka dia nahazo hery
Am-pandehanana.}

{4.
Ny rano be hitànay,
Ny fefy tsilo koa
Ny làlana mahàna
Ho afaka avokoa;
Ka dera, haleloia,
No mendrika hatao
Ambara-pahatonga
Am-pitsaharana.}
', '#1-VII-8', '#1') /end

insert into song values (15181, '518. Rehefa vita ny asako ety', NULL, NULL, '
{1.
Rehefa vita ny asako ety,
Ka ny fanahiko raisina ery,
Miara-mitoetra aminao,
Tomponay
No hasambarako mandrakizay;}

{FIV.
Mandrakizay, sambatra re,
Azoko ilay fahasambarana be!}

{2.
Rehefa ataoko veloma ny ety,
Ka azoko Ilay Paradisa ery,
Sady mahita ny tavan''ny Ray
No hasambarako mandrakizay;
[FIV.]}

{3.
Miombom-pahasambarana ary
Amin''ireo olon-tiako taty,
Ka tsy misaraka mandrakizay
No hasambarako mandrakizay:
[FIV.]}
', '#1-VII-8', '#1') /end

insert into song values (15191, '519. Mba maniry hody aho', NULL, NULL, '
{1.
Mba maniry hody aho,
Ry Jesosy Tompo o!
:,:Ka ny foko hatanjaho
Mba hankany aminao.:,:}

{2.
Tsy tamana eto aho,
Mampahory ny aty,
:,: Koa dia mitalaho,
Mba hafindranao ary:,:}

{3.
Hody aho, ry Mpiantra,
Aoka mba horaisinao,
:,: Ianao dia feno antra,
Ka matoky aho izao:,:}
', '#1-VII-8', '#1') /end

insert into song values (15201, '520. Ny masoandronay', NULL, NULL, '
{1.
Ny masoandronay
Mirefarefa izao,
Madiva hody izahay
Hiverina aminao;
Ry Jeso o! Sasao,}

{FIV.
Sasao madio ''zahay,
Ho mendrika ny lanitra
Izay honenanay.}

{2.
Tsy ela intsony koa
Ity ady lava ity,
Ho afa-tsasatra tokoa
Rehefa tonga ary:
Ry Jeso o!
[FIV.]}

{3.
Madiva ho hitanay
IIay Tomponay ary,
Madiva hihaona aminay
Ny namanay taty:
Ry Jeso o l
[FIV.]}

{4.
He! Kely sisa izao
Ny hisentoanay,
Dia hofafan''ny tananao
Ny ranomasonay:
Ry Jeso o l
[FIV.]}

{5.
Ity hita maso ity,
Izay tsy mahafa-po,
Hisolo paradisa ary,
Izay hifalian''ny fo:
Ry Jeso o!
[FIV.]}
', '#1-VII-8', '#1') /end

insert into song values (15211, '521. Ravoravo re ny foko', NULL, NULL, '
{1.
Ravoravo re ny foko,
Fa Jesosy ao am-poko,
Izy no tsaroako
Amin''ny alehako.}

{2.
Andro fohy, mora lany
No mba ahy aty an-tany,
Fa ny ao an-danitra
Dia tsara lavitra.}

{3.
Tsy mba Misy ota any,
Ka tsy hisy hitomany,
Tsy mba hisy ny manjo,
Na ny alahelom-po.}

{4.
Any aho no hahazo
Zava-tsoa tsy halazo.
Satroboninahitra,
Haingon''ny nanafika.}

{5.
Dia ao no hahitako
Sady tena hiderako
An''i Jeso avotro:
Mifalia re, ry fo!}
', '#1-VII-8', '#1') /end

insert into song values (15221, '522. He, tanim-pahazavana be!', NULL, NULL, '
{1.
He, tanim-pahazavana be!
Tsy Misy maizina ao,
Ka any tsy mba hitete
Ny ranomasonao.
Izay voavonjy hiangona ao,
Hiara-mihira ny hira vaovao.
Ho sambatra any an-danitra,
Ao amin''ny Mpanayotra.
Moa ao an-danitra?
Eny ao an-danitra,
Ka tsy hisaraka?
Eny, tsy hisaraka.
Ho sambatra any an·danitra,
Ao amin''ny Mpanayotra.}

{2.
Ry Rainay feno indrafo,
Be fitiavana,
Omàny ny fanahiko
Handray fiainana.
Izahay, ry Jesosy, maniry Anao,
Fa maty nisolo anay Ianao:
Nanayotra anay mba ho afaka,
Ka tsy voaheloka.
Moa dia ho afaka?
Eny, ho afaka.
Ka tsy helohina?
Eny, tsy helohina
Jesosy Mpamonjy nanafaka,
Ka tsy ho voaheloka.}

{3.
0, ry Jesosy Tompo o!
Tsarovy izahay,
Ka mba ambino hatrizao
Ny fandehananay.
Vonjeo ny ondry manaraka Anao,
Fa ny otany no namonoana Anao.
Ho faly izahay ao an-danitra
Ao amim-boninahitra.
Moa ao an-danitra?
Eny, ao an-danitra.
Ka tsy hisaraka?
Eny, tsy hisaraka.
Hiara-mifaly ao an-danitra,
Ho feno voninahitra.}
', '#1-VII-8', '#1') /end

insert into song values (15231, '523. Te hifindra', NULL, NULL, '
{1.
:,: Te hifindra,: ,:
Aho, ry Jesosy o!
Be manjo aty an-tany,
Fifaliam-be no any
Aminao, ry Tompo o!}

{2.
:,: ny anjely :,:
Sy ny olo-masina
Tafahaona ka mihoby;
Sambatra ny tafatody,
Faly fa noraisina.}

{3.
 :,: Faly koa: ,:
Aho, ry Mpamonjiko,
Manantena fa hahita
Ny fonenana ao am-pita,
Ka handray ny lovako.}

{4.
:,: Tsy tamàna :,;
Aho amin''ny manjo,
Fa vahiny aty an-tany,
Ka maniry mba ho any
Aminao. ry Tompo o!}

{5.
:,: Ao tsy Misy :,:
Ota na aretina!
Tsara raha hodimandry,
Nefa aho te-hiandry
Ny fetr''andro marina.}
', '#1-VII-8', '#1') /end

insert into song values (15241, '524. Hitsahatra aiza, ry foko, izao?', NULL, NULL, '
{1.
Hitsahatra aiza, ry foko, izao?
Ka iza ataonao hiaro anao?
Ny tany va Misy hitsaharan''ny fo,
Na lova tsy simba, tsy mety ho lo?
Tsia, tsia, tsy mba aty
Izany, fa any an-danitra ery.}

{2.
Afoizo ny tany izay manindao,
Katsaho ny lanitra ho lovanao.
Jerosalema any, tanàna vaovao
Sy tsara tokoa, miandry anao,
Eny, eny, ao dia ao
No Misy tokoa fonenam-baovao.}

{3.
Ny mponina ao dia tretrika ampo,
Fa resy ny ota, mahery ny to,
Ka faly midera ny Tompo ny ao;
Ny mino hihira ny hira vaovao!
Indro. indro, be dia be
Tokoa ny soa ao an-danitra re!}
', '#1-VII-8', '#1') /end

insert into song values (15251, '525. Vahiny eto isika', NULL, NULL, '
{1.
Vahiny eto isika
Ka monina amin-day.
Mitady mba hahita
Ny amin-dRay;
Fa ao ny fanjakany
Sy fiadanana,
Ka ao tsy hita lany
Ny fitiavana.}

{FIV.
Ao no fodiana,
Mihaona ao ny mino,
Ka tsy hisaraka.}

{2.
Izay aty an-tany
Dia mora tapitra,
Fa ny tsy mety lany
Dia ao an-danitral
Fa ao ny Tompontsika
Iray nohomboana
Mba hahazoantsika
Ny fifaliana.
[FIV.]}

{3.
Manafotra ny tava
Ny ranomaso be,
Kanefa dia hisava,
Fa hofafana e!
Isika manantena
Fa ny Mpampakatra
Ho tonga mba hitsena
Ny ampakarina.
[FIV.]}
', '#1-VII-8', '#1') /end

insert into song values (15261, '526. Jerosalema ambony', NULL, NULL, '
{1.
Jerosalema ambony
Irin''ny fo izao.
Fa ny aty
Dia tsy mba fy;
Ny lanitra hazony
Fa manina ny ao
Jerosalema ambony
Irin''ny fo izao.}

{2.
Fa any hofafana
Ny ranomaso be
Izay latsaka
Tan-dàlana
Ka hody fideràna
Sy fifaliam-be.
Fa any hofafana .
Ny ranomaso be.}

{3.
Jesosy Tompontsika,
Izay ao an-dapany,
Manao hoe:
Ireto re
Ny oloko nafindra
Handray ny lovany.
Jesosy Tompontsika
Manasa anao hankao.}
', '#1-VII-8', '#1') /end

insert into song values (15271, '527. Nahoana no voageja aty', NULL, NULL, '
{1.
Nahoana no voageja aty
Ity tena tsy tamana ity? .
Nahoana no tsy hitanay
Ilay tany tadiavinay?
FIV.
Ry lanitra o, ry lanitra o,
Ianao tokoa, irin'' ny fo,
Irin'' ny fo.}

{2.
Ny sombin-tsoa hita aty
No mariky ny be ary;
Izao tsy mahafa-po izao
No anirianay mba ho ao.
[FIV.]}

{3.
Ny fitiavanay aty
No tena akon''ny ary.
Izao tsy tanteraka izao
Manandratra ny fo ho ao.
[FIV.]}

{4.
Izao fiainanay izao
Tandindon''ny fiainana ao;
Ny andro tsy mateza aty
Dia tapaky ny mbola ary.
[FIV.]}
', '#1-VII-8', '#1') /end

insert into song values (15281, '528. Tsaroako ho avy ny maraina', NULL, NULL, '
{1.
Tsaroako ho avy ny maraina
Izay hisavan''ny zavona aty,
Ka hitrangan''ny masoandron''aina,
Dia Jeso Ilay malalako ary.}

{2.
Hevero fa tsy hisy fahotana,
Na heloka, na fahoriana;
Ny ranomasontsika hofafana
Ka hosoloam-pihobiana.}

{3.
Ny havan-tiana tety an-tany
Nandao antsika ka nisaonana,
Ireo no hitsena antsika any,
Hihaona indray ny tafasaraka.}

{4.
Ny fiderana ataon''ny olo-maro,
Izay samy masina sy ravo ao,
Sy hobin''ny anjely no hiharo,
Endrey ny hatsaran''ny hira vao.}

{5.
Ry tany tsara, aoka hahafaly
Ny foko ny mihevitra anao
Raha saro-dalàna sy mampijaly
Ny tany itoerako izao.}
', '#1-VII-8', '#1') /end

insert into song values (15291, '529. Jesosy no Mpiandry tsara maty', NULL, NULL, '
{1.
Jesosy no Mpiandry tsara maty
tia,
Ny rany nampiray antsika olony;
Fa Izy no mandidy hampifankatia
Izay te ho olony.}

{2.
Ka manahafa Azy Tomponao, ry
mino,
Tsarovinao ny didy, izay natolony,
Fa tiany hifankatia isika mpino,
Fa samy olony.}

{3.
Tsarovy fa iray tampo izao isika;
Tsarovy re fa samy mpiantafika;
Miraisa hina mba hiaraka tafita
Ho ao an-danitra.}

{4.
Ka aza mba mijery arina an-tava;
Mifanaraha tsara sy mifamelà,
Fa aza tia fotsiny amin''ny vava;
Dia aza mifandà.}

{5.
Mifankatiava ary mifanaova soa;
Mifandefera tsy hiavona am-po;
Vonoy re ny lolompo sy fosa koa,
Ka aoka ho tso-po.}

{6.
Morà fo , mihavàna, maleme Fanahy,
Ka aza mety hanaratsy namana;
Mifankatiava re, toy ny mpirahalahy
Miara-dàlana.}

{7.
Ka mivavaha re ho an''ny rahalahy,
Hiraisantsika tsara fa iray manjo;
Ny fo izay tia tsy mba
mampiahiahy, Horaisin''i Jeso.}
', '#1-VII-9', '#1') /end

insert into song values (15301, '530. Fitiavana rano velona', NULL, NULL, '
{1.
Fitiavana rano velona,
Loharano lalina ;
Ka miafina ao  anatiny
Ny fiainan-danitra!}

{2.
Fitiavana no nirahina
Avy tamin-Jehovah;
O, mba raisonao, dia ho fantratrao
Ny fahasambarana.}

{3.
Fitiavana didy tokana
Ary didy lehibe;
Mitoera ao, hitoeranao
Amim-piadanam-be.}
', '#1-VII-9', '#1') /end

insert into song values (15311, '531. Ry Jeso loharanon''aina', NULL, NULL, '
{1.
Ry Jeso loharanon''aina,
Izay foto-pahasoavana!
Ny fonay toy ny tany maina,
Fenoy fitiavana!
Aidino ao ny tena aina,
Mba hampifankatia anay,
Fandrao ho tonga very maina
Ny fitiavanao anay.}

{2.
Ny hasomparana levony
Soloy fitiavana;
Ny lolompo ampijanony
Ho tia fihavanana;
Ny fo malaky mankahala
Ovay ho tia namana;
Ampianaro hahalala
Ny didim-pitiavana.}

{3.
Toroy sy ampianaro
Ny fitiavanao izahay;
Mpiandry tsara o, velaro
Ny fonao mba ho hitanay;
Mba ho tarafiny tokoa
Ny fonay mbola maizina,
Ka samy hifanisy soa
Sy  hifamela heloka.}
', '#1-VII-9', '#1') /end

insert into song values (15321, '532. Ry Jeso lohan''ny mpandefitra', NULL, NULL, '
{1.
Ry Jeso lohan''ny mpandefitra,
Nilaozanao ny voninahitrao,
Mba hahatonga anay mpiavona
Hanetry tena eo imasonao.}

{2.
Ry tsy miavanayona am-po!
Izaho te-hianatra aminao
Mba hitsaharan''ny fanahiko,
Fa mampiadam-po ny entanao.}

{3.
Ny fietrenao, ry Voahombo o,
No iakaram-boninahitra;
Ny tany sy ny lanitra vaovao
Hataonao lovan''ny mpandefitra.}

{4.
Mifandefera mifanajà,
Ry samy voavela heloka!
Miraisa hazofijaliana,
Hiraisantsika voninahitra.}
', '#1-VII-9', '#1') /end

insert into song values (15331, '533. Iray ny Ray Tsitoha', NULL, NULL, '
{1 .
Iray ny Ray Tsitoha,
Mpitia olombelona;
Iray nyTomposoa,
Iray ny rano velona,
Iray ny vavahady .
Izay hidirana;
Ny rafy sy ny ady
Dia iombonana.
Iray ny fahantrana
Ilaozantsika aty,
Iray ny fanjakana
Alehantsika ary.}

{2.
Ry zana-Janahary,
Ry voavela heloka
Miraisa hina ary,
Ka mifandraisa tanana ;
Mifamelà isika.
Fa samy meloka;
Milanajà isika.
Fa samy zanaka.
Ry Jeso be fitia,
Ampio ny olonao
Mba ho mpifankatia
Manahaka Anao.}
', '#1-VII-9', '#1') /end

insert into song values (15341, '534. Misy didy tena tsara', NULL, NULL, '
{1.
Misy didy tena tsara
Izay nomen''ny Tompo hoe:
Mba mifankatiava tsara
Ianareo izay mino e!
:,: Ka miraisa :,:
Izao no didy lehibe.}

{2.
Tonga Jeso mba hanompo
Sy mba hampihavana;
Tsy mpanaraka ny Tompo
Izay tsy tia namana.
:,: Ka tadiavo :,:
Izay ho fampiraisana.}

{3.
Fa iray tokoa ny loha,
Dia Jesosy Zanaka,
Endriky ny Ray Tsitoha,
Izay halaina tahaka.
:,: tsy ho resy, :,:
Raha miray ny tafika.}

{4.
Jeso no ilay foto-kazo,
Tena voaloboka;
Ka sy sampana halazo,
Raha tafasaraka.
:,: Ikambano, :,:
Ka aza mba mihataka.}
', '#1-VII-9', '#1') /end

insert into song values (15351, '535. Finaritra ny manome', NULL, NULL, '
{1.
Finaritra ny manome
Ireo mahantra marobe
Ka mahafoy fananana
Hiantra ny tsy manana.
Izao no asa mahasoa
Izay manendrika tokoa;
Ny manome an-tsitrapo
Hitsinjo olana ory fo.}

{2.
Ry olon''Andriamanitra!
Mba mihetseha fatratra
Hamonjy sy hanafaka,
Hanampy ireo madinika.
Izay mamokin-tanana
Malain-kiantra namana,
Izay mihirim-belona}

{3.
Ny vola izay nafoinao
Dia tsy mba very foana izao,
Fa ny mahantra no mandray
Ka lasan''i Jehovah Ray
Ireo kamboty mba vangio,
Mpitondratena mba jereo;
Izao no fivavahana
Izay madio sy masina.}
', '#1-VII-9', '#1') /end

insert into song values (15361, '536. Mifankatiava tsara', NULL, NULL, '
{1.
Mifankatiava tsara
Jeso no mampihavana,
Fa Izy no alalantsika
Amin'' Andriamanitra.}

{2.
Mifankatiava tsara
Mba hahatsara antsika,
Fa fatotry ny didiny
Sady anton''ny lalàny.}

{3.
Hitsahatra finoana,
Mbamin''ny fanantenana,
Nefa ny fitiavana
Haharitra mandrakizay.}

{4.
Mifankatiava tsara,
Tsy maharatsy namana,
Ary tsy mahatezitra
An''Andriamanitra koa.}
', '#1-VII-9', '#1') /end

insert into song values (15371, '537. Endrey, ny hamaroan''ny soa ', NULL, NULL, '
{1.
Endrey, ny hamaroan''ny soa
Nomenao Ray ho ahy koa!
Ny aiko, he, navotanao,
Ny heloko vonoinao.}

{FIV.
Eny, Raiko,
He! Ravoravo tokoa
Ity zanakao voavonjy re,
Ka velombelona ery.}

{2.
He, azoko tokoa Jeso,
Ilay perla soa, madio sy vao;
Ny zava-tsoan''ny tany re
Dia tsy ahoako tokoa.
[FIV.]}

{3.
Ny vavako dia tsy mahay
Hamisavisa izao ry Ray;
Ny fanomezan-dehibe
Dia tsy voatonoko koa.
[FIV.]}

{4.
Ny foko no hidera izao,
Hisaotra sy hanaja Anao,
Hanoa, ho vonona hanao
Ny sitrakao rehetra.
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15381, '538. Finaritra ny masoko', NULL, NULL, '
{1.
Finaritra ny masoko
Mijery ny Mpamonjiko
Ny vavako hisokatra
Hidera ny Mpanavotro:}

{FIV.
Andro soa, andro soa,
Fa Jeso no Mpanavotra;
He! Ravo ny fanahiko
Mihira ato anatiko,
Andro soa, andro soa,
Fa Jeso no Mpanavotra.}

{2.
Ho tiako ny Tompoko
Ambara-pahafatiko,
Fa izy no Mpanafaka
Manaisotra ny heloko
[FIV.]}

{3.
Faingàna re, ry havako!
Hatòny e! ny Tomponao;
Andeha mba hiaraka
Hihira ny hivavaka:
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15391, '539. Faly dia faly ', NULL, NULL, '
{1.
Faly dia faly
Izahay mpanomponao,
Izay mihira eto
Anatrehanao.
Fa be fiantra Ianao,
Ry Jeso Tompo tianay!
Ka feno haravoana
Tokoa izahay;}

{FIV.
Faly dia faly
Izahay mpanomponao,
Izahy mihira eto
Anatrehanao.}

{2.
Ela dia ela,
No nandavanay Anao;
Be ny zava-dratsy,
Efa vitanay.
Na diso aza izahay,
Anefa faly aminao;
Fa afakao ny  helokay,
Ry Tompo tianay!
[FIV.]}

{3.
Sarotra tokoa
Ny alehanay izao;
Be ny alahelo,
Iaretanay.
Na ory aza izahay,
Dia mbola faly aminao.
Fa levonao ny tahotray,
Ry Tompo tianay!
[FIV.]}

{4.
Manantena tsara,
Izahay mpanomponao,
Mba handray ny zara,
Tehirizinao.
Rehefa maty izahay,
Ka entinao an-danitra;
Ho azonay ny lova soa,
Hatolotrao anay;
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15401, '540. Hitako ny lalan-tsara', NULL, NULL, '
{1.
Hitako ny lalan-tsara,
Ka hizorako izao,
:,: Azoko ny fa monjena,: ,:
Ka hotanako tokoa
:, :Manampia,:,:
Ry Fanahy masina.}

{2.
Ravoravo aho, Jeso!
Ianao no Tompoko;
:,:Faly aho, ry Mpamonjy! :,:
Afakao ny heloko;
:,: Gaga aho :,:
Noha ny fanayotanao.}

{3.
Fitiavana ihany
No natolotrao anay;
:,:Nefa kosa fahotana:,:
No navalinay Anao:
.,. Aza isaina :,:
Re ny hadisoanay.}

{4.
Ny mpanota hilazako
Ny Mpamonjy tànako;
:,:Hilazako fa ny rany: ,:
No nataony avotra;
:,: Matokia, :,:
He, ny fitiavany!}
', '#1-VII-10', '#1') /end

insert into song values (15411, '541. He! Izany hira manako re!', NULL, NULL, '
{1.
He! Izany hira manako re!
IIay zaza very tonga amin-dRay;
He! Falifaly izay mandre;
IIay efa maty velona indray.}

{2.
Indro ny Rainy lasa nankao,
He! Fa miteny: "Peratra soa.
Soloy ny lamba, kapa alao."
IIay efa maty velona indray.}

{3.
He izany hoby an-tranon-dRay!
Fa voavonjy ilay nania;
Hobio, ry tany sy lanitra!
IIay efa maty velona indray.}

{4.
O, ry anjely an-dapa soa!
Miraisa hina hanandra-peo;
Aza mangina, fa mba lazao,
IIay efa maty velona indray.}
', '#1-VII-10', '#1') /end

insert into song values (15421, '542. Jehovah no anjarako', NULL, NULL, '
{Jehovah no anjarako,
Izaho tsy halahelo,
Fa Jeso no Mpanayotro.
Izaho ho ravoravo.
Maro no tia harena,
Betsaka no mila vola,
F''izaho efa manana,
Jehovah no anjarako,
Ny didiny no mamiko,
Ny asany no tiako,
Ny teniny mahahendry,
Ny ran''ny Zanany
No mahadio.
Tsy mba mamiko ny tany,
Tsy sitrako ny filàny,
Omeko veloma izany;}
', '#1-VII-10', '#1') /end

insert into song values (15431, '543. Mihoby ny fanahiko', NULL, NULL, '
{1.
Mihoby ny fanahiko,
Fa voavonjy izao ;
Ny ratran''ny Mpamonjiko,
No misy aim-baovao.}

{FIV.
Ny ran''i Jesosy,
No tena avotro,
Ka sambatra aho
Sy tretrika ao am-po.}

{2.
Ny ratran''ny Mpamonjinao,
Ry foko, mba jereo.
Fa hanasitrana anao,
Ka ho fiainanao.
[FIV.]}

{3.
Mba mibitaha re, ry fo,
Ny gadra tapaka;
Ny hajambana izay nanjo,
Izao dia afaka.
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15441, '544. Misaora an''i Jehovah', NULL, NULL, '
{1.
Misaora an''i Jehovah,
Miantsoa ny anarany,
Mitoria ny asany,
Amin''ny olombelona.}

{2.
Ambarao ny fiantrany,
Lazao ny famindrampony,
Fa mahagaga antsika
Ny titiavany antsika,}

{3.
Mpanota tsy naringana,
Very mana-Mpanavotra,
Maty ka nomeny aina,
Ho velona mandrakizay.}

{4.
Ry Andriamanitray o!
Ry Jesosy Mpamonjy o!
Ry Fanahy Masina o!
Vonjeo ny olona ory.}

{5.
Fa zohy no itoerana,
Lava-bato no ierena
Fa indrafo no ifalian''
Ny mpivahiny mandalo.}
', '#1-VII-10', '#1') /end

insert into song values (15451, '545. Endrey, izany hafaliana', NULL, NULL, '
{1.
Endrey, izany hafaliana,
Fa velona aina izahay ;
Tanteraka ny faniriana,
Ka dera ho Anao ry Ray!
Atolotray tokoa ny tena,
Mba ho fanati-tsitrakao,
Satria manaiky sy ekena
Ho tonga olom-baovaoa}

{2.
Ny tanindrazanay malala,
Ry Jeso Tompo, anjakao
Hiorina eto, tsy hiala,
Ny fiadanana entinao!
Ny aizin''ota anie hisava,
Ny fahantrana koa handao,
Fa hotarafin''ny Mazava,
Hanjary tany vaovao.}

{3.
Ny fiangonanao angony,
Fanahy o, ho tena iray,
Ka tsy hizarazara intsony
Fa Ianao no mampiray,
Dia ho fanasina, fanilo,
Ho an''izao tontolo izao;
Hijoro, tsy handraraka ilo,
Hitafy toetra vaovao.}
', '#1-VII-10', '#1') /end

insert into song values (15461, '546. Haleloia, haleloia!', NULL, NULL, '
{1.
Haleloia, haleloia!
Jeso no Mpamonjiko,
Haleloia, haleloia!
Afaka ny heloko.
Sambatra aho tretrika aho
Ahy re ny lanitra,
Haleloia, haleioia!
Deso no Mpanavotra.}

{2.
Haleloia, haleloia!
Ravoravo re ny fo.
Haleloia, haleloia!
Jeso re no soloko.
Afaka aho, afaka aho,
Levona ny otako;
Haleloia, haleloia!
Lanitro no lovako.}

{3.
Haleloia, haleloia!
Matokia hianao.
Haleloia, haleloia!
Maty mba ho solonao
Jeso Tompo mahatoky,
Ry mpanota very o!
Haleloia, haleloia!
Raiso ny famindram-po.}
', '#1-VII-10', '#1') /end

insert into song values (15471, '547. Ry Jeso Masoandronay o !', NULL, NULL, '
{1.
Ry Jeso Masoandronay o !
Ny azin''ny fo lasa tokoa
Fa tonga aty anatinay izao
Mazava lehibe.}

{FIV.
Eny, Jeso! Zoky be fitia,
Soa lehibe no netinao taty,
Avy izahay, mba hisaotra Anao
Noho ny fitiavanao.}

{2.
Vangianao ny tokantrano
Ka menatra ery, kaody sy lao
Ny fomba ahitana angano
Izay tsy mba sitrakao.
[FIV.]}

{3.
Mpanjaka Fiadananay o !
Ekena tokoa ny herinao ;
Ny sabatry ny firenena
Hatao fangady izao.
[FIV.]}

{4.
Ry Jeso Tompo tia anay o !
Misaotra Anao izahay izao
Fa henika  ny hasoavanao
Izao tontolo izao
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15481, '548. He, Jehovah no Mpiandry ahy', NULL, NULL, '
{1.
He, Jehovah no Mpiandry ahy,
Tsy mba misy hatahorako;
Raha izy no mitondra ahy
Ahi-maitso no handriako
Eny, fa ny tany karankaina,
Dia hody saha lonaka,
:,: Ary ao amoron-dranon''aina
Hialan''ny foko sasatra :,:}

{2.
He, Jehovah no Mpitarika ahy
Amin''ny alom-pahafatesana
Koa tsy ho kivy ny fanahy
Noho ny aloky  ny fasana
Ianao, ry Tokiko irery,
No hiara-dia amiko.
:,: Ka ny tsorakazonao mahery
No hanafaka ny tahotro :,:}

{3.
Amelaranao latabatra aho
Eo imasom-pahavaloko ;
Namboaranao kapoaka aho,
Mampahery sy mahafa-po;
Eny, indrao tsy hita lany
No hiaraka amiko zaio
:,: Mandram-pahatongako ho any
Amim-pahazavan''ny tavanao :,:}
', '#1-VII-10', '#1') /end

insert into song values (15491, '549. Jesosy no asandratro', NULL, NULL, '
{1.
Jesosy no asandratro,
Fa Izy no Mpanavotro,
Ny tànany nanafaka
Ny aiko tao an-davaka;
Ny rafiko ho ketraka,
Izaho afa-kenatra,
Fa manana ny Tompoko,
Izay mandako sy aroko;}

{FIV.
Ry foko o! Ankalazao
Jesosy, Izay Mpamonjinao.}

{2.
Ny tsiron''ny fiainana
Ananako sahady re,
Fa efa mby an-tànana
Ny famonjena lehibe;
Ny raon''aina azoko,
Ny mofon''aina haniko,
Sakafo mahatanjaka,
Mamelona ny reraka;
[FIV.]}

{3.
Ny rany no anjarako,
Ny ainy no fananako ;
Ny lanitra ho lovako,
Ny tany dia hilaozako;
Ny ota tsy hanaraka,
Ny ady dia ho tapaka,
Ny aloka ho tapitra,
Fa ao ny tena zavatra;
[FIV.]}

{4.
Ny hanitry ny lanitra
Mamerovero fatratra ;
Ny kalon''ny voavotra
Manatsika ahy hiakatra;
Ny tavan''ny Mpamonjiko
No tsinjon''ny fanahiko,
Ny tavany manafaka
Ny azina ao anatiko:
[FIV.]}

{5.
Ny masoko mibanjina
Tahaka ny mpiambina,
Maniry ny Mpamonjiko
Ho tafahaona amiko,
Izaho hovimbininy
Hipetraka eo anilany,
Hanao ny hiran-danitra
Sy honina finaritra;
[FIV.]}
', '#1-VII-10', '#1') /end

insert into song values (15501, '550. Ry Tompo o, ny tany maina', NULL, NULL, '
{1.
Ry Tompo o, ny tany maina
No itoerako;
Midina re, ry Andon''aina
Ho ao anatiko.}

{2.
Tsy aloka malaky lasa
No angatahiko;
Fa hery monina hiasa
Sy hanavao ny fo.}

{3.
Ny afonao ampandreseo
Ny afon''nofoko;
Ny sitrakao anie hekeko
Ho tena sitrako.}

{4.
He! tena zavatra ilaiko
Ny hohavaozinao;
Ny ainao asoloy ny aiko
Ho velon-ko Anao.}
', '#1-VII-11', '#1') /end

insert into song values (15511, '551. Tsy lany fahoriana', NULL, NULL, '
{1.
Tsy lany fahoriana
Ity tanintsika ity,
Fa be ny fihafiana
Izay hitako aty.
Ny Tompo no jereko,
Raha reraka ny fo,
Ka eo no fiereko,
Raha azon''ny manjo.}

{2.
Ety alaim-panahy
Jeso, ny olonao,
Kanefa tsy manahy
Izay mahita Anao.
Ianao no hojereny,
Ka moa ho resy ve?
Ianao no handreseny
Ny fahavalo be.}

{3.
Tsy azo ialana .
Ny eo anilanao,
Mahita fanimbana
Izay mahafoy Anao.
Asain''ny fahoriana
Jerena Ianao,
Mahazo fifaliana
Ny maso aminao.}

{4.
Rehefa hialana
Izao tontolo izao.
Ny tany tsy hitana
Izay mahita Anao.
Ka dia hojereko,
Ry Tompo, Ianao,
Fa Ianao homeko
Izao aiko kely izao.}
', '#1-VII-11', '#1') /end

insert into song values (15521, '552. Ry foko o, mba atolory', NULL, NULL, '
{1.
Ry foko o, mba atolory
Ny Rainao ny manjo anao;
Na mafy aza ny mahory,
Dia hovonjeny Ianao;
Ny lanitra no andrandrao
Hahafaka ny sentonao.}

{2.
Andriamanitra mahita
Izay hahasambatra anao,
Ka Izy no mahay mamita
Izay zava-tsarotra aminao;
Tsy hotsitsiny aminao
Izay mety hahasoa anao.}

{3.
Ka aza kivy, ry mahantra,
Na ory aza hianao,
Fa matokia ny Mpiantra,
Izay tsy mahafoy anao;
Ny amboariny izao
Dia famonje a ho anao.}

{4.
Dia aoka hianao hatoky,
Raha mafy ny manjo anao,
Ny Rainao dia mahatoky,
Ka ampy hiankinanao;
Tsy Misy tsy ho afany,
Raha avy ny fotoany.}
', '#1-VII-11', '#1') /end

insert into song values (15531, '553. Any an-danitra ambony', NULL, NULL, '
{1.
Any an-danitra ambony
Misy mijery anao;
:,:He, feno antra ny fony,
Koa mangina izao! :,:}

{2.
Ry rera-po mitomany,
Jeso onena anao;
:,: Ampy ho toky izany,
Koa mangina izao!:,:}

{3.
Na dia be ny mahory,
Izy hiaro anao
:,: Tsy ho tra-doza akory,
Koa mangina ;zao!:,:}

{4.
Mba mahareta miandry,
Izy hanafaka anao;
:,: Haka anao hodimandry,
Koa mangina izao! :,:}
', '#1-VII-11', '#1') /end

insert into song values (15541, '554. Ry Tompo mahavonjy o!', NULL, NULL, '
{1.
Ry Tompo mahavonjy o!
Henoy ny vavakay.
Ka mamelà ny helokay
Fa tena tsy mba fantatray
Izay tokony hatao.}

{2.
Ry Tompo be fiantra o!
Malemy izahay,
Kanefa raha ampianao
Dia hnan-kery toa Anao
Izahay mpanomponao.}

{3.
Ry Tompo Izay Mpandresy o!
Omeo ny herinao,
Fa tianay handresena koa
Ny ota mety hahavoa
Ny olon-tianao.}

{4.
RVTompo Ilay mahery o!
Ampionao izahay,
Hijoro, hampiseho izao
Fa aminay tokoa Ianao,
Ry Tompo tia o!}
', '#1-VII-11', '#1') /end

insert into song values (15551, '555. Rahoviana no ho lany', NULL, NULL, '
{1.
Rahoviana no ho lany
Ny manjo aty an-tany?
Jeso Vatolampiko
Ampy hiankinako.}

{2.
Mahereza ry Fanahy,
Aza manana ahiahy,
Jeso itokiako
Hampionona ny fo.}

{3.
Ny mahory dia mandalo,
Ringana ny fahavalo,
Ny aty hilaozako
Mba handray ny lovako.}

{4.
Mino aho, ry Mpamonjy,
Fa ho tena voavonjy;
Afakao ny heloko,
Ianao isaorako.}

{5.
Dia ho lany ny mahory,
Ka ho samy tafavory
Eo anatrehanao
Izahay mpanomponao.}
', '#1-VII-11', '#1') /end

insert into song values (15561, '556. Ny Paradisa an-tany', NULL, NULL, '
{1.
Ny Paradisa an-tany
Tsy misy, re, izany!
Ny hazon''aina very,
Fa tsilo be no ao.}

{FIV.
Ry Foko, aza kivy,
Fa ao ny tany soa,
ny Paradisa hisy;
Ry foko, andrandrao!
Tsy Misy ory any,
Fa tretrika avokoa;
Afaka ady ka mihoby;
Misy hazon''aina ao.}

{2.
Mandrahona nyady,
Mirehitra sahady,
Fa be ny fahavalo
Mamely ahy izao.
[FIV.]}

{3.
Ny eso, latsa, ratra,
Matetika mihatra,
Tsaroako avaly,
Ny asa vitako.
[FIV.]}

{4.
Miferinaina eto
Tomany sy misento;
Indrisy! Ory aho,
Vonjeo, Tompo o!
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15571, '557. Raha manahy ny Fanahy', NULL, NULL, '
{1.
Raha manahy ny Fanahy
Azom-pahoriam-be,
Ka ny aina mitaraina,
Ranomaso mitete,
Aza ketraka ety,
Fa tazano ny ary.}

{2.
Ao miharo feo maro,
Fatratra ny hira ao;
Ao mifaly ny nijaly,
Ory teo, ravo izao;
Ao Jesosy zokinao
Faly hovantaninao.}

{3.
Hazon''aina tsy mba maina
No maniry tsara ao,
Voninkazo tsy halazo
No ho fifalianao;
Faly ny mandre i~o,
Mainka fa ny tonga ao.}

{4.Ry Mpamonjy, ho tra-bonjy
Anie ny olonao!
Fa maniry ny tahiry
Ao an-danitra aminao;
Mba faingàna Ianao
Haka ny malalanao!}
', '#1-VII-11', '#1') /end

insert into song values (15581, '558. Ity fonenana ity', NULL, NULL, '
{1.
Ity fonenana ity
Tsy manam-pitsaharana;
Maniry aho ho ary
An-koatry ny fasana,
Ny lanitra no hitodiako,
Fa tena tanindrazako.}

{2.
Kanefa mahatahotra
Ny ranobe alehako,
Fa ny tafio-drivotra
Manohitra ny samboko,
Ny tselatra, ny kotrokorana,
Mamely sy mandrahona.}

{3.
Ny sambon''ny fiainako
Mamaky onja sarotra,
Ka amin''ny alehako
Mitopatopa fatratra
Ny alondranom-pahoriana,
Kanefa tsy maninona.}

{4.
Fa ny Mpamonjy tokana
Mitarika ny samboko;
Ny Tompon''ny fiainana
Miara-mita amiko,
Mba ho tafita aho olony,
Ka honina any Aminy.}
', '#1-VII-11', '#1') /end

insert into song values (15591, '559. He, Vetivety ny aty an-tany', NULL, NULL, '
{1.
He, Vetivety ny aty an-tany;
Fa trano lay no itoeranao;
Ka aza ketraka na mitomany,
Fa andrandrao ny
tanindrazanao.}

{2.
He, Vetivety ao an-dalan-tery
No mahasarotra ny dianao.
Ka aza kivy, fa Ilay Mahery
Mamboa-trano hitsaharanao.}

{3.
He, Vetivety ao an-tany maina
No mahabe ny fijalianao,
Ka dia ho hitanao ny ranon''aina
Hahafaka ny hetahetanao.}

{4.
Arahanao ny Ialan-toratady,
Mba hahafaingana ny dianao;
Jesosy Tomponao no vavahady,
Ny lanitra no hovantaninao.}

{5.
He, Vetivety ary dia ho hita
Ny tany soa mandry fahizay;
Jesosy o, faingàna hanayita
Ny fikasanao tsara aminay!}
', '#1-VII-11', '#1') /end

insert into song values (15601, '560. Mpivahiny aho eto', NULL, NULL, '
{1.
Mpivahiny aho eto
Tahaka ny razako,
Ka ny foko feno sento
Amin''ny alehako.
Eny, tsy ety tokoa
No ho fitsaharako,
Fa mandalo avokoa
Izay rehetra hitako.
Ka izaho tsy miato
Andro aman''alina,
Nefa aho, izay misento
Dia hasoavina.}

{2.
Be ny mampahory ahy
Mila tsy ho zakako,
Toa velona ahiahy
Ao am-pandehanako;
Nefa kosa tsy mba tery
Sady tsy mamay fo,
Fa izaho tsy irery
Eto amin''ny manjo,
Fa Jesosy momba ahy .
Ka manala-tahotra;
Raha ory ny Fanahy,
Izy no mahavotra.}
', '#1-VII-11', '#1') /end

insert into song values (15611, '561. Raha maizina ny làlana', NULL, NULL, '
{1.
Raha maizina ny làlana,
Jehovah Tompo o!
Ianao no iankinana,
Ianao no jiroko.}

{2.
Tsy hitako izay hanjo,
Fa takona amiko
Ny andro sisa iainako,
Izay nalahatrao.}

{3.
Na mbola ela velona,
Na maty haingana,
Ny zarako voatendrinao,
Tsy te-hifidy aho.}

{4.
Fa Kristy ivelomako
Raha mitoetra;
Ny faty tombondahiko,
Fodiana Aminao.}

{5.
Ny fanompoako aty,
Tsy mahavesatra ahy;
Tsy maika te-niala ety
Raha tsy antsoinao.}

{6.
Raha tapitra ny androko,
Dia tsara lavitra
Ny mody any aminao
Hiala sasatra.}

{7.
Ny sitrakao no sitrako,
Jehovah Tompo o!
Ny teninao andrasako,
Ianao no tokiko.}
', '#1-VII-11', '#1') /end

insert into song values (15621, '562. Miandry ao ambony', NULL, NULL, '
{1.
Miandry ao ambony
Ny fifaliana;
Aty tsy mety tony
Ny fahoriana,
Ka tonga sesilany
Ny fitarainana;
Ary tsy hita lany
Ny hasambarana.}

{2.
Variana mikorana
Ny tsy misaina be,
Ny hendry dia mitana
Ny hevitra hoe:
Aleoko mitomany
Noho ny otaka;
Ny lanitra ihany
No fianinako.}

{3.
Ny lalan''ny adala
Tsy mba harahiko,
Tsy mampisalasala
Izay anjarako;
Ny sasany mikendry
Ho afa-po aty,
Izaho dia miandry
Ny lovako ary.}

{4.
Izaho manantena
Anao, ry Tompo o!
Ka hobim-pamonjena
No hoventesiko;
He, Vetivety ihany
No hiaretako,
Tsy ela dia hankany
An-tanindrazako.}
', '#1-VII-11', '#1') /end

insert into song values (15631, '563. Ry mana-manjo!', NULL, NULL, '
{1.
Ry mana-manjo!
Akaiky anao,
Ny Tompo, lIay be fiantra.}

{2.
Ka Izy anie
Hitahy anao,
Hanafaka ny mampahory.}

{3.
Ny ory aty
Ho faly ary,
Ka aza mba be alahelo.}

{4.
Ny Tomponao re
Hanefa tokoa
Izay hataky ny malahelo.}

{5.
Ny lapany ao
Voavoatra ho anao
Honenanao mandrakizay re.}
', '#1-VII-11', '#1') /end

insert into song values (15641, '564. Tsy hita izay androko sisa aty', NULL, NULL, '
{1.
Tsy hita izay androko sisa aty,
Ahiko fandrao tsy tonga ary,
Ry fanahiko! Ao ny Mpamonjy
anao,
He! Jeso Mpamonjy anao.(in-3)
Ry fanahiko! Ao ny Mpamonjy
anao,
He! Jeso Mpamonjy anao.}

{2.
Izaho anefa tamàna aty;
Tsy hiomana va, fa hifindra ary?
Fa efa fantatro izao fa havela ity,
He! Lanitra soa ary. (in-3)
Fa efa fantatro izao fa havela ity,
He! Lanitra soa ary.}

{3.
IIaozy ity, hadinoy, ry fo!
Afoizo izao, ry fanahiko o!
Fa efa reko ny Tompo miantso
anao,
He! Feo mahafaly izao. (in-3)
Fa efa reko ny Tompo miantso
anao,
He! Feo mahafaly izao.}
', '#1-VII-11', '#1') /end

insert into song values (15651, '565. Mandrahona ny andronay', NULL, NULL, '
{1.
Mandrahona ny andronay
Ka raiki-tahotra izahay;
Avia, manampia anay;
Ry Tompo o! Vonjeo!}

{2.
Ny teninao no tokinay,
Ny herinao no tanjakay;
Afaho loza izahay:
Ry Tompo o! Vonjeo!}

{3.
Fa Misy ifaharanay,
Ka toky be no azonay;
Ianao no iankinanay:
Ry Tompo o! Vonjeo!}

{4.
Noho ny nahaterahanao,
Noho ny nahafatesanao,
Sy noho ny fifonanao:
Ry Tompo o! Vonjeo!}
', '#1-VII-11', '#1') /end

insert into song values (15661, '566. Raha mafy ny manjo', NULL, NULL, '
{1.
Raha mafy ny manjo,
Raha be ny hovitray,
Raha malahelo fo,
Mihainoa, Jeso o!}

{2.
Endrikay notafinao,
Nentinao ny entanay,
Nitomany Ianao,
Mihainoa, Jeso o!}

{3.
Mafy ny manjo Anao
Nefa tsy mba meloka;
Helokay no nentinao,
Mihainoa, Jeso o!}

{4.
Voahombo Ianao,
Voatsindron-defona,
Nandry tao am-pasana,
Mihainoa, Jeso o!}

{5.
Raha ory izahay,
Ka ny fitarainanay
Tonga eo an-tsofinao.
Mihainoa, Jeso o!}

{6. Raha tàpitra aminay
Ny fe-taom-boatendrinao,
Aza mba mandao anay,
Mihainoa, Jeso o!}
', '#1-VII-11', '#1') /end

insert into song values (15671, '567. Aza malahelo', NULL, NULL, '
{1.
Aza malahelo,
Aza kivy fo;
Izay tia an''i Kristy,
Loza tsy hanjo,}

{FIV.
Jeso be fiantra!
Mba avia izao,
Ary ny mahantra
Mba tantanonao.}

{2.
Aza mba ahina,
Mpino hianao,
Fa mba miankina
Amin''ny Rainao.
[FIV.]}

{3.
Aza mba misento;
Kristiana anie!
Fa homen''ny Tompo
Fifaliam-be.
[FIV.]}

{4.
Tomponay mahery;
Mba tsinjovinao,
Tano sao ho very
Ny mpanomponao.
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15681, '568. Ry Fanahy ory', NULL, NULL, '
{1.
Ry Fanahy ory,
Azon''ny manjo!
Be ny mampahory,
Aza rera-po;
Feo mamelovelo
Mba henoy izao,
Aza malahelo,
Jeso havanao.}

{2.
Aza mba misento
Na mamoy fo,
Raha mbola eto,
Misy ny manjo;
Nefa Jeso tia
Tsy mandao anao,
Ka mba matokia,
Jeso havanao.}

{3.
Namanao mahantra,
''Ndeha, ilazao,
Jeso be fiantra
No Mpamonjy izao;
Izy Tompo tia
Ka mandray anao,
Dia mifalia,
Jeso havanao.}
', '#1-VII-11', '#1') /end

insert into song values (15691, '569. Indrisy! Ry sakaiza', NULL, NULL, '
{1.
Indrisy! Ry sakaiza
Ory fo!
Holazainao aiza
ny manjo?
Jeso no Mpamonjy
Ho anao;
Izy mahavonjy,
Ambarao
:,: Azy, ry trabonjy!
ny manjo
Ao am-po.:,:}

{2.
Indrisy! Ry mahantra!
Lozanao;
Aiza ny mpiantra?
Indro ao;
Jeso hanazava
Aizim-po.
Izy hampisava
ny manjo;
:,: Soa lalandava
No hatao
Aminao. :,:}

{3.
Harena be an-tany
Tsy anao,
Zava-mora lany
Ka mandao,
Nefa, indro Jeso
Zaranao;
Hira no venteso,
Ka derao
:,: Izay nataon''i Jeso
Ho anao
Hatrizao .:,:}

{4.
Ny lanitra mivoha
Rahateo;
Andrandrao ny loha
Ka Jereo!
Jeso Tompo soa
No ary,
Havanao tokoa
Hatrety;
:,: Dia mazotoa
Hiditra ao
Ianao. :,:
}
', '#1-VII-11', '#1') /end

insert into song values (15701, '570. Mafy ny alon-drano izao', NULL, NULL, '
{1.
"Mafy ny alon-drano izao,
Sady maria no manindao,
Nefa ny Tompo mamba anao;
Moa ho kivy va?"}

{FIV.
"Tsia, Jeso ato aminay,
Izy hampifaly ny fonay;
Tsy mba ho kivy re izahay;
Izy no tokinay."}

{2.
"Maro no efa tola sy lao
Noho ny alahelo izao;
Isan''ireny va hianao?
Moa ho kivy va?"
[FIV.]}

{3.
Amin''izao fiainana izao
Tsy mba ilaozan-java-manjo,
Nefa ao Jesosy, tokin''ny fo;
Moa ho kivy va?"
[FIV.]}

{4.
"Raha ny Tompo aza tety
Nety ho faty tao Kalvary,
Iza no tsy handefitra aty?
Moa ho kivy va?"
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15711, '571. Raha Misy mampahory', NULL, NULL, '
{1.
Raha Misy mampahory,
Indro Jeso Tomponao;
Tsy afoiny tsy akory,
Aza kivy hianao.
Raha rava ny harena,
Ka ho mafy ny manjo,
Mbola Misy famonjena,
Aza mety kivy fo.}

{FIV.
Raha Misy mampahory,
Indro Jeso Tomponao;
Tsy afoiny tsy akory,
Aza kivy hianao.}

{2.
Raha Misy ady mafy.
Indro Kristy herinao;
Raha avy ny mpandrafy,
Raiso koa ny sabatrao.
Raha tamy hanangoly
Ilay fahavalonao,
Ka mandrobo ny devoIV,
Mba aingao ny vavakao.
[FIV.]}

{3.
Na marary mafy aza
Ka mangirifiry ao,
Ary na ho faty aza
Ka handao ny havanao,
Mbola Misy antenaina,
Indro, Jeso avatrao;
Famonjena avotr''aina,
Indro, raiso ho anao.
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15721, '572. Jereo ny fahorianay', NULL, NULL, '
{1.
Jereo ny fahorianay,
Andriamanitra o!
Marary izao ny namanay,
Ka dia mba iantrao.}

{2.
Raha sitrakao, ry Tomponay!
valio ny vavakay,
Fa mora reraka izahay,
Tohana sy ampio.}

{3.
Ny fitondrana tianao
No iankinanay;
Ny didy avy aminao,
Izay no sitrakay.}

{4.
Sitrana ny fanahinay
Ho tonga zanakao,
Tariho làlana izahay
Hankany Aminao.}
', '#1-VII-11', '#1') /end

insert into song values (15731, '573. Mahereza, ry sakaiza', NULL, NULL, '
{1.
Mahereza, ry sakaiza
Vontom-pahoriana!
Matokia, aza kivy,
Kristy mialoha anao;
:,: Matokia.:,:
Mahereza hianao.}

{2.
Aza mba matoky tena,
Foana ny herinao;
Tano mafy ny apinga
Izay atolany anao;
:,: Raiso koa :, :
Ny Fanahy ho sabatrao.}

{3.
Maizina izao ny andro,
Sarotra ny làlanao;
Ao ambony ny mazava
Ampy hanazava anao;
:,: Mba jereo :,:
/lay mazava amboninao.}

{4.
Vetivety, afa-doza,
Vita ny alehanao;
Ao an-danitra ambony
Misy hifalianao ;
:,: Miravoa, :,:
Tapitra ny adinao.}
', '#1-VII-11', '#1') /end

insert into song values (15741, '574. Aza kivy hianao', NULL, NULL, '
{1.
Aza kivy hianao
Azom-pahoriana,
Tany no fonenanao,
Misy fihafiana,
Matokia hianao.
Raha tratry ny manjo,
Jeso Tompo herinao.
Aza mety rera-po.}

{2.
Aza kivy hianao
Naho ny manjo izao,
Raha Jeso aminao,
Afa-tahotra hianao;
Matokia hianao.
Fa Jeso Tompo tia tokoa.
Ka izao rehetra izao
Dia mety mahasoa.}

{3.
Aza kivy hianao
Na dia ory aza ety,
Ny Mpanjaka Tamponao
Mba nihafy koa tety;
Matokia hianao.
Na inona mihatra izao, .
Jeso Ilay Sakaizanao
Tsy mba mahafoy anao.}
', '#1-VII-11', '#1') /end

insert into song values (15751, '575. Ry Jeso o! Sakaiza', NULL, NULL, '
{1.
Ry Jeso o! Sakaiza,
Mpamonjy tia anay!
Raha mbola mianjera
Ny rian-drano be,
Mifofofofo mafy
Ny rivo-doza be,
Vimbino izahay re,
Mba tsy ho faty ao.}

{2.
Arovy izahay re,
Ry Jeso Tomponayl
Ambara-pahalasan''
Ny loza izay manjo ;
Dia ento izahay re
Ho any aminao,
Ka raiso ny Fanahy
Ho faly aminao.}

{3.
Kanefa raha mbola
Hijanona izahay
Aty ambony tany
Ka malahelo fo,
Na misy fahavalo
Miady aminay,
Avia, Jeso Tompo!
Ampio ny olonao.}

{4.
Tsy tambonay isaina
Ny olona aminao;
Hiadana tokoa
Izay horaisinao;
Fa Jeso no Sakaiza:
Anjely maro be
No hiaraka aminay ao
An-dapa rahatrizay.}
', '#1-VII-11', '#1') /end

insert into song values (15761, '576. Ry Sakaizan'' ny mpanota!', NULL, NULL, '
{1.
Ry Sakaizan'' ny mpanota!
Fantatrao ny zanakao,
Fa malemy, mora lavo,
Ka mangataka aminao;
:,: Mba todiho mba todiho
Ka henoy ny vavakay. :,:}

{2.
Raha mitombo harena
Noho ny fanambinanao,
Na matanjaka ny tena
Noho ny fitahianao,
:,: Mba tantano, mba tantano,
Sao tsy mahalala Anao :,:}

{3.
Raha mamely ny devoly,
Fahavalon-janakao,
Ka mandroso manangoly
Hampisaraka aminao,
:,: Mba tazony, mba tazony,
Fa tsy mahatana Anao:,:}

{4.
Raha mangirifiry mafy.
Noho ny tsorakazonao,
Ka ny havana rehetra
Tsy mahita izay hatao,
:,: Mba tsidiho, mba tsidiho,
Fa maniry indrindra Anao:,:}

{5.
Raha tonga ilay fotoana
Hiverenana aminao.
Ka manahinahy foana
Noho ny ratsy /zay natao,
:,: Mba tohano, mba tohano,
Hahatsiaro tsara Anao:, :}
', '#1-VII-11', '#1') /end

insert into song values (15771, '577. Jeso Sakaizanay', NULL, NULL, '
{1.
Jeso Sakaizanay,
Sakaiza rahateo!
Indreto izahay, vonjeo,
Fa be ny adinay}

{FIV.
Jeso Sakaizanay.
Sakaiza rahateo!
Indreto izahay, vonjeo,
Fa be ny adinay}

{2.
Jeso Sakaizanay,
Sakaiza be fitia!
Tariho izahay hanao
Ny zava-tianao:
[FIV.]}

{3.
Jeso Sakaizanay,
Sakaiza tsy mandao
Na dia mpanota aza re
Izahay mpanomponao
[FIV.]}

{4.
Jeso Sakaizanay,
Sakaiza tsy mamoy,
Na ory na finaritra
Izahay ankehitrio
[FIV.]}

{5.
Jeso Sakaizanay,
Omaly sy anio,
Ka mbola ho Sakaizanay
Mandrakizay doria.
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15781, '578. Lohasahan-dranomaso', NULL, NULL, '
{1.
Lohasahan-dranomaso
/zao fiainan-tsika izao;
Ka misento lalandava
/zay rehetra velona ao;
Koa aiza re no misy
Hampionona ny fo?
Iza no mahay mamonjy
Hampisava ny manjo?}

{2.
Manatona Ilay Mpamonjy,
Ry sakaiza ory o!
Ka aza mba misalasala,
Mandrosoa hianao;
Famonjena mahatoky
Tsy miova no anao,
Raha manana an''i Kristy
Ho fanala-tahotrao.}

{3.
Vola sy harena an-tany
Mety tsy hanananao;
Havana, sakaiza koa
Dia hisaraka aminao;
Nefa Jeso no anjara
Tsy alaina aminao;
Izy no Sakaiza tsara
Izay nanavotra aina anao.}
', '#1-VII-11', '#1') /end

insert into song values (15791, '579. Aza mba mandalo ahy', NULL, NULL, '
{1.
Aza mba mandalo ahy,
Ry Mpamonjy o!
Aza mba manary ahy,
Jeso Tompo o!}

{FIV.
Ry Mpanjaka,
Mihainoa re!
Mamonje fa mitaraina
Ny vahoakanao}

{2.
Fantatrao fa malahelo
Ny mpanomponao;
Raha mbola eto aho,
Mba tsinjovinao:
[FIV.]}

{3.
Indro! fa miandry eto
Ny navotanao;
Aoka tsy mba halahelo,
Mba falio izao:
[FIV.]}

{4.
Eny, manomeza hery
Mba ho ahy re,
Ka hahery sy handresy
Ny mpanomponao:
[FIV.]}
', '#1-VII-11', '#1') /end

insert into song values (15801, '580. Ry be fiantra sy be indrafo!', NULL, NULL, '
{1.
Ry be fiantra sy be indrafo!
Ao Aminao misy soa;
Ory izahay noho ny zava-manjo;
Ao Aminao misy soa;
Be siasia an-dàlana aty,
Sao tsy ho tratra ny tsara ary;
Tompo miantrà, vonjeo izahay,
Ao Aminao misy soa.}

{2.
Rahona mainty manarona anay,
Ao Aminao misy soa;
Mba hazavaonao izay dianay,
Ao Aminao Misy soa;
Ampaherezo hanaraka Anao,
Ampaherezo hatoky Anao,
Tompo mahery, hazony izahay,
Ao Aminao misy soa.}

{3.
Any an-danitra izay lapanao,
Ao Aminao misy soa;
Ka dia matoky izahay zanakao,
Ao Aminao misy soa;
Aza ilaozana akory izahay,
Sao dia very an-dàlana indray:
Tompo malala, tariho izahay,
Ao Aminao misy soa.}
', '#1-VII-11', '#1') /end

insert into song values (15811, '581. Aiza re no hahitako', NULL, NULL, '
{1.
Aiza re no hahitako
Izay hampionona ny fo?
Iza re no handrandraiko,
''Ndrao dia tolan''ny manjo?
O! ry ory sy mahantra,
Manatona Ahy izao
Hen ny foko feno antra
Hampiadana anao.}

{2.
Aiza re no hahitako
Izay hanala tahotra?
Fa ny làlana ombako,
Mahatora-kovitra!
Aza kivy, ry mahantra:
Manatona Ahy izao ;
He, ny foko feno antra
Ampy hiafenanao.}

{3.
Aiza re no hahitako
Izay hamela heloka?
''Ndrisy re ity fahotako
Tena mahaketraka!
Ry sakaizako mahantra!
Mba minoa Ahy izao;
He, ny foko feno antra
Hanavotako anao.}

{4.
Ry Jesosy o! Malala!
Fialofako Ianao;
Izaho tsy hisalasala
Hiantoraka aminao.
Eny, manatona Ahy
Feno fitiavana;
Aza manana ahiahy;
Izaho no mpiantoka.}
', '#1-VII-11', '#1') /end

insert into song values (15821, '582. Ry Jeso o! Tahio izahay', NULL, NULL, '
{1.
Ry Jeso o! Tahio izahay
Aty am-pandehananay,
Ny hadalanay avelao,
Ny fonay koa ovay ho vao:}

{FIV.
Arovy re ny olonao,
Ny lalanay mba hazavao.}

{2.
Ny aizim-be alehanay,
Mamely koa ny rafinay,
Tsy fantantray izay hanjo,
Ka be ny tahotra ao am-po:
[FIV.]}

{3.
Mikasa mafy izahay
Ho tsara, nefa fantatray
Fa raha tsy ambininao,
Tsy misy azonay atao:
[FIV.]}

{4.
Mba tano mafy izahay
Ambara-pahafatinay,
Dia ento ao an-danitrao
Izahay ho faly Aminao:
[FIV.]}
', '#1-VII-12', '#1') /end

insert into song values (15831, '583. Mba tarihonao', NULL, NULL, '
{1.
Mba tarihonao,
Tompo o! lzahay,
Fantatray fa mpivahiny
Izahay ankehitriny;
Any Aminao
No honenanay.}

{2.
Mafy ny manjo,
Reraka ao am-po;
Na dia be ny ady aza,
Tsy ho ketraka ny fonay;
Any Aminao
No halehanay.}

{3.
Alahelo be
Ranomaso koa
Azon''ny fakam-panalny,
Saiky resin''ny devoly;
Fa ny sandrinao
Iankinanay.}

{4.
Any Aminao
No honenanay;
Ry Mpitari-dàlan-tsara;
Mba tarihonao izahay re,
Hahatongavanay
Any Aminao.
}
', '#1-VII-12', '#1') /end

insert into song values (15841, '584. Arovy ny fanahinay', NULL, NULL, '
{1.
Arovy ny fanahinay,
Andriamanitra!
Tariho ny fanahinay (in-2)
Hanaraka Anao. (in-3)}

{2.
Ianao no hiankinanay,
Fa tsara Ianao;
Ka mamelà ny helokay (in-2)
Fa diso izahay. (in-3)}

{3.
Izahay hiantso Anao,
Andriamanitra!
Ka henoy ny fivavaka (in-2)
Ataonay Aminao. (in-3)}

{4.
Mitsinjova ny mpanota
Mahantra indrindra,
Fa Jeso, Ilay Mpanayotra (in-2)
No maty nisolo. (in-3)}
', '#1-VII-12', '#1') /end

insert into song values (15851, '585. Mahantra aho, Tompo o!', NULL, NULL, '
{1.
Mahantra aho, Tompo o!
Ka mba tsarovinao,
Maniry ho madio fo
Sy mendrika Anao.}

{2.
Manolo-tena aho izao,
Ry Jeso Tompo o!
Ka aza dia lavinao
Izay atolotro.}

{3.
Izaho voasoratrao
Ao amim-bokinao,
Ho isan''ny mpianatrao,
Hianatra Aminao.}

{4.
Ataovy izay hahaizako
Hanaraka Anao.
Amin''ny fahadiovam-po
Ho voninahitrao.}
', '#1-VII-12', '#1') /end

insert into song values (15861, '586. Jesosy Kristy Tompo o!', NULL, NULL, '
{1.
Jesosy Kristy Tompo o!
Ianao no antenaiko;
Tsy misy Mpamonjy toa Anao,
Ry Heriko sy Aiko,
Fa tsy mba misy olona
Mahafaka ny heloko,
Tsy zakako ny entako,
Ry Jeso o!
Ianao anefa tokiko.}

{2.
Ny otako dia lehibe,
Ny foko dia ory!
Mpamonjy o, vonjeo re!
Ny heloko esory.
Ny Rainao tsara ilazao,
Fa izaho no navotanao,
Ka mba havela heloka,
Dia azoko
Ny fiadanana ao am-po.}

{3.
Finoa-marina ao am-po
No aoka mba homenao,
Mba hanandramako tokoa
Ny tsirom-pamonjenao,
Mba hitiavako Anao
Sy ireo rahalahinao;
Raha avy ny fotoako,
O, mba vonjeo!
Ka mba reseo ny rafiko.}

{4.
Ny saotra, dera, ho Anao,
Ry Raiko be fiantra,
Sy ho Anao, ry Kristy o,
Mpamonjy ny mahantra,
Anao, Fanahy Masina,
Izay tena Mpampionona!
Izaho mba omeonao
Ny aim-baovao,
Haharitra ho olonao!}
', '#1-VII-12', '#1') /end

insert into song values (15871, '587. Jehovah Tompo no itokiako', NULL, NULL, '
{1.
Jehovah Tompo no itokiako,
Fa mampitombo ny fifaliako ;
Ny fo sy saiko miankina
Aminy,
Izay ilaiko omen''ny teniny,
Ka tena aiko ny toroheviny.}

{2.
Ampio aho, Jehovah Tompo o!
Fa mitalaho izao ny zanakao,
Omeo hery hitanako ny to,
Mba tsy ho very ny rakitra ao
am-po;
Ianao irery no iankinako.}

{3.
Ry Kristy Tompo, mba mamonje
anay,
Mba ho mpanompo mazoto
izahay,
Mpiasa tsara ankasitrahanao,
Izay manambara ny fitiavanao,
Ka hiafara eo anatrehanao.}
', '#1-VII-12', '#1') /end

insert into song values (15881, '588. Tompo Mpamindra fo!', NULL, NULL, '
{1.
Tompo Mpamindra fo!
Mahery sy Tsitoha
''Zay nidina taty
Hamonjy ny mania;
Mivavaka Aminao
sy mitalaho izahay
Ny gadranay tapaho
hanafaka anay!}

{2.
Tompo o! manjakà
Amin''ny olonao;
Faingàna, iantrao
Ireo izay narovanao;
Miondreha, mijere,
Izahay miandrandra Anao;
Ny herinao anie
Hatafinao ireo.}

{3.
Tompo Mpanova fo ,
Mpampionona toko a.
Midina ka ampio
Izahay hahery izao.
Toky mamy loatra
No omena anay:
Hatranyankoatra,
Arovinao izahay.
}
', '#1-VII-12', '#1') /end

insert into song values (15891, '589. Ry Jesosy Tompoko', NULL, NULL, '
{1.
Ry Jesosy Tompoko,
Izay nanetry tena o.
Naka ny tarehinay
Hanavotanao anay;
Mba omeo anay lzao
Ny Fanahy Masinao,
Hanarahanay Anao.}

{2.
ZanakiDavida o!
Mba midira ao am-po,
Raiso ka diovinao
Izy ho tempolinao;
Izahay manasa Anao
Honina ao am-po izao,
Koa mba ekeonao!}
', '#1-VII-12', '#1') /end

insert into song values (15901, '590. Kristy Tsitoha, Tomponay mahery', NULL, NULL, '
{1.
Kristy Tsitoha, Tomponay
mahery,
Mba mitodiha re ny olom-bery,
Ka mamonje anay izay mania;
O, mba avia!}

{2.
He, Ianao kiadin''ny mivalo,
Ka mba reseonao ny fahavalo ;
Ny ta-hanimba ny Anao sakano
Izahay tantano!}

{3.
Ny fiangonanao ampiadano,
Ny tanindrazanay re mba
tohano,
Ny tena sy fanahinay tahio,
Ampifalio!}

{4.
Ny asanao, ry Zana-Janahary,
Dia mankalaza Anao miharihary,
Ka izahay midera Anao
Mpamonjy
Fa voavonjy!}
', '#1-VII-12', '#1') /end

insert into song values (15911, '591. Ry Jesosy, Izay Mpiahy', NULL, NULL, '
{1.
Ry Jesosy, Izay Mpiahy,
Mba henoy izao; .
Raha misy mamely ahy,
Mba reseonao!}

{FIV.
Ry Jesosy, mba henoy izao;
Raha misy mahory ahy,
Mba tsarovinao!}

{2.
Raha avy ny devoly
Ka mandrahona,
Raha ny nofo manangoJV
Sy manintona,
[FIV.]}

{3.
Be ny fahavalo fetsy
Mampahory izao,
Izaho dia mora resy,
Ka vonjeonao!
[FIV.]}

{4.
Raha misalasala aho,
Mba toroy ny to!
Ary raha mitalaho,
Amindraonao fo!
[FIV.]}

{5.
Miangavy mafy aho,
Ry Mpamonjy o!
Mba vonoy ny fahotako
Amin''ny ranao.
[FIV.]}

{6.
Raha tapitra ny andro
Ivelomako,
Hazavao, ry Masoandro,
Ao am-pasako,
[FIV.]}
', '#1-VII-12', '#1') /end

insert into song values (15921, '592. Be ny mampahory', NULL, NULL, '
{1.
Be ny mampahory
Aty, Jesosy o!
Kely, osa, ory,
Ny mpanomponao.
Ry Mpiandry lehibe,
Anao ny tenako;
Ianao irery re,
No iankinako.
Tompo o, ny foko
Manantena Anao.}

{2.
Miarova ahy,
Jesosy Tompo o!
Tena sy Fanahy
Hasoavinao,
Ry Mpamonjy lehibe,
Anao ny tenako;
Ianao irery re
No vatolampiko.
Tompo o, ny foko
Manantena Anao.}
', '#1-VII-12', '#1') /end

insert into song values (15931, '593. Ry Jeso o, Mpanjakanay', NULL, NULL, '
{1.
Ry Jeso o, Mpanjakanay,
Hadinonao va izahay.
No tsy omenao faingana
Ny rano mahavelona?}

{2.
He, maina sy mangetana
Sy ivesarana entana,
Ka mapiferin''aina anay
Ny efitra onenanay.}

{3.
Tsy hitanao ve, Tompo o,
Fa valaka sy rera-po,
Malain-kanaraka Anao
Ny olom-boafidinao?}

{4.
Aidino ny Fanahinao
Sy fifaliam-po vaovao;
Ny ranon''aina anie
Hatondrakao ny tany re!}

{5.
Reseo ny fahavalonay,
Omeo rariny izahay,
Tapahonao ny gadranay,
Ampiadano izahay!}
', '#1-VII-12', '#1') /end

insert into song values (15941, '594. Ry Tompoko malala', NULL, NULL, '
{1.
Ry Tompoko malala.
Mpiandry tsara o!
Ianao no mahalala
Ny hasasarako.
Ny ota an-tapitrisa
Izao mandreraka!
Ka manomeza ahy
Fiadanana!}

{2.
Ry Tompoko malala,
He, zatra Ianao
Mampody ny adala
Niala taminao;
Ampiadano aho,
Tariho lalana,
Ka mahareta, Tompo
Aza sasatra!}

{3.
Ry Tompoko malala,
Te-hody aho izao.
Fa manina ny vala
Ny ondrikelinao;
Ka mitalaho aho:
Vohay, ry Jeso o,
Izay mba efitrano
Hitoerako!}
', '#1-VII-12', '#1') /end

insert into song values (15951, '595. Ry Raiko o! Mba raiso ny tanako', NULL, NULL, '
{1.
Ry Raiko o! Mba raiso ny tanako,
Tantano mba handroso ny diako,
Hazony tsy hania ny zanakao.
Raiko o, avia, tarihonao.}

{2.
Ny foko ta-hanoa ny didinao,
Kanefa mora voan''ny ratsy izao,
Ka dia mba fenoy ny herinao,
Ry Raiko o, henoy ny zanakao.}

{3. Rehefa tsy mahita ny làlana
Ka toa tsy ho tafita ny lalina,
Fa ketraka ny aiko, dia raisonao,
Trotroy aho, Raiko, an-tratranao.
}
', '#1-VII-12', '#1') /end

insert into song values (15961, '596. Indreto izahay mitomany', NULL, NULL, '
{1.
Indreto izahay mitomany,
Fa be alahelo ny fo;
Ny ota no fototr''izany,
Ny heloka sy ny manjo;
Fa reraka, re, ny Fanahy,
Mahantra sy be ahiahy,
Ka tena variana ery.}

{2.
Ry Tompo, Izay mofon''ny aina,
Ry rano mamelona anay,
Tsinjovy izay mitaraina,
Mahantra izao izahay!
Onony ny fonay fa voa,
Omeo izay mahasoa,
Monena aminay Ianao.}

{3.
Ny fonay dia be hetaheta,
Maniry indrindra Anao;
Ry Tompo, mba mampahareta
Anay ho mpanaraka Anao;
Ny reraka ataovy mahery,
Vonjeo izahay olom-bery;
Matoky Anao izahay!}
', '#1-VII-12', '#1') /end

insert into song values (15971, '597. O, ry Jesosy Mpamonjy mahery!', NULL, NULL, '
{1.
O, ry Jesosy Mpamonjy mahery!
Izaho maniry indrindra Anao,
Fa reraka aho ka mila ho very;
Ny foko saronan''ny ratsy izao.}

{FIV.
Ry Tompo, tsilovy,
Ny lalan''ny saiko,
Ka mba arovy
Ny fo sy ny aiko!}

{2.
Efa hanafotra ahy ny rano,
Faingàna, Jesosy, vonjeonao re!
Ho difotra aho, ry Tompo, ka
tano,
Diovy, afaho ny foko anie!
[FIV.]}

{3.
Efa akaikin''ny lalina aho,
Ka mila ho voa, hilentika ao;
Ry Tompo malala, ny foko tanjaho,
Ka mba hazavao hahalala Anao:
[FIV.]}

{4.
Aiza izao Ianao, ry Mpamonjy,
Ry Tokin''ny ory sy reraka o,
No tsy mampiseho ny herin''ny
vonjy
Hanafaka ahy izay ory am-po?
[FIV.]}

{5.
O, ry Jesosy, Izay naharara
Ny onja namely ny sambo tary
AnivoniGenesareta ho tsara,
Toneo ny on jan'' ny foko aty!
[FIV.]}
', '#1-VII-12', '#1') /end

insert into song values (15981, '598. Omeo mba handramako', NULL, NULL, '
{1.
Omeo mba handramako
Ny fitiavanao,
Hahaizako, ry Jeso o!
:,: Hihavana aminao,:,:}

{2.
Iriko re ny tavanao,
Ry Ilay malalako,
Fa tiako ny hahita Anao,
:,: Izay Sakaizako:,:}

{3.
Hanjavona ny masoko,
Hamoy ny namako,
Fa Ianao, Jesosy o,
:,: Ho eo anilako. :,:}

{4.
Raha ao ny ali-maizaim-be,
Izay handehanako,
Ho eo an-tananao anie
:,: No hahafatesako.:,:}

{5.
Ka tsy mba hatahorako
Ny aizim-pasana,
Hatsanganao ny tenako
:,: Ho te na sitrana.: ,:}
', '#1-VII-12', '#1') /end

insert into song values (15991, '599. O, ry Jesosy mifaly ny foko', NULL, NULL, '
{1.
O, ry Jesosy mifaly ny foko,
Raha tsy lavitra Anao,
Ka mba mangataka indrindra re aho
Ary mifona aminao;
Tano ny zanakao mba tsy ho very,
Tano akaiky anao!
Zaza adala ka mety hiery,
Raha dia lavitra Anao.}

{2.
Aza manary izay mitalaho,
Fa mba vonjeo anie;
Tena mahantra sy ory re aho,
Diso sy meloka be;
Nefa maniry hiova ho tsara,
Ka ta-hanoa Anao,
Ary ny zavatra izay voarara,
Tsy mba hataoko izao.}

{3.
Raiso, ry Tompoko, ny fikasako,
Raiso ny foko ania!
Raha ny marina no ialako,
Dia ahitsio sy ampio;
Fa Ianao, ry Mpamonjy mahery,
No tsy mba mety mandao,
Ary Izaho dia tsy mba irery,
Fa tsy ho lavitra Anao.}
', '#1-VII-12', '#1') /end

insert into song values (16001, '600. Ry Raiko o!', NULL, NULL, '
{1.
:,: Ry Raiko o! :,:
Izaho aty mpivahiny
Manina ny tranonao,
Ka mba tariho
Ny zanakao.}

{2.
:,: Ry Raiko o! :,:
Indro, Satana milolo
Mba hanangoly anay,
Ka mba vonjeo
Ny Zanakao.}

{3.
:,: Ry Raiko o! :,:
Maizina sady malama,
Sarotra ny làlanay,
Ka mba tantano
Ny zanakao.}

{4.
:,: Ry Raiko o! :, :
Maro ny zava-mihatra,
Izay mampanahy anay,
Ka mba tohano
Ny zanakao.}

{5.
:,: Ry Raiko o! :,:
Aoka anie ny Fanahy
No mba hitarika anay
Ho tonga any
An-tranonao.}
', '#1-VII-13', '#1') /end

insert into song values (16011, '601. Misaotra Anao, Jesosy o!', NULL, NULL, '
{1.
Misaotra Anao, Jesosy o!
Ny olom-boavonjy,
Fa be dia be ny indrafo
Nataonao, ry Mpamonjy.
Fa tonga ory Ianao,
Ny ainao no natolotrao
Hisolo ny mpanota.}

{2.
Ampibebaho izahay
Ho olonao manoa;
Ny ota iverasanay
Esory avokoa.
Ny filaliana vaovao
No aoka mba hasolonao
Ny alahelom-ponay.}

{3.
Ampio izahay izao
Hinoanay ny teny
Izay nolazain''ny vavanao,
Fa tanjakay ireny:
Izao no angatahinay,
Mba hatanjaho izahay,
Hanaraka izany.}
', '#1-VII-13', '#1') /end

insert into song values (16021, '602. Ry Andriamanitry ny famonjena', NULL, NULL, '
{1.
Ry Andriamanitry ny famonjena
Sy kintana antenain''ny firenena,
Henoy ny fangataham-pandresena,
Ry Ray Tsitoha!}

{2.
Ilay fitopatopan''ny alondrano
Sy reharehan-drafinay foano!
Ny olonao mahantra mba tantano,
Ry Ray Mpiaro!}

{3.
Raha osa, Ianao mahay mitàna,
Raha reraka asiam-pahotàna,
Ny herinao tsy resiniSatana,
Ry Ray Mahery!}

{4.
Ampionao izahay handresy raly,
Ampionao mba haharitra ny mafy,
Ampionao tsy ho ketraka mihafy,
Ry Ray Mpanampy!}
', '#1-VII-13', '#1') /end

insert into song values (16031, '603. He, sambatra Jesosy o', NULL, NULL, '
{1.
He, sambatra Jesosy o,
Ny ondry vitsinao!
Fa Ianao Mpiandry soa
Maharo ny Anao.
Ny ainao no natolotrao
Ho an''ireo ondrinao,
Ny very tadiavinao
Hiaraka Aminao.}

{2.
Izaho voamarika
Ho isan''ny Anao,
Anefa mba matahotra
Sao dia lavinao,
Ka aoka mba hotairinao
Ny foko, raha hitanao
Fa mila ho sondriana
Sy diso làlana.}

{3.
Ny hafanana mahamay
No mahatrotraka,
Anefa mbola tazanay
Ny fitsaharana.
Ho avy faingana anie
Ny andro soa lehibe,
Izay hamelaranao ny lay
Handrakotra anay.}
', '#1-VII-13', '#1') /end

insert into song values (16041, '604. Ento aho, ry Jehovah', NULL, NULL, '
{1.
Ento aho, ry Jehovah,
Mpivahiny raha ety;
Tano aho, fa malemy,
Raha tsy hazoninao,
:,: Mofon''aina.: ,:
Ho fihinako anie.}

{2.
Mba savao ny loharano
Hahazoako ny soa;
Aoka hitari-dàlana Ahy
Izay andri-rahonao,
:,: Ry Tsitoha :,:
Fiarovana Ianao.}

{3.
Raha hita aniJordana,
Ampandriony tahotro;
Ry Mpandresy fanimbana,
Ento any Kanana aho!
:,: Fiderana :,:
No hatolotro Anao.}
', '#1-VII-13', '#1') /end

insert into song values (16051, '605. Jeso Hery mahavonjy', NULL, NULL, '
{1.
Jeso Hery mahavonjy
Tena Fialofana,
Fa miantra sy mamonjy
Zanak''olombelona
:,: Hery soa, hery soa,
Itokianay tokoa. :,:}

{2.
Raha misy ny tafiotra
Izay mamely mafy anay,
Ny fitiany tsy ho ritra
Tokinay mandrakizay.
:,: Tsy mandao, tsy mandao,
Iankinana hatrizay: ,:}

{3.
Raha mambomba sy miaro,
Mampitony tahotra,
Jeso Tompo tia, Mpiaro;
Resy re, ny hovitra.
:,: Fa matoky, fa matoky,
/lay ampinga lehibe:,:}

{4.
Tianay tokoa tokoa
Ho an-tratranao doria,
Mba hatoky ka ho soa,
Ry Mpamonjy be fitia.
:,: Tompo soa, Tompo soa.
O! hazoninay tokoa:,:}
', '#1-VII-13', '#1') /end

insert into song values (16061, '606. O, ry Raiko Izay Mpandahatra!', NULL, NULL, '
{1.
O, ry Raiko Izay Mpandahatra!
Vato fantsika mafonja!
Aminao no mifahatra
Ito ny foko safotra onja.}

{FIV.
Ry Tsitoha o ! Mijere !
Ry Mpamonjy o, ma mon je!}

{2.
He, manonja mafy loatra
Itony fiainako voafofo
Toa dia diso tafahoatra
Ireo ankaso izay milofo,
[FIV.]}

{3.Nefa kosa tsy mamoy!
Izany foko manantena!
Fa ny masonao manjohy,
Iandrandrako fa mon je na.
[FIV.]}
', '#1-VII-13', '#1') /end

insert into song values (16071, '607. Jeso no Mpiambina antsika', NULL, NULL, '
{1.
Jeso no Mpiambina antsika
Sy Mpiandry tsara,
Ka natolony ny ainy
Hisolo ondry.}

{2.
Matokia hianareo,
Ry ondry vitsy
Fa ny Raiko no mikasa
Hitandrina anareo.}

{3.
Izaho no Mpitarika
Sy Mpitondra tsara
Izay manaraka Ahy
Handresy ny ratsy.}

{4.
Ary amin''ny farany
Halaiko ny Ahy
Tsy hisaraka amiko
Ela mandrakizay.}
', '#1-VII-13', '#1') /end

insert into song values (16081, '608. He, miandrandra Anao', NULL, NULL, '
{1.
He, miandrandra Anao,
Jehovah Tompo o!
Ny olonao;
Henoy ny vavakay
Vonoy ny helokay
Hahafantaranay
Fa zanakao.}

{2.
Raha hodiavinay
Ny tany maizina,
Tarihonao;
Fafao, Ry Jesosy
Ny ranomasinay,
Ka mba esorinao
Ny tahotray.}

{3.
Mba manomeza anay
Ny hasoavanao,
Ry Tompo o!
Mba hahazoanay
Ny hasambarana
Tsy mety levona,
an-danitra.}

{4.
Tohanonao izahay,
Rahefa tapitra
Ny andronay.
Ray o! Mba hamoray
Ny hanalana anay
Ho any aminao,
mandrakizay.}
', '#1-VII-13', '#1') /end

insert into song values (16091, '609. Ianao Jehovah Tompo!', NULL, NULL, '
{1.
Ianao Jehovah Tompo!
No hatoninay izao;
Avy izahay hanompo
Sy hanao ny sitrakao,
:,: Indro raiso,
Raiso ho Anao izahay.:,:}

{2.
Mba tantano sy arovy,
Fa malemy izahay;
He! Marary sy farofy,
Ianao no herinay;
:,: Ody aina
Hitsaboanao anay.:,:}

{3.
Mpivahiny ka mahantra,
Entonao, Fanahy o!
Ana sy mangetaheta,
Asehoy ny antranao;
:,:Ranon''aina
Hosotroinay izao.:,:}

{4.
Tranonao madio no any,
Vorodamba no anay;
Fa hilaozanay ny tany,
Ka soloy ny lambanay;
:,: Lamba fotsy
Hitafianay anie.:,:}
', '#1-VII-13', '#1') /end

insert into song values (16101, '610. Ny foko, ry Jehovah o!', NULL, NULL, '
{1.
Ny foko, ry Jehovah o!
Fidio ho lapanao;
Ekeo ho fonenanao
Ka ao no anjakao.}

{2.
Ny foko, ry Jesosy o!
Sasao ho voadio!
Izay eritreri-dratsy ao
Levoninao anio.}

{3.
Ny foko, ry Fanahy o!
Mba hamasinonao!
Avia, Fanahy Masina o!
Ovay ho fo vaovao.}

{4.
Ray, Zanaka, Fanahy o!
Mpamonjy mahasoa.
Mpanafaka ahy Ianao
Isaorako tokoa.}
', '#1-VII-13', '#1') /end

insert into song values (16111, '611. Jehovah Ray Malala o!', NULL, NULL, '
{1.
Jehovah Ray Malala o!
Izay ao an-danitrao
Henoy ny fivavahako
Ataoko aminao
Fa Jeso Kristy Zanakao
No tena sorona
Izay entiko ho aminao
Mpamela heloka.}

{2.
Ny lalako mba hazavao
Ianao no jiroko,
Fa maizim-pito e izao
Sy be ny kotroka!
Na misy loza aza
Izay mampamoy fo,
Tsy misy hatahorako
Ianao no amiko.}

{3.
Ny fikapohana, izay
Ataonao amiko
Dia misy fananarana
Mba hibebahako!
Ny fahafatesara, izay
Ho tonga koa izao
Dia Ialana halehako
Ho any aminao.}

{4.
Mba raisonao ny tanako,
Tantano aho izao
Fa zazabodo, Raiko o!
Ampio ny zanakao;
Na tondraka ny rano re
Ka safononoka ao
Ny faty tsy ho tahotro
Hodiako aminao.}
', '#1-VII-13', '#1') /end

insert into song values (16121, '612. Ry Ray Mpiaro lehibe', NULL, NULL, '
{1.
Ry Ray Mpiaro lehibe
Mba mihainoa ahy,
Ka ampio aho hanao hoe:
Hatao anie ny sitrakao!}

{2.
Izay rehetra sitrakao
No aoka mba hatao izao
Fa mahasoa ny tianao:
Hatao anie ny sitrakao!}

{3.
Na sarotra aza ny hatao,
Mba manampia ny zanakao
Hanaraka ny teninao:
Hatao anie ny sitrakao!}

{4.
Na tonga aza ny manjo,
Ka ory sy mivadi-po,
Ho toy izao no teniko:
Hatao anie ny sitrakao!}

{5.
Mba tanterahonao tokoa
Izay kasainao hahasoa;
Ny tena sy fanahy koa;
Hatao anie ny sitrakao!}

{6.
He, Ianao dia tsy mandao
Izay voaray ho olonao
Ka mankatò ny didinao;
Hatao anie ny sitrakao!}
', '#1-VII-13', '#1') /end

insert into song values (16131, '613. Mpiandry lehibe!', NULL, NULL, '
{1.
Mpiandry lehibe!
Vorio ny ondrinao
Hiray fisoko avokoa.
Hiadana aminao.}

{2.
Sakaiza herinay!
Jereo ny ondrinao
Mitebiteby, ory fo,
Ka miandrandra Anao.}

{3.
Manahy izahay,
He! Ala be izao;
Andrao ho very foana aty,
Vonjeo ny ondrinao.}

{4.
Henoy, ry Tokinay!
Vohay ny tranonao;
Raha avy ny hodianay
Hitoetra aminao.}
', '#1-VII-13', '#1') /end

insert into song values (16141, '614. O! Ray Jereo ny zanakao!', NULL, NULL, '
{1.
O! Ray Jereo ny zanakao,
Mivavaka aminao;
Henoy ny fitarainanay
Izay atao izao.}

{2.
O Ray! Vonjeo ny zanakao,
Fa osa izahay;
Ny tananao no herinay,
Handresy izahay.}

{3.
O Ray! Tahio ny zanakao,
Ny soa aminao,
Ka arotsahy aminay
Ny fitahianao.}

{4.
O Ray! Misaotra Anao izahay
Ka dera no atao;
Ny soa azonay aty
Isaoranay Anao.}

{5.
O Ray! Vahiny izahay,
Tsy taninay ity;
Ka raha hody raisonao
Ho aminao ary.}
', '#1-VII-13', '#1') /end

insert into song values (16151, '615. Ry Jehovah Ray Mahery!', NULL, NULL, '
{1.
Ry Jehovah Ray Mahery!
Indreto ny mpanomponao;
Fa mpanota saiky very
Nefa tsy mba foinao;
Fa ny masonao mijery
Ny fanahinay mania;
Ny fitaomanao mahery
No miantso hoe : "Avia".}

{2.
Ry Jehovah, Ray Mpitahy
Izay rehetra ory fo!
Ny tsy manana mpiahy
Mora resin''ny manjo;
Zovy ary no hanahy
Raha miankina Aminao?
Eny, faly ny Fanahy
Izay mitoky aminao.}

{3.
Ry Jehovah, Ray Mpiaro!
He ny fahavalonay
Izay masiaka sy maro;
Hodidinanao izahay,
Fa ety dia mifangaro
Amin-dratsy izahay,
Ka tsy misy izay maharo
Raha ilaozanao, ry Ray!}

{4.
Ry jehovah o ! Vimbino
/tony zanakao ety,
Aza avela hanadino
Ny fonenan-tsoa ary,
Ka omeo Fanahy mino
Tsy mivadika aminao;
Ary koa, mba sakambino
Tsy hisaraka aminao.}
', '#1-VII-13', '#1') /end

insert into song values (16161, '616. Rainay be fitia!', NULL, NULL, '
{1.
Rainay be fitia!
Avy izahay
Hila zavatsoa
Ato aminao;
Mihainoa anay.}

{2.
Rainay tsy miova!
Mijere anay,
Zanaka nikomy,
Fa naverinao;
Maneke anay!}

{3.
Rainay be fiantra!
Menatra izahay,
Dia mba mifona
Eo imasonao;
Miantrà anay!}

{4.
Rainay be harena,
Mpanome ny soa!
Izahay mahantra,
Mitaraina hoe:
Manomeza anay!}

{5.
Rainay be fitia!
Indreto izahay
Avy mba hitondra
Saotra ho Anao:
Aza lavinao!}
', '#1-VII-13', '#1') /end

insert into song values (16171, '617. Aza manadino ahy', NULL, NULL, '
{1.
Aza manadino ahy,
Ry Mpihaino vavaka !
Aza mba manary ahy
Hanalavitra Anao;
Mitodiha ry Mpanjaka!
Ka jereo ny olonao;
Aza manadino ahy,
Ry Mpihaino vavaka!}

{2.
Ianao no fiereko,
Vatolampy, heriko !
Na kamboty aza aho,
Faly manana Anao;
Afa-tahotra ny foko,
Raha miandrandra Anao,
Aza manadino ahy,
Ry Mpihaino vavaka!}

{3.
Eny, Ianao no Raiko,
Ka hatoniko izao ;
Nefa ny fahadalako
Aza mba heverinao;
O! ampifalio aho,
Ka ombay ny elatrao;
Aza manadino ahy,
Ry Mpihaino vavaka!}

{4.
Tabataba lalandava
No mba htaiko izao,
Ka izaho te-ho tonga
Ao am-pitsaharanao,
Mitenena, ry Jehovah!
Mamalia ahy izao;
Aza manadino ahy,
Ry Mpihaino vavaka!}
', '#1-VII-13', '#1') /end

insert into song values (16181, '618. Eny, Raiko! Mba tariho', NULL, NULL, '
{1.
Eny, Raiko! Mba tariho
Ny fanahin-janakao.
Ka ny saiko atodiho
Hankatò ny teninao.
Zaza be fisiasia,
Tano re ny zanakao,
Dia taomy mba ho tia
Sy hanao ny sitrakao.}

{2.
Eny, Raiko! Mba tsilovy
Ny alehan-janakao,
Ka tantano sy arovy
Mba hankany aminao;
Mora reraka ny aina
Raha tsy fahananao ,
Ka tohano tsy halaina
Mba hanao ny sitrakao.}

{3.
Eny, Raiko! Raha mafy
Izany tsorakazonao,
Ka ny aiko tsy mahohafy
Mba vonjeo ny zanakao;
Ka ny foko ''zay adala
Mba ovay sy hazavao,
Dia taomy hahalala
Mba hanao ny sitrakao.}

{4.
Eny, Raiko! Mba tohano
Raha mihatra ny manjo,
Ka vimbino sy tantano
Tsy ho kivy re ny fo:
Raha tapitra ny aiko,
Raiso hodyaminao ;
Eny, ento aho, Raiko!
Fa izay no sitrakao.}
', '#1-VII-13', '#1') /end

insert into song values (16191, '619. Ry Raiko o! Tariho ny zanakao', NULL, NULL, '
{1.
Ry Raiko o! Tariho ny zanakao,
Tariho re!
Fa maizina ny lalako izao,
Tariho re!
Tsilovy ka omeo inoana,
Tantano ao am-pandehanana.}

{2.
/zay ela izay ny lalan-tsitrapo
No tiako;
/zay ela izay izao tontolo izao
No mamiko;
Kanefa izao, tariho ny zanakao ,
Ry Raiko o! Handia ny làlanao.}

{3.
Tsy mba ahoako ny loza aty
An-dalana;
Tsy te-hitsinjo koa ny ary
An-koatra;
Fa isan''andro alaharonao,
Ry Raiko o! ny dian'' ny zanakao.}

{4.
Na mbola lavitra aza re izay
Alehako,
Tarihonao ambara-pahafaka
Ny alina,
Dia vao hitsahatra ny zanakao,
Ry Raiko o ! Avy an-danitrao.}
', '#1-VII-13', '#1') /end

insert into song values (16201, '620. Zanahary, Ray mahery', NULL, NULL, '
{1.
Zanahary, Ray mahery,
Izahay tsy manan-kery,
Nefa he, ny fahavalo
Fa masiaka sady maro ;
O! ampio re izahay,
Fa ny zavatra mihatra
Tsy mba misy maharatra
Ny arovanao, ry Ray,
Zanahary Ray mahery,
Mba arovy izahay!}

{2.
Zanahary, Ray miantra!
Olon-diso sy mahantra
Izahay izay mitaraina
Ianao no antenaina:
Iantrao re izahay!
Be tokoa ny fahotana
Mahatonga faimbana
Efa vitanay taty:
Zanahary, Ray miantra,
Mamelà ny helokay!}

{3.
Zanahary, Ray malala!
Enga anie ka hahalala
Mba ho tia sy hanaiky,
Ary koa mba hanakaiky
Ny Mpamonjy izahay;
Tsy ny tahotray ny loza
No nanaitra anay hiova
Fa ny ran''ny Zanakao:
Zanahary, Ray malala,
Mba hazony izahay!}
', '#1-VII-13', '#1') /end

insert into song values (16211, '621. Ry Jeso be fitia!', NULL, NULL, '
{1.
Ry Jeso be fitia!
Tariho izahay
Mba tsy haniasia
Fa hiaraka aminao.
Indrisy! Ela loatra
No nijanonanay,
Nanaram-po nihoatra
Ny sitrapon''ny Ray.}

{2.
Ry Tompo! Miarova
Ny fiananay ety,
Izay fohy, mora miova
Fa tsy toy ny ary.
Izay ianteharana eto
Mandalo avokoa:
Ny fo re no mba ento
Hikambana aminao.}

{3.
Tariho hahalala
Ny tianao hatao
Ka tsy hisalasala
Handà izay halanao;
Mandatsaka ao an-tsaina
Ny soa vitanao
Ka dia hikely aina
Hanao ny sitrakao.}
', '#1-VII-13', '#1') /end

insert into song values (16221, '622. Mba tantano Raiko o!', NULL, NULL, '
{1.
Mba tantano Raiko ô!
Ny fanahin-janakao,
Aoka re ny sandrinao,
No hiaro ahy izao;}

{FIV.
Indro! Raiso
Tano aho Tompo o!
Mba hanao ny sitrakao
Sy hitoetra ao aminao.}

{2.
Ray malala Ianao,
Sady tia ny zanakao:
Aoka re ny tavanao
Hanazava ahy izao!
[FIV.]}

{3.
Na dia maizina aza aty,
Dia matoky aho izao
Fa Ianao, dia tsy mandao,
Izay miaraka aminao:
[FIV.]}
', '#1-VII-13', '#1') /end

insert into song values (16231, '623. Mba omeo ahy', NULL, NULL, '
{1.
Mba omeo ahy
Fo madio kokoa
Afaka ahiahy
Ho Anao tokoa.
Izaho dia malaina
O! mba hafanao re
Mba hikely aina
Hahefako be.}

{2.
Mbola manangoly
Ny fakampanahy;
Ao koa ny devoly
Ta-hamotraka ahy;
Tompo o! Ampio
Ny mpanomponao izao,
Ary mba tafio
Herim-po vaovao.}

{3.
Atolory ahy
Zotom-po sy hery,
Fahendrem-panahy,
Fo mahay mietry.
Ampianaro aho
Mba hanolo-tena,
Ka mba hatanjaho
Ny finoako.}

{4.
Dia hamasino
Ny saiko, ry Tompo!
Ka omeo fo mino,
Fo mahay manompo,
Fo mahay mihafy,
Fo miravoravo ,
Na dia voa mafy,
Fo toy ny Anao.}
', '#1-VII-13', '#1') /end

insert into song values (16241, '624. Jeso o, Mpiandry tsara!', NULL, NULL, '
{1.
Jeso o, Mpiandry tsara!
Sambatra ny ondrinao;
Ianao no mialoha
Hanarahanay Anao;
:,: Mitariha, mitariha
Ny fanahinay izao :,:}

{2.
Jeso o, Mpiandry tsara!
Fohy ny fisainanay;
Ianao mahay manoro
Lalana halehanay;
:,: Mitariha, mitariha
Ny fanahinay izao:,:}

{3.
Jeso o, Mpiandry tsara!
Tsinjonao ny dianay;
Ondry very ka ho faty,
Raha tsy hatoninao;
:,: Mitariha, mitariha
Ny fanahinay izao:,:}

{4.
Jeso o, Mpiandry tsara!
Izao alehanay izao
Ala be sy làla-mafy,
Maty re ny ondrinao!
:, : Miarova, miarova
Ny fanahinay izao:,:}

{5.
Jeso o, Mpiandry tsara!
Mafy re ny tahotray;
Bibidia mamiravira
Mampivadi-po anay;
:,: Miarova, miarova
Ny fanahinay izao:,:}

{6.
Jeso o, Mpiandry tsara!
Ento mody izahay;
Aminao ny valan''ondry
Tsy mba misy tahotra;
:,: Mitariha, mitariha
Ny fanahinay ho ao:, :}
', '#1-VII-13', '#1') /end

insert into song values (16251, '625. Mpamonjy soa malala o!', NULL, NULL, '
{1.
Mpamonjy soa malala o!
Mba miandrandra Anao,
Ny foko sy fanahiko
Ankehitriny zao;
Fa Ianao irery
No tena Mpanome
Ny fahafinaretana
Sy fitsaharam-be,}

{2.
Mpiandry ondry mora o!
Ry Jeso be fitia!
Hazony aho ondrinao
Mba tsy haniasia,
Andraso isan''andro,
Arovinao anie,
Endrey! Fa mahatahatra
Ny biby marobe.}

{3.
Ry Kristy be fitia!
Ry Tompo tsy mandao!
Ny sandrinao iondanako
Fa mamy Ianao;
Ianao nanolo-tena
Ho faty fahizay,
Hanangana ala-meloka
Ho zanakao indray.}

{4.
Ry Tompo Zanahary
Be faharetarn-po!
Izaho te-ho olonao
Mahitsy sy madio;
Ovay ny toe-tsaiko,
Ny foko mba sasao,
Mba ho tempoly mendrika
Ny hitoeranao.}

{5.
Ry Jeso tsy miova!
Ry Vonivatoka!
Ry Zanak''Andriamanitra o!
Ry Aro lehibe!
Izaho dia matoky
Ny fitiavanao;
Fa sambatra ny olonao
Miankina aminao.}
', '#1-VII-13', '#1') /end

insert into song values (16261, '626. Jeso Tompo, mamindrà fo', NULL, NULL, '
{1.
Jeso Tompo, mamindrà fo
Amin''ny mpanomponao
Fa tsy misy famonjena
Afa-tsy ao aminao.
:,: O! vonjeo, :,:
Ny mifona aminao.}

{2.
Fa kamboty sy mahantra,
Ory, osa, reraka
Izahay aty an-tany,
Raha tsy vimbininao;
:,: O! ampio:,:
Ny miankina aminao.}

{3.
Tokinay sy vatolampy,
Aro sy Ampinganay!
Afa-tahotra, afa-doza,
Tsy ho ketraka izahay;
:,: O! arovy:,:
Ny mialoka aminao.}
', '#1-VII-13', '#1') /end

insert into song values (16271, '627. Vimbino tsara izahay', NULL, NULL, '
{1.
Vimbino tsara izahay,
Ry Jeso Kristy Tomponay
Mba tsy ho diso lalana
Aty ambony rano be,
:,: Ka na manonja aza re
Ny rian-dranomasina,
Tazony mafy izahay
Mba tsy ho rendrika aty.: ,:}

{2.
Dia mitsangàna, ka avia,
Ry Jesosy, Mpamonjy tia!
Jereo ny fahorianay;
Anaronao ny rivotra
:,: Izay mampanonja toy izao
Ny alon-dranomasina,
Izay manafotra anay
Aty am-pandehananay.:,:}

{3.
Fa hianao no aronay,
Ka dia sahy izahay
Hamaky sy hano hitra
Ny onjan-drano betsaka;
:,: Ka aoka tsy ho fefika
Aty an-dranomasina,
Fa mba ho tonga aminao
Ny sambonay, ry Tompo o! :,:}
', '#1-VII-13', '#1') /end

insert into song values (16281, '628. Ry Mpamonjy olom-bery!', NULL, NULL, '
{1.
Ry Mpamonjy olom-bery!
Ry Jesosy tia anay!
Izahay tsy manan-kery
Tsy mba hendry, tsy mahay,
Koa Ianao irery
:,: No mba iankinanay.:,:}

{2.
Ry Mpamonjy be fitia!
Aza ilaozanao izahay;
Ry Jesosy o! Avia,
Ianao no tanjakay!
:,:Miarova, manampia,
Be ny fahavalonay.:,:}

{3.
Ry Mpamonjy! Mitantàna
Izao mpanomponao izao;
Be tokoa ny fikasana
Ta-hanao ny sitrakao,
:,: Nefa be ny fahosana
Momba ny mpanomponao.:,:}

{4.
Ry Mpamonjy, Jeso tia!
Mivimbina, mamonje;
Ry mahery! Manampia,
Miambena, mijere;
:,: Miarova, mitariha,
Ry Sakaiza lehibe!:,:}
', '#1-VII-13', '#1') /end

insert into song values (16291, '629. Ry Jeso Mpamanjy, Mpanavotra anay', NULL, NULL, '
{1.
Ry Jeso Mpamanjy, Mpanavotra
anay!
Tafangona eto anio izahay
Hidera sy mba hankalaza Anao,
Fa Tompo namonjy anay Ianao;}

{FIV.
Haleloia! Haleloia!
Hosana an''ny Avo!
Haleloia! Haleloia!
Hosana ho Anao!}

{2.
Ry Jeso, Sakaiza tsy mety
mandao!
Sakaiza tsy mba mahafoy hianao,
Mijery anay raha misy manjo,
Ho aron''ny tena sy tokin''ny fo:
[FIV.]}

{3.
Ry Jeso, Mpanjaka Mpiaro anay!
Handrosa hiady tokoa lzahay,
Ka dia vonjeo handresy izao,
Fa tsy mba ho resy izay manana
Anao:
[FIV.]}

{4.
Ry Jeso, Mpitarika anay ho ary,
Tariho izahay tsy hivily aty.
Ka dia hamantana ao aminao,
Fa faly miandry anay Ianao:
[FIV.]}
', '#1-VII-13', '#1') /end

insert into song values (16301, '630. Ry Jesosy mba tantano', NULL, NULL, '
{1.
Ry Jesosy mba tantano,
Ho tafita soa ary,
Ka ambeno sy sakano,
Izay rehetra loza aty,
Rano be mangeniheny,
Izao alehako izao;
Ka aoko Ianao hiteny,
Mba ho tokiko hatrao.}

{2.
Aza miandry ao am-pita
Sao dia fefika aho aty ;
Fa avia hiara-mita,
Dia ho tody soa ary,
Raha sendra ka manonja
Izany alon-drano be,
Aoka Ianao ho tonga
Hampitony ahy re.}

{3.
Ao ny rivotra mahery
Mety hampivadi-po,
Fa ny feonao irery
Mampisava ny manjo
Any ko ny vatolampy
Sy ny biby marobe,
Fa ny herinao dia ampy,
Ka vonjeo ny zanakao.}

{4.
Ry Jesosy ! mihainoa,
Izao fivavako izao,
Mijoroa, misehoa,
Ka jereo ny diako izao ;
Mbola lavitra ao am-pita
Ka tsy hita izay hanjo,
Toy ilay tao Genesareta,
Misehoa Ianao!}
', '#1-VII-13', '#1') /end

insert into song values (16311, '631. Ry Jesosy Tompo Tsara', NULL, NULL, '
{1.
Ry Jesosy Tompo tsara,
Ry Mpamonjy o!
Sambatra aho, mananjara
Manana Anao;
Fa na maro ny mpandrafy
Ta-hamely ahy mafy,
Ianao mahay manafy
Hery ny Anao.}

{2.
Be ny manodidina ahy
Efa lasana;
Manakaiky, mampanahy,
Re ny fasanaj
Nefa, ry Mpamonjy soa,
Tena sitrakao tokoa
Ny hitantana ahy koa
Ao an-dàlana,}

{3.
Eny, izaho manantena,
Ry Mpamonjy o!
Fa hahazo famonjena
Any aminao.
Raha hilaozako ny tany,
Izaho tsy mba hitomany,
Fa hiakatra hankany
Anatrehanao.}
', '#1-VII-14', '#1') /end

insert into song values (16321, '632. Ampianaro aho, Tompo, mba hiambina!', NULL, NULL, '
{1.
Ampianaro aho, Tompo, mba
hiambina!
Mandalo tahaka ny rivotra ny
androko.
Ka mihalany isan''andro, isan''alina,
Ambara-pahatapitry ny fepotoako.}

{2.
Endrey ny voninkazo, raha vao
maraina e!
Fa tsara ka mamirapiratra ny
endriny;
Kanefa maty izy amin''ny
hainandro be;
Ka raha hariva dia simba ny
tarehiny.}

{3.
Ny tenako no voninkazo ka
hihintsana,
Ny aiko tahaka ny zavbna sy aloka
Izay mandalo ka tsy manampijanonana;
Ny taonako mandroso tahaka ny
riaka.}

{4.
Tsy manam-panorenana aty ny
heriko,
Mihelina ka tsy mba hita ny
alehany;
Malaky tahaka ny tselatra ny
androko,
Ka tsy miatoato ao ampandehanany.}

{5.
Ry Tompo o, ampio aho mba
hiomana,
Ka mba hadio fo na aiza no
alehako;
Ampaherezo mafy aho mba ho
malina,
Mba tsy ho tonga tampoka amiko
Ny fetrako!}
', '#1-VII-14', '#1') /end

insert into song values (16331, '633. Izaho tsy mba mahalala', NULL, NULL, '
{1.
Izaho tsy mba mahalala,
Izay hiainako aty;
Anio angaha dia hiala,
Ka hodimandry ho ary!}

{FIV.
Ny ran''i Kristy, Raiko o,
No amelanao ny heloko.}

{2.
Ry Ray o, mampianara ahy
Hanisa e, ny androko,
Hiantombenan''ny Fanahy
Ao amin''ny Mpamonjiko.
[FIV.]}

{3.
Ny asa sy ny ahiahy,
Izay maningotra ahy izao,
Vahao mba tsy hisakana ahy
Hiainga raha antsoinao.
[FIV.]}

{4.
Ny fahotako an-tapitrisa
Saronanao ny avotra,
Izay nandraisako batisa
Sy ny sakafo masina.
[FIV.]}

{5.
Tsy misy mahasaraka ahy
Amin''ny fitiavany:
Ny teniny no mahasahy
Ny olom-boafidiny.
[FIV.]}

{6.
Ka dia tsy mba mampanahy
Ny andro hahafatesako,
Jesosy no miantoka ahy,
Ny ainy no anjarako.
[FIV.]}
', '#1-VII-14', '#1') /end

insert into song values (16341, '634. Ny lalàna izay nataon''Andriamanitra', NULL, NULL, '
{1.
Ny lalàna izay nataon''
Andriamanitra
Tamin''ny olombelona
:,:Marina indrindra.:,:}

{2.
Raha manaraka izany,
Dia tsy manan-tsiny
Fa nanota ny olona
:,: Ka nanan-keloka. :,:}

{3.
Raha tsy mino an''i Kristy
Ny olombelona,
Fa matoky ny tenany
:,: Tsy azo hitsina. :,:}

{4.
Fa Jeso no manafaka
Ny ota rehetra,
Ny manaiky ny teniny
:,: No azo hitsina. :,:}
', '#1-VII-14', '#1') /end

insert into song values (16351, '635. Aina no fetra ry Jesosy Tompo', NULL, NULL, '
{1.
Aina no fetra ry Jesosy Tompo
Izany no tenin''ity zanakao
Vonon-kanaraka Anao sy
hanompo
Amin''ny mora sy sarotra hatao.}

{FIV.
Mandrakizay no tokimpanompoana
Mamin''ny olon''izay afakao.
Mandrakizay ekenay ho faneva
Ny holatrao navotana anay.}

{2.
Na ny mangidy heverina ho
mamy,
Raha miray aminao izahay,
Iza no tsy hahafoy ny ainy
Mba ho Anao Izay nisolo anay!
[FIV.]}

{3.
Ianao, ry Jeso! No ''lay Kapiteny,
Izay hampandresy tokoa anay;
Ka raha Ianao, Tompo, no mba
miteny,
Lasa izay mahakivy anay:
[FIV.]}

{4.
Vonon-kandeha na aiza na aiza,
Ireto mpanompo matoky Anao;
Na dia ny fahafatesana aza,
Tsy hahaleo anay mpianatrao:
[FIV.]}
', '#1-VII-15', '#1') /end

insert into song values (16361, '636. Ianao ry Jeso o!', NULL, NULL, '
{1.
Ianao ry Jeso o!
No mba hatoninay,
:,:Tompon''ny aina:,:
Mba mihainoa ka avia.}

{2.
Mba ampangino re
Ny ao anatinay,
:,: Tsy hisy renay: ,:
Raha tsy feonao.}

{3.
Aoka izahay handray
Ny fanavaozanao,
:,: Ka mba hitovy:,:
Toe-tsaina aminao.}

{4.
Mafy ny adinay,
Ka be ny tahotray;
:,: Nefa tsy resy:,:
Izay miankina aminao.}

{5.
Dia tafionao
Amin''ny herinao,
:,: Mba ho mpandresy :,:
Izahay mpianatrao.}

{6.
Ny fonay hafanao,
Mba hirehetanay
:,: Amin''ny asa :,:
Izay omenao hatao.}

{7.
Mba ho tanterakay
Izay tena anjaranay,
:,: Dia manomeza :,:
Fo miray aminao.}
', '#1-VII-15', '#1') /end

insert into song values (16371, '637. Mahareta, mahareta', NULL, NULL, '
{1.
Mahareta, mahareta,
Ry mpihazakazaka o!
Be ny zava-mahalavo.
Aza kivy hianao.}

{2.
Mahareta, mahareta,
Ry mpihazakazaka o!
Mahavesatra ny ota,
Aoka re harianao,}

{3.
Mahareta, mahareta,
Ry mpihazakazaka o!
Lavi-dalana haleha,
''Ndrao tsy tonga hianao.}

{4.
Mahareta, mahareta,
Ry mpihazakazaka o!
Indreo ny maso izay mijery,
Tazany ny dianao.}

{5.
Mahareta, mahareta,
Ry mpihazakazaka o!
Ao ny Tomponao miandry,
Izy no Jereonao.}
', '#1-VII-15', '#1') /end

insert into song values (16381, '638. Ry Jeso Mpamonjy malala', NULL, NULL, '
{1.
Ry Jeso Mpamonjy malala
Indreto ny mpanomponao ;
Raha misy izay mankahala,
Matoky fa manana Anao}

{FIV.
:,: Jereo, jereo!
Fa vonon-kanaraka Anao :,:}

{2.
Ny tafika ataon''i Satana
Milofo hamotraka any ;
Avia, ry Jeso ! mitantana
Fa mioman-kandresy izahay.
[FIV.]}

{3.
Na zovy mioman-kandrafy,
Irinay hanaraka Anao ;
Na iza hisakana mafy,
Hijoro, fa manana Anao,
[FIV.]}

{4.
Ireo nentinao voatantara
Na feo mampahery anay ;
Ny tokin''ireo Maritiora
Faneva mitarika anay;
[FIV.]}
', '#1-VII-15', '#1') /end

insert into song values (16391, '639. Ry miaramila ! mandrosoa izao', NULL, NULL, '
{1.
Ry miaramila ! mandrosoa izao,
Kristy Kapiteny mialoha anao ;
Ao aloha tafika, loza, ratra be,
Fitsaraharana tsy misy, misikina re,}

{FIV.
Ry miaramila! Mandrosoa izao
Kristy Kapiteny mialoha anao.}

{2.
Any lehy ratsy no manotrika,
Any ny tsipika no milatsaka;
Tany ny finoana ho ampinganao
Entonao ny fandresena
Izao rehetra izao:
[FIV.]}

{3.
Aza miamboho na miherika,
Ao an-tranonao ny fiarovana ;
Aza mba matahotra, fa sahia fandrao
Raha tsy mandresy, dia ho resy hianao;
[FIV.]}

{4.
Dia mandrosoa hianao izao
Ka miraisa dia aminay ho ao
Milofosa fatratra, aza ketraka,
"Miadia ny ady tsaran''ny
Finoana"
[FIV.]}

{5.
Kristy Kapiteny tapitra ohatra,
Izy no mpiaro ny mpanafika ;
Miankìna aminy mafy hianao,
Ka za mba ny tenano no itokianao:
[FIV.]}
', '#1-VII-15', '#1') /end

insert into song values (16401, '640. Misy fahavalo maro', NULL, NULL, '
{1.
Misy fahavalo maro
Te-hiady aminay,
Faly sy miravoravo,
Raha mamotraka anay;
Izahay tsy manan-kery
Handresenay azy ireo;
Ry Mpamonjy olom-bery!
Jeso Tompo o! Vonjeo.}

{2.
He! Mibaiko ny devoly,
Ilay mpihaza aina soa,
Ka mikendry hanangoly
Sy handatsaka aina koa;
Tsy mba misy izay hanampy
Hampandositra azy izao;
Ianao no tsy manary
Jeso Tompo o! Vonjeo.}

{3.
/ndreo ny fandrika harato
Izay mivelatra avokoa.
Tsara sy milanto lanto,
Toa fiainana tokoa;
Kanjo, indrisy ny mahazo
Izay fatoran''izy ireo;
Tsy mba hainy ny miala:
Jeso Tompo o! Vonjeo.}

{4.
Miomàna, ry sakaiza!
Be ny fahavalonao ;
Mitandrema sy mahaiza,
Sarotra ny làlanao;
Aza mba matoky tena,
Fa ny Tompo no jereo;
Indrisy re ny halemena:
Jeso Tompo o! Vonjeo.}
', '#1-VII-15', '#1') /end

insert into song values (16411, '641. Ry Kapiteny, be ny ady manjo', NULL, NULL, '
{1.
Ry Kapiteny, be ny ady manjo,
Mila ho reraka sy kivy ny fo,
Ka hatanjaho mba hatoky Anao,
Tompo o! Avia mamonjy ahy
izao.}

{FIV.
"Eny, Izaho mamonjy anao,
Ka mampahery sy miaro anao:
Aza matahotra mihitsy hianao,
Fa Izaho momba sy manampy
anao."}

{2.
Ny fahavaloko aty mba reseo,
Ka taomy aho mba handray
rahateo
Ny fiadiana avy aminao;
Tompo o! Vimbino aho,
aza mandao:
[FIV.]}

{3.
Lasa tokoa re ny tahotro teo,
Ka na mamely ahy aza ireo,
Tsy mba manahy intsony aho izao,
Fa mamonjy sady tsy mandao
Ianao:
[FIV.]}

{4.
Fa ny mpiady maharesy aty
No hosatroham-boninahitra
ary;
Maro no efa tonga ao Aminao,
Tompo o! Avia hitarika ahy
hankao:
[FIV.]}
', '#1-VII-15', '#1') /end

insert into song values (16421, '642. Hitako izao, sakaiza !', NULL, NULL, '
{1.
Hitako izao, sakaiza !
Jeso no Mpanavotra ;
Tsy mba lany ny fitia,
Jeso no Mpanavotra;
Na dia saky lasa aza,
Ka ny olona mandao
Izao no mampiadana ahy,
Jeso no Mpanavotra.}

{2.
Raha miady mafy aho,
Jeso no Mpanavotra;
Toky sy Ampinga Izy,
Jeso no Mpanavotra
Manome ny soa Izy,
Fa ny rany mahadio
No mahafa-keloka ahy,
Jeso no Mpanavotra.}

{3.
He ! ilaozako ny eto,
Jeso no Mpanavotra;
Indro lanitra no azo,
Jeso no Mpanavotra
Izy no Mpamonjy ahy,
Aina sy fanilo koa,
Fahendrena mahasoa
Jeso no Mpanavotra.}

{4.
Ray o ! hianao deraiko,
Jeso no Mpanavotra;
Ianao naniraka Azy,
Jeso no Mpanavotra;
Ianao Mpiaro ahy,
Mpitahiry, Tokiko,
Ka deraiko lalandava,
Jeso no Mpanavotra.}
', '#1-VII-15', '#1') /end

insert into song values (16431, '643. O ! mandrosoa mba ho tena mpandresy', NULL, NULL, '
{1.
O ! mandrosoa mba ho tena mpandresy
Dia mazotoa, miasà, mitomboa.
:,: Raiso ny tsara, ka roahy ny ratsy
Ka matokia an''i Jeso tokoa. :,:}

{2.
O! mandrese, ny finoana tano,
Na dia mafana ny ady hatao,
:,: ny fandresena homena ny mino,
Azy ny herin''ny Tompo izao. :,:}

{3.
Mba matokia ny hery tsy hita
Izay voatahirin''ny Ray ho anao,
:,: Ka voatolotra tao Gologota
Mba ho antsika rehetra izao. :,:}
', '#1-VII-15', '#1') /end

insert into song values (16441, '644. Matahora mangovita', NULL, NULL, '
{1.
Matahora mangovita,
Fa indro ny devoly
No mihendry hisambotra
Ny saintsika rehetra;
:,: Miambena,:,:
Ka mivavaha, tsara.}

{2.
Mifalia, mivavaha,
Jeso no arontsika.
Manaraha ny teniny,
Fa hovonjeny isika;
:,: Tano Izy :,:
Hahazoana aina.}

{3.
Manandrata feo hihira
Hidera an''i Kristy,
Hanome voninahitra
an''Andriamanitra
:,: Tompontsika,:,:
Fa marina izany.}
', '#1-VII-15', '#1') /end

insert into song values (16451, '645. Jesosy Kapiteninay', NULL, NULL, '
{1.
Jesosy Kapiteninay,
Izao miantso izao,
Hiady no ilàna anay
Ka zovy no hanao?
Izay mahafy hatraty,
Mahery fo tokoa
Ka tsy miraika hatrary,
Ireny no hanao.}

{2.
Ny tany hotafihinay,
Izao tontolo izao;
Ny tafika hikatrohanay,
Tsy ady mora atao;
Ny ota no hasianay
Ho ringana avokoa
Ny tany no hasondrotray
Ho Paradisa soa.}

{3.
lndreo, ny maritiora soa
No loha-Iàlanay,
Nalatsaka aina avokoa
Ka maika aminay.
Fandavan-tena lehibe
No vitan''izy ireo,
Hodian-tsy hita foana ve
Ireo maritiora ireo?}

{4.
Ry Jeso Kapiteninay!
Izao anio izao
No anavaozanay indray
Ny fanompoana Anao
Atolotray Anao tokoa
Izao tenanay izao,
Ka hamasino avokoa
Hanao ny sitrakao.}
', '#1-VII-15', '#1') /end

insert into song values (16461, '646. Na aiza no alehako', NULL, NULL, '
{1.
Na aiza no alehako.
Dia misy izay hanina;
Fa be ny fahavaloko
Mahery sy kirina.
Mikendry fatratra
Ireo mpamitaka,
Ka dia hazoniko ny to
Na aiza no alehako.}

{2.
Na aiza no alehako,
Dia misy ady mafy;
Ny ota hotoheriko,
Hataoko tena rafy;
Ny fahoriana,
Izay ikapohana,
Tsy maintsy hiaretako,
Na aiza no alehako.}

{3.
Na aiza no alehako,
Ny fasana mangina
Miandry eo alohako,
Fa aloka ny aina
Sy toy ny zavona
Malaky levona;
Izany no tsaroako
Na aiza no alehako.}

{4.
Na aiza no alehako,
Momba ahy ny anjely,
Ka tsy mba hatahorako
Satana izay mpamely.
Ry izao tontolo izao,
Ho resy hianao,
Anjely no ho mandako
Na aiza no alehako.}

{5.
Na aiza no alehako,
Jesosy no kiady;
Ny ratrany no aroko
Sy herin''ny mpiady.
Ny dian-tongony
Arahiko aty,
Ka fandresena azoko
Na aiza no alehako.}

{6.
Na aiza no alehako,
Ny lanitra andrandraiko,
Ka ravo ny fanahiko,
Fa ao ny Tompon''aiko,
Ry izao Tontolo izao,
Ny voninahitrao
Dia vovoka hitsahiko:
Ny lanitra ao alohako.}
', '#1-VII-15', '#1') /end

insert into song values (16471, '647. He! Sarotra ny lalam-pamonjena', NULL, NULL, '
{1.
He! Sarotra ny lalam-pamonjena,
Hitanana ny vonjy izay nomena,
Hamelona hatrao am-parany
Ny fitiavana voalohany.}

{2.
Ny maro dia mazàna
mankamamy
Izao tontolo izao, ka mora lany
Ny zotom-po sy ny finoana,
Dia tonga tia zava-poana.}

{3.
Malaina izy ka tsy te-hiady
Ny ady masina izay maro fady,
Ny lalan-tery dia halany,
Fa ny malalaka no tiany.}

{4.
Mazàna mety menatra ireny
Rehefa voatsindron-toriteny,
Fa hitany ny ratsy ao am-po,
Kanela tsy mba te-hiova fo.}

{5.
Ry Jeso, hianao no tokin''aina,
Izay novonoina sy nankahalaina.
Omeo ahy laharetam-po,
Hiaritra izay anjarako.}

{6.
Raha reraka aho ka misalasala,
Dia asehoinao, ry Ray Malala,
Ny lovako ary an-danitra
Hihazona ahy mba haharitra.}
', '#1-VII-15', '#1') /end

insert into song values (16481, '648. Miambena izao', NULL, NULL, '
{1.
Miambena izao,
Ry fanahiko!
Misy rafinao misaina
Hanangoly ny malaina;
Be ny fandrika
Voavelatra}

{2.
Miambena izao,
Ry fanahiko!
Mba tsy ho andevon''ota,
Na hotaomin''ny mpanota;
Mivavaha izao,
Ry fanahiko!}

{3.
Miambena izao,
Ry fanahiko!
Ka tohero ny devoly,
Raha avy manangoly;
Kristy raisonao
Mba ho aronao!}

{4.
Miambena izao,
Ry fanahiko!
Tsy ho ela tsy ako ry
Dia ho lasa ny mahory,
Ka ho afaka
Ny mpiambina.}

{5.
Miambena izao,
Ry fanahiko!
Fa ho avy indray ny Tompo
Mba hampody ny mpanompo
Ho finaritra
Ao an-danitra.}
', '#1-VII-15', '#1') /end

insert into song values (16491, '649. Ry mino, miambena re', NULL, NULL, '
{1.
Ry mino, miambena re,
Fa manam-pahavalo be;
Jesosy no arahonao
Hitarika anao.
Ezahonao ny dianao,
Ny satro-boninahitrao
Miandry ao aloha ao,
Ka mba tratraronao!
Mba mivavaha, ry havako,
Ka miomàna ao am-po;
Andeha izao ilaozinao
Ny zavatra ho lo!}

{2.
Alao no fiadiana
Omen''Andriamanitra,
Ny fahamarinana alao
Mba hisikinanao;
Hazony ny finoana
Ampinga hanohanana;
Ny tenin''Andriamanitra
No tena sabatra;
Ny famonjena raisonao
Ho fiarovan-dohanao;
Ireo no hanoheranao
Ny ratsy ao am-po.}

{3.
Ry havana malala o,
Andeha isika hiray am-po
Hikatsaka ny lanitra,
Fa ao tsy misy manjo.
Ho faly ao an-danitra
Ny vita fanompoana,
Ka aoka izao isika handao
Ny zava-poana!
Ho lasa faingana indray
Ny androntsika an-trano lay;
Vonjeonao ilay tany vao,
Izay mandry fahizay.}
', '#1-VII-15', '#1') /end

insert into song values (16501, '650. Miambena mivavaha, fa tsy fantatrao', NULL, NULL, '
{1.
Miambena mivavaha, fa tsy
fantatrao
Na ny andro, na ny ora fepotoanao;
Ny Mpanjaka masina ho tonga
tampoka
Hampaneno ny trompetra ao andrahona.}

{2.
Amboary ny fanilon''ny finoana
Sy ny solika fitaiza voaomana,
Ka velomy re ny afom-pitiavana,
Mba hidiranao an-dapampifaliana.}
', '#1-VII-15', '#1') /end

insert into song values (16511, '651. Jeso, ampio izahay mba handresy!', NULL, NULL, '
{1.
Jeso, ampio izahay mba handresy!
Ny fahavalo mameIy anay!
Indro ny aizina tsy mbola resy,
Ary Satana mamandrika anay,
Ka mba omeo anay ny mazava
Hampahasahy anay lalandava.}

{2.
Jeso, ampio izahay mba handresy!
Kely finoana re izahay;
Kivy matetika, mila ho resy;
Aoka ny herinao homba anay;
Dia hatoky izahay, ry Mpandresy,
Fa ny devoly dia ho voasesy.}

{3.
O , ry Mpandresy, arovy ny ainay!
Ady mafana hiseho izao;
Ka mba tohany hatoky ny fonay!
Dia hiainga hanaraka Anao;
Na dia loza ny herin-tSatana,
Sahy izahay, Ianao no mitana.}

{4.
Ampandreseo izahay, ry
Mpamonjy!
Ka raha vita ny ady aty,
Dia ho isan''izay voavonjy,
Ary ho faly sy tretrika ery;
Tsy mba hitsahatra intsony ny
hoby
Hankalazana Anao, ry Mpamonjy.}
', '#1-VII-15', '#1') /end

insert into song values (16521, '652. O, ry Jesosy, omeo ny hery', NULL, NULL, '
{1.
O, ry Jesosy, omeo ny hery,
Mba handreseko ny ota izao!
Osa sy ketraka aho irery,
Raha tsy avy izao Ianao .
Raiso ny fo,
Tano tokoa,
Mba handreseko ry Tompo o!}

{2.
O, ry Jesosy, omeo ny hery,
Ka amboary ny fonay izao;
O, mba esory izay mahavery,
Mitomoera aminay Ianao;
Ka hazavao
Ny fo izao,
Ampandosiro ny aizina ao!}

{3.
O, ry Jesosy, omeo ny hery!
Kely finoana re izahay,
Ary ny lalanay ety sy tery,
Ry Jeso o, mitantàna anay!
Tohanonao
Ny olonao,
Ampandreseo izay manaraka
Anao!}
', '#1-VII-15', '#1') /end

insert into song values (16531, '653. Sakaiza, raha tia', NULL, NULL, '
{1.
Sakaiza, raha tia
Ny Tompo hianao;
Faingàna ka avia
Hanaraka Azy izao!
O, mba tsarovinao,
Fa Izy tsy nifaly,
Naleony nijaly
Mba hamonjena anao.}

{2.
Fanahy, mahereza,
Efao ny adinao,
Fa làlana mideza
No hiakaranao;
Ny zava-tsoa aty
Dia zavatra mandalo;
Tsy azom-pahavalo
Ny lovanao ary.}

{3.
Ry Tompo, manomeza
Anay ny herinao;
Finoana mateza
Mba atolorinao,
Mba hanarahanay
Ny làlam-pamonjena,
Hahita Anao mitsena
Anay rahatrizay.}
', '#1-VII-15', '#1') /end

insert into song values (16541, '654. Mba faingàna re, ry mino', NULL, NULL, '
{1.
Mba faingàna re, ry mino,
Aza mba tamana aty,
Ary aza manadino
Ny anjaranao ary.}

{FIV.
Ny Mpamonjy anao,
No manangana faneva,
Ary mampihevaheva
Azy eo alohanao.}

{2.
Marobe no mba mitady
Ny fiainana vaovao,
Nefa kosa tsy miady
Amin'' izao tontolo izao.
[FIV.]}

{3.
Koa raiso ny ampinga,
Tano re ny sabatrao,
Fa izany mahakinga
Ny mpandeha toa anao.
[FIV.]}

{4.
He, akaiky anao Kanana,
Tany hovantaninao;
Ary any ny tanàna
Misy ny anjamnao.
[FIV.]}
', '#1-VII-15', '#1') /end

insert into song values (16551, '655. Kristy miantso antsika hiady', NULL, NULL, '
{1.
Kristy miantso antsika hiady,
Izy nandresy ilay ratsy sahady,
Ka mba henoy, ry sakaiza izao;
Kristy miandry indrindra anao!
Mila antsika
Kristy Mpanjaka.}

{2.
Kristy Mpitarika amin''ny ady,
Ka mampandresy izay te-hifady
Ny fahotana, izay maimbo sy lo;
Izy no mànda mafin''ny fo.
Miaro antsika
Kristy Mpanjaka.}

{3.
Kristy mampiatrana antsika
hahery,
Izy miaro antsika sao very,
Ka mba avia hifahatra ao,
Indro ny ady mandroso izao.
Miantso antsika
Kristy Mpanjaka.}

{4.
Kristy mamonjy antsika izay voa
Ary tsaboiny ho entiny koa
Ho ao an-dapany mandrakizay
Honina ao an-tranon''ny Ray.
Haka antsika
Kristy Mpamonjy.}

{5.
O, ry Jesosy, Mpanjaka,
Mpamonjy!
Tompon''ny dera aman-daza sy
vonjy,
Ireo rafinao mba reseo izao,
Ka tanteraha izay sitrakao.
O, mba avia,
Kristy Mpanjaka!}
', '#1-VII-15', '#1') /end

insert into song values (16561, '656. Maty malala, ka ory ny fo!', NULL, NULL, '
{1.
Maty malala, ka ory ny fo!
Lasa tiana ka mafy manjo!
Nefa isika tsy dia mba mamoy,
Fa manantena hihaona indray:}

{FIV.
Ao aminao, Tompo izy izao
Tsy mba marary na misy manjo!
Nentina any ambony madio
Dia ho sambatra mandrakizay.}

{2.
Mba notandremana fatratra koa,
Nefa tsy tana, fa lasa izao;
Eny, ry Tompo! Ekena tokoa
''Zay sitrakao, fa izany no soa:
[FIV.]}

{3.
Fantatra koa fa isika izao
Samy vahiny ka hody indray;
Indro! ny namana lasa izao,
Lasa miandry antsika ary:
[FIV.]}

{4.
Ary isika, raha hiala aty,
Dia hotsenain''ireo havan-tsy foy;
Eny, hihaona mifaly indray
Ka tsy hisaraka mandrakizay.
[FIV.]}
', '#1-VII-16', '#1') /end

insert into song values (16571, '657. Lasa re ilay nazoto', NULL, NULL, '
{1.
Lasa re ilay nazoto,
Nody ao an-tany soa,
Izay nanompo tamin-joto,
Ka nahefa be tokoa
:,: Indro, faly:,:
Fa mijinja voka-tsoa.}

{2.
Lasa nody fa voantso:
"Miakara," hoy ny Ray,
Ka fafao ny ranomaso,
Jeso Tompo no mandray,
:,: Faly, ravo, :,:
Fa tafakatra ao tokoa.}

{3.
Lasa re ilay nihafy
Ory tena, be nanjo,
Fa nanohitra ady mafy,
Nefa mba nahery fa.
:,: Ka nandresy :,:
Fahavalo maro be.}

{4.
Afaka ady ka nifaly,
Fa tsenaina hatraty;
Afaka ota, afa-jaly,
Ka mihoby, hatrary.
:,: Hoy ny Tompo: :,:
"Mandrosoa, ka mifalia."}
', '#1-VII-16', '#1') /end

insert into song values (16581, '658. Tonga ny fahafatesana!', NULL, NULL, '
{1.
Tonga ny fahafatesana!
Ary jinjany ny velona;
Izy manapaka ny lehibe,
Sady mamira ny kely, re!}

{2.
Nefa ny fasana maizina
Efa nidiran''ny Masina;
Izy nampody ny fiainana,
Resy ny fahafatesana.}

{3.
Amin''ny manam-pinoana,
Dia mazava ny fasana,
Izy hitsangana finaritra
Noho Jesosy Mpanavotra.}

{4.
Resin''ny Tompo ny fasana,
Koa, ry fahafatesana,
Aiza izao ny fanindroanao?
Aiza, ry helo, ny herinao?}
', '#1-VII-16', '#1') /end

insert into song values (16591, '659. Ry hava-malala misaona', NULL, NULL, '
{1.
Ry hava-malala misaona,
Mangina fa mbola hihaona,
Isika sy izy nalaina,
Raha tonga indray ilay maraina.}

{2.
Ny naman''ny tany ihany
No mody ho amin''ny tany,
Ka dia ny Fanahy mifindra
Ho amin''ny Avo Indrindra.}

{3.
Tsy maintsy afafy aloha
Ny vary vao met y hamoa;
Ny faty halevina koa
Tsy very fa mbola hifoha.}

{4.
Dia miandrandrà, ry mpisaona!
Ho avy ilay lohataona,
Ka dia haniry ny voa,
Izay misy ny ain''ny Tsitoha.}

{5.
Ho avy ny Tompon''aina
Hiantso ny vovoka maina
Hitafy ny fahazavany
Sy hiditra ao am-panjakany.}

{6.
Faingàna, ry Kristy mahery,
Ry Fanantenanay irery,
Ny fasanay iva tsarovy,
Ny toky nomenao mba tovy!
}
', '#1-VII-16', '#1') /end

insert into song values (16601, '660. Sambatra ny nodimandry', NULL, NULL, '
{1.
Sambatra ny nodimandry
Niala teto ka tafandry
Ao ampofoan''i Kristy;
Ravoravo izao ny fony
Ka tsy misasatra intsony,
Mby ao am-pitsaharany.
Ny asany taty
Manaraka azy ary,
Haleloia!
Tafiditra ao an-dapanao
Ry Jeso, ny malalanao.}

{2.
Saotra, dera, laza, hery,
Mba raiso ho Anao irery,
Ry zanak''ondry tokinay!
Ianao dia nialoha
Hanolotra anjara soa
Ho an''ilay malalanay.
Ny fahafatesanao
No misy aim-baovao,
Haleloia!
Hahita Anao ny olonao
Hohazavain''ny tavanao.}

{3.
Tsy mba misy alina any,
Jesosy no fahazavany
Sy tena masoandrony,
Azony izay niriny,
Ka sambatra ny mpivahiny,
Fa ao no tanindrazany,
Fa efa tapitra
Ny ali-maizina,
Haleloia!
Fa foana ny fasana,
Maraina no miposaka.}
', '#1-VII-16', '#1') /end

insert into song values (16611, '661. He, lasa ny zaza malalan''ny fo!', NULL, NULL, '
{1.
He, lasa ny zaza malalan''ny fo!
Atolotra ny tany ny tena ho lo.}

{2.
Tsy maty anefa ny zaza, fa he,
Jesosy no milaza: Matory izy e!}

{3.
Jesosy hamoha ny zaza indray,
Hiara-paly amin''anjelin''ny Ray.}

{4.
Ka mba mionona, ry havana aty!
Ho hitanao ny zaza ao an-danitra
ary.
}
', '#1-VII-16', '#1') /end

insert into song values (16621, '662. O, ry Mpamonjy avia Ianao', NULL, NULL, '
{1.
O, ry Mpamonjy avia Ianao,
Indro misento ny fonay izao;
Ny alahelo mamely anay,
Koa mitady Anao izahay!
Iza na aron''izay ory fo ,
Sady mpanampy ny manamanjo,
Fa tsy ny Tompo mahery sy to?
Dia Ianao izay be indrafo.}

{2.
Jeso Mpamonjy o, mba iantrno,
Izay malahelo miantso Anao!
Ny mitomany tsy hitanao va?
Fantatray fa Ianao tsy handà
Dia manatona ka mba mijere
Ny ranomaso izay mitete!
Fa Ianao no mahay manome
Fampiononana be dia be.
}
', '#1-VII-16', '#1') /end

insert into song values (16631, '663. He, lasa izao ny malala', NULL, NULL, '
{1.
He, lasa izao ny malala
Nifindra ao an-dapan''ny Ray;
Isika rehefa miala,
Ho faly hahita azy indray.
Isika izao mitomany,
Iharan''ny mafy manjo;
:,:Jesosy mahita izany,
Ka Izy hanony ny fo.: ,:}

{2.
Matetika re ny mangidy
Avela hihatra aminao,
Ka aoka mba tsy hanadidy,
Fa feon''ny Tompo izao.
Ny zanaka tian''ny rainy
Tsy maintsy anariny re!
:,: Izay mahasoa ataony,
Ombam-pitiavana be}

{3.
Ny fa mora kivy ario,
Fa tsy mba hanary ny Ray;
Ny mamy tsy hita anio
Haposany rahatrizay,
Hofahony ny ranomaso,
Saloam-pifaliana be;
:,: ny toky omeny andraso,
Fa ho tanterahiny re!:,:
}
', '#1-VII-16', '#1') /end

insert into song values (16641, '664. Nalain''ny Mpahary ity zaza ity', NULL, NULL, '
{1.
Nalain''ny Mpahary ity zaza ity
Ho sambatra mandrakizay.
Miandry antsika nilaozany aty,
Fa mbola hihaona indray.}

{FIV.
Onony ny fonao, ry mana-manjo;
Ao an-danitra izy izao,
Ravo izy izao,
Fa ny lapan''ny Ray
Fonenana mandrakizay.}

{2.
Nafahana amin''ny loza ety
Ka tretrika izy izao,
Ny nofo ho lo no alevina aty
Fa any ny tena vaovao.
[FIV.]}
', '#1-VII-16', '#1') /end

insert into song values (16651, '665. Ry mpisaona malahelo', NULL, NULL, '
{1.
Ry mpisaona malahelo,
Mijere ny Tomponao,
Izay namindra ny malala
Mba ho ao an-danitra;
Mba fafao ny ranomaso;
Andrandrao ny lanitra.
Mitsahara ry mpisento,
Ka derao ny Tomponao!}

{2.
Sento no anjarantsika,
Izy kosa faly izao,
Afa-jaly, afaka ota,
Tapitra ady izy ao;
Tsy mba misy ranomaso
Na izay mahory ao,
Koa aza mitomany,
Fa derao ny Tomponao!}

{3.
Nofo no aterintsika
Ao am-pasana anio;
Ny fanahiny anefa
Tsy mba ao anatin''io.
Jeso no namindra azy,
Lasany ny havanao,
Fa navotany ho Azy,
Ka derao ny Tomponao.}

{4.
Mangataha fahendrena,
Ka isao ny andronao;
Mikatsaha famonjena
Sy fiainana vaovao,
Mba horaisiny tokoa
Honina any aminy,
Ka ekeny ho sakaiza,
Mba ho ao an-tranony.}
', '#1-VII-16', '#1') /end

insert into song values (16661, '666. Eo ankavanan-dRay Ianao', NULL, NULL, '
{1.
Eo ankavanan-dRay Ianao,
Ry Zanak''ondry o!
Kanefa loza nahamay
No niaretanao.
Ny henatra aman-kelokay
Nihatra taminao,
Nivesatra ny otanay
No nahafatesanao.}

{2.
Ry Zanak''ondry masina
Nijaly an-tsitrapo,
Teren''ny fitiavana
Hanala otako!
Ny tongotra aman-tànanao
Dia voafantsika;
Ny gadranay dia tapakao,
Izahay dia afaka.}

{3.
Vahoaka alinalna
Mitafy fotsy ery,
Ka samy miramirana
Mahita an''i Kristy
Ny hira fihobiana
Manakoako ao,
Maneho fa navotana
Izao tontolo izao.}

{4.
lndreo ny vahoakanao
An-tapitrisany
Mitondra ny Anaranao
Eo an-kandriny.
He! Toy ny rian-drano be
Ny fiderana Anao,
Voasokatrao ny lanitra
Ho an''ny olonao.}

{5.
Misaotra Anao izahay, ry Ray
Izay navotanao,
Natolotrao ho solonay
Ny lahitokanao.
Ry Zanak''ondry, Avotray,
Derainay Ianao,
Derainay ho mandrakizay
Ny fitiavanao!}
', '#1-VII-17', '#1') /end

insert into song values (16671, '667. Zavatra iray ihany', NULL, NULL, '
{1.
Zavatra iray ihany
No ho tadiavina,
Fa izay aty an-tany
Dia zava-poana,
Ny nofo dia zavamandalo ihany,
Satria ho lo ao anatin''ny tany;
Raha azoko kosa ny zavatra iray
Ny foko ho sambatra mandrakizay.}

{2.
Tadiavinao tokoa
Va ny zavatra izay?
Mba hevero fa ny soa
Momba ny mandrakizay;
Ka aza fatoran''ny eto intsony,
Katsaho tokoa ny any ambony,
Fa any no misy ny zavatra iray
Izay hifalianao mandrakizay.}

{3.
Ry Fanahy o, ampio
Ny finoako izao!
Ry Mpamonjy o, tahio
Ny mpangataka aminao.
Tariho ny foko hitady tokoa
Ny any ambony izay mahasoa;
Jesosy ihany no zavatra iray,
Izay ho anjarako mandrakizay.
}
', '#1-VII-17', '#1') /end

insert into song values (16681, '668. Jesosy Tompoko no lalana', NULL, NULL, '
{1.
Jesosy Tompoko no lalana
Fahamarinana, fiainana.}

{2.
Tsy afaka manatona ny Ray
Raha tsy amin''ny alalany.}

{3.
Jesosy, Tompon''ny fiainana o !
Aoka Ianao no ho fiainako}

{4.
Ety an-tany : fialam-bavaka,
Rahatrizay: fiainan-danitra}

{5.
Iriko mafy ny hiakatra,
Hiaina izany fiainan-tsambatra !}

{6.
Jesosy tokana no hanome
Ahy ny fiainana mandrakizay.}
', '#1-VII-17', '#1') /end

insert into song values (16691, '669. He, tazako ny lalan-tery', NULL, NULL, '
{1.
He, tazako ny lalan-tery,
Izay misy fahoriam-be,
Kanefa feno hira re,
Fa na miady, tsy ho very.
Ny mino izay mizotra eo,
Na misy maty aza eo,
Hahazo ny lakrona.}

{2.
He, tazako Sakaiza mora,
Mpanala fahoriam-be,
Mpiaro soa, Mpanome
Ny toky tsara isan''ora;
Sakaiza feno herim-po,
Mamonjy amin''ny manjo;
Sakaiza tsy manova.}

{3.
He, tazako ny lamba soa
Omena ny voavotra
Ho fanesoran-keloka
Sy fanavaozana azy koa;
Fa Jeso efa nanome
Ny rany saro-bidy re,
Ho fanadiovana azy.}

{4.
He, tazako ny andro ho avy,
Izay hialako sasatra,
Ka ny ririnin''efitra
Hanjary lohataona mamy;
Ka any vao hivelatra
Ny vonindraozin-danitra
Ao amin''ny Edena.}

{5.
He, tazako ny voavonjy
Ao amin''ny Mpampakatra,
Dia ao an-tampon-danitra,
Izay niakaran''ny Mpamonjy;
Miravoravo izy ao,
Mihira fideram-baovao
Ho an''ny Zanak''ondry.
}
', '#1-VII-17', '#1') /end

insert into song values (16701, '670. Ny ony tsy miato, fa mitady', NULL, NULL, '
{1.
Ny ony tsy miato, fa mitady
Ny ranomasina ihaviany.
Ka toy izany koa ny Fanahy
Mitady ny Andriamaniny
Andriananahary
No Tompoko nahary
Ny faniriako,
Izay mitady Azy, ka manary
Izay misakana ny diako.}

{2.
Izay midongy sy mirehareha
Tsy hahatratra ny Edenany;
Lomorina ny rano tsy mandeha,
Ka simba ao am-pijanonany;
Ka toy izany koa:
Ho simba avokoa
Izay mijanona,
Ka tsy manatona ny Tomposoa,
Izay mitahy sy mamelona.}

{3.
Tsy misy zavatra aty an-tany
Mahafa-po ny faniriako;
Vahiny aho eto ka mankany
An-danitra izay fodiako.
Ny Tompoko malala
Ho avy hampiala
Ny olony aty;
Izaho dia tsy misalasala
Hanaraka ny Tompo ho ary!
}
', '#1-VII-17', '#1') /end

insert into song values (16711, '671. Ao an-danitra', NULL, NULL, '
{1.
Ao an-danitra
Dia ho sambatra
Izay tia ny Tompo aty!
Malahelo izao,
Ravoravo re ao
Amin-dRaintsika an-danitra ary.}

{2.
Fahoriana
Ao an-dàlana,
Fa iharan''ny zava-manjo;
Ao an-danitra re,
Fiadanana be;
Mba hevero, ry havana o!}

{3.
Aza kivy o,
Ry fanahiko,
Na dia maizina aza aty;
Vetivety izao
Dia hody hianao
Ao am-pahazavana ary.}

{4.
Ry Mpanavotray,
Indreto izahay
Mpivahiny an-dàlana izao;
Tsy tamana aty,
Te-hifindra ary
Ao an-danitra ao aminao.
}
', '#1-VII-17', '#1') /end

insert into song values (16721, '672. O, mamiratra fatratra ery', NULL, NULL, '
{1.
O, mamiratra fatratra ery,
Irony kintana tazana ety,
Ka manainga ny foko hankao
Amin-tranon''ny Raiko izao.
Raiko o, mba vohay
Aho hiditra ao aminao,}

{FIV.
Raiko o! Mba vohay
Fa iriko ny honina ao,}

{2.
Feno ota ny tany aty;
Tsy mba misy izany ary,
Tena masina ireo mponina ao.
Ka mifaly mijery Anao,
[FIV.]}

{3.
O, fonenana mamiko re!
Fa Jesosy nirina elabe,
Sady aina iainako izao,
No miandry handray ahy ao,
[FIV.]}

{4.
Mba tsilovy ny làlako izao,
Ry Fanahy Mpitarika o,
Hahitako ny tranon''ny Ray
Hovantaniko rahatrizay.
[FIV.]
}
', '#1-VII-17', '#1') /end

insert into song values (16731, '673. Hevero ny fihaonam-be', NULL, NULL, '
{1.
Hevero ny fihaonam-be
Ary an-danitra;
Vahoaka izany re!
:,:He, samy sambatra.:,:}

{2.
Ho vory ao ny iraky
Ny Tompom-bokatra,
Mitondra amboarany
:,:Am-pihobiana.: ,:}

{3.
Ny isam-pirenena aty
Dia samy misy ao,
Ny teny izay ninoany
:,: Tanteraka izao.:,:}

{4.
Ny feon''ny fisaorany
Ho toy ny riana.,
Ny hira iray iraisany,
:,: Ka samy mirana. :,:}

{5.
Ny lanitry ny lanitra
Dia feno hobiny,
Ny Zanak''ondry masina
:,: No ao anivony.:,:}

{6.
Ry Ray be fitiavana,
Izahay mba taominao,
Mba samy ho tafangona
:,: Ao anatrehanao.:,:
}
', '#1-VII-17', '#1') /end

insert into song values (16741, '674. Ry mpivahiny reraka sy ana!', NULL, NULL, '
{1.
Ry mpivahiny reraka sy ana!
Herezonao ny dia, fa iry
Ny lapa soan''ny fanantenana,
Ka aza mba mamoy fo ety,
Fa vetivety hianao no eto,
Dia tapitra ny alahelonao,
Ka dia mba ononinao ny sento
:,:.Fa mihafohy ny alehanao.:,:}

{2.
Jesosy no mpitsinjo mpivahiny,
Ka Izy dia ta-handray anao;
Ny fony dia toy ny fahiny
Ka vonona hitantana anao.
Ka miezaha hanatratra eny,
Antsoiny hianao izao hoe:
Minoa Ahy araka ny teny,
:,: Ny tranon-dRaiko dia lehibe:,:}

{3.
Mangoraka anao izao ny fony!
Ny tena aman-drany vatsinao.
Ka aza mba mihemotra intsony,
Fa ampy ho anao Jesosinao .
Ka raiso ny finoana ho ampinga,
Ny teny masina ho sabatra,
Ny fahamarinana mba hahakinga,
:,: Ny fitiavana hanandratra.:,:
}
', '#1-VII-17', '#1') /end

insert into song values (16751, '675. Indreo ao an-danitra ny olo-marobe,', NULL, NULL, '
{1.
Indreo ao an-danitra ny olo-
marobe,
Mitsangana manatrika ny
Zanak''ondry re!
Akanjo fotsy lava no anaovany,
Ka sampam-palma fandresena eo
an-tanany.}

{2.
Ka iza moa ireo, ary avy taiza re?
Ireny dia avy tamim-pahoriam-be,
Nanasa sy namotsy ny akanjony
Tao amin-dran''ny Zanakondry,
Izay Mpanavony.}

{3.
Ny tia ny maloto tsy ho ao an-danitra
Fa masina ny tonga any, sady sambatra
Mihira fiderana sy fisaorana
No zavatra ataony amim-pifaliana.}

{4.
Endrey, maniry mafy aho mba ho isany,
Hiaraka hihoby sy hifaly aminy;
Anefa an''ny Tompo ny fotoana,
Ka dia Izy no manendry ny hodiana
}
', '#1-VII-17', '#1') /end

insert into song values (16761, '676. Ny voarain''Andriamanitra ary', NULL, NULL, '
{1.
Ny voarain''Andriamanitra ary,
Ka afaka taty
An-tany be mahory
Dia lasa nody
Fa tsy maty tsy akory.}

{2.
Ny tafafindra monina ao an-danitra ao,
Nankao an-tany vao
Fonenan''ny mazava,
Dia tsy mba maty,
Fa nandao ny mety rava.}

{3.
Ny voasatro-boninahitra tsy lo,
Ka manaram-po
Misotro ranon''aina,
Dia tsy mba maty
fa nandray ny nantenaina.}

{4.
Ny ondrinao izay manaraka Anao
Ho ao am-balanao
Ry ''lay Miandry Tsara
Dia tsy mba maty
re, fa tsara fiafara}

{5.
Ny fo mivantana aminao ''zay nanome
Ny hasambaram-be
Nandramany sahady,
Dia tsy mba maty
fa mpandresy afaka ady.}
', '#1-VII-17', '#1') /end

insert into song values (16771, '677. He! Tazana ao an-danitra', NULL, NULL, '
{1.
He! Tazana ao an-danitra
Vahoaka finaritra
Mitafy fotsy ravo fo,
Ka mirana avokoa.
Ny vazan-tany efatra
No nielezan-kafatra,
Dia tonga izao
Ny ory tao
Ka afaka entana.
Ny ran''Ilay Mpamonjiny
Nahafotsy ny akajony;
Koa dia ireo
Manandra-peo
Ampifaliana.}

{2.
Ho an''Andriamanitra
An-tsezam-boninahitra
Ny famonjena anie!
Hobin''ny marobe,
''lay Zanak''ondry avotra
No nanafahan-tahotra
''zay voaray
Ho zana-dRay
Hobio mandrakizay!
Ka saotra, voninahitra
Ho an''Andriamanitra,
O, mba derao,
Ankalazao,
Haleloia! Amena!}
', '#1-VII-17', '#1') /end

insert into song values (16781, '678. Hajao ny olo-masina', NULL, NULL, '
{1.
Hajao ny olo-masina
Mpanaraka ny Tomponao,
Miely sy manerana
Izao tontolo izao;
Ireo dia tonga ohatra,
Ka tokony harahinao,
Na efa olon-dehibe,
Na zaza hianao ;
Tanteraka ny sasany,
Ka efa nody amin-dRay;
Ny sasany dia mbola aty
Milasy amin-day.}

{2.
Ka betsaka ny isany,
Na aty, na ao an-danitra;
Ny Rainy kosa dia iray,
Andriamanitra;
Ireo maro namanao
Miantso koa maka anao
Hiaraka hiezaka
Hankao an-danitra;
Ka dia miezaha re,
Ka aza kivy hianao;
Jereo ao aloha ao
Ny loka ho anao.
}
', '#1-VII-17', '#1') /end

insert into song values (16791, '679. Moa te-hiara-dia?', NULL, NULL, '
{1.
Moa te-hiara-dia?
Moa te-hiara-dia?
O! ry hava-malala,
Ho ao Kanana izao?
Eny, te-hiara-dia,
Fa antsoin''ilay Mpamonjy,
Ho ao Kanana izao!}

{FIV.
Dera, dera, haleloia!
Dera, dera, haleloia!
An''Andriamanitika,
Izay ao ambony ao.}

{2.
Kristy re no làlantsika,
Vatolampy, arontsika,
Izy no mitondra antsika
Ho ao Kanana izao.
Tano izahay, ry Tompo;
Hatanjaho ny mpanampo,
Ka tsilovy ny haleha
Ho ao Kanana izao!
[FIV.]}

{3.
Tazana ery sahady
Perla re ny vavahady,
Volamena ny tanàna
Ery Kanana ery.
E! hahita ny Mpamanjy,
Ka hifaly fa tra-bonjy,
Sy hidera re ny vonjy
Ery Kanana ery.
[FIV.]
}
', '#1-VII-17', '#1') /end

insert into song values (16801, '680. Aingao ny masontsika', NULL, NULL, '
{1.
Aingao ny masontsika
Hitsinjo tany soa ;
Fa ao ny Tompontsika
Manomana tokoa
Tanàna, trano tsara,
Fianan-tsambatra e!
Izany no anjara
Ho an''ny mino re!}

{2.
Ny andro fifaliana
Ho avy ka antenao ;
Ny andro nihafiana
Ho foana sy handao ;
Ny taona fihobiana
Homen''ny Tomponao ;
Ho feno fifaliana
Izao rehetra izao.}

{3.
Ny andro maharavo
Tsy ho ela intsony izao ;
Hiarina ny lavo
Handray ny tena vao,
Irio ny Tompo Avo
Ho avy hanavao ;
Ho faly izay mahazo
Izao fiainan-tsoa izao.}
', '#1-VII-17', '#1') /end

insert into song values (16811, '681. Iza moa ireo mitsangana', NULL, NULL, '
{1.
Iza moa ireo mitsangana
Ary an-koatra ary ?
Ny seza fiandrianana
No eo anilany,
Haleloia, mihobia!
Fa Mpandresy izy e!
Ka mitana ny rofia,
Voninahi-dehibe.}

{2.
Fahorina mafy loatra
No nentin''izy ireo,
Kanefa tsy nitsoaka
Nijaly fa tsy leo,
Famonjena firenena
Ady masina tokoa,
Nanolorany ny tena
Ho mpitory "Teny Soa".}

{3.
O! ry mponina ao an-danitra
Anjely marobe,
Omeo voninahitra
Ny maritiora e!
Ilay Kanana soa tokoa
No fonenany izao
Hetaheta, sanatria
Tsy haninona azy ao.}

{4.
Mba avia, ry olom-belona
Avia, ankalazao
Ilay maty nefa velona,
Jesosy Tomponao.
Dera, laza atolotray
Haleloia, haleloia!
Ho Anao, Mpanjakanay,
Ho Anao, doria doria.}
', '#1-VII-17', '#1') /end

insert into song values (16821, '682. Ianao, Jehovah Tompo', NULL, NULL, '
{1.
Ianao, Jehovah Tompo
No itokiako
Ka tsy matahotra aho
Fa manana Anao.
Ry Harambato mafy,
Izay ao mandrakizay
Ny foko tsy hanahy,
Fa miankina Aminao.}

{2.
Mitsoka mafy loatra
Ny rivo-doza re,
Kanefa tsy hihoatra
Ny fefy lehibe.
Ry Aron''ny tanora,
Ry Tokin-dehibe
Ho heriko isan''ora
Ny herinao anie.}

{3.
Raha tonga re ny fetra,
Ny andro tapitra,
Haneno ny trompetra,
Hihovitrovitra.
Kanefa matokia,
Ry foko fa Jeso
Hitantana ny dia
Ho eo anilanao.}
', '#1-VII-17', '#1') /end

insert into song values (16831, '683. Ao am-pitan''ilay lohasaha', NULL, NULL, '
{1.
Ao am-pitan''ilay lohasaha
izay
Alo-pahafatesana koa,
Moa mba tsinjonao ve ''ry
tanàna iray
''Zay mamiratra toa vatosoa ?
Ny masoandrony tsy mba
milentika koa
''Zay rehetra mby ao voadio
avokoa
Ka mamiratra toa vatosoa.}

{2.
Ny Mpanjakany dia ''Lay Ray
be fitia
Sady aina ho an''izay ao ;
Ry mpanota mibebaka o,
matokia
Hosasany hadio hianao,
Ny masoandrony ..sns.}

{3.
Nefa tsinjoko fa ny fidirana
ao
Dia am-pototr''ilay lakoroà!
He mideza sy ety ny lala-
mankao
O, ry foko, moa ho kivy va ?
Ny masoandrony ..sns.}

{4.
Olo-masina maro no tsinjoko
''ndreo
Ao mihira sy mitafy vao ;
Iriko indrindra ho isan''ireo
Ka tantano, Jeso aho, ho ao.
Ny masoandrony ..sns.}
', '#1-VII-17', '#1') /end

insert into song values (16841, '684. Ao ny lanitra tsara tokoa', NULL, NULL, '
{1.
Ao ny lanitra tsara tokoa
Izay fonenana mandrakizay,
Tany be hasambarana koa ;
Ao isika hihaona indray ;
Mifalia, mihobia,
Fa ao isika hihaona indray.}

{2.
Ao ny lova izay mamin''ny fo
Sy fiainana mandrakizay,
Voninahitra tsy mba ho lo ;
Ao isika hihaona indray ;
Mifalia, ..sns.}

{3.
Ao ny Ray, Loharanon''ny soa,
Hovantanina rahatrizay,
Izay hampiditra hoe :
mandrosoa
Ao isika hihaona indray ;
Mifalia, ..sns.}

{4.
Ao ny Zanaka be indrafo
Izay nijaly taty fahizay ;
Nefa tsy mba nahita ny lo,
Fa efa velona mandrakizay ;
Mifalia, ..sns.}

{5.
Ny fanahin''ny Ray ao tokoa
Izay mitarika antsika izao
Sy mananatra antsika ho soa,
No mitondra antsika hankao
Mifalia, ..sns.}

{6.
Matokia, miravoa, mifalia,
Fa azontsika ny lapan''ny Ray
Miderà, mihirà, mihobia,
Fiadanana mandrakizay
Mifalia, ..sns.}
', '#1-VII-17', '#1') /end

insert into song values (16851, '685. Matokia, mpivahiny!', NULL, NULL, '
{1.
Matokia, mpivahiny!
Any an-tanàna ary
Misy trano soa madio
Mandrosoa hianao :
Misy fitsaharana any,
Matokia hianao ;
Misy fitsaharana any,
Izay ho azonao.}

{2.
Ao ny Tomponao miandry
Hanome anao ny soa ;
Misy fiadanana any
Voavoatra ho anao :
Misy fitsaharana ..sns.}

{3.
Any tsy mba misy rofy
Amam-panaintainana ;
Izay rehetra tonga any
Afa-pahoriana
Misy fitsaharana ..sns.}

{4.
Tsy mba mamy va ny mody
Mba ho afa-tsatsatra
Sy horaisin''i Jesosy
Amim-piadanana ?
Misy fitsaharana ..sns.}
', '#1-VII-17', '#1') /end

insert into song values (16861, '686. Maniry izahay', NULL, NULL, '
{1.
Maniry izahay
Ho any aminao,
Jerosalema tanànanay,
Tanàna masina;
Ny trano lay aty tsy maintsy rovitra
Kanefa tazanay ery
''Lay tsara lavitra;}

{FIV.
Tazanay, tazanay
Ny tany masina}

{2.
Mba tanterahonao,
Ry Tompo tia anay!
Ny fikasan''ny Zanakao
Nataony taminay:
"Ho any amiko
Izay nomenao Ahy,
Fa tiako ho hitany
Ny voninahitro"
[FIV.]}

{3.
Mankany aminao
Izao ny dianay,
Na sarotra ny làlana
Mihira izahay;
Ny hasasaranay
Tsy misy intsony izao,
Fa tazanay sahady re
Ny tany masina:
[FIV.]}
', '#1-VII-17', '#1') /end

insert into song values (16862, '686. Maniry izahay (Zafindraony)', NULL, NULL, '
{1.
Maniry izahay
Ho any aminao,
Jerosalema tanànanay,
Tanàna masina;}

{FIV.
Tazanay, tazanay
Ny tany masina
Jerosalema tanànanay,
Tanàna masina;}

{2.
Ny trano lay aty
Tsy maintsy rovitra
Kanefa tazanay ery
''Lay tsara lavitra;
[FIV.]}

{3.
Mba tanterahonao,
Ry Tompo tia anay!
Ny fikasan''ny Zanakao
Nataony taminay:
[FIV.]}

{4.
"Ho any amiko
Izay nomenao Ahy,
Fa tiako ho hitany
Ny voninahitro"
[FIV.]}

{5.
Mankany aminao
Izao ny dianay,
Na sarotra ny làlana
Mihira izahay;
[FIV.]}

{6.
Ny hasasaranay
Tsy misy intsony izao,
Fa tazanay sahady re
Ny tany masina:
[FIV.]}
', '#1-VII-17', '#1') /end

insert into song values (16871, '687. Ny tranon''ny Raiko, trano', NULL, NULL, '
{1.
Ny tranon''ny Raiko, trano
madio,
Tsy misy mahory na zava-
manjo ;
Avia, ry malahelo, hankao,
Mizora, fa Izy hampiditra
anao!
:,: Sambatra re izay tonga
ao :,:
:,: Faly tokoa :,:
Izay tafiditra ao.}

{2.
Mandalo izao fiainana izao,
Ka aza sondriana aty hianao ;
Ny ratsy izay mitaona reseo,
Ny any an-danitra no mba
irio!
:,: Sambatra re, ..sns :,:}

{3.
He, ao no ho hitantsika tokoa
Ny tavan''Ilay Zanakondry
madio ;
Ny mpino tety no efa ao koa,
Maniry hihaona amintsika
tokoa,
:,: Sambatra re, ..sns :,:}

{4.
Tsy misy manjo izay afa-
mankao,
Na andro mahory hitseran''ny
fo ;
Fa any tsy misy alina koa,
Tanàna mazava, fonenana
soa,
:,: Sambatra re, ..sns :,:}

{5.
Ny mino rehetra ory tety
Tafakatra any ka faly tokoa,
Mihira ny famonjen''i Jeso,
Midera ny fiandrianan''ny Ray.
:,: Sambatra re, ..sns :,:}
', '#1-VII-17', '#1') /end

insert into song values (16881, '688. Misy tany mahafaly,', NULL, NULL, '
{1.
Misy tany mahafaly,
Tany izay alehanay,
Tany misy an''i Jeso,
Jeso Tomponay!
Tany soa maharavo,
Tany tadiavinay!
Afa-tahotra, afa-doza
Izay tafiditra ao.}

{2.
Tany tia mpivahiny,
Tany namboarinao,
Ry Jesosy be fitia,
Jeso Tomponay!
Ho anay aty an-tany
Izay mba malahelo fo
Noho ny fomban-tany ratsy
Mahazatra anay.}

{3.
Maro be ny fahavalo
Izay miady aminay ;
Mafy ny fakam-panahy
Hitanay aty ;
Sakan-dàlana, ady mafy
No alehanay izao :
Be tokoa ny mampahory,
Nefa fantatrao.}

{4.
Eny, fantatrao, ry Jeso
Fa izahay mpanomponao
Dia handresy ny devoly
Noho ny herinao ;
Koa ampionao izahay re
Mba hiankina aminao
Hahatraranay ny tany
Namboarinao.}
', '#1-VII-17', '#1') /end

insert into song values (16891, '689. Fandalovana aty,', NULL, NULL, '
{1.
Fandalovana aty,
Lanitra honenako ;
Efitra izao tany izao,
Lanitra honenako ;
Voahodidin-doza aty,
Reraka ao an-dàlana,
Valaka sy sasatra ;
Lanitra honenako.}

{2.
Na mandrivotra aza aty,
Lanitra honenako ;
Tany no hilaozako,
Lanitra honenako ;
Alon-dranomasina
Sy ny rian-drano be
Tsy hisakan-dàlana ;
Lanitra honenako.}

{3.
Ao an-tratran''i Jeso,
Lanitra honenako ;
Afa-pahoriana
Lanitra honenako ;
Ao ny olo-masina
Izay mba tiako taty,
Faly raha hitako ;
Lanitra honenako.}

{4.
Dia tsy halahelo aho,
Lanitra honenako ;
Mafy ny manjo aty,
Lanitra honenako ;
Ao no hitoerako
Faly anatrehan-dRay,
Ao homeny toerana ;
Lanitra honenako.}
', '#1-VII-17', '#1') /end

insert into song values (16901, '690. He! misy re fonenana ;', NULL, NULL, '
{1.
He! misy re fonenana ;
:,: Tsara e! :,:
Kanefa tsy mba lavitra ;
:,: Tsara e! :,:
Ny trano ao voavoatra ;
Ho an''izay mangataka ;
Fa Jesosy no Tompony,
:,: Tsara e! :,:}

{2.
Tsy misy fahoriana ao ;
:,: Tsara e! :,:
Tsy misy alahelo ao ;
:,: Tsara e! :,:
Ny ota tsy tafiditra ao,
Ny devoly tsy monina ao,
Fa ny anjely masina ;
:,: Tsara e! :,:}

{3.
Ny Tompo dia miantso anao ;
:,: Tsara e! :,:
Ny teniny henoinao ;
:,: Tsara e! :,:
" Avia aty ry zanako!
Ka mibebaha re anio,
Tsy misy ''zay holaviko "
:,: Tsara e! :,:}

{4.
Efa azonay ny santatra ;
:,: Tsara e! :,:
Voavelany ny heloka ;
:,: Tsara e! :,:
Ho any faingana izahay,
Fa Kristy manome anay
Fiainana mandrakizay ;
:,: Tsara e! :,:}
', '#1-VII-17', '#1') /end

insert into song values (16911, '691. Ny lapan''Andriamanitra', NULL, NULL, '
{1.
Ny lapan''Andriamanitra
No ivorian''olona
Tsy omby alinalina,
Izay madio sy masina.}

{2.
Izao firenen-drehetra izao
Dia samy afaka hankao ;
Tsy fomban''Andriamanitra
Hizaha tavan''olona.}

{3.
Ireny no naharitra
Ny hazo fijaliana,
Na dia nanao ny henatra
Ho zava-tsinontsinona.}

{4.
Endrey, ny hafaliany!
Mazava ny tarehiny ;
Fafàn''Andriamanitra
Ny ranomaso latsaka.}

{5.
Ataony ao ny hira vao
Hoe : " Voavono Ianao ;
Ny ranao, Zanakondrinay!
No nanavotanao anay "}

{6.
Ry Jeso o Mpanavotra!
Izaho dia milatsaka
Ho isan''ny mpanomponao
Manaraka ny dianao.}
', '#1-VII-17', '#1') /end

insert into song values (16921, '692. He! misy tany soa', NULL, NULL, '
{1.
He! misy tany soa
Izay tsy aty ;
Maro ny olona
Faly ary ;
Tsara fonenana
Sambatra sy tretrika
Sady finaritra
Izay mponina ao.}

{2.
Aza sondriana
Foana aty ;
O! miomàna re
Mba ho ary ;
Be fiadanana,
Tsara fitoerana,
Be haravoana
Izay mponina ao.}

{3.
Mafy izay manjo
Hita aty,
Ka misikìna re
Mba ho ary ;
Jeso mitandrina,
Sy mitahy fatratra
Ary mahenina
Izay mponina ao.}

{4.
Aza variana
Foana aty ;
O! miaingà izao
Mba ho ary ;
Jeso Sakaizanao
No Mpitari-dalana,
Ka dia hitondra anao
Ho tonga ao.}
', '#1-VII-17', '#1') /end

insert into song values (16931, '693. Ao ambony ny lapan''ny Ray,', NULL, NULL, '
{1.
Ao ambony ny lapan''ny Ray,
Ao an-danitra feno ny soa,
Hasambarana mandrakizay,
Kristy soa no iriko tokoa,
:,: Kristy soa, Kristy soa
No iriko ho ahy tokoa :,:}

{2.
Misy tsara ihany aty,
Mba ho santatra eto izao ;
Fa tsy mety ho toy ny ary,
Ka iriko tokoa mba ho ao,
:,: Kristy soa, ..sns.}

{3.
Ny voavotra ravo izao,
Afaka ota ka faly tokoa,
Afa-paty izay tonga ao,
Afa-doza, mahazo ny soa,
:,: Kristy soa, ..sns.}

{4.
Tsy ny lanitra be zava-tsoa,
Fa ny Tompo malalaka ao ;
Tsy mba tahotro helo tokoa,
Fa ny Tompo tsy foiko izao.
:,: Kristy soa, ..sns.}
', '#1-VII-17', '#1') /end

insert into song values (16941, '694. Enga ka hanana elatra soa', NULL, NULL, '
{1.
Enga ka hanana elatra soa
:,: Aho ety :,:
Mba hanidinako avo tokoa
:,: Hody ery! :,:
Fo malahelo ny ratsy natao
No ela-tsoa hitondra hankao ;
Hanana elatra aho anie,
Mba hanidinako ao!}

{2.
Trano mamiratra tsara iry,
:,: Raiko no ao :,:
Na nahabe ahy aza ity,
:,: Mamiko ao :,:
Havako maro no sambatra ao,
Faly ny foko hankeny izao ;
Enga ka hanana elatra aho,
Mba hanidinako ao!}

{3.
Na dia an-danitra aza ery,
:,: Lavitra va ? :,:
Raha tarihin''i Jeso ety,
:,: Ketraka va ? :,:
Tsia, fa hiainga tokoa aho
izao!
Indro akaiky ny lala-mankao ;
Enga ka hanana elatra soa,
Mba hanidinako ao!}

{4.
Andriamanitra Ray no ery,
:,: Tsara tokoa :,:
Jesosy koa ''Lay Mpamonjy
tety,
:,: Zanaka soa :,:
Ao ny Fanahy Masina re!
Tapitra ao avokoa ny soa ;
Hanana elatra aho anie,
Mba hanidinako ao!}
', '#1-VII-17', '#1') /end

insert into song values (16951, '695. Ary an-koatra ary', NULL, NULL, '
{1.
Ary an-koatra ary
Ilay Paradisa soa
Toa takona aty,
Kanefa ao tokoa,
Mamirapiratra ao,
Ny endriky ny Ray ;
Jesosy Tompo ao
Miandry mba handray.}

{2.
Tsy misy ny hanjo
Izay tafiditra ao ;
Ny ranomaso koa
Tsy afaka hankao
Ny hazavana soa
Manelatrelatra ao ;
Ny zava-mahasoa
Tsy misy izay tsy ao.}

{3.
Anjely marobe
Mihira any koa ;
Ireo olo-masina e
Mifaly avokoa.
Andeha re izao,
Sakaiza marobe,
Ario ny tahotrao,
Ka dia mizora re!}

{4.
Raha misy loza ety,
Jereo ny Tomponao ;
Fa izy raha tety
Nitondra toy izao.
Ireo fahoriana aty
Odio tsy hita re,
Handraisantsika ary
Ny fiadanam-be.}
', '#1-VII-17', '#1') /end

insert into song values (16961, '696. Raha ho faty aho (Maintimolaly)', NULL, NULL, '
(Maintimolaly)

{1.
Raha ho faty aho
Ka handao ny havako
Ary dia hisaonany,
Fa handeha hifaly aho ;
Raha afaka ny aiko,
Dia ho finaritra aho.}

{2.
He! antsoiny aho,
Ho any an-danitra,
Dia faly ny foko,
Sy ravo ny kiboko,
Fa azoko ny iriko
Sy nantenaiko ela.}

{3.
Havelako ny tany,
Fa efa azoko tokoa
Ny fifaliam-be ;
Lovako ny lanitra,
Dia tsy matahotra
Ny hafatesana aho.}
', '#1-VII-17', '#1') /end

insert into song values (16971, '697. Any ambony fonenana tsara', NULL, NULL, '
{1.
Any ambony fonenana tsara
Dia tsy mba misy itseran''ny
fo ;
Eny iriko ho tsara miafara
Hiara-manjaka aminao ry
Jeso!
Isaoranay ny Tomponay,
Raha ''mby any dia faly
tokoa ;
Isaoranay ny Anaranao soa!}

{2.
Rehefa tonga ilay andro
malaza
Izay hanatrehana Anao, ry
Jeso!
Izay no heveriko ho tena
laza :
Dia ny hidera Anao,
Tompo o!
Isaoranay ny Tomponay,
Raha ''mby any tsy mba ory
koa ;
Isaoranay ny Anaranao soa!}

{4.
Ao izany havana tena malala
''Zay mahafaly fa hita indray,
Nefa izay hampifaly tokoa
Dia ny mihaona aminao ry
Jeso!
Isaoranay ny Tomponay,
Raha ''mby any tsy misy
manjo ;
Isaoranay ny Anaranao soa!}
', '#1-VII-17', '#1') /end

insert into song values (16981, '698. Iza re ireto avy,', NULL, NULL, '
{1.
Iza re ireto avy,
Voatafy lamba soa,
Samy voasatroka avy
Satro-bolamena koa ?
Avy taiza izy ireo,
Ka ho aiza izy ireo ?}

{2.
Izy ireo dia voavonjy,
Voavela heloka ;
Samy nino an''i Kristy
Fony mbola velona ;
Be ny ratsy nentiny,
Fa izao dia sambatra.}

{3.
Maro be ny fahavalo
Niadiany taty,
Fa tsy mba misy izany
intsony ;
Afaka ny sarotra,
Afa-pahoriana,
He, ny hafaliany!}

{4.
Miramirana tarehy,
Indreo, avy izy ireo
Mba hihoby an''i Kristy,
Tompo sy Mpanjaka koa,
Fa nahazo zava-tsoa,
Ka dia tretrika tokoa.}
', '#1-VII-17', '#1') /end

insert into song values (16991, '699. Midera Anao ny olo-masina', NULL, NULL, '
{1.
Midera Anao ny olo-masina
Noho ny fandresena
nomenao ;
Ry Jeso o! Mpitari-dalana!
Haleloia! Haleloia!}

{2.
Ory sy lao, fadiranovana,
Natao ho zava-tsinontsinona
Nefa ireo no tsy nivadi-po ;
Haleloia! Haleloia!}

{3.
Mafy ny ady aman-tafika,
Izay nihatra tao an-dàlana,
Fa Ianao no nitokiany ;
Haleloia! Haleloia!}

{4.
Indro! mpandresy no idirany
Ao afovoan''ny ao an-danitra,
Handray ny satro-boninahitra ;
Haleloia! Haleloia!}

{5.
" Mpanompo tsara sady
mahasoa,
Midira am-pifalian''ny
Tomponao "
He, sambatra izy, fa efa
voaray ;
Haleloia! Haleloia!}

{6.
Aoka ireo no hotahafinay
Amin''ny ady iadiana,
Hiraisam-boninahitra aminy :
Haleloia! Haleloia!}

{7.
Ankehitrio, Anao ny fiderana,
Ry Ray sy Zanaka, Fanahy koa
Eran''ny tany aman-danitra :
Haleloia! Haleloia!}
', '#1-VII-17', '#1') /end

insert into song values (17001, '700. Ao ny andro lehibe', NULL, NULL, '
{1.
Ao ny andro lehibe
Misy fifaliam-be,
Dia ny andro hahatongavana
Amin''ny fonenana
Feno fiadanana.
Tsy mba misy ota na heloka,
Eny, tena andro malaza tokoa
izay.
Jeso Ilay nandresy ny fasana,
Izy no hitantana
Ao am-pahafatesana,
Ka ho mora ny handehanana.}

{2.
Ao ny andro lehibe
Misy fifaliam-be,
Izay hihaonana any an-danitra,
Afa-pahoriana,
Afa-pijaliana,
Sambatra tokoa sy tretrika.
Enga ka ho ao avokoa isika
izao
Ka tsy hisy very na dia iray!
Tava miramirana,
Endrika mamiratra
No hananantsika mandrakizay.}

{3.
Ao ny andro lehibe
Misy fifaliam-be,
Hahitana an''Andriamanitra
Sy Jesosy Zanany
Mbamin''ny Fanahiny.
Hoby no hameno ny lanitra.
Serafima sy arkanjely no
namana
Mankalaza ilay feno indrafo.
Ny fitomaniana
Sy ny fisentoana
Tapitra, fa afaka ny manjo.}
', '#1-VII-17', '#1') /end

insert into song values (17011, '701. Ny hiran''ny lanitra', NULL, NULL, '
{1.
Ny hiran''ny lanitra
Tsara tokoa sy mahafinaritra re!
Henoy ''zany feon''ny olona maro
''zay faly tokoa mihira hoe:}

{FIV.

(feo 1)
Haleloia, Amen!
Haleloia, Amen!
Haleloia, Amen!
Haleloia, Amen!

(feo 2)
Ny dera aman-daza sy hery, anie,
Ho Anao, ry Zanak''ondry
Ny dera aman-daza sy hery, anie,
Ho Anao, Anao, Amen!}

{2.
Toy ny feon''ny rano mahery
Ny hira ataon''ny tra-bonjy ary;
Manerana ny lanitra ny fiderany,
Fa vita ny adiny mafy taty.
[FIV.]}

{3.
Tsy mety mangina ny hiran''ny lanitra
Velona mandrakizay!
Endrey ny fihaonana any an-danitra
Sy ny fiainana mandrakizay.
[FIV.]}
', '#1-VII-17', '#1') /end

insert into song values (17021, '702. An-danitra, an-danitra', NULL, NULL, '
{1.
An-danitra, an-danitra
Tempolin''i Jehovah,
No hahitàna Azy, re
Am-boninahitra!
:,: Ka masina i Jehovah :,:
No hohiraina ao.}

{2.
An-danitra, an-danitra
No mba hamiratra
Ny voninahi-dehibe
Hananantsika e!
:,: Raha tonga
ny fihaonana :,:
Ao amin''ny Jehovah.}

{3.
An-danitra, an-danitra
No ho finaritra
Ny tena izay nijaly be
Am-pahoriana
:,: Fa any no hifaly e :,:
Fahasambarana.}

{4.
Ny fo sy ny fanahy e,
No faly anie,
Fa afaka ny entana,
Izay nivesarany ;
:,: Ka fitsaharan-dehibe :,:
Omen''i Jehovah..}

{5.
Tsy hain''ny lela anie
Hanonona izao
Ny soa antenaina e,
Raha Anatrehany,
:,: Manenika ny lanitra :,:
Ny fitiavany.}

{6.
Ry Jeso Kristy Tomponay
Tariho izahay,
Ka mamelà ny helokay,
Mba hitokianay,
:,: Fa tsy ho menatra
izahay :,:
Fa ao i Jehovah.}

{7.
Raha avy ny hodianay
Ho anatrehany
Tariho izahay,
Mba ento, Jeso, izahay
Hankany Aminy!
:,: Mba ho anay
mandrakizay :,:
Ny fitsaharana!}
', '#1-VII-17', '#1') /end

insert into song values (17031, '703. Ry Jehovah, Ray Tsitoha', NULL, NULL, '
{1.
Ry Jehovah, Ray Tsitoha
Vory eto izahay,
Ray hatramin''ny taloha,
Mihainoa ny vavakay,
:,: Mitsangàna :,:
Eto afovoanay.}

{2.
Andro fampakaram-bady,
Fifaliam-be izao ;
Ianao no mampivady
Raha voatendrinao ;
:,: Vady hendry :,:
Lova avy aminao.}

{3.
Ry Jehovah Ray malala!
Mba tezao ireo ho soa,
Samy tsy hisalasala,
Fa hifankatia tokoa ;
:,: Hamasino :,:
Mba ho olonao ireo.}

{4.
Ho fanahy mifandanja
Sady tsy ho toy ny roa,
Ho mpivady mifanaja
Sady mifamaly soa ;
:,: Akambano :,:
Mba ho fo iray tokoa.}
', '#1-VIII-1', '#1') /end

insert into song values (17041, '704. Ry olon-kanao fanekena', NULL, NULL, '
{1.
Ry olon-kanao fanekena
masina,
Misaora ny Tompo, fa ny
sitrapony
No nampikambana
Amim-pitiavana ;
Ka dia tsy olona roa intsony,
Ny tany Edena tsahivina
indray.}

{2.
Ny fahamarinan''
Andriamanitra
No haingo manendrika ny
tokantrano
Ho toy ny lanitra
Mamirapiratra.
Ry mpivady marina, Kristy no
tano
Handamina ny tokantrano
vaovao.}

{3.
Ny fahasoavan''Andriamanitra
No andry manohana ny
tokantrano
Ka mampisandratra
Ny te ho sambatra,
Izay ankohonanareo
atsangano
Hidera ny famindrampo
lehibe.}

{4.
Ny fanaky ny tokantrano
masina,
Manoatra be noho ny
volamena ;
Ny fivavahana
Sy ny finoana,
Fisaorana sy fiderana homena
Marain-tsy hariva ny
Mpamonjy e!}

{5.
Jehovah Mpitana ny
fanekena o!
Tahio ny toky nomeny ho
mafy,
Ho masina sy to,
Ho marina am-po ;
Ka izy handresy izay
ta-handrafy ;
Ho azy ny lova ho
mandrakizay.}
', '#1-VIII-1', '#1') /end

insert into song values (17051, '705. Tompo Zanaharinay!', NULL, NULL, '
{1.
Tompo Zanaharinay!
Jereo ny fivorianay,
Izao fampakaram-bady izao
Tahio sy hamasinonao.}

{2.
Ireo zatovo roa ireo
Manketo anatrehanao,
Fa mifanaiky izy izao
Hivady sy hifanasoa.}

{3.
Mangataka aminao izahay
Hitahy ireo havanay,
Raha ory mba ononinao
Raha faly, aza ilaozanao}

{4.
Arovinao ny tenany,
Tsinjovy ny taranany ;
Ho-tsara fara anie ireo,
Ho ravoravo toy izao.}

{5.
Ny fifalianay anio
Hitohy hatrary anie!
Ity andro lehibe ity
Ho santatry ny soa ary.}
', '#1-VIII-1', '#1') /end

insert into song values (17061, '706. Izao manatrika Anao,', NULL, NULL, '
{1.
Izao manatrika Anao,
Ry Jesosy, izahay,
Indro tokantrano vao
No hoarina indray.
Izahay mangataka ;
Ny tsodranonao omeo,
Ka avia hanapaka
Ny fiainan''izy ireo.}

{2.
Ho Kristiana velona
Anie ny zanany,
Ho vavolombelona
Fa Anao ny tenany.
Aoka mba hamiratra
Ny filazantsaranao,
Velona hanaraka
Ny fonay ka hazavao.}

{3.
Ka ny fiangonana
Hanompoanay Anao,
Hanana olom-banona
Faly ny hiasa ao.
Antenainay marina,
Fa Ianao hanohana
Izay firaisa-masina,
Hisy fahasoavana.}
', '#1-VIII-1', '#1') /end

insert into song values (17071, '707. Ry Jesosy be fitia!', NULL, NULL, '
{1.
Ry Jesosy be fitia!
Manatreha, manampia,
Ny mpanomponao izao
Izay mivavaka aminao,
Fifaliana madio
No omena anay anio.}

{2.
Indreo ny havanay jereo,
Mba ho soa anie ireo!
Tanan-droa mifandray,
Nofo roa, tonga iray
Mifaniry soa tokoa,
Mifanampy, mifanoa.}

{3.
Ilay tso-drano hatrizay
No omeo ireo havanay ;
Mba ho marofara anie,
Hanam-panambinana,
Feno fiadanana
Noho ny fitahianao}

{4.
Raha ory mba vangio,
Mitaraina mba valio ;
Mitoera aminy
Ao an-tokantranony
Hamasino, hafanao
Ny fitiavany Anao.}
', '#1-VIII-1', '#1') /end

insert into song values (17081, '708. Ry Jehovah Ray kiady', NULL, NULL, '
{1.
Ry Jehovah Ray kiady
Sy mpikarakara anay,
Ka nitahy ny mpivady
Tao Edena fahizay,
Indro misy tonga koa
Eto anatrehanao,
Manam-pikasana soa
Ho mpivady hatr''izao.}

{2.
Mba tahio, ry Ray mpiahy,
Ny firaisany izao,
Hifanaraka am-panahy
Sy hiankina aminao.
Ny tsodranonao fahiny
Angatahinay indray,
Mba hatao ankehitriny
Hahato ny hatakay!}

{3.
Raha omenao zaza izy,
Dia mba ampio re,
Hitaizany ny ankizy
Amim-pahendrena be ;
Aza avela ho irery
Raha tratry ny manjo,
Fa tafionao ny hery
Mahatanjaka ny fo.}

{4.
Raha ao ny andro tony
Mila haharenoka,
Dia taironao ny fony,
Ry Mpamela heloka.
Raha tapitra ny taona
Efa voafetranao,
Enga ka ho tafahaona
Ao an-danitra aminao.}
', '#1-VIII-1', '#1') /end

insert into song values (17091, '709. Jehovah o, mba mihainoa!', NULL, NULL, '
{1.
Jehovah o, mba mihainoa!
Indreo misy olon-droa izao
Izay manam-pikasana soa
Hanorin-tokantrano vao :
O! tahio, O! tahio,
Mba ho tokantrano soa
madio
O! tahio, O! tahio,
Mba ho tokantrano soa!}

{2.
Dia angatahinay, ry Ray
Ny fitahianao ho azy ireo
Ho nofo sy fanahy iray
Ka hifanaja hatrizao.
O! tahio, ..sns.}

{3.
Fenoy ny fiadananao
Ny tokantranon''izy ireo
Ka mba hamirapiratra ao
Ny fivavahana Aminao.
O! tahio, ..sns.}

{4.
Tahio ireo, ry Tompo soa,
Mba ho mpivady marina,
Hitana sy hanaja koa
Ny fanekena masina.
O! tahio, ..sns.}

{5.
Tahio, hifampitantana
Mandritra ny fiainany,
Ka dia ny fahafatesana
No fetra isarahany.
O! tahio, ..sns.}

{6.
Ho maro fara anie ireo
Ka mba hitaiza ho Anao
Ireo solofo ho tia Anao,
Hanompo sy hanaja Anao.
O! tahio, ..sns.}
', '#1-VIII-1', '#1') /end

insert into song values (17101, '710. Nakambanao, ry Tompo o!', NULL, NULL, '
{1.
Nakambanao, ry Tompo o!
Adama sy Eva nankato
Ny fampakaram-bady,
Tahio sy vimbinonao
Ireto anatrehanao
Manaiky ho mpivady,
Hahay handray
Ny anjara izay hihatra
Izy roa
Ary hifanisy soa.}

{2.
Dia nifanaiky izy izao ;
Ry Tompo o! vimbinonao
Mba ho iray tokoa ;
Ny fony mba tarihonao
Hanaraka ny didinao
Nomenao azy roa ;
Izao handao
Ray sy reny, ka ny teny
Nolazainy
Enga anie ka tsy hovany.}
', '#1-VIII-1', '#1') /end

insert into song values (17111, '711. Tompo o, endrey ny fitahiana', NULL, NULL, '
{1.
Tompo o, endrey ny fitahiana
Izay natondrakao ny zanakao
Vory eto, feno fifaliana
Ampoky ny fahasoavanao.
Izay rehetra nikelezan''aina
Dia nombàn''ny fitantananao
Vonona avokoa izay nilaina
Tsy mba lany ny harenanao.}

{2.
Indro trano efa novitaina
Hanasoavana taranaka
Hitaizàna, hanolokoloana
Ireo tanora halain-tahaka,
Izay voafafy, antenaina
Mba hamokatra tsy tapaka ;
Dia ho vanona ny firenena
Ka hitombo, hisandrahaka.}

{3.
Izany re, ry Tompo, no
finoana
Tananay, ka mba herezonao,
Izahay handroso tsy hivoana,
Eo ambany fiarovanao,
Ka ny fangataham-pitahiana
Tompo o! akarina Aminao
Ny Anaranao no iaingàna
Hoderaina ny Anaranao.}
', '#1-VIII-2', '#1') /end

insert into song values (17121, '712. Raha tsy Jehovah', NULL, NULL, '
{1.
Raha tsy Jehovah
No manao ny trano,
Dia miasa foana
Ny mpanorina.
Raha tsy Jehovah
No miambina àzy,
Miari-tory foana
Ny mpiambina.}

{2.
Nahavita trano
Ny mpianakavy,
Ka mifaly, ravo
Nahatsangana
Nefa koa tsaroana
Fa Jehovah Tompo,
Feno fitahiana,
No nanambina.}

{3.
Mankasitraka aho,
Ary koa misaotra
An''Ilay Nahary
Be fitiavana.
Izay rehetra ahy
Dia nomen''ny Ray ;
Eny, ity trano
Dia avy taminy.}

{4.
Tompo o! tahio
Izay miditra ato ;
Dia arovy koa
Izay mivoaka;
Mba monena ato,
Miaraka aminay
Anjakao ny trano,
Hiadananay.}
', '#1-VIII-2', '#1') /end

insert into song values (17131, '713. Miposaka indray ny', NULL, NULL, '
{1.
Miposaka indray ny
masoandro,
Ny ali-maizim-be dia tonga
andro ;
Ry foko, mba derao ny Ray
Tsitoha,
Ka dia andrandrao,
Derao, ankalazao
Ny Rainao soa.}

{2.
Ny loza marobe aty an-tany
Dia niarovanao anay izany,
Ka zava-mahafaly no omeny,
Dia fiarovana
Sy fitiavana
Tsy hita lany.}

{3.
Ry Masoandronay, mba
mitariha
Ny dianay anio mba tsy
hania ;
Mahery Ianao ka maharara
Ny fahavalonay
Tsy hahataona anay
Handà ny tsara.}

{4.
Ny zavatra manjo tsy
mampanahy
Napetraka aminao ny ahiahy,
Ka dia matoky aho, ry
Tsitoha!
Omeo ny zanakao
Izay heverinao
Ho mahasoa.}
', '#1-VIII-3', '#1') /end

insert into song values (17141, '714. Jehovah Zanahary,', NULL, NULL, '
{1.
Jehovah Zanahary,
Izahay miderà Anao ;
Ny alina nanjary
Ho firaketanao.
Maraina izao ny andro,
Ka lao ny maizina ;
Savao, ry Masoandro,
Izay fahoriana.}

{2.
Ry Kintan'' ny maraina,
Jesosy Havanay,
Arovinao ny aina,
Tahio izahay ;
Ny asanay anio
Mba hasoavinao,
Ny dianay tariho
Ho any aminao.}
', '#1-VIII-3', '#1') /end

insert into song values (17151, '715. Tonga ny maraina,', NULL, NULL, '
{1.
Tonga ny maraina,
Dimbin''alina,
Ka ny Tompon''aina
No isaorana.}

{2.
Raha mbola nandry
Aho Tompo o,
Ianao Mpiandry
Dia aroko.}

{3.
Tano sy ampio
Ny madinikao,
Sao hanota anio,
Ka handao Anao.}

{4.
Aoka ny anjara
Fanompoako
Mba ho vita tsara
Ka ho sitrakao.}

{5.
Ary raha lany
Ny fotoako,
Izaho hody any
Anatrehanao.}
', '#1-VIII-3', '#1') /end

insert into song values (17161, '716. Ry Tompo o, tahio', NULL, NULL, '
{Ry Tompo o, tahio
Izao dianay izao ;
Ny lehibe ampio,
Ny kely mba tezao.
Fa ny Anaranao
No iaingana anio ;
Tantano sy vimbino
An-dalana izahay.}
', '#1-VIII-3', '#1') /end

insert into song values (17171, '717. Andeha raha maraina,', NULL, NULL, '
{1.
Andeha raha maraina,
Raha antoandro koa,
Andeha raha hariva
Hivavaka tokoa ;
Fo marina no ento,
Ario ny tahotrao,
Miondreha ao an-trano,
Ka mivavaha ao.}

{2.
Tsarovy ny sakaiza,
Ny havan-tianao ;
Raha misy fahavalo,
Mifona ho azy koa,
Ka dia mangataha
Ny soa ho anao,
Jesosy Ilay Mpamonjy,
Alao ho tokinao.}

{3.
Na tsy irery aza,
Fa be ny olona ao,
Ka misy hevi-tsara
Miposaka ao am-po,
Ny hataka mangina
Voaeritreritrao
Ho ren''ny Rainao tia,
Jehovah Tomponao.}

{4.
Mba manatona Azy
Raha azon''ny manjo ;
Tsarovy koa raha faly
Hisaotra ao am-po ;
Tsy misy hafaliana
''Zay mamy toy izao ;
Ny fahazoantsika
Mivavaka aminy.}
', '#1-VIII-3', '#1') /end

insert into song values (17181, '718. He torimaso mamy', NULL, NULL, '
{1.
He torimaso mamy
No atolory ahy,
Ry Raiko Mpiandany,
Sy herin''ny fanahy.
Ny sandrinao mahery
No hiampofo ahy ;
Ny masonao mijery,
Manala ahiahy.}

{2.
Ny otako androany
Koseho, Raiko o!
Ireo ory mitomany
Ampaherezonao,
Ny havako rehetra
Sy ny sakaiza koa,
Tahio tsy hanjoretra
Ampifalio tokoa.}

{3.
Ireo sahiran-tsaina
Sy mandry fotsy izao
Ny kivy, mitaraina
Vonjeo, ry Tompo o!
Ireo marary maro
Sy ny mpitsabo koa,
Tahio, ry Mpiaro,
Arovy tsy ho voa.}

{4.
Ianao no tokinay
Ka dia hatory izao
Ireto nofonay
Am-pelatananao.
Sakano re ny ratsy
Mba tsy hamely izao
Ry Jeso Mpiambina ahy
Sy voninahitra o!}
', '#1-VIII-4', '#1') /end

insert into song values (17191, '719. Efa lasan-davitra,', NULL, NULL, '
{1.
Efa lasan-davitra,
Ka hariva izao ny andro ;
Ianao no Masoandro,
Ry Andriamanitra,
Mba arovinao ny aiko,
Fa atolotro Anao ;
Ampidiro ao am-poko
Izao ny fiadananao.}

{2.
Raiso ny fisaorako,
Ka tondraho ranon''aina
Ny fanahy aman-tsaina,
Mba ho velona izao,
Dia hamoa voa tsara
Amiko ny teninao,
Ka ho tiako hambara
Ny fahasoavanao.}

{3.
Ry Andriamanitra o!
Hitanao ny fahotako,
Fantatrao ny fahosako ;
Mamindrà fo amiko ;
Izaho tsy misalasala
Mba hanatona Anao,
Fa ny Zanakao malala
Maty noho ny otako.}

{4.
Izaho ta-hatory izao,
Fa mba sasatra niasa ;
Tsy heveriko ny lasa,
Fa apetraka aminao
Aoka aho mba hahazo
Fitsaharana izao ;
Raha maraina mba fohazy,
Dia hankalaza Anao.}

{5.
Kintan''i Jakoba o,
Sady Masoandron''aina,
Hazavao ny fo sy saina,
Mba tsy hatahorako
Ny mpanimba sy mpandoza,
Liona mierona,
Izay minia hampidi-doza
Andro aman''alina.}

{6.
Arosoy ny teninao,
Hazavao ny firenena,
Mba ho samy hanantena
Famonjena aminao ;
Ka ny manam-pahefana
Aoka hotarihinao,
Mba hitondra fanjakana
Araka ny sitrakao.}

{7.
Fantatrao, Jesosy o,
Izay rehetra mitaraina
Ka miandry ny maraina
Sy ny fahasitranana ;
Ny kamboty sy mahantra
Aoka hotsarovanao ;
Ry Sakaizanay Mpiantra,
Iantrao ny olonao.}
', '#1-VIII-4', '#1') /end

insert into song values (17201, '720. Lany indray ny andro ;', NULL, NULL, '
{1.
Lany indray ny andro ;
Ianao ry Ray,
No ho masoandro
Sy ampinganay.}

{2.
Be ny zava-tsoa
Azo taminao ;
Saotra aman-dera
Entinay izao.}

{3.
Andronay mandalo
Toy ny rivotra ;
Be ny fahavalo
Mampitahotra.}

{4.
Jeso o, Mpiandry,
Izaho ondrinao,
Aoka re mba handry
Eo an-tratranao.}

{5.
Mpivahiny ihany
Izahay aty,
Ka mba te-ho any
Aminao ary.}
', '#1-VIII-4', '#1') /end

insert into song values (17211, '721. Ny andro efa lany', NULL, NULL, '
{1.
Ny andro efa lany
Mangina izao ny tany,
Miala sasatra ;
Heveronao, ry saina,
Fa mendri-koderaina
Ny Raiko ao an-danitra.}

{2.
Na maty masoandro,
Ka alina ny andro,
Tsy mampaninona.
Fa Jeso, Izay sakaiza,
Dia ao fa tsy mankaiza,
Ho tena Masoandroko.}

{3.
Hatory izao ny maso,
Ry Tompo mba andraso
Ny ondrikelinao ;
Ny teny sy fanahy
Sy izay rehetra ahy
Atolotro harovanao.}
', '#1-VIII-4', '#1') /end

insert into song values (17221, '722. He alina ny andro,', NULL, NULL, '
{1.
He alina ny andro,
Ka mba esorinao
Ny fahotako maro,
Mba tsy ho hitanao ;
Reseo ny mpamely
Ny zanakao, ry Ray ;
Iraho ny anjely
Hiambina anay.}

{2.
Raha maizina ny tany,
Ny andro tapitra,
Tsaroanay ihany
Ny fasa-maizina ;
Arovy lalandava
Izahay, ry Tomponay,
Ka aoka ho mazava
Ny hitsangananay.}
', '#1-VIII-4', '#1') /end

insert into song values (17231, '723. He, alina indray izao,', NULL, NULL, '
{1.
He, alina indray izao,
Jesosy Tompo o ,
Ka aoka ny mpanomponao
:,: Homban''ny elatrao :,:}

{2.
Fa iza no hahita anay,
Raha azon-doza izao,
Kanefa kosa aronay
:,: Ireo anjelinao :,:}

{3.
Ka raha hatory izahay,
Ry Rainay tsara o!
Ny tena sy fanahinay
:,: Dia mba arovinao :,:}

{4.
Ka raha haposakao indray
Ny masoandronao,
Fohazy tsara izahay
:,: Hisaoranay Anao :,:}
', '#1-VIII-4', '#1') /end

insert into song values (17241, '724. Mpamonjy, Masoandronay!', NULL, NULL, '
{1.
Mpamonjy, Masoandronay!
Fahazavan''ny alinay
Ny zavon-tany mba savao
Tsy hanakonany Anao.}

{2.
Tontolo andro Ianao
No anton''ny iainanay ;
Raha alina, mombà anay,
Ny faty tsy ho tahotray.}

{3.
Ny aizina mba hazavao,
Tarihonao ny lalana
Halehanay an''efitra
Ho tonga any aminao.}

{4.
Izay marary mba tsaboy,
Ny ory aza afoinao,
Ho toy ny torin-jaza anie
Ny fiadananay izao.}

{5.
Tahio ny hifohazanay
Hanaovanay ny sitrakao
Ambara-pahatapitra
Ny andro ivelomanay.}

{6.
Raha mby aminao izahay,
Hahita fiadanana,
Fa ao tsy misy alina,
Ianao no Masoandronay.}
', '#1-VIII-4', '#1') /end

insert into song values (17251, '725. Alina ny andro', NULL, NULL, '
{1.
Alina ny andro
Ka efa mba hatory
Ny masonay ;
Ianao Jehovah o!
Izay tsy mety sasatra,
No aronay. (in-2)}

{2.
Lasa ny mazava,
Ka ny masoandro
Tsy hitanay ;
Tsy matahotra izahay,
Ianao an-danitra
Mahita anay. (in-2)}

{3.
Fa ny kintana eny
Efa manazava
Ny dianay ;
Koa tsy nafoinao,
Fa nasehonao ireo
Ho tokinay. (in-2)}

{4.
Alina sy andro
Samy mba manaiky
Ny tendrinao ;
Tonga re ny maizina,
Nefa tsy ilaozanao
Ny olonao. (in-2)}

{5.
Eto ny mazava
Dia manam-petra ;
Fa ao aminao
Tsy mba misy alina,
Fa mamirapiratra
Ny tavanao. (in-2)}
', '#1-VIII-4', '#1') /end

insert into song values (17261, '726. Efa hariva, Jeso tia!', NULL, NULL, '
{1.
Efa hariva, Jeso tia!
Tomoera aminay ;
Ny fanahinay tahio,
Fa hatory izahay.}

{2.
Fa ny helokay ekenay,
Mahavonjy Ianao ;
Ny manjo anay lazainay,
Fa miantra ny fonao.}

{3.
Alina maharitra ela,
Aizi-mampivadi-po,
Fa ny olona fidinao
Tsy mba takona aminao.}

{4.
Ianao no mitahiry
Ny miankina aminao ;
Voahodidin''ny anjely
Ny mialoka aminao.}

{5.
Raha anio no tapitra andro
Izahay mpanomponao,
Aoka izahay hifoha
Ao an-dapa soanao.}
', '#1-VIII-4', '#1') /end

insert into song values (17271, '727. Andriananahary', NULL, NULL, '
{1.
Andriananahary
Nikarakara anay,
Ka nampitsiry vary
Ho fivelomanay!
Ny haninay tahio
Ho tonga tanjakay,
Ny hevitray tariho
Mba hiadananay.}

{2.
Ry Rainay ao ambony
Misaotra Anao izahay,
Fa zava-tsoa itony
Izay nohaninay.
Ny zavatra rehetra
Dia manantena Anao,
Fa tsy mba misy fetra
Ny fitiavanao.}
', '#1-VIII-5', '#1') /end

insert into song values (17281, '728. Atreho ny sakafonay', NULL, NULL, '
{Atreho ny sakafonay,
Jehovah, mpanome ny soa!
Velominao ny tenanay,
Ny sainay hatanjaho koa.
Amen!}
', '#1-VIII-5', '#1') /end

insert into song values (17291, '729. Ry Jehovah, Ray Tsitoha!', NULL, NULL, '
{1.
Ry Jehovah, Ray Tsitoha!
Mba avia hialoha
Sy hiaro ny mpandeha ;
Lavi-dalana haleha,
Mba jereo ny olonao ;
Sarak''olona, irery,
Sady tsy mba manan-kery,
:,: Ka jereo, ry Tomponay! :,:
Mba tazano ny mpandeha ;
(in-3)
Ianao no herinay.}

{2.
Ao ny andro mamanala,
Sakambino ao an''ala ;
Raha mandeha mita rano,
Mba hazony sy tantano,
Ka vonjeo ny olonao ;
Be ny zavatra mihatra
Izay manimba sy mandratra,
:,: Ka vonjeo,
ry Tomponay! :,:
Mba arovy ny mpandeha ;
(in-3)
Ianao no aronay.}

{3.
Raha ory, mba tohano,
Ka vimbino sy tantano
Tsy ho reraka ny fony,
Aza afoy, fa mba hazony,
Iantrao ny olonao ;
Raha mahantra izy any,
Ory sady mitomany,
:,: Dia vangio,
ry Tomponay! :,:
Mba tsarovy ny mpandeha
(in-3)
Ianao no tokinay.}
', '#1-VIII-6', '#1') /end

insert into song values (17301, '730. O! tahio ny havanay re', NULL, NULL, '
{1.
O! tahio ny havanay re
Izay handeha lavitra ;
Be tokoa ny alahelo
Noho ny fisarahana ;
:,: Manatreha :,:
Izao veloma atao izao.}

{2.
Mba sakanonao ny loza
Tsy hamely azy izao,
Raha misy mampahosa,
Asehoy ny herinao ;
:,: Mba tantano :,:
Tsy hisaraka aminao.}

{3.
Aoka mba ho tonga soa,
Tsy ho azon-tsampona ;
Aoka mba hampodina
Eto aminay indray,
:,: Ka hisaotra :,:
Noho ny fivimbinanao.}
', '#1-VIII-6', '#1') /end

insert into song values (17311, '731. Enga anie ka homba anao Jeso !', NULL, NULL, '
{1.
Enga anie ka homba anao Jeso !
Hanadio sy hanazava,
Hampahery lalandava
Enga anie ka homba anao Jeso!}

{FIV.
Homba anao, Homba anao Jeso
Homba anao anie Jeso,
Homba anao, Homba anao Jeso
Enga anie ka homba anao Jeso!}

{2.
Raha mitambatra ny rahona,
Ka mikimpy ny masoandro,
Raha Jeso momba isan''andro
Ho tantely koa ny vahona.
[FIV.]}

{3.
Mangamanga indray ny lanitra
Loharanom-pitahiana
Sady foto-pifaliana
Jeso Zanak''Andriamanitra.
[FIV.]}

{4.
Raha mitolona ady sarotra,
Matokia fa tsy ho irery,
Homba anao Jeso mahery,
Homba anao tsy miady varotra
[FIV.]}

{5.
Raha hilaozana ny havana,
Sady mainty koa Jordana,
Ho mazava ny fitàna
Ao Jeso hanangana avana.
[FIV.]}

{6.
Raha Jesosy no mitantana,
Paradisa koa ny tany,
Hasambarana hatrany,
Ao Kanana no hamantana.
[FIV.]}
', '#1-VIII-6', '#1') /end

insert into song values (17321, '732. Velona indrindra,', NULL, NULL, '
{1.
Velona indrindra,
ry havana o!
Velona e, mandra-pihaona ;
Ry Tompo, vimbino
an-dàlana ao
Ny havana izay vao handeha ;
Ka ny mifanao
Veloma izao
Tahio, mba ho tafahaona!}

{2.
Ry Jeso Mpamonjy, tariho
izahay
Hihaona indray ao ambony,
Hiara-pinaritra mandrakizay,
Rehefa tafita ny ony,
Hiara-manao
Ny hira vaovao,
Fa tsy mba hisaraka intsony!}
', '#1-VIII-6', '#1') /end

insert into song values (17331, '733. Ny fihaonana an-tany,', NULL, NULL, '
{1.
Ny fihaonana an-tany,
Ry Jesosy Tomponay,
Vetivety, mora lany,
Ka mangoraka izahay ;
Namana nifankazatra
Hifanao veloma anio,
Ry Jesosy be fiantra,
Ny mpandeha mba tahio.}

{2.
Ny ho lasa mba tantano
Tsy ho voa an-dàlana ;
Ny hitoetra koa tohano,
Dia mba hiadana.
Na maraina na hariva,
Na aiza alehany ety,
Mba ombay mandrakariva
Mandra-podiny ary.}

{3.
Any volana, any taona,
Ny misaraka izao
Enga anie ho tafahaona
Raha mbola sitrakao.
Ilay fihaonana ao ambony
No irinay fatratra,
Tsy hisaraka intsony
Izahay ka sambatra.}
', '#1-VIII-6', '#1') /end

insert into song values (17341, '734. Zanahary o tahio ny tanindrazanay', NULL, NULL, '
{1.
Zanahary o tahio
Ny tanindrazanay,
Handroso lalandava,
Handry fahizay.
Enga anie hanjaka ao
Ny saina mahay miray,
Dia ho tany sambatra
Ilay Madagasikaranay.}

{2.
Ireo mpitondra anay
Arovy lalandava koa,
Tolory saina hendry,
Kinga sy mahay,
Ka ny marina anie no
Mba hanjaka ao tokoa
Dia ho tany sambatra
Ilay Madagasikaranay.}

{3.
Ny taranakay tahio
Ho vanona tokoa ;
Ireo no solofo,
Dimby rahatrizay,
Mba ho tena mahatoky,
Feno fahendrena koa,
Dia ho tany sambatra
Ilay Madagasikaranay.}
', '#1-VIII-7', '#1') /end

insert into song values (17351, '735. Arovy ry Tsitoha,', NULL, NULL, '
{1.
Arovy ry Tsitoha,
Ny Tanindrazanay,
Vimbino avokoa
Izay mitondra anay ;
Ity Nosy ity tahio
Mba handry fahizay ;
Tantano sy ampio
Handroso izahay.
Ny tsara mba tohano,
Ry Zanaharinay,
Ny ratsy mba sakano,
Mba hiadananay!}

{2.
Tahio ny mpitsara
Nampanapahinao,
Hahay hitondra tsara
Izao olonao izao;
Ny lehibe rehetra
Vangio sy hafanao,
Mba hanam-po miantra,
Hatahotra Anao,
Ny tsara mba tohano, ..sns.}
', '#1-VIII-7', '#1') /end

insert into song values (17361, '736. Mba tahionao, ry Jehovah!', NULL, NULL, '
{1.
Mba tahionao, ry Jehovah!
Ny tanindrazanay,
Ho tsara sy hiova ;
Hambininao anie!
Ho tany izay mandroso
Sy sambatra tokoa,
Ka hanjakan''i Jeso
Mpamonjy Tompo soa.}

{2.
Mba tahio ny firenena
Izay monina eto izao,
Handroso hihatsara,
Hatahotra Anao,
Handao ny fomba ratsy
Izay manadala re ;
Hanary koa ny sampy
Mampidi-doza Ã¨!}

{3.
Mba tahionao ny mpitondra
Nampanapahinao,
Arovy isan''andro
Mba tsy handà Anao,
Tariho lalandava
Hiankina aminao,
Omeo fahendrena
Hanao ny sitrakao.}

{4.
Mba tahio koa, ry Jehovah!
Ny fiangonanao
Handroso lalandava
Homban''ny heerinao.
Hanerana ny tany
Itoeranay izao,
Ka isam-bazantany
Hiondrika aminao.}
', '#1-VIII-7', '#1') /end

insert into song values (17371, '737. Ry Ray, tahionao,', NULL, NULL, '
{1.
Ry Ray, tahionao,
i Madagasikara,
Izay nohasoavinao, ka
voakarakara,
Ka nandefasanao ny teny
masinao;
Dia angatahanay
:,: ny fitahianao :,:}

{2.
Ny Fiangonanao,
ry Rain''ny fahazavana
Izay natsanganao hatao
fahazavana
Ka nodidianao hanao ny
asanao,
Dia angatahanay
:,: ny fitahianao :,:}

{3.
Ny asa marobe sy izay
fanaovan-tsoa
Miorina eto izao, ka sitrakao
tokoa
Ny fanjakana koa, izay
natsanganao,
Dia angatahanay
:,: ny fitahianao :,:}

{4.
Ny tokantranonay, ny tena
sy fanahy,
Ny fivelomanay, ry Ray Izay
Mpiahy,
Ireo rehetra ireo dia iantsoana
Anao.
Ka angatahanay
:,: ny fitahianao :,:}
', '#1-VIII-7', '#1') /end

insert into song values (17381, '738. Havaozy, Tompo o,', NULL, NULL, '
{1.
Havaozy, Tompo o,
Ny endriky ny tany,
Ny voninahitrao hatao
Fahazavany,
Izao aizim-pito izao
Levoninao tokoa
Izao ratsy be izao
:,: esory avokoa :,:}

{2.
Havaozy Tompo o,
Ny endriky ny tany,
Reseo ny rafinao,
Ravao ny fanjakany,
Ampanjakao tokoa
Jesosy Zanakao,
Izao rehetra izao
:,: hanao ny sitrakao :,:}

{3.
Havaozy, Tompo o,
Ny endriky ny tany,
Koa averenonao indray
Ny hatsaràny ;
Fanahy Mpanavao,
Ny famelomanao
Tadiavinao izao
:,: vitao ny asanao :,:}

{4.
Havaozy, Tompo o,
Ny endriky ny tany,
Ny olonao izao maniry
Indrindra izany.
Henoy ny sentonay
Miakatra aminao,
Ka hafainganonao
:,: ny fanavaozanao :,:}
', '#1-VIII-7', '#1') /end

insert into song values (17391, '739. Ry Jehovah Tompon''aina,', NULL, NULL, '
{1.
Ry Jehovah Tompon''aina,
O, tahio ny taninay!
Mba fohazy, re ny saina
Sy fanahinay hahay
Mankatò sy mankalaza
Ny Anaranao,
Mba ho fantatray mazava
Ny fitiavanao.}

{2.
Iantranao, ry Jesosy,
Hazavao ny Nosinay,
Ka ny fon''ny Malagasy
Diovy re ka mba ovay,
Tsy hanompo sampy intsony
Fa hazava re,
Ka ho sitraky ny fony
Ny ho masina e!}

{3.
O, tahio ny Faritany
Mba handray ny teninao
Na ambony na ambany
Mba tsarovy, iantrao!
Ny mpitory mba iraho,
Ka hiely anie,
Omeo hery, hatanjaho
Hahavita be!}

{4.
O, Jehovah, Ray mahery,
Sakambino izahay,
Eny, Ianao irery
No mba hany tokinay ;
Ka ny ratsy mba sakano
Tsy hanimba anay,
Fa ny tsara no tohano
Hiadananay.}

{5.
Ao koa ny mpanao lalàna
Mba toroy ny marina,
Ireo manam-pahefana
Omeonao sain-dalina!
Ka ny tany mba ampio
Handry fahizay!
O, arovy sy tahio
Ny mpitondra anay!}

{6.
Indreo ny isan-tokantrano
Mba tsidiho, hafanao,
Ka ny zanakay tantano
Sy trotroy ho olonao!
Eny, Tompo, mandrosoa,
Anjakao izahay
Ka onenonao tokoa
Izao ny Nosinay.}
', '#1-VIII-7', '#1') /end

insert into song values (17401, '740. He! ny tany manontolo', NULL, NULL, '
{1.
He! ny tany manontolo
Mba ambinonao, ao izao,
Ry Jeso Mpanolokolo,
Tena Tompo tsy mandao,
Mba vontosy andon''aina
Mba ho lonaka tokoa,
Tsy ho tany karankaina
Fa ho vokatra tokoa.}

{2.
Mba tahio ny fanjakana
Izay mitondra anay izao
Ireo mpitana ny lalàna
Mba omeo ny hevitrao.
Ny mpifehy isan-toko,
Ny mpikarakara izao
Anehoy ilay vokovoko
Mba hitondra marina ao.}

{3.
Ny komity sy voamiera
Raha miady hevitra ao
Tsy hitompo hevi-tena
Fa handray ny sitrakao
Ka ny didinao hatrany
No mba hijoroany izao
Fa ny tetiky ny tany
Dia ho laviny tokoa.}

{4.
Ry Mpanjakan''ny mpanjaka,
Miasà mahery izao
Ireo mpitondra samihafa
Mba toroy ny sitrakao.
Ireo mpanjaka an-tany hafa,
Revon-diadema koa,
Mba sintony re hitafa
Aminao ry Tompo soa.}
', '#1-VIII-7', '#1') /end

insert into song values (17411, '741. Jehovah Tompo o!', NULL, NULL, '
{1.
Jehovah Tompo o!
Tahio sy hatsarao
Ny taninay,
Tahio ny namanay
Ho tia ny teninao,
Dia mba hazavao
Ny nosinay.}

{2.
Ovay ny fombanay,
Ho foin''ny taninay
Ny sampiny ;
Omeo fo marina
Izao rehetra izao,
Ka dia mba iantrao
Ny nosinay.}

{3.
Jehovah Tompo o!
Tahio ho olonao
Ny zanakay ;
Tahio ho zanakao
Tahio ho marina
Ka dia ho taninao
Ny nosinay.}

{4.
Izay mpitondra anay
Tahio ho tia Anao,
Jehovah o!
Omeo sai-marina
Hanao ny sitrakao,
Ka ampanompoy Anao
Ny nosinay.}
', '#1-VIII-7', '#1') /end

insert into song values (17421, '742. Zanahary no mahela', NULL, NULL, '
(Maintimolaly)

{1.
Zanahary no mahela
Izay fe-taona iainanay,
Ka miantra sy mamela
Tombon''andro ho anay ;
:,: Nefa vitsy :,:
No ho andronay aty.}

{2.
Betsaka ny andro lany
Nanaranam-po izao ;
Misy feo manontany :
Vita ve ny asanao ?
:,: Miezaha :,:
Avy re ny alim-be.}

{3.
Mamindrà fo, Zanahary!
Mamelà ny helokay ;
Tompo! aza mba manary,
Andrao ho very izahay ;
:,: Mihainoa :,:
Ny mifona aminao.}

{4.
Ry Jehovah! manomeza
Izay ilainay olonao;
Mitahia, mampahereza,
Mba ho tonga ao aminao,
:,: Ka hahazo :,:
Satro-boninahitra ao.}
', '#1-VIII-8', '#1') /end

insert into song values (17431, '743. Ny andronay mandalo', NULL, NULL, '
{1.
Ny andronay mandalo
Lany foana taminay ;
Ny mandrakizay no hatoninay,
Nefa adala izahay.
Ry mpanota, ifony ny
helokao,
Fa Jeso tia anao
Mba tsy ho very mandrakizay,
:,: Avoty ny ainao :,:}

{2.
Ny fiadanan-dratsy
No mba manadala anay
Hijanona foana aty izao
Hanadino ny Rainay,
Mba hevero ry olon-ko
faty e!
Tsy ela no ety ;
Tsarovinao fa hody tokoa ;
:,: Antsoiny hianao :,:}

{3.
Ny taninay havela,
Mpivahiny izahay ;
Tsy maintsy hiainga hiala aty
Any no hodianay.
Tsara sady mazava
Ny tranonao,
Miomàna, fa hankao ;
Tazano, fa mivoha izao ;
:,: Midira hianao :,:}
', '#1-VIII-8', '#1') /end

insert into song values (17441, '744. He! ny andro mora lasa,', NULL, NULL, '
{1.
He! ny andro mora lasa,
O, ry mpivahiny o!
Tsy ho ela, kely sisa,
Dia hody hianao!
Tsy mba fantatrao akory
Izay hahafatesanao ;
Koa aza mba matory,
Sao ho very hianao.}

{2.
Miomàna ry sakaiza,
Aza mba tamàna aty,
Fa ny eto tsy mateza,
Miaingà mba ho ary!
Ao ny Tomponao Jesosy,
Hampihaingo tsara anao,
Fa ny rany no mamofsy
Ny akanjo ho anao.}
', '#1-VIII-8', '#1') /end

insert into song values (17451, '745. Henoy, ry Jeso Tompo!', NULL, NULL, '
{1.
Henoy, ry Jeso Tompo!
Ny fitarainanay,
Fa amin''alahelo
No ipetrahanay ;
Ny fonay maniry
Hitovy aminao,
Kanefa be ny ratsy
Masakana anay ?}

{2.
Nateraky ny nofo,
Ka nofo izahay ;
Nanompo tompo ratsy,
Ka very hatrizay!
Ny razanay nanota
Ka nodimbiasànay
Ry Tomponay Tsitoha,
Havaozy izahay!}

{3.
Ny asam-pamonjena
Natolotra Anao;
Tsy hisy tsy ho vita
Izay natombokao ;
Raha resy avokoa
Ny fahavalonao
Hiposaka tokoa
Ny tena taom-baovao!}

{4.
Ny tavanao malala,
Ry Jeso Tomponay,
Ho tena masoandro,
Izay hifalianay.
Ry nosatrohan-tsilo,
Faingàna Ianao,
Hanangana ny maty,
Hitondra aim-baovao!}
', '#1-VIII-8', '#1') /end

insert into song values (17461, '746. Mifalia, ry sakaiza !', NULL, NULL, '
{1.
Mifalia, ry sakaiza !
Fa nahazo taom-baovao,
Zanahary no mitaiza,
Ka isaory sy hajao.
Mihirà sy mihobia,
Jeso Tompo no derao,
Miravoa, mifalia,
Velona aina hianao,}

{FIV.
Mifalia, ry sakaiza !
Fa nahazo taom-baovao,
Zanahary no mitaiza,
Ka isaory sy hajao.}

{2.
Izao no andro mampifaly,
Andro lehiibe anio,
Dia hira mahafaly
No hatao ankehitrio;
Zanahary no mitàna,
Maifalia hianao;
Tononkira hoe: Hosana!
No atao aty izao.
[FIV.]}

{3.
He, Hosana, he ! Hosana,
Ho Anao, ry Tompo o!
Fihirana, fiderana
No asandratray izao;
Dera, saotra, hery, laza,
Ho Anao, Mpanjakanay,
Ho Anao anie ny haja,
Raiso ho Anao izao.
[FIV.]}
', '#1-VIII-9', '#1') /end

insert into song values (17491, '749. Ny taom-baovao dia tonga', NULL, NULL, '
{1.
Ny taom-baovao dia tonga
indray
Ho fiantsoanao anay
Mba hanoloranay Anao
Ny tenanay voavotrao.}

{2.
Fa maro be, ry Tomponay!
Ny zava-tsoa nomena anay
Tamin''izao taon-dasa izao,
Izay isaoranay Anao.}

{3.
Ny mafy izay manjo anay,
Tsy tonga foana aminay,
Fa mba ho fitahianao
Izay miankina aminao.}

{4.
Ka enga anie ny otanay,
Izay mitambesatra aminay
No hibebahanay izao,
Hanaovanay ny sitrakao.}

{5.
Ny taom-baovao, diavinay,
Ny taona teo, tafitanay,
Ka dia ny fitarihanao
No mba irinay aminao.}
', '#1-VIII-9', '#1') /end

insert into song values (17501, '750. Dera, laza, ry Mpanjaka!', NULL, NULL, '
{1.
Dera, laza, ry Mpanjaka!
No atolotray Anao
Noho Ianao nitahy ;
Tratranay ny taom-baovao.}

{2.
Andro maro izay no lasa,
Be ny soa hitanay ;
Fitahia-mahagaga
Azo taminao, ry Ray!}

{3.
Marobe ny ratsy vita,
Lehibe ny helokay,
Ka tsy mendrika hiantso
Ny Anaranao izahay.}

{4.
Miantrà, ry Zanahary!
Mamelà ny helokay,
Ka sasao ny ran''i Jeso,
Mba hadio izahay.}

{5.
Aoka re ny andro sisa
Hanompoanay Anao ;
Hatanjaho, ry Jehovah!
Izahay mpanomponao.}
', '#1-VIII-9', '#1') /end

insert into song values (17511, '751. He, vaovao ny taona,', NULL, NULL, '
{1.
He, vaovao ny taona,
Ry Jesosinay,
:,: Ka mba te hihaona
Aminao izahay :,:}

{2.
Ianao mitahy
Ny mpanomponao
:,: Koa tsy manahy
Izay tarihinao :,:}

{3.
Ary izaho koa
Mba tantanonao
:,: Dia hahita soa
Ao an-danitrao :,:}
', '#1-VIII-9', '#1') /end

insert into song values (17521, '752. O, ry Jesosy, ny Anaranao', NULL, NULL, '
{1.
O, ry Jesosy, ny Anaranao
Manatsika ahy izao hanatona,
Hanolotra ny foko ho Anao,
Mpamonjy tia sy Mpamelona.}

{2.
Manatona aho, ka miondrika
Hanoroka ny tongo-masinao ;
Izaho olona tsy mendrika,
Ka mba havaozy, Tompo,
ho Anao!}

{3.
Antsoiko isan''andro Ianao
Ry Toky tsara mahafatra-po,
Ka amin''ny Anara-masinao
No anankinako izay hanjo.}

{4.
Raha io no entiko mangataka,
Hisokatra amiko ny lanitra,
Ka dia ho azoko malalaka
Ny zava-tsoa izay maharitra.}

{5.
Mamirifiry izao tontolo izao
Tsy misy tena fitsaharana ;
Fa fiereko ny Anaranao
Ka ahitako fiadanana.}

{6.
O, ry Anaran''i Jesosiko,
Ry Paradisa aty an-tany o!
Ry voninkazo mampifaly fo!
Ry tohan''aina ao an-dalako.}

{7.
Iriko andro aman''alina
Ny hanana Anao ho ao
am-po,
Ho Tokiko raha mbola
velona
Sy amin''ny hahafatesako.}
', '#1-VIII-9', '#1') /end

insert into song values (17471, '747. Haleloia! Sambasamba!', NULL, NULL, '
{1.
Haleloia! Sambasamba!
Hitanay ny taom-baovao;
Avy izahay, Jehovah!
Mba hifaly aminao ;
Fihirana no atao,
Saotra, vavaka,
No atolotray Anao,
Ry Mpamelona!
Haleloia! Sambasamba!
Faly izahay izao :
Haleloia! Sambasamba!
Hitanay ny taom-baovao.}

{2.
Lasana ny andro maro,
Be ny zavatra natao,
Na ny mety, na ny diso,
Samy efa fantatrao ;
Aoka anie ny tsininay
Hovonoinao,
Fa ny mety vitanay
Mba haharitra :
Haleloia, ..sns.}

{3.
Ka ny taom-baovao, Jehovah!
Aoka hanomezanao
Fahendrena ny mpitondra,
Hahasoa ny olonao ;
Ka ny Fiangonana
Hamasinonao
Mba handroso marina
Hahalala Anao:
Haleloia, ..sns.}

{4.
Mitsingerina ny taona,
Fohy andro izahay,
Vetivety dia hihaona
Aminao, ry Tomponay!
Enga ka ho hitanay
Ao an-danitra
Ny Sabata tsara izay
Tsy ho tapitra :
Haleloia, ..sns.}
', '#1-VIII-9', '#1') /end

insert into song values (17481, '748. Havaozy, Tompo o!', NULL, NULL, '
{1.
Havaozy, Tompo o!
Izao rehetra izao
Ka aoka mba ho hita ao
Ny voninahitrao
Ry Tompo Mpanasoa,
Levony avokoa
Ho fongatra tsy hisy re
Ny ratsy marobe.}

{2.
Izao taona vaovao
Diavinay izao
Ho fanavaozan-dehibe
Ny tenanay anie!
Ka ny fiainanay
Izay ratsy hatrizay,
Ovay hatramin''ny anio
Ho tsara sy madio.}

{3.
Havaozy, Tompo o!
Ny Fiangonanao
O! akambano ho iray,
Hiray mandrakizay.
Ry Tompo be fitia,
Tongava, mamangia ;
Mba tanteraho aminay
Ny fanavaozanao.}
', '#1-VIII-9', '#1') /end

insert into song values (17531, '753. Tiako ny hiaraka Aminao, Jesoa', NULL, NULL, '
{FIV.
Tiako ny hiaraka Aminao, Jesoa,
Na aiza na aiza hitondranao ahy,
Tiako ny hiaraka Aminao, Jesoa,
Fa Ianao no miantso ahy.}

{1.
Hiaraka aminao aho o ry Jeso Tompoko,
Tsy manana olon-kafa aho fa
Ianao no fiainako
Hanara-dia Anao: izay no faniriako
Hifikitra Aminao aho Jesoa.
[FIV.]}

{2.
Hiaraka aminao aho, o ry Jesoa Tompoko,
Hitory teny soa amin''ireo havako,
Hanara-dia Anao: izay no faniriako
Hanohy ny asanao aho, Jesoa!
[FIV.]}

{3.
Hiaraka aminao aho, o ry Jesoa Tompoko,
Ho zary famonjena anie  ny fahoriako,
Hanara-dia Anao: izay no faniriako
Hifantsika aminao aho, Jesoa!
[FIV.]}

{4.
Hiaraka aminao aho, o ry Jesoa Tompoko,
Hibanjina ny Ray, Tompo Andriamanitro,
Hanara-dia anao: izay no faniriako
''zay misy Anao, no hisy ahy, Jesoa.
[FIV.]}
', '#1-IX', '#1') /end

insert into song values (17541, '754. Mitsangàna, mandehana,', NULL, NULL, '
{1.
Mitsangàna, mandehana,
Ry zatovo be laolao!
Manatona ny Mpamonjy,
Avelao ny sitrapo.}

{2.
He, tsarovy fa mahantra
Sady ory hianao,
Ka hatony ny Mpamonjy,
Dieny mbola hitanao.}

{3.
Mandehana, mianara,
Mba ho hendry hianao ;
Jeso Tompo no miandry
Hampianatra anao.}

{4.
Ry zatovo! mandehana,
Mba vitao ny asanao,
Mahereza, aza osa,
Ny Mahery homba anao.}
', '#1-IX', '#1') /end

insert into song values (17551, '755. O! ry tanora mba tsarovy', NULL, NULL, '
{1.
O! ry tanora mba tsarovy
Ilay Tompo soa maty noho
ny helokao ;
Izy miantso anao moramora
Vohay ny fonao hidirany izao.
Ny saina sy ny safidy nomeny.
Ka mba fidio ny ho
mpanaraka
An''i Jesosy, Mpanjaka
Tsitoha,
Ny fonao atolory hanjakany
ao.}

{2.
O! ry tanora miambena,
Fa ny devoly dia mpamitaka
mahay
Maro ny fandrika ka
mitandrema
Tsy ho tra-nenina re
mandrakizay.
Ka ekeo anatrehan''ny Tompo,
Handao ny ratsin''izao
tontonlo izao.
Raha Jeso no iankinanao
mafy,
Izy hiaro mba mba tsy
hivadihanao.}

{3.
O! ry tanora, mivavaha
Fa Jeso Tompo dia Mpihaino
vavaka
Azy irery ny hery rehetra,
Dia matokia mba ho
afa-tahotra.
Ka raha ory sy mila ho kivy,
Dia andrandraonao Ilay
Tompo be fitia ;
Ny fitarainanao dia hohenoiny
Ary ny fonao dia
hampiadaniny.}

{4.
O! ry Jesosy, mba ampio
Izaho mpianatrao mba ho
madio fo
Tantano aho, vimbino ry Jeso,
Fa sarotra ny lalana alehako ;
Iriko ny ho mpandresy ny ratsy,
Iriko hahatanteraka ny soa ;
Omeo hery sy tanjaka aho.
Efao ny asa tsara ao anatiko.}
', '#1-IX', '#1') /end

insert into song values (17561, '756. Avia ry sakaiza o!', NULL, NULL, '
{1.
Avia ry sakaiza o!
Antsoin''ny Mpanjaka izao
Handray fiadiana
Hoentina hanafaka.
Velona ny baikonao,
Andrandrainay Ianao
Ka arahinay tokoa,
Ry Mpandresy ao Ziona!}

{2.
Ny sabatra atolony
Dia ny teny masina,
Ampinga fiarovana
Mahery ny finoana.
Velona ny baikonao, ..sns.}

{3.
Ny vatsy izay atolony
Dia ny ra sy tenany
Ho herintsika olony
Izay miaramilany.
Velona ny baikonao, ..sns.}

{4.
Jesosy o, arahinay
Ny fiantsoanao anay,
Ka manampia ny olonao
Hiady sy handresy izao.
Velona ny baikonao, ..sns.}
', '#1-IX', '#1') /end

insert into song values (17571, '757. Tsarovy, ry sakaiza,', NULL, NULL, '
{1.
Tsarovy, ry sakaiza,
Ny RAY an-danitra e,
Fa Izy no nitaiza
Sy nahalehibe ;
Fa raha mbola zaza
Nampitomboiny,
Ka na ho antitra aza,
Tsy mba hafoiny.
Ny saina sy safidy
Nomeny ho anao,
Ka tiany hifidy
Ny tsara hianao}

{2.
Tsarovy, ry tanora,
JESOSY, avotrao,
Mahay mitondra mora,
Mitandrina anao.
Miaraka mibata
Ny fahorina,
Ka dia mahareta
Ny hatrotrahana,
Fa Izy no miaro
Izay mpianany
Ka dia mba fantaro
Ny fitiavany!}

{3.
Tsarovy ry zatovo,
Ny MPANAMASINA
Fa Izy no manovo
Ny rano velona,
Ka dia mba mampandro
Ny ta-hadio fo.
Lazaina isan''andro
Ny marina sy to,
Ka ny masirasira
Esoriny aminao,
Izay maniratsira
No halavironao.}

{4.
Tohero, ry zatovo,
Izay mamitaka,
Satana ta hamono
Ny tsy miambina
Ny nofo mitsikilo
Ny voninahitrao ;
Mamono ny fanilo
Izao tontlo izao
Ka raisony ampinga
Alao ny sabatra,
Izany mahakinga
Ny te ho sambatra.}
', '#1-IX', '#1') /end

insert into song values (17581, '758. Tsarovy e! rankizy', NULL, NULL, '
{1.
Tsarovy e! rankizy
Ilay Mpanao anao!
Ka tadiavo Izy!
Dieny hitanao ;
Dieny mbola kely,
Tsarovy rahateo,
Ka aza manadino
Ilay Mpanao anao.}

{2.
Tsarovy ry zatovo
Ilay Mpanao anao!
Ilaozy ny mpanota,
Fandrao manimba anao.
Tsarovy ny Jehovah
Namorona anao,
Mampanantena lova
Sy nahatoy izao.}

{3.
Tsarovy, ry sakaiza,
Jehovah Tomponao!
Fa Izy no mitaiza
Sy manasoa anao ;
Koa aza manadino
Ilay Mpanao anao ;
Ny teniny araho,
Fa Izy no Rainao.}
', '#1-IX', '#1') /end

insert into song values (17591, '759. Ry Sakaizan''ny tanora !', NULL, NULL, '
{1.
Ry Sakaizan''ny tanora !
Zoky be fitia,
Sakambino moramora,
Izahay mpania.}

{FIV.
Tompo o! Arovy
Ny madinikao,
Aza afoy fa mba tsarovy,
Tano mafy ho Anao.}

{2.
Ry Sakaizan''ny tanora !
Tsinjonao ary,
Izany ady tsy mba mora
Hitany ety.
[FIV.]}

{3.
Ry Sakaizan''ny tanora !
Tanonao izahay ;
He ! fa be ny manakora
Ravo lavo indray.
[FIV.]}

{4.
Ry Sakaizan''ny tanora !
Efa, sitraky,
Isan''andro, isan''ora
Ho Anao izahay
[FIV.]}
', '#1-IX', '#1') /end

insert into song values (17601, '760. Aza mba manadino', NULL, NULL, '
{1.
Aza mba manadino
Ny ray sy reninao;
Fa izy no nitaiza
Sy nanasoa anao;
Ka na aiza na aiza aleha
An-tany lavitra,
Aza mba manadino
Ny ray sy reninao;
Tsara izay! Tsara izay!
Eny, tsy ho hadinonay
Ny ray sy reninay.}

{2.
Aza mba manadino
Ny tanindrazanao,
Tsarovy ny tanàna
''zay nahabe anao;
Fadio izay tsy mety
fanaony fahizay,
Aza mba manadino
Ny tanindrazanao.
Tsara izay! Tsara izay!
Eny, tsy ho hadinonay
Ny tanindrazanay.}

{3.
Aza mba manadino
Ireo mpitondra anao,
Araho ny lalàna
mba hiadananao:
Ka na aiza na aiza aleha
An-tany lavitra,
Aza mba manadino
Ireo mpitondra anao:
Tsara izay! Tsara izay!
Eny, tsy ho hadinonay
Izay mpitondra anay.}

{4.
Aza mba manadino
Ny fivavahanao,
Ataovy isan''andro
mba hamonjena anao;
Mpitondra, ray sy reny
ny tany, tena koa,
Aza mba manadino
Ny fivavahanao:
Tsara izay! Tsara izay!
Eny, tsy ho hadinonay
Ny fivavahanay.}
', '#1-IX', '#1') /end

insert into song values (17611, '761. Ry Jeso, Zoky be fitia', NULL, NULL, '
{1.
Ry Jeso, Zoky be fitia!
Avia, mitantana
Fandrao izahay maniasia
Avilin''i Satana}

{FIV.
Ry Jeso o! Tahio izahay,
Fa mbola kely saina
He! Tsy mba hendry, tsy mahay
Ka mora ambakaina.}

{2.
Maniry izahay, Jeso!
ho zandry izay manoa,
Ka dia ampaherezonao
Ho tia Anao tokoa.
[FIV.]}

{3.
Indreto izahay, Jeso!
Tariho hahalala
Hanaraka ny sitrakao
''zay zokinay malala.
[FIV.]}
', '#1-IX', '#1') /end

insert into song values (17621, '762. Modia, modia,', NULL, NULL, '
{1.
Modia, modia,
Fandrao very tokoa,
Fa ny efitra be
No diavina izao ;
Ry zanako o!
Modia izao.}

{2.
Modia, modia,
Fa miandry ny Ray
Hampandroso anao
Sady hanome soa ;
Ry zanako o!
Modia izao.}

{3.
Modia, modia,
Fa nanavotra anao
Jeso ilay be fitia
Izay nisolo anao ;
Ry zanako o!
Modia izao.}

{4.
Modia, modia,
Ka ilaozy izao
Izay ratsy natao
Nahameloka anao,
Ry zanako o!
Modia izao.}

{5.
Modia, modia,
Hovantaninao ao
Ny tranon''ny Ray
Sy ny lamba madio
Ry zanako o!
Modia izao.}
', '#1-IX', '#1') /end

insert into song values (17631, '763. Tanora o! Ny nosindrazanao', NULL, NULL, '
{1.
Tanora o! Ny nosindrazanao
Miandry izao, manasa anao,
Ingao anio ilay fanevanao
O! tafio ny herinao.}

{FIV.
O! mifohaza izao tahio ny herinao,
/njao ny Nosinao, manantena Anao
Jereo ny namanao taninin-doza izao
O! Vonjeo fa mitoreo.}

{2.
Avia, jereo ny Fiangonanao;
O! mba vonjeo fa mila anao.
Tanora soa no mba ilaina koa
O! avia hiara-dia.
[FIV.]}

{3.
Omeo anio ny herinao zaio,
Ka dia falio /reo tapakao.
/reo didy soa sy baikon''ny Tsitoha
Tanteraho sy hajao.
[FIV.]}
', '#1-IX', '#1') /end

insert into song values (17641, '764. Tsy mangataka aho, Tompo', NULL, NULL, '
{1.
Tsy mangataka aho, Tompo
Zava-py, harena, zo,
Fa ny tena mba iriko
Dia izao : ny fo madio.
Fo madio izay akàny
Feno eritreri-tsoa
Fo madio : haren-tsy lany
Sarobidy avokoa.}

{2.
Indrisy, ny soan''ny tany
Manodoka, mora lo
Ny haren-tsy mety lany
Dia izao : ny fo madio.
Fo madio ..sns.}

{3.
Na maraina na hariva,
Dia ity no hatako :
Mba ho ahy ilay soa indrindra,
Dia izao : ny fo madio.
Fo madio ..sns.}
', '#1-IX', '#1') /end

insert into song values (17651, '765. Henoy ny antson''ny Tompo ;', NULL, NULL, '
{1.
Henoy ny antson''ny Tompo ;
" Iza no ho irakay,
Vonon-kandeha sy hanompo,
Hiasa an-tany hay ? "
Adidy sarotra hefaina
Asa mangetana re,
Maty handritra ny aina,
Iza no vonona re ?}

{2.
Tsy didy miendrika baiko
Nefa mahery tokoa ;
Nanindry mafy ny saiko
Ny antsonao, Tompo soa
Satriko lavina anefa
Manery ahy hanoa,
Any am-po no miantefa,
Ny ako, onjany koa.}

{3.
Inty, ry Tompo malala
Izaho ho irakao,
Nofo ka misalasala
Raha toa tsy tohananao.
Ny foko omeo fahendrena
Hanao ny sitrakao,
Ny saiko mora mivena
Fenoy ny hasoavanao.}

{4.
Vonona ka hamasino
Hanefa ny irakao ;
Eny, ry Tompo, vimbino
Handia ny lalanao.
Sarotra nefa hataoko
Ny asa atolotrao,
Ny sitrapo dia hafoiko,
Tohano ny herinao.}
', '#1-IX', '#1') /end

insert into song values (17661, '766. Jeso Mpanjaka Malaza', NULL, NULL, '
{1.
Jeso Mpanjaka Malaza
Nefa sakaizan''ny ankizy koa,
Mamin''ny foko ny fitiavanao
Sintony e, ny foko ho tia anao,
Jeso malala o! Tompo be
fitia
Tano ny foko tsy haniasia,
Ho zaza hendry,
hahay hanompo,
Ho tia Anao anie,
ry Tompo o!}

{2.
Mamin''ny foko hotantaninao,
Tiako Jeso hiaraka Aminao
Ny foko e, arovy ho madio
Ny faniriako Tompo o, valio!
Jeso malala ..sns.}

{3.
Ny hazazako ry Tompo, tahio,
Enga ho zaza tia ny madio
Ho zaza hendry, zaza
mpankato
Anao Mpanjaka, Tompo
manan-jo,
Jeso malala ..sns.}
', '#1-X', '#1') /end

insert into song values (17671, '767. Ry Jeso, Zaza masina o!', NULL, NULL, '
{1.
Ry Jeso, Zaza masina o!
Izay tonga zaza toa anay ;
Nafoinao ny lapanao
Hitondra vonjy ho anay,
Ry Jeso, Zaza masina o!}

{2.
Ry Jeso, Zaza masina o!
Nanetry tena Ianao ;
Nilaozanao ny lanitrao
Noho ny fitiavanao,
Ry Jeso, Zaza masina o!}

{3.
Ry Jeso, Zaza masina o!
Ianao no fihavanana ;
Izahay izay nandà ny Ray
Tadiavanao ho Azy indray,
Ry Jeso, Zaza masina o!}

{4.
Ry Jeso, Zaza masina o!
Hihira ho Anao izahay,
Ianao nanondrotra anay ;
Ny lapan-dRay ho azonay,
Ry Jeso, Zaza masina o!}
', '#1-X', '#1') /end

insert into song values (17681, '768. Endrey! endrey! Tompo o!', NULL, NULL, '
{1.
Endrey! endrey! Tompo o!
He, ny hasoavanao,
Fa tsy tapaka hatrany
Izany fitahianao ;
Notantananao, ry Tompo
Itony ondrikelinao.}

{2.
Endrey! endrey! Tompo o!
He, ny fitiavanao ;
Efa elabe tokoa no mba
niheveranao
Izany zaza Malagasy
Izay irinao mba ho soa.}

{3.
Anio izahay, Tompo o!
Mankasitraka Anao,
Fa ny sandrinao mahery
No niaro toy izao
Tratranay mandrakariva
Ny androm-pahasoavanao!}

{4.
Tahio tokoa, Jeso o!
Madagasikaranay
Mba handroso ka ho tonga
Madagasikaranao ;
Izany re no faniriana
Ho tanteraka anie!}
', '#1-X', '#1') /end

insert into song values (17691, '769. Misy toerana voafidy', NULL, NULL, '
{1.
Misy toerana voafidy
Manan-daza lehibe,
Dia saha saro-bidy,
Tena mahavokatra e!
Saha feno fitahiana,
Misy voka-tsoa ery ;
Ao no mendrika haniriana
Saha masina izy ity!}

{2.
Ny Sekoly Alahady,
Io no sahanao, ry Ray
Ny Baiboly no angady
Hanasoavanao anay ;
Mahavatra ny Mpamboatra,
Ka mirobona tokoa ;
Voakarakara loatra,
Ka mitombo sy mamoa.}

{3.
Midadasika ny tany,
Nefa karakaina koa ;
Fa ny sahanao ihany
No tsy manam-paharoa.
Misy rano tsy mba ritra
Dia Teny Masinao ;
Izay toriana eran-tany
Mikoriana lava izao.}

{4.
Ny ankizy sesehena
No mivory eto izao ;
Ireo no hazo ambolena
Ao anaty Sahanao,
Raha mirona mba tohano,
Raha lavo, ajoroy,
Zaza osa, ka tantano,
Mbola kely ka toroy!}
', '#1-X', '#1') /end

insert into song values (17701, '770. Hajain''ny saiko', NULL, NULL, '
{1.
Hajain''ny saiko
Fototry ny aiko,
Itsaohan''ny fanahiko,
Tokana iriko,
Malala tiako :
Ianao, Jeso Mpamonjiko!}

{2.
Mety malazo
Ny voninkazo,
Ny ahi-maintso koa re!
Fa misy lova
Tsy mba miova :
Jesosy Perla soa e!}

{3.
Ao lalandava
Ny andro mazava,
Ny kintana, ny volana ;
Jesosy irery
Tompon''ny hery :
Tsy azo ihoarana.}

{4.
He, igagàna
Ny hatanorana,
Fa na mahafinaritra,
Mety mihena
Ny herin-tena :
Jesosy no maharitra.}

{5.
Tsy misy fetra
Zavatra rehetra
Ianao Jesosy, Tokiko,
Akaiky ahy,
Zoky Mpiahy,
Ny tsara indrindra amiko.}

{6.
Raha ho lasa,
Fa vita ny asa,
Tohano aho, Jeso o!
Ampio aho
Sy atanjaho,
Ho sambatra eo an-tratranao.}
', '#1-X', '#1') /end

insert into song values (17711, '771. Tsinjovinao ny vorona,', NULL, NULL, '
{1.
Tsinjovinao ny vorona,
Fa tsy mamafy na mijinja,
Nefa ao Ilay Mpamelona
Miahy sy mikojakoja,
Jereo ny voninkazo soa,
Fa tsy miasa na mamoly ;
Ireo ahin''ny Tompo koa,
Ka velona ao an-tanimboly.}

{2.
Isika izay malalany
No dia andrian''ny fony
indrindra,
Ka mifalia, ry zanany!
Ny Zanahary no mpitondra,
Ry Tompo, Ray malalanay!
Hatoky tsy hanahy intsony
Ny tena sy fanahinay,
Fa manana ny Ray ambony.}
', '#1-X', '#1') /end

insert into song values (17721, '772. Manatona, manatona', NULL, NULL, '

{(Vakio Matio 11 : 28)}

{1.
Manatona, manatona
An''i Jeso izao ;
Manatona an''i Jeso izao.}

{(Vakio Jaona 3 : 16)}

{2.
Mba minoa, mba minoa
An''i Jeso izao,
Mba minoa an''i Jeso izao.}

{(Vakio Hebreo 7 : 25)}

{3.
Mahavonjy Jeso Tompo
Mahavonjy izao
Mahavonjy Jeso Tompo izao.}

{(Vakio Jakoba 5 : 11)}

{4.
Be fiantra, be fiantra
Jeso Tompo izao,
Be fiantra Jeso Tompo izao.}

{(Vakio Jaona 6 : 37)}

{5.
Tsy hanary Jeso Tompo,
Tsy hanary anao,
Jeso Tompo tsy hanary anao.}

{(Vakio Marka 10 : 47)}

{6.
Miantsoa, miantsoa
An''i Jeso izao
Miantsoa an''i Jeso izao.}

{(Vakio Marka 10 : 51)}

{7.
Fa mamaly Jeso Tompo
Fa hamaly anao,
Fa hamaly Jeso Tompo izao.}

{(Vakio I Jaona 1 : 7)}

{8.
Fo madio, fo madio
No homeny anao
Fo madio no homeny anao.}

{(Vakio Apokalypsy 3 : 5)}

{9.
Lamba fotsy, lamba fotsy
No hatafy anao,
Lamba fotsy no hatafy anao.}
', '#1-X', '#1') /end

insert into song values (17731, '773. Ianao, ry Jehovah no', NULL, NULL, '
{1.
Ianao, ry Jehovah no
Rainay tokoa,
Ka manatona Anao izahay,
Ary Jeso no Hava-malalanay
koa.
Dia matoky Anao ny fonay,
Mivoria ry ondry madinika o!
Mba ho ao am-pisokoanio ;
Mihainoa ny feon''ny Mpiandry
izao
O, faingàna, modia hianao.}

{2.
Eny, mamy tokoa ny mihaino
Anao
Ry Mpamonjy mpanavotra
Anay!
Fa Sakaiza malala tokoa
Ianao,
Te hihaino Anao izahay,
O! andeha, rankizy madinika
e!
Fa miantso Jesosy ery,
Mahafaly antsika ny teniny
Hoe :
" Avelao izy mba hankaty! "}

{3.
O! ry Tompo tsy toha!
Tsinjovy ''zahay,
Fa masiaka ny lion-dozabe,
Ka manenjika mba hahafaty
anay
Sy mihaza ny ain-dehibe,
Mitsangàna, zaza
madinika o!
Mandosira ny loza ety
Manatona an''i Jeso
Mpamonjy izao,
Ka modia ho an-danitra ary.}
', '#1-X', '#1') /end

insert into song values (17741, '774. Fony Jeso nivahiny', NULL, NULL, '
{1.
Fony Jeso nivahiny
Nisy zazakely koa
Tonga tao, ka notahiny
Sy noraisiny tokoa :
:,: He, rankizy!
Hy fitiavan-dehibe :,:}

{2.
Manatona, izao, rankizy!
An''i Jeso Tompo soa,
Fa mandray ny zaza Izy
Ka mitahy azy koa :
:,: He, rankizy! ..sns.}

{3.
Maro be ny zaza faly
Ao an-dapan-dRainy ao,
Afaka ota, afa-jaly,
Sambatra izay tonga ao :
:,: He, rankizy! ..sns.}

{4.
Mihobia izao, rankizy!
Fa isika koa hankao ;
Eny, mifalia, fa Izy
Ho Sakaizantsika ao :
:,: He, rankizy! ..sns.}
', '#1-X', '#1') /end

insert into song values (17751, '775. Ry Jesosy Tomponay!', NULL, NULL, '
{1.
Ry Jesosy Tomponay!
Mamindrà fo aminay ;
Be ny hasoavanao,
Be ny fitiavanao ;
Zazakely izahay,
Mamindrà fo aminay.}

{2.
Fony teto aminay
Ianao sakaizanay,
Tia zaza Ianao,
Zaza notrotroinao :
Zazakely ..sns.}

{3.
Masina sy marina
Ianao, ry Tomponay!
Hamasino izahay
Mba ho zanakao tokoa :
Zazakely ..sns.}

{4.
Be ny fahavalonay
O! arovy izahay ;
Manomeza fo madio,
Fo miankina aminao :
Zazakely ..sns.}

{5.
Ary raha tapitra
Ny iainanay aty,
Ento ny fanahinay
Honina ao an-danitra :
Zazakely ..sns.}
', '#1-X', '#1') /end

insert into song values (17761, '776. Mba ekeo, ry Jeso Tompo,', NULL, NULL, '
{1.
Mba ekeo, ry Jeso Tompo,
Izahay madinikao,
Fa maniry te-hanompo
Sy hanao ny sitrakao ;
Tsy mba zaza noterena,
Na nomena zava-tsoa,
Fa minia manolo-tena,
Ho Anao tokoa tokoa.}

{2.
Tsy ny manan-kery ihany,
Na ny lalin-tsaina koa
No mahazo miandany
Aminao, Mpamonjy soa ;
Fa ny mbola zaza osa,
Ka malemy toa anay,
Izay mba te-hidodododo
Homba Anao, Mpanjakanay.}

{3.
Maminay ny fanompoana
Izay natolotrao anay,
Ka hataonay sasa-poana
Izay mamily dia anay,
Fa ny hany mba ilaina,
Dia ny fanohananao ;
Mba ho azonay vitaina
Izay rehetra sitrakao.}

{4.
Enga anie ny faniriana,
Izay anananay izao,
Ka harahim-pitahiana
Sy hotanterahinao
Dia ho feno fifaliana
Ny fanahy anatinay,
Ka tsy hisy hakiviana
Hanan-kery aminay.}
', '#1-X', '#1') /end

insert into song values (17771, '777. He! zaza maro be tokoa', NULL, NULL, '
{1.
He! zaza maro be tokoa
Ary an-danitra,
Mifaly amin''i Jeso,
Finaritra avokoa :
He! mihoby mafy izy,
Ka miderà an''i Jeso!}

{2.
Moa nentin''iza izy ireo
Ho ao an-danitra ao
No miramirana endrika
Sy faly toy izao ?
He! mihoby ..sns.}

{3.
Jesosy Tompo no nandao
Ny lapa fahizay,
Ka nahatonga azy ireo
Ho aminy ary :
He! mihoby ..sns.}

{4.
Ny lambany novàny soa,
Dia lamba tena vao,
Ny fony nosasany koa,
Ka faly izy izao :
He! mihoby ..sns.}
', '#1-X', '#1') /end

insert into song values (17781, '778. Vory izahay izao', NULL, NULL, '
{1.
Vory izahay izao
Eto anatrehanao,
Ry Jeso Tomponay!
He! faly ny fonay,
Faly, faly, faly,
He! faly ny fonay,
Avy mba hidera Anao.}

{2.
Saotra tsara entinay,
Dera koa atolotray
Anao, ry Tompo o!
He! faly .. sns.}

{3.
Zazakely izahay
Fohy ny fisainanay,
Tarihonao izao :
He! faly ..sns.}

{4.
Ela no nasianao
Soa ny mpanomponao,
Ry Tompo be fitia!
He! faly ..sns.}

{5.
Ao ny ray sy reninay,
Ary koa ny havanay,
Ataovy olonao :
He! faly ..sns.}

{6.
Zaza, ray, sy reny koa
Dia hihaona avokoa
An-danitrao ary :
He! faly ..sns.}
', '#1-X', '#1') /end

insert into song values (17791, '779. Jeso Kristy Tomponay!', NULL, NULL, '
{1.
Jeso Kristy Tomponay!
Jeso Kristy, Avotray!
Avy mba hidera Anao
Izahay mpianatrao.}

{FIV.
:,: Mihainoa, ry Jeso Tompo o! :,:
Fa ny fonay, fa ny fonay,
Entinay midera Anao.}

{2.
Zazakely izahay,
Zava-bitsy fantatray,
Koa ny mahasoa anay
Dia ankinina aminao.
[FIV.]}

{3.
Mamindrà fo aminay,
Mamelà n y helokay,
Mba hirasana aminao,
Fa izay no maminao.
[FIV.]}

{4.
Mba tahionao izahay,
Ary koa ny havanay
Atsangano ho Anao,
Fa izay no sitrakao.
[FIV.]}
', '#1-X', '#1') /end

insert into song values (17801, '780. He, dera, laza, hery', NULL, NULL, '
{FIV.
He, dera, laza, hery
Anao ry Jeso o!
Ny zaza no hilaza
Ny voninahitrao.}

{1.
Ianao Mpanjaka tia,
Ianao no Tomponay,
''zay tonga ho Mpamonjy
Sy ho Mpisolonay.
He, dera, laza, hery
Anao ry Jeso o!
Ny zaza no hilaza
Ny voninahitrao.}

{2.
Midera Anao, ry Jeso!
Anjely maro be,
Ka izahay an-tany
ta-hankalaza Anao.
Toy ''zay nataon''ny zaza
''zay tao Jerosalema,
Nitondra ny rofia
Ho fanajana Anao.
[FIV.]}

{3.
Raha mbola tsy nijaly
Ianao, ry Tompo o!
Nidera Anao ny zaza,
Ka faly Ianao;
Koa mbola hoderaina
Ianao ankehitrio,
Ry Jeso Tompo tia!
Mandrakizay doria.
[FIV.]}
', '#1-X', '#1') /end

insert into song values (17811, '781. Mijere, ry ray sy reny!', NULL, NULL, '
{1.
Mijere, ry ray sy reny!
Ireto sombinainareo ;
Aoka tsy hirenireny,
Tano toy ny ainareo ;
Aza avela ho adala
Ray sy reny o! jereo,
Aza mahafoy malala,
Fa tezao ho anareo.
Indro o! indro o!
Indro ny zanakareo, jereo!
Jeso o! Jeso o!
Indro ny zanakay mba vonjeo.}

{2.
Ray sy reny! mananara,
Aza mampanaram-po ;
Manomeza teny tsara,
Fa ny zaza hankatò :
Aza avela mba hanompa,
Aoka ho tezainareo ;
Mampahory raha manota,
Aoka tsy hafoinareo.
Indro o, ..sns.}
', '#1-X', '#1') /end

insert into song values (17821, '782. " Lazao, lazao rankizy!', NULL, NULL, '
{1.
" Lazao, lazao rankizy!
Izay nandaozan''i
Jeso ny fanjakany
Mba honina tety "
" Ry mpampianatray o!
Hamonjy sy handio
Antsika olom-bery
No nihaviany "
:,: Mifalia izao :,:
Misy avotra sy vonjy
Ho antsika aty.}

{2.
" Rankizy o, rankizy!
Mpitrosa hianao,
Ka inona no homenao
Hanefa trosanao ? "
" Ry mpampianatray o!
Ny saina amam-po
Homenay mba ho Azy
Raha velona izahay "
:,: Mifalia izao, ..sns.}

{3.
" Rankizy o, rankizy!
Hamonjy sy handray
Anao ve Jeso Tompo
Raha mino hianao ? "
" Ry mpampianatray o!
Matoky izahay
Fa efa maty Izy
Ho avotray tokoa "
:,: Mifalia izao, ..sns.}

{4.
" Rankizy! angataho
Ny indrafony izao "
" Ry Jeso o, Mpamonjy!
Mba iantrao izahay "
Ry Tompo o! ry Tompo!
Henoy, henoy izahay,
Na kely na vaventy,
Izay mitaraina izao ;
:,: Mifalia izao, ..sns.}
', '#1-X', '#1') /end

insert into song values (17831, '783. He, maharavoravo!', NULL, NULL, '
{1.
He, maharavoravo!
Miantso zaza e
Ny Andriamanitra avo,
Mpanjaka lehibe ;
Ka andeha hanatona Azy,
Fa hoy ny Tomponao :
"Mba avelao ny zaza
Hanatona Ahy izao."}

{2.
Isika mbola zaza
Kanefa tian''ny Ray,
Na dia tsy hendry aza,
Na dia tsy mba mahay!
Ka andeha, ..sns.}

{3.
Ny tahotra esory,
Ny henatra ario ;
Tsy laviny akory,
Ka manatona anio :
Ka andeha, ..sns.}

{4.
Manetry tena izy,
Ka saotra no atao;
Isika mbola ankizy
Matoky Azy izao,
Ka andeha, ..sns.}

{5.
He maharavoravo!
He, mahafaly fo!
Fa zanaky ny Avo
Isika aty anio :
Ka andeha, ..sns.}
', '#1-X', '#1') /end

insert into song values (17841, '784. Jeso be fitia', NULL, NULL, '
{1.
Jeso be fitia!
Zaza izahay,
Avy mitaraina
Mihainoa anay.}

{2.
Bodo sy adala,
/ndreo ny zanakao;
Jeso o, malala!
Aza lavinao.}

{3.
Mpiandry ondry mora!
Mba tarihonao;
Tano tsy hania
Ny ondrikelinao.}

{4.
Aza avela hitady
Laza aman-jo!
Fa Ianao irery
No mahafa-po.}

{5.
Tianao ny zaza,
Jeso! Fantatray,
Ka manolo-tena
Ho Anao ''zahay.}

{6.
Jeso be fitia
Zaza izahay,
Avy mba hidera;
Mihainoa anay.}
', '#1-X', '#1') /end

insert into song values (17851, '785. Izahay ankizy', NULL, NULL, '
{1.
Izahay ankizy
Te hidera Anao,
Ry Jeso Mpamonjy!
Ka henoy izao.}

{2.
Zanaky ny Avo,
Jeso Tomponay,
Faly sady ravo
Raha hatoninay.}

{3.
Te hifalifaly
Izahay izao ;
Hira mifamaly,
Tianay hatao.}

{4.
Te hiravoravo
Miaraka aminao
Nefa mavomavo
Ka antsoy izao.}

{5.
Te hisoka-bava
Hankalaza Anao ;
Hira lalandava
Satrinay hatao.}

{6.
Sambatra ny zaza!
Izay trotroinao ;
Na kamboty aza,
Tonga zanakao.}
', '#1-X', '#1') /end

insert into song values (17861, '786. Ny teninao Jehovah', NULL, NULL, '
{1.
Ny teninao Jehovah!
Izay ianaranay,
Dia tsara, tsy miova,
Ka ifalianay
He! Tena mananjara,
''zahay ankehitrio,
Fa lova tsara fara,
No ao amin''io.}

{2.
/reo zaza teo aloha
Dia tsy nahita izao
Tsy nisy hampifoha,
Natory foana tao;
Nahantra sady ory,
Niferina aina koa,
Tsy fantany akory
''zay tena foto-tsoa.}

{3.
Mamiratra indrindra
Ny masoandronay,
Milanto sy mirindra
Ny antom-piainanay;
Ny teninao ihany
No mahatonga izao,
He! Sambatra ny tany,
Fa mahalala Anao.}

{4.
Irinay mba hihombo
''zay ianaranay,
Ampio koa mba hitombo
Ny sainy anananay;
Ho avy zatoheny
Ny voan''ny teninao,
Hiely eran-tany
Ny voninahitrao!}
', '#1-X', '#1') /end

insert into song values (17871, '787. ''Zay mahakolokolo', NULL, NULL, '
{1.
''zay mahakolokolo
No manana ny soa.
Zahao ny tanimboly,
''zay fatratra tokoa,
Fa he /reo voninkazo
Mamerovero ao,
Toa nofy izay handazo
Andeha mba jereo.}

{2.
Ireo izay mahazatra
Handray ny voka-tsoa,
Ka tena tsy hahantra,
Tsy reraka tokoa;
Jereo ireo taninketsa
Mampanantena voa,
Fa soa tsy manam-petra
Sy hanim-be tokoa.}

{3.
''zahay, ry ray sy reny,
No voninkazo koa,
Hamerovero any
An-tokatrano soa;
Tezao mba hihatsara
Ny fofo-manitray,
Mba tsy harahin-tsento
Ny toetra anananay.}

{4.
''zahay no toy ny ketsa
Ho vanona tokoa,
Hamoa tsy misy fetra,
Sy tena hahasoa;
Ny fiangonantsika
Hahita valiny,
Ny fivavahantsika
Handroso hery koa.}
', '#1-X', '#1') /end

insert into song values (17881, '788. Ry Jeso Sakaizan''ny zaza,', NULL, NULL, '
{1.
Ry Jeso Sakaizan''ny zaza,
Kanefa Mpanjaka Malaza;
Midera ny Anaranao re
Izahay vory eto izao.
:,: Ny saotra sy dera aman-
daza
no atolotray ho Anao :,:}

{2.
Ry Jeso, malemy fanahy!
Manatona Anao tsy manahy
Ny zaza mivory aty izao,
Ekeo mba ho mponina ary.
:,: Ny saotra sy dera, ..sns.}

{3.
Fahiny Ianao tonga teto
Nanala ny rofy sy sento,
Avia mitahy anay re,
Matoky Anao izahay.
:,: Ny saotra sy dera, ..sns.}

{4.
Ry Jeso, Mpamonjy sy Zoky
Izay ampy tokoa mba ho toky,
Mifaly ny manana Anao izao,
Arovy ka aza mandao.
:,: Ny saotra sy dera, ..sns.}
', '#1-X', '#1') /end

insert into song values (17891, '789. Zaza izahay, ry Jeso o!', NULL, NULL, '
{1.
Zaza izahay, ry Jeso o!
Nefa Ianao dia tia anay;
Injany re ny feonao
Miantso hoe : " Avia! Avia!"
Mba raisonao re, mba
raisonao re, ry Tompo o!
Mba raisonao re, mba
raisonao re izahay izao.}

{2.
Zaza izahay, ry Jeso o!
Ianao taty mba zaza koa ;
Tarihonao ny hevitray
Hanahaka ny hevitrao!
Mba raisonao re, ..sns.}

{3.
Zaza izahay, ry Jeso o!
Ny tenanay malemy koa;
Kanefa he, ny herinao
Miaro ny fanahinay!
Mba raisonao re, ..sns.}
', '#1-X', '#1') /end

insert into song values (17901, '790. Jeso o, Sakaiza tsara!', NULL, NULL, '
{1.
Jeso o, Sakaiza tsara!
Be ny fitiavanao;
Tsy mba misy voarara
Tsy hihavana Aminao ;
:,: Mahagaga :,:
Ny haben''ny antranao.}

{2.
Jeso o, mba manampia!
Ny mpanompokelinao
Tsy hivily, tsy hania,
Fa hanara-dia Anao,
:,: He, malemy :,:
Nefa be ny herinao.}

{3.
Jeso o, Mpamonjy mora!
Manampia, mamonje,
Fa isan''andro, isan''ora
Misy fahavalo be
:,: ''Zay mamely :,:
Ka harovanao anie!}
', '#1-X', '#1') /end

insert into song values (17911, '791. Tonga ny hariva,', NULL, NULL, '
{1.
Tonga ny hariva,
Ka ny aizina
Efa mba madiva
Ho mpanarona.}

{2.
Jeso o, tsinjovy
Ka tahio izahay,
Eny, mba arovy
Handry fahizay!}

{3.
Maniraha anjely
Mba hiambina
Ahy zazakely
Raha maizina.}

{4.
Ary raha maraina
Mba fohazy indray
Ho madio sy tsara
Eo imasonao.}
', '#1-X', '#1') /end

insert into song values (17921, '792. Misy zaza marobe', NULL, NULL, '
{1.
Misy zaza marobe
Faly, sambatra ao ambony ;
Ny devoly lozabe
Tsy mamely azy intsony,
Izy ireo dia zazakely
Tonga naman''ny anjely.}

{2.
Mba irionao tokoa
Ny ho naman''izy ireny,
Indro, misy zava-tsoa
Ho anao raha tonga eny,
Dia afoizo sy ario
Izay rehetra tsy madio.}

{3.
Tompo, raiso izahay
Mba ho zanakao handova
Lapa soa rahatrizay
Lova tsy miovaova ;
Manan-jara aoka izany
Izay rehetra tonga any.}
', '#1-X', '#1') /end

insert into song values (17931, '793. Mba tsarovy re, rankizy', NULL, NULL, '
{1.
Mba tsarovy re, rankizy
Ao am-piangonana,
:,: Tsy hatory fa haniry
Mba ho zaza maotina :,:}

{2.
Tonga ao Jesosy Tompo,
Ka mihaino vavaka ;
:,: Ny manatona hanompo
Raisiny ho zanaka :,:}

{3.
Mba hirao ny fiderana,
Ka ezaho mafy hoa :
:,: " Arahaba re, hosana!
Ry Mpanjaka lehibe " :,:}

{4.
Eny, Tompontsika Izy,
Tompo tsara, mora fo,
:,: Ka hatoninao, rankizy,
O! hatony eram-po :,:}

{5.
Aza mba misalasala,
Atolory izao ny fo,
:,: Fa ny Tomponao malala
Faly ta-hamindra fo :,:}

{6.
Ry Mpamonjiko mahery,
Raiso aho ho Anao,
:,: Raiso ho Anao irery,
Raiso re, ka anjakao :,:}
', '#1-X', '#1') /end

insert into song values (17941, '794. Ry Jesosy tsara', NULL, NULL, '
{1.
Ry Jesosy tsara
Ao an-danitra
Izahay dia zaza
Saro-tahotra.}

{2.
Koa ny anjely
Mba irahonao
Ho anay izay kely,
Raha sitrakao.}

{3.
Dia mba tantano,
Jeso, izahay,
Ary mba fahano
Ny fanahinay.}

{4.
Ianao, Mpamonjy,
Dia tianay;
Izahay voavonjy
Tia Anao indray.}
', '#1-X', '#1') /end

insert into song values (17951, '795. Jeso o, Sakaizanay,', NULL, NULL, '
{1.
Jeso o, Sakaizanay,
Ao an-danitra aminao
Misy zaza namanay
Mifaly ravo ao.}

{2.
O, ry tia zaza o!
Mba ampio izahay
Mba hifaly aminao
Aty an-dasinay.}

{3.
Raha mbola ho aty,
Mba tantano izahay,
Ka tariho ho ary,
Ry Zoky tia anay.}

{4.
Ary raha tapitra
Ny anjaranay aty
Raisonao mba hiditra
Ao aminao ary.}
', '#1-X', '#1') /end

insert into song values (17961, '796. Henoy izany hira vao', NULL, NULL, '
{1.
Henoy izany hira vao
Ery an-danitra,
Fa teraka Jesosinao,
Ka mahafinaritra.}

{2.
Ianaronao matetika
Ny fitiavany,
Fa sambatra sy tretrika
Izay sakaizany.}

{3.
Atero zava-manitra
Sy volamenam-po
Ilay Mpanjakan-danitra,
Mpanjakantsika koa.}

{4.
Valio fitiavana
Ilay Zoky tia anao,
Ateronao fisaorana,
Derao fa Tomponao.}
', '#1-X', '#1') /end

insert into song values (17971, '797. Ry Jeso Zaza masina,', NULL, NULL, '
{1.
Ry Jeso Zaza masina,
Mitsaoka Anao izahay,
Atolotray ho hasina
Ny tena kelinay.}

{2.
Mihoby ny fanahinay
Mihaona aminao ;
Ry Zanak''Andriamanitray,
Tsy misy toa Anao.}

{3.
Ny zavatra anananay
Dia aoka ho Anao,
Ny tena sy fanahinay
Mba hasoavinao.}

{4.
Ny lanitra nafoinao,
Nilaozanao ny Ray
Mba hanavao ny olonao
Ho velona indray.}
', '#1-X', '#1') /end

insert into song values (17981, '798. Izahay zazakely manatona', NULL, NULL, '
{1.
Izahay zazakely manatona
Anao,
Ry Jeso izay teraka androany
Fa tonga hanavotra anay
Ianao,
Ka faly izahay izany ;
Mitehafa tànana
Dera no asandratra,
Mihobia, mifalia,
Fa Kristy Tompo teraka
Ho antsika!}

{2.
Izahay zazakely maniry izao
Ho tena sakaiza tokoa,
Ka avy izahay mba hanaraka
Anao
Ry Jeso Mpanjaka Tsitoha ;
Mitehafa ..sns.}

{3.
Izahay zazakely mangataka
Anao
Hamory ny zaza rehetra,
Fa be ny ankizy miandry Anao
Ho Tompon''ny tany rehetra.
Mitehafa ..sns.}
', '#1-X', '#1') /end

insert into song values (17991, '799. Zaza sambatra tokoa,', NULL, NULL, '
{1.
Zaza sambatra tokoa,
Ry Jesosy, izahay,
Fa ny Tompo Zanahary
No mamelona anay.}

{2.
Zaza sambatra tokoa,
Ry Jesosy, izahay,
Ianao nijaly mafy
Hanavotanao anay.}

{3.
Zaza sambatra tokoa,
Ry Jesosy, izahay,
Ny Fanahy Mpanadio
Manamasina anay.}

{4.
Zaza sambatra tokoa,
Ry Jesosy, izahay,
Fiarovanay tsy toha
Ianao, ry Tomponay!}
', '#1-X', '#1') /end

insert into song values (18001, '800. Haleloia, asandratray', NULL, NULL, '
{1.
Haleloia, asandratray
Ho Anao, ry Jeso o!
Resinao ny rafinay,
Fa nitsangana Ianao.}

{2.
Foana ny fasana
Fa tsy nahatana Anao;
Ny fahafatesana
Dia nofolahinao.}

{3.
Ka mihira izahay,
Fa nandresy Ianao,
Ka ho velona indray
Izahay madinikao.}

{4.
Ianao hisaoranay
Raha hitsangana izahay
Ka handao ny fasanay,
Mba handray ny lovanay.}

{5.
Dera no ventesinay
Ho Anao, ry Zokinay ;
Hoby no asandratray,
Ry Mpanjaka maminay.}
', '#1-X', '#1') /end

insert into song values (18011, '801. Izaho dia zaza', NULL, NULL, '
{1.
Izaho dia zaza
Malemy, osa fo,
Fa na izany aza,
Mba ondrin''ny Jeso.}

{2.
Ary an-danitra Izy
Mitsinjo ahy koa,
Satria ny ankizy
Dia tiany tokoa.}

{3.
Anefa ko akaiky
Jesosy mamiko,
Fa Izy dia manaiky
Ho eto amiko.}

{4.
Manafa-tahotra ahy
Ny tsorakazony,
Manaisotra ahiahy
Ny fitiavany.}

{5.
Izao tsy mbola hita
Ny endrik''Izy izay,
Fa raha ary am-pita
Hiahona izahay.}
', '#1-X', '#1') /end

insert into song values (18021, '802. Misy ny fonenana', NULL, NULL, '
{1.
Misy ny fonenana
Vita re, ho ahy,
Tany ifaliana,
Mamin''ny fanahy ;
Ao, tsy misy tahotra,
Ao ny Tompon''avotra,
Hira mahafinaritra
No ataon''ny any.}

{2.
Saotra no hataoko ao,
Faly fa nandresy ;
Hiakanjo fotsy vao,
Haingon''ny mpandresy.
Ny anjely masina
Hiaraha-monina,
Jeso Tompo marina
Hiampofo ahy.}

{3.
Izaho voavidinao,
Ry Mpamonjy tsara,
Sady namboaranao
Lova tsara fara.
Koa mba arovinao
Toy ny ondrikelinao
Izaho izay fanananao
Ry Mpiandry Tsara!}
', '#1-X', '#1') /end

insert into song values (18031, '803. Avia, ry Fanahy o!', NULL, NULL, '
{1.
Avia, ry Fanahy o!
Ho eto aminay,
Ny fanahinay mba fenoinao
Ny hasoavanao.}

{2.
Maloto ny fanahinay,
Ka mba diovinao ;
Tsy mahomby re ny
fihevitray,
Mba amboarinao.}

{3.
He, zaza osa izahay,
Ka mba tantanonao,
Be ny ratsy izay te-hitaona
anay,
Ampio ny olonao.}

{4.
O, ry Fanahy hendry ô,
Miandry izahay,
Manantena Anao hanadio ny fo
Henoy ny hatakay.}
', '#1-X', '#1') /end

insert into song values (18041, '804. O, ry Fanahy Masina!', NULL, NULL, '
{1.
O, ry Fanahy Masina!
Izahay ampionao,
Fa izahay madinika
Maniry ho Anao.}

{2.
Ny fonay dia maizina,
Ka tsy mba fantatray
Ny tena lala-marina :
Tariho izahay.}

{3.
Koa amorony fo vaovao
Izahay mpanomponao ;
Ny ratsy mba esorinao
Hitiavanay Anao.}

{4.
O, ry Fanahy Masina!
Mangataka izahay :
Mombà anay madinika,
Henoy ny hatakay.}
', '#1-X', '#1') /end

insert into song values (18051, '805. Izao rehetra izao dia', NULL, NULL, '
{1.
Izao rehetra izao dia
mankalaza Anao,
Ry Ray Mahery, Tomponay
tokoa,
Ny zava-boahary no asan-
tananao
Ka tsara indrindra sady soa.}

{2.
Miondrika aminao izahay
madinikao
Midera Anao, ry Rainay be
fitia!
Ampio izahay, ary mba
arovinao
Ny fonay mba tsy haniasia.}

{3.
Ry Rainay be fiantra, tariho
izahay
Ho tonga zaza tia ny mazava,
Hifaly ao an-danitra ao
rahatrizay
Ho sambatra any lalandava.}
', '#1-X', '#1') /end

insert into song values (18061, '806. Avia ry zaza, avia izao,', NULL, NULL, '
{1.
Avia ry zaza, avia izao,
Fa indro ny Tompo miandry
anao ;
Lazao an''i Jeso ny zava-
manjo,
Fa Izy onena ny ory am-po.}

{2.
Jesosy Mpamonjy nanavotra
anao,
Ka zaza malalany koa hianao,
Ka dia matokia tokoa
eram-po
Fa Izy Mpanjaka mahery
sy to.}

{3.
Jesosy no tsy manadino anao,
Tsarovy izany ka mba
andrandrao
Ny Tompo malala Izay be
indrafo ;
Anao re ny lova tsy mety
ho lo.}

{4.
Sasaonao ny foko,
Mpanavotra o!
Ho tonga madio sy mendrika
Anao
Ho sambatra aho hahita Anao,
Mpamonjiko tsara ao
an-danitra ao.}
', '#1-X', '#1') /end

insert into song values (18071, '807. Manatona re, rankizy,', NULL, NULL, '
{1.
Manatona re, rankizy,
Ny Mpamonjy avotrao,
Fa henoy, miantso Izy,
Izao fotoana anio izao :
" Zaza be faniasia,
Miverena hianao ;
Ondry kely, malakia,
Manaraka Ahy izao! "}

{2.
Ny devoly andosiro,
Ka ny ota ialao,
Fa ny tsara no fikiro,
Tiavo re Jesosinao ;
Miovà ka manenena
Noho ny ota izay natao,
Raisonao ny famonjena
Ka hazony hatrizao.}

{3.
Miaingà ka manompoa
An''i Jeso Tomponao,
Tanteraho avokoa
Ny anjara asanao ;
Halaviro re ny laina,
Miasà ka mazotoa,
Hatanjaho re ny saina
Mahareta, mandrosoa.}
', '#1-X', '#1') /end

insert into song values (18081, '808. Eo an-tratranao Jesosy', NULL, NULL, '
{1.
Eo an-tratranao Jesosy
Dia mahafinaritra;
Fa ny hafa mampahory
Ka tsy mahasambatra.
Raha Ianao no aro
Afaka ny tahotro,
Raiso ho Anao ny foko
Ry Jesosy tia o!}

{2.
Mampifaly ahy koa.
Jeso, ny fitianao;
Zara mamiko tokoa
''zany ampofoinao;
Na matory na mifoha
Dia eo an-tratranao;
Ry Mpamonjy Tompo soa,
Hoderaiko Ianao.}

{3.
''zaho tena tsy tamàna,
Raha lavitra Anao.
Fa mitebiteby lava
''zay tsy eo ankaikinao;
Eo an-tratranao, Jesosy,
Tsara hitoerako;
Eo ihany é! no misy
Zava-tsoa mahafa-po.}
', '#1-X', '#1') /end

insert into song values (18101, '810. Tompo manampia ny', NULL, NULL, '
{1.
Tompo manampia ny
mpianatrao,
Ary mitahia ny madinikao,
Mba ho tonga zaza sitrakao
izahay,
Zaza mankalaza ny
Mpanavotray.}

{2.
Maminay tokoa ny Anaranao,
Sambatra avokoa re ny
havanao ;
Manana anjara any aminao,
Ry Mpamonjy tsara ny
madinikao.}
', '#1-X', '#1') /end

insert into song values (18111, '811. O, rankizy izay mivory,', NULL, NULL, '
{1.
O, rankizy izay mivory,
Mba derao ny Tompo soa,
Ka ny fonao atolory,
Ny fanahy, tena koa
:,: Mba venteso re ny hira,
Ka derao ny Tomponao :,:}

{2.
O, ry Tompo be fitia,
Mba henoy ny zanakao ;
Fa miantso hoe : Avia!
Manatreha anay izao!
:,: Izay hataonay re tahio
Ho madio sy masina :,:}

{3.
Mba tolory fahendrena
Izahay madinikao,
Ka omeo ny famonjena
Hifikirana aminao ;
:,: O, tarihonao ny dia
Hahitana Anao, ry Ray! :,:}

{4.
He, ny fonay manontolo,
Izay mangetaheta re,
Dia atolotray daholo,
Aza dia lavina e!
:,: Mba onenonao ny fonay
Ho Anao mandrakizay :,:}
', '#1-X', '#1') /end

insert into song values (18121, '812. Ry Jeso Zokinay o!', NULL, NULL, '
{1.
Ry Jeso Zokinay o!
Jeso malalanay,
Henoy ny vavakay izao,
Fa zandrinao izahay.}

{2.
Ny batisa no nandio
Anay ho zandrinao,
Ka dia mba tahio
Hanaraka Anao.}

{3.
Ry Zokinay malala,
Arovy izahay,
Fa be ny manadala,
Manimba ny fonay.}

{4.
Ry Zokinay malala,
Trotroy ny zandrinao,
Rehefa mba hiala
Ho any aminao.}
', '#1-X', '#1') /end

insert into song values (18091, '809. Jeso Sakaizan''ny zaza,', NULL, NULL, '
{1.
Jeso Sakaizan''ny zaza,
Tompo koa,
Jeso Sakaiza Malaza!
Mihainoa!}

{2.
Zaza izahay mitaraina
Aminao ;
Mbola marefo ny aina,
Tanonao.}

{3.
Jeso Sakaiza malala,
Tokinay ;
Aoka mba tsy ho adala
Izahay.}

{4.
Jeso o! ampianaro
Izahay,
Jeso! avia hiaro
Ny dianay.}

{5.
Zaza izahay, ka sakano
Tsy hania ;
Kely izahay ka tantano
Jeso tia!}

{6.
Ary ny ray aman-dreny
Raisonao,
Aoka ny fony homeny
Ho Anao.}

{7.
Jeso Sakaizan''ny zaza,
Tompo koa!
Jeso Sakaiza malala
Mihainoa.}
', '#1-X', '#1') /end

insert into song values (18131, '813. Tia zaza, tia zaza', NULL, NULL, '
{1.
Tia zaza, tia zaza
Ny Jesosinay!
Lanitra nafoiny,
Zaza notrotroiny
Tia zaza, tia zaza
Ny Jesosinay!}

{2.
Trano tsara, tany tsara
Namboariny;
Trano be anjely,
Ho anay izay kely,
Trano tsara, tany tsara
Namboariny.}

{3.
Voankazo, voninkazo
Tsara fofona,
Tsy halazo intsony
Ao Edena ambony
Voankazo, voninkazo
Tsara fofona.}

{4.
Zazakely sy anjely
samy sambatra;
Hira no hirainy
Ao an-tranon-dRainy.
Zazakely sy anjely
samy sambatra.}

{5.
Seza tsara, lamba tsara
Ho antsika ao;
Hiakanjo lava
Fotsy lalandava.
Seza tsara, lamba tsara
Ho antsika ao.}

{6.
Ento mora, ento mody,
Jeso, izahay,
Ho an-tranon-dRainao,
Fa ao no tranonay o!
Ento mora, ento mody,
Jeso, izahay.}
', '#1-X', '#1') /end

insert into song values (18141, '814. Tsara re, ny lanitra', NULL, NULL, '
{1.
Tsara re, ny lanitra
Sady mahafinaritra!
Ao ny Rainay be fitia,
Ao ny zaza tsy mania,
:,: Ao ny fifaliam-be :,:}

{2.
Ondry kely izahay,
Ary tsy mba fantatray
Ny hanjo ety an-tany,
Ka maniry mba ho any
:,: Aminao, ry Tomponay :,:}

{3.
Ry Mpiandry Tsara o!
Aoka mba hamindra fo
Aminay izay mbola kely,
Mba iraho ny anjely
:,: Ho mpiambina anay :,:}
', '#1-X', '#1') /end

insert into song values (18151, '815. ''Lay ora tao Getsemane', NULL, NULL, '
{1.
''Lay ora tao Getsemane
Tsaroako tsara izao,
Fa Tompo tia sy lehibe
No ory loatra tao;
Tsaroako ''zao Getsemane
''Zay niaretanao
Tsy haiko hadinoina re
''Lay fahoriana tao}

{2.
Ny mpianatrao malalanao
Natory avokoa;
Nafoin''izao rehetra izao
Jesosy Tompo soa;
Tsaroako ''zao, sns.}

{3.
Nanjary ra ny dinitrao
Noho ny helokay;
Fadiranovana Ianao
Nisolo voina anay;
Tsaroako ''zao, sns.}

{4.
Soraty ao am-poko re,
Ho sary velona ao,
Ny sarin''i Getsemane
Zay niaretanao
Tsaroako ''zao, sns.}
', '#1-X', '#1') /end

insert into song values (18161, '816. Ny lohan''ny eklesia', NULL, NULL, '
{1.
Ny lohan''ny eklesia
Dia Jeso Tompony,
Ny rano aman-teny
No iorenany;
Nitady azy Izy
Ho ampakariny,
Nalatsany ny rany
Ho avo-tsoany.}

{2.
Na be ny firenena
Izay ihaviany,
Iray ny famonjena
Izay inoany;
Iray ny antenaina
Rehefa mby ary;
Iray ny mofon''aina
Izay fihinany.}

{3.
Na dia mba hita aza
Ny fisarahana,
Na dia tsy manan-daza
Ny Fiangonana,
Miambina ny mino,
Ka mitaraina hoe:
"Aza ela, ry Mpamonjy,
Avia mamonje!"}

{4.
Tsy ela dia ho hita
Ilay Mpampakatra
Tsy ela dia ho vita
Ny ady masina;
Ny rafy horeseny,
Ny loza tapitra,
Fa Izy ho tonga eny
Am-pitsaharana.}

{5.
Iraisany anefa
Andriamanitra
Sy ireo izay nahefa
Ny ady masina.
Ry Tompo, miarova
Anay izay mbola aty,
Hiraisanay handova
Ny lanitra aminy.}
', '#1-X', '#1') /end

insert into song values (18171, '817. Ry Andriamanitra Rainay,', NULL, NULL, '
{1.
Ry Andriamanitra Rainay,
Mihainoa! (in-3)
Miondrika eto anatrehanao
izahay,
Fa Ianao no Andriamanitray,
Ka henoy izahay!}

{2.
Ry Jeso Kristy, Zanak''
Andriamanitray,
Mamonje! (in-3)
Fa Ianao no Mpamonjy
izao tontolo izao,
Ary ny Ranao no nanavotanao
anay,
Ka vonjeo izahay!}

{3.
Ry Fanahy Masina,
Mpananatra anay!
Diovinao! (in-3)
Fa Ianao no mahay manadio
ny fonay
Mba ho tempoly mendrika
ny hitoeranao,
Diovinao izahay!}
', '#1-XI', '#1') /end

insert into song values (18181, '818. Ry Zanakondry o, Mpisolo ny mpanota', NULL, NULL, '
{Ry Zanakondry o, Mpisolo ny
mpanota!
Mamindrà fo aminay!
Ry Zanakondry o, Mpisolo ny
mpanota!
Mamindrà fo aminay!
Ry Zanakondry o Mpisolo ny
mpanota!
Omeo anay ny fiadananao!
Tompo o, mba miantrà!
Mamelà re, ny helokay
Niara nanota izahay tamin-
drazanay
Ka tsy madio, fa ratsy ny
toetray!
Ry Tompo o, aza ataonao
aminay
Araka ny fahotanay
Ka aza averina aminay ny
helokay!
Henoy ny hatakay,
Ka raisonao fifonanay!
Amen!}
', '#1-XI', '#1') /end

insert into song values (18191, '819. Andriamanitra Tompo o!', NULL, NULL, '

{Fizarana I}

{1.
Andriamanitra Tompo o!
Ray, Fanahy, Zanaka,
Mihainoa anay izao
Ry Telo Izay Iray!}

{2.
Jeso Zanak''i Maria,
Naka endrik''olona,
Tonga zaza toa anay :
Mihainoa, Jeso o!}

{3.
Notafian-dreninao
Lamban-jaza Ianao
Tao anaty vilona :
Mihainoa, Jeso o!}

{4.
Novangian''olona
''Zay niandry ondriny
Tao an-tsaham-Betlehema :
Mihainoa, Jeso o!}

{5.
Notezaina Ianao,
Notrotroin''i Simeona,
Notompoin''i Ana koa,
Mihainoa, Jeso o!}

{Fizarana II}

{6.
Amin''ny avonavona,
Amin''ny fahatezerana,
Amin''ny fialonana :
Mamonje, ry Jeso!}

{7.
Amin''ny hevi-tsy madio,
Amin''ny teny ratsy koa,
Amin''ny fahasarotam-po :
Mamonje, ry Jeso!}

{8.
Hankatò ny didinao,
Hankahala heloka,
Mba ho zanakao tokoa :
Mitariha, Jeso!}

{9.
Mba hazoto izahay,
Tia fivavahana,
Tsy ho resin''i Satana :
Mitariha, Jeso!}

{Fizarana III}

{10.
Noho ny nahaterahanao
Noho ny ranomasonao,
Noho ny fahorianao :
Mamonje, ry Jeso o!}

{11.
Noho ny satro-tsilonao,
Noho ny ra nalatsakao,
Noho ny nitsangananao
Mamonje, ry Jeso o!}

{12.
Noho ilay Anaranao
Iondrehon''olona
Sy ny ao an-danitra :
Mamonje, ry Jeso o!}

{13.
Noho ny herinao tsy toha
Noho ny voninahitrao,
Noho ny famindramponao
Mamonje, ry Jeso o!}
', '#1-XI', '#1') /end

insert into song values (18201, '820. Jeso Kristy Tomponay,', NULL, NULL, '
{1.
Jeso Kristy Tomponay,
Zanaka malalan-dRay,
No naetry ho anay ;
Mamonje anay anie.}

{2.
He! ny fahatsoranao
Sy ny fahadiovanao,
Tsy mpanota Ianao :
Mamonje anay anie.}

{3.
Ianao no marina,
Mora fo sy masina,
Ary tsy mialona :
Mamonje anay anie.}

{4.
Tsy niala sasatra,
Tsy nandà ny sarotra,
Fa nitondra henatra :
Mamonje anay anie.}

{5.
Heloka tsy rariny,
Ianao najaliny
Tao an-tampon-Kalvary :
Mamonje anay anie.}

{6.
Lasanao ny helokay,
Raha mibebaka izahay
Tsy manao ny sitrakay :
Mamonje anay anie.}

{7.
He! ny fitsangananao,
Resy ny fahavalonao
Tamim-boninahitra :
Mamonje anay anie.}

{8.
Ianao, ry Jeso o!
Anateranay izao
Saotra izay anjaranao :
Mamonje anay anie.}

{9.
Fo, fanahy, tena koa,
Ho anao mba hahasoa
Izay rehetra tia tokoa :
Mamonje anay anie.}
', '#1-XI', '#1') /end

insert into song values (18211, '821. Rainay o, Mpamorona!', NULL, NULL, '
{1.
Rainay o, Mpamorona!
Jeso o, Mpanavotra,
Ary ny Mpananatra :
Mihainoa, ry Tompo!}

{2.
Ianao no foto-tsoa
Sy harem-panahy koa,
Handrosoanay tokoa :
Mihainoa, ry Tompo!}

{3.
Be ny heloka natao,
Be ny famindramponao,
Mamelà anay izao :
Mihainoa ry Tompo!}

{4.
Malahelo izahay,
Fa maditra hatrizay,
Aza mba mandao anay :
Mihainoa ry Tompo!}

{5.
Mba diovy ny fonay,
Hatanjaho ny sainay,
Ahitsio ny dianay :
Mihainoa, ry Tompo!}

{6.
Be tokoa ny indrafo
Izay naseho ho anay,
Tena latsaka ao am-po :
Mihainoa, ry Tompo!}

{7.
Eny, famonjena re,
Hahitanay aim-be,
No efa vitanao tety :
Mihainoa, ry Tompo!}

{8.
Aina, hery, andro koa
No atolotray tokoa,
Mba hahaizanay ny soa :
Mihainoa, ry Tompo!}

{9.
Ho Anao tsy tapitra
Haja, dera, saotra koa
Ho Anao mandrakizay :
Mihainoa, ry Tompo!
Amena.}
', '#1-XI', '#1') /end

insert into song values (18221, '822. Nohariana mba ho tsara ny', NULL, NULL, '
{1.
Nohariana mba ho tsara ny
tontolo
Ka ho sambatra, ho mendrika
ny Ray
Nihasimba, simban''ota dia nisolo
Fanjakan''ny haratsiana
mahamay,
Nisy anefa Tompo izay
nampanantena
Mba hamerina ny soa ''zay efa
very
Kristy fiainana mizara famonjena,
Tokana no azo antok''Izy irery.}

{2.
Noharina mba ho iray isika mpino
Ka hanompo Tompo tokana
Tsitoha
Ndrisy anefa, fa mikipy manadino
Fa matory am-pisarahana tsy
mifoha
Nisy anefa, mbola misy Tompo
iray
Azo toky fa hamory ny mania
Kristy fiainana, hanangona indray
Ny Fiangonany hiray raha toa
minia}

{3.
Tsia, tsy nisy nohariana mba hijaly
Tsia, tsy nisy voatokana ho faly
Tsy hi-fandrafy sy hiady sanatria
No namoronana ny olona, o,
tsia!
Fa ireny vokatry ny kihotana
Ka mamikitra, tapisaka, lovàna
''Reo rehetr''ireo ho levona sy
foana
Raha Kristy Fiainana no ho
Tomponao.}
', '#1-XI', '#1') /end

insert into song values (18231, '823. Hoderaina tokoa', NULL, NULL, '
{1.
Hoderaina tokoa
Ianao, o ry Ray
Noho ny teny soa,
Izay nomenao anay
Teninao ny Baiboly
Andriamanitra o!
Teny fiainana
Ho an''izao tontolo izao.}

{2.
Fa ny Teny nomenao,
Tompo o, Ray Tsitoha,
Manadio, manavao,
Mampahery ny fo!
Teninao..., sns.}

{3.
Mitsangàna hanompo
O! ry mino izao,
Ka ny tenin''ny Tompo
No torio, ambarao.
Teninao..., sns.}

{4.
Dia tariho lalandava
Mba hiely, ry Ray,
Teny soa hanazava
Ity firenenay!
Teninao..., sns.}
', '#1-XI', '#1') /end

insert into song values (18241, '824. Ry Andriamanitra nahary,', NULL, NULL, '
{1.
Ry Andriamanitra nahary,
Isaoranay ny Anaranao
Nasehonao miharihary
Ny herin''ny fitiavanao:
Fa ny Fiangonanao najoro
Ho fiorenam-pahamarinana,
Dia vatolampy tsy hikoro
Sy andry azo ifikirana.}

{2.
Ny Teninao, Jeso Mpandresy,
No nifaharan-dRasalama
Tanaty memy nifanesy
Sy fiakarana malama:
Fa ny Fiangonanao..., sns.}

{3.
Ny tranomaizim-pijaliana,
Nampian''ny lefona mifofo,
Nanjary hiram-pihobiana
Nandresy lahatra ny nofo:
Fa ny Fiangonanao..., sns.}

{4.
Ilay nantsoina ho maritiora,
Fitarikandro nanazava,
Namerovero toy ny miora,
Nandroaka aizina hisava:
Fa ny Fiangonanao..., sns.}
', '#1-XI', '#1') /end

insert into song values (18251, '825. Raha manjavona ny andro', NULL, NULL, '
{1.
Raha manjavona ny andro
voasarona avokoa
Ny tendrombohitra avo
sy ny lohasaha koa
Sy ny renirano tsara izay
mangarana madio.
Tsy hita ankehitriny
fa manjavona anio.
Na manjavona aza anio
Nefa ao ambony ao
Ny mazava lalandava
Tsy miova na mandao
:,:Matokia, ry sakaiza
Aza kivy hianao.:,:}

{2.
Raha misava indray ny andro,
dia ho hita avo-koa
Ny tendrombohitra avo
sy ny lohasaha koa
Sady ao ny renirano
ka mangarana izao,
Ka afaka ny zavona
nanarona azy tao:
Na manjavona aza anio, sns.}

{3.
Raha mbola aty an-tany
ka vahiny izahay,
Manjavona matetika
tokoa ny andronay.
Na dia ao ny masoandro aza,
takona izy ''zao
Fa maizina ny andro,
ka tsy fantatra fa ao:
Na manjavona aza anio, sns.}

{4.
Raha tapitra ny dian''ny fanahinao
aty,
Fatiny Ka lasan''Andriamanitra
ho aminy ary,
Tsy hanjavona ny andro,
fa tsy misy rahona ao,
Hazava ery an-danitra avokoa ny
andronao.
Na manjavona aza anio, sns.}
', '#1-XI', '#1') /end

insert into song values (18261, '826. Taona tokana sarobidy', NULL, NULL, '
{1.
Taona tokana sarobidy
Ka isaorana Ilay Tsitoha
Tena taona mba fitadidy
Fa tsingerina mampifoha
Ny hafalian''ny lohantaona
No manenika izato lasy
:,:Fa naharitra ka tsy foana
Ny Baibolin''ny Malagasy.:,:}

{2.
Ny vahoakan''ny firenena
''Njao mianoka fifaliana
Fa ny Tenin''ny Famonjena
No mvimbina fitahiana
Ka manenika ny eran-tany
Sy manerana ny tontolo
:,:Ary isany tamin''izany
Ny taninay ka niova volo.:,:}

{3.
Be ny sarotra nosetraina
Be ny sakana izay nandrara
Nisy aza famoizana aina
''Zay voarakitry ny tantara.
Nefa, tao Andriamanitra
Ka ny Teniny tsy mba resy
:,:No sabatra iray maranitra
Nampahatoky sy nampandresy.:,:}

{4.
Irina indrindra ho tafatoetra
Hitondrana ny firenena,
Ho filamatra, tarigetra
Izany tenin''ny Famonjena:
Ny fitiavana hahazo laka,
Filaminana no hiseho,
:,:Fiadanana no hanjaka,
Ny fahantrana ho foana hatreo.:,:}
', '#1-XI', '#1') /end

insert into song values (18271, '827. Na mamely ny tafiotra lehibe', NULL, NULL, '
{1.
Na mamely ny tafiotra lehibe
Sy mandrivotra fatratra
Ka efa mila hahavery saina re!
Hozakaiko fa lahatra
Jeso Tompo no amiko izao
Ka ny loza resiny avokoa
Izy no mitondra ahy mba ho ao
Amin-dRainy handova ny soa.
Mandrosoa hoy ny Tompo
Mandrosoa hoy ny Tompo
Fa Izaho hitondra anao
Ka hitandrina anao
Sy hanatitra anao
Any Ziona fonenan''ny Ray.}

{2.
Na dia maizina ny lalako ety,
Sady be fahavalo koa;
Kelikely foana dia ho tonga ary
Ka ho tretrika ao tokoa.
Jeso Tompo no misakambina
Sy mihazona ny tanako roa,
Sady be fitia ka te-hiambina
Ny fanahy sy ny aiko koa.
Mandrosoa, sns...}

{3.
Reko re ny alon-drano lehibe
''Zay mamely ankehitrio;
Ka toa kivy ny fanahiko mandre,
Kanjo tsara ho ahy io.
Fa tsy tiako ny hanalavitra
''Lay Mpanafaka ny zava-manjo,
Raha toa ny saiko tsy finaritra,
Ka mangovitra mafy ny fo.
Mandrosoa, sns...}
', '#1-XI', '#1') /end

insert into song values (20011, 'Tsanta 1. Jehovah o, iza no hitoetra ao amin''ny tabernakelinao?', NULL, NULL, '
(Salamo 15)
{1.
Jehovah o, iza no hitoetra ao amin''ny tabernakelinao?
iza no honina ao an-tendrombohitrao masina?
Izay mandeha tsy misy tsiny ary manao izay mahitsy,
sady misaintsaina ny marina ny fony.}

{2.
Tsy manendrikendrika amin''ny lelany,
sady tsy manisy ratsy ny sakaizany,
ary tsy manao izay hahafa-baraka ny namany
Tsy manaja ny ratsy fanahy}

{3.
fa manome voninahitra ny matahotra an''i Jehovah;
tsy mivadika amin''ny fianianany, na dia maningotra ny tenany aza
Sady tsy mampihana ny volany, na mandray kolikoly hanameloka ny marina,
izay mitandrina izany dia tsy mba hangozohozo mandrakizay.}

{4.
Hisaorana anie ny Ray sy ny Zanaka
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandrakizay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20021, 'Tsanta 2. Jehovah no Mpiandry ahy', NULL, NULL, '
(Salamo 23)
{1.
Jehovah no Mpiandry ahy?
Tsy hanan-java-mahory aho,}

{2.
Mampandry ahy amin''ny ahi-maitso Izy;
Mitondra ahy eo amoron''ny rano fialan-tsasatra Izy.}

{3.
Mamelombelona ny fanahiko Izy;
Mitarika ahy amin''ny lalan''ny fahamarinana noho ny anarany Izy.}

{4.
Eny, na dia mandeha mamaky ny lohasaha aloky ny fahafatesana aza aho,
Dia tsy hatahotra ny loza aho.}

{5.
Fa Ianao no amiko;
Ny tsorakazonao sy ny tehinao, ireo no mahafa-tahotra ahy.}

{6.
Mamelatra latabatra eo anoloako eo imason''ny fahavaloko Ianao;
Manosotra diloilo ny lohako Ianao; }

{7.
Ny kapoakako feno dia feno.
Eny tokoa, fahasoavana sy famindram-po}

{8.
No hanaraka ahy amin''ny andro rehetra hiainako,
Dia hitoetra ao an-tranon''i Jehovah andro lava aho.
Hisaorana anie ny Ray, sns.}
', '#1-XII', '#1') /end

insert into song values (20031, 'Tsanta 3. Andriamanitra anie hamindra fo aminay sy hitahy anay', NULL, NULL, '
(Salamo 67)
{1.
Andriamanitra anie hamindra fo aminay sy hitahy anay;
hampamirapiratra ny tavany aminay anie Izy;}

{2.
Mba ho fantatra ety ambonin''ny tany ny lalanao
Sy ny famonjenao any amin''ny jentilisa rehetra.}

{3.
Hidera Anao ny firenena, Andriamanitra o;
Eny, hidera Anao ny firenena rehetra.}

{4.
Hifaly sy hihoby ny firenentsamy hafa, Satria mitsara marina ny firenena Ianao;
Ny firenen-tsamy hafa ambonin''ny tany dia entinao;}

{5.
Hidera Anao ny firenena, Andriamanitra o;
Eny, hidera Anao ny firenena rehetra.}

{6.
Ny tany efa nahavokatra;
Mitahy antsika Andriamanitra, dia Andriamanitsika.}

{7.
Mitahy antsika Andriamanitra;
Ary hatahotra Azy ny vazan-tany rehetra.
Hisaorana anie ny Ray, sns.}

', '#1-XII', '#1') /end

insert into song values (20041, 'Tsanta 4. Mihobia ho an''i Jehovah', NULL, NULL, '
(Salamo 100)
{1.
Mihobia ho an''i Jehovah,
ry tany rehetra.
Manompoa an''i Jehovah amin''ny fifaliana;
Mankanesa eo anatrehany amin''ny fihobiana.}

{2.
Aoka ho fantatrareo
fa Jehovah no Andriamanitra;
Izy no nanao antsika, ary Azy isika,
Dia olony sy ondry fiandriny.}

{3.
Midira eo amin''ny vavahadiny amin''ny fisaorana,
Ary eo an-kianjany amin''ny fiderana;
Misaora Azy,
mankalazà ny anarany,}

{4.
Fa tsara Jehovah;
Mandrakizay ny famindram-pony,
Ary mihatra amin''ny taranaka fara mandimby
ny fahamarinany.
Hisaorana anie ny Ray, sns.}

', '#1-XII', '#1') /end

insert into song values (20051, 'Tsanta 5. Misaora an''i Jehovah, ry fanahiko', NULL, NULL, '
(Salamo 103)
{1.
Misaora an''i Jehovah, ry fanahiko;
Ary izay rehetra ato anatiko, misaora ny anarany masina.
Misaora an''i Jehovah, ry fanahiko;
Ary aza misy hadinoinao ny fitahiany rehetra,}

{2.
Izay mamela ny helokao rehetra,
Izay manasitrana ny aretinao rehetra,
Izay manavotra ny ainao tsy hidina any an-davaka,
Izay manarona famindram-po sy fiantrana anao,}

{3.
Izay mahavoky soa ny vavanao;
Ny fahatanoranao mody indray toy ny an''ny voromahery
Jehovah manao fahamarinana
Sy fitsarana amin''izay rehetra ampahorina.}

{4.
Efa nampahafantatra an''i Mosesy ny lalan-kalehany Izy;
Ny Zanak''Isiraely nampahafantariny ny asany.
Mamindra fo sy miantra Jehovah,
Mahari-po sady be famindram-po.}

{5.
Tsy mandaha-teny mandrakariva Izy,
Na mitahiry fahatezerana mandrakizay.
Tsy mba manao amintsika araka ny fahotantsika Izy,
Na mamaly antsika araka ny helotsika.}

{6.
Fa tahaka ny hahavon''ny lanitra ambonin''ny tany
No haben''ny famindram-pony amin''izay matahotra Azy.
Tahaka ny halavitry ny atsinanana amin''ny andrefana
No halavitry ny anesorany ny fahotantsika amintsika.}


{7.
Tahaka ny fiantràn''ny ray ny zanany
No fiantran''i Jehovah izay matahotra Azy;
Fa Izy mahalala ny toetsika
Ka mahatsiaro fa vovoka isika.}

{8.
Tahaka ny ahitra ny andron''ny zanak''olombelona;
Tahaka ny vonin-javatra any an-tsaha ny famoniny,
Satria tsofin''ny rivotra izy ka lasa,
Ary tsy mahalala azy intsony ny fitoerany.}

{9.
Fa ny famindram-pon''i Jehovah dia hatramin''ny taloha indrindra
Ka ho mandrakizay amin''izay matahotra Azy
Ary ny fahamarinany mihatra amin''ny taranaka.
Dia amin''izay mitandrina ny fanekeny sy mahatsiaro hankatò ny didiny.}

{10.
Jehovah efa nampitoetra ny seza fiandrianany any an-danitra;
Ary ny fanjakany manapaka izao tontolo izao.
Misaora an''i Jehovah, ianareo anjeliny, dia ianareo izay mahery indrindra
Sady mankatò ny didiny Ary mihaino ny feon''ny teniny.}

{11.
Misaora an''i Jehovah, ny miaramilany rehetra,
Dia ny mpanompony izay manao ny sitrapony.
Misaora an''i Jehovah, ny asany rehetra eran''ny fanjakany;
Misaora an''i Jehovah, ry fanahiko.
Hisaorana anie ny Ray, sns.}
', '#1-XII', '#1') /end

insert into song values (20061, 'Tsanta 6. Aoka ho tonga amiko ny famindram-ponao, Jehovah ô', NULL, NULL, '
(Salamo 119 : 41 - 56)
{1. Aoka ho tonga amiko ny famindram-ponao, Jehovah ô,
Dia ny famonjenao, araka ny teninao.
Dia hamaly izay miteny ratsy ahy aho,
Satria matoky ny teninao.}

{2. Ary aza dia esorina amin''ny vavako ny teny marina;
Fa miandry ny fitsaranao aho.
Dia hitandrina ny làlanao mandrakariva aho,
Dia mandrakizay doria.}

{3. Ary handeha amin''ny malalaka aho;
Fa ny didinao no tadiaviko.
Hilaza ny teni-vavolombelonao eo anatrehan''ireo mpanjaka aho
Ka tsy ho menatra.}

{4. Ary hiravoravo amin''ny didinao aho
Satria tiako ireny.
Hasandratro ny tanako ho amin''ny didinao satria tiako ireny:
Hosaintsainiko ny didinao.}

{5. Tsarovy ny teny natao tamiko mpanomponao
Izay nampanantenanao ahy
Izao no mampiononona ahy amin''ny fahoriako;
Ny teninao no mamelona ahy.}

{6. Ny mpirehareha maniratsira ahy:
Tsy mba miala amin''ny lalànao aho.
Mahatsiaro ny fitsipikao izay hatramin''ny taloha aho, Jehovah ô,
Dia mionona aho.}

{7. Fahatezerana mirehitra no nahazo ahy
Noho ny amin''ny ratsy fanahy izay mahafoy ny lalànao.
Ny didinao no ataoko an-kira
Ao amin''ny trano fivahiniako.}

{8. Mahatsiaro ny anaranao nony alina aho, Jehovah ô,
Ka mitandrina ny lalànao.
Izao no ahy:
Mitandrina ny didinao aho.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20071, 'Tsanta 7. Tao amin''ny lalina', NULL, NULL, '
(Salamo 130)

{1. Tao amin''ny lalina
No niantsoako Anao, Jehovah ô}

{2.Tompo ô, henoy ny feoko;
Aoka ny sofinao hihaino tsara ny feon''ny fifonako.}

{3. Raha mandinika heloka Hianao, Jehovah ô,
Iza no hahajanona, Tompo ô?}

{4. Fa eo aminao no misy ny famelan-keloka,
Mba hatahorana Anao.}

{5. Niandry an''i Jehovah aho; eny, niandry Azy ny fanahiko,
Ary ny teniny no nanantenako.}

{6. Ny fanahiko miandry ny Tompo mihoatra noho ny fiandrin''ny mpiambina ny maraina,
Dia ny fiandrin''ny mpiambina ny maraina.}

{7. Manantenà an''i Jehovah, ry Isiraely;
Fa eo amin''i Jehovah no misy famindram-po sy fanavotana be;}

{8. Ary Izy no hanavotra ny Isiraely
Ho afaka amin''ny helony rehetra.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20081, 'Tsanta 8. Miderà an''i Jehovah, fa tsara Izy', NULL, NULL, '
{1. Miderà an''i Jehovah, fa tsara Izy;
Fa mandrakizay ny famindram-pony.
Midera an''Andriamanitra Avo Indrindra;
Fa mandrakizay ny famindram-pony}

{2. Miderà ny Tompon''ny tompo;
Fa mandrakizay ny famindram-pony;
Izy irery no Mpanao fahagagan-dehibe
Fa mandrakizay ny famindram-pony,}

{3. Izay nanao ny lanitra tamin''ny fahendrena
Fa mandrakizay ny famindram-pony,
Izay namelatra ny tany ho ambonin''ny rano,
Fa mandrakizay ny famindram-pony,}

{4. Izay nanao ireo fanazavana lehibe
Fa mandrakizay ny famindram-pony,
Dia ny masoandro ho mpanapaka ny andro
Fa mandrakizay ny famindram-pony,}

{5. Ary ny volana sy ny kintana ho mpanapaka ny alina
Fa mandrakizay ny famindram-pony,
Izay naneho hery namonjy antsika ho afaka tamin''ny fahavalontsika
Fa mandrakizay ny famindram-pony,}

{6. Izay manome hanina ny nofo rehetra
Fa mandrakizay ny famindram-pony;
Miderà an''Andriamanitry ny lanitra;
Fa mandrakizay ny famindram-pony.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20091, 'Tsanta 9. Haleloia. Miderà an''i Jehovah, ry any an-danitra', NULL, NULL, '
(Salamo 148)

{1. Haleloia. Miderà an''i Jehovah, ry any an-danitra;
Miderà Azy any ambony.
Miderà Azy, ry anjeliny rehetra;
Miderà Azy, ry miaramilany rehetra.}

{2. Miderà Azy, ry masoandro amam-bolana;
Miderà Azy, ry kintana mazava rehetra.
Miderà Azy, ry lanitry ny lanitra,
Ary ianareo rano ambonin''ny lanitra.}

{3. Aoka hidera ny anaran''i Jehovah ireo;
Fa Izy no nandidy, dia ary ireo.
Nampitoetra ireo ho mandrakizay doria Izy;
Nomen-dalàna ireo ka tsy mba mihoatra.}

{4. Midera an''i Jehovah eto an-tany,
Hianareo dragona sy ianareo rano lalina rehetra,
Ny afo sy ny havandra, ny oram-panala sy ny zavona,
Ny rivotra mahery izay mankatò ny teniny,}

{5. Ny tendrombohitra sy ny havoana rehetra,
Ny hazo fihinam-boa sy ny sedera rehetra,
Ny bibi-dia sy ny biby fiompy rehetra,
Ny biby mandady na mikisaka ary ny voro-manidina,}

{6. Ny mpanjaka amin''ny tany sy ny vahoaka rehetra,
Ny mpanapaka sy ny mpitsara rehetra amin''ny tany,
Na ny zatovolahy, na ny zatovovavy,
Na ny antitra, na ny tanora,}

{7. Samia midera ny anaran''i Jehovah avokoa,
Fa ny anarany ihany no misandratra;
Ambonin''ny tany sy ny lanitra
Ny voninahiny.}

{8. Ary manandratra tandroka ho an''ny olony Izy,
Dia fiderana ho an''ny olony masina rehetra,
Eny, ho an''ny Zanak''Isiraely, firenena akaiky Azy.
Haleloia.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20101, 'Tsanta 10. He! Hianareo rehetra izay mangetaheta', NULL, NULL, '
(Isaia 55:1-3 ; Isaia 55:6-8)

{1. He! Hianareo rehetra izay mangetaheta,
Mankanesa amin''ny rano!
Ary ianareo izay tsy manam-bola,
Avia, mividia, ka homàna!}

{2. Eny, avia, mividia divay sy ronono,
Nefa tsy amim-bola, na amin-karena.
Nahoana ianareo no mandany vola amin''izay tsy hanina,
Ary ny vokatry ny fisasaranareo amin''izay tsy mahavoky?}

{3. Mihainoa dia mihainoa Ahy, ka hano izay tsara,
Dia aoka hiravoravo amin''ny matavy ny fanahinareo
Atongilano ny sofinareo, ka mankanesa aty amiko,
Mihainoa, dia ho velombelona ny fanahinareo;}

{4. Ary hanao fanekena mandrakizay aminareo Aho,
Dia ny famindram-po mahatoky nampanantenaina an''i Davida.
Mitadiava an''i Jehovah, dieny mbola hita Izy,
Miantsoa Azy, dieny mbola akaiky Izy.}

{5. Aoka ny ratsy fanahy hahafoy ny lalany,
Ary ny tsy marina hahafoy ny heviny;
Ary aoka hiverina ho amin''i Jehovah ireny,
Fa hamindra fo aminy Izy.}

{6. Eny, ho amin''Andriamanitsika,
Fa hamela heloka dia hamela heloka tokoa Izy.
Fa ny fihevitro tsy fihevitrareo
Ary ny lalanareo kosa tsy mba lalako, hoy Jehovah.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20111, 'Tsanta 11. Midera anao izahay, Andriamanitra ô', NULL, NULL, '
{1. Midera anao izahay, Andriamanitra ô,
Manaiky anao ho Tompo izahay;
Ny tany rehetra dia mitsaoka Anao,
Rain''ny mandrakizay.}

{2. Midera Anao ny anjely rehetra
Ny lanitra sy ny mahery rehetra ao aminy
Midera Anao, lalandava
Ny Kerobima sy ny Serafima manao hoe:}

{3. Masina, masina, masina,
Andriamanitra, Tompon''ny maro
Manerana ny lanitra sy ny tany
Ny fiandrianan''ny voninahitrao.}

{4. Midera Anao,
Ny apostoly rehetra,
Midera Anao,
Ny mpaminany rehetra.}

{5. Midera Anao,
Ny maritiora rehetra.
Ny fiangonana eran''izao tontoloizao
Manaiky Anao.}

{6. Ho Ray manam-piandrianana tsy hitafetra,
Sy ny Zanakao-lahy tokana be voninahitra
Ary ny FanahyMasina,
Mpampionona ny fo.}

{7. Hianao no Mpanjakan''ny voninahitra,
Ry Jeso Kristy ô,
Hianao no Zanaky ny Ray,
Zanany mandrakizay,}

{8. Raha nanalo-tena hamonjy ny olona Hianao,
Dia tsy nolavinao ny hateraky ny virijina
Rahefa nandresy ny herin''ny fahafatesana Hianao
Dia novohanao ny fanjakan''ny lanitra mba hidiran''ny mino rehetra}

{9. Mipetraka ao an-tanana an kavanan''Andriamanitra Hianao,
Ao amin''ny voninahitry ny Ray.
Mino izahay fa ho avy Hianao
Ho Mpitsara anay.}

{10. Izany no ifonanay aminao hanampy anay mpanomponao,
lzay navotanao tamin''ny ranao soa indrindra.
Ataovy ho isan''ny olo-nao ''zahay
Any amin''ny voninahitra	mandrakizay.}

{11. Tompo ô,vonjeo ny olonao,
Ary tahio ny lovanao.
Manapaha azy,
Ary asandrato ho ambony mandrakizay}

{12. Isan''andro isan''andro
Dia mankalaza Anao ''zahay,
Ary mihoby ny Anaranao
Izahay mandrakizay.}

{13. Mba miarova anay, Tompo ô,
Tsy hanota izao anio izao,
Mamindrà fo aminay, Tompo ô,
Mamindrà fo aminay.}

{14. Tompo ô, aoka ho tonga aminay ny famindramponao
Araka izay itokianay Anao.
Tompo ô, Hianao, no itokiako,
Aoka tsy ho menatra aho mandrakizay}
', '#1-XII', '#1') /end

insert into song values (20121, 'Tsanta 12. Ry Raiko ô! Tariho', NULL, NULL, '
{1. Ry Raiko ô! Tariho.
Fa maizina ny alehako.
Ary be ny rahona sy ny
kotroka ao ambony:
Indro, mivezimbezina aho,
fa variana; Raiko ô
tariho aho hody re;}

{FIV.
Raiko ô! Raiko ô!
Mombà ny zanakao.}

{2. Ry Raiko ô! Tsy hita
Ny faran''ny alehako,
Ary maniry ny fitsaharana
ny foko ''zay omenao.
Raha mbola eto aho, sintony
sao mivily; Raiko ô!
Mba tano aho ho Anao;
[FIV.]}

{3. Ry Raiko ô! Tariho,
Fa be ny tsilo sy ny fandrika
Ao an-dalambe,
ka ketraka,
Nefa nantsoinao aho tsara;
Raiko ô!
Mba raisonao ny tanako:
[FIV.]}

{4. Ry Raiko ô! Manindry
Ny hazo-fijaliana
Mavesatra,
Izay entiko isan''andro;
Tohany aho, ry Raiko ô!
fa reraka aho; Raiko ô!
Mba manampia ahy ''zao:
[FIV.]}
', '#1-XII', '#1') /end

insert into song values (20131, 'Tsanta 13. Avia, aoka isika hihoby ho an''i Jehovah', NULL, NULL, '
(Salamo 95)
{1. Avia, aoka isika hihoby ho an''i Jehovah;
Aoka isika hanao feo fifaliana ho an''ny Vatolampy famonjena antsika.
Aoka hankeo anatrehany amin''ny fiderana isika;
Aoka hanao feo fifaliana ho Azy amin''ny fihirana isika.}

{2. Fa Andriamanitra lehibe Jehovah,
Ary Mpanjaka lehibe ambonin''ny andriamanitra rehetra Izy.
Eo an-tànany ny fitoerana lalina amin''ny tany;
Ary Azy ny tendrombohitra avo.}

{3. Azy ny ranomasina, fa Izy no nanao azy,
Ary ny tany maina dia noforonin''ny tànany.
Avia, ka aoka hiondrika sy hiankohoka isika;
Aoka isika handohalika eo anatrehan''i Jehovah, Mpanao antsika;}

{4. Fa Izy no Andriamanitsika;
Ary olona fiandriny sy ondry tandremany isika.
Enga anie ka hohenoinareo anio ny feony manao hoe:
Aza manamafy ny fonareo tahaka ny tao Meriba,}

{5. Sy tahaka ny tamin''ny andro tao Masa fony tany an-efitra
Izay nakan''ny razanareo fanahy Ahy,
Sy nizahany toetra Ahy,
Na dia efa hitany aza ny asako.}

{6. Efa-polo taona no nahamonamonaina Ahy tamin''izany taranaka izany,
Ka hoy Izaho: Olona maniasia amin''ny fony izy Ka tsy mahalala ny lalako;
Ka dia nianiana tamin''ny fahatezerako Aho hoe:
Tsy hiditra amin''ny fitsaharako mihitsy izy.}

{Hisaorana anie ny Ray sy ny Zanaka,
Ary ny Fanahy Masina;
Eny, hisaorana sy hankalazaina anie
Andriamanitra mandraki-zay. Amen.}
', '#1-XII', '#1') /end

insert into song values (20141, 'Tsanta 14. Ny "Amena" fito', NULL, NULL, '
{Amena, Amena, Amena, Amena, Amena, Amena, Amena.}
', '#1-XII', '#1') /end

insert into song values (30011, 'Antema 1. Masina, Masina, Masina', NULL, NULL, '
{Masina,
Masina,
Masina,
Jehovah Tompo!
Feno ny voninahitrao
izao rehetra izao,
ry Tompo o!
Amena}
', '#1-XIII', '#1') /end

insert into song values (30021, 'Antema 2. Toy ny diera maniry ny rano an''ony', NULL, NULL, '
{Toy ny diera maniry ny rano an''ony, ny foko maniry Anao. Ry fanahiko! nahoana moa no mitanondrika hianao ka mitoloko ao anatiko? Manantenà ny Tomponao.}
', '#1-XIII', '#1') /end

insert into song values (30031, 'Antema 3. Ny Tompo hajao', NULL, NULL, '
{Ny Tompo hajao noho ny fahamarinany sy ny fahamasinany. Matahora Azy re, ry vahoaka marobe! Ny voninahiny mahagaga. He ny heriny sy ny hatsarany.}
', '#1-XIII', '#1') /end

insert into song values (30041, 'Antema 4. Ary maro amin''izay matory ao', NULL, NULL, '
{:,: Ary maro, ary maro, ary maro amin''izay matory ao, matory ao, matory ao, matory ao amin''ny vovoky ny tany no hifoha, no hifoha, no hifoha.:,:}
{:,: Ary izay hendry dia hamirapiratra, ary izay hendry dia hamirapiratra, ary izay hendry dia hamirapiratra, toy ny famirapiratry ny lanitra.:,:}
{:,: Ary izay mamerina ny maro, ary izay mamerina ny maro ho amin''ny fahamarinana, dia ho tahaka ny kintana mandrakizay doria.:,: Amena.}
', '#1-XIII', '#1') /end

insert into song values (30051, 'Antema 5. Indro ny aron''ny mandanao ry Jerosalema', NULL, NULL, '
{Indro ny aron''ny mandanao,
ry Jerosalema!
Miadàna ka mandria fahizay.
Mandry tokoa ny tany izao,
ny tany izao fa Jehovah
Manampy anao,
Manampy anao,
Manampy anao,
ka aza kivy fo.
Aza mangina
Aza mangina,
fa ny faneva no atsangano.
Haleloia!
Haleloia!
Haleloia! Amena.
Haleloia!
Haleloia!
Haleloia! Amena.
Amena! Amena! Amena!
Amena!
Haleloia!
Haleloia!
Haleloia! Amena.
Haleloia!
Haleloia!
Haleloia! Amena.
Amena! Amena! Amena!
Amena!}
', '#1-XIII', '#1') /end

insert into song values (30061, 'Antema 6. Miderà, miderà, miderà an''i Jehovah', NULL, NULL, '
{Miderà, miderà, miderà an''i Jehovah. Miderà an''i Jehovah, ry firenena rehetra.}
{:,: Miderà, miderà, miderà Azy, ry olona, ry olona rehetra. Miderà Azy, ry olona rehetra. Miderà Azy, ry olona rehetra. Fa lehibe ny famindrampony amintsika, fa lehibe ny famindrampony amintsika. Ary mandrakizay, ary mandrakizay, ary mandrakizay ny fahamarinan''i Jehovah. Haleloia, Haleloia, Haleloia, Haleloia, Haleloia!:,:}
', '#1-XIII', '#1') /end

insert into song values (30071, 'Antema 7. Tamin''ny andro voalohany tamin''ny herinandro', NULL, NULL, '
{:,: Tamin''ny andro voalohany tamin''ny herinandro, dia nankany amin''ny fasana izy, nitondra zava-manitra izay efa namboariny.:,:}
{:,: Ary hitany fa indro ny vato efa voakodia niala tamin''ny fasana. Dia niditra izy, nefa tsy hitany ny fatin''i Jesosy Tompo. Ary raha mbola very hevitra tamin''izany izy ireo, indreo nisy roalahy niseho taminy, nitafy fitafiana manelanelatra.:,:}
{:,: Ary nony natahotra izy ka niankohoka, dia hoy izy roalahy taminy: Nahoana no ato amin''ny maty no itadiavanareo ny velona? Tsy ato iIzy fa efa nitsangana; tsarovy ny teny izay nataony taminareo, fony Izy mbola tany Galilia hoe: :,:}
{:,: Ny Zanakolona tsy maintsy hatolotra eo an-tànan''ny mpanota ka hohomboana amin''ny hazo fijaliana, ary, ary, hitsangana, hitsangana amin''ny andro fahatelo.:,:}
', '#1-XIII', '#1') /end

insert into song values (30081, 'Antema 8. Mandrakizay ny Anaranao no hoderaiko', NULL, NULL, '
{Mandrakizay ny Anaranao no hoderaiko eram-po, eran-tsaina. Ianao, ry Jehovah! no hoderaiko eram-po, eran-tsaina; fa tsara sady miantra Jehovah, mahatsindry fo ka tsy maika. Izao asany rehetra izao dia samy tiany sy amindrany fo. Ny Anarany lehibe hoderaiko sy hankalazaiko.}
', '#1-XIII', '#1') /end

insert into song values (30091, 'Antema 9. An''i Kristy ny fanjakana', NULL, NULL, '
{An''i Kristy ny fanjakana, Azy izao na ny hery, na ny fahefana, na ny fandresena. Azy koa na ny voninahitra, na ny fiandrianana, na ny laza. Mpanjaka mandrakizay Kristy Tomponay. Azy irery ny seza fiandrianana sy ny satro-boninahitra. Ary izao sy mandrakizay.}
', '#1-XIII', '#1') /end

insert into song values (30101, 'Antema 10. O! mihobia, ry vahoaka any Ziona! ', NULL, NULL, '
{O! mihobia, ry vahoaka any Ziona! fa indro, ny Masina Jehovah Mpanavotra, mitsangan-kitahy eo afovoanareo, hahatsara an''i Ziona mba ho tany hiadananareo. Sambatra ao Ziona fonenanareo. Endre, ny tranom-panjakana ao Ziona! Izahao. Hotantaraina hatramin''ny farany, samy hahre izay taranakareo. O! mihobia, ry vahoka any Ziona! fa indro Jehovah eo afovoanareo.}
', '#1-XIII', '#1') /end

insert into song values (30111, 'Antema 11. O, lanitra! O, tany masina!', NULL, NULL, '
{O, lanitra! O, tany masina! Raha tapitra ny asanay, dia ravo aminao. Ny mandanao no hitanay, sy vavahady soa; ny lalam-bolamena koa no hodiavinay. Fonena-mahasambatra, tsy misy heloka. Ny làlana mahazo ao no mba hizorako. O, lanitra! O, tany masina! Nahoana ny ho faty re no hatahorako? Ny lanitra honenako mandrakizay doria. O, lanitra! fonena-masina! O, lanitra! ny dianay havitrika ho tonga aminao. O, lanitra! ho tonga aminao.}
', '#1-XIII', '#1') /end

insert into song values (30121, 'Antema 12. He ny aiko', NULL, NULL, '
{He ny aiko
Tsy lalaiko
Hamonjeko ny fanahinao;
Famonjena
No nomena,
Kanefa tsipahinao.}

{Menatra aho,
Ry Mpamonjy!
Mamelà ny mpanomponao!
Fa ny tena
Sy fanahy
Omeko Anao izao.}
', '#1-XIII', '#1') /end

insert into song values (30131, 'Antema 13. Akory ny hamaroan''ny asanao, ry Jehovah o!', NULL, NULL, '
{Akory ny hamaroan''ny asanao, ry Jehovah o! Fahendrena no nanaovanao azy rehetra. Henika ny asanao izao tontolo izao. Ao ny ranomasina feno zava-manana aina, na kely na lehibe. Indro ny lanitra, feno fampirapiratan mahagaga; ary ny rehetra samy feno zavatra avokoa, ka mampiseho ny voninahitrao Andriamanitra o!}
', '#1-XIII', '#1') /end

insert into song values (30141, 'Antema 14. Haleloia!', NULL, NULL, '
{Haleloia! Izao moa ireo miakanjo fotsy lava, ary avy taiza izy ireo? Ireo no avy tamim-pahoriana lehibe, ny akanjony nosasany sy nofotsiany tamin''ny ran''ny Zanakondry! Haleloia! Ary noho izany dia mitoetra eo anoloan''ny seza fiandrianan''Andriamanitra izy, ka manompo Azy andro aman''alina eo amin''ny tempoly. Tsy ho noana na hangatsiaka intsony ireo, fa ny zanak''ondry izay eo afovoan''ny seza fiandrianana no ho Mpiandry azy, ka hitondra azy amin''ny ranon''aina, ary hofafan''Andriamanitra ny ranomasony rehetra amin''ny masony.}
', '#1-XIII', '#1') /end

insert into song values (30151, 'Antema 15. Voninahitra amin''ny avo indrindra ho an''Andriamanitra', NULL, NULL, '
{Voninahitra amin''ny avo indrindra ho an''Andriamanitra, ary fiandrianana ho Anao, ry Tompon''ny Tompo! Miankohoka eo anatrehanao izahay.}
{:,: Derao, hobio ny Anaran''i Jehovah! Azy anie ny dera sy ny laza ety an-tany ka hatrany an-danitra! Dia hamasino izahay, ry Tompo o, hitoeran''ny voninahitrao?:,:}
{Voninahitra amin''ny avo indrindra ho an''Andriamanitra, ary fiandrianana ho Anao, ry Tompon''ny Tompo! Miankohoka eo anatrehanao izahay.}
', '#1-XIII', '#1') /end

insert into song values (30161, 'Antema 16. Isaoranay, isaoranay', NULL, NULL, '
{Isaoranay, isaoranay
Jehovah Tomponay,
isaoranay, isaoranay
Andriamanitray;
Fa be fitia ny Tomponay.
Mpanjaka tia, Mpamindra fo.
Deraina Ianao.}

{Isaoranay izao
Isaoranay izao,
fa be fiantra Ianao,
Mpanjaka be fitia.}

{Isaoranay, isaoranay
Ny Tomponay.
Isaoranay, isaoranay
Ny Tomponay.}

{Ny tany aty mandray fitahiana;
Ny lanitra ary mamoaka ny soa,
Ka misy tokoa ny vokatra tsara,
Jehovah Tsitoha manambina anay.
Deraina ny Anaranao Andriamanitray.
He! faly izahay izao manao ny hiranay!
Isaoranay, isaoranay
Isaoranay, isaoranay
Andriananaharinay Mpanjaka be fitia.}

{Deraina ny Anaranao Andriamanitray.
He! faly izahay izao manao ny hiranay!
Isaoranay, isaoranay
Isaoranay, isaoranay
Andriananaharinay Mpanjaka be fitia.}

{Isaoranay, isaoranay
Jehovah Tomponay,
isaoranay, isaoranay
Andriamanitray;
Fa be fitia ny Tomponay.
Mpanjaka tia, Mpamindra fo.
Deraina Ianao.
Isaoranay, isaoranay
Ny Tomponay.
Isaoranay, isaoranay
Ny Tomponay.}
', '#1-XIII', '#1') /end

insert into song values (30171, 'Antema 17. Manopy ny masoko ho amin''ny tendrombohitra aho', NULL, NULL, '
{Manopy ny masoko ho amin''ny tendrombohitra aho. Avy aiza ny famonjena ahy? Ny famonjena ahy avy amin''i Jehovah, Izay namorona ny lanitra sy ny tany. Jehovah no Mpiaro anao. Ny Tompo no fialofanao eo amin''ny ankavananao. Ny masoandro tsy hamely anao amin''ny atoandro na ny volana nony alina. Jehovah hiaro anao amin''ny ratsy rehetra. Hiaro ny fanahinao Izy na mivoaka na miditra, hatramin''izao ka mandrakizay; Jehovah hiaro anao amin''ny ratsy rehetra hatramin''izao ka ho mandrakizay. Haleloia! Amena.}
', '#1-XIII', '#1') /end

insert into song values (30181, 'Antema 18. Saotra, dera, an''Ilay Mpanjaka Tompo Tsara', NULL, NULL, '
{Saotra, dera, an''Ilay Mpanjaka Tompo Tsara, dia Andriamanitra. Izy mitahy ny taona ka finaritra re ny vahoaka, fa Izy miaro ny loza tsy hitranga matetika. Ny ranonorana tonga hanatsara ny zavatra maro; izay fitahiana rehetra dia ateriny avokoa. Miderà, saotra no atero, fa tsy mba diso fanomezana. Izy no derao eo anatrehan''ny vahoaka maro. Miderà, saotra no atero, fa tsy mba diso fanomezana. Izy no derao re, Izy no derao eo afovoan''ny fiangonana.}
', '#1-XIII', '#1') /end

insert into song values (30191, 'Antema 19. Didy vaovao no omeko anareo, ', NULL, NULL, '
{Didy vaovao no omeko anareo, dia ny mba hifankatiavanareo. Eny, aoka ho tahaka ny nitiavako anareo, no mba hifankatiavanareo koa. Izany no hahafantaran''ny olona rehetra fa mpianatro hianareo, fa mpianatro hianareo, fa mpianatro hianareo, raha mifankatia.}
{Izao no didiko, izao no didiko, izao no didiko, dia ny mba hifankatiavanareo, tahaka ny nitiavako anareo. Tsy misy manana fitiavana lehibe, lehibe noho izao: manolotra, manolotra ny ainy hamonjy ny sakaizany.}
', '#1-XIII', '#1') /end

insert into song values (30201, 'Antema 20. Endrey ny hatsaran''trano', NULL, NULL, '
{:,: Endrey ny hatsaran''ny trano, tranonao, Andriamanitra o! Maniry anao ny foko sy fanahiko. Sambatra izay, izay mitoetra, sambatra izay mitoetra ao an-tranonao, tranonao. Hidera Anao. Tompoko : Tompoko sy Andriamanitro :,:}
{Amen.}', '#1-XIII', '#1') /end

insert into song values (30211, 'Antema 21. Jehovah o!', NULL, NULL, '
{:,:Jehovah o! Ianao no Vatolampy iorenan''ny finoanay sy fanantenanay, Jehovah o, Jehovah o!:,:}
{Mpamonjy mora azo indrindra, ka mety mba hanavotra izay manota. Jehovah o, Ianao no faleha: mijere anay, ry Tompo Masina o! Dia tafionao ny hery vaovao ny mpanomponao, ho mendrika ny asa izay atao izao, aty antranonao, noho ny fitiavanao anay. Ka ny dera aman-daza, hery voninahitra anie, ho Anao mandrakizay, Jehovah Tompo o! Jehovah o! Jehovah o! Jehovah o! Ianao no faleha: mijere anay ry Tompo Masina o!}
', '#1-XIII', '#1') /end

insert into song values (30221, 'Antema 22. Jehovah Tomponay o!', NULL, NULL, '
{Jehovah Tomponay o! Ray mandrakizay, Andriamanitra Mpanjakanay, manerana izao rehetra izao ny herinao. Tsy tratry ny fisainanay ny voninahitrao, an-danitra ao.}
{Te-hidera Anao izahay noforoninao. Kanefa tsy hainay izay vavaka ho enti-mankalaza ny fiandriananao. Fa masina sady marina, be fahendrena, Ianao Izay andohalehan''izao tontolo izao. Noho ny famindramponao izay maharitra mandrakizay, Tompo o! mba omeo ny Fanahinao Masina, hifona sy hitaraina ho anay. Amin''ny Anaran''i Jesosy Zanakao, mba henoy ny fivavakay.:,:}
{Noho ny haben''ny fahasoavanao, ary noho ny fitiavanao, jereo, Tompo o, ny Fiangonanao! Mba ampiraiso, hatanjaho, arovy izy, mba hijoro amin''ny Teninao. Ka na mafy ny tafiotra sy ny alon-dranomasina, ny Ran''ny Zanakao no hamonjy azy sy hanavotra azy. Ampitomboy ny finoany.
Ny Anaranao ankalazaina mandrakizay, sy hanefanay isan''andro ny voadinay. Mba raiso ny fisaoranay. Henoy, henoy ny vavakay, ry Tompo o!}
', '#1-XIII', '#1') /end

insert into song values (30231, 'Antema 23. Hoderaina ny Anaranao, ry Andriamanitra o', NULL, NULL, '
{Hoderaina ny Anaranao, ry Andriamanitra o! Tompo be fahasoavana, mba henoy ny vavakay! Ry Mpanjakan''ny mpanjaka o! Raiso ny fifonanay! O ry Tompo Mpampionona, mba henoy ny vavakay! Mihainoa ny vavakay, mihainoa ny vavakay!}
{Tompo tia sy Mpamindra fo! Mihainoa anay. Amen!}
', '#1-XIII', '#1') /end

insert into song values (30241, 'Antema 24. Miderà ny Tomponao', NULL, NULL, '
{:,: Miderà ny Tomponao:,:}
{Ry Jehovah Tomponay, tafavory izahay; avy hankalaza Anao izahay vahoakanao.}
{Faly, ravo izahay noho ny soa azonay; tonga mba hisaotra Anao voavotrao.}
{Saotra tsara entinay, hira no asandratray mba ho fiderana Anao Rain''izao rehetra izao.}
{(Famaranana :)}
{Derao, derao, dera no omena Anao.}
', '#1-XIII', '#1') /end
