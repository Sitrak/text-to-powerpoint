
delete from song_book where _id = '#2' /end

insert into song_book values (
'#2',
'Fihirana Fanampiny'
) /end

delete from song_chapter where chapter_book = '#2' /end

insert into song_chapter values ('#2', 'Fihirana Fanampiny #1', NULL, '#2') /end

delete from song where song_book = '#2' /end

insert into song values (40011, '1. Hira faneva faha-30 Taonan''ny FJKM', NULL, '(820. amin''ny fihirana sasan-tsasany)', '
{1.
Mitsangana ianao ry mino
fa ilain''ny taninao
Ny finoanao hifikiro,
mivonona ianao
Ny ady hatrehinao tsy mora,
tena sarotra tokoa
Nefa toy ilay tao Edoma
''lay Mahery momba anao.}

{FIV.
Mbola ho avy ny maraina
''zay mitondra hery vao
Ry mpiambina miarena
Ka ingao ny zovinao
Ambarao fa ny aizina
Indro efa lasa re
Efa tonga ny maraina,
Hisy hazavana be.}

{2.
Raha sendra ady mafy,
mila handreraka ny fo
Dia tsarovy fa mihafy,
vao mahita soa
Raha tafita ianao,
Hisy hazavana be
Ny haizin-kitroka ho lao,
tapi-dalana haleha
[FIV.]}

{3.
Fahafahana doria,
tsisy tambiny aminao
No omen''ny Tompo tia,
raha tahina ianao
Ho fanilo mandra-maty,
Ho mpitantana ny dia
''lay finoana ao anaty,
homba anao doria doria}
', '#2', '#2') /end

insert into song values (40021, '2. He! Manolo-tena', NULL, '(821. amin''ny fihirana sasan-tsasany)', '
{1.
He manolo-tena ho Anao
Fa nandre ny feo sy antsonao
''Zato tenako izay mpania
Ka raiso Tompo raiso ry Jesoa tia.}

{FIV.
Hanoa ny sitrakao, hanao ''zay tianao
Ka ho mpanompo vonona sy sahy koa
Hitory hatrany ilay fitiavanao
Ambaram-pialam-pofon''aiko Jesoa.}

{2.
Eny Tompo tsy mba mendrika aho
feno loto ny ao am-poko ao
Nefa re ''lay fitiavanao
No inoako handray ahy ho mpanomponao.
[FIV.]}

{3.
Eo anilanao no fifaliako
Ho Anao mandrakizay no tiako
Ny ho vavolombelonao Jesoa
No mamiko sy tena iriko tokoa
[FIV.]}

{4.
Fantatro fa sarotra ny diako
Marobe ny zavatra setraiko
Nefa raha Ianao no momba ahy
Dia tsy hanatahotra ny fo sy fanahy.
[FIV.]}
', '#2', '#2') /end

insert into song values (40031, '3. Jesoa vato fehizoro', NULL, '(822. amin''ny fihirana sasan-tsasany)', '
{1.
Jeso vato fehizoro,
Afa-tahotra ny fo
Raha Ianao no mampijoro,
sy iankinana tokoa
Vato fiorenanay,
Jeso zanaky ny avo
Vato, fialofanay,
Amin-dratsy te handavo.}

{FIV.
Vato azo itokiana sady tsy mba mivena
Afaka ny fahoriana
Raha Ianao no ekena
Jesoa Tompo irery
Jesoa Tompo irery
No ilay vato tsy mifindra
Jesoa Tompo irery
Jesoa Tompo irery
No ''lay fehizoro indrindra}

{2.
Jeso vato fehizoro,
mampisinda ahiahy
Fa mandresy horohoro
manatanjaka fanahy
O! ry vato tsara paika,
''Zay mandray anao am-po
Dia mijoro tsy miraika,
n''inon''inona manjo.
[FIV.]}
', '#2', '#2') /end

insert into song values (40041, '4. Ry Fanahy o!', 'RANDRIANANDRAINA', '(823. amin''ny fihirana sasan-tsasany)', '
{1.
Ry Fanahy o! midìna,
manatreha anay anio Ianao
Mba hanoloranay ny vavakay mangina
Vetsovetsom-po izay atolotray
Vola sy ny harena
samy tsy hananay
Fa indro, indro vavaka mangina
fanoloran-tena ho anao tokoa.}

{2.
Ry Fanahy o! monena
Aza mba mandao anay Ianao
Kolokoloy izahay hitombo fahendrena
Ka tsy hahafoy ny hiaraka Aminao
Ota sy rendrarendra,
samy te handavo anay
Soa ihany, soa fa tao Ianao Fanahy
Vonona hitahy ny fiainanay.}

{3.
Ry Fanahy o! meteza
Mba handray anay ho mpianatrao
Avia Ianao, avia mampahereza
Hahatonga anay mba ho irakao.
Eso sy fitsapana,
samy manakivy anay
Ka taomy, taomy hanam-po mafana
Herim-po vaovao, hiasa ho Anao.}
', '#2', '#2') /end

insert into song values (40051, '5. Izaho no fananganana', 'J. ANDRIANTSILANIHASY', '(824. amin''ny fihirana sasan-tsasany)', '
{1.
Hoy Jesosy Tompo ''zaho no fananganana
Fananganana ny maty sy fiainana
Izay rehetra mino ahy na dia maty aza}

{Dia 
ho velona indray (x3)
Satria velona aho, ho velona ianareo
Aza matahotra
Fa
ho velona indray (x3)}

{2.
Maharavo fo ny teniny, heno indray koa,
Zay rehetra velona ka mino Ahy tokoa,
Dia tsy ho faty mandrakizay hatsangako}

{Ka
ho velona indray (x3)
Satria velona aho, ho velona ianareo
Aza matahotra
Fa
ho velona indray (x3)}

{3.
O! Jesosy Tompoko, Indro re hampitomboy
Ny finoako Anao ny herinao hasehoy
Tompo iantrao ny foko, tsy finoako vonoy}

{Mba
ho velona indray (x3)
Mino Anao tokoa ny foko Jesosiko
Feno toky aho
Fa
ho velona indray (x3)

}
', '#2', '#2') /end

insert into song values (40061, '6. Ny teninao Jehovah', 'BESSA', '(825. amin''ny fihirana sasan-tsasany)', '
{1. Ny teninao Jehovah Ray mahery 
Izay nomenao ho anay 
Dia teny tsara, sady mampahery 
Ka mampifaly ny fonay.}

{FIV. 
Ny teninao mofon''aina sakafo tokoa 
Ny teninao ranon''aina satria foto-tsoa 
Ny teninao no teny tsy mba maty
Maharitra mandrakizay.}

{2. Soraty re ho velona ao anaty 
Hitondra aina vaovao 
Hitana ny finoana tsy ho lany 
Ka hankatò ny didinao. 
[FIV.]}
', '#2', '#2') /end

insert into song values (40071, '7. Tsy hainay ny hangina', NULL, '(826. amin''ny fihirana sasan-tsasany)', '
{1.
Tsy hainay ny hangina
Ka tsy hankalaza ny avo
nameno fitahiana
Ka tsy mba namela ho lavo}

{FIV.
Ka dera ry Ray,
Sy saotra no hatolotray ho Anao
Raiso (fo sy fanahy)
Fo faly loatra,
No entina midera Anao
Ny sainay tsy ampy hahalala
Ny fatran''ny fitiavanao
Ka raiso ry Tompo malala
Ny fankasitrahana Anao ry Ray.
Ka dera ry Ray,
Sy saotra no hatolotray ho Anao
Raiso (fo sy fanahy)
Fo faly loatra,
No entina midera Anao}

{2.
Tsy hainay hadinoina
''Lay fara-fitiavanao Ray o!
Fa maty novonoina
Ny zanakao hamonjy anay.
[FIV.]}
', '#2', '#2') /end

insert into song values (40081, '8. Masoko manganohano', 'Daniel RAKOTOARIVONY', '(827. amin''ny fihirana sasan-tsasany)', '
{1.
Masoko manganohano,
Nitomany feno rano
Noho ny helok''efa vita,
Dia nitony raha nahita
Masonao ry be fiantra,
ry Mpamonjy ny mahantra
O! voavotrao ''lay very,
ry fitiavana mahery.}

{2.
Tanako mangadihady
Nisarangotra nitady
Fanavotana sy Vonjy
F''efa resy ka nilonjy.
Dia noraisinao ry Tompo
Na dia tànana maloto
O voavotrao ''lay very
Ry Fitiavana mahery.}

{3.
Foko tena fahavalo
Nefa resy ka mimalo
Raha nifona fa naditra
Hitanao nipitrapitra.
Babo ka tsy afaka ho aiza
''Zay noraisinao ho sakaiza
O voavotrao ''lay very
Ry Fitiavana mahery.}
', '#2', '#2') /end

insert into song values (40091, '9. Isaorana anie Jehovah', 'RAKOTOVAZAHA', '(828. amin''ny fihirana sasan-tsasany)', '
{Isaorana anie Jehovah
Andriamanitra anie
Andriamanitry ny Israely
Isaorana anie Jehovah
Andriamanitra
Izy irery ihany no nanao
fahagagana
Isaorana mandrakizay
(deraina)
Ny Anarany malaza
Ary aoka ny tany rehetra
Ho henika (ny voninahiny)
Ny famirapiratry
ny voninahitr''i
Jehovah Ray Tsitoha
Amen !}
', '#2', '#2') /end

insert into song values (40101, '10. Mivavaka aho satria', 'RAJAOFETRA John', '(829. amin''ny fihirana sasan-tsasany)', '
{1.
Miloatra avy ao am-po
Ny tonom-bavako
Maneho ny fanajako
Sy ny fankatoavako
Ny Tompoko Tsitoha
Nitahy hatrizay
Hatramin''ny taloha
Sy ho mandrakizay.
[FIV.]}

{FIV.
Mivavaka aho satria
(tena mino)
Mino sy mankato,
Manaja sady tia
an''Andriamanitro ;
Mivavaka aho satria
(tena faly)
Faly sy manantena
Ny avotra omena
Mandrakizay doria.}

{2.
Mibahana ato am-poko
Fanahin''i Kristy ;
Mitaiza tsy miato
ka mahafaly ery ;
Manesika ahy hivoatra
Hiasa be kokoa,
Ka ataony tafahoatra
Ny hasoavako.
[FIV.]}
', '#2', '#2') /end

insert into song values (40111, '11. Faly aho', 'Henri RATSIMBAZAFY', '(830. amin''ny fihirana sasan-tsasany)', '
{Faly aho,
raha hoy izy ireo tamiko
Andeha isika ho any an-tranon''i Jehovah
Andeha isika ho any an-tranon''i Jehovah
Andeha isika ho any an-tranon''i Jehovah}

{Ny tongotray efa mijoro
Ao anatin''ny vavahadinao
Ry Jerosalema
Ry Jerosalema
(Dia ianao)
Dia ianao ry Jerosalema
(Dia ianao)
Dia ianao ry Jerosalema
Izay efa vita,
tahaka ny tanàna
Efa voalamina tsara
(Dia ianao)
Dia ianao ry Jerosalema
(Dia ianao)
Dia ianao ry Jerosalema
Izay efa vita,
tahaka ny tanàna
Efa voalamina tsara}

{Any no hiakaran''ny firenena
Ny firenen''i Jehovah
Any no hiakaran''ny firenen''i Jehovah
Mba hisaotra ny anarany
Mba hisaotra ny anarany
Mba hisaotra ny anaran''i Jehovah
Mba hisaotra ny anarany
Mba hisaotra ny anarany
Mba hisaotra ny anaran''i Jehovah.}
', '#2', '#2') /end

insert into song values (40121, '12. Am-pelatananao ry Raiko', 'Dr. ANDRIAMIADAMAHATRATRA', '(831. amin''ny fihirana sasan-tsasany)', '
{1.
Ampelatananao ry Raiko,
Ny momba momba ahy rehetra
Iriko Ianao no ho mpibaiko,
Ianao anatiko ao mitoetra
Ho andrirahoko tokoa,
hitarika ahy hatrany, hatrany
Andriafo tsy mandao Ianao
Jesoa ianao no hazavana tsy ho lany
Am-pelatananao ihany,
mandrakizay doria doria
Na ny fiainako eto an-tany, 
Na hatrary an-koatra ary.}

{2.
Ny saiko am-pelatananao
Ny foko efa tsy ahy intsony
Fa voaova ho vaovao,
dia lasanao ry Ray ambony
Ny hasambarako ombieny
Mifaly manana Anao
Ilay fiainako mpanjeninjeny
No voafehinao anatiko ao
Am-pelatananao ihany
Mandrakizay doria, doria
Na ny fiainako eto an-tany
Na hatrary an-koatra ary.}
', '#2', '#2') /end

insert into song values (40131, '13. Mihirà fihiram-baovao', 'RAMAROLAHY A.N.J.', '(832. amin''ny fihirana sasan-tsasany)', '
{Mihirà fihiram-baovao
Ho an''i Jehovah ry tany
Mihirà fihirana
Dia ambarao amin''ny
jentilisa
Ny voninahiny.}

{Mihirà fihiram-baovao
Mihirà fihirana
Ho an''i Jehovah ry tany
Dia ambarao amin''ny
jentilisa
Fa Jehovah no mendrika
Izy no mendrika ny fiderana.}
', '#2', '#2') /end

insert into song values (40141, '14. Jesoa apetrako aminao', 'RALALARIJAONA', '(833. amin''ny fihirana sasan-tsasany)', '
{1.
Jesoa apetrako Aminao
Ny fanahiako rehetra
Ny asa vita hatrizao
Anao tokoa tsy misy fetra
Jesoa apetrako Aminao}

{2.
Jesoa apetrako Aminao
Ny asa fikelezan''aina
Tsy misy miafina Aminao
Ny hetaheta tsy hay lazaina
Jesoa apetrako Aminao}

{3.
Jesoa apetrako Aminao
Ny toky sy fanantenana
Ny sitrakao no aoka hatao
Na aiza lalana ombana
Jesoa apetrako Aminao}
', '#2', '#2') /end

insert into song values (40151, '15. Raiko o!', 'Julien RAMIANDRISOA', '(834. amin''ny fihirana sasan-tsasany)', '
{1.
Tompo malala tsitoha, inty aho
Izao vao nandre ''lay feo niantso ahy
Tsapako anatiko ao ny herinao
Mitaona ahy mba hiverina Aminao
Raiko o! Raiko o! inty aho
Raiso mba ho isan''ireo zanakao
Fa izaho izay nania dia hiova
Hanaraka ny didinao Ray o!}

{2.
Jesosy zokiko o! raiso aho
Ho isan''ireo zandrinao tianao
Neniko ireny taloha fa hiova aho
Raiso aho, raiso aho, Jesoa! Ho Anao
Tompo o! Tompo o! inty aho
Raiso mba ho isan''ireo zandrinao
Fa izaho izay nania dia hiova
Hanaraka ny didinao Jesoa!}
', '#2', '#2') /end

insert into song values (40161, '16. Tsy izany no ilaina', 'BESSA', '(835. amin''ny fihirana sasan-tsasany)', '
{1.
Tsy vitam-po onena,
tsy vitan-dranomaso
Ny asam-pamonjena
Tsy azo atao mifampiandry andraso
Tsy azo ifanerena,
mila fandavan-tena
Sy hery tsy mba maty,
herim-po, herintsaina koa}

{FIV.
Na onena ny fonao
Na tomany ny maso
Tsy izany no ilaina
Fa fifankatiavana}

{2.
Ny mpianatry ny Tompo
Asainy mifanompo
Na mamy na mangidy,
miara-dia f''izany no didy
Mamonjy ny mahantra,
manarina ny lavo
mitsabo ny maratra,
ary koa tsy mba miavom-po
[FIV.]}
', '#2', '#2') /end

insert into song values (40171, '17. O ! Tompo be fitia', 'R. RABEHARINORO', '(836. amin''ny fihirana sasan-tsasany)', '
{1.
O ! Tompo be fitia
He ! Isaoranay Ianao
Fa manaiky hiara-dia
Aminay mpanomponao
''zay miasa hanasoa
Olo-maro mpiara-miaina
Manantena vokatsoa
Amin''ny asa izay efaina
[FIV.]}

{FIV.
He! talenta samihafa
No nomenao hampiasaina
Raha ireo no miara-miasa
Dia ho avy ny maraina
Ka ho maro sesehena
No hahazo tombotsoa
Hampandroso firenena
''zay kinasanay ho to.}

{2.
Taona maro izay ry Ray
No nodiavina hatrizao
Niara-nilofosanay
Mba hanao ny asanao
Nefa tsapanay tokoa
Vitsy no mba vita tsara
Ka tafio ny herim-po
Hahavita izay anjara.
[FIV.]}
', '#2', '#2') /end

insert into song values (40181, '18. Afafazo izay voa mendrika', 'RAZAFINTSEHENO M.', '(837. amin''ny fihirana sasan-tsasany)', '
{1.
Raha tonga ny andro
Izay itsaràna anao
Am''izay fotoana izay
Adinina ianao
Ka hiatrika ''lay Mpanjakanao
Efa vonona ve ianao ?
(Ary koa) Efa ampy ve
''zay anananao
Hiatrehanao ny Tomponao.
[FIV.]}

{FIV.
Fihatsarambelatsihy
Aok''izay dieny ety
Fahafaham-baraka
ho anao, ka ialao
Raha toa tianao
ho voavonjy ianao
Afafazo ''zay voa mendrika
(Mendrika).}

{2.
Raha tonga ny andro
Izay itsaràna anao
Am''izay fotoana izay
Adinina ianao
Efa ampy ve ''zay nafafinao
Mba ho voa mendrika
tokoa ?
(Ary koa) efa tontosa ve
ny anjaranao
Tsy hahameloka anao
rahatrizay.
[FIV.]}

{3.
Raha ho avy ny fotoana
hodianao
Amim-boninahitra
Ho ao an-tranon-dRay
Tsy hatahotra, na hangovitra
Ianao raha madio fo
(Ary koa)
Satroboninahitra tsy mba lo
No an''ny mpanompo mahasoa.
[FIV.]}
', '#2', '#2') /end

insert into song values (40191, '19. Raha Jehovah no mpiandry', NULL, '(838. amin''ny fihirana sasan-tsasany)', '
{1.
Raha Jehovah no mpiandry,
tsy mba hisy mampahory
Ahimaintso no handriany,
Ny fanahy velombelona
Eny tsy hanahy akory,
Lala-marina itondrany
Lala-tsambatra}

{FIV.
Raha Jehovah no mpiandry
Tsy mba hisy ''zay hampahory
Ahimaintso no handriako
Ranon''aina ho sotroiko
Ny fanahiko ho velominy
Miasa izy tsy mba matory
Lala-marina itondrany tsy ho foiko}

{2.
Ny aloky ny fasana aza,
tsy ho tazantsika intsony
Tsy hatahotra ny loza aho
Izy no miazona ahy
Tsora-kazo nitondrany,
sy ny tehiny any ambony
No manolotra ny toky ny fanahiko
[FIV.]}

{3.
Eo imasom-pahavalo,
ny latabatro velariny
Dilohilo feno hanitra,
Efa namotosany ahy
Safononoka tantely,
ny kapoakako namboariny
Fahasoavana no sisa, afaka ahiahy.
[FIV.]}

{4.
Ao an-tranonao ry Tompo,
no iriko hialofana
Ka tsy afa-miditra any
ny hetraketraky ny tany
Raha ny fonao feno antra,
no ho lamba hirakofanao
Ary ny androm-pifaliana, tsy ho lany.
[FIV.]}
', '#2', '#2') /end

insert into song values (40201, '20. Jesoa Fitiavana', 'Dr. ANDRIAMIADAMAHATRATRA', '(839. amin''ny fihirana sasan-tsasany)', '
{Efa trotraky ny adin-tsaina
Vao mba nahatsiaro Anao ry Tompo
Menatra aho tsy sahy mitaraina
Ry Jesoa fitiavana}

{Kanjo tambitamby safosafo
No nasetrinao ny ditrako
Eny tsapako fa novidinao lafo
Tamin''ny ranao ny tenako.}

{Jesoa malalako tafio ny herinao
Ahaizako mandresy ny fankam-panahy
He! tsinontsinona izao tontolo izao
Fa fanimbana tena, saina sy fanahy}

{Ho reharehako ''lay tanambokovoko
Hiamina eo anilanao anie ny foko}

{Fa eo anilanao tsy misy ny tolokoloko
Eo no mahatsara ahy Jesoa
Fa eo anilanao tsy misy ny tolokoloko
Eo no mahatsara ahy Jesoa}
', '#2', '#2') /end

insert into song values (40211, '21. Mahatoky ka tokony hekena', 'RAJAOFETRA John', '(840. amin''ny fihirana sasan-tsasany)', '
{Mahatoky ka tokony hekena
Amin''ny fankasitrahana
rehetra
Izao teny izao (2)
Tonga teny ambonin''ny tany
Kristy Jesoa ;
Hamonjy ny mpanota,
Ary amin''ireny izaho
(Izaho) (3)
Amin''ireny, izaho no lohany
Kanefa izao no namindrana fo
tamiko (2)
Dia ny mba hisehoan''i
Jesoa Kristy eo amiko,
izay lohany (2)
Ny fahari-po rehetra
no fianarana
Ho an''izay hino Azy
Hahazoana fiainana
Mandrakizay ;
Mahatoky ..sns.}
', '#2', '#2') /end

insert into song values (40221, '22. Tsy mendrika ahy', 'Pasteur RABEMANAHAKA J.', '(841. amin''ny fihirana sasan-tsasany)', '
{Tsy mendrika ahy mpanompo
ny atao hoe zanakao
Kanefa Ianao ry Tompo
no miantso ahy izao,
Izay manatona Ahy
tsy ho laviko hoy Ianao,
Dia tamy aho ka sahy,
nanatona Anao.}

{Ianao no madio
Mba diovy aho re
Ianao no marina
Mba hamarino aho re
Ianao no masina
Mba hamasino aho re
Tsy mendrika hitsimpona ny latsaka
Amin''ny latabatrao aho,
kanefa hoy Ianao hoe :
"Izay manatona Ahy,
tsy ho laviko mihintsy"
Noho ny teninao, noho ny teninao
Dia sahiko ny manantona}

{Ianao no madio
Mba diovy aho re
Ianao no marina
Mba hamarino aho re
Ianao no masina
Mba hamasino aho re
Mba diovy aho re,
Hamarino aho re
Mba diovy aho re,
hamarino aho re}

{Ianao no masina
Hamasino aho re
Mba diovy, hamarino
Hamasino re!
Mba diovy aho re,
Hamarino aho re
Mba diovy aho re,
hamarino aho re
Ianao no masina
Hamasino aho re
Mba diovy, hamarino
Hamasino re!}
', '#2', '#2') /end

insert into song values (40231, '23. Andriamanitra o !', 'RAMANOELINA, ANDRIANASOLO R.', '(842. amin''ny fihirana sasan-tsasany)', '

{1.
Andriamanitra o !
Ry Tompo Zanahary
Be fitiavana
Mamonjy sy mamelona
Vatolampy, toky tsy mifindra
Fa maharitra mandrakizay.
[FIV.]}

{FIV.
Raha ny eto an-tany
Dia mandalo ihany
Miovaova tsy azo
ianteherana,
Be no efa voa ka mamoy fo
Fa izany no fomban''ny tany
Toa fiainana raha jerena
Kanjo indrisy fa mahonena
Hasambarana miserana ihany.}

{2.
O! ry velona tsarovy,
ny andronao fa mandalo,
tsy mba manam-pijanonana
Ka tandremo sao variana
ianao
''zao tontolo izao
ho levona avokoa.
[FIV.]}

{3.
Mbola ho entin''
Andriamanitra hotsaraina
''zaho sy ianao ka tsy ho
afa-maina ;
Fa Jesoa Mpanjaka no hitsara
Ka hanavaka ny tsara
sy ny ratsy.
[FIV.]}
', '#2', '#2') /end

insert into song values (40241, '24. Mahereza! ny Tompo momba anao', 'RAMASITERA T.', '(843. amin''ny fihirana sasan-tsasany)', '
{1.
Miaingà, mazotoa
Mikeleza aina koa,
Mivavaha, mivavaha
Ho an''izao tontolo izao
Mijoroa, mitoria
Mamafaza Teny soa
Fa ny Tompo be fitia
No manohana anao
Sarotra ny lalanao
Kanefa matokia tokoa
Andrandrao ny masonao
Fa mbola ao ny Tompo soa
Izy no aminao
Ka hampandresy izao
O ! mahereza
fa ny Tompo momba anao.}

{2.
Mandrosoa, miasà
Tano ny finoanao
Aza mba matahotra,
na manahinahy izao
Miadia, mitafia
Ny firaisan-tsaina soa,
Fa ny fahavalonao
Dia handositra tokoa
Aza kivikivy foana
Fa ny Tomponao dia ao
Mampahery lalandava
Ary koa tsy mba mandao
Izy no aminao
Ka hampandresy anao
O ! mahereza
fa ny Tompo momba anao.}
', '#2', '#2') /end

insert into song values (40251, '25. Ny voadiko', 'RASOLOJAONA M.', '(844. amin''ny fihirana sasan-tsasany)', '
{1.
Ny ondry mania no maneho
anio
''zao fanekena sy voady izao
Nandinika ny zava-bita Jesoa
Tsy mendrika hanatona Anao
Kanefa tsaroako ny
FITIAVANAO
Hamonjy ity mpanomponao.}

{2.
''lay Teny mahery mananatra
Nampiova ny foko torovana
''lay Fanahy Masina inoako
Hampianatra sy hanadio
Ny ho aviko apetrako eo
Aminao
Izany no voadiko izao.}
', '#2', '#2') /end

insert into song values (40261, '26. Jesosy reharehako', 'RAJAONARIVELO Germain', '(845. amin''ny fihirana sasan-tsasany)', '
{1.
Tsy misy reharehako
Afa-tsy Ianao Jesosy o!
Anao ny hery, Anao ny haja
Sy Voninahitra
Ny fasana sy ny fiainan-tsy hita,
Indro efa resinao,
Izao tontolo izao dia nahita
Fa efa nitsangana Ianao}

{FIV.
Sambatra aho Jesoa o!
Manana Anao ''zay Tompon''ny hery
Sambatra aho Jesoa o!
Miankina eo an-tratranao
Ampy ahy ny fitiavanao
Ampy ahy ny finoako Anao
Raketiko ao am-po lalandava
Fa reharehako Ianao.}

{2.
Tsy nisy nahitam-pamonjena
Afa-tsy taminao
Nanaiky Ianao tsy mba noterena,
Hitondra aina vao
Ny tananao mahery,
Natsotranao mba hitantana
Ahaizako mandeha sy mizotra,
ny lalà-marinao.
[FIV.]}

{3.
Nasehonao ilay fahefana,
manda sy aroko
Ka niala maina teo ''lay satana
''zay fahavaloko
Tsy misy maharombaka akory
''zay eo an-tananao
Izany indrindra no hilazako,
Fa reharehako Ianao
[FIV.]}
', '#2', '#2') /end

insert into song values (40271, '27. ''lay alim-pamonjena', 'RAJOELISON', '(846. amin''ny fihirana sasan-tsasany)', '
{1.
''lay alim-pamonjena
Tsarovanao eto izao
Fa tompo be harena
No teraka tamin''io
He! mahagaga ilay fitia
Nasehon''Andriamanitra
[FIV.]}

{FIV.
He! ravo ny fo,
He! mirana koa
Mifaly (falifaly),
sambatra doria
Miventy ny hiran''ny lanitra
Haleloia !}

{2.
''lay alim-pamonjena
(famonjena)
Tsarovana eto tokoa
Fa vita fanekena
Izaho sy ny Tompo soa
Ny tenako atolotro
Ny tenany ho avotro.
[FIV.]}

{3.
''lay alim-pamonjena
Tsarovana eto tokoa
Mpanota sesehena
Mandova fiainan-tsoa
''lay alim-pivavahana
Nitondra fiadanana.
[FIV.]}

{4.
''lay alim-pamonjena
Nitondra fiadanana
Tsarovana eto tokoa
Nitondra fietrena
Tsy mba manam-paharoa
Fitiava-mampitovy zo
Ny miadana sy ory fo.
[FIV.]}
', '#2', '#2') /end

insert into song values (40281, '28. Efa voadinikao Andriamanitra o', 'ANDRIANIRINA Jean Charles', '(847. amin''ny fihirana sasan-tsasany)', '
{Efa voadinikao Andriamanitra o!
Ny fiainanay zanak''olombelona
Tsy misy ''zay miafina amiko, tsy hitanao
Ianao no manendry izany Andriamanitra o!
Indro ny fikasako, voalahatrao
Ny fiainako rehetra dia Aminao
Ny fahotana vitako taminao
Mangataka famelàna Andriamanitra o!}

{Aza avela aho hangaihay,
Fa ampioreno mafy ny finoako
Ka na mamely ahy ny fankam-panahy
Dia matoky anao ny fanahiko}

{Ny nofoko anie malemy, no sady kely
Torovana tsy ampy hery ka manahy
Meteza ianao anio Andriamanitra o!
Ho hery sy toky ho ahy mandrakizay}

{Ianao no vatolampy sy fiarovako
Ny herinao no indro, hizorako
Ny foko ao anatiko ao efa fantatrao
Dia raiso aho zanakao Andriamanitra o!}
', '#2', '#2') /end

insert into song values (40291, '29. Tsy mba misy reharehako', 'RAMAROKOTO Heritsimba Antoine', '(848. amin''ny fihirana sasan-tsasany)', '
{1. 
Tsy mba misy reharehako
Toy ny hazo nijalianao Jeso (Jesosy o!)
Eo amin''izay rehetra alehanao
Izany no voadiko aminao
He! izao rehetra izao ekeko (ry Jesoa)
Mora lo sy tsy maharitra avokoa (kanefa Jeso)
Fa ny fitiavanao jereko ry Jeso
Tsy niala tamiko.}

{2.
Tsy mba hisy fanorenako
Raha eto an-tany eto Raiko o!
Fa ny lanitrao noho fonenako
Izany no mba faniriako
He! izao tontolo izao tazaniko (tazaniko)
Rendrarendr''izay mamitaka avokoa (kanefa Ray o)
Fa ny eo anilanao, no mamiko
Ry Raiko raiso aho ho Anao.}
', '#2', '#2') /end

insert into song values (40301, '30. Fo mitambatra', 'RAKOTOARISON R.', '(849. amin''ny fihirana sasan-tsasany)', '
{1. Fo mitambatra ho iray
No entina eto izao
Mitrotro ny fitia,
Hohamasinin''Andriamanitra
Hitombo hery sy hamerovero koa
Ka hifanaiky ireto olon-droa,
fa hiombom-piainana
Na inona miseho eto an-tany
Dia vonona hifampitantana.}

{FIV.
Fitiavana, toy ny an''ilay Jesoa
Fitiavana, manolotra ny aina
Fitiavana, mandresy fahotana
Fitiavana, ho mandrakizay doria}

{2.
Toy ''zao andro anio izao
''lay nofinofy izay niainanay taloha
Ka indro, fa miara-belona,
Miara-tsambatra, anivon''ny fitia
''Lay dina izay, niarahana nikolo
No hamafisina ho doria
Notehirizina ho vaovao,
Handrobona, hitombo hasina
[FIV.]}
', '#2', '#2') /end

insert into song values (40311, '31. Mifalia ry manam-pofonaina', 'SOLOARIVELO R., SOLOARIVELO A', '(850. amin''ny fihirana sasan-tsasany)', '
{1.
Mihirà ry manam-pofonaina
Mikaloa sy mitsikia
Tapaka ny gadra novonjena
Afak''i Jesosy be fitia
''lay Edena nihidy tany aloha
(nihidy aloha)
Noho ny ota ratsin-
drazantsika
He, midanadana sy mivoha
Ka idiro hitondray mozika.}

{2.
''lay satana tena fahavalo
Resy teo ka voakodia
Koa herezo aza mimalo
Tokinao anio ilay Mesia
Ry voahosotra hitondrana
famonjena
(Ry voahosotra famonjena)
Ndreto ento ho Anao ''zahay
Dia izahay izay miferin''aina
Noho ny ota izay mpahazo
anay.}

{3.
Afaka izahay ankehitriny
Tonga ny Mpanavotra anay
Ka ilay gehenanay fahiny
Voasolo paradisa indray
Koa isaoranay tokoa tokoa
(Koa isaoranay tokoa)
Ianao ry Rainay be fitia
Dia Ianao Jehovah Ray
Tsitoha
Nanome anay Ilay Mesia.}
', '#2', '#2') /end

insert into song values (40321, '32. Aza avela hahalala', 'RANDRIAMANANTENA George', '(851. amin''ny fihirana sasan-tsasany)', '
{1.
Aza avela hahalala
Ny antony nanaovanao
Ny hariva mamanala
Ny ireo maraina vao}

{FIV.
Tsy ho takatrin''ny saiko
''Zay rehetra alahatrao
Ny angatahiko ry Raiko
Aoka aho hatoky Anao}

{2.Aza avela hanontany
Ny hafaliana atolotrao
Na ny aton''ny tomany
Efa hitako hatr''izao
[FIV.]}

{3.Aza avelanao hamefy
Ny ampitso izay Anao
Aza avelanao handrefy
''Zay ho fitantananao
[FIV.]}
', '#2', '#2') /end

insert into song values (40331, '33. Inty aho Jesoa', 'RATSIMBA Manahasaha', '(852. amin''ny fihirana sasan-tsasany)', '
{1.
Inty aho Jesoa, miantso ahy Ianao
Na dia tsy mendrika e! manolontena izao
Ianao no nifidy, tsy haiko ny handa
Ty antso sarobidy, ry Tompo mamela
Ny heloko rehetra, diovy aho havaozy
Ho olom-baovao hanao ny sitrakao
Ho mendrika tokoa, ny antsonao Jesoa}

{2.
Inty aho Jesoa, miantso ahy Ianao
Ny fahalemeko anie ry Tompo fantatrao
Ianao no nifidy sy e! naniraka ahy
Avy aminao Jeso, ny herin''ny fanahy
Ho entiko manao ny asa atolotrao
Fa sarotra e ny dia, ry Tompo manampia
Ho mendrika tokoa ny antsonao Jesoa}

{3.
Inty aho Jesoa, miantso ahy Ianao
Mba ho fahazavan''izao tontolo izao
Ianao no nifidy mba ho fanasina
Hanefa ny adidy lehibe sy masina
Koa hamasino Tompo fa vonona aho hanompo
Hombay ny herinao, hitondra aim-baovao
Ho mendrika tokoa ny antsonao Jesoa.}
', '#2', '#2') /end

insert into song values (40341, '34. Tompo Malala o!', 'J.C. ANDRIAMANANTONINA', '(853. amin''ny fihirana sasan-tsasany)', '
{1.
Tompo Malala o! mpanjakako Ianao
Taomy aho fa resy lahatra
Nohon''ny famonjena vitanao
Eny ry Tompo tsy ho haiko hadinoina
Intsony re ny fitiavana nasehonao}

{FIV.
Ka anio no hanavaozako ny fanompoko Anao
Fa tsy ho voavaliko izay natolotrao
Ny fitiavana natolotrao
Ka raiso Tompo
Ekeo re sy anjakao
Ny fiainako ho voninahitrao}

{2.Tompo Malala o! ''lay hazo fijaliana
Niaretanao tao Kalvary,
Niaretanao Mpamonjiko
No nanehoanao fa tianao
ity mpanompo izay
Tsy mendrika Anao akory ity.
[FIV.]}

{3.Tompo Malala o! tsy ho adinoiko intsony
Ny fitiavana natolotrao,
ny fitiavana natolotrao
Eny ry Tompo
inty ''zaho ''zay voababonao
Fa vonona hanao ny sitrakao.
[FIV.]}
', '#2', '#2') /end

insert into song values (40351, '35. Ry Tomponay o avia!', 'RANDRIANJAFIHARISON A.', '(854. amin''ny fihirana sasan-tsasany)', '
{1.
Ry Mpamonjiko
Ray malalako ou ou ou
Mifona Aminao ''ty Zanakao
mitalaho Tompo o !
Fa diso taminao izahay
ka mangataka ou ou ou
Famelana sy famindrampo
avy Aminao Ray o !
[FIV.]}

{FIV.
Ry Tomponay o ! avia,
omeo anay izao anio
Ny fo madio
sy masina anananao
Dia tsy ho very na hoe
Mbola ho diso ihany indray
Ny tena sy fanahy
izay anananay.}

{2.
Ry Mpamonjiko
Ray malalako ou ou ou
Be tokoa ireo ota efa vita
sy nambabo anay
Ka angatahanay Aminao
sy irinay koa ou ou ou
Famelana sy famindrampo
avy Aminao Ray o !
[FIV.]}
', '#2', '#2') /end

insert into song values (40361, '36. Mifalia', 'RAKOTOSON Pascal', '(855. amin''ny fihirana sasan-tsasany)', '
{1.
Vahoaka sesehena
No miara-paly izao
Miventy ny Antema
Dia hira vaovao
Injao fiderana
Manakoako ery
Maneho fanajana
Ho an''ilay Mesia.
[FIV.]}

{FIV.
Iza ary no tsy hifaly
(Mifalia, mihobia)
Hisaotra ao am-po
(Misaotra ao am-po)
Fa afa-doza, afa-jaly,
afaka ianao
He, famonjena tokoa
O! mihirà, mihobia
afaka ianao
Teraka ny Tompo tia,
resy ny rafinao.}

{2.
Tsy ireo Anjely ihany
No faly ao am-po
Ny mpino eto an-tany
Miravo avokoa
Fa indro ny Mpamonjy
Mpanjaka be fitia
Nietry an-tranon''omby
Hamonjy ny mania.
[FIV.]}
', '#2', '#2') /end

insert into song values (40371, '37. Fa tsy misy fahoriana', NULL, '(856. amin''ny fihirana sasan-tsasany)', '
{1.
Fa tsy misy fahoriana
''Zay natolotrao anay
Ka nitarik''hakiviana
Satria efa zakanay
Noho ''lay vesatra indrindra
Mafy sy manonja koa
Izay nentinao namindra
Raha novonoin''Ianao Jesoa
[FIV.]}

{FIV.
Ny fanirim-panahy
Hiarak''Aminao
Hamelon''ahy ho sahy
Hiasa ho Anao
Ny famonjena soa
''Zay nomenao ry Ray
No anton''ny fiainanay}

{2.
Fa tsy misy hafaliana
Tena mampifaly anay
Raha tsy ilay fitahiana
Avy Aminao ry Ray
Fa tsy misy koa fitia
Hahasambatra anay
Raha tsy Ianao ry Tompo tia
No mitoetra anatinay
[FIV.]}

{3.
Ny fisainanay ''zay kely
Tsy mahatakatr''indraindray
Fa Ianao Ianao irery
No toky iankinanay
Ny finoana mila hivaha
Tsy mahasedra sarotra
Ampijoroy tsy hanahy
Ho finoana matotra
[FIV.]}
', '#2', '#2') /end

insert into song values (40381, '38. Mba todihonao ny lasa', 'RAKOTOARITSIMBA Gildas', '(837. amin''ny fihirana sasan-tsasany)', '
{1.
Mba todihonao ny lasa
Dia ho hitanao ny asa
Nandaniana ny fotoana
Fa dia vita mora foana.
[FIV.]}

{FIV.
Mitsangàna sahia ! sahia
Mijoroa, mitoria, mitoria
Aza kivy sanatria
Fa aminao Kristy.}

{2.
Eny tsara ho tsaroana
Ireo adidy nokoloina
Maro koa no tsy tontosa
Nampitsoaka ny kanosa
[FIV.]}

{3.
Aoka hiezaka hatrany
Mba hanarina izany
Fa hipetrak''ho tantara
Ho an''ny zanak''aman-para.
[FIV.]}
', '#2', '#2') /end

insert into song values (40391, '39. Faniriako', 'Pasteur RAHAGALAHY', '(858. amin''ny fihirana sasan-tsasany)', '
{1.
Ray o! Mba faniriako, raha toa ka sitrakao
Ny handova lanitra, dieny ety an-tany
Kanefa tsy izaho irery, f''ireo ''zay mino Anao
Hiaraka amiko hatrany hatrany}

{FIV.
Endre, ny fiainanay
Anilan-dRay ny Tompo iray Fanahy iray
Iraisana avokoa
''reo asa soa
Iray ny fo, iray ny loha
Ray o! Nahoana moa e
Raha hatomboka anio?}

{2.
Ray o! Mba faniriako, raha toa ka sitrakao
Ny ho zanaka manoa, feno fahendrena
Hilofo hatrany koa hamita, ireo asanao
Ka tsy hirehareha, fa hanetry tena
[FIV.]}
', '#2', '#2') /end

insert into song values (40401, '40. La nuit du salut', 'RAJOELISON', '(859. amin''ny fihirana sasan-tsasany)', '
{1.
C''tait la nuit du salut
On s''en souvient à présent
Car le seigneur très riche
Naquit en ce moment-là
C''est un amour fort étonnant
Que Dieu a bel et bien
montré}

{FIV.
le coeur est rempli
d''immense gaieté
De joie, du bonheur éternel
On chante tout haut
la chanson du Ciel
Haleluia !}

{2.
C''tait la nuit du salut
On s''en souvient à présent
J''ai conclu avec le Seigneur
le pacte de la rédemption
A lui j''ai donné tout
mon corps
Pour moi, son sang est
mon rachat.
[FIV.]}

{3.
C''tait la nuit du salut
On s''en souvient à présent
Des pécheurs innombrables
Héritent la belle vie
Cette nuit réconciliant
Etait porteuse de la paix.
[FIV.]}

{4.
C''tait la nuit du salut
On s''en souvient à présent
Mon Jésus s''est rabaissé
Par sa supreme pitié
O ! bel amour rendant égaux
les riches et les malheureux.
[FIV.]}', '#2', '#2') /end

insert into song values (40411, '41. Jesoa no mpanavotra', 'BESSA', '(860. amin''ny fihirana sasan-tsasany)', '
{O ! iza iza ?
Iza no hamonjy ahy
Iza, iza no hamonjy ahy re ?
Fa rerak''aho e !
Fa dia reraka
Milonjy entam-pahotana
Fahotana be.}

{O ! manatona
Manatona an''i Jesoa
Fa izy ihany
Izy no hamonjy anao.}

{Fa noreseny
Efa noreseny na ny
Fasan''aza dia ny
Fahafatesana !
Ny fasan''aza !
Efa noreseny izao
Ny fahafatesana
Ilay fahavalonao
Ka mba mifalia !
Mifalia ianao
Fa Jesoa Tompo no
Mpanavotra anao.}

{Ry Jesoa Tompo !
Ry Jesoa Tompo o !
Mba raiso re ny foko
Ho anao izao !
Fa famonjena
Fanavotan-dehibe
Sy avotr''aina koa
No he atolotrao.}

{Fa novonoina !
Novonoina Ianao
Hisolo voina
Mba hisolo voina anay.}

{Fa noresenao
Efa noresenao na ny
Fasan''aza dia
Ny fahafatesana !
Ny fasan''aza !
Efa noreseny izao
Ny fahafatesana
Ilay fahavalonao
Ka mifalia re !
Ka mifaly izahay
Fa Jesoa Tompo no
Mpanavotra anay.}
', '#2', '#2') /end

insert into song values (40421, '42. Moa misy va?', 'RAKOTONIRAINY J.', '(861. amin''ny fihirana sasan-tsasany)', '
{1.
Moa misy va ety,
iriko toa Anao ?
Ry Tompo be fitia,
Mpanao izao rehetra izao
Ny foko lasanao
Ry Ray malala o !
Jereo ''ty Zanakao
Izao miantoraka Aminao.}

{2.
Moa misy va ety,
iriko toa Anao ?
Jeso sakaiza avia Ianao
hiara-dia izao
Ny foko tia Anao,
Oneno, anjakao
Tsy hatakaloko Ianao,
''ry fiainako vaovao}

{3.
Moa misy va ety,
iriko toa Anao ?
Fanahy hendry ery,
Mpanoro ''zay rehetra atao
Ny foko he ! Anao,
Fanahy Masina o !
Ny fiainako ovay ho vao,
Dia hamasinonao.}

{4.
Moa misy va ety,
iriko noho Ilay
Nesoina tany Kalvary ?
''lay indrafon''ny Ray
Jeso Malala o !
Endrey ''ty vitanao
Ianao no hetahetako
sy hasambarako.}
', '#2', '#2') /end

insert into song values (40431, '43. Aiza moa ianao?', 'HERITSIMBA A.R.', '(862. amin''ny fihirana sasan-tsasany)', '
{Jesosy Tompo miantso anao
Manontany izao
"Aiza moa ianao ?"
Ny ondrikelinao noana indreo
Miandry anao re ireo
"Aiza moa ianao ?"
[FIV.]}

{FIV.
''Ty aho Jeso, ekeo
(ekeo mba) ho irakao
Raiso hanao ny sitrakao
tokoa
Jesosy o ! (omeko)
mba ho Anao
Ny foko, tenako,
ny fanahiko.}

{E ! lalan-tsarotra ilàna anao
Ady mafy no ao,
"Aiza moa ianao ?"
Reseonao
''zao tontolo izao
Mba ho ahy re izao,
"Aiza moa ianao ?"
[FIV.]}

{Ny hery, aina sy fanananao
No ilaiko izao
"Aiza moa ianao ?"
Ny teninao sy ny fanahinao
No takiako izao
"Aiza moa ianao ?"
[FIV.]}

{Ny sandriko no hiaro anao
Hisakambina anao
"Aiza moa ianao ?"
Raha ny Fanahy
no hitantana anao
Vonona ve ianao ?
"Aiza moa ianao ?"
[FIV.]}
', '#2', '#2') /end

insert into song values (40441, '44. Ry Madagasikara soa', 'RAKOTONJANAHARY A.', '(863. amin''ny fihirana sasan-tsasany)', '
{1.
Ry Madagasikara soa
Natolo-janahary anay
Nosim-boahangy manirery
Fa nahasoa ny razanay
He ! sarobidy tsara loatra
Tsy sandana, tsy atakalo
Feno ny fo ka toa mangina
Heriny mivavaka hoe :
Andriamanitra o !
Tondrahy andon''ny lanitra
Vontosy ny tsiron''ny tany
I Madagasikara.}

{2.
Ry firenena Malagasy
Antsoin''ny Nosy hianao
Andeha malaky, mitsangàna
Alao hery, mandrosoa !
''zay mahaolona anao re
Atolory, asorony
Aza mandevina talenta
Fa anondroty Firenena e !
Andriamanitra o !
Tondrahy andon''ny lanitra
Vontosy ny tsiron''ny tany
I Madagasikara.}

{3.
Andriamanitra ambony
Fenoy ny fiahianao
Ny mponina rehetra eto
Ary izay aza mahasoa
Ireo nantsoinao hitondra
Mba tsidiho, mba tariho
Ho sambatra tokoa ny Nosy
Raha Ianao no azy e !
Andriamanitra o !
Tondrahy andon''ny lanitra
Vontosy ny tsiron''ny tany
I Madagasikara.}
', '#2', '#2') /end

insert into song values (40451, '45. Hira Fiderana', NULL, '(864. amin''ny fihirana sasan-tsasany)', '
{Hira fiderana ho anao ry Ray
Tsy afaka ao am-bavako mandrakizay
Hira mampifaly, mampitony jaly
Mamparavoravo fo}

{Ny talenta izay efa nomenao ahy
Hiderako Anao n''aiza misy ahy
Mba hamelon''aina, hampitony saina
Hampiray ny miaina}

{Hira tsotra ihany, nefa na dia izany
Ho Anao tokoa no hikaloako
Raiso re ry Tompo
F''io no porofom-pitiavako Anao}

{Hira avy ao am-poko indro fa kaloiko
Ho Anao tokoa ry Raiko, izay antsoiko
Hira teny mamy, manambara hatrany
Fa tena tiako Ianao}
', '#2', '#2') /end

insert into song values (40461, '46. Mba tantanonao ry Raiko!', 'G. ANDRIAMANANTENA', '(865. amin''ny fihirana sasan-tsasany)', '
{1.
Mba tantanonao ry Raiko
Izato lalan-janakao
Ny mifidy toa tsy haiko
Fa maniry indrindra Anao
Hitanao ny adi-tsaina
Mitambesatra ao an-doha
''zany tsikin''ny maraina
Toa ela vao hifoha.}

{2.
Moa diso ny safidiko
Raha mandroso ka mankao
Sa ''ty lalà-nofinidiko
Dia tsy ilay voasoritrao
Tanteraho ny faniriako
O ! ry Tompo tena tia
Fa ity zavatra iriko
Masina amiko doria.}

{3.
Nefa raha tsy izay no didy
Namboarinao ho ahy
Amboary ny safidy
Sy ny ratrako am-panahy
Atolorinao ny hery
Hampijoro izay nikoa
Ka sitranonao ny fery
Sy ny ratrako ao am-po.}
', '#2', '#2') /end

insert into song values (40471, '47. Fiadanana', 'ANDRIANAIVONIAINA N.', '(866. amin''ny fihirana sasan-tsasany)', '
{1.
Ianao ''zay mitady
ny fiadanantsoa
Amin''izao fiainana izao
Ka mandeha mankatsy
ary sy aroa
Mikorana ary koa milalao
Kanefa tsy mahita
izay mahasoa
Fa vizana foana ny nofo
Inty misy fiainana
atoroko anao
Andramo dia ho hitanao.
[FIV.]}

{FIV.
Jesosy, Jesosy
(Jesosy Tompo
sy Mpamonjy tia)
No fiainana atoroko
anao (Jeso)
Jesosy, Jesosy
(Jesosy Tompo
sy Mpamonjy tia)
No hany hampiadana anao
Koa raiso Jesosy
Ho Tompo Mpamonjy
Hanapaka ny fiainanao
Dia ho hitanao ''lay fiadanana
Haharitra mandrakizay.}

{2.
Ianao ianjadiam-pahoria-marobe
Amin''izao fiainana izao
Tsy misy hazavana
fa toa haizim-be
No mandrakotra
ny fiainanao
Ka mila ho kivy
sy hamoy fo
Noho ireo adim-piainana maro
Inty misy fiainana
atoroko anao
Andramo dia ho hitanao.
[FIV.]}
', '#2', '#2') /end

insert into song values (40481, '48. Andriamanitra Fitiavana', NULL, '(867. amin''ny fihirana sasan-tsasany)', '
{1.
Andriamanitra Fitiavana Ianao
Ka mihevitra ny momba anay ety
Raha mifoha maraina izahay
Dia tsy afaka am-bavanay hoe:
Ampy ho anay ny fahasoavanao.}

{FIV.
Raha maraina mibaliaka
Ny andronay ety
Ianao no masoandro niaviany
Raha ny alin''ny ririnina no handalo ety
Ianao no kintana hitantana ny dia
Ka ny vavaka atao izay mipololotra
Am-ponao ao hoe:
Izahay anie hatoky Anao.}

{2.Andriamanitra Fitiavana Ianao
Ka mitantana ny dianay ety
Raha mandimby ny andro alina
Mbola tokana ny hideranao hoe:
Ampy ho anay ny fahasoavanao.
[FIV.]}
', '#2', '#2') /end

insert into song values (40491, '49. Mifona re Ray', 'RAKOTOMALAlA J.', '(868. amin''ny fihirana sasan-tsasany)', '
{1.
Mifona re ry Ray
Ireto zanakao
Fa diso izahay
Tsy mety ny natao.}

{FIV.
Tompo o !
Vonjeo ''zahay.}

{2.
Ny ota sesilany
Nandramana avokoa
Kanefa re izany
Ifonana tokoa.
[FIV.]}

{3.
Izay rehetra hita
Natao fa tsy ho lo
Izao ny ratsy vita
Mandrotika ny fo.
[FIV.]}

{4.
Ry Jeso o ! anio
Indreto izahay
Manaiky ka fidio
Ho zanakao ry Ray.
[FIV.]}

{5.
Tsy tsapanay tokoa
Ny antsonao ry Ray
Fa hay ny zava-tsoa
Dia takona aminay.
[FIV.]}

{6.
Maniry fahafahana
Izao ''ty fo madio
Ekeo ny fibebahana
Atolotray anio.
[FIV.]}
', '#2', '#2') /end

insert into song values (40501, '50. Ianao izay miasa fatratra', 'RANDRIANANDRAINA S.', '(869. amin''ny fihirana sasan-tsasany)', '
{1.
Ianao izay miasa fatratra
Ka mikendry mba hanefa
zava-tsarotra
Moa tsy tsapanao ho
mahasambatra
Ny asa izay atao
am-pitiavana !
Mihamaivana mora entina
Ny fahasahiranana toa
mitambesatra
Raha ao ny Manaiky
marina
Ho tia sy handala
ny asa rehetra.}

{2.
Hoy ny hafatry ny Tompo
aminao
Manatona ahy raha toa
sahirana
Fa izaho tsy manary
na mandao
Izay miezaka
ka hita reraka
Nefa moa ve, vonon''izao
Handà ny tenanao,
hamoy izao tontolo izao
Dia manaiky koa mba
ho tarihina
Fa sarotra ny làlana
sy asa atao.}

{3.
Mitsangàna,
mandrosoa hiasa e !
Fa malalaka ny saha
izay omena anao
Dia meteza koa hanaiky
hanome
Harena herim-po
ilaina aminao
Aza ketraka,
matokia izao
''lay hery ''zay manohana
Ny fihezahanao
Ka mitondra soa,
manova mba ho vao
Ny vokatry ny asa
izay nanasatr''anao.}
', '#2', '#2') /end

insert into song values (40511, '51. Ry mpamonjy olom-bery', 'RAJAONAHSON J.P', '(870. amin''ny fihirana sasan-tsasany)', '

{Ry Mpamonjy olom-bery
Sarobidy sy mahery
Ny fitiavanao ahy
''zay mambabo ny fanahy
Noho ilay fitiavanao
Babonao ny foko izao
Tsy hamoizako Anao.}

{Ny fitiavanao, ry Tompo
No mambabo ahy hanompo
Tia ahy Ianao
Ka mba tiako tsotra izao
Eny, fantatrao fa tiako
Izao fiainana eto izao
Tiako izao rehetra izao
Nefa mbola tiako Ianao.}

{Na dia sarotra sy mafy
Ny hanaraka Anao
Izaho vonona hihafy
F''efa tiako Ianao
''zao fiainana izao
Tsy atakaloko Anao
Na dia mamiko aza eto
Mbola tiako Ianao.}
', '#2', '#2') /end

insert into song values (40521, '52. Avia! Torio Kristy', 'RANDRIANDRAINY F.', '(871. amin''ny fihirana sasan-tsasany)', '
{FIV.
Ry tanora o ! avia !
Mitsangàna, mitoria
Ambarao ''lay famonjena
ka sahia
Avia, avia, torio Kristy.}

{1.
Fa vory amintsika izao ny hery
Ka mendrika hanaovan-tsoa
Meteza ka ampio ''reo
mbola very
Ho mpino toa antsika koa.
[FIV.]}

{2.
Anio miandry antsika
ny tontolo
Hitondra hazavana soa
Isika izay voataiza
sy voakolo
No indro irahan''i Jesoa.
[FIV.]}

{3.
Ny herin''ny fanahy
no momba antsika
Hahatomombana ny dia
Andeha amin''ny endrika
sariaka
No hitoriana an''i Kristy."
[FIV.]}
', '#2', '#2') /end

insert into song values (40531, '53. Havaoziko ilay voady', 'D. RANOASON', '(872. amin''ny fihirana sasan-tsasany)', '
{1.
Tsy ho toy izao Jesoai ity mpanomponao
Raha toa tsy tao tokoa ny fahasoavanao
''Zay nanohana ''lay hery kely tao am-poko,
Nahazoako mba nanao ny asam-panompoako
[FIV.]}

{FIV.
Indro fa havaoziko ry Jesoa
''Lay voady miorina ao am-po
Ny andro hivelomako
Izay omenao iainako
Dia atokako sy mbola hitoriako
''Lay fitiavanao.}

{2.
Ny asa izay natao, Jesoa
Dia tsapako ao am-po
Fa tsy dia toy inona
sy bitika tokoa
Nefa na izany aza
dia inoako koa
Fa ''zay kely vitako :
hitera-bokatsoa.
[FIV.]}

{3.
Na ho lava na ho fohy
ny androko ety
''zaho dia irakao,
ry Jesoa Tompo be fitia
Ka ilay fitiavanao
miorim-paka anaty
No hany herim-pitokiana
mandra-maty !
[FIV.]}
', '#2', '#2') /end

insert into song values (40541, '54. Finoana, fanantenana, fitiavana', 'RAMAROLAHY Marka, RAMAROLAHY Lioka', '(873. amin''ny fihirana sasan-tsasany)', '
{1.
Mandrosoa hatrany
Aza kivy foana
Zava-tsoa tsy lany ao anaty
Ny finoana.}

{FIV.
''reto no miray (indray)
Hery lehibe (tokoa)
Dia ny finoana
Ny fanantenana
Ny fitiavana.}

{2.
Tano tsy ho very
Ny Fanantenana
Fa mitondra hery
Eny an-dàlana ombana.
[FIV.]}

{3.
Aina vaovao
Herin''ny fanahy
Ny fitiavanao
Ka tanako tokoa ho ahy.
[FIV.]}
', '#2', '#2') /end
