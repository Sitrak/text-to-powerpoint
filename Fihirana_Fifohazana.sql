
delete from song_book where _id = '#3' /end

insert into song_book values (
'#3',
'Fihirana Fifohazana'
) /end

delete from song_chapter where chapter_book = '#3' /end

insert into song_chapter values ('#3-I', 'FIDERANA SY FISAORANA AN''ANDRIAMANITRA', NULL, '#3'),
('#3-II', 'JESOA KRISTY TOMPONTSIKA', NULL, '#3'),
('#3-II-1', 'NY NAHATERAHANY', '#3-II', '#3'),
('#3-II-2', 'NY NIJALIANY SY NAHAFATESANY', '#3-II', '#3'),
('#3-II-3', 'NY NITSANGANANY TAMIN''NY MATY', '#3-II', '#3'),
('#3-II-4', 'NY NIAKARANY', '#3-II', '#3'),
('#3-II-5', 'FIDERANA AZY', '#3-II', '#3'),
('#3-III', 'NY FANAHY MASINA', NULL, '#3'),
('#3-IV', 'HIRA SOATANANA', NULL, '#3'),
('#3-IV-1', 'NY TELO IZAY IRAY', '#3-IV', '#3'),
('#3-V', 'SORATRA MASINA', NULL, '#3'),
('#3-VI', 'FIANTSOANA HIBEBAKA', NULL, '#3'),
('#3-VII', 'FIFONANA SY FANOLORAN-TENA', NULL, '#3'),
('#3-VIII', 'FINOANA SY FANANTENANA', NULL, '#3'),
('#3-IX', 'FIAINANA KRISTIANA', NULL, '#3'),
('#3-X', 'FITARAINANA AMIN''NY MPAMONJY', NULL, '#3'),
('#3-XI', 'FANGATAHAM-PITAHIANA', NULL, '#3'),
('#3-XII', 'FAMPORISIHANA SY FANOMEZAN-TOKY', NULL, '#3'),
('#3-XIII', 'FAMPORISIHANA HIASA HO AN''I JEHOVAH', NULL, '#3'),
('#3-XIV', 'NY LANITRA', NULL, '#3'),
('#3-XV', 'NY TOBY', NULL, '#3'),
('#3-XVI', 'HIRA SAMIHAFA', NULL, '#3'),
('#3-XVI-1', 'FANATERAM-BOKATRA', '#3-XVI', '#3'),
('#3-XVI-2', 'JOBILY', '#3-XVI', '#3'),
('#3-XVI-3', 'TANINDRAZANA', '#3-XVI', '#3'),
('#3-XVI-4', 'TAOM-BAOVAO', '#3-XVI', '#3'),
('#3-XVI-5', 'FIARAHABANA', '#3-XVI', '#3'),
('#3-XVI-6', 'FANAOVAM-BELOMA', '#3-XVI', '#3'),
('#3-XVI-7', 'FANDEVENANA', '#3-XVI', '#3'),
('#3-XVI-8', 'TSODRANO', '#3-XVI', '#3'),
('#3-XVI-9', 'HIRA FIRAVANA', '#3-XVI', '#3') /end

delete from song where song_book = '#3' /end

insert into song values (50011, '1. Derao Andriamanitra !', NULL, NULL, '
{"Derao Andriamanitra !
Derao ry vazan-tany ô !
Derao ry lanitry ny lanitra !
Derao, eny, ô derao !
Derao ny Zanany"}

{"Fa Izy no namindra fo
Sy nanome ny to,
Hanafaka ny olon''ory
Sy hamony izay very;
Derao Andriamanitra !
Derao ny Zanany"}

{"Derao ny zava-drehetra !
Derao ny ranomasin-dehibe
Derao ny tendrombohitrao !
Derao ny rahona eny amin''ny lanitra !
Derao ny voron-kely tsara feo !
Derao ry zavona amin''ny lanitra !
Derao Andriamanitra !
Derao ny Zanany"}

{"Fa he! ny voninahiny,
O ! Manerana ny tany !
Derao Andriamanitra !
Derao ny Zanany"}

{"Hobio ! ataovy manehoeho
Derao, ry vahoaka marobe
Derao tsy mijanona,
Ho an''ilay namorona"}
', '#3-I', '#3') /end

insert into song values (50021, '2. Saotra sy dera no asandratray', NULL, NULL, '
{1.
Saotra sy dera no asandratray
Mba ho Anao ry Andriamanitra ô !
Fa ainao no nanavotana anay,

Mba raisonao, ry Ray, ny saotra atolotray
Ho voninahitrao mandrakizay
(x2)}

{2.
Jesosy ô, henoy ny vavakay
Ataonay amin''ny anaranao;
Omeo hery ho enti-manao
Ny asa eo an-tanimbolinao

Anao ny asa atao sy ny iraka hanao
Ka raiso re ho voninahitrao
(x2)}

{3.
Fanahy ô, tantanonao ''zahay
Sao dia very an-dàlana izahay
Fa tano mafy hanaraka Anao,
Ka aza avela hiala aminao

Irinay fatratra ny mba ho sambatra
Sy ho finaritra mandrakizay
(x2)}
', '#3-I', '#3') /end

insert into song values (50031, '3. Miderà an''i Jehovah ry firenena rehetra !', NULL, NULL, '
{"Miderà an''i Jehovah ry firenena rehetra !
Miderà, Haleloia, Miderà, Haleloia ! miderà an''i Jehovah"}

{"Miderà an''i Jehovah ry firenena rehetra !
Miderà, Haleloia, Miderà, Haleloia ! miderà an''i Jehovah"}

{"Fa lehibe ny famindram-pony amintsika;
Ary mandrakizay ny fahamarinan''i Jehovah,
Miderà an''i Jehovah ry firenena rehetra !
Miderà, Haleloia, Miderà, Haleloia ! miderà an''i Jehovah"}

{"Voninahitra ho Anao ry Ray
Voninahitraho ho Anao ry Ray ô !
He ! falifaly ny mpanomponao
Hisaotra sy hidera ny Anaranao.
Anao Mpanjakanay,
Voninahitra ho Anao ry Ray
Voninahitraho ho Anao ry Ray ô !
He ! falifaly ny mpanomponao
Hisaotra sy hidera ny Anaranao."}
', '#3-I', '#3') /end

insert into song values (50041, '4. Tsaroanay, Jehovah ô !', NULL, NULL, '
{"Tsaroanay, Jehovah ô !
ny famindramponao,
Miaro Ianao, Miaro, mitahy Ianao,
Ka mendrika hisaorana !
Ny ainay sy ny tenanay
dia very hatrizay,
Raha tsy noho ny famindramponao :
Famindramponao lehibe tokoa !
Indreto izahay mpanomponao,
Ravo tokoa, faly tokoa,
Ka midera Anao ''zahay hoe :"}

{"Voninahitra anie ho Anao Jehovah ô !
Dera, laza, saotra, haja,
Raiso mba ho Anao"}

{"Haleloia ! Haleloia !
mandrakizay doria,
Doria, Amena"}
', '#3-I', '#3') /end

insert into song values (50051, '5. Isaorana anie Jehovah', NULL, NULL, '
{"Isaorana anie Jehovah
Noho ny soa nomeny antsika
Fa tsy tapaka isan''andro
Ny fanomezan-tsoa ;
Ny taona sy ny volana
Dia samy voalahatra ;
Ny andro sy ny alina
Dia samy tsy ho diso fitahiana"}

{"Ny lohataona sy ririnina,
Ary koa ny fararano
Sy ny fahavaratra,
Dia feno fahasoavana.
Ny tena mbola salama,
Finaritra, miadana,
Fa voky soa betsaka
Tsy tambo tononina
Asandrato re ny feo izao
Mba hidera ny Tompo be indrafo,
Voninahitra ho Anao anie,
Ry Tompo Avo indrindra ô,
Mpamonjy be fitia."}
', '#3-I', '#3') /end

insert into song values (50061, '6. Misaotra an''i Jehovah izao, ry fanahy', NULL, NULL, '
{1.
Misaotra an''i Jehovah izao, ry fanahy,
Deraonao ny Anaramasiny,
Ka tsarovy ny fitahiany ahy,
Ankalazao ary ny asany,
Fa Izy no nahary sy nitahy;
Ny herinao dia fanomezany;
Misaotra an''i Jehovah izao, ry fanahy
Derao ny Anaramasiny.}

{2.
Ny heloko dia navelany maina
Ny Zanany maty ho soloko
Ka hianao dia tolorany aina
Sy novàny ho olona vaovao;
Ny vavanao dia vokisany soa;
Ny hatanoranao, he! mody indray,
Ka hianao dia lalainy tokoa,
Tahaka ny zanaka tian''ny Ray}

{3.
He ! Jehovah no Ray
izay be fiantra,
Ka tondraka ny famindrany fo;
Ny fahamarinany dia mihatra
Amin''ny taranaka mpankato.
''Zany fitiavany tsy misy fetra !
Tsy tratry ny saina heverina :
Mankasitraha Azy, ry tany rehetra;
Derao Izy, ry olo-masina.}

{4.
Misaora an''i Jehovah re, ry anjely
Mihobia izao rehetra izao
Misaora Azy, na lehibe re na kely
Manandrata ny voninahiny,
Ianareo ry miaramila sahy;
Milazà ny fiandrianany !
Misaora an''i Jehovah izao ry fanahy,
Deraonao ny Anaramasiny}
', '#3-I', '#3') /end

insert into song values (50071, '7. Mifohaza', NULL, NULL, '
{"Mifohaza
Mifohaza ka mihirà
Dera ho an''i Jehovah"}

{"Mifohaza marina ! mifohaza,
Mifohaza marina"
Ka aza manota.}

{"Mifohaza marina ! mifohaza,
Mifohaza marina"
Ka aza manota.}

{"Ianareo izay mandeha
Ho ao an-tanàna,
HO an-tanànan''i Ziona,
Mifalia isan''andro
Amin''i Kristy Mpamonjy"}

{"Ho rentsika faingana Izy
Hilaza hoe : Avia re, Avia re !
Izay notahin''ny Raiko
Handova ny fanjakany"}

{"Mifohaza marina ! mifohaza,
Mifohaza marina"
Ka aza manota.
"Mifohaza marina ! mifohaza,
Mifohaza marina"
Ka aza manota.}
', '#3-I', '#3') /end

insert into song values (50081, '8. Feno fahasoavana izao rehetra izao', NULL, NULL, '
{"Feno fahasoavana
izao rehetra izao,
Ka miara-mifaly avokoa
Izao tontolo izao,
Ka miderà ny Tomponao
Fa Izy no Mpiaro mahery
Fa Izy no Mpiaro anao"}

{"Fiderana no asandratray re,
Ka raiso ho Anao izao,
Ry Tomponay"}

{"Fiderana no asandratray,
Fa olon''osa, fa olon''osa izahay.
Ka tsy manan-kery,
Afa-tsy aminao :
Ianao no Tokinay.
Eny, fa Ianao tokoa no Rainay !}

{FIV.
Fiderana no asandratray,
Fa olon''osa, fa olon''osa izahay.
Ka tsy manan-kery,
Afa-tsy aminao :
Ianao no Tokinay"}

{"O ! mandrosoa, hoy ny Tompo,
Tompo Jesosy,
Avy miantso anao izao,

O ! A ! dia mandrosoa,
O ! mandrosoa, hoy ny Tompo,
Tompo Jesosy
Avy miantso Anao izao"
[FIV.]}

{Ianao no tokinay}
', '#3-I', '#3') /end

insert into song values (50091, '9. Zava-tsoa lehibe tokoa', NULL, NULL, '
{"Zava-tsoa lehibe tokoa
{"Zava-tsoa lehibe tokoa
No efa noraisintsika izao,
Fa zava-tsoa lehibe
No efa noraisintsika izao"}

{"Isaorana anie
Isaoranay anie ny Jehovah,
Isaoranay anie ny Jehovah Tsitoha"}

{"Nitahy anie Izy ka azo izay nirina
Ary nanolotra Izy ka nandray an-kafaliana
Ary nanolotra Izy ka nandray an-kafaliana
Ny zanak''olombelona"}
', '#3-I', '#3') /end

insert into song values (50101, '10. Asandrato avo re ny hiranao', NULL, NULL, '
{1.
Asandrato avo re ny hiranao
Ka derao Ilay Mpanjaka lehibe,
Izay namorona izao tontolo izao
Tamim-pahendrena be.}

{FIV.
Saotra ! hira fiderana,
Saotra ! atolorinao.
Saotra ! hira fanajana,
Aoka ho an''Andriamanitra}

{2.
Dia saino ny ataon''ny vorona,
Izay mahalala mankasitraka
An''Andriamanitra, Mpamorona,
Andro aman''alina.
[FIV.]}

{3.
Mba derao ''zao Andriamanitra;
Izy no nahary sy namelona;
Ary na marary na finaritra,
Izy no Mpanohana.
[FIV.]}

{4.
Lehibe tokoa ny fitiavany;
Ny Malalany nirahiny tety
Mba hanandratra anao ho zanany,
Ho mpandova ny ery.
[FIV.]}

{5.
Tompo Izy, ka atero hasina.
Vola sy harena, Azy rahateo;
Ianao no sitrany ho raisina;
Manolotra tena re !
[FIV.]}
', '#3-I', '#3') /end

insert into song values (50111, '11. Mba jereo ry zanak''olombelona', NULL, NULL, '
{1.
Mba jereo ry zanak''olombelona
Izay rehetra manodidina,
Fa ny Zanahary, Tompo tokana,
Izy no namorona.}

{FIV.
O ! mihevera ry namako ô !
Ka miderà hianareo
Voninahitra ry Zanahary ô !
Atolotray
Anao tokoa}

{2.
He ! ny lemaka efa maitso mavana;
Betsaka ny voninkazo ao;
Rivodrivotra mpamelombelona
Sy mpanaiso-dotom-po.
[FIV.]}

{3.
Ranomasina mirohondrohona
Ka mitopatopa fatratra;
Fa ny fahavaratra tsy tapaka
Manome anay ny ranonorana
[FIV.]}
', '#3-I', '#3') /end

insert into song values (50121, '12. Haleloia ! miderà Azy', NULL, NULL, '
{"Haleloia ! miderà Azy,
An''i Jehovah,
Ny fitiavany antsika"}

{"Ry Jeso, endre ny fitiavanao !
Ianao, teraka ho olona,
Ka isaoranay Ianao :
Avotr''aina vitanao
Ry Jeso, endre ny fitiavanao !
Avotr''aina vitanao"}

{"Jehovah o ! Andriamanitro Ianao.
Hanandratra Anao aho,
Hidera ny Anaranao,
Hanandratra sy hidera ny Anaranao"}

{"Indro ny fitiavanao anay Fitiavanao
Fitiavanao anay !
Fitiavanao anay !
Fitiavanao,
Tsy miova. Ianao tsy mba miova.
Tsy miova Ianao mandrakizay"}
', '#3-I', '#3') /end

insert into song values (50131, '13. Noely ! andro mahafaly', NULL, NULL, '
{Noely ! andro mahafaly,
Soa tokoa, no niseho
- "Tonga ny Zanaky ny Avo Indrindra,
Haleloia ! no atao" -}

{- Nisy anjely maro nilaza
Tamin''ny mpiandry ondry :
Soa ! aza matahotra"
Fa Mpamonjy no teraka
Ao an-tanànan''i Davida mpanjaka.
Mihirà haleloia
Hobio ny fiderana
Fa tonga eto ny Zana''Andriamanitra
Haleloia ! no atao"}
', '#3-II-1', '#3') /end

insert into song values (50141, '14. Henoy izao ny feon''anjely', NULL, NULL, '
{- "Henoy izao ny feon''anjely
Manako eran''ny lanitra
Sady mitory teny soa
Amin''ny olon-drehetra".}

{- "E ! ny ankamaroan''ny lanitra
E ! no miara-mihoby tokoa
Milaza fa misy Mpamonjy vao teraka
Ao an-tanànan''i Davida :
Izao no Kristy Tompo" -}

{- "Ity Mpanjakanay, eo an-tànany
Ny rariny no hitsarany ny olony" -}

{- "Voninahitra ho an''Andriamanitra !
Ary fiadanana ho an''ny olona
Ankasitrahany" -}

{- "Fahazavana lehibe, mifalia izao !
Ho an''izao tontolo izao
Mifalia izao !
Sady voninahi-tsy ho lo
Eny, hobio fa Zazakely
Fa ny firenena maro,
Izay efa tia ka nandray
Ny indrafony, sady efa namoy
Ny azy mba hananany
Azy mandrakizay" -}
', '#3-II-1', '#3') /end

insert into song values (50151, '15. Hosana no asandratray', NULL, NULL, '
{- "Hosana no asandratray
Ho Anao ry Tompo soa,
Fa ity no andro Noely,
Ka ifalianay" -}

{- "Izao ankehitriny izao
Dia misy filazana mahafaly,
Izay lazain''ireo anjely
Avy ao an-danitra ;
Fa tanteraka ny fihavanan''Andriamanitra
Sy ny zanak''olombelona" -}

{- "O ! mihirà sy mifalia sy mihobia" -}

{- "Voninahitra amin''ny Avo Indrindra,
Ho an''Andriamanitra Mpanjaka !
Ary fiadanana ety ambonin''ny tany
Ho an''izay ankasitrahany" -}

{- "Izao tontolo izao nateran-java-tsoa,
Mihirà, miderà !
Nomena Ilay Andrian''ny fiadanana,
Mifalia, mihobia !
Fa misy fifaliana lehibe tokoa,
Mihirà, miderà !
Mifalia, mihobia
Ary an-danitra sy ety an-tany" -}

{- "Mifalia, mihobia !
Azontsika tsy vidina re
ny avo-dehibe !
Mihirà, mihobia !" -}

{- "Voninahitra amin''ny Avo Indrindra,
Ho an''Andriamanitra Mpanjaka !
Ary fiadanana ety ambonin''ny tany,
Ho an''izay ankasitrahany" -}

{- "Izao tontolo izao ... ety an-tany" -}

{- "Voninahitra ... ankasitrahany" -}
', '#3-II-1', '#3') /end

insert into song values (50161, '16. Jesoa Sakaizanay', NULL, NULL, '
{- "Jesoa Sakaizanay,
Jesoa anay" -}

{- "Nisy anjely nihira teo,
''Zay nihira nanao hoe :
Haleloia
Haleloia no ansandratray,
Asandratray ho mandrakizay" -}

{- "Andeha isika ry zareo
Hitady fonenana tsara e!
Haleloia
Haleloia no ansandratray,
Asandratray ho mandrakizay" -}

{- "Tsy vola, tsy harena be
No aterinay aminao, fa
Haleloia
Haleloia no ansandratray,
Asandratray ho mandrakizay" -}

{Andeha isika ry zareo
Hitady fonenana tsara e !
Haleloia
Haleloia no ansandratray,
Asandratray ho mandrakizay.}
', '#3-II-1', '#3') /end

insert into song values (50171, '17. Haleloia, miderà!', NULL, NULL, '
{- "Haleloia, miderà!
An''i Jehovah ry firenena rehetra;
Fa heniky ny fahasoavany
Izao rehetra izao" -}

{- "A ! miderà
Miderà
Fa lehibe ny asany" -}

{- "Ny voninahitr''Imanoela
Hanenika izao rehetra izao;
Ho afaka ny rofintsika,
Fa tonga Ilay Mpamonjy,
Mpanavotra ny olon''ory,
Fa tonga Ilay Mpanolo-tsaina" -}

{- "Haleloia
Isaorana anie  ny Tompo Andriamanitra !
Izay nanome ny Zanany ho antsika" -}

{- "Hosana amin''ny Avo
Hobio
Hosanna amin''ny Avo" -}

{Hobionareo
Hobionareo ry firenena}

{- "Hosana amin''ny Avo
Hobio
Hosana amin''ny Avo" -}

{- "Haleloia
Isaorana anie  ny Tompo Andriamanitra !
Izay nanome ny Zanany ho antsika" -}
', '#3-II-1', '#3') /end

insert into song values (50181, '18. Iza moa izao mhira tsara izao?', NULL, NULL, '
{- "Iza moa izao mhira tsara izao?
Ar ny feony manako eran''ny lanitra,
Toy ny feon''ny mozika
Ary ny feon''ny anjomara
Ary ny zava-maneno rehetra
Ety ambonin''ny tany" -}

{- "Anjely no mihira amin-karavoana soa
Sy ny hafaliana be
Ka midera an''Andriamanitra,
Ka manao hoe :
Haleloia
Anjely no mihira
Haleloia
Ka midera an''Andriamanitra
Haleloia
Ka midera an''Andriamanitra" -}

{- "Injao fa mihira, milaza an''i Jesosy
Fa teraka ao Betlehema
Ny Mpanjaka mahery tsitoha,
Mpandresy amin''ny ady.
Anjely re no mihira
Haleloia
Ka midera an''Andriamanitra
Haleloia
Ka midera
Haleloia
Ka midera an''Andriamanitra" -}

{- "" -}
', '#3-II-1', '#3') /end

insert into song values (50191, '19. Tena zava-maminay tokoa', NULL, NULL, '
{1.
Tena zava-maminay tokoa
''Zany fetinao, ry Jeso ô !
Fety tsara sady mahasoa
Mahafaly loatra Tompo ô !}

{2.
Fety mampikambana ny fo;
Fety ben''izao tontolo izao;
Fety tsy ho rava na ho lo
Fety izay hankalazana Anao}

{3.
Fety manan-daza ela be;
Fety vaovao lava koa;
Fety tsy mba mety lefy re !
Fety mahasambatra tokoa.}

{4.
Fety masina sy manan-jo;
Fety mahasoa fanahy e !
Fety mampiravoravo fo;
Fety manadio ny tena re !}

{5.
fety fihiràn''anjely soa;
Fety misy fifaliam-be;
Fetin''ny ankizy maro koa;
Fety ho mandrakizay anie !}

{6.
Arahaba re Jesosy ô !
Arahaba re, fa fetinao !
Arahaba re ry Zoky ô !
Arahaba, tonga Ianao !}
', '#3-II-1', '#3') /end

insert into song values (50201, '20. Tonga ny andro mahafaly', NULL, NULL, '
{Tonga ny andro mahafaly
Sy mampiravo fo,}

{- "Ka iza re no tsy ho faly
Raha mahita izao" -}

{- "Fa ny Zanak''Andriamanitra
No tonga olona tahaka antsika" -}

{- "Hanavotra ny very
No anton''ny diany
Ka iza no handà izany" -}

{- "Ry mpanota ! indro Jesoa
Mandondona
Eo am-baravaran''ny fonao,
Te honina ao, raha mety hianao" -}

{- "Ka iza no hisalasala
FA he ! ny Tompo mitaraina hoe :
Vohay
Vohay Aho" -}

{- "Ry mpanota ! Indro Jesoa
Mandondona
Eo am-baravaran''ny fonao
Te honina ao, raha mety hianao" -}
', '#3-II-1', '#3') /end

insert into song values (50211, '21. Endrey izany hiran''ny anjely', NULL, NULL, '
{- "Endrey izany hiran''ny anjely,
Tery an-dohasahan''i Betlehema
Endrey, endrey i Betlehema
Tery an-dohasahan''i Betlehema
Endrey, endrey i Betlehema" -}

{- "He ! izany hira :
Hiran''ny anjely
Tery an-dohasahan''i Betlehema
Betlehema
Tery an-dohasahan''i Betlehema" -}

{Voninahitra amin''ny Avo indrindra,
Ho an''Andriamanitra !}

{- "Fiadanana
Fiadanana
Ho amin''ny olona
Izay ankasitrahany" -}

{- "Fa inona izany
Izany Jesosy Mpanjakan''ny lanitra
No nateraka androany
Izany Jesosy Mpanjakan''ny lanitra
No nateraka androany" -}

{- "Ry mpanota ô !
Tsy ho an''ny anjely ny avotra,
Fa ho antsika mpanota mahantra !
Tsy ho an''ny anjely ny avotra,
Fa ho antsika mpanota mahantra !
Tsy ho an''ny anjely ny avotra,
Fa ho antsika mpanota mahantra !" -}
', '#3-II-1', '#3') /end

insert into song values (50221, '22. Dia mandrosoa isika', NULL, NULL, '
{- "Dia mandrosoa isika ;
MAndrosoa izao isika ''ray olona
Mba ho any Betlehema ;
Ho any Betlehema,
Fa teraka ho antsika ao e
Ao e ! ny Mpamonjy,
Izay atao hoe : Kristy Tompo" -}

{- "A ! ny, ho ''aho,
Izay nambaran''ireo mpaminany
Ka nolaizain''ireo;
Izay nambaran''ireo mpaminany
Ka nolazain''ny anjely" -}

{- "Ilay Mpanjaka be voninahitra sy laza,
Mandrosoa hijery ao Betlehema,
Mandry ao an-tranom-bilona" -}

{- "A ! tonga mba haninona antsika moa Izy io?
- Tsy fantatrao va, fa Io Ilay namelàn''Andriamanitra ny helotsika?" -}

{Inona moa no mety hatao?
- Ento ny hasina sy ny fo marina}

{- "Mba mizora ho àry,
Amin''io Mazava io,
Fa hitarika antsika Izy" -}

{- "O ! andro mahafinaritra
Ny andro lehibe;
Andro mba niriko re,
Ny andron''izao fifaliana izao" -}
', '#3-II-1', '#3') /end

insert into song values (50231, '23. Aiza re no misy ilay tanàna soa ?', NULL, NULL, '
{1.
Aiza re no misy ilay tanàna soa ?
Vitsy trano, tsy mba kely laza koa,
''Zay nahaterahan''i Jesosy re !
Ali-maizim-pito, sasak''alim-be ?}

{FIV.
Ao Betlehema
Any Jodia
Ao Imanoela no teraka}

{2. Kitana ihany no dihihina,
''Zay nataony izay ela be izay
Ny mamantatra azy isan''alina,
Koa aiza re Imanoela ?
[FIV.]}

{3.
Izahay izao dia avy lavitra,
Te hahita Azy, na dia sasatra
Ka hitsaoka Azy izahay anio.
Aiza moa Izy no nateraka ?
[FIV.]}

{4.
O ! avia ry olona rehetra ô !
Mba hitsaoka an''Ilay Zaza soa,
Efa nantenaina fony ela be,
Mba hamonjy ny mpanota marobe
[FIV.]}
', '#3-II-1', '#3') /end

insert into song values (50241, '24. Hobio', NULL, NULL, '
{- "Hobio
Anio dia horaisintsika
Ny fikasana tany Edena,
Hobio ! fikasana tany Edena" -}

{- "Ny tany sy ny lanitra
miara-paly amintsika
Kristy Zaza no teraka
Any Betlehema, hobio !
Nihorohoro mafy
Ny devoly hatrizay,
Fa miramirana kosa isika" -}

{- "Voavonjy ka mifalia, eny mihobia !
Tsy hariany tsy akory,
Mitehafa tànana,
Fa ny Zanany aza, nandefitra mafy :
Fitiavany antsika no ataon''izany" -}

{- "Fa ny aintsika sarobidy indrindra !
nalaina tamin''ny endriny,
ka tsy foiny
Isaorana anie ny Anaranao, haleloia !
ho Anao, ry Tompo tia anay
Haleloia
Isaorana anie ny Anaranao,
Ry Tompo tia anay" -}
', '#3-II-1', '#3') /end

insert into song values (50251, '25. O ! mifalia ry olombelon-drehetra', NULL, NULL, '
{- "O ! mifalia ry olombelon-drehetra
Amin''izao famangian''Andriamanitra
Antsika izao : indro fa nomeny
Ny Zanani-lahy Tokana
Mba ho sambatra isika" -}

{- "He ! ny fitiavan-dehibe :
Ny saotra no ataovy be.
Foiny io Malala io,
Fa tonga zana-behivavy" -}

{- "Dia asandrato re ny feo !
Venteso re ny hirantsika izao
Hidera sy hankalaza io
Fa Zaza be voninahitra" -}

{- "Misaora sy manomeza
Voninahitra an''Andriamanitra
Izay Tompo manan-kery,
Nefa kosa be fiantra,
Voninahitra
Ho amin''ny Avo Indrindra" -}
', '#3-II-1', '#3') /end

insert into song values (50261, '26. Arahaba ! ho anay', NULL, NULL, '
{- "Arahaba ! ho anay
Ilay andro nirinay
Fa tonga aty ilay andro
Mahafaly ny fonay" -}

{- "Ka mifalifalia re !
Ry sakaiza rehetra
Sady namako izao;
Mifalia, mifalia re !
Fa tonga ny andro
Ilay andro mahafaly
Antsika rehetra ka
Mifalia re" -}

{- "Mifalia izao ry olona rehetra !
Fa miherina ny taona
Fiderana ny Mpamonjy
Mihobia sy manaova antso hoe :
Tsofy tsara ny mozika ry zareo,
Eny, samie mihoby Azy,
Fa tratry ny Noely indray" -}

{- "Fa isika kosa e !
Raha mbola velon''aina
Sy tafahaona eto
Mba hidera ny Mpamonjy" -}

{- "O ! mihobia sy mihirà;
O ! mitehafa tànana;
Asandrato mafy  ny feo,
Fa tratry ny Noely indray" -}

{- "Fa isika kosa e !
Raha mbola velon''aina
Sy tafahaona eto
Mba hidera ny Mpamonjy" -}
', '#3-II-1', '#3') /end

insert into song values (50271, '27. Samia mihoby mafy', NULL, NULL, '
{- "Samia mihoby mafy
Isika ankehitrio,
FA resy ilay mpandrafy;
Venteso ka hobio !
Ny ziongan''i satana
Namatotra taty
Tsy maintsy hovahana
Amin''ny andro Jobily" -}

{- "Hobio
Hobio ry manan''aina
Jeso Mpanjaka soa
Ilay mendrika hoderaina
Fa tonga aty tokoa
Jereo ny tranon''omby,
Kintana Iray iry !
Anjely no mihoby
Manakoako ery" -}

{- "Na lehibe na kely,
Samia manandra-peo
Tahaka ny anjely,
Ka saotra no omeo;
Na dia tezitra aza
Heroda lehibe,
Ny Magy hankalaza,
Ka mba venteso e" -}

{- "Mangina ve ny maro,
Ka vitsy no manao hoe :
Hobio dia hobio, akaro,
Manaova izao hira vao" -}

{- "Derao ny tany maina
Sy ny ranomasin-dehibe
Samiamiray saina,
Avia maneke :
Derao, avia, maneke." -}

{- "Hobio
Hobio ry mananan''aina
Jeso Mpanjaka soa,
Ilay mendrika hoderaina,
Fa tonga aty toko.
Jereo ny tranon''omby,
Kintana iray iry,
Anjely no mihoby
Manakoako ery." -}
', '#3-II-1', '#3') /end

insert into song values (50281, '28. Ry vahoaka maro', NULL, NULL, '
{1.
Ry vahoaka maro
Vory eto izao !
Arahaba hianareo
Tratr''izao Noely izao}

{FIV.
Haleloia ! hosana
ary no venteso.
Mifaly ny mino,
mihoby ny tra-bonjy,
Fa tonga ny andro famonjena
''Zay natokana ho antsika
Haleloia ! hosana,
Hobio mihoby fiderana
Haleloia ! hosana
Jesosy teraka}

{2.
He ! tonga indray Noely
Alina mampiravo
Alina, anefa toa antoandro
Alin''ny Mpamonjy antsika
[FIV.]}

{3.
Indro Ilay Zazakely,
Hotronin''ny anjely,
Dia Mpanjaka feno hery,
Ho Mpanavotra ny very
[FIV.]}
', '#3-II-1', '#3') /end

insert into song values (50291, '29. Zao andro lehibe izao', NULL, NULL, '
{- "Zao andro lehibe izao
No ihobiantsika
Fa noresen''ny aim-baovao
Ny fahafatesantsika" -}

{- "Ny Zana''Andriamanitra
''Zay Tompom-boninahitra,
Nandao ny lapan-dRainy,
Nateraka ho olona,
Hamonjy sy hamelona,
Hamelona antsika sombin''ainy" -}

{- "Velomy ny fisaorana,
Na kely monja aza re !
Tahaka izay navotana
Hankalaza ny Zanak''Andriamanitra,
Niala tao an-danitra,
Hanafaka ny alahelo,
Ry voavonjy ô ! derao !
Derao Ilay nanaovotra anao" -}

{- "Na dia tratry ny manjo
Izay mampiferin''aina
''Zahay mba tsy hamoy fo,
Fa manan-kantenaina" -}

{Jesosy dia Zokiko
Ka tsy hohadinoiko
Omeo hery hanohanana
Ny foko izay manahy.
Haleloia ! fa tapitra ny fahoriantsika
Derao Andriamanitra !
Derao fa efa tia antsika, derao !}

{- "O ! ry fanahiko,
Derao Ilay Mpamonjy anao !
Derao Ilay Mpamonjy anao !
Derao" -}

{Velomy ny fisaorana
Na kely monja aza re !
Tahaka izay navotana,
Hankalaza ny Zanak''Andriamanitra,
Niala tao an-danitra,
Hanafaka ny alahelo.
Ry voavonjy ô ! derao,
Derao Ilay nanaovtra anao !}
', '#3-II-1', '#3') /end

insert into song values (50301, '30. Ataovy antso avo hoe : Hobio !', NULL, NULL, '
{- "Ataovy antso avo hoe :
Hobio !
Azonay ny fandresena ;
Anio no handraisantsika
Ny fikasana tao Edena" -}

{- "Asandrato re ny feo !
Mihobia sy miderà;
Eny, hobio, hobio mafy re !
Io no ilay Solofo
Avy amin''ny fototry Jese" -}

{- "Ny anjely ao an-danitra
Miara-paly amintsika,
Ka mhira sy mihoby hoe
Haleloia ! Haleloia" -}

{- "Hosana
No ansandratray
Ho an''Ilay tia anay" -}

{- "Ny anjely ao an-danitra
Miara-paly amintsika
Ka mihira sy mihoby hoe
Haleloia ! Haleloia" -}

{- "Hosana
No ansandratray
Ho an''Ilay tia anay" -}
', '#3-II-1', '#3') /end

insert into song values (50311, '31. Ao anaty alina mangina', NULL, NULL, '
{1.
Ao anaty alina mangina,
Misy kintana izay miseho
Dia Jesosy Zaza fitahiana
Ka asandrato mafy re ny feo :}

{FIV.
Haleloia, eny, Haleloia !
Ho Anao anie ry Tompo soa,
Haleloia, eny Haleloia !
Ho Anao mandrakizay doria !}

{2.
O ! ry Kintan''aina mba hanala
Ny aizim-pito ao anaty ao,
Dia Jesosy Zaza nantenaina,
Famonjena izao tontolo izao !
[FIV.]}

{3.
Mifalia re ry firenena !
Miravoa hatramin''izao.
Indro tonga izao ny kitan''aina :
Kristy Izay ho famonjena anao.
[FIV.]}

{4.
O ! ry Kitan''aina famonjena
Sady Zanaka, Iraky ny Ray
Indro, atolotray ny fo sy tena
Ka mba raiso ho Anao izahay
[FIV.]}
', '#3-II-1', '#3') /end

insert into song values (50321, '32. Teraka ao i Betlehema', NULL, NULL, '
{- "Teraka ao i Betlehema
Ny Mpanjaka malaza,
Ny Mpanjaka malazan''
Izao tontolo izao,
Izay nantenaina izay ela izay" -}

{- "Ary ny anjely nandeha
Tamin''ny mpiandry ondry
Tany an-tsaha, ka nanao hoe :
Teraka ho anareo
Ao an-tànana''i Davida
Ny Mpamonjy" -}

{- "Ary mandehana ka mba haingàna
Ankehitriny, ka mba haingàna
Mba hitsaoka ny vao teraka
Ao Betlehema" -}

{- "Ao i Betlehema no Tanàna tsara indrindra" -}

{- "Ao i Nazareto, Vavahady niandohana" -}

{- "Dera sy voninahitra
Amin''ny Avo indrindra,
Jeso ô" -}

{- "Voninahitra ! Voninahitra ho
An''ny Ray ary ny Fanahy Masina" -}

{- "Voninahitra ho an''ny Tompo !
Ho an''ny Tompo Avo indrindra" -}

{- "Ao i Betlehema no Tanàna tsara indrindra" -}

{- "Ao i Nazareto, Vavahady niandohana" -}

', '#3-II-1', '#3') /end

insert into song values (50331, '33. A ! iza moa izao derain''ny anjely izao', NULL, NULL, '
{- "A ! iza moa izao derain''ny anjely izao
Jesosy Mpanjakan''ny lanitra !
O ! avy taiza moa Izy izao,
Ary ho aiza izao?" -}

{Avy tao an-danitra
Ka nidina nankaty re !}

{- "Fa ny famonjen-dehibe re
No mba ho anton''izay
Mba nidinany !
Famonjen-dehibe re no mba
Ho anton''izay mba nidinany !" -}

{Tsy hanjaka intsony
Izao satana izao
Fa hisava re ny herin''ny maizina
Ka hiadana mandrakizay kosa
Re ny ory,
Fa ny Mpandresy Tsitoha
No tonga ho anao e !
Mifalia, miravoa, mihirà !}

{- "Ary dia mifalia ka
Avia mba hihira.
Manakorà mafy kosa hianao.
Mifalia, mihirà,
Manakorà mafy kosa hianao" -}

{A ! eny ô !
Tsy mba misy tsiny,
Ka mihaino Izy
Ka mitaraina
Zanaky ny aina e no ory
Fa miteny Izy hamonjy anao.
O ! aza kamo mba hitady aina
Ianao re ry mpanota
Sao dia very.}

{A ! ampoizo !
Tsy mba misy tsiny...}

{A ! misaotra !
Tsy mba misy tsiny...}

{A ! veloma !
Tsy mba misy tsiny...}
', '#3-II-1', '#3') /end

insert into song values (50341, '34. Jeso Mpamonjy', NULL, NULL, '
{Jeso Mpamonjy, Ilay Tompon''ny aina no hita
Nidina teto an-tany hamonjy antsika
O ! mba derao.
Izy Mpanavotra anao,
Ka mifalia, ry mino.}

{Indro ny tenin''ny anjely milaza ny soa !
Teraka Jeso Mpamnjy antsika toko :
O ! Mba derao
Tonga ny andro vaovao
Andron''ny Filazantsara.}

{Dia iventesonao ny hira vaovao re izany!
Tonga Jesosy hamonjy izay mitomany.
O ! mba derao
Afaka isika izao :
Izy Mpisolo antsika.}

{Tona ny Tompon''ny tompo hisolo ny ory
Ka mifohaza ry olona mbola matory !
O ! Mba derao
Amin''ny feo vaovao,
Jesoa, Izay Tompo mahery!}
', '#3-II-1', '#3') /end

insert into song values (50351, '35. Ny hazo fijaliana', NULL, NULL, '
{1.
Ny hazo fijaliana
No mitory fahoriana.
Ianao no Hazon''aina,
Manome ny tena aina;
Foto-kazon''i Edena,
Itaozam-pamonjena}

{FIV.
- "Isaoranay Ilay Hazo
Oliva tsy malazo,
Fa Ilay nijaly teto
No isaoranay injato
Fa nahavony anay mpanota,
Mpanota hatrany Golgota" -}

{2.
Raha misy tsora-kazo
Avy taminao, ry Rainay,
Ka ny zavatra mahazo,
Fitsapana mahamay
Dia tano mba hatoky
Sy hifikitra Aminao
[FIV.]}
', '#3-II-2', '#3') /end

insert into song values (50361, '36. Fiadanana ho an''ny olona', NULL, NULL, '
{- "Fiadanana ho an''ny olona!
Fiadanana no asandratra
Any Kalvary sy ny fifaliana" -}

{Indro, ao an-tampon''ilay havoana,
Havoana kely, nahitana
Ny famonjena ireo izay miely ;
Mandrosoa ry mpanota mahantra!
Irio tokoa ny aina lava,
Tompo tia sy miantra,
Sady te-hamonjy anao.}

{Tsy ety Edena no nahalavoana,
Fa tany soa nandresena ao,
Asandrato haravoana!
Fa nahazo famonjena.
Asandrato haravoana
Fa nahazo famonjena;
famonjena!}

{- "Ao ny hazo fijaliana
Sy ny Tompo Jeso tia
Izay nitondra fahoriana
Mba hisolo ny mpania
Nigororoana toy ny rano be
Ny ràny mpanadio ny fo,
Hanafak''ota lehibe
Sy hanala ny manjo;
Hanafak''ota lehibe re,
Sy hanala ny manjo;
Hanala ny manjo" -}

{- "Tao Kalvary, ilay havoana,
Kristy Mpisolo, maty tia;
Hazo fijaliana!
Hazo fijaliana ilay nihomboany,
Famonjen''izay mankeo;
Hazo fijaliana!
Famonjen''izay mankeo"-}
', '#3-II-2', '#3') /end

insert into song values (50371, '37. Jesosy nijaly tao Getsemane', NULL, NULL, '
{1.
Jesosy nijaly tao Getsemane,
Ka moa hadinonao va
Ny dinitra taminy ''zay nitete,
Nirotsaka be toy ny rà?}

{FIV.
Indrisy! niaritra Irery Ianao
Ry Tompo nanavotra anay,
Hananan''ireo olo-mino Anao
Fiainana mandrakizay}

{2.
Ry mpianatra vitsy nanatrika teo,
Toa fantatrareo rahateo
Ny loza mangidy ''zay sedrainy teo,
Kanefa natory hianareo !
[FIV.]}

{3.
Ry saha manginan''i Getsemane,
Ry fototr''oliva, lazao !
Raha misy mpanota mba efa nandre
''Zao avotra atao azy izao?
[FIV.]}

{4.
Fitiava-mangina tsy misy mpandre
No mamin''ilay Tompo tia
Raha tànan-kavanana no manome,
Tsy hitan''ny tànan-kavia.
[FIV.]}

{5.
Ny foko mangina tsy hitan''ny be.
Ry Tompo, atolotro Anao,
Fa vidin''ny sento tao Getsemane,
Tsy hitan''izao tany izao.
[FIV.]}
', '#3-II-2', '#3') /end

insert into song values (50381, '38. Tazanao anio Getsemane', NULL, NULL, '
{1.
Tazanao anio Getsemane
Fa tonga tao Jesosy,
jereo ange !
- "He maizina ny saha fa alina :
Ao ilay kapoaka hosotroina" -}

{2.
Fadiranovana Izy noho hianao,
Ka mitaraina mafy ery izao,
- "Odrey ! miari-tory hivavaka.
Ny Rà sy dininy dia niraraka" -}

{3.
''Njay mitaraina Izy hisolo anao
Ka mitalaho mafy sy ory koa.
- "Ry fo matory, aoka hivavaka,
Mba tsy hidiranao amin''ny fandrika" -}

{4.
Ry Raiko ô !
Iriko hesorinao
Ny mafy izay asainao ho entiko,
- "Anefa, Ray malala, izay sitrakao,
FA tsy ny Ahy re ! no atao" -}

{5.
Ry Tompo ô ! iriko hiataka
Ary izaho koa hivavaka :
- "Ny sitrakao anie no aoka hatao,
Fa tsy ny ahy intsony re! hatrizao" -}

', '#3-II-2', '#3') /end

insert into song values (50391, '39. Kalvary no tiako jerena', NULL, NULL, '
{- "Kalvary no tiako jerena
Tao ny Tompo, naka ny fomba,
Ny fomban''olombelona,
Mba hanala entana an-damosiko" -}

{- "Afaka ny fahoriana tamiko,
Raha nahita ny Mpianatra,
Teo ambonin''ny hazo fijaliana;
Afaka re ny fahoriana tamiko !
Raha nahita ny Mpianatra,
Teo ambonin''ny hazo fijaliana
Tsy eto va o vao mahafinaritra !
Tsy eto va no nahatapaka ny kofehy?
Tsy eto va no niala
Ny entana an-damosiko?
Veloma, veloma indrindra" -}

{- "Veloma ry tsilo, veloma ry lefona
Veloma ry fantsika, veloma ry hazo
Mitanambokovoko,
Kanefa Ralehilahy, Ralehilahy
No omeko veloma
Veloma indrindra" -}
', '#3-II-2', '#3') /end

insert into song values (50401, '40. Mangetaheta Aho', NULL, NULL, '
{- "Mangetaheta Aho,
hoy Jeso, hoy Jeso,
Hoy Jeso Mpamonjy ny olo-meloka.
Nitambesatra taminy,
Ny helok''izao tontolo izao;
Nivesatra ny fahotana,
Nampagetaheta ny Tompo.
Izy Irery no nitondra ny valin''ny ota" -}

{- "Indrisy ! Indrisy !
Nangetaheta ny Tompo;
Ny fahavalo nanolotr''Azy
Ny vinaigra mangidy
Eny ! ry Jeso, fa niaretanao izay
Noho ny fitiavanao anay;
Nampisotroinao anay
Ny ranon''aina mahavelona,
Izay nivoaka avy amin''ny
Lanivoan''Imanoela
Izay nivoaka avy amin''ny
Lanivoan''Imanoela" -}

{- "Ry ranomasin-dehibe,
Ry renirano maria,
Ry ony Jordana,
Sy farihin''i Genesareta,
Sy fatsakan''i Jakoba
Ry rano ao an-danitra
Nahoana re no tsy mirotsaka be
Mivatravatra,
Fa Jeso Kristy, Mpanjakan''ny lanitra
No mangetaheta" -}

{- "Indrisy ! Indrisy !
Nangetaheta ny Tompo..." -}

{- "Ry ranomasin-dehibe,
Ry renirano maria..." -}

', '#3-II-2', '#3') /end

insert into song values (50411, '41. Ho an''i Jehovah ihany', NULL, NULL, '
{- "Ho an''i Jehovah ihany,
Ny hery sy ny voninahitra,
Manako eran''ny lanitra,
Sy ety ambonin''ny tany;
Mendrika avokoa, fa isika
Hanome voninahitra an''i Jeso
Zanak''Andriamanitra,
Izay Avo Indrindra" -}

{- "Ataovy manako ny hirantsika izao
Fa isika hanome voninahitra
An''i Jesosy, Izay nijaly
Taty fahizay, nisolo ny helotsika;
Haleloia
Hoderaina Izay Avo Indrindra" -}

{- "Ry zanak''olombelona,
Tsy mba mangoraka va ny fonareo
Raha mijery ny fahoriana
Nentin''i Jesosy Tompo?
Indro Izy nasandratra
Eo ambonin''ny hazo fijaliana,
Ka nandefitra, ka mitaraina hoe :
Mamelà, mamelà ny helony,
Fa tsy fantany
Izay ataony" -}

{- "Nodiany tsy tiany tsy akory
Ny Zanany malalany,
Ka dia navelany eo
Am-pelatànan''ny jiosy,
Hamonjeny ny zanaky ny ory
Nafatotry ny ota" -}

{- "Ry zanak''olombelona,
Tsy mba mangoraka va ny fonareo..." -}
', '#3-II-2', '#3') /end

insert into song values (50421, '42. Nitsangana tamin''ny maty ny Tompo', NULL, NULL, '
{- "Nitsangana tamin''ny maty
Ny Tompo Andriamanitsika,
Rehefa nandresy ny rafy
Izay no hery ho antsika" -}

{- "Edena izay nahalavoana
Nidiran''ny fahadisoana
Tao i Kalvary nisehoan''
Ny rano nandio halotoana.
Natsangana tamin''ny maty
Ny Tompo Andriamanitsika
Rehefa avy nandresy ny rafy
Izany no hery ho antsika" -}

{- "Fahiny nihoby satana;
Ny fasana hita nivoha:
Anio dia hihira Hosana !
Nanjaka ny Tompo Tsitoha.
Hosana" -}

{- "Fa isika rehefa ho faty,
Tsy handry ao am-pasana ela,
Fa hatory,
Fa isika tsy mba ho faty,
Fa hitsangan''Imanoela "-}

{- "Fahiny nihoby satana;
Ny fasana hita nivoha:
Anio dia hihira Hosana !
Nanjaka ny Tompo Tsitoha.
Hosana" -}

{- "Veloma ry hazo mijoro,
Ny fasana tsy nahatana !
Devoly natao torotoro,
Fa efa nazera satana" -}

{- "Fahiny nihoby satana;
Ny fasana hita nivoha:
Anio dia hihira Hosana !
Nanjaka ny Tompo Tsitoha.
Hosana" -}
', '#3-II-3', '#3') /end

insert into song values (50431, '43. Velona Ilay nohomboana', NULL, NULL, '
{- "Velona Ilay nohomboana
Orim-baton''ny finoana
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana
Velona Ilay nohomboana
Orim-baton''ny finoana
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana
Ka hobio, hobio !
Fa nandresy Ilay nohomboana
Hobio ny fiderana
Fa nandresy Ilay nohomboana" -}

{- "Sitrapony no nijaly,
Novonoina tsy namaly ;
Faly volony Gehena,
Kanjo diso fanantenana,
Sitrapony no nijaly,
Novonoina tsy namaly ;
Faly volony Gehena,
Kanjo diso fanantenana
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana
" -}

{- "Tao ny fasam-bato mafy,
Ny tsy tia sy ny rafy;
Tsy mba nisy nahatana
Toran-kovitra sy ana
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana" -}

{- "Tao ny miaramila maro
Sy ny foko mifangaro,
Nanodidina nijery
Fa tsy tàna Ilay Mahery
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana" -}

{- "O ! ty tany mitehafa !
Ry lanitra mba misokafa !
Fa nitsangana ny Tiana,
Jeso Mesia Andriana
Fa nitsangana ny Tiana
Ka hobio ny fiderana
Fa nandresy Ilay nohomboana" -}
', '#3-II-3', '#3') /end

insert into song values (50441, '44. Tsofy mafy ny farara', NULL, NULL, '
{- "Tsofy mafy ny farara
Izay milaza an''i Kristy.
Eny ! mihobia mafy,
Fa efa tonga ny Jobily" -}

{- "
Misy hanafaka tokoa!
Misy hanafaka
Ny mpitatotra an-tany
Sy hamory ny miely,
Fa efa tonga ny Jobily" -}

{- "Hanavotr''ahy izay very,
Azon''ny loza be.
Modia izay miely,
Fa efa tonga ny Jobily" -}

{- "Efa resy izao satana :
Misy famelan-keloka.
Miverena ry mpiely,
Fa efa tonga ny Jobily" -}
', '#3-II-3', '#3') /end

insert into song values (50451, '45. Ry taranak''i Adama', NULL, NULL, '
{1.
Ry taranak''i Adama
Very faha-razana,
O! ry Zanak''i Adama,
Very tanindrazana!
Misy antso avo re,
Avy any Golgota;
Feon''Ilay mitafy rà,
No miantso mafy hoe :
Vita re, vita re!
Vita re, vita re!
Vita re ny avotra,
Ry mpanota very ô !}

{2.
Efa resy ny devoly,
Lohan''ny mpanohitra;
FA satana mpanangoly,
Efa voafatotra.
Rava re ny fasana,
Ka ny fahafatesana
Efa toran-kovitra.
Afaka isika re!
Mihirà, mihirà!
mihirà, miderà,
Miderà ny Tomponao
''Zay niady ho anao}
', '#3-II-3', '#3') /end

insert into song values (50461, '46. Andro mahafaly izao anio izao', NULL, NULL, '
{1.
Andro mahafaly izao anio izao,
Ka venteso mafy re ny hirana;
Andro nahefana ny avo-dehibe,
Izay nataon''ny TOmpo ho antsika re !}

{FIV.
Mifalia isika fa voavotra,
Fa ho lovantsika izao ny lanitra}

{2.
Teo, te-ho mpandresy satana re;
Toa mba izy iny no mpanjaka be,
Kanjo resin''i Jesosy izy izao,
Ka ny heriny nitsahatra fa lao
[FIV.]}

{3.
Indro fa ny vatom-pasana tsy leo,
Dia nikodia moramora teo,
Mba homban''ny Zanak''Andriamanitra,
Izay nitsangana amim-boninahitra
[FIV.]}

{4.
Mifalia re ry olombelona
Fa Jesosintsika dia velona
He! tsy misy heriny ny fasana
Ka tsy tàna Izy fa nitsangana
[FIV.]}
', '#3-II-3', '#3') /end

insert into song values (50471, '47. Tsofy mafy ny trompetra', NULL, NULL, '
{1.
Tsofy mafy ny trompetra
Mba ho ren''ny namanao!
Fa Jesosy tsy mitoetra
Ao anaty fasana!}

{FIV.
Ampanenoy mba ho re
Hatrety anaty fasa-mangina;
Tsofy mafy avo be fa velona
Jeso mba ho santatra}

{2.
Tsofy mafy ny trompetra
Mba ho ren''ny havana;
Fa ny olona rehetra
Dia ho hasoavina
[FIV.]}

{3.
Tsofy mafy ny trompetra
Ny Baiboly masina!
Mifalia, ry majoretra,
Fa ny loza farana !
[FIV.]}
', '#3-II-3', '#3') /end

insert into song values (50481, '48. He ! Tsara ny fonenanao', NULL, NULL, '
{- "He ! Tsara ny fonenanao
Andriamanitra ô!
Ziona tendrombohitra
tanàna masina" -}

{- "Ka iza no hiakatra ao?
Izay mandio tànana, izay manana ny to
Ka mino ny Mpanavotra :
Ireo no hiakatra ao" -}

{- "Mba misandrata faingana,
ry varavarana!
Mba hidiran''ny Mpanjakanay be voninahitra
Moa Iza izao Mpanjaka izao" -}

{- "Andriamanitra Mahery sy mandresy koa,
Mpanjaka masina, Mpanjakanay;
Miantranoa, Tompo ô !
mba hifalianay
Voavoatra ny tranonao
Midira, midira ao Ianao." -}

{Moa Iza
Iza izao Mpanjaka izao?
Mahery sy mandresy koa
Moa Iza
Iza izao Mpanjaka izao,
Be voninahitra}

{Moa iza izao Mpanjaka izao?}

{- Andriamanitra Mahery sy mandresy koa,
Mpanjaka masina, Mpanjakanay;
Miantranoa, Tompo ô !
mba hifalianay
Voavoatra ny tranonao
Midira, midira ao Ianao.}

{Mba misandratra faingana, ry varavarana!
Mba hidiran''ny Mpanjakanay be voninahitra}
', '#3-II-4', '#3') /end

insert into song values (50491, '49. Hoderaina mandrakizay', NULL, NULL, '
{- "Hoderaina mandrakizay
Ny famindrampon''i Jehovah,
Fa manerana izao tontolo izao
Ny fiantrany, ka mihatra
Any am-bazan''ny tany :
Ary ny masony mijery
Ka dia miantra, nanome
Ny Zanany malalany hamonjy
Antsika, izay nanjakan''i satana.
A ! o, eny o! hoderaina" -}

{- "Faly izahay, ô ! ry Mpanjavotra anay !
Ao an-koatra ny fahafatesana
No tsy maintsy hidera Anao ihany" -}

{- "Fa Ianao ihany no mendrika
Handray saotra aman-dera,
Fa Zanaky ny Avo indrindra !
Fa manerana izao tontolo izao
Ary ny fiantràny;
Nefa ary ny fahadalanay
Dia tsy mba nojerenao,
Satria lehibe ny fitiavanao anay;
Ka tsy mandrika handray saotra
Aman-dera afa-tsy Ianao ihany" -}

{Endre! mampieritreritra anay
Sy mampisento anay tokoa re
Ny fahoriana nentinao,
Ry Zanak''Andriamanitra ô!
Fa ny rànao latsaka
No mampisento anay anay;
Fa ny ainao ''zay niala
no mampangoraka anay.
Endre! mampieritreritra anay
Indrisy mampisento
Sy mampisento anay tokoa re}

{Ny fahorianao, ry Zanak''Andriamanitra ô!
Fa ny rànao latsaka
No mampisento anay;
Fa ny ainao ''zay niala
No mampangoraka anay
Endre! mampieritreritra anay
Sy mampisento anay tokoa re
Ny fahorianao,
Ry Zanak''Andriamanitra ô!}
', '#3-II-5', '#3') /end

insert into song values (50501, '50. A ! inona moa no mety havaliko', NULL, NULL, '
{- "A ! inona moa no mety
Havaliko ny soa nataon''i Jeso
Fa tsy nisy mafy
Izay tsy niaretany hamonjeny ahy?
Tsy mba misy fitiavan-dehibe
Mihoatra izany, fa nanolotra
Ny ainy, eny, fa ny eso sy ny Latsa aza dia samy niaretany avokoa
Ary ny zava-mangidy
Dia mafy lavitra noho izany,
Ary ny farany indrindra aza
Dia naleony namoy ny ainy
Mba hamonjeny antsika" -}

{- "Fa tsy misy zava-tsoa
Mba ilaina aminao,
Afa-tsy ny mandeha mahitsy
Ka milaza ny to ao am-ponao
Sady miala amin''ny ratsy nataonao." -}

{FIV.
Asandrato mafy ny feo
Miderà
Asandrato mafy ny feo
Miderà
Ka midera an''Andriamanitra
Miderà
Ka midera an''Andriamanitra Miderà
Asandrato mafy ny feo
Miderà
Ka midera an''Andriamanitra}

{- "Ento, torio amin''izao
Tontolo izao sy ny jentilisa koa
Ny famonjen-dehibe izay nataony,
Miverena
Fa ny ain-dehibe no nafoiny taminao, ka
[FIV.]" -}

{- "Isaorako anie ny Andriamanitry ny famonjena;
Isaorako anie ny Andriamanitry ny famonjena, fa tsy maty tamin''ny ratsy nataoko aho,
Fa tsy maty tamin''ny ratsy nataoko aho!
Isaorako anie ny Andriamanitry ny famonjena
Fa mbola tsy maty tamin''ny ratsy nataoko anie ahoe" -}

{Raha mbola velona koa,
aoka tsy mba hohadinoina mandrakizay
Ilay namonjy antsika
Eritrereto isan''andro,
Ka misaora an''Andriamanitra,
Ka miderà, mihirà
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50511, '51. Hatrizay re, nivesarako ny heloko', NULL, NULL, '
{- "Hatrizay re, nivesarako ny heloko
Sady tsy mba nahafaka ny fahoriako
Mandra-pingadoko teto
Endre ! ity fitoerana
Mandra-pingadoko teto
Endre ! ity fitoerana" -}

{- "Tsy eto va no nialan''ny entana
Tao an-damosiko? Tsy eto va
No nandresena ny herin''ny fasana?
Tsy eto va no nahatapaka
Ny kofeky namehezana?
Endre ! ity fitoerana
Mandra-pingadoko teto
Endre ! ity fitoerana" -}

{- "Veloma re ry ilay hazo
Mitanambokovoko !
Veloma re, ry ilay fasana,
Fa Ralehilahy izay nomenarina
Nisolo ahy ka veloma re!
Dia veloma indrindra" -}

{- "Dera, laza, haja
Sy ny voninahitra,
Ho Anao, ry Tompo
Haleloia
Voninahitra ho Anao" -}

{Veloma re ry ilay hazo
Mitanambokovoko !
Veloma re, ry ilay fasana,
Fa Ralehilahy izay nomenarina
Nisolo ahy ka veloma re!
Dia veloma indrindra}
', '#3-II-5', '#3') /end

insert into song values (50521, '52. Ny Mpanjakan''ny Jerosalema', NULL, NULL, '
{1.
Ny Mpanjakan''ny Jerosalema
Monina ao Ziona;
Izy no Lohan''ny firenenana
Tompon''ny tany ama-monina :
Mba derao sy omeo hasina,
Fa ambony voninahitra}

{2.
Izy no tsy refesi-mandidy,
FA ataony izay sitrany,
Nefa ny fandidiany mahitsy,
Mampiadana ny olony
Sambatra izay feheziny,
Ka manaraka ny sitrany.}

{3.Ny Anarany Imanoela :
Izy no Andrian''ny andriana,
Ka misatroka diadema
Amin''ny lohany masina,
Ary ny tehim-panjakany,
Tsy miala eo an-tànany.}

{4.
Ny eklesia no anjakany,
Ny kristiana olony,
Ny Baiboly no tena lalàny,
Fitiavana ambarany,
Vavaka no asainy atao,
Mahavelona izay manao.}

{5.
Ny mpanompony manompo Azy,
Tretrika sady finaritra,
Fa mahazo ny soa tsy lany?
Valin''ny fitiavam-bavaka;
Ary ny olona dia tretrika
Fa nomeny hasoavana.}

{6.
Ry Mpanjakan''i Jerosalema,
Mba ampio ny mpanomponao,
Hahalala sy hanantena
Ny fahasoavana ao Aminao.
Mba jereo izahay, ka iantrao
Fa ambany fiarovanao.}
', '#3-II-5', '#3') /end

insert into song values (50531, '53. Jeso Tompo Andriamanitra', NULL, NULL, '
{1.
Jeso Tompo Andriamanitra,
Zananky ny Tompon''ny lanitra;
Tonga Izy mba hanavotra
Sy hanafa-patotra}

{FIV.
Nomeny ny ràny soa,
Mba ho avotra tokoa;
Io no diloilon''ny ratram-po.
Izay fanasitranana}

{2.
Jeso Tompo natao fanatitra
Mba hanafa-keloka,
Ka ny Tenany no natolony
Ho an''izay olony.
[FIV.]}

{3.
Jeso Tompo dia nanelatra,
Teo imason''ny rafiny,
Ny latabatra mahasambatra,
Ho an''ny mpiantafiny.
[FIV.]}

{4.
Jeso Kristy tena Mpisorona,
''Zay niakatra ao an-danitra :
Any Izy manao fifonana
Ho an''ny voavotra
[FIV.]}

{5.
Jeso Tompo ! henoy ny vavakay !
Ka amindranao fo izahay
Mamelà ny fahadisoanay,
Fandrao very izahay.
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50541, '54. Ao an-danitra ao ambony', NULL, NULL, '
{1.
Ao an-danitra ao ambony
Sy ety an-tany koa,
Soa tsy manam-paharoa
Ny Anaranao Jeso !}

{FIV.
"Maminay
Ny Anaranao Jeso !"}

{2.
Voninahitra ama-daza,
An''izao tontolo izao,
Tsy mba misy mahalaza
Ny Anaranao Jeso !
[FIV.]}

{3.
Ny anaran''ny anjely,
Na dia tsara sady soa,
Tsy mba misy velively,
Mahasambatra tokoa.
[FIV.]}

{4.
Ny Anaranao Irery,
Ry Jesosy Tomponay,
No mamonjy tsy ho very,
Ny mpanota, toa anay.
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50551, '55. Manehoa ny lazan''i Jesosy hianareo', NULL, NULL, '
{- "Manehoa ny lazan''i Jesosy hianareo,
Ary dia mankalazà ny Anaran''i Jesosy" -}

{- "Manehoa ny lazan''i Jesosy
Ianareo mpanompony !
Torio aminy, lazao aminy,
Fa tsy misy fitiavana toy izany,
Fa nanolotra ny ainy Hamonjy ny mpanota" -}

{- "Mandehana mba hilaza an''i Jesosy.
Ambarao re ny heriny;
Lazao re ny fiantràny,
Fa tsy misy izay olona
''Zay tsy niantràny" -}

{- "Ambarao re ny heriny,
Ankalazao ny Anarany,
Fa zanaky ny Avo Indrindra Izy;
Fa ho sambatra hianao
Raha manana Azy re!
Izy re no tokinao" -}

{- "Ny Andriamanitry Isiraely hanome,
Hanome hery, hanome tanjaka,
Ho anareo olona.
Isaoranay
Isaoranay Izy" -}
', '#3-II-5', '#3') /end

insert into song values (50561, '56. Mifohaza ka mihirà', NULL, NULL, '
{- "Mifohaza ka mihirà
Mihirà dera ho an''i Jehovah
Mifohaza ry fanahy
Hidera ny Mpanavotra;
Mifohaza ry fanahy
Hidera ny Mpanavotra;
Derao ny fitiavany,
Derao koa ny heriny !
Derao, Derao!
Miderà ny fifonany !
Miderà izay nataony
Ho antsika" -}

{- "Ianareo izay mandeha
Ho an-tanàna, any Ziona,
Mifalia, mifalia isan''andro
Amin''i Kristy Mpamonjy
Mifalia
Mifalia isan''andro" -}

{- "Ho rentsika faingana Izy
Hilaza hoe : Avia aty
Izay notahin''ny Raiko,
Handova ny fanjakany" -}

{- "Dia any no hihirantsika
Amin''ny feo tsara indrindra,
Dia ny fihiràn''i Mosesy
Sy ny Zanak''Ondry koa" -}
', '#3-II-5', '#3') /end

insert into song values (50571, '57. Haleloia, ry Mpanjaka', NULL, NULL, '
{- "Haleloia, ry Mpanjaka,
Haleloia!
Hoby no asandratray,
Haleloia!
Tonga Jeso Tompo haka,
Haleloia
Ny Fanjakany hatrizay" -}

{- "Haleloia, ry Mpanjaka,
Hoby no asandratray,
Tonga Jeso Tompo
Haka Fanjakany hatrizay
Zanaka Malala,
''Lay Tokana tsy foy
Nefa toy ny olon-kala,
Dia, tsy mimoimoy" -}

{- "Haleloia
Haleloia, ry Tsitoha
Vonjy, Iraky ny Ray
He ! miondrika ny loha
Mba ho fanajana Anao,
Dia tsy misalasala
Sady tsy miroa foa,
Haleloia ! hampody ny adala
Haleloia ! ho an-dapa feno soa ;
Haleloia ! hampody ny adala
Haleloia ! ho an-dapa feno soa" -}

{- "Haleloia ! hoderaina Ianao,
Ianao, ry tiana anay ô!
Haleloia ! hoderaina Ianao,
Haleloia ! tia anay,
Ka ny saotra eran-tsaina
Ho Anao Mpanavotra anay,
Novidina tamin''aina
Ho Anao dia Anao;
Tsy mba manan-kolazaina,
Haleloia ! no atao.
Haleloia ! hoderaina Ianao,
Haleloia ! tia anay,
Ka ny saotra eran-tsaina
Ho Anao Mpanavotra anay" -}

{- "Haleloia
Haleloia, ry Tsitoha
Vonjy, Iraky ny Ray
He ! miondrika ny loha
Mba ho fanajana Anao,
Dia tsy misalasala
Sady tsy miroa foa,
Haleloia ! hampody ny adala
Haleloia ! ho an-dapa feno soa ;
Haleloia ! hampody ny adala
Haleloia ! ho an-dapa feno soa" -}
', '#3-II-5', '#3') /end

insert into song values (50581, '58. Jeso avao ro, Mpanjaka Tale', NULL, 'Hira Antandroy', '
{1.
Jeso avao ro, Mpanjaka Tale,
Tompon''ny gnay ndrake koa mora fo;
Mivory eto ieahay ndra miné,
Hangatake Aminao ''zay soa fo :}

{FIV.
"Jesosiko ô ! lombao fo mitoreo,
Mba eteo devoly manao anay, omeo
Vata, vôho gn''ay
Ndrake koa fagnahinay,
Fa ia avao ro Mpanjakanay
Magnatse avao betandroke lomay"}

{2.
Tompon''gny lagnitse
avao ro Tale,
''Ndra an-tane atoy,
Tompo ndra an-dagnitse ao
Mivola soa mare hoe he :
An-dagnitse ao tragno soa ho anao.}

{3.
Tompogny lagnitse avao ro soa
Miambe hasoa Aminao ''zahay;
Ao gn''Ampandomba malazanay soa,
Fagnahy Masy, Mpagnanatse anay
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50591, '59. Fitiavana ny Andriamanitsika', NULL, NULL, '
{- "Fitiavana ny Andriamanitsika
Fitiavana
Fitiavana ny Andriamanitsika !
Fitiavana ny Andriamanitsika !
Fitiavana
Fitiavana ny Andriamanitsika" -}

{- "Terena, tsy vozonana
''Zay te-ho mpanompony" -}

{- "Be no mimonomonona
Maro no misentosento
Misy koa izay manahy,
Satria tsy fantany
Ny soa izay ho azony" -}

{- "Indrisy fa mahantra,
Mahantra hianao
Ry kely finoana
Indrisy fa mahantra,
Mahantra hianao
Ry kely finoana" -}

{- "Betlehema tsinjovinao,
Kalvary tazanonao
Fa ao no tena fototr''aina
Nanehoana ny soa
Famaranana farany !
Vita izao ny famonjena
Voninahitra, Voninahitra
Ho Aminy, ho Anao
Ry Avo Indrindra !
Fiadanana ho amin''ny
Ety ambonin''ny tany
Amin''izay ankasitrahany" -}

{- "Indrisy fa mahantra,
Mahantra hianao
Ry kely finoana
Indrisy fa mahantra,
Mahantra hianao
Ry kely finoana" -}
', '#3-II-5', '#3') /end

insert into song values (50601, '60. Ary raha nanakaiky', NULL, NULL, '
{- "Ary raha nanakaiky
An''i Jerosalema Izy
Ka tonga teo
An-tendrombohitra Oliva
Ary ankabiazan''ny vahoaka
Dia namelatra ny lambany
Teo amin''ny làlana" -}

{- "Ary ny sasany nanapaka
Ny rantsan-kazo
Dia nahahany teny an-dàlana izany,
Ary ny vahoaka izay nialoha
Sy nanaraka Azy dia
Niantsoantso hoe :" -}

{- "Hosana
Hosana
Hosana
Hosana ny Anaranao Jehovah" -}

{- "Lazao amin''ny Zanakavavin''i Ziona hoe :
Indro ny Mpanjakanao avy Aminao
Malemy fanay sady mora fo indrindra ô
Hosana ho an''ny Zanak''i Davida ô!
Hotahina Izay avy amin''ny Anaranao Jehovah !
Hosana ho amin''ny Avo indrindra ô!
Hotahina Izay avy amin''ny Anaranao Jehovah" -}

{- "Hoderaina
Hoderaina ny Anaranao Jehovah !
Hoderaina
Hoderaina
Ny Anaranao Jehovah" -}
', '#3-II-5', '#3') /end

insert into song values (50611, '61. He ! ny hatsaran''ny tongotrao', NULL, NULL, '
{- "He ! ny hatsaran''ny tongotrao
Ry Ilay mitory teny soa" -}

{- "Iza
Re izao?
Iza" -}

{- "Indro Izy, indro avy
Ao Edoma
Indro Izy, indro avy Izy,
Indro avy." -}

{- "He ! ry Ilay mitory
Teny soa mahafaly fo!
He! ny fitafiany mamirapiratra,
Tahaka ny orampanala ô!" -}

{- "Indro miakatra Izy
Ho any Jerosalema" -}

{- "Jereo Kalvary, Jereo Golgota,
Tazano Getsemane, fa tao no nijalian''ny Tompo tia antsika,
Fa tao no nijalian''ny Tompo tia antsika
O ! andeha anio isika
Ho any Jerosalema
Hanao fanatitra ho an''Andriamanitra Avo" -}
', '#3-II-5', '#3') /end

insert into song values (50621, '62. Anio no hisantaranay', NULL, NULL, '
{- "Anio no hisantaranay
''any ka
Haleloia rahatrizay !
Fa lehibe Ilay Mpamonjy soa malalanay
Haleloia, Haleloia" -}

{- "Ary an-dapanao,
Ry Ray, Haleloia!
Ny namanay manandra-peo
Haleloia" -}

{- "Fidio ny fiangonanay
Ho lapam-boninahitrao,
Ho toy ny afak''ady aho,
Mihira vaovao
Haleloia
Haleloia
Haleloia" -}

{- "Ry Ranao ry Zanak''Ondry soa
No manafaka ahy izao :
Avy izahay izao,
Ianao Izay mandrika handray
Haleloia" -}

{Avy izahay izao,
Ianao Izay mendrika handray
Haleloia
Haleloia
Haleloia}

{- "Ry Ranao ry Zanak''Ondry soa
No manafaka ahy izao :
Avy izahay izao,
Ianao Izay mandrika handray
Haleloia" -}
', '#3-II-5', '#3') /end

insert into song values (50631, '63. He ! mihoby ny foko, ry Jesosy ô !', NULL, NULL, '
{1.
He ! mihoby ny foko, ry Jesosy ô!
Voavonjy ka faly ery,
Indrafo mahagaga no azoko izao,
Fiadanana am-po hatrety :}

{FIV.
Dera, laza, sy haja no aoka ho Anao,
Ry Mpamonjy malalako ô!
Avy amin''ny foko
Izay navotanao,
No atolotro izany izao}

{2.
He ! ny heloko betsaka navelana,
Ry Mpanjakako be indrafo !
Ka tohano ny iako hanaraka Anao,
Hahatrarako tsara ny To
[FIV.]}

{3.
Eo ambonin''ity tany màzana ity,
Dera, laza, omeko Anao,
Ao an-danitra tsara fonenana soa,
Ihobiako ny fitiavana
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50641, '64. He ! zava-tsoa no re aty', NULL, NULL, '
{1.
He ! zava-tsoa no re aty.
Tanteraka tao Kalvary
Ny vonjy ho fiainana,
Tsy faly va ny olona ?}

{FIV.
Jesosy dia natao ho sorona,
Hanafaka ny olombelona ;
He ! ho antsika ny mandrakizay,
Fiainan-dapa soan''ny Ray}

{2.
Nahoana no mikaroka,
Mitady fanavotana
Amin''ny tsy maharitra
Kanefa hoe ! tolorana ?
[FIV.]}

{3.
He ! andro soa sy lehibe
No tratrany anio izao;
Rombaho sao ho neninao
Fa mbola re ny teny hoe :
[FIV.]}
', '#3-II-5', '#3') /end

insert into song values (50651, '65. Ny Anaranao Jesosy', NULL, NULL, '
{1.
Ny Anaranao Jesosy
No mba ifikirako,
Fa satria misy vonjy,
Sady ifaliako}

{FIV.
Tsara re
Ny Anaranao, Jeso.}

{2.
Ahazoam-pamonjena
Ny Anaranao Jeso,
Sady misy fandresena,
Ho azon''ny mino e !
[FIV.]}

{3.
Na dia iza aza ambony
Sy malaza anarana,
Ny Anaranao Jesosy,
No malaza fatratra
[FIV.]}

{4.
Ny Anaran''i Jesosy,
Mazala ao ambony ao :
Ny anjely dia mihoby
Sy midera Azy ao
[FIV.]}

{5.
Enga aho ry Mpamonjy !
Hankalaza Anao am-po !
Ny Anaranao Jesosy,
No mba hoventesiko.

Jeso ô
Ny Anaranao Jeso}
', '#3-II-5', '#3') /end

insert into song values (50661, '66. He ! fotoana ankalazaina izao', NULL, NULL, '
{- "He ! fotoana ankalazaina izao,
Maharavo ny marobe tokoa;
Manambara sy milaza tokoa
Fitiavan-dehibe}

{Ny fitiavana ry Jesosy,
No ivorianay, mpianatrao
Hidera sy hankalaza Anao,
Tompo be fitia" -}

{- "Maminay tokoa
Ny fitiavanao anay
Hahatonga anay
Hanolo-tena ho Anao;
Ka diovinao ny fonay
Ry Jesosy ô !
Mba hahaizanay
Hitondra tena tsara e "-}

{- "Enga anie ''ty andro anio ity
Mba hamoha anay tokoa,
Hanoloranay ny tenanay
Ho tempoly masina,
Hisy vokatra ny fiainanay ety
Ho fankalazana re,
Ny Anara-masinao" -}

{- "Maminay tokoa
Ny fitiavanao anay
Hahatonga anay
Hanolo-tena ho Anao;
Ka diovinao ny fonay
Ry Jesosy ô !
Mba hahaizanay
Hitondra tena tsara e "-}
', '#3-II-5', '#3') /end

insert into song values (50671, '67. Mifalia, ry voavonjy', NULL, NULL, '
{- "Mifalia, ry voavonjy,
Mifalia, mihobia, miravoa
Eny, manandrata feo,
Azontsika ny famonjena
Izay very tao Edena !
Isaorana anie ny Mpamonjy
''Zay nisolo voina
Ny meloka indrindra;
Ny ainy no nomeny
Mba hanavotany antsika
Ny ràny no nalatsany,
Mba hanadiovany ny ota" -}

{- "Ry meloka mba manatona;
Ianareo mpanota mba avia,
Fa miandry Jesosy Tompo
Hanadio ny otanao,
Fa avy izao ny andro famonjena :
Ilaozy re ny sitrapo.
Nahoana re no tsy manatona,
Dieny mbola mazava?
Fa raha avy ny alina maizina re,
Sy ny tahotra izay mampahory,
Mandehana ary dieny malaina
Raha mazoto tsy afaka intsony" -}

{- "Indrisy fa na dia maty
Mahantra aza ny Tompo,
Ny mpanota mbola mifikitra ihany
Amin''ny ditra''ny fony ô !
Eny, eny, ry Jeso,
Fa niaretanao izany
Noho ny fitiavanao anay
Tsy hohadinoina mandrakizay
Ny fitiavanao, ry Tompo Tsitoha,
Raha mbola velon''aina,
Na faly na malahelo :
Ianao Irery ihany
No tokin''ny fiainanay" -}

{- "Sambatra iazy manana Anao,
Ry Jesosy Tompo;
Tsy mba misy fahoriana" -}

{- "Sambatra iazy manana Anao,
Ry Jesosy Tompo;
Tsy mba misy fahoriana
''Zay haninona ny fo arovanao,
Ry Mpamonjy
Eny, eny, ry Jeso,
Fa niaretanao izany
Noho ny fitiavanao anay
Tsy hohadinoina mandrakizay
Ny fitiavanao, ry Tompo Tsitoha,
Raha mbola velon''aina,
Na faly na malahelo :
Ianao Irery ihany
No tokin''ny fiainanay" -}
', '#3-II-5', '#3') /end

insert into song values (50681, '68. An''i Kristy, an''i Kristy', NULL, NULL, '
{- "An''i Kristy, an''i Kristy
Ny fanjakana, an''i Kristy
Azy izao, an''i Kristy
Na ny hery? an''i Kristy
Na ny fahefana, an''i Kristy
Na ny fandresena, an''i Kristy
An''i Kristy ny fanjakana" -}

{- "An''i Kristy ny fanjakana,
Azy izao?
An''i Kristy ny fanjakana
Azy izao koa, na ny hery
Na ny fahefana, na ny fandresena
An''i Kristy ny fanjakana" -}

{- "Azy koa ny voninahitra
Sy ny fiandrianana,
Na ny laza na ny fiandrianana :
An''i Kristy ny fanjakana" -}

{- "Fa Izy izao, Izy izao
No Tompo, Izy izao
Mitondra, Izy izao
An''izao, Izy izao
Tontolo izao, Izy izao
No Mpanjaka, Izy izao
Mandrakizay, Izy izao
Mandrakizay Kristy Tomponay" -}

{- "O ! Azy Irery ny seza fiandrianana
Azy Irery !
Azy Irery ny satroboninahitra
O ! Azy izao sy mandrakizay
Sy ho mandrakizay,
Azy izao sy mandrakizay
Ny satroboninahitra ho Azy" -}

{- "Fa Izy izao, Izy izao
No Tompo, Izy izao
Mitondra, Izy izao
An''izao, Izy izao
Tontolo izao, Izy izao
No Mpanjaka, Izy izao
Mandrakizay, Izy izao
Mandrakizay Kristy Tomponay" -}
', '#3-II-5', '#3') /end

insert into song values (50691, '69. Andriamanitra ô', NULL, NULL, '
{- "Andriamanitra ô" -}

{- "Mitsinjova, mitsinjova ahy
Ianao, ry Ray !
Fa mahantra indrindra anie aho,
Raha tsy Jeso
No Sakaizako" -}

{- "Haleloia !
Amena, Haleloia
Haleloia, Amena !
Ny dera aman-daza,
Ny hery anie
Ho anao anie!
Ho Anao anie
Ry Zanak''Ondry
Ny dera aman-daza
Sy hery anie
Ho Anao anie" -}
', '#3-II-5', '#3') /end

insert into song values (50701, '70. Ry Fanahy Masina ao ambony', NULL, NULL, '
{1.
Ry Fanahy Masina ao ambony,
Irak''i Jeso Tompo,
Izahay aty dia mila vonjy,
Hafanao, hatanjaho !}

{FIV.
Ry Fanahy Masina ô ! midina,
Hanafana, hampahery
Izany fona lazolazom-pahotana,
O ! vonjeo, sao very.}

{2.
Ry Fanahy Masin-Janahary,
Hazavao re ny sainay,
He ! Ny fonay osa sy marary,
Mba sitrano handresenay
[FIV.]}

{3.
O ! ry Afo-Masin''i Jesosy
''Zay irin''ny firenena,
lasanao koa re ''ty Nosy,
Mba handray ny famonjena
[FIV.]}
', '#3-III', '#3') /end

insert into song values (50711, '71. Ny andro soa nirina', NULL, NULL, '
{1.
Ny andro soa nirina,
Omeo anay Tompo ô !
Ho anton''ny fifaliana
Sy hifalian''ny fonay.}

{FIV.
Maniry ho tony
Ny hetahetanay
Ry Andro ao ambony
Mba mirotsaha ho anay !}

{2.
Fanahy efa nomena,
Midina eto indray,
Ho anton''ny famonjena
Sy hifalian''ny fonay.
[FIV.]}

{3.
Izay rehetra mahazo
Ny fitahiany soa,
Manitra toa voninkazo,
Manitra sy feno soa.
[FIV.]}
', '#3-III', '#3') /end

insert into song values (50721, '72. Ry zatovo mitsangàna', 'Rainisoalambo', NULL, '
{1.
Ry zatovo mitsangàna
Mitsangàna, mitoria;
Mandehana hanambara;
Ny marary hasitrano;
Ny malemy hatanjaho;
Ny Fany efa nomeny;
Ny hery efa natolony
Hampahiratra ny jamba}

{
2.
Haingàna ''ray olom-bery,
Fa i Jeso no miantso
''Zay rehetra voavonjy,
Amin''ny Anarany;
Fa akaiky ny Mpitsara,
Koa miderà
Ny Mpanjakan''ny lanitra,
Ny Sakaizan''ny mino}
', '#3-IV-1', '#3') /end

insert into song values (50731, '73. Ry malala, aza dia fatra-pitia vola re !', 'Rajeremia', NULL, '
{1.
Ry malala, aza dia
Fatra-pitia vola re !
Fa andrao hanary ny soa,
Ka lasan-davitra}

{2.
Ka ho lasa voavily re !
Ry hava-manaraha
Ny Tomponao ambony ao :
Hanasoa anao Izy.}

{3.
Fa miantso anao Izy,
Ry malala, avia !
Manatona Azy hianao,
Fa Jeso Tomponao.}
', '#3-IV-1', '#3') /end

insert into song values (50741, '74. O ! mifohaza ''zay olom-bery', 'Rasaia', NULL, '
{O ! mifohaza ''zay olom-bery
Fa ny Tompo miantso anao
Amin''izao làlana izao,
Mahitsy, mazava izao.}

{FIV.
Faly re ny foko
Ravo re ny saiko
Rehefa nandre ny teniny
Nolazain''ny anjely.}

{Tamin''ny mpiandry zanakondry
Tao Betlehema. O ! misia
Hidera Azy finaritra
Ao an-danitra ao ambony
[FIV.]}

{Jeso Tompontsika,
Ray hava-malala,
Iza moa izao olo-mino izao
No tsy ho faly amin''io andro io.
[FIV.]}

{Andron''ny Mpanjaka be voninahitra
Ao an-danitra, andro fahasambarana
Andro lehibe, hifaliana :
Mihobia, ry olom-bery !
[FIV.]}

{Fa hahita fiadanana
Ao an-danitra
Raha ''haritra hianao,
Hoy ny Tomponao ao an-danitra
[FIV.]}
', '#3-IV-1', '#3') /end

insert into song values (50751, '75. Ry Jeso Tomponay ô !', 'Rajeremia', NULL, '
{1.
Ry Jeso Tomponay ô !
Mba raisonao ny hatakay
Mba ampianaro izahay,
Fa zazabodo izahay.}

{2.
Sady tsy àry ny finoanay
Ka mangataka Aminao
Izay mangataka
Ny Fanahy Masinao ato.}

{3.
Mba hampianatra anay;
Mba hiara-monina eto aminay
Mba hampianatra anay izao,
Ry Jeso Tomponay izao}

{4.
Mba omeo fahalalana aoa,
Mba ho mpianatrao.
Ry mpanota isika,
Miverena hianao.}

{5.
Mba hibebak izay natao,
Ao ny Tompontsika izao,
Izay Mpamindra fo izao;
Jeso ao ambony.}

{6.
Tompon''ny finoana,
Izy tsy mba nahafoy izao
Tompon''ny finoana,
Izy tsy mba nahafoy izao}
', '#3-IV-1', '#3') /end

insert into song values (50761, '76. Rainay Masina any an-danitra', 'Rajeremia', NULL, '
{1.
Rainay Masina any an-danitra,
Aza manadino anay izao,
Fa omeo ny Fanahy Masina,}

{2.
Izay efa nirahinao
Amin''ny Anaran''ny Zanakao, aty,
Malalanao Izay efa namonjy anay;}

{3.
Fa aza avelanao koa izahay
Fa mba hohazoninao mba ho
Olonao sy ho mpanomponao}

{4.
Marina, mba ho irakao,
Mba hitory ny teninao isan''andro
Sy hahalala Anao : ho olo-masina}
', '#3-IV-1', '#3') /end

insert into song values (50771, '77. Mamindrà fo aminay', 'Rajeremia', NULL, '
{1.
Mamindrà fo aminay
Ry Mpanjakanay izao !
Ry Jeso Mpanjaka malaza,
No Mpanjakanay izao}

{2.
Mpanjaka be voninahitra,
Sady tia zazakely e !
No Mpanjakanay izao,
Manatona an''i Jeso}

{3.
Ianao mpanota izao,
Fa Izy no Mpamonjintsika
Dia ho voavonjy hianao,
Raiso ny Fanahy Masina}

{4.
Mba hampianatra anao,
Dia hovonjena hianao,
Ka ho afa-keloka.
Jeso Ilay Maherinay.}

{5.
No nijaly Irery tao;
Nafeniny tsara ny heriny
Mba ho voavonjy ny mpanota izao;
Dia ho afa-keloka hianao.}

{6.
Ry havako izao.
Izy niakatra any an-danitra,
Mipetraka an-kavanan-dRainy e !
Manatona Azy hianao,}

{7.
Dia ho be ny famindrampony
Aminao noho ny fahotanao
Dia ho be ny famindrampoy
Aminao izao.}
', '#3-IV-1', '#3') /end

insert into song values (50781, '78. Ry Tomponay an-danitra', 'Rainisoalambo', NULL, '
{1.
Ry Tomponay an-danitra
Nahitany soa be,
Aoka mba hidera Azy,
Fa mpanota lalandava
- "Fa mpanota lalandava
Izahay mpanomponao" -}

{2.
Ry Jeso mana-kery,
Nanangana malemy,
Faly sy mihoby izao ny olonao.
Amin''izao andronao izao,
- "Ry ondry mania ô !
Miverena hianao" -}

{3.
Fa akaiky ny Tomponao;
Fa maro no antsoiny,
Ka vitsy no manatona.
O ! avia ry havako,
- "Aza mba misalasala
Fa Jeso no Mpanavotrao" -}
', '#3-IV-1', '#3') /end

insert into song values (50791, '79. Tsy misy mahalala ny andro', 'Rainisoalambo', NULL, '
{1.
Tsy misy mahalala ny andro
Hahatongavan''ny Tompo
Ka, na atoandro, na maraina,
Koa miomàna ry havako malala;
Eny, ry mpanota, ry havako !
Indro fa hiposaka ny mazava izao,
Ny mazava lehibe indrindra,
Koa mibebaha ry havako izao :
Raha tsy mibebaka ianao
Dia fanamelohana no anjaranao}

{2.
Koa mahereza mangataka hianao,
Amin''ny Tomponao, an-danitra
Koa manaraha ny Tompo hianao ;
Koa miezaha, ry havako izao,
Mba handray ny anjaranao :
Izy hanome ny rariny anao,
Hifalianao tokoa
Amin''Andriamanitra.
Hifalianao tokoa
Amin''Andriamanitra !}
', '#3-IV-1', '#3') /end

insert into song values (50801, '80. Mifohaza, mifohaza', 'Rainisoalambo', NULL, '
{1.
Mifohaza, mifohaza,
Ry fanahy izao!
Fa mandona izao ny Tompo,
Ka antsoy ny namanao.}

{- Ka antsoy ny namanao,
Fa i Jeso no mizara" -}

{2.
Lova soa, lova soa,
Tsara fara izao!
Ao an-danitra ao ambony
Fiainana mateza.}

{- Fiainana mateza,
Fiadanana no any"-}

{3.
Ny mpanota, ny mpanota
Mba maniry mba hahita
Ny Mazava lehibe
Mipetraka ao an-tànana,}

{- Mipetraka ao an-tànana
Ankavanan''ny Ray"-}

{4.
Miravoa, miravoa,
Fa hahita ny Mazava;
Ny Mazava lehibe
Mipetraka ao an-tànana,}

{- Mipetraka ao an-tànana
Ankavanan''ny Ray"-}
', '#3-IV-1', '#3') /end

insert into song values (50811, '81. Miambena, ry mpanota', 'Rainisoalambo', NULL, '
{1.
Miambena, ry mpanota,
Fa akaiky ny fotoana;
Indro fa ho tonga izao
Ny andron''ny Tompo:
Mivavaha, mivavaha!
Ka aza mba mitsahatra}

{2.
E! Jeso Ilay Mahery
No hitsena ny mpanota,
Aoka mba hilaozantsika
Ny fomban''ny maizina;
Andeha mba hanatona
Ny Mazava amboninao.}

{3.
E! Jeso Ilay Mazava,
No harahin''ny mpanota
Ao an-tranon''ny Ray.
Ao an-danitra ao ambony
No honenan''ny mino
An''i Jeso Tompontsika}
', '#3-IV-1', '#3') /end

insert into song values (50821, '82. Mamiko ny teny masina', NULL, NULL, '
{1.
Mamiko ny teny masina,
Dia ny teninao Andriamanitra ô!
Hitako ny zava-miafina
Iveloman''ny fanahiko.}

{FIV.
Eny, mofon''aina
Iveloman''ny fanahiko.}

{2.
Sitrako ny teny masina,
Dia ny teninao Andriamanitra ô!
''Zay mamelona fiainana
Tsy mba hampisaraka Aminao
[FIV.]}

{3.
Heriko ny teny masina,
''Zay teninao Andriamanitra ô!
Toky amin''ny andro sarotra
Hibanjinako ny tavanao.
[FIV.]}

{4.
Velona fa tsy matroka
''Zay teninao Andriamanitra ô!
Lova soa azo antoka
Sy haharitra mandrakizay.
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50831, '83. Ny Baiboly re dia zava-tsoa tokoa', NULL, NULL, '
{1.
Ny Baiboly re dia zava-tsoa tokoa,
Zava-maminay fa nahitanay soa,
Ka derao ary sy ataovy an-kira hoe :
Ny Baiboly re dia zava-tsoa tokoa
Ny Baiboly no mampiray
Ny fon''ny maro mba ho tonga iray.}

{FIV.
Ny Baiboly re dia zava-tsoa tokoa,
Zava-maminay fa nahitanay soa,
Ka derao ary sy ataovy an-kira hoe :
Ny Baiboly re dia zava-tsoa tokoa}

{2.
Jereo kely ange itony soa be
''Zay manerana ity Nosy makadiribe,
Ka derao ary sy ataovy an-kira hoe:
Ny Baiboly re dia zava-tsoa tokoa:
Ny Baiboly no mampisava
Ny aizim-be.
[FIV.]}

{3.
Hevero kely ange, isika vory izao,
Fa mpanompo-sampy
avokoa teo aloha,
''Nefa niova re dia zava-tsoa tokoa.
Ny Baiboly no sabatra
Ho entinay ho fandresena.
[FIV.]}

{4.
Diniho kely ange ny fomba fahizay,
Fa sikidy, ody samihafa no notompoinay,
Fa ankehitriny dia ny Baiboly indray
No manjaka eto an-tanindrazanay.
Ny Baiboly, io ihany,
No nandresy ireo rehetra ireo.
[FIV.]}

{5.
Ny Baiboly re dia zava-tsoa tokoa
Fa mampanantena anay fiainana mandrakizay
fa hilaozanay ity tany miova ity,
Fa misy lanitra tsy mety miova ary.
Ny tenin''ny Baiboly
No iankinan''ny fiainanay rahatrizay.
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50841, '84. Ry Sekoly Alahady', NULL, NULL, '
{1.
Ry Sekoly Alahady
''Zay mianatra Baiboly,
Aza afa-po sahady,
Mbola ilaina ny Sekoly"}

{FIV.
"Ao no ianaranay ny Tenin''Andriamanitra
Ao no ianaranay ny Tenin''Andriamanitra"}

{2.
Matio, Marka, Lioka, Jaona,
Apostoly sy Romana,
Korintiana, isa, roa,
Galatiana, Efesiana
[FIV.]}

{3.
Filipiana, Kolosiana
Tesaloniana, isa, roa,
Timoty, isa, roa,
Ary Titosy koa.
[FIV.]}

{4.
Filemona, Hebreo, Jakoba,
Petera, isa, roa,
Jona, isa, roa, telo,
Joda, Apokalypsy koa.
[FIV.]}

{5.
Mba misy boky hafa,
Izay manan-kolazaina
Amintsika olombelona
Raha isika tsy halaina
[FIV.]}

{6.
Ao ny zava-tsaro-bidy,
Ka antsika ny safidy;
Aoka isika tsy halaina
Fa nomen''ny Tompo anina.}

{Aoka samy hianatra ny Tenin''Andriamanitra
Aoka samy hianatra ny Tenin''Andriamanitra!}
', '#3-V', '#3') /end

insert into song values (50851, '85. Anafarana izay mbola miaina', NULL, NULL, '
{1.
Anafarana izay mbola miaina,
Ho tsarovana tsara ao an-tsaina,
Fa na inona na inona atao,
Dia raiso ilay Baibolinao;
Fa voambara ao anatiny ao,
Fitiavana tsy mba mandao
Baiboly, Boky : Boky masina indrindra}

{Raha mino ianao,
Dia ho voavonjy tokoa;
Aza kivy mamaky ny Boky hianao,
Fa voambara ao anatiny ao,
Fitiavana tsy mba mandao
Baiboly, Boky : Boky masina indrindra}

{2.
Ry mpamaky izay mahalala
Ny atoro sy izay voalaza;
Jesosy izao no ambara,
Jesosy, Ilay fitiavana
Fa voambara ao anatiny ao,
Fitiavana tsy mba mandao
Jesosy, Tompo : Tompo tsy mba mandao.}

{Raha misy manjo,
Tsy mba kivy ny fo;
Loharano madio, fanavaozam-panahy,
Fa voambara ao anatiny ao,
Fitiavana tsy mba mandao:
Jesosy, Tompo : Tompo tsy mba mandao.}
', '#3-V', '#3') /end

insert into song values (50861, '86. Baiboly, tenin''Andriamanitra', NULL, NULL, '
{1.
Baiboly, tenin''Andriamanitra,
Boky manambara ny fiainana,
Boky feno toky, fiadanana,
Boky feno fahasambarana}

{FIV.
Mba diniho fatratra
Ny Baiboly Masina,
Ny Baiboly, tena Filazantsara
Amena! ho tanteraka tokoa,
Amena! ''zay lazain''ny teny ao;
Tena fiainana mandrakizay, Amena!}

{2.
Ao Andriamanitra, ''zay Ray tokoa;
Ao Jesosy Zanaka Mpamonjy soa,
Ao no ahitana ny Fanahy koa,
Ao ny Telo ''nefa koa Izy
[FIV.]}

{3.
Io no fototr''aina mahavelona;
Io no tena vatsy ivelomana;
Io no tena aina mahavelona;
IO no fototr''aina, foto-tsoa
[FIV.]}

{4.
Be ny fitahiana efa hita tao;
Be ny fifaliana mbola raisina ao;
Be ny fiononam-po atolony,
Be dia be ka misy ho anao
[FIV.]}

{5.
Olon-dehibe mahazo soa tokoa;
Olom-bodo, zazakely, voky soa,
Olona efa tonga voaova fo,
Ota voavela, voadio.
[FIV.]}

{6.
Lova tsara fara no omena anao;
Lapa soa madio, amboariny;
Làlana mazava itarihany,
Lanitra vohany ho anao
[FIV.]}

{7.
Indro, raiso tsara ity Baiboly ity;
Indro, tano mafy ''zay anananao;
Indro, raiso ka torio mafy hoe:
Io no foto-piainantsika re
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50871, '87. Ny teninao Jeso Mpanjaka', NULL, NULL, '
{1.
Ny teninao Jeso Mpanjaka
Dia mandroso tsara izao;
Ny jentilisa, izay tra-bonjy,
Dia faly hankalaza Anao.}

{FIV.
Ka saotra, dera, laza, hery,
Voninahitra eran-tany
Atolotray Anao Irery,
Fa mendrika ho Anao izany.}

{2.
Tsaroanay ireo anjara
Izay natolotrao anay;
Ny fitondrana tsara fara,
Dia azo sady sitrakay
[FIV.]}

{3.
Tsaroanay tokoa anio
Ireo mpiasa namanay;
Ry Tompo tsara ô ! tahio
Izay nafafy taminay.
[FIV.]}

{4.
Rehefa tonga ny fotoana
''Zay handaozanay ety,
Tahio re hotafahaona
Eo anatrehanao, ary
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50881, '88. Ny Baiboly, boky tsara sady soa', NULL, NULL, '
{1.
Ny Baiboly, boky tsara sady soa
No manazava sy mampianatra
''Tony tena mba ho masina tokoa,
Handova ny fiainan-danitra.}

{FIV.
Ka mandrosoa sy mazotoa izao,
Mba hianatra ny Baiboly soa :
Io no harena tsy mety mba ho lo,
Fa ho lovanao mandrakizay}

{2.
Ny Baiboly, tena boky masina,
No sady feno hevi-dàlina;
Tsy mba mety ritra na ho levona :
Miboika, toy ny rano velona.
[FIV.]}

{3.
Ny Baiboly, boky tsy mba mankaleo,
Ka raiso tsy hisaraka aminao,
Fa manoro sy mibitsika ao am-po,
''Zay miendrika ho teotranao tokoa.
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50891, '89. Genesisy, Eksodosy', NULL, NULL, '
{1.
Genesisy, Eksodosy,
Levitikosy sy Nomery,
Deoteronomia ary Josoa,
Mpitsara, Rota,
Samoela Isa, Roa :}

{FIV.
Vakio ny Baiboly,
Vakio izy izao;
Ho hitanao ao
Ny soa ho anao
Anio no fotoana :
Dieny izao mba vakio
Ilay Teny velona,
Ka ny tahotrao, ario.}

{2.
Mpanjaka Isa, Roa,
Tantara Roa,
Ao koa Ezra sy Nehemia
Estera, Joba, Salamo,
Ohabolana, Mpitoriteny,
Tonon-kiran''i Solomona,
[FIV.]}

{3.
Isaia, Jeremia,
Fitomaniana,
Ezekiela, Daniela,
Hosea, Joela, Amosa,
Obadia, Jona, Mika,
Nahoma, Habakoka,
Zefania, Hagay,
Zakaria, Malakia
[FIV.]}
', '#3-V', '#3') /end

insert into song values (50901, '90. Ranomaso fibebahana', NULL, NULL, '
{- "Ranomaso fibebahana
O! nenina, indrisy!
Maro no efa nahita soa,
Fa izaho irery niharan''ny
Mafy tsy eran''ny aina
Mafy re no nahazo ahy" }

{- "Ka inona re no hataoko?
Hijanona maty mosary,
Hipetraka iharan''ny mafy:
Veloma fa hiainga aho,
Hiainga aho
Hiainga
Hiainga aho ka hody,
Hody any amin''ny Raiko aho,
Ka hilaza teny hoe :
Ry Raiko ô ! Izaho nanota
Tamin''ny lanira sy taminao,
Ka tsy mendrika ho zanakao
Intsony aho" -
Ataovy toy ny olona
''Zay nokaramainao aho,
ry Raiko ô!}

{Andeha mba hifaly isika ry zareo,
Fa tonga soa aman-tsara ny lavitra.}

{Ento! ento ny lamba soa
Ento! ento ny peratra,
Efa very izy ity ka hita indray;
Efa maty ka velona indray
Andeha mba hifaly isika ry zareo,
Fa tonga soa aman-tsara ny lavitra
Ento! ento ny lamba soa
Ento! ento ny peratra,
Efa very izy ity ka hita indray;
Efa maty ka velona indray.}
', '#3-VI', '#3') /end

insert into song values (50911, '91. Ny fitondran''Andriamanitra', NULL, NULL, '
{- "Ny fitondran''Andriamanitra
Tsy mba fantatsika loatra,
Fa ao an-koatry ny rahona,
Ka mihatra vao mampiaiky" -}

{- "Indraindray re!
Indraindray toa misento
Ka mijaly lalandava;
Raha mba mifaly aza
Dia diasam-pahoriana
Tsy mba misy izay maharitra
Itony fiainantsika eto :
Mora miova ka tsy azo ianteherana" -}

{- "Tsy mahagaga anefa izany,
Fa tany ity fonenantsika,
Ka raha mbola monina eto,
Izany no anjarantsika" -}

{- "Ry antitra sy tanora ô!
Aza be fangatak''andro,
Tsy mba fantatrao akory anie
Izay andro hodianao;
Tsy mba hitanao va re
Ny olo-maro toa anao,
Fa efa lasa any am-pasana,
Ka mangina izy any" -}

{- "Ka raha izany ihany,
Andeha mba hifankatia,
Velona manan-kavana,
Maty tsy mba hohadinoina" -}
', '#3-VI', '#3') /end

insert into song values (50921, '92. Fiadanana no omeko anareo', NULL, NULL, '
{- "Fiadanana no omeko anareo.
Fiadanana
No omeko anareo
Fiadanana no omeko anareo.
Fiadanana
No omeko anareo" -}

{- "Fa Kristy Tompo
Zava-mahagaga :
Ny maizina manjary mazava;
Ka raha alina tonga andro,
Inona no antony" -}

{- "Saino tsara fa Andriamanitra
Tsy manaonao zavatra foana :
Izany no antony!
Efa maizina sy maloto
Ny fo sy ny sain''ny olona
Ka tsy maintsy diovina
Ho mazava sy madio" -}

{- "Very lavitra ka tadiavina
Amin''ny maizina, ka mba tsilovy re!
''Ndreo ny jiro fa kely loatra,
Ary ny fanala tsy ampy
Kristy no Masoandron''ny masoandro,
Tena Masoandro!
Ilay fahazavana ny alina,
Sy ny kintana mamirapiratra
No entina mitady" -}

{- "Manatona ry mpanota izao,
Sao dia lany ny andronao
Sao dia tapitra ny andro
Itadiavana anao.
Ny mangatak''andro mahavery mandrakizay.
O! Avia dieny mbola tadiavina hianao" -}

{- "Very lavitra ka tadiavina
Amin''ny maizina, ka mba tsilovy re!
''Ndreo ny jiro fa kely loatra,
Ary ny fanala tsy ampy
Kristy no Masoandron''ny masoandro,
Tena Masoandro!
Ilay fahazavana ny alina,
Sy ny kintana mamirapiratra
No entina mitady" -}
', '#3-VI', '#3') /end

insert into song values (50931, '93. Ahay taloha ijay', NULL, '(Hira Antanala)', '
{1.
Ahay taloha ijay
Ahay Antagnala,
Tsy nahay mazava,
Tsy nahay lalàna;
- "Ny jakan''ny ombiasa
Narahinay aby;
Lovia, am-bilany,
Dia noravàna aby" -}

{2.
Ahay hanane ijao,
Tsy ankadalain''ombiasa;
Ny vandevanden-dreo,
Dia fantatray mazava,
- "Jesosy avao, ro soa,
Mahafaka havoa;
Ny tena mino Azy,
Tsy lany aombe koa" -}

{3.
Ny tegna, raha marary,
Vonjeo ny Dokotera,
Fa irary Farantsa,
Hamonjy ny mahantra
- "Jesosy avao, ro soa,
Mahafaka havoa;
Ny tena mino Azy,
Tsy lany vola koa" -}

{4.
Isika moa jalahy,
Hanaraka an''i Jeso,
Hamaky ny Baiboly,
Hahita an''i Jesosy;
- "Jesosy avao, ro soa,
Mahafaka havoa,
Ny tena mino Azy,
Jesosy mete koa" -}

{5.
Nareo lehilahy,
Ry zoky aman-jandry,
Maneo ny fianara,
Fa io no ma''mpanjaka.
- "Ny tsy mimane aze
Ho mpitako bangazy;
Tsy manam-pahefana
Fa ambakae-gny nama" -}

{6.
Fanahy raha marary,
Vonjeo ny Apostoly,
Fa irak''i Jesosy,
Hamoka ny devoly.
- "Jesosy avao, ro soa,
Mahafaka havoa,
Ny tena mino Azy,
Devoly, resy koa" -}
', '#3-VI', '#3') /end

insert into song values (50941, '94. O! ry zatovo teraky  ny Nosy', NULL, NULL, '
{1.
O! ry zatovo teraky  ny Nosy,
Mba mitsangàna re ankehitrio,
Dia atolory ho an''i Jesosy,
Tena sy saina ary fo madio !}

{FIV.
He! mandalo re ny ora,
Izay honenantsika ety
Hararaoty ry tanora,
Sao tsy tratr''ilay ary!}

{2.
O! ry tanora malagasy maro,
Mba miaingà daholo re anio,
Ka dia ilaozy sady atsaharo
Ny fomba adala, fahotana ario.
[FIV.]}

{3.
Dieny tomady sady maitso volo,
Mba matanjaka, mahery fo,
Tombon-tsoa anananao daholo,
Aza atakalo zava-mety lo!
[FIV.]}

{4.
Ny zava-maminao ety an-tany :
Vola, harena sy ny laza be,
Dia tsy mitoetra na mateza izany,
Fa mihelina avokoa.
[FIV.]}

{5.
O! ry Jesosy Zoky,
Tompon''aina,
Indreto izahay manolotra Aminao :
Fo sy fanahy, tena ary saina,
Ento ho Anao Irery, anjakao!
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (50951, '95. Mba manao ahoana kosa', NULL, NULL, '
{1.
Mba manao ahoana kosa
Izato fihevitrao raha mba
Misaina ny fiainana ho avy" -}

{FIV.
- "Mba ho sondriana ve
Ka tsy mba hifindra
Samy efa lavorary tsara ve
Ny aleha, na ny helo hovantanina
Na ny Paradisa soa honenana?
Saino tsara fa aza sondriana re:
Andro iray monja no anao!
Mba manao ahoana kosa
Izato fihevitrao raha mba
Misaina ny fiainana ho avy" -}

{2.
Mba manao ahoana kosa
Fa lasa izao ny taona maro?
Fa ny andro sisa itiavo an''i Jeso" -
[FIV.]}

{3.
Mba manao ahoana kosa,
Akaiky izao ny fitsarana
Ka omeo veloma ny toetra mandalo" -
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (50961, '96. Avia ry tanora', NULL, NULL, '
{1.
Avia ry tanora,
Avia
Jeso miantso mora
Avia
Isika no antsoiny:
Avia
Fa tiany ka tsy foiny
Avia}

{2.
Henoy izao ny teniny :
Avia
Ekeo ny famonjeny,
Avia
Ny helokao itsory,
Avia
Ny fo no atolory
Avia}

{3.
Jeso no manan-kery,
Avia
Hanavotra ny very
Avia
Ny tahotrao esory :
Avia
Tsy laviny akory,
Avia}

{4.
Anio no andro mety,
Avia
Ho lasa vetivety
Avia
Jesosy mbola akaiky :
Avia
Jesosy dia manaiky :
Avia}
', '#3-VI', '#3') /end

insert into song values (50971, '97. Ry zatovo maraina', NULL, NULL, '
{1.
Ry zatovo maraina
Voninahitry ny tany,
Mba diniho ''tony aina
Sy ny andro mora lany!
Marobe ireo tanora
Sy ny namanao fahiny,
No nalentiky ny ora,
Ka tsy hita ankehitriny.}

{2.
Fa indramina ny aina,
Misy tompony ao ambony,
Raha tiany indray halaina,
Tsy fanananao intsony;
Ao ny anti-potsy volo,
Na ny zoky, na ny zandry,
Tsy mahay mifampisolo,
Raha voantso hodimandry.}

{3.
Ry Jehovah, Rain''ny aina,
Ivalozana ny lasa
Na ny diso tao an-tsaina,
Na ny teny, na ny asa;
Aminao no famelana,
Mahavonjy olom-bery.}

{4.
Tsy mba misy làlan-kafa
Ahazoako mitoloko;
Tsy mba misy mahafafa
Ny hamatroky ny foko;
Fa ny rà tsy hay esorina,
Latsaka tao Golgota,
Mba ekeo ho solo voina
''Zay manenina amin''ota.}
', '#3-VI', '#3') /end

insert into song values (50981, '98. He! tsinjoko izao, ry sakaiza', NULL, NULL, '
{1.
He! tsinjoko izao, ry sakaiza,
Voahangy madio dia madio,
Mamiratra mandrakariva,
Firava-panahy tokoa,
Mihoatra noho ny volamena,
Izay saro-bidy ety,
Harena nomen''i Jehovah,
Ho lovan''ny zanany e!}

{FIV.
Mandalo ny eto sakaiza,
Koa aza fidina ho anao,
Ireo ny mateza indrindra
Fidio re, fidio ho anao.}

{2.
Ny laza sy haingo madera
Izay momba izao tany izao,
Mihelina fa tsy mateza,
Ho lo vetivety tokoa,
Tahaka ny vovoka kely,
Izay tsofin-tadio lehibe,
Manahaka rahona mainty
Alohan''ny rivotra be.
[FIV.]}

{3.
Ho simba ny harena an-tany,
Izay momba izao nofo izao,
Hitsahatra tsy hampihaingo
Zatovo sy antitra koa;
Tsy maintsy ho tonga ny anjara
Ny fahafatesana e!
Mahery tsy azo ialana,
Fa tendrin''ny Tompo Tsitoha.
[FIV.]}

{4.
Ry be sitrapo tsy hitoetra,
Hankaiza idnray moa hianao?
Ny Tenin''ny Ray no milaza
Fitiavana, herezo, ianaro
Ny fankatoavana e!
Dieny mandroso ny saina
Henoy ny fananarana hoe :
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (50991, '99. O! ry mpani te-hovonjena', NULL, NULL, '
{- "O! ry mpani te-hovonjena
Indro ny Tompo izay tia anao,
Mbola miantso hoe :
miverena,
Sao azon-doza re hianao" -}

{- "Mbola ho sarotra ny làlana
Ka miverena izao hianao;
Mbola hisakana ny alina;
Ka tanteraho ny mety hatao" -}

{- "Mpirenireny ô! modia,
Fa ny ety be bibidia,
Ka nahoana hianao
No tsy mba mody izao" -}

{- "O! mihainoa, O! mihainoa,
Fa misy feo izay manao hoe:
Izaho Jehovah no Ray,
Ka modia" -}
', '#3-VI', '#3') /end

insert into song values (51001, '100. Ry tany mangetana ô', NULL, NULL, '
{1.
Ry tany mangetana ô
O! meteza hidiran''ny rano velona"-
Dia ny fon''ny zanak''olombelona
No tany mangetana,
"''Zay tsy mba misy rano
Miditra any, afa-tsy
Ny tenin''Andriamanitra"-}

{2.
Ry tany karankaina ô
O! meteza hidiran''ny andon''ny lanitra"-
Dia ny fon''ny zanak''olombelona
No tany karankaina
"''Zay tsy mba misy
Mahamando azy afa-tsy
Ny tenin''Andriamanitra"-}
', '#3-VI', '#3') /end

insert into song values (51011, '101. Ry mpanota mahantra', NULL, NULL, '
{1.
Ry mpanota mahantra
Fantaro izao, fa
Ho levona ity tany ity.
He! miantso Jesosy,
Ka mitady anao,
Miverena hianao, ka modia.}

{FIV.
Mitsangàna hianao, ka modia izao,
Mba horaisin''ny Tompo tia;
Tsy mba henonao ve,
feo mibitsika hoe :
Miverena ry ondry mania!}

{2.
Ry mpanota mahantra,
Mba saino ange,
Fa mandalo ity sitrapo;
He! miantso Jeso,
Ka mangoraka anao,
Miverena anio, ka modia!
[FIV.]}

{3.
Ry mpanota mahantra,
Hevero ange!
Marobe fahavalo ety;
He! miantso Jeso,
Te hiaro anao,
Sao ho lanin''ireo bibidia.
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (51021, '102. Tanora ô! mba mihevera fatratra', NULL, NULL, '
{1.
Tanora ô! mba mihevera fatratra
Ny andronao dia toy ny vonin-javatra,
Ny tanjakao dia zavona hiraraka,
Ny hazon''aina nolosoan''ny fasana,
Ka mba heveronao"-}

{- "Ao ny ravina manalokaloka;
Ao ny voniny soa mahafinaritra
Mamerovero, anefa tsy maharitra,
Ny hazon''aina nosoloan''ny fasana,
Ka mba heveronao" -}

{2.
Ny tany ve mba manam-pitsaharana?
Ny nofo ve mba manam-panorenana?
Ny saina ve mba manam-piadanana?
Ny hazon''aina nosoloan''ny fasana,
Ka mba heveronao"-}

{- "Ao ny fifaliana toa mihelina,
Ao ny fiadanana miserana
Ny fahoriana tonga ka tsy ferana,
Ny hazon''aina nosoloan''ny fasana
Ka mba heveronao" -}

{3.
Ry olona izay mbola mitanondrika,
''Ty tany ''ty tsarovy fa maningotra,
Ny fo izay miorina eto dia ho fongotra,
Ny hazon''aina nosoloan''ny fasana
Ka mba heveronao"-}

{- "He! mandalo ny eto sy mihelina,
Soa nefa tsy azo ianteherana.
He! mamy anefa aokoa tsy hatelina;
Ny hazon''aina nosoloan''ny fasana
Ka mba heveronao" -}

{4.
Edena izao dia efa voasandratra.
Endre, endre, ny endriky ny lanitra!
Maneho santatr''izay efa sambatra,
Fa feno fitoerana mahafinaritra
Ka mba heveronao"-}

{- "Ao ny Ray izay feno voninahitra,
Ao Jesosy mbamin''ny voavotra,
Fanahy, tena tokin''aina fatratra;
''Zay tonga ao dia feno voninahitra
Mandrakizay doria." -}
', '#3-VI', '#3') /end

insert into song values (51031, '103. Fotoa-manam-petra ny androko ety', NULL, NULL, '
{- "Fotoa-manam-petra ny androko ety
Ka dia matahotra anie aho,
Soa mba tsy niomana akory aho,
Ka lany fotoana sahady,
Ka tonga tsy nampoizina
Ny fahafatesana" -}

{- "Anefa ny vatsy tsy mbola voafehy,
Kristy tsy mbola sakaiza,
Ary re ny finoana
Tsy mbola ampy hahatonga;
Ny raharaha tsy efa, tsy vita,
Ka be eritreritra foana,
Anefa lavitra ny lanitra
izay alehanao." -}

{Dieny mbola tsy tonga
Ny feon''ny mpiantso,
Tano Jesosy ho vatsin''ny ainao;
Raha mitaona handrodana ny devoly,
Dia mandrosoa ary ka mamahara
Eo an-tongotr''i Jesosy Tomponao.
Raha mitaona handrobo ny sitraponao,}

{- "Dinidiniho ary ny ety
An-tany ka ataovy hoe :
Fandalovana ety, fa
Ny lanitra re no andrandrao,
Fa ny any no mamiko ô!
Feon''Andriamanitra izao reko izao,
Ka ataovy hoe :
Zarako
Zarako ihany aza fa misy izany" -}

{- "Anefa ny vatsy tsy mbola voafehy,
Kristy tsy mbola sakaiza,
Ary re ny finoana
Tsy mbola ampy hahatonga;
Ny raharaha tsy efa, tsy vita,
Ka be eritreritra foana,
Anefa lavitra ny lanitra
izay alehanao." -}
', '#3-VI', '#3') /end

insert into song values (51041, '104. Ny varavaranao voahy', NULL, NULL, '
{1.
Ny varavaranao voahy,
Ry fo tanora be laolao;
Jeso mandona, ka voahy,
Mitady honina aminao.}

{FIV.
Ry Tompo soa sy mora fo
O! mandrosoa ho aminay;
Mba manjakà irery ao:
Anao mandrakizay ''zahay!}

{2.
Hodian-tsy hita foana ve
Ny fitiavan-dehibe?
Mandona moramora re
Jeso, ka mamalia hoe :
[FIV.]}

{3.
Izay mahafinaritra,
Tsy mety mba haharitra;
Ny zanaky ny lanitra
Mandona re, te-hiditra!
[FIV.]}

{4.
Ry Tompo soa malalanay
Indreto ''zahay, anio
Te-hampandroso sy handray
Anao, Jeso, ka mba vangio!
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (51051, '105. Aoka ''zay ny ditra atsaharonao', NULL, NULL, '
{1.
Aoka ''zay ny ditra atsaharonao;
Zaranao indrindra mbola tratr''izao;
Avelao ny kiry fa manimba anao,
Fa tsy misy firy izany ainao.}

{FIV.
Mba heveronao ''zay lasa ''zay,
Soa ho neninao mandrakizay.}

{2.
Ampy izay ny lasa nanaranam-po,
Sao dia voalatsa ny laza aman-jo;
Efa simba loatra izany hajanao,
''Ndao dia tafahoatra raha mihoatr''izao.
[FIV.]}

{3.
Ny fotoana sisa, takona avokoa,
Tsy mba fantatr''isa, na firy moa,
Ka tapaho anio izay hevitrao,
Jeso no irio hanadio anao*
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (51061, '106. Iza moa no manontany', NULL, NULL, '
{- "Iza moa no manontany
Ny farany, iza moa?
Izao moa izao" -}

{- "Mihevera re, ry Zanak''olombelona
Fa efa akaiky ny farany" -}

{- "Lavorary va ny asanao
Vitanao va ny anjaranao?
No manontany hoe:
Rahoviana no ho tonga ny farany" -}

{"Tsy mba misy olon-tsy miomana
Ka miantso fitsarana,
Miomàna, miomàna"}

{- "Eny, ho avy anie Aho!
Dia hoy ny Tompo,
Ka miomàna;
Tsy mba hilaza hoe:
Anio na ampitso anie Aho,
Fa efa antomotra ny fihaviana:
Ny fihavian''ny Tompo" -}

{"Lavorary va, ...
... Miomàna, miomàna"}
', '#3-VI', '#3') /end

insert into song values (51071, '107. Fa ireo Roa ambin''ny Folo lahy ireo?', NULL, NULL, '
{- "Fa ireo Roa ambin''ny Folo lahy ireo?
Nahoana moa ireo no
Nahazo sitraka
Amin''ny Tompontsika" -}

{- "Tsy miantso ny marina ny Tompo,
Fa ny mpanota izay mibebaka.
A! eny Tompo!
Tsy miantso ny marina ny Tompo,
Fa ny mpanota izay mibebaka" -}

{- "Ny marina no tiany;
Ny miantra no iantràny:
Mba miantrà, mba hiantràna agnareo!
Olo-masina no namany,
Ny madio no itoerany,
Fa ny mazava no onenany" -}

{Tsy miantso ny marina ny Tompo,
Fa ny mpanota izay mibebaka.
A! eny Tompo!
Tsy miantso ny marina ny Tompo,
Fa ny mpanota izay mibebaka}

', '#3-VI', '#3') /end

insert into song values (51081, '108. Endre, ny hatsaràn''ny tranonao, ry Jehovah!', NULL, NULL, '
{- "Endre, ny hatsaràn''ny tranonao, ry Jehovah!
Ny rindrina dia mijoro,
Ary ny tafony dia manelatra;
Ny varavarana mivoha re,
Ka samy miantso hoe :
Mandrosoa, Mandrosoa!
Mandrosoa ry Mpanjakanay ô!
Ka midira fa ny Tomponay
Sy izahay dia efa niomana" -}

{- "O! avia hianareo niasa teto,
Fa Izaho no hanonitra
Izay lany teto" -}

{- "A! moa mba misy maniry
Te-hovonjena va isika,
A! moa mba misy maniry
Te-hovonjena va isika
Fa ny Zanaky ny Avo Indrindra re
No maniry te-hamonjy antsika" -}

{- "O! eny,
O! eny fa ny saiko sy  ny fieritreretako
Maniry indrindra mba hovonjena:
Indrisy! fa ny fahotako
No mbola misakana ahy,
Mbola misakana ahy matetika!
O! indrisy fa ny fahotako
No mbola misakana ahy,
Mbola misakana ahy matetika!
O! indrisy fa ny fahotako
No mbola misakana ahy.
Indrisy, fa ny fahotako
No mbola misakana ahy matetika
Ka mila tsy handroso re ny diako" -}

{- "O! eny, fa iriko,
Iriko indrindra ny ho Amin''Andriamanitra Avo Indrindra
Sy ho amin''ny Jesosy,
Jesosy mpanefa,
Mpanefa ny fahoriantsika
Mba handrava ny asan''i satana
Izay mandrava sy manimba
Ny tokotany hiadanantsika, Amena" -}
', '#3-VI', '#3') /end

insert into song values (51091, '109. O! Miverena, ho''aho hianareo re', NULL, NULL, '
{- "O! Miverena, ho''aho hianareo re,
Mba hanompo an''i Jehovah Andriamanitray!
O! miverena,Ô! hianao"}

{- "A! ny lanitra, no tadiavinay,
A! hianao e ry zatovo" -}

{- "O! aza mba Jesosy no atao
Tan-tsirambina indray,
ho''aho,
A! ry zatovo,
Andriamanitra re no hatony,
Fa Izy ihany, ho''aho no ho Rainareo" -}

{- "Izy handray anay raha miala ety!
Ka hisolo vava anay raha miditra
Eo am-pitsarana
A! izay ''ray ory e!
Izy handray anay raha miala ety e!
Ka hisolo va vanay raha miditra
Eo am-pitsarana" -}

{O! raha mino Azy ny fonao
Sy manaiky Azy mba ho Ray:
Izy no ho Rainareo!}

{A! O! eny, O!
O! raha mino Azy ny fonao
Sy manaiky Azy mba ho Ray:
Izy no ho Rainareo!
Raha arianao izao ditranao
Sy ny reherehanao.
A! O! eny, O!
Izy no ho Rainareo!
Raha arianao izao ditranao
Sy ny reherehanao.}

{- "O! raha tsy mitranga
Avy tamin''ny vovoka
Ny fahoriana sy izato alahelo,
A! O! eny, O!
Velona tra-pamonjena
Ary ny olona rehetra" -}


{- E! fa ny ota e, no mandreraka azy
Ny fahoriany aza dia efa mandroso
Noho ny ditrany.
O! lazaiko aminao
E! fa ny ota e, no mandreraka azy
Ny fahoriany aza dia efa mandroso
Noho ny ditrany" -}

{- "O! raha mino Azy ny fonao
Sy manaiky Azy mba ho Ray:
Izy no ho Rainareo" -
Raha arianao izao ditranao
Sy ny reherehanao.}

{A! O! eny, O!
Izy no ho Rainareo!
Raha arianao izao ditranao
Sy ny reherehanao.}
', '#3-VI', '#3') /end

insert into song values (51101, '110. Mibebaha hianareo', NULL, NULL, '
{- "Mibebaha hianareo,
Ry Isiraely ô
Miverena hianareo,
Miverena, Isiraely,
Ry Isiraely" -}

{- "Manatona Azy
Miverena hianao" -}

{- "Indro ny Mpanjakanareo,
Mpanjaka!
Indro ao Betlehema,
Indro ny Mpanjakanareo
Ao Betlehema,
Indro ao Betlehema" -}

{- "Mba heveronao ange,
Raha hianao mba te-ho soa,
Iza dia Iza kosa indray re
No mba tianao hanasoa anao?
Iza
Iza no hanasoa anao" -}

{- "Mifaliana, ry vahoaka,
Ry vahoaka ao i Ziona,
Fa indro Jehovah
Andriamanitry Isiraely" -}
', '#3-VI', '#3') /end

insert into song values (51111, '111. O! aiza ny làlana', NULL, NULL, '
{- "O! aiza ny làlana,
Ny làlan-kaleha,
O! aiza ny lalan-kaleha
''Zay tsy mba misy fahafatesana" -}

{- "O! aiza ny làlana,
Ny làlan-kaleha,
O! aiza ny lalan-kaleha
''Zay tsy mba misy fahafatesana" -}

{- "Fa ny mahery mivavaka ihany
No manana ny ody aina" -}

{Ny mahatontosa izany
No manana ny ody aina,
fa tsy hisy tsy ho faty:
Ny mahatontosa izany
No manana ny ody aina,
fa tsy hisy tsy ho faty
Na ambony na ambany,
Na ny hendry na ny adala
Na ny kely na ny lehibe
Tombon-dàlana ny lasa.}

{O! mihevera ka misaina re,}

{- "Fa tsy hisy tsy ho faty
Na ambony na ambany,
Na ny hendry na ny adala
Na ny kely na ny lehibe
Tombon-dàlana ny lasa." -}

{- "O! ry sakaiza tia fahamarinana,
Tadiavonao anie,
Tadiavonao ny marina,
Ka aza mba mijanona:
Katsaho ny fanahy,
Ka aza mba mitsahatra,
Raha tsy hitanao ny marina,
Aza mba mijanona,
Aza mba manao ratsy
Fa fandrao manenina:
Raha tsy hitanao Jesosy,
Aza mba mijanona" -}

{- "Fandrao e, maty tsy miomana,
Ka modimandry amin-nenina
Mihevera hianareo" -}

{- "Fa tsy hisy tsy ho faty
Na ambony na ambany,
Na ny hendry na ny adala
Na ny kely na ny lehibe
Tombon-dàlana ny lasa." -}
', '#3-VI', '#3') /end

insert into song values (51121, '112. O! ry Jesosy ô! tsinjovy ''zahay', NULL, NULL, '
{FIV.
- "O! ry Jesosy ô! tsinjovy ''zahay
Fa Hianao no Mpanjaka voatendry,
Ka faly izahay anio
An-danitra re!
Finaritra izay tonga any.
Ka faly izahay anio,
An-danitra re!
Finaritra izay tonga any" -}

{1.
Na dia nateraka tamin''ny nofo,
Tsy mba nanota Hianao,
Fa Hianao Irery ihany anie
No Mpanjaka voatendry
ho anay
[FIV.]}

{2.
Rehefa niala tety ambonin''ny tany
Ho any an-danitra anie Izy,
Dia nanafatra ny Apostoly:
Ny didiny ihany no tandremo.
Ry mpianatry Kristy
Samia, misy ny didin''ny andro Sabata,
Fandrao manadino ny Sabata,
Ao an-danitra re!
Finaritra izay tonga any.
Fandrao manadino ny Sabata,
Ao an-danitra re!
Finaritra izay tonga any}

{3.
Mba saintsaino sy eritrereto
Ny nijalian''i Jesosy!
Afoizo ny ratsy rehetra,
Fandrao dia misakana tsy ho any.
Mibebaka ry mpanota
Fa izay miaiky ny fahadisoany
No hahatratra ny fiainana
Ao an-danitra re!
Finaritra izay tonga any
No hahatratra ny fiainana,
Ao an-danitra re!
Finaritra izay tonga any}
', '#3-VI', '#3') /end

insert into song values (51131, '113. Ho aiza moa hianao ry mpanota', NULL, NULL, '
{- "Ho aiza moa hianao, ry mpanota
No tsy mba mihevitra
Izay hodiana" -}

{O! aza tamàna
Amin''ity tany ity re,
Fa tsy ho ela
Hilaozanao eto izao!}

{- "Jereonao ny tanàna
Izay hodianao" -}

{O! aza tamàna, sns.}

{- "Jereonao ny tanàna
Izay hodianao" -}

{- "A! tsy ho kivy izahay,
O! ry Jesosy Tomponay" -}

{Tsy ho kivy izahay e!
Ry Jesosy Tomponay
Efa mandroso ny dianay
Mba ho any Aminao!}

{A! tsy ho kivy
Tsy ho kivy, sns.}
', '#3-VI', '#3') /end

insert into song values (51141, '114. Lasan-davitra ny andro lasa', NULL, NULL, '
{- "Lasan-davitra ny andro lasa,
Ka tsy ho hitanao:
Mba vitao re ny asa
Izay tandrify anao" -}

{- "Marobe ny namanao
Niala tsy hita intsony,
Mihevera re izao!
Aza manota intsony" -}

{- "Sao tratry ny hariva,
Efa fohy sy madiva
Ny andron''ny tany,
Ka andrao dia mitomany" -}

{- "O! miverena, ry zaza adala:
Malahelo ny Ray Malala
Mpamindra fo.
O! modia" -}

{- "O! miverena
Marobe no tsy misaina
Ka mitebiteby lava,
Na ory mitaraina,
Misento lalandava,
Nefa, Izaho Jehovah Tompo,
Miantso ny mpanompo
Miverena, manenena!
Fa ho sambatra ianao." -}

{- "O! miverena, ry zaza adala:
Malahelo ny Ray malala
Mpamindra fo.
O! modia." -}
', '#3-VI', '#3') /end

insert into song values (51151, '115. Andao, andao ry namako', NULL, NULL, '
{1.
Andao, andao ry namako
Andao, andao hivavaka!
Tsy fantatrao fa mandalo tokoa
Ny androntsika, tanora, ety?
Fa voninkazo ka hihintsana,
Sy rivotra mihelina koa"}

{2.
Henoy, henoy ry havako!
Henoy, henoy ny trompetra!
Miantso mafy ny olona izao,
Izay tena revon''ny tany ety,
Ka manambara fa mbola hafoy
Izay hita maso sy tiana tokoa" -}

{3.
Jereo, jereo ry namako!
Jereo, jereo ny fasana!
Fa taola-maina no ho hitanao,
Izay olona nitovy taminao;
Ka mandiniha, ry be sitrapo,
Sao dia hanenina re hianao" -
Andao, andao ry namako
Andao, andao hivavaka!}
', '#3-VI', '#3') /end

insert into song values (51161, '116. Ny fonenana an-danitra ao', NULL, NULL, '
{1.
Ny fonenana an-danitra ao
Dia manasa  ny mponina ety;
He, mazava ny làlana mankao izao.
Mandrosoa, mandrosoa hianao.}

{FIV.
Fa ho ao dia ho ao hianao,
''Zay manaiky ny vonjy natao,
Lova soa tsy ho lo no miandry anao:
Mandrosoa, mandrosoa hianao.}

{2.
He ny zavatra tsara tokoa
Fihavanana mafy sy soa;
Fa ny mponina an-tany,
Jehovah Ray,
He, sambara mandrakizay
[FIV.]}

{3.
He! miantso antsika ho ao
Mpamonjy ''zay tonga izao,
Fa ny fahatongavany dia hianao:
Mandrosoa, mandrosoa ianao
[FIV.]}

{4.
O! mihevera ry mponina ety,
Fa miandry Jehovah ary;
Aza dia sondriana re hianao;
Miangà, miasà izao.
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (51171, '117. Hevero ry tanora', NULL, NULL, '
{1.
Hevero ry tanora
''Zao andronao izao;
Mandalo anie ny ora
Sao tsy mba fantatrao.}

{FIV.
"Sao ho neninao anie
Ao am-parany ao,
Raha tsy tandremanao
''Zao andronao izao" -}

{2.
Fotoana fototr''aina
''Zao andronao izao;
Aza atao very maina,
Lalao sy hajao.
[FIV.]}

{3.
Fotoana saro-bidy
''Zao andronao izao;
Hevero ny adidy
''Zay tsy mbola vitanao
[FIV.]}

{4.
Tanora ô! tandremo
Ny ditraditranao;
Ny ota, aneneo
''Zay efa vitanao
[FIV.]}

{5.
Raha mbola velon''aina,
Mandia ny tany koa:
Ny asa izay ilaina,
Dia ny fanaovan-tsoa
[FIV.]}
', '#3-VI', '#3') /end

insert into song values (51181, '118. Tonga Jesosy, Mpamonjy ny very', NULL, NULL, '
{- "Tonga Jesosy, Mpamonjy ny very" -}

{- "Hamonjy antsika, fa izany re
No anton''ny diany,
Mba hitady izay very" -}

{- "O! iza moa re izay very,
No tadiavin''i Jesosy Tompo" -}

{- "A! iza moa re izay very
No tadiavin''i Jeso?
A! iza moa re izay very
No tadiavin''i Jesosy?
Fa tsy hianao ihany va,
Ry mpanota" -}

{- "Tsarovinao anie izao, ry mpanota!
Fa nahoana no sondriana foana,
Hianao lahy: tsarovy ny Tompo" -}

{- "Ka mbola re mandà ihany,
Fa hihodina amin''i Jesosy va hianao?
Tsy mba manahy ihany va, hono,
Hianao re?
Tsy mba manahy va, ny hadinin''Andriamanitra, eo am-pitsarana va hianao?
Manaiky tokoa fa matahotra Anao re!
Andeha re mba hiara-mifona,
Ho ao isika, fandrao dia very eto" -}

{- "A! dia andeha isika,
Mba hiaraka amin''i Jesosy Tompo" -}

{- "O! andeha anie aho
Mba hiaraka amin''i Jesosy izao;
O! andeha fandrao diso làlana,
Fa mpivahiny: andeha anie aho
Mba hiaraka amin''i Jesosy izao:
Koa dia raiso aho,
Raiso aho, ry Tompo,
Mba ho any Aminao" -}
', '#3-VI', '#3') /end

insert into song values (51191, '119. Tsy maintsy mibebaka izay hovonjena', NULL, NULL, '
{1.
Tsy maintsy mibebaka izay hovonjena
Didiana avokoa izao firenena
Tsy misy avakavaka ny olon-drehetra
Daholo daholo,
Mifona mibebaka ka manjoretra
Ny olon-tontolo}

{2.
Tsy maintsy hateraka indray ny mpanota
Havaozina indray fa efa simba sy tonta
Ny saina maloto, ny fo ratsy koa,
Tsy maintsy hadio:
Ny zavatra ratsy soloina ny soa,
Manombok''anio}

{3.
Tsy maintsy mivavaka mandrakariva
Na andro maraina, na andro hariva
Izany no hafatr''i Jesosy Tompo,
Mivavaka hatrany;
Tsy maintsy miezaka isika mpanompo,
Fa aina izany.}

{4.
Tsy maintsy ho tia ny olon-drehetra
Tsy misy avakavaka, tsy misy fetra
Izany no mariky ny famonjena;
Fo tia, fo tia!
Ny fon''i Jesosy mibaiko ny tena,
Ka miara-dia.}
', '#3-VI', '#3') /end

insert into song values (51201, '120. Nanao akore tika zay taloha zay', NULL, '(Hira Antandroy)', '
{1.
Nanao akore tika zay taloha zay
Nibabe voigne lako nahaferenay
Nafore i kokolampy nabokoboke e!
Natae fitobohagne e" -}

{"Lehe niavy ty Jesosy Masigne
Vaho haha tika olombelogne
Vaho izay tika tsy sahiragne
Satria nahazo Logosoa" -}

{Nanao akore tika zay taloha zay
Nibabe voigne lako nahaferenay
Nafore i kokolampy nabokoboke e!
Natae fitobohagne e}

{2.
Mieretsereta mafe nareo ry longoay!
Ho avy ty Jesosy alegnalegne
Hitsara antika ologne}

{"Mitognona mafe re ry longoay
Misy lavake ty aloha oeo
Naho tsy mitao hijogn''agnate ao,
Le tsy ho afakafake" -}

{Mieretsereta mafe nareo ry longoay!
Ho avy ty Jesosy amizay andro izay
Arahe ty anjely alegnalegne
Hitsara antika ologne}
', '#3-VI', '#3') /end

insert into song values (51211, '121. Vonona aho, Tompo', NULL, NULL, '
{1.
Vonona aho, Tompo,
hiaraka Aminao,
Ka manolo-tena hofehezinao;
Tsy ny tena ihany no homeko Anao,
Fa ny fo, fanahy, raiso anjakao.}

{FIV.
Raiso aho, Tompo ô!
Vonona aho, Tompo ô
Hiaraka Aminao;
Raiso aho izao
Ka manolo-tena hofehezinao;
Tsy ny tena ihany no homeko Anao,
Fa ny fo, fanahy, raiso anjakao.}

{2.
Izao no fikasao izay irin''ny fo:
Ho mpanomponao, ry Tompo manan-jo;
Raha teo mantsy, mety mora lo,
Fa ny teninao no maro mpankato.
[FIV.]}

{3.
Raisonao ny foko, tairo sy hetseho,
Ka ny fahotako, eny,
mba koseho,
Ka ny herinao no aoka mba hiseho,
Fa maniry Anao hoentiko maneho,
[FIV.]}

{4.
Raha avy ny fotoana hialako ety,
Ho foiko mantsy ''ty tany ity;
Ho afoiko mantsy ''ty tany ity;
Ao ny Paradisa lovako ary,
''Zay hodiako mandry raha miala ety.
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51221, '122. Tompo, tariho ity zanakao', NULL, NULL, '
{1.
Tompo, tariho ity zanakao
Mba hanarahako ny antsonao;
Mafy ny ady ka aza mandao,
Sao dia ho kivy ny foko, ka lao.}

{2.
He! ny fanahiko mety anie,
Nefa ny nofo maditra endre!
Tompo, avia ka mba mamonje:
Aza mangina fa mba mandrese.}

{3.
Tapaka tsara ny hevitro izao,
Mba te-handroso tokoa aho izao,
Amin''ny asa izay tianao,
Sady hanaraka ny hevitrao.}

{4.
Hery hanohana ahy, ry Ray,
No angatahiko mandrakizay:
Izay no iriko, fa he!
mahamay,
Itony fonenana eto an-day}
', '#3-VII', '#3') /end

insert into song values (51231, '123. Ry Jesosy Tompo tia nanavotra ahy', NULL, NULL, '
{1.
Ry Jesosy Tompo tia nanavotra ahy,
Ry Mpamonjy, eto ho Anao ny foko;
Raisonao re tsy arahin''ahiahy;
Ento ho Anao ny foko!}

{2.
Ry Jesosy Tompo, Loharanon''aina,
Ry Mpamonjy, ento ho Anao ny foko;
Hatanjaho, Hianao no Mofon''aina.
Ento ho Anao ny foko!}

{3.
Ry Jesosy Tompo tia manan-kery
Ry Mpamonjy, Ento ho Anao ny foko;
Tano mafy ho eo an-tànanao Irery
Ento ho Anao ny foko!}

{4.
Ry Jesosy Tompo tia,
Sakaiza tsara,
Ry Mpamonjy, Ento ho Anao ny foko;
Tehirizo tsy ho diso ny anjara;
Ento ho Anao ny foko!}
', '#3-VII', '#3') /end

insert into song values (51241, '124. Manomboka anio re ny Tompo', NULL, NULL, '
{1.
Manomboka anio re ny Tompo
Ny tena fitiavako Anao,
Hanaraka Anao sy hanompo,
Anio sy ho mandrakizay.}

{FIV.
Tantano izahay fa tanora,
Mitady hanary ny soa,
"Ka na isan''andro, na isan''ora,
Tantano ho Anao tokoa"}

{2.
Ny lanitra ery mba tazano,
Ho lovako mandrakizay,
Tariho izahay sy tantaro,
Anio sy ho mandrakizay!
[FIV.]}

{3.
Jesosy no Ilay Kapiteny,
Indreto ny miaramilanao,
Ka aoka Hianao mba hiteny,
Anio sy ho mandrakizay!
[FIV.]}

{4.
Eto an-tany dia be fahavalo
Sy maro mpisompatra koa;
Mijoroa ho mpandresy hatrany,
Miambena fandrao dia ho voa}

{Reheto mihitsy satana
Reheto ka ataovy mafana;
"Ny Fanahy Masina kosa anie
Hanampy antsika re"}
', '#3-VII', '#3') /end

insert into song values (51251, '125. Misy hafatra nomena', NULL, NULL, '
{1.
Misy hafatra nomena,
Hafatrao Jesosy ô!
Hampandre ny firenena
Ho Filazantsaranao.
"Vonona aho hatrizao,
Hankato ny hafatrao"}

{2.
Izaho efa mahalala,
''Zato fitiavanao;
Koa tsy misalasala
Hiara-miasa Aminao.
"Vonona aho hatrizao,
Ka iraho fa Anao"}

{3.
Na ho mafy, na ho mora,
Izao anjara asa izao,
Na dia hisy mpanakora
Ahy, noho ny Aminao,
"Vonona aho hatrizao,
Mba hanao ny asanao"}

{4.
Nefa he! ny fahosako,
Ry Mpamonjy tsara ô!
Ka mombà ny fikasako,
Hahefako ho Anao
"Vonona aho hatrizao,
Ka aza mba ilaozanao"}
', '#3-VII', '#3') /end

insert into song values (51261, '126. Jesosy Tompo Masina', NULL, NULL, '
{- "Jesosy Tompo Masina,
Zanak''Andriamanitra,
Indreto izahay manatitra ny volamena
Ka raiso ry Jesosy Tompo lehibe,
Na tsy mendrika aza mba raisonao re!" -}

{- "Tena sy fanahy omena,
Raisonao ho tena harena,
Raisonao ny tena rehetra
Aza dia lavinao" -}

{- "Fantatrao ny fanahinay,
Fa tsy mendrika Anao izahay,
Tompo tena Zanaky ny Ray
Feno voninahitra madio,
Nefa manantena famindrampo,
Aminao ry Masina sy To.
Raha ny mpanota no miova,
Dia horaisina ao an-danitra." -}

{Jesosy Tompo Masina,
Zanak''Andriamanitra,
Indreto izahay manatitra ny volamena}
', '#3-VII', '#3') /end

insert into song values (51271, '127. He! vonona izahay izao', NULL, NULL, '
{1.
He! vonona izahay izao,
Ry Jeso soa malalanay,
Hiaraka hanoa Anao,
Anio sy ho mandrakizay.}

{FIV.
Ampio, ry Tompo soa
Ny miaramilanao,
Ho sahy sy handray
Ny baiko omenao;
Ho resy avokoa
Ny fahavalonao;
Hisandratra tokoa
Ny voninahitra"}

{2.
Ry Kristiana, avia re
Hiaraka hanohana,
Hiaraka handroso anio,
Ka matokia sy mifalia
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51281, '128. Indreto, Tompo, izahay tanora', NULL, NULL, '
{1.
Indreto, Tompo, izahay tanora,
Anaro sy bitsiho moramora
Hiasa sy hamboly isan''ora
Handresy ireo devoly mpanakora,
Izay nampidi-doza tao Gomora
- "Herezo, Tompo ô!
Ny miaramilanao" -}

{2.
Raha sendra ka mandia ny làlan-tery,
Hianao Jesosy Tompo no mijery
Ny olonao, na vitsy, na irery;
Tantano izahay mba tsy ho very;
Hianao no Tompon''aina manan-kery.
- "Tantano, Tompo ô!
Tohano izahay" -}

{3.
Indreto ny tanora vitsivitsy,
Hiasa sy hamoha ny jentilisa
Fa he! mivoha izao ny Paradisa,
Hidiran''ny olo-mino tapitriksa
Indrindra fa ny mino voaisa
- "Mba raiso, Tompo ô!
Ho tena zanakao" -}
', '#3-VII', '#3') /end

insert into song values (51291, '129. Indreto izahay, Jesosy', NULL, NULL, '
{1.
Indreto izahay, Jesosy,
Ho mpanara-dia Anao,
Ka hanetsika ny Nosy,
Mba hanao ny sitrakao.}

{FIV.
Hafanao izahay
Ry Mpamonjy soa,
Ho mpiasa mahafatra-po
Anao tokoa!}

{2.
Ray sy reny sy sakaiza,
Tany sy ny havanay,
Dia nafoy; fa n''aiza n''aiza
Tianao hizoranay
[FIV.]}

{3.
Hianao re no nifidy
Sy naniraka anay,
Ka aoka mba hahavita adidy
Anie ny mpiasanao!
[FIV.]}

{4.
Enga ka ho tsara fara,
Izahay ry Tompo ô!
Ka tsy hisy diso zara
Eo anatrehanao.
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51301, '130. Ory, manina, dia malahelo', NULL, NULL, '
{- "Ory, manina, dia malahelo
Ny kianjanao, ry Jehovah
Ory aho, ory aho mba raiso
Ho any Aminao any" -}

{- "Tsy tiako, tsy mba tiako ''tony tany;
Hanaraka Anao re aho" -}

{- "Ity fonenana ity re,
Fonenana fahoriana,
Ka mihebiheby, ka ahiko
Ka hitabataba mandrakariva" -}

{- "Ary iza moa ary ho''aho
No ampisaraka anay sy hianareo?
Oa, e! ny fitiavan''i Jesosy Tompo!
Ny fahoriana, sa sabatra,
Fanenjehana, dia sanatria izany" -}

{- "Oa! ny fahoriana, ao,
ao sy ny mosary,
Ataoko tsinontsinona ireny,
Fa hanaraka Anao aho, ry Jesosy" -}

{- "O! na dia tsy tianao,
Na dia izany aza, ry Jesosy,
sady efa nolavinao;
Na dia izany aza
Mamelà, Jesosy" -}

{- "Say oviana moa, eny ho''aho,
No mba hiaraka Aminao,
Oa, oa, oa, any am-paradisa" -}

{- "O! andrandrao ''tony masonareo re
Ka mba jereo: iry Jesosy" -}

{- "O! na dia tsy tianao,
Na dia izany, ry Jesosy,
Sady efa nolavinao,
Na dia izany aza,
Mamelà, Jesosy" -}
', '#3-VII', '#3') /end

insert into song values (51311, '131. Mamelà ny heloko Jehovah!', NULL, NULL, '
{- "Mamelà ny heloko Jehovah!
Nanota taminao aho,
Jehovah, Tompon''ny maro
Ry Tompo! avotr''aina, mamelà,
Ny fahotako izay nataoko taminao" -}

{- "Tompo ô! Tompo ô!
Mamelà ny fahotako,
Izay nataoko teo anatrehanao.
Mamelà ny heloko izay efa vita,
Tompo ô! Andriamanitr''Isiraely" -}

{- "Indro ny indrafonao tsy lany!
Mamelà ny fahotako
He! ny fitiavanao tsy mba miova
Mamelà ny heloko,
Ry Tompo tsy mandao" -}

{- "O! ry Mpamonjy, hazony aho
Hialoka amin''ny fitianao,
Hialoka amin''ny fitiavanao;
Olo-meloka, afaho aho,
Ho aminao mandrakizay" -}

{Tompo ô! Tompo ô!
Mamelà ny fahotako,
Izay nataoko teo anatrehanao.
Mamelà ny heloko izay efa vita,
Tompo ô! Andriamanitr''Isiraely}
', '#3-VII', '#3') /end

insert into song values (51321, '132. Anio, ry Jeso Tompo', NULL, NULL, '
{1.
Anio, ry Jeso Tompo,
Mivoady aho izao
Hanaiky sy hanompo,
Hanao ny sitrakao.}

{FIV.
Mandrakizay, mandrakizay,
Velon-ko Anao :
Izay no faniriako;
Tarihonao ny diako,
Ho antom-pifaliako,
Ho velon-ko Anao.}

{2.
Mba tano mafy aho
Hikambana Aminao,
Hanjary tena taho
Mivelona Aminao!
[FIV.]}

{3.
Ny nofo, ny filàny,
Folahy tsara izao,
Hanaiky koa hatrany,
Hanao ny sitrakao.
[FIV.]}

{4.
Anao Irery ihany,
Hatreto sy hatrao,
Dia tombon-tsoa izany
Ho velon-ko Anao
[FIV.]}

{5.
Raha avy ny hodiako,
Ho any Aminao,
Tanteraka ny diako,
Hahita tava Anao.
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51331, '133. Izaho, ry Jeso, ho miaramilanao', NULL, NULL, '
{1.
Izaho, ry Jeso, ho miaramilanao,
Ka hirotsak''eo an-tobinao;
''Zao no fikasako, sitrapo tokoa,
Miaramila tsaran''i Jeso.}

{FIV.
Miaramila tsara, sahy toa Anao,
Nofy raha hivadika Aminao.
''Zao no fikasako,
sitrapo tokoa:
Miaramila tsaran''i Jeso!}

{2.
''Zaho, ry Jeso, hanaradia Anao;
Na aiza na aiza hitarihanao,
Vonona hihafy hampanjaka Anao,
Miaramila tsaran''i Jeso!
[FIV.]}

{3.
Kapiteny tsara mba tarihonao,
Ity miaramilanao ity,
Tsy hatoky tena, fa hiankina Aminao,
Miaramila tsaran''i Jeso!
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51341, '134. O! ry Andriamanitra Raiko ô!', NULL, NULL, '
{- "O! ry Andriamanitra Raiko ô!
Ry Andriamanitro ô
Ray malalako,
Manina ny lanitra aho,
Fa ny tany dia manasatra ahy,
Fa mampahory lalandava" -}

{- "Raiko, tsara e! ny lanitra Ray malalako,
Sambatra ny monina ao Raiko!
Izaho koa mba maniry fatratra
Ho tafiditra ao" -}

{- "Raiko ô! O! ry Raiko ô!
Tsara ny eo Aminao;
Any no hitoerako, ô! ry Raiko,
Raha sendra miala ety" -}

{- "Fa ny tany mampahory;
Na miravo aza, ory koa,
Miovaova, miova vetivety" -}

{- "O! ry Raiko ao ambony,
Hianao no antenaiko;
Ny fiainako tsy azo antoka,
Fa mora levona
Ny fiainako ety.
Feno ota, mampitebiteby
Tsy azo ianteherana;
Fa izao dia tapaka ny hevitro:
Hody aho ho any Aminao,
Ry Andriamanitro" -}

{Raiko, tsara e! ny lanitra
Ray malalako
Sambatra ny monina ao Raiko;
Izaho koa mba maniry fatatra
Ho tafiditra ao.}
', '#3-VII', '#3') /end

insert into song values (51351, '135. Izaho hanaraka Anao ry Mpamonjy', NULL, NULL, '
{1.
Izaho hanaraka Anao ry Mpamonjy;
''Zaho hanaraka Anao!
Androko mandalo ny eto
''Zaho hanaraka Anao.
''Zany soa arahin-tsento,
Jeso Mpamonjy malala,
O! mba todiho aho re.}

{2.
''Zaho nihafy ny onjan''ny tany,
Fa te-hanaraka Anao,
Ka mizaka fihafiana;
''Zaho hanaraka Anao,
Te-hahita fifaliana.
Indrisy! Ny foko manahy,
O! mitahia ahy re.}

{3.
Raha maningotra ahy satana,
Dia te-hanaraka Anao;
''Zany nofo koa mitana,
''Zaho hanaraka Anao;
Ka tsy hita hiantefana,
Jeso, ny foko sintony.
Sao dia ho renok''ety.}

{4.
Tsy mba mamiratra intsony ny soa,
''Zaho hanaraka Anao;
Hazo tsara tsy mamoa,
''Zaho hanaraka Anao
Tsy mba antenaina koa,
Ao Aminao no iriko,
Soa ho mandrakizay.}

{5.
Tapitra irina ny ao am-Paradisa,
''Zaho hanaraka Anao;
Ao ny mino tapitrisa,
''Zaho hanaraka Anao
No iriko mba ho hita,
Aiza Hianao ry Mpamonjy?
O! mitariha ahy re!}

{6.
O! mamelà ity fahotako,
''Zaho hanaraka Anao
''Zaho tsy tafatoetra eto,
''Zaho hanaraka Anao;
''Zay mba hita hiantefako
''Zaho hanaraka Anao,
Honina any Aminao.}
', '#3-VII', '#3') /end

insert into song values (51361, '136. Injany misy feo miantso moramora', NULL, NULL, '
{1.
Injany misy feo miantso moramora;
He! miangavy ka henoy kely ange,
Ka moa tia Ahy ve hianao ry tanora?
Raha izany, mamalia tokoa izao.}

{FIV.
Vovon-kanaraka Anao ry Jeso Tompo;
Vonon-kanaraka Anao tokoa izahay,
Nefa tohano izao ny herinao tokoa
Hahatanteraka izao ny adidinay.}

{2.
Indreo ny kintana mazava mialoha,
Mamirapiratra mazava ho anay,
Ka aza avela izahay hisalasala,
Hanara-dia anao tokoa, ry kintana ô!
[FIV.]}

{3.
Ry Kitana ô! tariho izahay ho tonga,
Ka asehoy ny tara-pahazavana;
Tantanonao ny dianay mba ho tontosa
Eo amin''ny toerana izay ipetrahanao
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51371, '137. Mba modia ry saiko', NULL, NULL, '
{1.
Mba modia ry saiko, moa tsy fantatrao va
Fa ao an-tranon-dRaiko, fitiavam-be?
Eto dia manahy, voan''ny kotopia;
Ao mitsimbina ahy, Raiko be fitia.}

{FIV.
Hody aho, Ray malala
Ka avia tantano
Hiditra ao an-trano;
Raiso aho, na adala,
Fa nania tokoa,
Ka nandao ny soa.}

{2.
Mba modia ry saiko, moa tsy fantatrao va.
Fa ao an-tranon-dRaiko, misy hanim-be?
Mpino sesehena, sambatra tokoa,
Eto mosarena, tsy mahita soa.
[FIV.]}

{3.
Mba modia ry saiko, moa tsy fantatrao va
Fa ao an-tranon-dRaiko, fitafiam-be?
Eto fanazimba ny akanjonay;
Any tsy ho simba ny fivavakay.
[FIV.]}

{4.
Mba modia ry saiko, moa tsy fantatrao va
Fa ao an-tranon-dRaiko, fifaliam-be?
Eto dia mijaly, sento lava izao;
Any dia hifaly, hira no atao.
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51381, '138. Raiso ho Anao, ny foko, ry Jeso ô!', NULL, NULL, '
{- "Raiso ho Anao, ny foko, ry Jeso ô!
Ho Anao dia Anao tokoa
Ho Anao varomaty,
Fa tsy mba fehivavany" -}

{- "Raiso hikambana Aminao,
Ry Voaloboka marina!
Raha eo an-tànanao
No hiainan''ny foko,
Eo no hitombo sy hamoa" -}

{- "Tsy ho kivy na hamoy fo
Raha eo anilanao;
Tsy ho kivy na hamoy fo
Raha eo anilanao ny aina
Tsy ho kivy na hamoy fo
Raha eo anilanao ny aina
Raha eo anilanao ny aina" -}

{Ny hasasaranao mafy, ry Mpamonjy ô!
Dia ekeko mba ho fitadiavanao ny very}

{- "Ny rànao no androako,
Ny ainao no mamelona"}

{Velomy ny fanahinay,
Hanaovanay ny asanao,
Ho mpiasa tsy mba manan-tsiny,
Raha eo an-tanimbolinao ry Ray.
Ny hasasaranao mafy, ry Mpamonjy ô!
Dia ekeko mba ho fitadiavanao ny very.}

{- "Ny rànao no androako,
Ny ainao no mamelona"}

{Velomy ny fanahinay,
Hanaovanay ny asanao,
Ho mpiasa tsy mba manan-tsiny,
Raha eo an-tanimbolinao ry Ray.}
', '#3-VII', '#3') /end

insert into song values (51391, '139. Ry Tompo ô! ''ndreto ny zanakao', NULL, NULL, '
{1.
Ry Tompo ô! ''ndreto ny zanakao
Miverina Aminao;
Raiso, ry Tompo, ho ao Aminao,
Ho tena zanakao.}

{FIV.
''Ndreto, ry Tompo,
Ny zanakao re, miverina Aminao!
Vonon-kanompo,
Na sarotra aza, ''zay tianao hatao.}

{2.
Hatry ny ela izahay no nania,
Nanaran-tsitrapo;
Nandà Anao re, ry Ray be fitia,
Ka loza no nanjo.
[FIV.]}

{3.
Tsarovanay izao, fa diso tokoa,
Ka dia mamelà;
Ny tsara indrindra, dia an-dapanao,
Ka aza dia mandà!
[FIV.]}

{4.
Diso tokoa izahay, fa nandà,
''Zay tianao hatao,
Ka dia mifona izahay, mamelà;
Mba mamelà izao!
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51401, '140. Ao, e! miorin-dasy', NULL, NULL, '
{1.
Ao, e! miorin-dasy,
Ao, e! miorin-dasy ny aty,
Ao, e! miorin-dasy ny aty re,
Ao, e! miorin-dasy ny Aty"}

{2.
Ao, eny, efa fantatro izao!
Ao, eny fa fantatro izao!
Izao fonenana izao re,
Izao fonenana izao"}

{3.
Ao, e! lohasaha,
Lohasahan-dranomaso ny aty,
Fa tany itondram-pahoriana ny ety,
Tany itondram-pahoriana"}

{4.
Ao, e! dia homeko,
Dia homeko veloma ny ety,
Fa tany be fahoriana ny aty e!
Tany be fahoriana"}

{5.
Ao, e! manatona,
Manatona an''i Jeso Mpamonjy
Hianao ''ray mino
Homba anao anie Izy"}
', '#3-VII', '#3') /end

insert into song values (51411, '141. Ry Tompo ô! tahio', NULL, NULL, '
{1.
Ry Tompo ô! tahio
Ity mpanomponao,
Ka atokàny anio
Hanao ny asanao,
Hianao no nahitana;
Hianao no manome,
Avia mitantàna
Ry Tompo lehibe.}

{2.
Tanora maitso volo
No voatendrinao,
Izay hanolokolo
Ireto ondrinao,
Rotsahy fitahiana
Ilay voahosotrao,
Hamita am-pifaliana
Ny zava-tianao.}

{3.
Ny saina sy fanahy
Mba hatanjahonao
Ho tena lehilahy
Mahefa izay Anao,
Ka na dia sarotar aza
Ny raharahanao,
Dia aoka tsy hilaza:
Tsy ho efa lahy izao!}

{4.
Tariho làlana izy
Hanao ny sitrakao,
Fa maro ''reto ankizy
Hamboarina ho Anao.
Tanora sesehena
Sy olon-dehibe
hantsoina hanolo-tena
Hanompo Anao anie!}

{5.
Ny zavatra rehetra
Apetraka Aminao,
Satria tsy manam-petra
Ny hasoavanao;
Tolory fo mikasa
''Zahay mpanomponao
Handroso hiara-miasa
Mba hampanjakana Anao.}
', '#3-VII', '#3') /end

insert into song values (51421, '142. Ry Jeso Mpitarika hendry!', NULL, NULL, '
{1.
Ry Jeso Mpitarika hendry!
Tsinjovy ny olom-boatendry,
Tolory ''zay hevitra hendry
Handinika sy hahakendry}

{2.
Ry Tompo Mpanjaka mahery!
Vangio raha ory sy tery;
Tohano raha mahatsiaro very,
Vimbino ''ndao saraka irery}

{3.
Raha miasa dia miasa ny saina,
Kanefa ny tany no maina
Ka mila ho kivy ny aina,
Tondrahy, fandrao karankaina.}

{4.
Ny mpiasa izay voafidy
Hamoha ny fo ''zay mihidy,
Hitari-panahy saro-bidy,
Tantano hanaraka didy.}
', '#3-VII', '#3') /end

insert into song values (51431, '143. Jeso Kapiteny, mpiady manan-kery', NULL, NULL, '
{1.
Jeso Kapiteny, mpiady manan-kery,
Indreto ny miaramilanao,
Vonona hiasa ho Anao Irery,
Ka tafio ny hery avy Aminao.}

{FIV.
"Eny, mandrosoa ry miaramila,
Fa Jeso Mpamonjy, indro eo aloha
Mivonona tsara ho an''i Gasikara,
Ho modely soa, mendrika tokoa;
Miankina amintsika ny hoavy sisa,
Milofosa, miasà, tanora ô"}

{2.
Be ny ady mafy, ao koa ny devoly,
Miambena ry tanora ô!
Jeso no banjino, raiso ny Baiboly,
Dia ho foana sy ho lao ny tahotrao.
[FIV.]}

{3.
Na inona kasaina, na inona atao,
Dia ankino amim-bavaka;
Jeso no Mpamonjy,
Loharano soa,
Vonona hihaino sy hanafaka.
[FIV.]}
', '#3-VII', '#3') /end

insert into song values (51441, '144. Iza, vahiny malala', NULL, NULL, '
{1.
Iza, vahiny malala,
Lazao ny anaranareo,
Fa sitrako indrindra, malala,
Ka iza moa hianareo?
- ''Zahay ilay tena mpanota
Kanefa nandray indrafo;
Na diso ''zahay sy nanota,
Ny loza tsy mbola nanjo!}

{FIV.
Vahiny isika ety,
Fa tompon-tanàna ary
Ampoizo, sakaiza, fa misy
Fiainana mandrakizay}

{2.
Aiza, vahiny, malala
No làlan-kalehanareo,
Fa sorotra sady be ala
Ny tany hodiavinareo?
- Hamaky ny ala sy ny ony,
Ka efitra no làlanay;
Ho any Kanàna ambony,
Hiakatra ao izahay.
[FIV.]}

{3.
Iza, vahiny malala,
Hanoro ny làlanareo,
Ary iza rehefa mby any
No mba hovantaninareo?
Tarihin''i Jeso ny dianay
Ho sambatra ao izahay,
Fa ao Andriamanitra Rainay
Miandry hampiditra anay
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51451, '145. Iza no hahasaraka ahy', NULL, NULL, '
{1.
Iza no hahasaraka ahy,
Aminao, ry Jeso ô?
Tompo tia nanavotra ahy
Hianao, ry Kristy ô! ô! ô!}

{FIV.
Ka loza ve, sa mosary,
Sa fanenjehana mafy?
Ny fahafatesana aza, eny,
Eny, tsy hamoizako Anao.}

{2.
Iza no hahasaraka ahy
Amin''ny fitiavanao?
He! ny foko tsy manahy
Tafatoetra ao Aminao!
[FIV.]}

{3.
Ny finoana no ampinga,
Ny Baiboly sabatra,
Ka manoatra ny mpandresy,
Amin''ny alàlanao!
[FIV.]}

{4.
Ry Jesosy feno hery,
Tano aho ho Anao;
Ry Mpamonjy olom-bery
Vonona hanoa Anao!
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51461, '146. Na aiza na aiza, Jeso itoeranao', NULL, NULL, '
{1.
Na aiza na aiza, Jeso itoeranao,
Sambatra eo akaikinao ny mpanomponao;
Ota tsy tafiditra ao; loza tsy hanjo;
Eo no paradisa fanaperan-tsoa.}

{FIV.
Na aiza na aiza, Jeso, tany misy Anao,
Mamy lalandava ny eo akaikinao!}

{2.
Na aiza na aiza, Jeso, itarihanao,
Sahy ny mpanomponao homba Anao izao;
Na mamaky ala be, mita rano koa,
Afa-tahotra aho, miaraka Aminao.
[FIV.]}

{3.
Na aiza na aiza, Jeso, anirahanao,
Vonona aho Tompo ô! hankato Anao;
Hisarahana avokoa izao rehetra izao,
Nefa tsy ho irery izay akaikinao.
[FIV.]}

{4.
Na aiza na aiza, Jeso, eo akaikinao,
Velona aho Tompo ô! eo akaikinao;
Eny, raha ho faty koa, mba akaikinao;
Tokam-bolana aho: eo akaikinao.
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51471, '147. Tiako sy iriko lalandava', NULL, NULL, '
{1.
Tiako sy iriko lalandava
Jeso, ny miaraka Aminao
Ka ny sentom-poko dia hisava,
Eo anilanao}

{FIV.
Tano aho ho eo akaikinao;
Tano sao dia tolan''ny manjo;
Tano aho: eo akaikinao,
No mampiadam-po}

{2.
Mamiko tokoa, na dia mahatra,
Ny mandeha miaraka Aminao;
Fa harena be tsy hita fetra,
Eo anilanao.
[FIV.]}

{3.
Mamiko tokoa na dia marary,
Ny miara-monina Aminao;
Fa ho sitrana aho Zanahary,
Eo anilanao.
[FIV.]}

{4.*
Mamiko tokoa na dia ho faty,
Raha eo am-pelatànanao;
Manan''aina, velona ao anaty,
Eo anilanao.
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51481, '148. Ao an-koatr''iry rahotra ambony iry', NULL, NULL, '
{- "Ao an-koatr''iry rahotra ambony iry
Ny Tompo no mitazana
Sy mitsinjo ny fahorianay:
Tsy hamela anay ho ory, na ho reraka,
Fa mamindra fo mandrakariva re" -}

{- "He! ny soa izay nomeny re,
Mahafaly fo, ka
Mihobia
Asandrato ny feo hidera,
Ao an-koatr''iry rahona ambony iry,
Ny Tompo no mitazana ahy re" -}

{Fa nomeny hifaliantsika
Ny soa ankehitriny;
hiadananay izao,
Izay rehetra vitanao.
Haleloia, hosana
Ho an''ilay tia anay!
Hosana
Ho an''ilay tia anay!}

{Eny! eny, ny soa izay nataonao
No hiadananay
Hosana
Haleloia! hosana
Ho an''ilay tia anay!
Hosana
Ho an''ilay tia anay!}
', '#3-VIII', '#3') /end

insert into song values (51491, '149. Vavahadin''ny lanitra eto', NULL, NULL, '
{1.
Vavahadin''ny lanitra eto,
Tranon''Andriamanitra re
Mba saintsaino sy eritrereto,
Ndreo fonenana masina re!}

{FIV.
Trano fivorian''olo-maro,
Trano fanoloran-java-tsoa
Ho fitaizana ny fanahy tsy hiharo
Amin''izay tsy mety manoa}

{2.
Misy tohatra miorina eto,
Dia Jesosy Mpamonjy anao:
Mba diniho sy eritrereto,
Hanamafy ny finoanao.
[FIV.]}

{3.
Tafaorina ny tsangam-bato,
Dia Jesosy Mpamonjy anao;
Eny, Kristy no mpandantolanto,
Mampiray ny samy lavitra.
[FIV.]}

{4.
Dia misaotra Anao, ry Mpamonjy,
Fa mitsinjo ny mpanomponao
Ka ny teninao tsy mba miova,
Mahatoky anay zanakao
[FIV.]}

{5.
Hamasino ny mpivory eto,
Ry Fanahy mpitarika ô!
Ny Fanilonao re, areheto
Hanazava ny finoanay!
[FIV.]}

{6.
Indro! sorona atao manontolo,
No atolotro Anao, Tompo ô!
Tena, saina, fanahy, manontolo,
O! mba raiso fa te-ho olonao.
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51501, '150. Andriamanitra e! no aronay', NULL, NULL, '
{- "Andriamanitra e! no aronay,
O, amin''ny andron''ny fahoriana;
Eto agnay no miantso" -}

{- "Fa mafy ka sarotra ny manjo aty,
O, amin''ny làlan-kaleha:
Aizina sy alim-pito no manodidina" -}

{- "Fa ny làlan-tsy fantatra
No alehanay re;
Ala-mazina re no vakinay,
Ary ny ony aza dia notetezinay,
Izay tsilo aza tsy nahasakana" -}

{- "Fa mamy ny aina ho''aho, ry Jehovah,
O, amin''ny làlan-kaleha,
Aizina sy alim-pito no manodidina." -}

{- "Fa izao e! no mba iriko
Eo imasonao anie izao ry Jehovah,
Hainao e, ny hampitombo
Ny taona sy ny andro, ka ampitomboinao,
Mba hanaovako ny sitraponao ety an-tany.
O, ampitomboinao anie izao, ry Jehovah" -}

{- "Andro lava no tadiavinay
Taona maro no mba iriko
Famonjena ary sy fifaliana
Entinay hanompo Anao
O, ampionao anie izao ry Mpamonjy" -}
', '#3-VIII', '#3') /end

insert into song values (51511, '151. Iza no tsy mahalala izao fitiavana izao?', NULL, NULL, '
{1.
Iza no tsy mahalala,
Izao fitiavana izao?
Iza no hisalasala
Ka hivadika Aminao?
"Inona no hampiala
Aminao, ry Ray malala"}

{2.
Moa ny fahoriana an-tany
Sy ny fihafiana ve,
No andaozanao izany
Fitiavan-dehibe?
"Inona no hampiala
Aminao ry Ray malala"}

{3.
Voninahitra aman-daza,
Hery fahefana koa,
Na fahafatesana aza,
Tsinontsinona tokoa!
"Moa ireo va no hampiala
Aminao ry Ray malala"}

{4.
Izao rehetra izao tokoa,
Izao fiainana izao,
Sy ny ao an-koatra koa,
Tsy hamoizako Anao.
"Ireo dia tsy mba mahay mampiala
Aminao ry Ray malala"}
', '#3-VIII', '#3') /end

insert into song values (51521, '152. Mangataha izao', NULL, NULL, '
{1.
Mangataha izao
Mangataha izao
Fa samy hody
Ao an-danitra!}

{2.
Sao sodoka ety
Fandalovana izao,
Fa samy hody
Ao an-danitra!}

{3.
Jeso Tompo ery
Miantso antsika izao,
Fa samy hody
Ao an-danitra!}

{4.
Ry mpanota mania
Antsoin''i Jeso hianao
Fa samy hody
Ao an-danitra!}

{5.
Jeso be indrafo
Miantso antsika izao,
Fa samy hody
Ao an-danitra!}

{6.
Falifaly izy ireo
Fa voantso izao,
Fa samy hody
Ao an-danitra!}
', '#3-VIII', '#3') /end

insert into song values (51531, '153. Hankaiza izahay Jesosy ô', NULL, NULL, '
{1.
Hankaiza izahay, Jesosy ô?
- Aminao, Aminao, ao Aminao.
Tsy misy zava-tsoa, Jesosy ô,
Ho anay, afa-tsy ao Aminao}

{FIV.
Aminao, Jesosy,
Aina ho anay;
Aminao, Jesosy
''Zay no mahatsara anay!}

{2.
Raha mbola eto koa, Jesosy ô,
Toky sy aronay, ao Aminao!
Tsy misy vonjy koa, Jesosy ô,
Ho anay, afa-tsy ao Aminao!
[FIV.]}

{3.
Raha hody izahay, Jesosy ô,
Sambatra, tretrika ao Aminao;
Hohitanay ny Ray, Jesosy ô,
Ampy izay, sa tokoa ao Aminao!
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51541, '154. Ny Sakaiza Izay tsara indrindra', NULL, NULL, '
{1.
Ny Sakaiza Izay tsara indrindra,
Dia tsy Iza, fa Jesosinay
Raha miova sy mandao
Izao rehetra àry izao,
O! Jesosy no tsy mba miova.}

{FIV.
Jeso no Sakaiza tia,
Sakaiza tsy mba miova Izy;
Raha misy ny manjo,
Izy no antsoy ange!
O! Sakaiza mamonjy Jeso!}

{2.
Raha misy manaitra  ny saina,
Ka sahiran-kevitra tokoa,
Jeso, Ilay Sakaiza soa,
Mampilamin-tsaina koa.
O! Jesosy no tsara indrindra!
[FIV.]}

{3.
Izay rehetra mahita Azy,
Dia mahita tena fiainana;
Mahasambatra tokoa,
Jeso Ilay Sakaiza soa,
O! Jesosy mamy indrindra!
[FIV.]}

{4.
Raha mamaky ilay lohasaha
Aloky ny fahafatesana,
Dia hatoky izany fo,
Tsy hatahotra tokoa.
O! mitantana anay, Jesosy!}

{Jeso no Sakaiza tia,
Sakaiza tsy mba miova Izy;
Raha misy ny manjo,
Izy no antsoy ange!
O! Mitantàna anay, Jesosy}
', '#3-VIII', '#3') /end

insert into song values (51551, '155. Tompo ô! ankehitriny ny mpanomponao dia alefanao amin''ny fiadanana', NULL, NULL, '
{- "Tompo ô! ankehitriny ny mpanomponao dia alefanao amin''ny fiadanana;
Alefanao amin''ny fiadanana, araka ny teninao.
Tompo ô! ankehitriny ny mpanomponao dia alefanao amin''ny fiadanana,
Araka ny teninao." -}

{- "Fa ny masoko efa nahita
Efa nahita ny famonjena
Izay namboarinao
Teo anatrehan''ny firenena rehetra" -}

{- "Dia fahazavana
Hahazava ny firenena
Sady voninahitry ny Isiraely olonao" -}

{- "Fa ny masoko efa nahita
Efa nahita ny famonjena
Izay namboarinao
Teo anatrehan''ny firenena rehetra" -}

{- "Dia fahazavana
Hahazavana ny firenena
Sady voninahitry ny Isiraely olonao." -}
', '#3-VIII', '#3') /end

insert into song values (51561, '156. Mahatsiarova ny andro sabata', NULL, NULL, '
{1.
Mahatsiarova ny andro sabata,
Ry mino ô!
Mba hanamasina Azy,
Izay Tomponao!}

{FIV.
"Fitoerana masina ity,
Vavahadin''ny lanitra eto.
Fitoerana masina ity,
Vavahadin''ny lanitra eto."}

{2.
Tandremo tsara ny teny lazainy,
fa rakitra soa;
Tenin''ny Filazantsara,
Be zava-tsoa.
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51571, '157. Ny ain''ny olombelona', NULL, NULL, '
{- "Ny ain''ny olombelona,
Aloka; aloka aman-javona,
Mihelina ka lasa.
Tsy mba hita izay alehany" -}

{Mananjara izay mino :
Afak''ady, afa-doza,
Jehovah no anjarany,
Fiainana no ananany!
Mananjara izay mino,
Afak''ady, afa-doza:
Jehovah no anjarany.}

{- "Tsy ho noana intsony,
Fa mofon''aina no haniny
Tsy hahita alahelo:
Hafaliana no lovany" -}

{- "Ny mazava no honenany;
Ny madio no hitoerany;
Olo-masina no namany:
Tompon''aina no havany" -}
', '#3-VIII', '#3') /end

insert into song values (51581, '158. Tsy misy tantara izay mamin''ny foko', NULL, NULL, '
{1.
Tsy misy tantara izay mamin''ny foko
Toy ny tantaran''i Jeso:
Tantara mamabo ny foko tokoa}

{FIV.
Tantarao amiko indray
Mba tantarao,
Tantarao indray;
Tantarao indray.
Iny Jesoa tsy foiko,
Tantarao indray;
Mba tantarao.}

{2.
Tsy misy tantara izay mamin''ny foko,
Toy ny tantaran''i Jeso:
Tantara tsy lehy, tsy maintsy ho to.
[FIV.]}

{3.
Tsy misy tantara ilaiko ety,
Toy ny tantaran''i Jesoa:
Tantara ilaiko, raha hody ary
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51591, '159. Ny voady ''zay nataoko aminao', NULL, NULL, '
{1.
Ny voady ''zay nataoko aminao
Ekeo, Jeso
Ny foko rehetra omeko Anao,
Ekeo, Jeso!}

{FIV.
Izay rehetra tazako izao,
Manakona ahy tsy hahita Anao;
Kanefa izay matoky Anao,
Handresy izao, sambatra tokoa.}

{2.
Mbola tanora, mora mania, Jereo Jeso!
Tsy manan-kery hiadiana ety,
Jereo, Jeso!
[FIV.]}

{3.
Omeo ahy Fanahinao,
Omeo, Jeso!
Ka mba tafio ny herinao,
Omeo, Jeso!
[FIV.]}
', '#3-VIII', '#3') /end

insert into song values (51601, '160. Hianao tokoa Andriamanitra ô', NULL, NULL, '
{1.
Hianao tokoa
Andriamanitra ô
Fitiavana
No nanaovanao
Izao tontolo izao:
Fitiavana tokoa!}

{2.
Ny fanahinao
Ivelomanay
Fitiavana;
Ary koa Jeso,
Ilay Mpamonjy anay
Fitiavana tokoa!}

{3.
Izahay, Jeso,
Ampianaronao
Fitiavana;
Fa ny mpianatrao
Dia marihinao
Fitiavana tokoa!}
', '#3-IX', '#3') /end

insert into song values (51611, '161. Anaro re ny zaza', NULL, NULL, '
{1.
Anaro re ny zaza,
Toroy raha mbola kely,
Tsy ho mpilazalaza,
Tsy ho mpiteniteny
"Kapohy mafy re;
Aza hantaina re;
Ny zaza diso hanta,
Tonga mpisatasata"}

{2.
Ny reny tia zaza,
Ny rainy manan-kaja,
Mahay mitondra zaza,
Sady mikajakaja.
"Nasiany mafy re,
Faiziny loatre e!
Fa toa zaza tsy tiana,
Nefa anie sombin''ny aina"}

{3.
Ry zanako malala,
Toa hala, toa tsy tiana,
Anefa anie malala,
Sady vololon''ny aina!
"Sombin''ny aiko re,
Silaky ny aiko re!
Matezà anie aho malala,
Matoa dia tsy mandàla"}

{4.
Mba miovà, ry saina,
Fa mampiferin''aina,
Ny ana-dray mba ataovy,
Ny reny tsy ho kivy.
"Zaza maditra re!
Zaza mihanta re!
Mihantahanta ratsy,
Mitera-doza ratsy"}

{5.
Ry Jeso tia zaza,
Ampianaro aho,
Hahay mitondra zaza,
Izao rehetra izao.
"Aza mandao anay,
Tantano mafy ''zahay
Fandrao dia diso vala
Ry Jeso ô! malala"}
', '#3-IX', '#3') /end

insert into song values (51621, '162. Mba tano, ry Raiko', NULL, NULL, '
{1.
Mba tano, ry Raiko,
Tano mafy re, ry Raiko
Ny ondry manaraka Anao;
Mba tano sao very;
Tano mafy re, sao very,
Raha manalavitra Anao}

{FIV.
Na dia marobe
Na fahavalo aza,
"Tsy hatahotra tokoa
Fa Jesosy no Mpanazava"}

{2.
Ny làla-mateza,
Fitarihana hatraiza
Ny ondry manaraka Anao,
Ny làlana lava,
Tora-tady sy mazava
Raha manalavitra Anao.
[FIV.]}

{3.
Ny ahitra maitso,
Tsy eo an-tanimbolinao;
Ny liona mamitaka ny ondry
Nandao fikasana,
Manambaka koa ny aina,
Raha manalavitra Anao.
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51631, '163. Mamy ny firaisan-kina', NULL, NULL, '
{1.
Mamy ny firaisan-kina
Ikambanantsika izao,
Izay tsy misy fanerena,
Fa atao an-tsitrapo;
Izany no firaisan-tsara
Sy be fifaliana.
Mamy ny firaisan-kina
Ikambanantsika izao.}

{2.
Manam-petra sady fohy
Izao fiainantsika izao;
Ka samia manao ny mety
Dieny mbola velona;
Hampisaraka ihany
Ny fahafatesana
Mamy ny firaisan-kina
Ikambanantsika izao.}

{3.
Ry mpanompon''i Jesosy!
Ny taranako anie
Hihamaro sy hitombo
Fifankatiavana!
Izany no ho tsangam-bato
Tsy ho levona aminao.
Mamy ny firaisan-kina
Ikambanantsika izao.}

{4.
Fahoriana ho antsika
Raha tsy mifankatia;
Ka aoka hankato ny didy
Izay nomena ho anao:
Izany no hananantsika
Ny Fanahy Masina.
Mamy ny firaisan-kina
Ikambanantsika izao.}
', '#3-IX', '#3') /end

insert into song values (51641, '164. Ry kristiana, mba mivondrona', NULL, NULL, '
{1.
Ry kristiana, mba mivondrona,
Mifankatiava tsara isika, ka miraisa;
Aoka mba hiray, ka tsy hirondrona
Hanara-kefa, sao diso rasa}

{"Miraisa hina to''ireo tantely,
Miara-miasa sady mifanampy;
Mamela lova soa sy
Mamy ho an''ny dimby;
Tsara miafara, tsara solofo,
Ny firenena hifanasoa"}

{2.
Ireo zoky efa mandroso,
Mitantana ny zandry izay tsy afaka mamindra;
Tsara miafara, tsara solofo,
Ny firenena hifanasoa}

{"Ny Betsileo sy ny Hova miara-miasa,
Hanandratra ny Bara sy Tanala,
Ny Sakalava, Vezo,
Bezanozano, Betsimisaraka
Sy ny Tsimihety, Hiara-mandroso hifanasoa"}
', '#3-IX', '#3') /end

insert into song values (51651, '165. Sambatra ny mpampihavana', NULL, NULL, '
{1.
Sambatra ny mpampihavana,
Mamin''ny tany sy ny lanitra;
Tretrika ny be fitiavana,
Izy no tena zanak''Andriamanitra.}

{FIV.
"Endriky ny tany ety,
Ravaky ny lanitra ery.
Sambatra ny mpampihavana,
Izy no tena zanak''Andriamanitra"}

{2.
Ny teny izay lazain''ny vavany.
Toa vato soa mangarangarana;
Ny olona izay lalovany,
Toa mahatsinjo lanitra miserana!.
[FIV.]}

{3.
Izy no honina ao an-danitra,
Hamantana anjely marobe;
Ho toy ny ditin-kazo manitra,
Ankasitrahan''ny Mpanjaka Lehibe.
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51661, '166. Fony ny Tompo nandia ny tany', NULL, NULL, '
{1.
Fony ny Tompo nandia ny tany,
Ory mihitsy ka vitsy mpandray,
Dia ny vavaka, hery tsy lany,
No nikambanany tamin''ny Ray}

{2.
Vavaka koa no tohan''ny aiko,
Ahazakako ny zavatr''ety;
Vavaka ihany no herin''ny saiko,
Ahitako ilay herin''ny fo.}

{3.
Vavaka ihany no tenin''ny Tompo
Mba handreseny ny ratsy taty,
Ka nanasoavany antsika mpanompo
Tamin''ity tanin''ota ity.}

{4.
Vavaka koa no ento ry mino,
Ho fandresena sy fanaovan-tsoa.
Eny, ny Tompo no tena banjino,
Ka hianao hahita tokoa!}

{5.
Nony ho faty ny Tompo Mpamonjy,
Vavaka indray no niantsony ny Ray.
Ka nanisian-kase koa ny vonjy,
Hanana lanitra mandrakizay.}

{6.
Ray ô! raha lany ny androko eto;
Raha ilaozako izao tany izao;
Raha ataoko veloma ny eto,
Vavaka re no hodiana Aminao.}
', '#3-IX', '#3') /end

insert into song values (51671, '167. Ry tanora maitso volo', NULL, NULL, '
{- "Ry tanora maitso volo
Maitso volo ary saina sady kinga
Aza hadinoina Ilay Mpisolo anao.
Ilay Mpisolo Izay nanolotra ampinga:
Miomàna mba hiady,
Mba hiady amin''izao tontolo izao." -}

{"Fa ny bika soanao
Sy ny hadiovana
No ataony tady lava
Hamandrihany anao"}

{- "Aza ataonao kilalao
''Zato ho anjaranao;
Aza atao kivazivazy,
Fa Jesosy monina ao" -}

{- "Ka ny fo sy tenanao,
Mba tandremo ho madio
Ary ireo zava-pady
No manimba ka fadio" -}
', '#3-IX', '#3') /end

insert into song values (51681, '168. Fitiavana re no kendrena', NULL, NULL, '
{1.
Fitiavana re no kendrena
''Zay no voalazan''ny Tompo,
Mifankatiava isika!
Diniho ange ny Tompo:
Nietry tena Izy,
Nidina aty
Hamonjy ny mpanota}

{Fitiavana no kendrena}

{FIV.
Ny fialonana no atsaharo;
Tiavo ny namana toy ny tena;
Fitiavana ihany no didy malaza:
Mifankatiava ry havan-drehetra}

{2.
Fitiavana re no atao
''Zay no fototry ny aina,
Mitondra ny zava-madio;
Na maro aza ny didy
Anatin''ny Boky, mandidy,
Ny fitiavana ihany
No voalohan''ireo.}

{Fitiavana re no kendrena
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51691, '169. Soa ho antsika re', NULL, NULL, '
{1.
Soa ho antsika re
Ny ho be fitia,
Mba ho miran''endrika
Raha manandra-peo,
Misy fiadanana
Tafatoetra ao aminao;
Faly isan''andro koa
Raha mifankatia.}

{FIV.
Mamy re ny fitiavana,
Endriky ny tenanao:
Lamba soa tokoa.}

{2.
Na dia ory aza ety,
He! jereo ange
Itony fialonana,
Resin''ny fitia;
Fa izao rehetra izao
Tonga tanimboly soa,
Maitso mavana tokoa,
Raha mifankatia
[FIV.]}

{3.
Indro koa ny lanitra,
Ao ambony ao,
Mirana mahita anao,
Raha be fitia!
Aoka mba hifankatia
Dieny mbola velona,
Fa rehefa lasa anie
Hanenenanao!
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51701, '170. Ny fiainana eto manasatra', NULL, NULL, '
{1.
Ny fiainana eto manasatra,
Fa ny tany be ota, be loza,
Ka be fahoriana mavesatra
Sy feno areti-mandoza.
Isaorako Andriamanitra,
Fa efa namonjy ny aiko;
Narary kanefa finaritra,
Jehovah, Mpitsabo, no Raiko}

{FIV.
Venteso ny hira fisaroana;
Derao ilay nitàna ny aina!
Hajao, asandrato ny Anarany:
Misaora ry fo sy ry saina!}

{2.
Izaho raha mila ho ketraka,
Ny teniny mandravoravo;
Nasian''ny areti-mandreraka,
Ny tànany manafosafo;
Na be fahavalo aza etikatra,
Na tazo, ny ota mamely,
Dia tsy mahagaga, fa efitra;
Eo amiko re Ilay Mahery!
[FIV.]}

{3.
Talohan''izaho tsy nivavaka,
Toa helo  ny faharariana,
Kanefa rehefa nibebaka,
finaritra teo am-pandriana;
Izao sady mbola tsy lasana
Ho any amin''ny fiainan-tsi-hita,
Na nidina ho any am-pasana,
Tsy mbola nasainy any am-pita.
[FIV.]}

{4.
Koa inona no mba havaliko
Anao ry Mpamonjy ny aiko?
Ny maty no efa nampoiziko,
Kanefa natsangan''ny Raiko.
Hanandratra ny Zanahariko
No hevitro amin''izao sisa izao,
Fa vonton''ny soa ny fanahiko,
Soa betsaka tsy hita isa
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51711, '171. Anio no itodihanay ny lasa', NULL, NULL, '
{1.
Anio no itodihanay ny lasa,
Hisaoranay ireo nanasoa anay,
Fa hitanay mazava re ny asa
Natao tety mba hamonjena anay.}

{FIV.
Isaoranay Ilay fototra iandohana:
Ny ray sy Zanaka Fanahy koa,
''Zay ta-handray ny mponina eto an-tany,
Ho sambatra ao am-paradisa soa.}

{2.
Misaotra ireo mpitory ny Baiboly,
Fa azonay ilay tena hanim-py;
Misaotra ireo mpitana ny sekoly,
Na ny efa lasa na ny mbola ety
[FIV.]}

{3.
Misaotra ireo Misiona any an-dafy,
Naniraka olon-kendry sy mahay,
Izay vonona hijaly sy hihafy,
Hanandrana ny tanindrazanay.
[FIV.]}

{4.
Misaotra ireo mpiasa miorin-dasy,
Mivoady noho ity firenena ity,
Fa Kristy ho an''ny Malagasy,
Ny Malagasy ho an''i Kristy.
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51721, '172. Ny androm-pahasoavana', NULL, NULL, '
{1.
Ny androm-pahasoavana
Maheno zava-baovao;
Mahazo fahasambarana
Izao tontolo izao.
Ravoravo ny efitra maina
Mbamin''ny tany karankaina;
He, torina ny teny soa,
ka dia afaka avokoa
Ny olom-bery maniry ho velona.}

{2.
Ka miandrandra ny lanitra
Ny malahelo izao;
Mihoby izy fa sambatra
Nahazo aina vaovao.
He, vahàna ireo migadra,
Afaka midradradradra
Mitsambikina ny mandringa:
Faingana izy sady kinga,
Ary mihoby ny lelan''ny moana.}

{3.
Ny very no tadiavina
Mba hody amin''ny Ray;
Ny maty dia velomina
Ka ravo mandrakizay
Izy dia mifady ny ota,
Noho ny lova tsy ho tonta,
Sady manaraka Azy Tompo,
Izay voaeso sy voahombo
Ambara-podiny any an-danitra.}
', '#3-IX', '#3') /end

insert into song values (51731, '173. Jeso ô! ny hamarinanao', NULL, NULL, '
{- "Jeso ô!
Jeso ô! ny hamarinanao
No lamba itafianay
Amin''ny andro farany,
Izay hifalianay" -}

{- "Raha hitsangana izahay
Handray ny lova aminao
Dia tsy ho resy lahatra
Fa Kristy no Mpanavotra" -}

{- "Io lamba io no mbola vao
Na ho levona aza izao tany izao
Haharitra tsy ho rovitra
Havaozin''Andriamanitra" -}

{- "Ny  hery sy ny antranao
Toroy izao tontolo izao,
Ampandrosoy ny teninao
Ampanekeo ny rafinao" -}

{- "Raha hitsangana izahay
Handray ny lova aminao,
Dia tsy ho resy lahatra,
Fa Kristy no Mpanavotra" -}
', '#3-IX', '#3') /end

insert into song values (51741, '174. Fanirian''ny foko loatra', NULL, NULL, '
{1.
Fanirian''ny foko loatra
Ny ho eo akaikinao
Jeso, Zoky tia malala ô!
Ao ny toerana voavoatra
Fanaperan-tsoa tokoa;
Mamiko indrindra ny ho ao.}

{FIV.
Sambatra aho manana Anao,
Ho Mpitari-dàlana ary
Tsy hivily na hania
Fa ho tody soa ny dia
Mba ho ao am-pitsaharana}

{2.
Fanirian''ny foko loatra
Ny mba ho havaozinao,
Hanan-toetra miendrika Anao.
Aza avelanao hihoatra
Na hania, handao Anao;
Tano aho ho eo akaikinao.
[FIV.]}

{3.
Fanirian''ny foko loatra
Ny hiaraka Aminao,
N''aiza no alehanao aty.
Volamene tsy manoatra
Noho ny soa aminao,
Any an-danitra, ary aminao.
[FIV.]}

{4.
Fanirian''ny foko loatra
Ny honenanao aty,
Eto an-dasim-pandalovana,
Mba hahaizako misaotra
Ny avo-tsoa natao taty,
Gologota izay tsarovana.
[FIV.]}

{5.
Fanirian''ny foko loatra
Ny hiaraka aminao
Ao an-dapam-boninahitra
Nefa aho te-hitoetra,
Miandry tendry sitrakao
Vao mandray ny voninahitra ao.
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51751, '175. Jeso, Vatolampinay', NULL, NULL, '
{1.
Jeso, Vatolampinay
O, arovy izahay,
Ka ny fonay mba sasao,
Manafaha anay izao,
"Mba tsy hisy izay hanjo
Olon-tsy mahitsy fo"}

{2.
Tsy ny asa izay atao
Mahato ny didinao;
Tsy ny hazotoam-po
No mahazo manadio;
Tsy ny ranomasonay
No mahafa-trosa anay.}

{FIV.
Jeso, Vatolampinay!
O, arovy izahay,
Ka ny fonay mba sasao,
Manafaha anay izao,
"Mba tsy hisy izay hanjo
Olon-tsy mahitsy fo"}

{3.
Tsinontsinona izahay
Sady be ny helokay;
Ry mpiaro lehibe,
Jeso Tompo! Mamonje,
Fa miankina aminao
Izahay mpanomponao.
[FIV.]}

{4.
Tomoera aminay
Raha ho faty izahay,
Ary raha hiakatra ao
Mba hiseho aminao,
Jeso, Vatolampinay!
O, arovy izahay.
[FIV.]}
', '#3-IX', '#3') /end

insert into song values (51761, '176. Eny, aiza ho aiza e!', NULL, NULL, '
{- "Eny, aiza ho aiza e!
No mba hipetrahanao Jesosy,
No mba hitsinjovan''
Ny mpanomponao"}

{- "O! avia re, O! avia re,
Mba hiara-mifaly;
Hiara-mifaly, avia!
O! avia re mba hiara-misento,
Hiara-misento amin''ny
mpanomponao"}

{- "Ananarako anao, ry zatovo,
Mandehana, dieny mazva,
Fandrao lahy tonga aminao
''Tony miaizina manko
Hianao, tovolahy"}

{- "Ataonao fa miteny foana ve
Ny Soratra Masina :
Eny, fa hifanatrika isika,
Eny, fa hifanatrika isika"}

{- "O! matahora fa aza ny maso
No apitrapitra;
Eny, ho''aho, izay mitomany
Amin''ny ranomaso;
Ranomaso tsy misy mpamoaka!
E! mitomany amin''ny ranomaso;
Ranomaso tsy misy mpamoaka"}

{- "Hariva sy maraina no ivavahako
Sy itarainako;
Ary Izy hihaino ny feoko
Amin''ny fiteniko,
Hanafaka ahy amin''ny ady:
Ady amin''izay rheetra
Manohitra ahy"}

{- "O! matahora fa aza ny maso
No apitrpitra;
Eny, ho''aho, izay mitomany
Amin''ny ranomaso;
Ranomaso tsy misy mpamoaka!
E! mitomany amin''ny ranomaso;
Ranomaso tsy misy mpamoaka"}
', '#3-X', '#3') /end

insert into song values (51771, '177. Mate raty aho, Jesosy Tompo,', NULL, '(Hira Antandroy)', '
{1.
Mate raty aho, Jesosy Tompo,
Voarendre-gny devoly ka voa;
Malahelo gny foko efa trobo
Tsy nahtia gny namako ''jay"}

{FIV.
"Engà , engàko ijao,
Lombalombao aho,
Fa engàko ijao, lombao aho Jeso,
Ifognako gny raty nataoko,
Ifognàko fa raty mare"}

{2.
Lakolosy mivango alahady,
Tsy dareko fa ataoko ho vande;
Malahelo gny foko efa trobo,
Tsy nahita gny namako''jay"
[FIV.]}

{3.
Ndre mbo jaja aho
Jesosy Tompo
Tsy nohaiko hijy toritena
Malahelo gny foko namako ''jay"
[FIV.]}

{4.
Dia itoke aho, Jesosy Tompo,
Mba rambeso ho mpanomponao
Fa Iha é nahita ahy trobo
Ailiro mba ho ao am-balanao"
[FIV.]}
', '#3-X', '#3') /end

insert into song values (51781, '178. Havaozy Tompo ô!', NULL, NULL, '
{- "Havaozy Tompo ô!
Izaho rehetra izao
Ka aoka ho hitanay
Ny voniahitrao"}

{- "Fa ny fiainanay
Izay ratsy hatrizay
Ovay hatramin''ny anio
Ho tsara sy madio
Ovay ho tsara sy madio"}

{- "Havaozy Tompo ô!
Ny Fiangonanay,
O! akambano ho iray
Havaozy, Tompo ô!
Ny Fiangonanay
O! akambano
Ho iray mandrakizay"}

{- "Fa ny fiainanay, sns
.............
Ho iray mandrakizay"}
', '#3-X', '#3') /end

insert into song values (51791, '179. Aiza no misy Anao ry Jeso', NULL, NULL, '
{1.
Aiza no misy Anao ry Jeso,
Fa ny foko mitady Anao
Toeran''aiza no mba hahitako Anao,
Ry Jeso, misy Anao, aiza moa?
Mahantra aho no mitady Anao,
Aiza Hianao, ry Jeso ô
Ny entana dia mavesatra ety,
Ry Jesoa, misy Anao, aiza moa?}

{2.
Aiza no misy Anao, ry Jeso?
Mangetaheta ny foko izao!
Toeran''aiza no mba hahitako Anao,
Ry Jeso misy Anao, aiza moa?
Mpanota aho no mitady Anao?
Aiza Hianao, ry Jeso ô!
Miasa tsy miato ka sasatra ery,
Ry Mpanafaka, aiza moa Hianao".}
', '#3-X', '#3') /end

insert into song values (51801, '180. Hoy Jehovah : ô! Avia hianao', NULL, NULL, '
{- "Hoy Jehovah : ô! Avia hianao
Fa Kristy no miantso hoe :
Manatona,
Fa Izaho no Mofon''iaina
Mahavelona"}

{- O! ry Mofon''aina ô!
Sy ry Tompon''aina :
Raha miantso aho mba valionao;
Raha mandona mba vohainao"}

{- "Dia namaly hoe, ny Tompo :
Izay mangataka no homena;}

{Izay mitady no hahita,
Izay mandona no hovohana,
Hianareo"}

{- "O! ry Mofon''aina ô!
Sy ry Tompon''aina :
Raha miantso aho mba valionao;
Raha mandona mba voahinao"}
', '#3-X', '#3') /end

insert into song values (51811, '181. Hianao, ry Jehovah', NULL, '(Hira Antandroy)', '
{1.
Hianao, ry Jehovah
''Ndriananaharinay
Raha ambinina izahay tolihonao,
Hianao, ry Jesosy Tompon-danitra,
Raha mifona Aminao ''zahay"}

{FIV.
Velona ny aiko manana Anao
Raha mifona Aminao ''zahay"}

{2.
Ndaty reko marobe mikoràto
Harotsahy ny Fanahy Masina;
Hianao, miaro ndaty marobe,
Raha mifona Aminao ''zahay"
[FIV.]}
', '#3-X', '#3') /end

insert into song values (51821, '182. Jehovah Andriamanitra', NULL, NULL, '
{- "Jehovah Andriamanitra,
Mpamaly ny natao;
Jehovah Mpamaly ny natao,
Mamirapiratà!
Misandratà ry Mpitsara ny tany ô!
Mamalia ny mpiavonavona;
Mandra-pahoviana ny ratsy fanahy
Jehovah o!
Mandra-pahoviana no mbola
Hifalifaly ny ratsy fanahy"}

{- "Mandra-pahoviana no mbola
Hifalifaly ny ratsy fanahy"}

{- "Fa ny olona, Jehovah ô
Indrisy! Torotoroiny
Eny, ny lovanao ampahoriny"}

{- "Dia ny mpitondratena
Sy ny vahiny vonoiny
Ary ny kamboty dia matiny"}

{- "Fa hoy izy  tsy mijery ahy Jehovah,
Sady marenina Andiramanitra,
Mirehareha foana ny mpanao ratsy"}

{- "Mahalalà hianareo olona ketrina,
Ary hianareo adala
Rahoviana re no mba ho hendry!
Rahoviana re no mba ho
Hendry hianareo"}

{- "Ny Mpanao ny sofina va
no tsy handre?
Ny Mpamorona ny maso
Va no tsy hahita?
Jehovah no mahalala
Ny fihevitry ny olona,
Fa zava-poana izany"}
', '#3-X', '#3') /end

insert into song values (51831, '183. Mitsimoka ao an-tanimbolinao ry Ray', NULL, NULL, '
{1.
Mitsimoka ao an-tanimbolinao ry Ray,
Ireto zanakazo mbola kely,
Ka raha sendra tanink''andro mahamay
Na rivo-doza be izay mamely,
Alofy sy tantano mafy izahay;
Ny herinay tsy ampy velively
Fa Hianao, ry Ray, no hany herinay.}

{2.
Ry Jeso, Loharano feno soa
Mangahalahala sady fototr''aina;
Mahafa-ketaheta izao tontolo izao,
Ary efa ampy izay rehetra ilaina,
Ny taninay, ry Jesoa mba onenonao
Fa raha ilaozanao dia karankaina
Tomoera Tompo ô! Monena re izao.}

{3.
Ny asanao mangina ry Fanahy
Na dia tsy hita aza , afafazo;
Ny fakany omeo tsiro avy aminao
Ny tahony tondraho tsy halazo;
Ry hery lehibe mba miasà izao,
Mba hampidokadoka ireto hazo;
Ny asa izay atao ho voninahitrao.}

{4.
Mitsimoka ao an-tanimbolinao ry Ray,
Ireto zanakazo mbola kely,
Ka raha sendra tanink''andro mahamay
Na rivo-doza be izay mamely,
Alofy sy tantano mafy izahay;
Ny herinay tsy ampy velively
Fa Hianao, ry Ray, no hany herinay.}

{5.
Ry Jeso, Loharano feno soa
Mangahalahala sady fototr''aina;
Mahafa-ketaheta izao tontolo izao,
Ary efa ampy izay rehetra ilaina,
Ny taninay, ry Jesoa mba onenonao
Fa raha ilaozanao dia karankaina
Tomoera Tompo ô! Monena re izao.}

{6.
Ny asanao mangina ry Fanahy
Na dia tsy hita aza , afafazo;
Ny fakany omeo tsiro avy aminao
Ny tahony tondraho tsy halazo;
Ry hery lehibe mba miasà izao,
Mba hampidokadoka ireto hazo;
Ny asa izay atao ho voninahitrao.}
', '#3-XI', '#3') /end

insert into song values (51841, '184. Ny Fanjakanao anie', NULL, NULL, '
{1.
Ny Fanjakanao anie,
Ry Mpanjakanay lehibe,
Handresy ny fahavalonay
Hiravoanay olonao.}

{FIV.
Manjakà aminay,
Ry Mpanjakam-piadanana;
Manjakà aminay
Mampiadàna anay!}

{2.
Mba roahy ny lolom-po
Sy ny fomban''ny nofo koa;
Fitiavana aidiro ao,
Hiala satana ao am-po
[FIV.]}

{3.
Sitrano ny rofinay,
Ry Mpamonjy malala ô!
Fa tsy misy tsy ho afakao,
Ka alao ny aretinay
[FIV.]}

{4.
Raha ho faty anie izahay,
Aoka mba hahita ny tavanao
Sy ilay voninahitrao
Irinao mba ho hitanay.
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (51851, '185. Mitsinjova ny mazava!', NULL, NULL, '
{1.
Mitsinjova ny mazava!
Eny, manenika ny tany
"Olo-meloka mahantra
Tsy mahalala an''i Jeso"
"Manatera
Manatera ny Teny ho aminy"}

{2.
O! avelao mba hahalala
Eny, ny olona rehetra
"Zanakao malala
Izay mahay manavotra
"Maneleza
Maneleza ny Teny ho aminy"}

{3.
O, asehoy ny ao andrefana
Eny, sy ny ao atsinaana
"Ary koa ny ao avaratra
Ny ao atsimo, ny an-tranonao"
"Dia homaro
Dia ho maro no tonga hidera Anao"}

{4.
O! asehoy ny ao andrefana
Eny, sy ny ao atsinanaa
"Ary koa ny ao avaratra
Ny ao atsimo, ny antranonao"
"Dia ho maro
Dia ho maro no tonga hidera Anao"}
', '#3-XI', '#3') /end

insert into song values (51861, '186. Mombà anay, ry Tompo', NULL, NULL, '
{Mombà anay, ry Tompo
Am-pandehanana :
Ny kely sakeleho,
Ny  lehibe zahao,
Ka ny Anaranao
No entinay miainga!
Ry Jeso ô, ambino
Ny handrosoanay!}
', '#3-XI', '#3') /end

insert into song values (51871, '187. Mandrahona ahy ny maizina', NULL, NULL, '
{1.
Mandrahona ahy ny maizina,
Ka atahorako
Manarona ahy ny elatrao
Jesosy tsara ô
Manarona ahy ny elatrao,
Jesosy tsara ô!}

{2.
Koa hahita an''iza raha alina
Raha senda misy manjo?
Fa ny anjely manidina
Hiambina ahy tokoa
Fa ny anjely manidina
Hiambina ahy tokoa}

{3.
Andriamanitra Rainay ô!
Mba raiso izahay
Ho zanaka hotrotroinao
Am-patorianay
Ho zanaka, hotrotroinao
Am-patorianay.}

{4.
Koa raha aposakao indrya
Ny masoandronao
Fohazy tsara izahay
Hisaoranay Anao
Fohazy tsara izahay
Hisaoranay Anao}
', '#3-XI', '#3') /end

insert into song values (51881, '188. Tompo ô! Indreto ny tanoranao', NULL, NULL, '
{1.
Tompo ô! Indreto ny tanoranao,
Mba jereo
Fa malemy, osa, eny!
Fantatrao
Ka todiho
Ny adidy sarotra iantsoana,
Mila fo sy saina, hery koa
Nefa fantatrao fa kely hery
Ka jereo}

{Tompo ô, tohanonao
Fa malemy ka jereo;
Asa sarotra tokoa anie
Ka jereo sy todiho!}

{2.
Madagasikara miandry anao izao
Miandry izao
Nefa ny tanora olon-tsy mahay
Tsy mahay
Miandry Anao izahay ry Jeso Zoky
Miandry Anao ho andry sy ho toky
Tompo ô ampio ireo tanora
Fa Hianao no aminay
Tompo ô}

{3.
Fantatray tokoa ry Kapiteninay
Fa Hianao no herinay
Ny herinao tsitoha anie
Ho mandrakizay!
Lehibe tokoa anie!
Izany herinao, ry Tompo tia,
No irina hitantana ny dia;
Ny onja sy tafiotra dia hangina
Raha Hianao no aminay
Tompo ô!}
', '#3-XI', '#3') /end

insert into song values (51891, '189. Ray ô! Mamin''ny foko', NULL, NULL, '
{1.
Ray ô! Mamin''ny foko
Ny eo anilanao
Ray ô! Izany no iriko
Tano aho sao saraka Anao
"Ray ô! Ray ô!
Anilanao, Raiko ô"}

{2.
Ray ô! Vonon-kanompo
Ny eo anilanao;
Ray ô! Mitafy nofo,
Tano aho sao saraka Anao
"Ray ô! Ray ô!
Anilanao, Raiko ô!"}

{3.
Ray ô! Olo-kandalo,
Ento aho anilanao
Ray ô! Be fahavalo,
Tano aho sao saraka Anao.
"Ray ô! Ray ô!
Anilanao, Raiko ô!"}

{4.
Ray ô! Anilanao
Raiko, feno ny soa
Ray ô! Saraka Anao
''Zany no loza tokoa!
"Ray ô! Ray ô!
Anilanao, Raiko ô!"}
', '#3-XI', '#3') /end

insert into song values (51901, '190. Ry Ray be fiantra manankery', NULL, NULL, '
{1.
Ry Ray be fiantra manankery,
Vonjeo izahay mba tsy ho very;
Dia taomy ny tsy mino mba hijery
Ny fitiavanao!}

{FIV.
Miantrà, mamonje
Ry Tompo lehibe!
Ry Tompo lehibe izao
Miantrà, mamonje!
Ry Tompo lehibe.}

{2.
Ho masina anie ny fivoriana,
Ho sitraka ny faniriana,
Hirotsaka be ny fitahiana
Ho voninahitrao
[FIV.]}

{3.
Ry Kristy Jeso Fitarikandro
Midira ao am-poko isan''andro,
Ho jiro sy fanilo sy masoandro
Aty an''efitra
[FIV.]}

{4.
Ny eto tsy hampadisadisa
Fa ho tonga varavaram-paradisa
Hamory ny vahoaka tapitrisa,
Ho tonga zanalao.
[FIV.]}

{5.
Ny masom-panahin''ny mpanota,
Mandinika an''i Golgota,
Mahita ny haratsian''ny ota
Nentin''ny Tompo tao
[FIV.]}

{6.
Ry vahoaka izay tafavory eto,
Aoka re mba tsy hisy tsy hisento,
Fa raha mba mihevitra an''i Jeso
Nisolo heloka
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (51911, '191. Ny voady izay nataoko', NULL, NULL, '
{1.
Ny voady izay nataoko
Tànako tokoa,
Fa voady hanompoako
Anao, Jesosy soa.}

{FIV.
Herinao no antenaiko,
Hanohana ny fo,
Hery masina lalaiko,
Tsaroako manan-jo.}

{2.
Hodidinim-pahotana,
Mety lavo koa
Nefa ataoko mafy tàna,
''Lay sandrinao tsitoha.
[FIV.]}

{3.
Zava-dehibe kendreko,
Ny hodiana;
''Zay tsy sitrakao reseko,
Mba hifaliana.
[FIV.]}

{4.
Raha mbola velona eto,
Ka azon''ny manjo,
Nofo ihany ka misento :
Onony re ny fo!
[FIV.]}

{5.
Rehefa antsoinao ka hody
Ka hiala ety.
Mba tsenao? Jeso, ho tody
Ho Aminao ary.
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (51921, '192. Zanahary no mahela', NULL, NULL, '
{1.
Zanahary no mahela,
Izay andro iainako
Izaho mba hazoto hanao
Ny sitrpon''i Kristy"
"Andro vitsy
No ananàn''ny olona
Andro vitsy no
Ananàn''ny olona"}

{2.
Maro ny andro lasa
Fa tsy hita izay sisa,
Ka tokony hanontaniana :
Vita va ny asanao"
"Alina avy
Ka tsy misy hiasana;
Alina avy
Ka tsy misy hiasana"}

{3.
Mamindra fo, Zanahary,
Mamelà ny helokay;
Aza manisa heloka
''Ndrao very izahay".
"Ny ràn'' Kristy
No ifonànay izao;
Ny ràn''i Kristy
No ifonàny izao"}

{4.
O! ry Jehovah, ampio hery
Izay ilain''ny olonao;
Omeo azy rehetra
Hanompoany Anao"
"Ampio izy
Hankalazàny Anao
Ampio izy
Hankalazàny Anao"}
', '#3-XI', '#3') /end

insert into song values (51931, '193. Jereo ''zao andronay izao', NULL, NULL, '
{1.
Jereo ''zao andronay izao
Andriamanitra ô!
Izay niantsoanao anay
Hanao ny asanao
Izay niantsoanao anay
Hanao ny asanao.}

{2.
Fohazy, Jeso Tompo ô!
''Zahay mpanomponao,
Hitarika ny ondrinao
Ho ao am-bàlanao
Hitarika ny ondrinao,
Ho ao am-balanao.}

{3.
Ampionao, Fanahy ô!
''Zahay voafidinao.
Hanefa koa ny adidinay
Izay niantsoana anay
Hanefa koa ny adidinay
Izay niantsoana anay}

{4.
Tahio ny Fifohazanay,
Ry Telo izay Iray,
Ary koa ny Nosinay,
Mba raiso anjakao
Ary koa ny Nosinay,
Mba raiso anjakao}

{5.
Ka raha tapitra indray
Ny andro hiainanay,
Mba raiso ny fanahinay,
Ho eo an-tratranao
Mba raison y fanahinay,
Ho eo an-tratranao}
', '#3-XI', '#3') /end

insert into song values (51941, '194. Ry Jehovah Ray Tsitoha', NULL, NULL, '
{1.
Ry Jehovah Ray Tsitoha,
Mba henoy izao :
He! Nantsoinao ka nifoha
Ny mpanomponao;
''Zao no asa vao miandoha
Ka tarihonao;
Dia tariho eo aloha
ny mpanomponao
Mba tarihonao
Dia tariho eo aloha
Ny mpanomponao.}

{2.
Ry Jesosy ô!
Tantano eto izahay;
Ampiraiso, akambano,
Mba ho tena iray;
Raha mania mba sakano:
Avereno indray;
Raha reraka, fahano,
ry Jesosinay.}

{3.
Ry Fanahy ô! Midina
Aza ela re!
Ny fiàsanao mangina, zava-dehibe
Izahay izay nofidina mba hiteny hoe :
''Zao no zavatra irina :
Hamasino e!
Hamasinonao
''Zao no zavatra irina :
Hamasino e!}
', '#3-XI', '#3') /end

insert into song values (51951, '195. Jesosy ô! Avia midina', NULL, NULL, '
{1.
Jesosy ô! Avia midina
Jeso avia
Mba entonao ny fitahiana
Jeso avia, avia!}

{FIV.
Fa raha eto Jesosy tia anay,
Dia ho feno fifaliana
Ny fonay izay te ho akakinao
Jeso avia, avia!}

{2.
Jesosy ô! Avia hitahy
Jeso avia
Ho an''ny tena sy fanahy
Jeso avia, avia!
[FIV.]}

{3.
Jesosy ô! Avia atrehonao
Jeso avia
''Zao fivoriana atao eto izao
Jesoa avia, avia!
[FIV.]}

{4.
Jesosy ô! Avia hitoetra
Jeso avia
Hoy ny Fanahinay rehetra
Jesoa avia, avia!
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (51961, '196. Amboary, ry Jesosy', NULL, NULL, '
{1.
Amboary, ry Jesosy,
Ireto maintso volonao,
Mba ho ravaky ny Nosy
Sy ny voninahitrao
Voninkazo vao maniry
Eo an-tanombolinao,
Ka tahio mba ho tsara tsiry,
Hasoavy ho Anao}

{2.
Ny Sekoly Alahady,
Tena raharahanay;
Vonona izahay hiady,
Ry satana rafinay!
Baoboly, Fihirana,
Sabatra voavoatranao
Jeso Tompo kapiteny,
Manampia ho anay!}

{3.
Na aiza  na aiza, miambena,
Be ny fahavalonao;
Mionona, mivonona
Jeso Tompo no jereo.
Mivavaha isan''andro,
Mianàra teny soa
Fa tsy ho ela hahazo babo,
Tena marina sy to.}

{4.
Dinidinika azo tsara
Nefa hijanona izahay;
Fomban-teny misy fara,
Ka tsy maintsy arahinay
Mibebaha, mahareta
Diovinao ny tenanao;
Jeso Tompo tsy nanota
Tadidio ny Tomponao.}
', '#3-XI', '#3') /end

insert into song values (51971, '197. A! ry Andriamanitra', NULL, NULL, '
{- "A! ry Andriamanitra
Mpihaino vavaka,
Tsarovy ny mpanomponao!
''Zay nataon''ny fahavalonao,
A! tsy naninona"}

{- "Fihavanana, nataonao taminay,
Ady kosa no navalinay
Anao izao;
Fandeferana, nataonao taminay,
Ka novalianay ny ditra"}

{- "O! fandeferana izay nataonao taminay,
Ka novalianay ny ditra"}

{- "Ary iza kosa no avy amin''izany?
Tafondro sy ny fiadiana
Maro karazana
Ary iza kosa no avy amin''ireny?
Jesosy Kapiteny"}

{- "O! avia, avia!
Avia moa ny Manan-kery
Mba homba ny mpanomponao,
Izay ho lasa ao an-tsisin-tany :
Mba hotahinao agne any izy".}
', '#3-XI', '#3') /end

insert into song values (51981, '198. Tsy ho diso fitahiana', NULL, NULL, '
{- "Tsy ho diso fitahiana
Ny olonao, Jehovah ô!
Tsy ho diso fitahiana
Ny olonao, Jehovah,
Jehovah Tompo ô!
Fa mandray soa isan''andro;
Isan''andro, inska''alina}

{- "Ny orana be mamonto tany,
Hafanàna mapaniry zava-maniry
Hafanàna
Avy Aminao izao, ry Ray"}

{- "Endrey! Izany fitiavan''
 :Andriamanitra izany
Lehibe ka tsy azo
Oharina amin-javatra,
Fa omeny avokoa,
Izay rehetra mahasoa :
Ny Zanany nafoiny"}

{- "Mifalia, mihobia,
Miderà, ry firenena"}

{- "Azontsika anie ny
Fanjakan''ny Ray!
Haleloia, haleloia, amena"}

{- "Endrey , izany fitiavan''
Andriamanita izany :
Lehibe ka tsy azo
Oharina amin-javatra,
Fa omeny avokoa,
Izay rehetra mahasoa :
Ny Zanany nafoiny"}
', '#3-XI', '#3') /end

insert into song values (51991, '199. Mpandombako mora', NULL, NULL, '
{1.
Mpandombako mora,
Indreto izahay
Manday ny havoanay
Fa hotsekotse gny devoly
Fa sorombahonao ''zahay
Jesosy ô!
Lombaonao fa voa
Andeso an-tagna
Fa Iha avao ro
Mpandombanay mahalomba}

{FIV.
Tompo soa mba iferegneso
Izahay mivory eto izao
Omeo anay ny Fanahy
Mba handio ny fonay
Omeo anay koa ny here
Ho anay maraimaray.}

{2.
Jesosy Mpanjaka
Avia ! mba hanjaka
Atoy an-taninay atoy
Fa mboa maike izahay
Fa Iha avao ro Fanalolahinay
Handay fandrembaha
Aty aminay
Lombao fa maleme
Izahay vaveanao atoy
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (52001, '200. Izao ty bekoay Tandroy', NULL, NULL, '
{1.
Izao ty bekoay Tandroy
Mifalia  fa afa-boy
Le ty ondaty maro iaby toy
Le ty bekoay Tandroy
Le masaotse Anagnahare zahay}

{FIV.
"Mivelatagnagne ty
Zagnahare be hatea
Rombarombaho anio fa tea
Fa manigne antika ho anae
Ho raty ty tane toy
Rombarombaho ry Tandroy
Rombarombaho, ô!
Ragnandria, ragnandria"}

{2.
Ry Fanahy Masina ô!
Irak''i Jesosinay,
O! mba hamasinonao zahay
Lahilahio mandrakizay,
Fa maizina ao am-po
Mba hazavao.
[FIV.]}

{3.
Le akianahareo
Le hahao ty gadrae
Ianareo ro tokoave
Le mbe maro ty amae
Miandy Jeso Longotikagne.
[FIV.]}
', '#3-XI', '#3') /end

insert into song values (52011, '201. Raha hihaino tsara ny didiko', NULL, NULL, '
{- Raha hihaino tsara ny didiko
''Zay andidiako anareo anio
Ka ho tia an''i Jehovah
Andriamanitrareo
Sy  hanompo Azy
Amin''ny fonareo rehetra
Sy ny fanahinareo rehetra"}

{- "Ary raha hihaino tsara ny didiko
''Zay andidiako anareo anio
Ka ho tia an''i Jehovah
Andriamanitrareo,
Amin''ny fonareo rehetra
Sy ny fanahinareo rehetra"}

{- "Dia halatsako amin''ny taninareo
Ny ranonorana amin''ny fotoany,
Ny loha-orana sy ny fara-orana
Hanangonanareo ny vokatrareo"}

{- "Ary hampaniriko voly
Eo amin''ny sahanareo,
Mba hohanin''ny biby fiompy"

{- Indro hanome fitahiana
Hanome fitahiana
Izao ka ho mandrakizay".}
', '#3-XII', '#3') /end

insert into song values (52021, '202. Ry tanora izay mivory maro eto', NULL, NULL, '
{1.
Ry tanora izay mivory maro eto,
Mifalia mbola notahin''ny Tompo!
Ravoravo tsy mba misy izay misento;
Mifalia, mihobia!}

{2.
Ry tanora! misy maro izay mifofo,
Mba hanimba sy hamambo ny fanahy,
Dia satana, izao tontolo izao sy nofo
Miambena, mivavaha!}

{3.
Ry tanora! aza mety ho sodoka
Laingan-tany; mora miova re ny andro
Miomàna ka mba raisonao ny loka
Ao ambony, amin''i Jeso}
', '#3-XII', '#3') /end

insert into song values (52031, '203. Aza mba kivy raha mikasa', NULL, NULL, '
{- "Aza mba kivy raha mikasa
Hanao ny soa ankehitriny,
fa mahereza amin''ny asa
Dia misikina fa hotahina,
Na maro aza izay tsy mba tia
Izy no hery hanatontosa"}

{- "Ny andro ratsy dia mbola ho avy,
Sy itony taona izay mampahory;
Jeso Mpamonjy dia miangavy,
Fa be ny tsy vita, ka aza matory;
Ny sitraponao dia ajanony
Sy ny filàna momba  ny nofo"}

{- "Rahatrizay dia mbola hivaly,
Na soa na ratsy, izay efa vita;
Jeso Mpitsara no manambara;
Izy miandry ery am-pita
Aza fidina ny eto an-tany
Ezaho ny anjara any an-koatra.
Satriko anie ka tsy hanantsiny!
Jeso ô! Hianao no manamboatra.
Satriko anie ka tsy hanantsiny!
Jeso ô! Hianao no manamboatra"}

{"Izay manana asa, aoka hazoto
Fa tsy mba fantatrao anie
Ilay andro sisa;
Fa mora miova ka tsy mateza,
Dia mba katsaho anie Ilay Paradisa!
Dia mba katsaho ilay Paradisa".}

{- "Rahatrizay, sns,
...
... no manamboatra"}
', '#3-XII', '#3') /end

insert into song values (52041, '204. Sambatra ny fo izay matoky', NULL, NULL, '
{"Sambatra ny fo izay matoky
An''Andriamanitra,
Fa ny fiadanan-tsaina
No manapitra ny androny"}

{"Fa ny fiadanan-tsaina
Sy ny haravoam-panahy
No manapitra ny androny
Sambatra ny fo izay matoky
An''Andriamanitra,
Fa ny fiadanan-tsaina
No manapitra ny androny.}

{"Esory ny ahiahy ao anatinao,
Fa io no fefy misakana ny dianao;
Sambatra ny fo izay matoky
An''Andriamanitra,
Fa ny fiadanan-tsaina
No manapitra ny androny.}

{"Avelao ny ota fa manimba anao,
Ka ajanony hatramin''ny anio izao.
Sambatra ny fo izay matoky
An''Andriamanitra,
Fa ny fiadanan-tsaina
No manapitra ny androny.}
', '#3-XII', '#3') /end

insert into song values (52051, '205. Ranon''aina, izay tena mateza', NULL, NULL, '
{1.
Ranon''aina, izay tena mateza,
Ao an-tampon''i Kalvary
Saro-dàlana sady mideza
Nefa kosa tsy lavitra anao
[FIV.]}

{FIV.
"Ranon''aina tsara loatra
Mora azon''ny maniry azy re;
Tsy mba eto fa any an-koatra,
''Zay tafiditra ao dia tretrika tokoa"}

{2.
Loharano tsy mety ho ritra,
Fa maharitra mandrakizay;
Manadio ny loto rehetra,
Nefa kosa tsy lavitra anao
[FIV.]}

{3.
Ny fitiavan''i Jesossy Tompo
No manolotra io rano io,
Dia ho afaka izay mampisento
Hatak''andro, tapaho anio
[FIV.]}

{4.
Tonga Jesoa hamonjy ny maro;
''Zay mibebaka ô! Matokia;
Ny Fanahy nirahin''ny Tompo
Hanadio ny ota ao am-po
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52061, '206. Jeso Mpanjaka Malaza', NULL, NULL, '
{- "Jeso Mpanjaka Malaza,
No mitondra izao tontolo izao,
Koa aza matahotra hianao"
''Ray mino tsara fo"}

{- "Indro avy! Jesosy Tompo
No mitondra izao tontolo izao,
Ka miomàna hianao
''Ray mino tsara fo"}

{- "Inona no havelanao aty ''ray mino,
Rehefa lasa any hianao ''ray mino,
Ny ranomasonao efa latsaka
No milaza azy hoe :
Rehefa tonga any am-bavahady,
Ka mbola handona anie aho"}

{- "O! lavorary ny dianao
Izay mandona;
Izay mandondona, izay mangataka,
Dia mahazo, lavorary ny dianao"}

{- "O! aza mikoy e! fa tsy mba
Hikoy intsony anie aho;
Lavorary ny dianao izay mandona;
Izay mandondona, izay mangataka
Dia mahazo; lavorary ny dianao"}

{- O! any an-danitra no mba hifaliantsika!
Lavorary ny dianao izay mandona
Izay mandondona, izay mangataka
Dia mahazo; lavorary ny dianao"}
', '#3-XII', '#3') /end

insert into song values (52071, '207. Ry mpiantsambo mankany', NULL, NULL, '
{1.
Ry mpiantsambo mankany
Am-paradisa vaovao,
Aza lao fa io no tany,
''Zay misy soa tsy lany,
Hahasambatra anao"}

{2.
Matokia fa ny sambo
Dia ho tody ary,
Na dia toa tsy ho tambo
Ny  loza sy ny antambo
''Zay mamely ety"}

{3.
Ny Tsitoha ao ambony,
No hivimbina anao;
Izy no hampitony
Ny fisafoaky ny ony,
Ka minoa hatrizao"}

{4.
Raha tonga ao am-pita,
Dia ho tretrika ery,
Fa ny dianao ho vita,
Jesosy koa ho hita
Mifalia hatrizao}
', '#3-XII', '#3') /end

insert into song values (52081, '208. Moa hafoin''Andriamanitra ho very va?', NULL, NULL, '
{- "Moa hafoin''Andriamanitra
Ho very va?
-Tsia!
Tsy hafoin''Andriamanitra
Ho very hianao
Eny, O! fa nomeny ny
Zanani-Lahy Tokana"}

{Moa hafoin''Andriamanitra
Ho very va ianao?
-Tsia
Tsy hafoin''Andriamanitra
Ho very hianao
Fa nomeny ny
Zanani-Lahy Toakana}

{- "Manatona, ry mpanota
Manatona, ry mpanota
An''i Jeso, Avotrao"}

{- "Henoy
Henoy, fa Jesosy no
Nitondra ny vonjy fahiny
Hanavotra anao,
Ry mpanota"}

{Eny, fa ny ràny no nomeny
Eny, fa ny ràny no nomeny
Ho anao, ry Mpanota!
O! eny, fa ny ràny no nomeny!
Eny, fa ny ràny
Ho anao, ry mpanota.}
', '#3-XII', '#3') /end

insert into song values (52091, '209. He ! sarotra ny làlana', NULL, NULL, '
{- "He ! sarotra ny làlana
Mamaky ity fiainana ity ô!
Mandrahona , mandrivotra
Manasatra, mandreraka"}

{- "Ny aizina manodidina,
Ny aizam-pito,
Ny aizina manarona ny tany,
Ny aizim-pito, ny firenena,
Fa Aminao no iposahan''
ny mazava
Fa aminao no isehoan''ny voninahiny"}

{- "Na dia tony aza ety, sy malama
Na tsy mandrivotra aza ety;
Mamifirifiry
Ny zava-tsoa ety mety miova
''Zay rehetra mamy ety dia mandalo"}

{- "Tsy misy miadana izay monina eto,
Fa fandalovana ity tany ity;
Ka andrandrao ny lohanao,
Fa ao ny tanindrazana"
Ka asandrato ny lohanareo,
Ka mijere ao ambony ao
Ao an-danitra ambony ao hianao;
Aza kivy : ny tanindrazana!
Aza kivy hianao raha mitoetra
Ao an-dàlana}

{- "Na dia tony aza ety, sy malama
Na tsy mandrivotra aza ety;
Mamifirifiry
Ny zava-tsoa ety mety miova
''Zay rehetra mamy ety dia mandalo"}
', '#3-XII', '#3') /end

insert into song values (52101, '210. Ry zanak''Andriamanitra ô!', NULL, NULL, '
{1.
Ry zanak''Andriamanitra ô!
Aza manahy na mivadi-po;
Na dia mafy y zava-manjo,
Andriamanitra momba anao.}

{FIV.
Inona no mampanahy anao?
Andriamanitra no aminao :
Izy mahita ny zava-manjo,
K''aza mangovitra na ory fo}

{2.
Maro ny loza mamely aty
Nefa tsy hisy hanimba anao,
Na dia mahery fidona ery
F''eo an-tranon''ny Ray hianao.
[FIV.]}

{3.
Tsy Izy ihany va re no nanao
Sady Izy koa no mamonjy anao,
Koa nahoana ange hianao
No mitanondrika tahaka izao?
[FIV.]}

{4.
Tsy hotsaroako ny helokao,
Ary vonoiko ny otanao;
Izay no toky nomeny anao,
Aza miondrika : miandràndrà!
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52111, '211. Raha manjombonjombona ny andronao,', NULL, NULL, '
{1.
Raha manjombonjombona ny andronao,
Ka tsy hitanao izay tokony hatao,
Dia ny varavaranao re no vohay,
Mba hidiran''i Jesoa}

{FIV.
Ny masoandronao dia ao
Dia ny varavaranao re no vohay
Mba hidiran''i Jeso.}

{2.
Raha misy ady mafy izay hatao
Ka ny olona rehetra toa mandao,
Dia ny varavaranao re no vohay,
Mba hidiran''i Jeso
[FIV.]}

{3.
Raha misy fahoriana mahalao,
Izay manetoketoka ny fonao
Dia ny varavaranao re no vohay,
Mba hidiran''i Jeso.
[FIV.]}

{4.
Raha mihozongozona ny hevitrao
Ka tsy misy valiny ny vavakao
Dia ny varavaranao re no vohay,
Mba hidiran''i Jeso
[FIV.]}

{5.
Raha miramirana ny endrikao,
Ka tsy fantatrao izao fahoriana izao,
Dia ny tranonao no diovy sy fafao,
Mba hidiran''i Jeso
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52121, '212. Jesosy re no mandako', NULL, NULL, '
{1.
Jesosy re no mandako
Sy fiarovako;
Ny sandriny no aroko
Sy fialofako.}

{FIV.
Endre! Mba mamiko tokoa
Ny eo anilany,
Mandre ny teny mahasoa,
Izay ambarany"}

{2.
Tanora maro namako,
Andramo sy jereo,
Izato hasambarako
Anilan''i Jesoa,
[FIV.]}

{3.
Ny fahavalo mierona,
Sakano ry Jeso,
Satana izay mandrahona
Tsy maharesy Anao.
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52131, '213. Mandrosoa, ry olo-mahantra izao', NULL, NULL, '
{1.
Mandrosoa, ry olo-mahantra izao
Malemy sy kivy ery
Ny Tompo mifaly mahita anao
Mandom-baravarana ety"}

{FIV.
"Dony
Dony! Fa mety izao"
Dony! Dony mahery re
"Dony
Dony fa tiany izao
Hovohany tokoa hianao"}

{2.
Misy trano madio ao anatiny ao
Ho an''izay tahaka anao,
Tsy mba mety manary mpanota iray
Izay tonga mandona ety"
[FIV.]}

{3.
Matokia tokoa, mpivahiny izao.
Fa manna-kiahy hianao!
Mpiaro mahery, hiaro anao
Ho velona mandrakizay"
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52141, '214. Ravoravo ny efitra', NULL, '(Isaia 35)', '
{- "Ravoravo ny efitra
Sy ny tany karankaina,
Ary ny tany hay mifaly sy mamony,
Mamony be dia be;
Eny, mifaly sy mihoby izy"}

{- "Lazao amin''izay reraka am-po :
Mahereza f''aza matahotra
Fa indro : Andriamanitrareo?
Avy hitondra famonjena,
Dia ny famonjen''i Jehovah;
Eny, Izy avy hamonjy anareo".}

{- "Ampahiratina ny mason''ny jamba;
Ampahaladina ny marenina
Hitsambikina tahaka  ny diera
Ny mandringa"}

{- "Ary hihoby ny lelan''ny moana;
Hiboiboika ao an''efitra ny rano,
Ary hisy renirano ao an-tany karankaina"}

{- "Hobio, hobionareo!
Hobio
Azontsika izao ny fandresena!
Hobio
Azontsika izao ny fandresena!
Hobio
Haleloia"}
', '#3-XII', '#3') /end

insert into song values (52151, '215. Rahoviana no ho tonga ny farany', NULL, NULL, '
{- "Rahoviana no ho tonga ny farany,
Hialako amin''ity fahoriana ity?
Hafiriana no sisa hiaretako"}

{"Tsy fantatro"}

{- "O! aza matahotra
Ry mpanompon''i Jehovah
Fa ny fahoriana rehetra
Efa nariany ao Kalvary,
Ka natobiny teo
Ambadiky ny hazo fijaliana
Ka sahia miteny hoe :
To''zao! To ''zao"
Tsy eto va
O nialany aina?
Tsy eto va
No tapaka ny kofehy?
Tsy eto va
No mahafinaritra ahy?
Tsy et ova?
Tsy eto tokoa re!
Tsy eto va
No nialany aina?
Tsy eto va
No tapaka ny kofehy?
Tsy eto va
No mahafinaritra ahy?
Tsy eto va?}

{- "Veloma, veloma ry ilay fasana!
Fa Ralehilahy nomeny hisolo
Hatramin''izao ka ho mandrakizay
Tsy eto va? sns}
', '#3-XII', '#3') /end

insert into song values (52161, '216. Nandeha tamin''ny làlana', NULL, NULL, '
{1.
Nandeha tamin''ny làlana
Saro-pantarina Andriamanitra"}

{FIV.
Fa Jehovah ihany
No hanoro ny làlanao,
Handeha any aho;
Aza matahotra fa Jehovah
No anjarako.}

{2.
Mba tsofy mafy izao
Trompetra izao
Hanàko eran''ity Nosy ity"
[FIV.]}

{3.
Na hitanondrika izao tany izao
Kanjo dia hoby re no farany"
[FIV.]}

{4.
Mba sainonao ange ''ray olona
Fa fandalovana ity tany"
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52171, '217. Izay rehetra mangetaheta', NULL, NULL, '
{- "Izay rehetra mangetaheta
Manatona ny loharano hianareo"}

{- "Manatona an''i Jesoa
Ilay Mpamonjy,
Mpamonjy antsika"}

{- "Sady Izy no nanafaka ahy,
Tamin''ny fahoriana"}

{- "Ho amin''ny andro,
Andro iainako,
Andro iainako rehetra"}

{"Sady Izy no nanafaka ahy
Tamin''ny fahoriana".}
', '#3-XII', '#3') /end

insert into song values (52181, '218. Mifohaza', NULL, '(Isaia 52)', '
{- "Mifohaza
Tafio ny herinao ry Ziona ô!
Tafio ny fitafiana tsara tarehy,
Ry Jerosalema ô!
Ry Jerosalema, tanàna masina.
Ahintsano ny vovoka
Hiala amin''ny tenanao,
Ry Jerosalema!
Mitsangàna, ry Jerosalema ô!
Ry Jerosalema ô"}

{- "Mifohaza
Tafio ny herinao ry Ziona ô!
Tafio ny fitafiana tsara tarehy,
Ry Jerosalema"}

{- "Ahintsano ny vovoka
Amin''ny tenanao, mitsangàna ary mipetraha
Ao i Jerosalema
Vahao ny fatorana,
Amin''ny tendanao,
Ry Ziona ô
Vahao ny fatorana
Amin''ny tendanao,
Ry Ziona ô}

{- "Velomy ny hoby
Ry fitoeran-dava
Any Jerosalema
Fa Jehovah no efa nampionona
Sady efa nanavotra
An''i Jerosalema"}

{- "Ahintsano ny vovoka
Amin''ny tenanao, Mitsangàna
Mitsangàna ary mpiteraha
Ao i Jerosalema
Vahao  ny fatorana
Amin''ny tendanao,
Ry Ziona ô
Vahao ny fatorana
Amin''ny tendanao,
Ry Ziona ô"}

{Velomy ny hoby
Ry fitoeran-drava
Any Jerosalemna
Fa Jehovah no efa nampionona
Sady efa nanavotra
An''i Jerosalema"}

{- "Ahintsano ny vovoka
Amin''ny tenanao, Mitsangàna
Mitsangàna ary mipetraha
Ao i Jerosalema
Vahao  ny fatorana
Amin''ny tendanao,
Ry Ziona ô
Vahao ny fatorana
Amin''ny tendanao,
Ry Ziona ô"}
', '#3-XII', '#3') /end

insert into song values (52191, '219. Zanak''i Ziona ô!', NULL, NULL, '
{- "Zanak''i Ziona ô!
Toneo ny alahelo
Fa ireo mpankahala anao
Ho lao"}

{- "Andro mazava nirina
Itony, mifohaza
Fa ny aizim-pahorianao
Ho lasana"}

{- "Zanak''i Ziona ô!
Toneo ny alahelo
Fa ireo mpankahala anao
Ho lao"}

{- "Na dia be sy matanjaka aza
Ireo fahavalo miady aminao
Ho toy ny akofa
Aelin''ny rivotra izy
Dia tsy hahajanona eo anoloanao"}

{- "Zanak''i Ziona ô!
Derao Ilay Tsitoha,
Izay namonjy anao!
Tsy ho resy ny momba Azy
Mlihobia fa tsy manana,
Tsy manankery intsony
Handresy anao
Ilay devoly rafinao
Zank''i Ziona ô!
Toneo ny alahelo,
Fa ireo mpankahala anao
Ho lao"}

{- Na dia be sy matanjaka aza
Ireo fahavalo miady aminao
Ho toy ny akofa
Aelin''ny rivotra izy
Dia tsy hahajanona eo anoloanao"}

{- "Zanak''i Ziona ô!
Toneo ny alahelo
Fa ireo mpankahala anao
Ho lao".}
', '#3-XII', '#3') /end

insert into song values (52201, '220. Andao tika mba hidera', NULL, NULL, '
{1.
Andao tika mba hidera
Anagnahare
Hivetsevetse ty Agnarae
Hirebareba akanjo foty
Fientegn''Alahady
Hitsena ty fahatongavae}

{FIV.
Tsy haligno ty Jesosy
Ilay Mpagnafake
Anan''Agnahare Tokagne
Mpanavotse e!
Vonjeo zahay midrakadrakake
Omeo fo mandray anatse"}

{2.
Ko sorobagne ty hateao
Anagnahare
Hanompo sikily naho hivavake
Jesosy avao ro tano ndra ihe marare
Hagnavots''azo mba ho afake.
[FIV.]}
', '#3-XII', '#3') /end

insert into song values (52211, '221. Malalaka ery izao', NULL, NULL, '
{1.
Malalaka ery izao
Ny saha feno vokatra,
Ka iza no  handeha hankao
Hamory azy hiakatra?}

{FIV.
Ry Ray, Anao ny vokatra;
Anao ny iraka hanao;
Anao ny hery fatratra
Ka mba tendreo izay hankao}

{2.
Endrey! Fanahy marobe,
No mitoloko mafy ao
Andeha hamonjy azy ireo,
Andeha ankehitriny izao,
[FIV.]}

{3.
Zatovo mbola henjana!
Tandrify indrindra hianao,
Ka moa tsy hitsangana
Raha mba tendren''ny Tomponao?
[FIV.]}

{4.
Isika faly eto izao,
Dia aza mba mangina re!
Andeha hiaraka hanao
Izao vavaka izao hoe
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52221, '222. Indro Jeso miantso izao', NULL, NULL, '
{1.
Indro Jeso miantso izao
Te-haniraka anao;
Mba henoy ny feony hoe
''Ndeha ho irako!}

{"''Ndeha ho irako,
Mba hirahiko;
Be ny tany maizina
''Ndeha ho irako"}

{2.
Mba meteza re handre
Ry zatovo marobe,
''Tony hafa-dehibe :
"Ndeha ho irako!}

{"''Ndeha ho irako,
Mba hirahiko;
Be ny tany maizina
''Ndeha ho irako"}

{3.
Ry tanora manan-jo
Feno hery sady soa,
Maneke an-tsitrapo :
''Ndeha ho irako!}

{"''Ndeha ho irako,
Mba hirahiko;
Be ny tany maizina
''Ndeha ho irako"}

{4.
Antso mora, Tompo tia,
Antso anaty, reko ery
Feo miangavy tsotra anie
Fa mahery e!}

{"Reko ny antso izao,
''Zaho hamaly Anao,
Nefa olon''osa, Jeso
Ka tohanonao"}

{5.
Kapiteny tsara ô!
Hotohizako ny antsonao
Hanavao ny lokalo;
Ry Mpanajako}

{"Reko ny antso izao,
''Zaho hamaly Anao,
Nefa olon''osa, Jeso
Ka tohanonao"}
', '#3-XIII', '#3') /end

insert into song values (52231, '223. Tanoran''i Jesosy Kristy no mandeha', NULL, NULL, '
{1.
Tanoran''i Jesosy Kristy no mandeha
... ny Filazantsara;
Tsy sasatra na lavitra izay aleha,
Fa misy Ilay Mpikarakara,
Avia, ry olombelona!
Avia, ry mpanota ô!
Tanoran''i Jesosy Kristy no mandeha
Hitory ny Filazantsara.}

{2.
Tanoran''i Jesosy Kristy no mandeha
Mamangy izay mangetaheta
Manetry tena ka mifady rehareha
Mandositra ny tevateva.
Vonjo re ny ory fo!
Mahantra sy mijaly re!
Tanoran''i Jesosy Kristy no mandeha
Mamangy izay mangetaheta}

{3.
Tanoran''i Jesosy Kristy no mandeha
Ny paradisa no lovàna.
Tsy volamena na karama no mandeha
Fa ny fitiavana ihany
Aza dia ela re izao!
Indreto lova ho anao
Tanoran''i Jesosy Kristy no mandeha
Ny Paradisa ho lovàna.}
', '#3-XIII', '#3') /end

insert into song values (52241, '224. Misy feo miantso mafy', NULL, NULL, '
{1.
Misy feo miantso mafy
Ao anaty ao,
Mba henoy ka valio anio,
Ny Fanahy Masina manadina anao,
O! heoy ka mba valio anio.}

{FIV.
Inona no vitanao
Mba ho tsangabatonao,
Fa izao tontolo izao
Manna-tsotra aminao?}

{2.
Misy feo miantso mafy
ao anaty ao,
Mba henoy ka valio anio :
"Reo maritiora no manolotra anao,
O henoy ka mba valio anio.
[FIV.]}

{3.
Misy feo miantso mafy
ao anaty ao,
Mba henoy ka valio anio
Ao ny jentilisa koa,
Haka am-bava anao,
O! henoy ka mba valio anio
[FIV.]}

{4.
Misy feo miantso mafy
Ao anaty ao,
Mba henoy ka valio anio :
Ireo efa lasa manan-
kafatra aminao
O! henoy ka mba valio anio
[FIV.]}

{5.
Misy feo miantso mafy
Ao anaty ao
Mba henoy ka valio anio :
Ny taranaka ho avy manontany anao
O ! henoy ka mba valio anio
[FIV.]}

{6.
Misy feo miantso mafy
Ao anaty ao,
Mba henoy : aina mahasoa;
Dieny mbola misy ora
Hanorenanao,
Mahereza, mahefà tokoa!
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52251, '225. Misikina re, ry sakaiza ô!', NULL, NULL, '
{1.
Misikina re, ry sakaiza ô!
Ady mahamay no atao izao :
Ento ny ampinga sy sabatrao,
Sy ny fiarovan-dohanao}

{FIV.
Miomàna re, miomàna re
Aoka ho mpandresy an-tafika
Mialoha anao, Tompo Lehibe
Dia Jeso Mpanjakan-danitra.}

{2.
Miomàna fa efa natokana
Ho miaramila ny tenanao
Ka hahazo babo haondrana
Ho am-Paradisa ambony ao.
[FIV.]}

{3.
Mandrosoa sakaiza ho tonga
Ao an-daharan''adin''ny masina;
Aringano ireo fahavalonao,
''Zay mivory alinalina.
[FIV.]}

{4.
O! ry voatendry hanafika!
Mifalia fa tsara Mpitarika:
Aoka tsy hiverin''ilàlana
Eny! Mahereza tanàna!
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52261, '226. Aza kivy hianao', NULL, NULL, '
{Aza kivy hianao,
Ry mpiasan''i Jehovah,
Fa tsy maintsy mbola hisy be
Ny famalian-tsoa}

{Ary izay mba te hiasa tokoa,
Aoka tsy hisalasala re,
Fa miainga izao hankary!
O! aza kivy foana,
Faly tokoa Ilay Maherinao,
Fa hisy vokatra}

{Mahereza ry sakaiza malala,
Fa izao no fotoana fiasana,
Fa ny aretina mandreraka}

{O! eny re izao; izao re
Fa ny aizina tsy mbola tonga;
Eny re! faly ka dia mba avia
Ry sakaiza o!
O! eny re izao; izao re
Fa ny andronao izao mazava;
Eny re! faly ka mba avia
Ry sakaiza o!}

{Mahereza ry sakaiza malala,
Fa izao no fotoana fiasana
Fa ny aretina mandreraka}
', '#3-XIII', '#3') /end

insert into song values (52271, '227. Antsoin''ny Tompo hianao', NULL, NULL, '
{1.
Antsoin''ny Tompo hianao
O! miainga sakaiza o!
Hiasa ao an-tsaha ao,
Fa be ny vokatra ao izao}

{FIV.
O! miainga ankehitrio
Ny hatak''andro mba ario;
Ny Tompo manantena anao,
Faingana ry sakaiza o!}

{2.
O! miainga an-tsitrapo
Hamonjy olo-marobe,
''Zay mitaraina eram-po,
Maniry ranon''aina re
[FIV.]}

{3.
Mandalo re ny andronao!
He! mihariva ny ety,
Ka  aza ela hianao,
Fandrao dia very ny any
[FIV.]}

{4.
O! miainga sakaiza o!
Tsy renao va izany feo ?
Anio ihany no anao,
Ka hararaoty rahateo}
', '#3-XIII', '#3') /end

insert into song values (52281, '228. Tsy an-tery re,tsy vozonina', NULL, NULL, '
{1.
Tsy an-tery re,tsy vozonina
No nidirana ao am-piangonana!
Tena sitrapo tsy hanenenana,
Ka nahoana moa no hiafenana ?}

{FIV.
Moa misy firy sosona ny lanitra,
Ary firy izay Andriamanitra ?
Nahoana hianao tsy ho atsy ,tsy ho aroa ?
Tapaho izay hevitrao!}

{2.
Tsy mba fanjakan''Andriana mahamay
No netezanao hatao batisa indray :
Tena sitrapo tsy rombon''olona,
Ka nahoana moa no ijanonana ?
[FIV.]}

{3.
Tsy mba ranomaso teren-tsetroka,
No netezanao hilatsaka ho mpandray :
Tena sitrapo tsy misy fitaka,
Ka nahoana moa no misintaka ?
[FIV.]}

{4.
Raha sitrapo no ivavahana,
Aza tia tena,fa ny namana;
Raha sitrapo no iangonana,
Azerezereo ny fialonana;}

{Fa tsy folo sosona ny lanitra.
Tsy mba manan-njay Andriamanitra
Ny marina atao,ny diso ialao,
Anao ny lanitra!}
', '#3-XIII', '#3') /end

insert into song values (52291, '229. Sao misy te-hirahiko ?', NULL, NULL, '
{1.
Sao misy te-hirahiko ?
Iza re
Hanangona ny ondriko,
Iza re
Fa he! ny manodidina,
Miraparapa maizina,
Fa tsy mandeha marina :
Very re}

{2.
Ny mponina ao Imerina
Be mania
Ka iza no hamerina ?
Mba misia
Torio Jesoa Mpanavotra;
Ka aza mba matahotra,
Na mba miahotrahotra :
Matokia}

{3.
Ny olona Sihanaka
Mba jereo
Sy ny Betsimisaraka
Mba vonjeo
Ny Bara sy ny Betsileo,
Ny Sakalava koa jereo ,
Mahantra sady hitoreo :
mba vonjeo}

{4.
Ny anaram-pitiavana,
Mijere
Izay onena havana,
Mamonje
Fa namanao anie ireo,
Ka mba hevero sy jereo
Ka aza avela hitoreo :
Mamonje}

{5.
Ny ao amoron-tsiraka,
Tompo o!
Angony tsy hisaraka
Aminao
Vonjeo tsy hanalavitra
Ny Fanjakan''ny lanitra,
Fa samy ho finaritra
Aminao}
', '#3-XIII', '#3') /end

insert into song values (52301, '230. O! ry raozy,mifohaza re!', NULL, NULL, '
{1.
O! ry raozy,mifohaza re!
Tonga indray ny lohataona soa,
Soa hianao matory,tsy mandre.
Ny voahary,samy taitra avokoa!
Velona,mahazo aim-baovao.}

{FIV.
Mifohaza re! ry tanora o!
Raozin-dohataona anie hianao,
Ka mifohaza re!}

{2.
O! ry raozy,mivelatra remba handrara-pofo-manitra,
Fo madio sy toetra masina,
Be fitiavana,mahasoa tokoa;
Io no tena haingo fatratra,
[FIV.]}

{3.
o! ry raozy mahavokatra
Asam-pahasoavana meteza,
Tena sitrak''Andriamanitra,
Tian''ny olona,tsangam-bato soa
Ny tanora vonin-draozy
[FIV.]}

{4.
Asandrato ilay fotoana
Hamindrana anao ry raozy soa,
Tsy halazo na hihintsana
Fa hakarina ao an-danitra ao,
Dia ho velona maharitra.
[FIV.]}

{5.
Ry Jesosy Zoky be fitia,
Mba torahy fitahiana feno,
''Reo vonindraozinao maniry;
O! fefeo manodidina,
Tsy ho simbam-pahavalo ao.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52311, '231. Ny masoandronao miposaka', NULL, NULL, '
{1.
Ny masoandronao miposaka
Ka araraaoty sao milentika,
Adidy maro no miandry ao,
avia re! avia mba tontosao.}

{FIV.
Tanora o! avia,avia re!
Andeha hiaraka hiasa ao;
Izay kely vitanao tsy very re,
Fa voasoratra ao an-danitra ao.}

{2.
He! maizina izao ny làlany,
Tsy fantany izay hizorany;
Ireo havanao ireo miandry ao;
Tarihonao ireo fa adidinao.
[FIV.]}

{3.
Tanora namanay mba matokia,
Jesosinao hitarika ny dia;
Ka na aiza na aiza no alehanao,
Banjino Izy, fa tsy mba mandao.
[FIV.]}

{4.
Ny andronao ety hifarana,
Jesosy ihany re ny làlana
Ireo nahefa ny adidiny,
Mankao an-danitra,tahiriny.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52321, '232. Mifohaza, ry tanora', NULL, NULL, '
{1.
Mifohaza, ry tanora,
Tonga indray ny fotoana,
Fa Jesosy miantso mora :
Mifohaza, ry tanora!}

{FIV.
He! Jesosy miantso mora,
Mba hiasa,ry tanora,
He! Jesosy miantso mora :
Mifohaza, ry tonora!}

{2.
Andro maro sy taona maro,
No nanaovanao ny anao,
Fa izao no Jeso miantso :
Mifohaza hianao!
[FIV.]}

{3.
Hianao, ry tanora,
No tandrify izao antso izao,
Ka aza dia sondrian-tory :
Mifohaza hianao!
[FIV.]}

{4.
Andeha àry isika, ry tanora,
Mba hanao izao asa izao :
Ho tanteraka tokoa
''Lay didy vaovao
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52331, '233. Ambarao ny famonjena', NULL, NULL, '
{1.
Ambarao ny famonjena,
Famonjena lehibe;
Jesoa Kristy no nomena,
Hamonjena marobe.}

{FIV.
Ambarao,tantarao,
Mba ho ren''ny marobe
Ambarao,tantarao,
Famonjena lehibe.}

{2.
Ambarao ny famonjena,
An''izao tontolo izao;
Vita izao ny fanekena,
Jeso tonga ho anao.
[FIV.]}

{3.
Ambarao ny famonjena,
Vonjy isa,fa tsy roa;
Ny mpanota hovonjena,
''zay mibebaka tokoa.
[FIV.]}

{4.
Ambarao ny famonjena ,
Paradisa azo izao;
Soa very tao Edena ,
Hamodina indray izao.
[FIV.]}

{5.
Ambarao ny famonjena,
''zay no hafatry ny Ray!
Fa ny terak''i Adama,
Ho velomina indray.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52341, '234. Mbola matory hianao ver y mpiasa ?', NULL, NULL, '
{1.
Mbola matory hianao ver y mpiasa ?
O! mifohaza fa ny andro maraina!
Isaorako indrindra fa velona ny aina,
Izaho hiezaka
Faran''ny aina.}

{2.
Henekeneno,fehezonao ny vatsy,
O! mandehana,vitao ny adidy;
Dieny tsy lasa,hararaotinao ny andro,
Mba ahazoanao manao asa soa.}

{3.
Indro miposaka indray ny masoandro!
O! mandehana,afafazo ny voa.
Oranareo izao,ka aza variana.}

{4.
Ento ny tena sy saina matsilo,
Mba ho fiasàn''i Jesosy izao,
Ny maizin-tsaina,omeo ny fanilo;
''zay mbola maizina,mba hazavao.}
', '#3-XIII', '#3') /end

insert into song values (52351, '235. Mitsangana,mitoria!', NULL, NULL, '
{1.
Mitsangana,mitoria!
Hianareo izay nohazavaina;
Mandehana,mampieleza
Ny tenin''ny fiainana}

{FIV.
Mitoria ny Baiboly;
Ambarao ny teny soa,
Ka lazao ny
Tenim-pamonjena}

{2.
Ny Tanosy sy ny Tandroy
Miantso hoe :vonjeo fa maniry
Ny Anaran''i Jesosy,
Izay famonjena lehibe!
[FIV.]}

{3.
Mahonena koa ry Bara
Sy ny Tesaka ary Sakalava,
Fa fatoran''ny satana,
Ka moa afointsika ireo ?
[FIV.]}

{4.
Ry zatovo sy mpitory!
Indreo hianareo izay mandre izany
Mirotsaha hanambara
Ny soa efa azonao
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52361, '236. Ry Ziona o!', NULL, NULL, '
{- Ry Ziona o!
Izay mitondra teny soa mahafaly hoe :
Miakara ao an-tendrombohitra avo,
Ry Jerosalema o
Izay mitondra teny soa mahafaly hoe :
Asandrato mafy ny feonao
Asandrato fa aza matahotra}

{- Lazao,lazao amin''ny tanànan''''i Jodia hoeandriamanitrareo
Indro avy Jehovah Tompo amin''ny heriny}

{- Ka ny sandriny ihany no hanapaka.
Indro ny valim-pitia hatolony,
Ary ny valin''asa homeny,dia eo anoloany}

{- Ary ny sandriny ihany no hanapaka,
Ary ny valin''asa homeny,
Dia eo anoloany.}

{- Ry Ziona o!
Izay mitondra teny soa mahafaly hoe :
Miakara ao an-tendrombohitra avo,
Ry Jerosalema o!
Izay mitondra teny soa mahafaly hoe :
Asandrato mafy ny feonao,
Asandrato fa aza matahotra}
', '#3-XIII', '#3') /end

insert into song values (52371, '237. Mba mitsangana re mba ho mpiasa soa', NULL, NULL, '
{1.
Mba mitsangana re mba ho mpiasa soa,
Ka hiasa eo an-tanim-boalobok''i Jehovah,
Fa izao ankehitriny izao
Ny andro mazava,
Ka reseo izay misakatsakana
Ao an-tsaha ao;
Ny mpamonjy he! miandry
''zay ho irany,
Ka manendry anao hiandry
''reo navotany}

{2.
Indreo ny ondrin''i Jesosy
''zay andraikitrao;
Maro koa no tsy manaiky
Ka adidinao;
Entonao ny famonjena,
Ka vonjeo e!
Mazotoa manolo-tena
Am-pitia re!}

{3.
Ry jesosy Mpiandry tsara,
Ny fitiavanao,
Aoka mba hitoetra tsara
Ao anatiko ,
Ho fiainan''ny Fanahy,
Toky,heriko!
Ho mpanirakiraka ahy,
Amin''asa soa.
4.
Mananjara ny mpiasa
"''zay mahantra,
Fa ny teny fikasana
Tsy mivadika;}

{- Miainga ankehitriny!
Aza mba mangatak''andro
Fa mihelina ka lasa
''zao fiainatsikaa izao :
Miasà re,milofosa koa,
Aza ela loatra hianao}

{- Ny ety an-tany ihany
No andro famafazana
Fa ny ao ankoatr''
Izao fiainana izao ,
Dia andro efa hijinjana izay nahaty.
Mahereza,ry sakaiza ,raha mbola ety :
Ao ny lova ho anao}

{- Tsara re,soa tokoa!
Jeso Tompo no miandry
Handray anao,
Ka faly Izy hanome anao,
Zava-tsoa irina indrindra,
Ka hiara-paly Aminy mandrakizay.}
', '#3-XIII', '#3') /end

insert into song values (52381, '238. Mifohaza ry mpiasa voafidy o!', NULL, NULL, '
{Mifohaza ry mpiasa voafidy o!
Nohosorana hiasa
Mahareta,mivavaha,
Fa miandry ao
Lova soa tsara fara,
Indro ho anao.}
', '#3-XIII', '#3') /end

insert into song values (52391, '239. Mifohaza ry tanora o!', NULL, NULL, '
{1.
Mifohaza ry tanora o!
Aza mba sondran-tory
Andro fifohazana izao,
Mitsangana hianao.}

{FIV.
Mitsangana mba hitory
Ny fitiavan''i Kristy Tompo,
Fa izao no andron-kerinao;
Mitsangana hianao!}

{2.
Moa tsy manaitra anao va re,
Ny fitiavan''i Jesosy!
Mba tsarovy ''lay rà nitete,
Nanavotany anao.
[FIV.]}

{3.
Moa tsy renao va fa be
Ny tanora tafa-foha!
Ry sakaiza ,mifohaza re,
Aza mody tsy mandre.
[FIV.]}

{4.
Mifohaza ry tanora o!
Mba hendre fa mana-tsaina
Aza mba manara-tsitrapo
Aza mb ataman-daolao.
[FIV.]}

{5.
Taoimy ny tanora namanao;
Taomy mba ho vaovonjy;
Tsy mba fantany ny fantatrao,
Dia ny fiainana.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52401, '240. Misy feo manaitra mafy', NULL, NULL, '
{1.
Misy feo manaitra mafy
Moa mba renao namako ?
Ondry tsy misy mpiandry
Iza no hamonjy ireo ?
Bibidia,mosary koa,
Samy mety hampahory ireo.
- "Mamonje! mamonje!
Fa ny Tompo dia onena ireo"}

{2.
Misy feo manaitra mafy
Moa mba renao namako ?
Voly tsy misy mpiahy
Iza no hamory ireo ?
Be tokoa ny vokatra,
Nefa ny mpiasa vitsy re!
- "Mamonje! mamonje!
Fa ny Tompo dia onena ireo"}

{3.
''Ndreto izahay ry Tompo
Ho fanirakirakao :
Hianao e! no nifidy
Y mpiasa hirakao.
Manomeza hery be
Ny mpiasa voafidinao
- "Manampia! manampia!
Hanefanay ny adidinay"}
', '#3-XIII', '#3') /end

insert into song values (52411, '241. Tanora mandrosoa', NULL, NULL, '
{1.
Tanora mandrosoa
Mba hiantafika :
Tonga ny fotoana,
Jesosy Kapiteny,
Mahay mitarika,
Ka tsy atahorana,
Sasa-poana re!
Ny mikatroka aminay,
Fa Ilay Mpanjaka hendry,
Mahery sy mahay
No mibaiko ny dianay}

{FIV.
"aoka isika ho vonona re!
Tanora mpiady,
Tano tsara ny ampinga;
Finoana ny tenin''ny Ray,
Ampinga tsy toha
''zay mahery ho anao"}

{2.
Tsarovy ry sakaiza,
Ny ady masina!
Satro-boninahitra
Ho azon''ny tanora
Miady marina,
Dia ny soan''ny lanitra.
Mahereza re!
Aza kivy fo hianao,
Jesosy tompon'' ady
Miantoka anao,
Ka dia mandrosoa hianao
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52421, '242. Sambatra izay mahefa soa', NULL, NULL, '
{1.
Sambatra izay mahefa soa ,
Raha velon''anina ety an-tany;
Izy ireny dia mamaky voa
Izay tsy very foana ,fa any ihany.}

{FIV.
"o! ry tanàm-piantrana mamafaza,
Fa ny Tompontsika no mandidy;
Na tsy fantatr''olombelona aza,
Jeso Tompo mitahiry,mitadidy"}

{2.
Na dia misy aza fihafiana
Ary be fandavan-tena mafy,
Mbola ao ny andro hifaliana,
Hijinjana indray ny voa nafafy.
[FIV.]}

{3.
Iza kosa moa no andrandraina
Mba hanampy ireo izay mahantra,
Hampitony ireo izay miferin''aina,
Raha tsy misy ao ny fo miantra ?
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52431, 'Mifohaza,aza mba matory!', NULL, NULL, '
{1.
Mifohaza,aza mba matory!
''zay re no baikontsika izao;
Aza miala sasatra akory
Mba hanaraka ny Tomponao.}

{FIV.
Jeso Kapiteninao tokoa
No mpitarika anao;
Mandrosoa,mandrosoa!
Mandrosoa izao,
Fa miandry anao ny nosy;
Momba anao Jesosy.}

{2.
Mitsangana,aza dia sondriana,
Misy asa tena ilàna anao,
Tomponao nizaka fihafiana
Eo alohanao arahinao.
[FIV.]}

{3.
Eo akaikinao ny famonjena,
Dia Jeso loharanon-tsoa :
Raiso Izy,dia ny fandresena
No ho ao an-tobinao tokoa
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52441, '244. Ry tanora maitso volo', NULL, NULL, '
{1.
Ry tanora maitso volo,
He! andro anjaranao ankehitriny izao,
Ary ny tenanao dia matanjaka,
Ary ny saina koa matsilo fatratra}

{- "nefa saino ity fiainana,
Fa mandalo sy mihelina,
Ka ny mety izao dia mihavàna
Amin''i Kristy Mpamonjy be fitia"}

{2.
Mba jereo itony rano,
Misononoka mandeha mora;
Vetivety foana dia mivarina
Amin''ny antsana mahatsiravina}

{- "toy izany koa ity fiainana :
Vetivety faly,vetivety ory,
Ka ny mety izao dia mihavàna
Amin''i risty Mpamonjy"}

{3.
Mora miova ny eto an-tany;
Tsy misy tsara izay mateza.
Ny any ambony dia tsy hita lany
Fahasambarana sy fiadanana}

{- "Mora miova ity fiainana,
Fa mandalo sy mihelina,
Ka ny mety izao dia mihavàna
Amin''i Kristy Mpamonjy be fitia"}
', '#3-XIII', '#3') /end

insert into song values (52451, '245. Misehoa ho mpiasa soa', NULL, NULL, '
{- "Misehoa ho mpiasa soa
Amin''izao andronao izao
Dia mazotoa o! milazà,
Mitoria ny tsara,
Dia asehoy ny indrafo amin''ireo.
Ny Tompo ambarao tokoa"}

{"O! mandehana,mazotoa,
Ka torio ny Mpamonjy soa,
Milazà hiara-mandre,manehoa!
Mitoria adidinao tokoa,
Mitondrà!
Hitondra ny teny soa
Hahazava amin''ireo
Maro,maizin-tsaina ireo"}

{- "torionareo ny famonjen-dehibe,
Aelezo eran''izao tontolo izao
Ny soa azonao,
Fa efa misy famonjena
Ho an''izao rehetra izao
Samy antsoiny hankao"}

{- "voninahitrao,sakaiza, hanasoa
Ireo mpanota marobe,
Sady namanao tokoa"}
', '#3-XIII', '#3') /end

insert into song values (52461, '246. Ry zanaky ny filazantsara', NULL, NULL, '
{1.
Ry zanaky ny filazantsara,
Isika anie ka manana anjara :
Vavolombelona hanambara
Ny asan''i Jesoa Tompo soa.}

{FIV.
Ento afafazo,
Ento ny teny soa;
Ka samia mazoto
Amin''ny asa soa,
Hanao isa''andro,
Hanao an-tsitrapo,
Amin''ny zotom-po,
Ny asan''i Jeso Tompo soa.}

{2.
Ry notahian''ny Tompo Jehovah,
Ry zanaky ny andro mazava,
Toloran-kafatra hampisava
Ny aizm-be manodidina.
[FIV.]}

{3.
O! ry tanora izay tafahaona,
Isika anie marain-dohataona!
Taom-pafazana ka hararaoty :
Fafazo voa ity tany ity.
[FIV.]}

{4.
Ry eo an-tanim-bolin''ny  Tompo,
Aoka mba ho mpanompo mazoto,
Ny fanjakan''i Jehopvah Ray.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52471, '247. Miantso ny Tompo haniraka anao', NULL, NULL, '
{1.
Miantso ny Tompo haniraka anao,
Hiasa ao an-tanim-boalobony ao;
Saina mazava sy tanjaka be
Efa nomeny hanaovana re!
Ka mandrosoa izao
Hanao an-tsitrapo;
Ankino aminy izay hanjo}

{2.
Misy anjara miandry anao,
Ka n''iza n''iza hirotsaka ao,
Tsy hahavita ny anjaranao,
Moa tsy hamonjy izay hianao ?
Koa mandrosoa izao,
Hanao an-tsitrapo;
Vitao ny asa na inon-kanjo}

{3.
Mety ho mora na sarotra atao,
Ny hamitanao izany tokoa,
Nefa aza kivy,fa hoy Izy aminao :
Ny fahasoavako ampy ho anao!
Ka mandrosoa hanao an-tsitrapo,
Izy hamonjy raha misy manjo}

{4.
Ny fikelezanao aina tety,
Tsy very foana,ho hita ary;
Jesosy Tompon''ny asa natao
No hanome hasambarana anao,
Ka mandrosoa izao,
Hanao an-tsitrapo,
Odio tsy hita ny zava-manjo}
', '#3-XIII', '#3') /end

insert into song values (52481, '248. Jesosy mikaroka mpiasa vaovao', NULL, NULL, '
{1.
Jesosy mikaroka mpiasa vaovao
Hanitatra ny fanjakana izao.
Ka zovy no mendrika izany lazao ?
Ho mpiasa mazoto no sady manao.}

{FIV.
Meteza
Mateza ho mpiasa vaovao,
Meteza ho mpiasan''ny Tompo hianao;
Meteza fa izy hanambina ny asanao!}

{2.
Miandr dia miandry antsika izy izao,
Hamonjy ireo asa tsy mpanao ,
Ka isan''ny vonona va hianao
Hamadika bainga,hamboatra ny lao
[FIV.]}

{3.
Andeha hirotska isika ''zareo,
Hanaiky ny antson''ny Tompo anikeo,
Ny ratsy mitaona toero reseo!
[FIV.]}

{4.
Manezaka ambony dia ambony tokoa,
Hanefa ny adidy sy fanaovan-tsoa;
Mikatsaka mafy dia mafy tokoa,
Hamonjy taranaka simba sy voa.
[FIV.]}

{5.
Raha tojo ny mafy sy sarotra atao,
Ka mila hikoy sy ho kivy ny fo,
Herezo ny vavaka fa io no fanao :
Ny herin''ny Tompo hamonjy anao
[FIV.]}

{6.
Jesosy, tohano ireo mpiasanao :
Raha noana sy reraka fahanonao;
Tolory ny hanin-tshaza azy oreo,
Hatanjaka tsara sy hahaleo.
[FIV.]}

{7.
''Zaytompon''adidy,hatseho, Tompo o!
Hanohana tsara ireo mpiasanao.
Ny mpasa matanjaka mahefa be,
Ny reraka lava mamono antoka e!
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52491, '249. Hafatra nomen''ny Tompo anao', NULL, NULL, '
{1.
Hafatra nomen''ny Tompo anao :
Ny namanao ilazao,
Ary koa ireo am-pielezana,
Sao tsy tafiditr''ao.}

{FIV.
"O! miaingà ka mitoria,
Sao tsy ho tratranao
Ilay ora  farany;
Aza mba mangatak''andro hianao
Fa hararaoty hatramin''izao,
Sao hanadinana anao indray
Ilay ora farany"}

{2.
Ela be no niantsoana anao,
Ka tsy manantona e!
Ary koa ireo anilanao,
Moa tsy tsaroanao va ?
[FIV.]}

{3.
Marina tokoa va hianao,
No miandry tsy miomana
Ka hanisy tsiny ny Tomponao
Amin''ny farany ?
[FIV.]}

{4.
Raha ny tsiron''ny tany no tsy foy,
Sao dia mamatotra e!
Dia ny lanitra no sisa hafoy,
Ka tsy ho hitanao.
[FIV.]}

{5.
Koa miomana re hatrizao,
Jeso mitarika e!
Ka torio ilay fitiavam-baovao,
Mba hiadananao.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52501, '250. Indro avy ny maraina vao', NULL, NULL, '
{1.
Indro avy ny maraina vao
Ny fitarikandro tsinjo ao,
Famonjena naposakao
Ry Jeso Tompo o!}

{FIV.
Indro navy ny maraina,
Ka ilay Filazantsara soa,
No fanilon''aina,
Mamiratra tokoa!}

{2.
Mifohaza ry tanora o!
Mitsangana ankehitriny  izao,
Fa Kristy hampahazava anao
Sy ho fiainanao
[FIV.]}

{3.
Ry tanora o! ambarao,
Ka torio ny soa noraisinao
Mba ho lovan''ny taranakao;
Famonjena re lazao!
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52511, '251. Moa inona ange no mba vitako e !', NULL, NULL, '
{1.
Moa inona ange no mba vitako e !
Hatrizay nahaveloman''izay,
Sao dia laniko toana ny androko va,
Tsy mba nahavitana asa soa ?}

{FIV.
O! andeha honerana kosa
''Reo fotoam-bery
foana ireo;
Ny adidy anie miavosa
Aza varimbariana
hianao!}

{2.
O ! mba saino, hevero, ry namako ô!
''Tony androm-piainana aty,
Fa mandalo, mihelina ny andronao,
Moa tsy ho hararaotina va?
[FIV.]}

{3.
Asa maro no indro miandry anao
Mila tanjaka sy herim-po,
Fanasoavan-taranaka,
firenenao:
Io no andrin''ny fanaovan-tsoa.
[FIV.]}

{4.
Miezaha ry miaina ! miasà dieny izao;
Be ny asa tandrify anao
Aza avela hisy very ireo andronao.
Sao manenina ambony hianao.
[FIV.]}

{5.
Enga anie ny asa kely mba vitako e !
Ho ambinina raha mbola ety,
Mba ho sitraka enti-matory anie,
Hovaliana, tsarovana ery.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52521, '252. Mankanesa ao Betifaga re hianao', NULL, NULL, '
{Mankanesa ao Betifaga re hianao
D''io vohitra tandrify anao:
Misy zana-boriky izay mifatotra ao,
Andeha mba vahao"}

{FIV.
"Fa ilain''ny Tomponao
Mba homena Azy re ! Tadiavin''i Jesosy re ka alao!
Jeso no mandidy anao"}

{- "Betifaga akory tsy mba lavitra anao
Ireo tanàna manodidina
Fa vahoaka maro no migadra ao!
Fibebahana no atao"
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52531, '253. Ny Tompo miantso mpanompo', NULL, NULL, '
{1.
Ny Tompo miantso mpanompo
Ho iraka, solon''ny Tompo,
Hanompo ny Ray, ho mpanompo.
Ka izao no vononkandeha ?
Hiantso ny olon-drehetra
Hanompo ny Tompon''ny tompo;
Hanompo ka aina no fetra
Ka iza no vonon-kandeha?}

{FIV.
"Izaho, hirotsaka ho irakao.
Iraho, fa vononkandeha tokoa"}

{2.
Ny Tompo miantso mpanompo
Hitory ny tenim-pamonjena,
Hanafaka ireo mahonena,
Ka iza no vonon-kandeha
Hitaona ny velon-drehetra
Hanompo ny Tompo Tsitoha;
Hanompo, ka aina no fetra; Ka iza no vonon-kandeha
[FIV.]}

{3.
Ny Tompo miantso mpanompo
Hamoha ny olo-matory, Hidera ny Tompon''ny tompo,
Ka iza no vonon-kandeha
Ho mpiady handresy ny mafy,
Hanao soronaina ny tena
Hanjera ny herin''ny rafy,
Ka iza no vonon-kandeha
[FIV.]}

{4.
Ny Tompo miantso mpanompo
Ho iraka, solon''ny Tompo,
Hanaraka Ilay Kapiteny
Ka iza no vonon-kandeha
Mba raiso ry Tompo ny foko,
Fa atolotro Anao manontolo;
Ekeo re, ny fo mitoloko,
Fa vonona ho irakao.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52541, '254. Moa ho vita ihany ve', NULL, NULL, '
{1.
Moa ho vita ihany ve
 ''Ty adidy lehibe,
''Zay napetraky nyTompo Jesosy ?
Ny havoana izay aleha,
Lohasaha mangina re,
Tany maro ''zay manerana ny Nosy}

{FIV.
Ry Jesosy ô avia!
Mba meteza Hianao
Mba hitantana ny dia
Tsy izahay no misafidy,
Fa Hianao maniraka
No mandidy.}

{2.
Ny fahaizana va re,
Tsy anananay anie,
Fa ny hafatrao ihany no lazaina ;
Votsa vava izahay,
Kely hery, tsy mahay,
Ny Fanahy Masina andrandraina.
[FIV.]}

{3.
Raha eo imasonao
Sy izao tontolo izao,
Tsinontsinona tokoa izahay
Nefa ny Anaranao,
Reharehanay izao,
Ka ny voninahitrao momba anay.
[FIV.]}

{4.
Tsy avelanay tokoa
Hianao ry Jeso ô!
Raha tsy momba sy mitahy anay,
Dia hitombo sy hamoa,
Ka ny asanao ho soa.
Ho mpanompo matatoky Anao ''zahay
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52551, '255. He ! mitsangàna hatrizao', NULL, NULL, '
{1.
He ! mitsangàna hatrizao
Ry tanora ô henoy ny feon''ny Tomponao,
''Zay miantso anao izao.}

{FIV.
Ka mitsangàna, ry tanora ô!
Fa Izaho no maniraka anao,
Ka misikina mba hitory,
Fa Jesosy Tompo miantso anao :
Indreto izahay, ry Tompo ô
Hidera Anao, hankalaza Anao.
Indreto izahay, ry Tompo ô
Hidera Anao, hankalaza Anao"}

{2.
He ! maizim-be ny tany ao,
Ry tanora ô andeha re hitory ao !
Ka aoka mba tsy hatory izao.
[FIV.]}

{3.
Indrisy re! Indrisy re!
Ry tanora ô olo-mahantra marobe
Izay miandry anao izao.
[FIV.]}
', '#3-XIII', '#3') /end

insert into song values (52561, '256. Jesosy no miteny', NULL, NULL, '
{1.
Jesosy no miteny,
Miantso hoe Avia !
Ka iza no hankeny
Mba misy mety va?
Ny vetsonao mangina,
Dia ny eritreritrao,
No feo izay mandona,
Ka raiso sy ekeo.}

{2.
Matetika ny lasa,
Tsy tena mahasoa;
Tohero mba hampiova
Ho fatratra tokoa
Ny sento sy tomany.
Iriko ho natao
Hanondrotra ny tany
Ho lanitra vaovao.}

{3.
Ny Tompo tsy mametra
Izay vonona sy tia,
Ny olona rehetra,
Antsoiny hoe avia !
Ny Tompo no mandona,
Mlantso hoe vohay !
Ny feon''Izay mandona,
Tazâno, fa izay.}

{4.
Jehovah ô ! midina
Hiaraka aminay.
Tongava ka midina
Ho ao an-tranonay;
Fa efitra mangina
Ity aleha ity,
Fa tany be ahina,
Tantano ho ery.}
', '#3-XIII', '#3') /end

insert into song values (52571, '257. Ao ampitan''ilay lohasaha iray', NULL, NULL, '
{1.
Ao ampitan''ilay lohasaha iray,
Alo-pahafatesana koa,
Moa mba tsinjonao va ilay tanàna iray,
''Zay mamiratra toa vaotsoa}

{FIV.
Ny masoandrony tsy mba milentika koa,
''Zay rehetra mby ao voadio avokoa,
Ka mamiratra toa vatosoa.}

{2.
Ny Mpanjakany dia Ilay Ray be fitia,
Sady aina ho an''izay ao,
Ry mpanota mibebaka ô ! matokia !
Hosasany dia hadio hianao
[FIV.]}

{3.
Nefa tsinjoko fa ny fidirana ao, am-pototr''ilay lakroa ;
He ! mideza sy ety ny lala-mankao,
O ! ry foko, moa ho kivy va ?
[FIV.]}

{4.
Olo-masina maro no tsinjoko, indreo,
Ao mihira sy mitafy vao ;
He ! iriko indrindra mba ho isan''ireo,
Ka tantano, Jesoa aho ho ao,
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52581, '258. Be zava-manjo ety', NULL, NULL, '
{- "Be zava-manjo ety,
Ka iza no hanafak''ahy,
Ka iza no hanafaka anao izao"}

{- "A ! ny fonao ô
Tsy manadino anao,
Ry Jerosalema,
Tanàna ao ambony"}

{- "Fa ity, tany tsy mba mahasoa,
Fa misy fahoriana;
Lanitra vaovao,
O! lanitra vaovao,
No irinay"}

{- "Ry tany be fifaliana,
Herezo ry fanahy,
Hanaraka Anao anie aho,
Ry Ilay Mpamonjy"}

{- "Mizora ry fanahy,
Ka aza mba mania,
Fa ento ny fonao
Ho any an-tany soa ao.
Eny, ka dia mba mizora re"}

{- "Hanaraka Anao aho,
Ry Ilay Mpamonjy anay,
Lasa mialoha anay
Ao i Ziona"}

{- "Handray anao
Handray anao soa amantsara"}

{- "Mizora ry fanahy,
Ka aza mba mania,
Fa ento ny fonao
Ho any an-tany soa"}
', '#3-XIV', '#3') /end

insert into song values (52591, '259. Jesosy no fitsipiky ny marina', NULL, NULL, '
{1.
Jesosy no fitsipiky ny marina;
Ao ambadiky ny rahona,
Ao amin''io ilay tany vaovao,
Famirapiratan''ny lanitra"}

{- "Tany rako-boninkazo, Fonenan''ny olona sambatra
Tanindrazana mandrakizay re !
Fonenan''ny tra-bonjy re"}

{2.
Andron''ny fahazavana,
Hianao no Mpamela heloka,
Jesosy no anton''izany,
No mitarika ny diantsika"}

{- "Avia àry Hianao!
Vonjeo izahay amin''ny fisalasalana,
Ary atoroy anay re,
Ny làlan-tsara haleha any an-danitra"}
', '#3-XIV', '#3') /end

insert into song values (52601, '260. A ! tendrombohitra avo indrindra', NULL, NULL, '
{- "A ! tendrombohitra avo indrindra
Ny lanitra"}

{- "A! tendrombohitra avo indrindra
Ny lanitra"}

{- "A! misikina, misikina e !
Ho ''aho hianareo,
O ! mba hiakatra"}

{- "A misikina
Ho ''aho hianareo
Misikina mba ho ary"}

{- "O! ho an-tany soa honenana,
Honenantsika izao re !
Misikina mba ho ary"}

{- "A! ry Jesosy,
Ilay Maherinay,
Mba tariho làlana izahay}

{- "Voninahitra
Ho amin''ny Avo Indrindra lahy e !
Voninahitra
Ho an''Andriamanitra!}

{A ! voninahitra !
Voninahitra
Ho amin''ny Avo Indrindra lahy e !
Voninahitra
Ho an''Andriamanitra"}
', '#3-XIV', '#3') /end

insert into song values (52611, '261. Mangamanga fatratra', NULL, NULL, '
{1.
Mangamanga fatratra
Iry lanitra,
Ka mahafinaritra
Tazan-davitra.
Miaingà, ry foko ô !
''Ndeha hitsidika ao,
Fa tanàna soa tokoa
No hohitanao
Ao ny lapan-dRay,
Eny, efa hatrizay,
Vonona handray anao
Ka modia izao.}

{2.
He ! mivoha ho anao
Re ny lanitra:
''Zay rehetra te-hankao
Ho tatiditra ao.
Zanak''Andriamanitra
Mampitohy indray
Tany sy lanitra
Rava fahizay.
Ao ny lapan-dRay,
Eny, hatrizay,
Vonona mandrakizay
Ka modia izao.}

{3.
''Zaho hody Tompo ô!
Hody Aminao ;
Any ihany misy soa
''Zaho ho Anao ;
Raha mivily, ahitsio,
Fa malemy re !
Eny, osa ka ampio,
''Zaho hody re!
Zaho hody re !
Zaho hody re !
Hody Aminao, ry Ray,
Ho mandrakizay.}
', '#3-XIV', '#3') /end

insert into song values (52621, '262. Raha afaka aho ety', NULL, NULL, '
{1.
Raha afaka aho ety
Ka hohitako ary
Ilay Mpamonjy mamiko,
Any aho afa-po.}

{FIV.
Afa-po dia afa-po,
Fa Jeso malalako,
Hiaraha-monina:
Any aho sambatra!}

{2.
Raha ny ota tapaka,
ka ny ady tapitra,
Lanitra honenako:
Any aho afa-po,
[FIV.]}

{3, Raha hitafiako
Zany tena tsy ho lo,
Endre ! ny hasambarako,
Fa izaho afa-po.
[FIV.]}

{4.
Raha hitako ary
Ilay malalako taty,
Endre! ny fifaliako :
Zay vao tena afa-po.,
[FIV.]}

{5.
Tompo ô! ny tavanao
No iriko hatrizao ;
Aoka mba ho hitako,
Dia ao izaho afa-po.
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52631, '263. Ry mpivahiny malala', NULL, NULL, '
{1.
Ry mpivahiny malala,
Mba zava-tsarotra atao
Ny mahafoy tany hita
Sy izao fiainana izao,
Ary izay hampangina
Ny ratsy efa natao
Mba hitady indray kosa
Izay hiadanan''ny fo,
Ao aloha ao ilay
Tany katsahinao.}

{2.
E! ry malemy sy resy,
Ana sy reraka koa,
Avy mamaky ny toby,
Ho feo manaitra ny fo
Tao ny devoly mpanimba
Izay nanangoly tokoa,
Ny sitrapo manadala
Ny loza toa lalao.
[FIV.]}

{3.
Ry mpivahiny mahery
Efa nandresy tokoa,
Nefa ny zavatra maro,
Nisakan-dàlana anao,
Tahotra sy vava ratsy,
Ny alahelo manjo,
Ary ny fanahy nisento,
Samy nandrava avokoa
[FIV.]}

{4.
O! mahereza, sakaiza,
Fa afak''ady izao;
Fa tsy mba lavitra intsony,
Jerosaléma vaovao,
Izay fiadanana ampy,
Fiainana tsisy manjo;
A ny anjely mihira
Ny fitiavan''i Jesosy Tompo.
[FIV.]}

{5.
Ry mpivahiny malala,
Mba mandrosoa ho ary,
Fa he ! izany hira sy hoby
Izay fampandrosoana
Izay tampandrosoana hoe:
Ry mpivahiny midira
Ao an-dapan''ny Ray,
Ho tapi-javatra irina,
Velona mandrakizay.
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52641, '264. Ry mpivahiny àna', NULL, NULL, '
{1.
Ry mpivahiny àna,
Mba faingàna, mitsangàna,
Tsy eto no tanàna:
Andrandrao ny lanitra!
Andrandrao fa ery ny lanitra
Indro ao ny Tompo soa,
''Zay manasa anao hankao.}

{2.
Tazânonao Kanàna:
Volamena ny tanàna;
Ao, feno hazavana,
Lapa soa Sy masina;
Indro ao ny Lapa soa sy masina;
Indro ao ny Lapa soa,
Vita hovantaninao.}

{3.
Ny eto mampahory;
Maro no manorisory,
Ka aza mba matory:
Mba jereo ny tany soa.
Mba jereo fa ery ny tany soa!
Fa ery ny tany soa,
''Zay fonenana vaovao.}

{4.
He ! ''zany hira tsara,
Eo alohan''ny Mpitsara,
''Njay reko ny farara,
Mba henoy ny hoby e!
Mba henoy izany feo mihoby e!
Feon''ny marobe mihoby
Mankalaza ny Mpanao.}

{5.
Ny Tompo izao miantso
Renao va ny feo manako
Ka aza mba miato.
Mba avia ry havako,
Mba avia aty izao ry havako;
Avia aly ry mpivahiny,
Mba ho ata-tsasatra.}
', '#3-XIV', '#3') /end

insert into song values (52651, '265. Ny lanitry ny lanitra', NULL, NULL, '
{- "Ny lanitry ny lanitra
Tsy omby Anao.
Ka mainka ve ity trano
nataon-tànana
Ny lanitry ny lanitra
tsy omby Anao"}

{Ny lanitry ny lanftra,
Tsy omby Anao.}

{- "Ry Jerosalema!
Endre, ity voninahitrao
Ny sandrin''i Jehovah hiaro anao.
Matokia, matokia !
Ry taranak''Isiraely"}

{- "Eny, matokia ry Jerosalema,
Asandrato ny lohanao
Ka jereo ny lanitra !
Hosana, hosana ho Anao
Ry Tompo! Haleloia,
Amena"}
', '#3-XIV', '#3') /end

insert into song values (52661, '266. Ny hiran''ireo Kerobima', NULL, NULL, '
{Ny hiran''ireo Kerobima,
Manako ao an-danitra soa
Ny hobin''ireo Serafima,
Manetsika anay eto koa.}

{- "Mitaona ny fanahinay
Hidera ani Jehovah Ray
Mitaona ny fanahinay
Hidera an''i Jehovah Ray,
Miara midera ny Ray.}

{Ny hiran''ny masina indrindra,
Venteso sy ankalazao,
Milamina sady mirindra
Manolotra izao andro izao.}

{- "Ampiana fitiavana indray
Hitantana mandrakizay
Ampiana fitiavana indray
Hitantana mandrakizay.
Ny masina ary am-paradisa,
Miventy ny hira madio;
Ny mino tavela eto sisa,
Misantatra izany eto koa."}

{- "Akaro ny feo hifandray
Hidera ani Jehovah Ray
Akaro ny feo hifandray
Hidera ani Jehovah Ray,
Miara-midera ny Ray.}
', '#3-XIV', '#3') /end

insert into song values (52671, '267. Jerosalema an-danitra', NULL, NULL, '
{1.
Jerosalema an-danitra,
Tanâna soa sy masina,
Ao no misy tsara izay maharitra,
Ho an''ny mino sy ny marina :
Ny aty mandalo sy mihelina
Ka hilaozantsika tsy hitoetra,}

{FIV.
Sambatra sy tretrika tokoa
Izay tafiditra ho ao,
Ka irio Jeso làlana
Mba hitarika ariao;
Fahamarinana sy fahamasinana,
Mba tadiavonao hatrizao,
Mba tratrarona hatrizao,
Dia handray anjara ao hianao.}

{2.
Jerosalema an-danitra,
Fonenam-pahazavana be,
Ao Jesosy Tompomboninahitra
Te-handray ny afak''ady re!
Ny ety be ota sy be loza koa,
Ka dia feno sento tsy ahitana soa.
[FIV.]}

{3.
Jerosalema an-danitra,
No mba irionao izao,
Tany mahafaly mahafinaritra,
Tsy mba misy ali-maizina
Fa Jesosy ihany ''zay manjaka ao
No fahazavany mandrakizay doria.
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52681, '268. He tanim-pahazavana be', NULL, NULL, '
{- He tanim-pahazavana be
Ao an-koatr''izay tsy hitanao ;
Tsy misy malomaloka,
Na takona tsy tâzana}

{- "Tsy misy masoandro ao,
Na alina manefitra,
Na taona sy volana
Ny tavan''Andriamanitra,
Mbamin''ny voninahiny,
Mazava maharitra}

{- "Tsy hisy zay tsy hanana,
zay maniry fatratra;
Tsy hita ny hamoizam-po
Na ny hadiram-po,
Na ny zavatra manjo"}

{- "He, sambatra sy tretrika
Izay tafiditra ao, faly tokoa.
Tsy misy alahelo ao,
Fonenam-piadanana"}

{- Tsy misy masoandro ao,
Na alina manefitra,
Na taona sy volana :
Ny tavan''Andriamanitra,
Mbamin''ny voninahiny,
Mazava maharitra"}
', '#3-XIV', '#3') /end

insert into song values (52691, '269. Ao Edena ambony no fonenako', NULL, NULL, '
{1.
Ao Edena ambony no fonenako,
Any no hodiako raha miala ety;
Vetivety monja dia ho hitako,
Ilay tanàna tsara mamiko
ery.}

{FIV.
Koa aza kivy,
Ry fanahy ô!
Fa mba andrandrao
Ny tanindrazanao !}

{2.
Ao no hahitako ny malalako
Efa nodi-mandry elabe izay,
Ka ho miramirana ny tavako,
Fa hiara-paly ao tokoa izahay.
[FIV.]}

{3.
Ao koa Jesosy ''Lay Mpamonjiko
Faly hampandroso ahy olony,
Ka handray ny satroboninahitra
Fanomezan-tsoa izay hatolony.
[FIV.]}

{4.
Koa tsy ahoako ny manjo ety,
Zavatra mandalo avokoa  ireo,
Tanin-dranomaso rahateo ety
Vetivety aho dia ho afaka eo.
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52701, '270. Henoy ny hiran-tserafima', NULL, NULL, '
{- "Henoy ny hiran-tserafima
Miredona ao an-danitra,
Manatrika Ilay Masina,
Dia Jeso Tompon-danitra.}

{- "Asandrato ny fiderana
Ka misaora an''Andriamanitra,
Ka samia manandram-peo
Haleloia, Haleloia.
Amena !}

{Ilay Mpanjakan-danitra,
Tompo tena Masina.
Endre! ny hafaliako, Tompo !
Raha tonga any Aminao
Manatrika ilay tavanao,
Dia Jeso Tompon-danitra.}

{- "Henoy ny hiran-tserafima
Miredona ao an-danitra,
Manatrika Ilay Masina,
Dia Jeso Tompon-danitra.}
', '#3-XIV', '#3') /end

insert into song values (52711, '271. A ! inona no anjely', NULL, NULL, '
{A ! inona no anjely
A ! ny anjely
Fanahy
Anjely, fanahy madio indrindra re !
A ! ny anjely oa
Fanahy oa
Fanahy madio indrindra!}

{- "A ! toetra manao ahoana indray re
No naharian''Andriamanitra reo anjely ireo"}

{- "A ! toetra Masina,
Sambatra,
No naharian''Andriamanitra reo anjely ireo"}

{- "Oa ! a! toetra masina
No ananany;
Oa !  oa ! ny fahasambarana
A ! no anjarany"
A ! ny anjely
Fanahy
Anjely fanahy madio indrindra re !
A ! ny anjely oa
Fanahy oa
Fanahy madio indriridra !}
', '#3-XIV', '#3') /end

insert into song values (52721, '272. He, tazako Jerosalema vaovao', NULL, NULL, '
{1.
He, tazako Jerosalema vaovao,
Endrey, mahafinaritra !
Mazava lehibe no hita ao,
Manelatselatra !
Ny hazon''aina mahasitrana;
Ny raviny dia tsy hihintsana.}

{FIV.
Mifaly aho, raha mijery,
Fa kely sisa ny lalan-tery,
Dia tazako Jerosalama vaovao"}

{2.
He, tazako Jerosalema vaovao,
Endrey, mahafinaritra !
Mazava lehibe no hita ao.
Manelatselatra !
Tsy tapitra ny fifaliana,
Fa velona ny fihobiana.
[FIV.]}
', '#3-XIV', '#3') /end

insert into song values (52731, '273. Ambatoreny tanàna malala', NULL, NULL, '
{1.
Ambatoreny tanàna malala,
Anatin''ny efitra be mpahalala
Aono akany nipoiran''ny aina.
Ambatoreny tsy maintsy hajaina !}

{2.
Maro ny olon''ity Gasikara,
Mahazo ny tsiron''ny Filazantsara,
Miezaka mafy tokoa izy ireny
Mamangy ny Tobin''Ambatoreny.}

{3.
Babon''ny Tompo ny olona tao,
Baiboly mantsy no zavapanao,
Baikon''ny Tompo nanova ny fomba,
Be no voavonjy ny olon''ny tromba.}

{4.
Asa sy vavaka misy finoana,
Ary fitiavana tsy mba mirona.
Aina mamelona ny olo-mifoha,
Avy ny Nosy mamangy azy koa.}

{5.
Telo ambin''ny folo no tao tahizay,
Tena nikambana ho fo anankiray,
Tsy mba nitoko na niavakavaka,
Tanteraka tsara ny fon''ny mpivavaka.}

{6.
O! ry vahiny mamangy ny Toby,
O ! mba anàno ny fo tehihoby,
Ovay re ny fomba izay mahazatra,
Oneno ny fiainana soa tonga fatra.}

{7.
Rainisoalambo no rain''ny mino,
Rain''ny mpifoha tsy mety ho hadino;
Rehefa mazava ny làlankombana,
Roso namindra azy ao Soatànana.}

{8.
Eritrereto, ry olo-mifoha,
Esory ny sento manindry ny fo,
Eo ny Nosy tsaroana fa voky,
Eny, Jesosy no Hery sy Toky !}

{9.
Ny fo sy ny saina sokafy handray
Ny herin''ny Zanaka ary ny Ray
Ny herin''ny afo ataon''ny Fanahy,
No aoka ho azon''ny vavy sy lahy.}

{10.
Izao no hafatra averiko etoana,
Izay ho raketin''ny manampinoana,
Ilay famonjena, zareo, no anàno,
Izay hakiviana ario sy sakano}
', '#3-XV', '#3') /end

insert into song values (52741, '274. SOATANANA no tiako hambara', NULL, NULL, '
{1.
SOATANANA no tiako hambara,
Soa sady mahatamana no tsara
Sady itoriana ny Filazantsara;
Sambatra isika mahazo anjara !}

{2.
O! mba jereo ity Fiangonana ity,
O mihevera ry mponina aty.
Ity vohitra kely mitokana irery,
Kanefa malaza sy maro mpijery.}

{3.
Anivon''ny lemaka sy ny havoana,
Anjara fonenan''ireo tafahaona,
Alofan''ny ala kininina soa,
Arovan''ny Tompo ao andanitra koa.}

{4.
Tafangona eto ny olona maro,
Tanala sy Hova, indreo mifangaro,
Torina ny tenin''i Jeso Mpiaro,
Totina fa he ! famonjena ny maro.}

{5.
Atao antso ave ny FILAZANTSARA,
Antsoina hianao mba hahazo anjara,
Akaiky tokoa izao ny Tompo Mpitsara,
Aoka hianao mba hihevitra tsara.}

{6.
Na inona zava-misakana anao,
Na zovy mikasa haningotra anao,
Ny Tompo zao te-hampiditra anao,
Ny tànany roa no mananty anao.}

{7.
Atoizo ny zava-maningotra eto,
Ataovy veloma ireo mampisento,
Afoizo fa tena mandalo tokoa,
deha re mba hody ao an-danitra soa !}

{8.
Nidina teto Jesosy Mpamonjy,
Nidina teto nitondra ny vonjy
Naterak''ilay zazavavy madio
Nanavotra anao re ka mba ifalio.}

{9.
Ataoko veloma ny eto an-tany
Ataoko veloma fa mampitomany,
Anjaran''ny mino ny lanitra ary,
Antsika mpanompo izay ory tety.}
', '#3-XV', '#3') /end

insert into song values (52751, '275. Misy re tanàna iray', NULL, NULL, '
{1.
Misy re tanàna iray,
Maha te-hamisavisa
Fa voahosotry ny Ray,
Mba ho sarim-paradisa.
O ! tsy iza re akory,
Fa i SOATANANA izay
Hany vala mahavory,
Ny olona erambazantany.
He ! tanâna kely ihany
Ao anaty bozaka ao
Neta ra ka sesilany,
Ny vahoaka izay mankao}

{- "Miorina eto Betsileo,
Vala kely toa mangina,
Nefa dia manehoeho,
Manan-daza mahaliana"}

{2.
Nefa tsy mba vola akory
No haren''i SOATANANA.
F''izy kosa no mpitory
Ny idealin Kanàna.
Vohi-tsoa, tsara hasina,
Fa mitaiza ireo mpitory,
''Zay mitondra teny masina,
Mampifaly ny olon''ory.
Didin''ny Filazantsara
No iainana ao an-Toby
Foana ny tsinjarazara,
Fa miray sy miara-mihoby.}

{- "Tontosaina lalandava,
Fivavahan-tsamy irery,
Ka ny saina miha-mazava
Ny finoana mihamahery}

{3.
Ny Fanahin''i Jehovah,
No mibaiko ny safidy;.
Na Tanindrana, na Hova,
Samy mankato adidy.
SOATANANA no manala
Irony efitry ny antoko,
Ka indray mikambam-bala
Ny olon-tsamy hafa foko
O ! veloma sy masina
Re hianao ry SOATANANA
Ny asanao dia hahazina
Ny fararanon''i Kanàna.}

{- "Ry Jehovah Tompon''asa
Mitahia ny Tobinao,
Ka hihoatra noho ny lasa
Re ny vokatra ho Anao".}
', '#3-XV', '#3') /end

insert into song values (52761, '276. Soatanàna', NULL, NULL, '
{1.
Soatanàna,
Tany nahabe anay;
Ambatoreny,
Tany maminay,
Ka hatao ahoana
''zay ho fanadino anao ?
Zionanao Jehovah,
Eny maminay,}

{FIV.
"Soatanàna,
Tsy hadinoinay hianao;
Ambatoreny,
Eny maminay"}

{2.
Jesosy Tompo,
Indreto ny voafidinao
Avy hanompo
Sy hanoa Anao :
Sambatra sy faly
Ny miaraka Aminao;
Afaka ahiahy,
Fa tantananao.
[FIV.]}

{3.
Tompo ô ! Tsarovy,
''Tony Toby masinao;
''Zahay arovy,
Tano ho Anao,
Fa fiainan-tsoa
No atolotrao anay;
Fiadanana koa,
Rahatrizay.}
[FIV.]
', '#3-XV', '#3') /end

insert into song values (52771, '277. Entonareo re ny fahafolon-karena rehetra', NULL, NULL, '
{- "Entonareo re ny fahafolon-karena rehetra
Entonareo re ho ao amin''ny trano firaketako
Mba hasian-kanina ao an-tranoko" -}

{- "Entonareo ny fahafolon-karena rehetra
Entonareo re ho ao amin''ny trano firaketako
Mba hasian-kanina ao an-tranoko" -}

{- "Ary izahao toetra amin''izany Aho,
Hoy Jehovah, Tompon''ny maro
Raha tsy hovohàko ny varavaran''ny lanitra,
Ny varavaran''ny lanitra ho anareo" -}

{- "Ka hampidinako fitahiana
Manana amby ampy ho anareo.
Ary hasiako teny mafy ny valala
Noho ny aminareo" -}

{- "Ka tsy hanimba ny vokatry ny tany instony ireny,
Ka dia hahavanom-boa ny voalobokareo any an-tsaha,
Hoy Jehovah" -}

{- "Ary ianareo hataon''ny firenena rehetra
Hoe: sambatra
Fa ho tany mahafinaritra ianareo,
Hoy Jehovah, Tompon''ny maro" -}

{- "Ary ianareo hataon''ny firenena rehetra
Hoe tany mahafinaritra ianareo,
Hoy Jehovah, Tompon''ny maro" -}

{- "Ary ianareo hataon''ny firenena rehetra
Hoe: sambatra
Fa ho tany mahafinaritra ianareo,
Hoy Jehovah, Tompon''ny maro" -}
', '#3-XVI-1', '#3') /end

insert into song values (52781, '278. Fa mahasoa kokoa ny manome', NULL, NULL, '
{- "Fa mahasoa kokoa ny manome
Noho ny mandray izay atolotra;
Fa misy fitahiana lehibe
Ho an''izay tsy ketraka
Amin''ny fanaovan-tsoa ny namana" -}

{FIV.
"Tsy very foana izay atolotrao,
Fa voatahiry ao an-danitra.
Jehovah no mahatsiaro fatratra ny asanao"}

{- "Hotahiany ianao, hohasoaviny tokoa:
Hotahiny ianao raha miditra,
Hofenoina ny tranom-barinao;
Hohamaroina ny biby:
Hampitomboina avo heny" -
[FIV.]}
', '#3-XVI-1', '#3') /end

insert into song values (52791, '279. Manompoa ny Zanahary Ilay nanao antsika', NULL, NULL, '
{- "Manompoa ny Zanahary Ilay nanao antsika
Tiana Izy, tsy hanary,
Tsy tiana, lozantsika" -}

{- "He! Ny fahasoavana azo,
Vokatra ny tanintsika,
Ka misaotra azy Tompo,
Noho ny soa izay nomeny antsika;
Ka dia tsy mba ho ela loatra,
Ka aza mba sondriana" -}

{- "Mba tsarovy ry sakaiza
Ny zoky sy ny zandry,
Sy ny ray sy ny reny nitaiza efa nodi-mandry" -}

{- "Tsy very foana izay atolotrao,
Fa voatahiry ao an-danitra.
Jehovah no mahatsiaro fatratra ny asanao" -}
', '#3-XVI-1', '#3') /end

insert into song values (52801, '280. Hidera Anao ny firenena Andriamanitra ô', NULL, NULL, '
{- "Hidera Anao ny firenena Andriamanitra ô
Eny, hidera Anao!
Hidera Anao ny firenena rehetra
Ny firenena rehetra" -}

{- "Ny tany dia efa nahavokatra
Mitahy antsika Andriamanitra:
Andriamanitra,  dia Andriamanitsika" -}

{- "Mitahy antsika Andriamanitra,
Ary hatahotra Azy ny vazan-tany rehetra;
Mitahy antsika Andriamanitra,
Ary hatahotra Azy ny vazan-tany rehetra" -}
', '#3-XVI-1', '#3') /end

insert into song values (52811, '281. Andriamanitra ô! Ry Raiko ô', NULL, NULL, '
{1.
Andriamanitra ô! Ry Raiko ô
Indreo ny zanakao, ry Raiko ô
''zay mitondra hanitra mba homena ho Anao,
Ka hatao fanatitra eto anatrehanao,
Ry Raiko ô!
Ny mpanota ilazao sy ireo maro tsy mandre,
Aoka mba harovanao
''zay misakana azy re, ry Raiko ô!}

{2.
Na kely aza re, ry Raiko ô
Mba hamasino e! ry Raiko ô
Ho Anao ny vokatra
Sy ny iraka hanao
Ka izay hatolotray aoka tsy holavinao, ry Raiko ô!
''reto olona mpanao dia samy hafanao
Hahavita ny asanao,
Efa nametrahanao, ry Raiko ô!}

{3.
Ny Fifohazana, ry Raiko ô
Tahio ho fantatray, ry Raiko ô
Indreo ny irakao
Sy ireo mpiasa marobe,
Aoka tsy mba hety fay
Hahavita soa be, ry Raiko ô!
Samy faly hampandre
Ka hiantso mafy:
Izaho no hiasa re, ka iraho aho e! ry Raiko ô!}

{4.
Fanahy izay hankao, ry Raiko ô
Tariho mba hanao, ry Raiko ô
Aoka samy hiaritra
''zato hatrotrahana,
Hahafoy hanolotra
vola sy fananana, ry Raiko ô!
Ka hataonay tohatra raha hody mba ho ao,
Lafatra hiakatra ao an-danitra ao Aminao, ry Raiko ô}
', '#3-XVI-1', '#3') /end

insert into song values (52821, '282. Fikamabanana no hery!', NULL, NULL, '
{1.
Fikamabanana no hery!
Indreo, firaisan-kina tsara
Hampiverina ny very,
Mba ho tsara fiafara.}

{FIV.
Ny fanaovana asa soa,
Toy ny fofo-manitra,
Sady tena asa soa,
Ren''Andriamanitra}

{2.
Asan-tànan-dehilahy,
Herim-pon''ny vehivavy,
Ka velomin''ny Fanahy,
No milahatra eto avy.
[FIV.]}

{3.
O! ry Zana-Janahary,
Fototry ny zava-tsoa,
Raisonao ireto,
Ary hamasino hahasoa!
[FIV.]}
', '#3-XVI-1', '#3') /end

insert into song values (52831, '283. Tao an-tany karankaina', NULL, NULL, '
{1.
Tao an-tany karankaina
Nisy olo-maro be,
Tsy mba nampodina maina
Tao Jesosy nanome}

{2.
Na dia mofo kely aza
Tonga nahavoky ireo,
Fa nitombo mahagaga,
Samy faly izy ireo}

{3.
Fa ny herinao ry Jeso
Izay namonjy fahizay
No miasa mafy lalandava
Hahavoky soa anay}

{4.
Dia tahio ny fambolena
Mba ho vokatra tokoa
Miantrà ny firenena
Tsy ho diso zava-tsoa}

{5.
Na dia voky fatratra aza
Mora reraka izahay
Ao ambony ny mateza
Iankinan''ny ainay}
', '#3-XVI-1', '#3') /end

insert into song values (52841, '284. He! Taona volamena', NULL, NULL, '
{1.
He! Taona volamena
Ny andron''ny jobiily;
Ny tenim-pamonjena
Miantso ny mivily;
Mananatra ny saina,
Manainga ny fanahy,
Mamelona ny maina,
Hahazo ranon''aina.}

{FIV.
Na be mpisakana aza
Ny teny mahasoa,
Ny dera aman-daza
Ho Anao, ry Tompo soa
Izay mandantolanto
Ny andro izay toa zato
Haleloia! Hosana! No asandratray!}

{2.
Tsy mahavolovolo
Hanatrika ny fety,
Fa be ny fahavalo
Mikendry sy mifetsy;
Ny masony mahita
Ny soa efa vita;
Mahery Ilay Mpahita
Ka maro no tafita
[FIV.]}

{3.
Jesosy mitahiry
Ny tena masom-boly;
Miàva ny maniry
Mandinika ny voly;
Milatsaka maraina
Ka tsy mitandro ny aina,
Manondraka ny maina
Hahazo ranon''aina
[FIV.]}

{4.
Malalaka ny saha
Mirava-boninkazo
Avia mba hizaha!
Tondrahy ny malazo;
Mandroso lalandava
Ny soa azonay;
Handroso be ny asa,
Hidera Anao ''zahay.
[FIV.]}

{5.
Atolotray, ry Tompo
Ny tena sy fanahy;
Havaozy ho mpanompo
Marisika ny sahy,
Handray ny adidy
Natolo-Janahary;
Harahina ny teny
Ny tenin''ny Mpahary
[FIV.]}

{6.
Arovy, ry Jesosy,
Ny voa tafavory,
Fa be mpanosihosy
Ny asanay mpitory;
Tolory fahendrena
Mba hahatsiaro tena,
Handray ny famonjena,
Izay lova tsy mihena.
[FIV.]}
', '#3-XVI-2', '#3') /end

insert into song values (52851, '285. Andro iray toa zato', NULL, NULL, '
{1.
Andro iray toa zato,
Izao andro anio izao,
Tsy hay hadinoina
Ry mpifoha ô!
Nanomezan''ny Tompo ny fahaleovatena
Hatao tsangam-bato anio izao.}

{FIV.
" mifalia ry mpifoha ô!
Derao Jeso Tompo,
Ilay tia anao
Asandrato ny hira hoe: Mifalia!
Mifalia, derao Jesoa" -}

{2.
Hotsarovana eto anio
Ireo Raiamandreny,
Izay nitondra izao
Fifohazana izao,
Izay nifandimby hatrany hatrany:
Tsy hay hadinoina ry mpifoha ô
[FIV.]}

{3.
Dada RAINISOALAMBO
No niandohany
Sy dada RAINITIARAY
Ary dada RAGOSTINA
Ary koa dada RAPETERA
Sy dada RAJOELINA:
Tsy hay hadinoina, ry mpifoha ô!
[FIV.]}

{4.
Taona maro nifandimby
Izay nitondran''ireo
Ary samy efa lasa
Nodimandry avokoa,
Fa dada RABENJA
No nisolo azy koa,
Izay Raiamandreny, antsika mpifoha
[FIV.]}

{5.
Misaotra anao Jehovah,
Ray sy Zanaka
Sy ny Fanahy Masina,
Izay nanome anay
Ny fahaleovantena
Hatao tsangam-bato anio izao
[FIV.]}
', '#3-XVI-2', '#3') /end

insert into song values (52861, '286. Andro soa sy tena kanto', NULL, NULL, '
{1.
Andro soa sy tena kanto,
Andro fety anio izao;
Fifaliana maharavo
No manetsika ny fo;
Haravoana volamena,
Nandrandraina ela be,
Ka tsy foana, very maina,
Fa ao ny Tompo lehibe.}

{FIV.
" Koa misaotra Anao ''zahay,
Tompo Andriamanitra ô!
Nanome ny soa Izy,
Ka enga anie ho voatahiry" -}

{2.
''Zao no andro iray toa zato,
Fetim-pifaliana anio,
Ka samia mandantolanto
''Tony androm-pamonjena.
O! hobio, ry vahoaka
Jeso Ilay Mpanjakantsika;
O! avia mba hitsaoka
Sy hanoa ny Tompo soa
[FIV.]}

{3.
Ry vahoaka manoloana,
Vory maro sesehena
Isika re, izay velon''aina,
Izay manatrika etoana,
Falifaly miran-tava,
Samy miara-mankalaza:
Raiso, Didy vaovao,
Aoka ho vonona hanao!
[FIV.]}
', '#3-XVI-2', '#3') /end

insert into song values (52871, '287. Eo am-pelatànanao, Jesosy ô, ny taninay', NULL, NULL, '
{1.
Eo am-pelatànanao, Jesosy ô, ny taninay
Ka arovy re mba handry fahizay
''zay rehetra monina eo,
Tariho mba hifankatia
Hahay miara-dia.}

{FIV.
Ny tànanao Jesoa, irinay mba ho ao;
Ny eo am-pelatànanao
Mijoro, tsy mba fefika
Ke tena tretrika.}

{2.
Eo am-pelatànanao Jesosy, ny mpitondra anay
Ka tolory saina hendry sy mahay,
Mba hitsimbina ny zo,
Hiaro ny fanananay,
Ka hiadananay.
[FIV.]}

{3.
Eo am-pelatànanao, Jesosy, ny Fiangonanay,
Ka fohazy hanam-pombonana
Hampandroso ny Sekoly sy hanao Fiangonana:
Hiombon''asa soa.
[FIV.]}

{4.
Ka eo am-pelatànanao, Jesosy, ny taranakay,
Ho toy ny fahazazanao fahizay,
Ka hitombo saina sy fanahy hamololona,
Ho tena olona.
[FIV.]}
', '#3-XVI-3', '#3') /end

insert into song values (52881, '288. Herinao Jeso no manafaka', NULL, NULL, '
{1.
Herinao Jeso no manafaka
Ny fatorana sy ny gadra koa
Fahafahana no manafaka
Tanindrazantsika mba ho sambatra.}

{FIV.
"Asandrato ny faneva!
Fa Jesosy no Mpanjaka eran-tany" -
Haleloia
Fa  Jesosy no Mpanjaka eran-tany.}

{2.
Herinao Jeso, mampitovy zo
Izao tontolo izao, tena iraitampo
Fitovian-tena mahasambatra
Azo taminao, ry Jesoa Tompo soa.
[FIV.]}

{3.
Herinao, Jeso, mampifankatia,
Ho mpirahalahy tena irai-tampo.
Izay mifankatia dia ho sambatra :
Tena iray tokoa ny tena sy ny fo.
[FIV.]}

{4.
Ny fitiavanao ry Jesoa Tompo soa
Ranovelona izay mahafa-po,
Loharano soa tsy mba ritra koa.
Dera, haleloia, hosana ho Anao.
[FIV.]}
', '#3-XVI-3', '#3') /end

insert into song values (52891, '289. O! mba saintsainonao izao', NULL, NULL, '
{1.
O! mba saintsainonao izao,
Ry tafavory marobe,
Ny soa efa azonao
Tamin''ny Tompo be fitia,
Fa tsy mba nisy andro koa
izay tsy nandraisanao ny soa,}

{FIV.
Ka asandrato mafy ny feo:
- "Haleloia, fa fitahiana no azonay izao,
Hosana! Sambatra isika re" - }

{2.
Ankehitriny eto izao
Dia eo am-pelatànanao re
Ny soa efa azonao,
Mamirapiratra sy soa:
Indro ny taona vaovao!
Izay raisinao ankehitriny,
[FIV.]}
', '#3-XVI-4', '#3') /end

insert into song values (52901, '290. Mahagaga ny sain''I Jehovah', NULL, NULL, '
{1.
Mahagaga ny sain''I Jehovah,
Fa voalahatra tsara ny olony;
Ny antokon''ny zaza dia hita,
Fa miombo isan-taona.}

{FIV.
"Misaora, misaora an''I Jehovah!
Zarantsika fa velon''aina;
Ka tsarovy isan''andro,
Taon-dava,
Fa mendrika hohajaina"-}

{2.
Ravinkazo ririnina ny zaza;
Ny tanora dia tany mandondona,
Fa ny antitra hazo amoron-tevana:
Samy ho antsoin''ny Tompony.
[FIV.]}

{3.
Ao ny antitra izay efa zokiny,
Maro taona izay efa niainana,
Mbola velona tsy mba nantsoiny,
Ka sondriam-piadanana.
[FIV.]}

{4.
Hitako ny tanora maronjana,
Fa mavitrika sady matanjaka,
Fa nomena ny hery eo an-tànany
Tsy mba misy voavaka.
[FIV.]}
', '#3-XVI-4', '#3') /end

insert into song values (52911, '291. Arahaba, ry sakaiza', NULL, NULL, '
{1.
Arahaba, ry sakaiza,
Tafahaona soa aman-tsara
- "amin-dreny izay nitaiza sy ny ray mpikarakara.
Mifalia, mifalia! Andro soa tokoa ny anio" -}

{2.
''Ndreto eto koa ny maro
Samy velon''aina avy,
- "tafahaona, tafaharo, samy tena mpianakavy.
Mifalia, mifalia! Andro soa tokoa ny anio" -}

{3.
Marobe no te hahita
Andro soa toy itony,
- "ny anjarany efa vita, tsy miverina intsony;
Mifalia, mifalia! Andro soa tokoa ny anio" -}

{4.
Aza mba misalasala,
Hampiseho fo mifaly,
- "fa izany no manala sentom-po tsy hampijaly,
Mifalia, mifalia! Andro soa tokoa ny anio" -}

{5.
Ry Jehovah Ray mahery,
Amindraonao fo izahay!
- "mba tsy hisy ho very fa harovanao tokoa.
Mifalia, mifalia! Andro soa tokoa ny anio" -}
', '#3-XVI-5', '#3') /end

insert into song values (52921, '292. Arahaba ry havan-drehetra', NULL, NULL, '
{1.
- "arahaba ry havan-drehetra izay vory eto ankehitriny,
Tsy mba sampona fa tafahaona velona''aina, ka dia arahabaina:
Arahaba, arahaba mba hiara-mifaly" -}

{2.
- "azafady, mba henoy kely: vita izay ny arahaba,
Ka atodiho kely re ny saina: azafady mba eritrereto
Andriamanitra nanao antsika, izay nitahy sy niaro,
Ka nanambina antsika ho soa" -}

{3.
- "aza mba misalasala re ianao,
Aza mba misalasala: manatona Azy izao,
Sao ity no toriteny fara-fanasana anao:
Miverena, miverena, ka modia ao am-balan''I Jesosy" -}
', '#3-XVI-5', '#3') /end

insert into song values (52931, '293. Arahaba re ry havanay', NULL, NULL, '
{1.
- "arahaba re ry havanay,
Arahaba ry sakaiza!
Tratra izao ny andro soa
Na tsy mendrika aza isika
Zara re fa tratra izao"}

{2.
- "O mihobia!
Endre, jereo fa misy namantsika be,
Izay nandao ity fiainana ity,
Fa isika kosa re!
tratra izao anio izao,
mifalia tokoa
ka misaora re ny Tomponao,
izay namindra fo anao,
ka tratry ny anio tokoa.
Mifalia tokoa, ry sakaiza, Mifalia tokoa" -}
', '#3-XVI-5', '#3') /end

insert into song values (52941, '294. Falifaly ny fonay', NULL, NULL, '
{1.
Falifaly ny fonay,
Raha mahita anareo,
Ry havanay malala,
''zay tonga aty aminay.
"arahaba! Arahaba! Tafahaona tsara indray"}

{2.
Andro fifaliana izao,
Ivoriantsika izao:
Fotoan-tsy sampona,
Fa tendrin-Janahary e!
"ary efa samy velon''aina, tafahaona tsara indray"}

{3.
Isaorana anue Jehovah,
Izay nitahy anareo,
Sy niaro anay taty,
Ka tafahaona tsara indray.
"ho Azy anie ny voninahitra ho mandrakizay"}
', '#3-XVI-5', '#3') /end

insert into song values (52951, '295. Mifalia, mihobia, zanaka sy ray', NULL, NULL, '
{1.
Mifalia, mihobia, zanaka sy ray,
Velon''aina mihaona indray!
Asandrato izany hira
Asandrato venteso mafy hoe:
"Isaoranay Ianao ry Ray fa nitahy;
Tratranay ity andro ity,
Tombon-tsoa lehibe,
Mihobia, mifalia
Velon''aina mihaona indray"}

{2.
Andro iray toa zato ifalianay
Fa ahitanay ny tavanareo;
Ravoravo izany sainay;
Feno hoby indray ny fonay
"Isaoranay Ianao ry Ray fa nitahy;
Tratranay ity andro ity,
Tombon-tsoa lehibe,
Mihobia, mifalia
Velon''aina mihaona indray"}
', '#3-XVI-5', '#3') /end

insert into song values (52961, '296. Ry havana rehetra tafavory!', NULL, NULL, '
{1.
Ry havana rehetra tafavory!
Nambinin''I Jehovah Tompo Soa,
Voasakana ny ratsy mampahory,
Afaka isika ka mifalia tokoa.}

{FIV.
He! Arahaba fa tafita tsara,
Tratra izao fotoana anio,
Arahaba no filazana azonay hambara
Ho anareo rehetra tadidio!}

{2.
Ry havanay mpamonjy fivoriana!
Faly tokoa ka dia manandra-peo
Fa azontsika izao ny fitahiana,
Ka arahaba no atao anareo
[FIV.]}

{3.
Jehovah ô! Arovy mba ho lava
Ny arahaba ifanaovana,
Ka samy ho miramiran-tava,
Raha tonga indray ny fotoana tendrin-dRay
[FIV.]}
', '#3-XVI-5', '#3') /end

insert into song values (52971, '297. Arahaba indrindra e!', NULL, NULL, '
{1.
Arahaba indrindra e!
Fa mifankahita indray,
Ry sakaiza marobe,
Tafasarak''ela izay;}

{FIV.
Mifalia eram-po;
Fa tsy sampon''ny manjo
Ka derao Jesosy tia
Izay nanambina ny dia}

{2.
Loza maro nahamay,
No nipaoka marobe,
Fa isika kosa indray,
Dia voaro tsara re!
[FIV.]}

{3.
Ry Jesosy Tomponay!
Tano eo anilanao;
Raha ho faty izahay
Dia ho velona Aminao,}

{Dera ho Anao ry Tompo
Fa nanavotra anay
Eny, dera, eram-po,
Raiso ho mandrakizay}
', '#3-XVI-5', '#3') /end

insert into song values (52981, '298. Ry tanora hava-malala ô', NULL, NULL, '
{1.
Ry tanora hava-malala ô
Tafasaraka ela be izao,
Ka izao dia tonga aty indray
Ka mihaona aminay}

{FIV.
Izao no firarianay henoy,
Venteso ny hiranao, henoy:
Arahaba indrindra ry havanay!
Ho mandrakizay, Amena.}

{2.
Na manora-baventy aza re,
''tony androm-piainantsika ety,tsy nopaohin''ny riandrano be,
Ka moa tsy hihoby ve?
[FIV.]}

{3.
Tsy mba kivy mihafy hatrety,
Fa misikina hery hatrery
Ka maniry mangetaheta
An''ity fotoana ity
[FIV.]}

{4.
Dia fankasitrahana anie
Sy fisaorana be no atao,
Mba ho entina ho an''izay manao
Ilay didy vaovao.
[FIV.]}

{5.
Hotahian''ny Tompo anie
Handrotsahana fitahiana be,
handrobona sy hamoa be
''ty fihaonantsika ity
[FIV.]}
', '#3-XVI-5', '#3') /end

insert into song values (52991, '299. Arahaba tafahaona', NULL, NULL, '
{1.
Arahaba tafahaona,
Ry tanora vory izao}

{FIV.
"mba hanao ny asa masina
Ho famonjena ny olona marobe
Arahaba tafahaona velona" -}

{2.
Andro maro tsy nihaona
He! Anio mikambana
[FIV.]}

{3.
Mifalia, miravoa,
Mazotoa, ry mino ô
[FIV.]}
', '#3-XVI-5', '#3') /end

insert into song values (53001, '300. Ry kristiana ô!', NULL, NULL, '
{1.
Ry kristiana ô!
Mifalia sy miravoa,
Fa ny Tompo Andriamanitsika,
Efa mampiseho ny fitia.
Ry kristiana ô!
Mifalia sy miravoa,
Fa nahazo fitahiana soa}

{2.
Mifalia, mihobia, miravoa,
manandràta feo,
Fa efa nohasoavin''Andriamanitra
Izao ny havantsika
Mifalia isika ry zareo}

{3.
Miramirana ny fonay
mirana ny fonay
ravo koa ny sainay;
falifaly koa,
mijery anao ry havanay.
Arahaba ry havanay, sady voahosotra
Arahaba re ry havanay}
', '#3-XVI-5', '#3') /end

insert into song values (53011, '301. Faly, ravo izahay', NULL, NULL, '
{1.
Faly, ravo izahay
Zay mihira eto izao
Nahatratra ny andro soa lehibe,
Ka tsy mionona ny fo,
Raha toa tsy manao
Hira soa ventesinay manao hoe:}

{FIV.
Arahaba
Tafahaona soa amantsara
Eto indray isika izao
Arahaba Tompo, no ventesinay
Mba ho Anao}

{2.
Tsy nampoizinay akory re!
Ny mba ho toy izao
Zava-maro isan''andro no misakana
Nefa , he! Narovanao
Ary koa novimbininao
Ka dia feno haravoana
[FIV.]}

{3.
Saotra, dera, haja, hery
Tompo, no omena Anao,
Ka mba raiso, raisonao re izao,
Ny fanahinay izao,
Ary koa ny tenanay,
Dia samy atolotray Anao avokoa
[FIV.]}
', '#3-XVI-5', '#3') /end

insert into song values (53021, '302. Mifalia, ry sakaiza !', NULL, NULL, '
{"Mifalia, ry sakaiza !
Fa nahazo fiangonana ;
Mifalia, ry sakaiza!
Velon''aina hianao
Mihirà sy mifalia,
Jeso Tompo no derao,
Zanahary no nitahy
Velon''aina hianao"}

{"Mahafaly !
Zao no andro mahafaly
Andro lehibe anio,
Dia hira mifamaly
No atao ankehitrio,
Zanahary no nitahy
Mifalia hianao!
Tonon-kira hoe: Hosana!
No atao anio zao"}

{Mifalia, ry sakaiza
Fa nahazo fiangonana;
Mifalia, ry sakaiza,
Velon''aina hianao}

{Mihirà sy mifalia,"
Jeso Tompo no derao
Zanahary no nitahy
 Velon''aina hianao}

{"Mifalia, He! Hosana
He! Hosana
He! Hosana ho Anao
 Ry Tompo ô!
He! Hosana
Ho Anao, ry Tompo ô!
Fihirana, fiderana}

{No asandratray
He ! Hosana
Ho Anao, ry Tompo ô!
Ho Anao anie ny haja
Raiso ho Anao izao"
Mifalia, ry sakaiza sns}

{"Dera, laza, hery haja
Ho Anao, Mpanjaka ô!
 Ho Anao anie ny haja,
Raiso ho Anao izao"
Mifalia ry sakaiza, sns}
', '#3-XVI-5', '#3') /end

insert into song values (53031, '303. Efa notahin''ny Tompo', NULL, NULL, '
{1.
Efa notahin''ny Tompo
Isika, na ny kely, na ny lehibe ;
Efa vonona hanompo,
Amim-pifaliam-be.}

{FIV.
Dia veloma re!
Ho mandra-pihaona koa.
Dia veloma re !
Irinay hianareo re !}

{2.
Fangataham-pitahiana
Ho an''ny raharaha sy ny dianareo,
Mba ho feno fifaliana,
Ny dia izay ataonareo
[FIV.]}

{3.
Fivoriana natao teto,
Mba ho santatry ny ataontsika ary,
Fa any tsy mba misy sento,
Ka irina mba hanjaka aty
[FIV.]}

{4.
Dia tongava soa amantsara
Hianareo rehetra tsy avahana
Ka sahia mba hanambara,
An''I Jeso Havanao!
[FIV.]}

{5.
Tadidio dia tadidio,
Izay rehetra efa renareo taty,
Ka ny fitiavana torio,
Baikon''I Jesosy Tia,
[FIV.]}
', '#3-XVI-6', '#3') /end

insert into song values (53041, '304. Hira veloma, no asandratray', NULL, NULL, '
{1.
Hira veloma, no asandratray,
Eny, masina no ventesinay,
Ho anareo izay nivory teto,
Fa efa vita ny asa natao.}

{FIV.
Veloma re ry havanay
Veloma re no sasandratray zanakareo.}

{2.
Eny, veloma tsy mandrakizay,
Fa antenaina hihaona indray,
Raha tsy misy intsony ety,
Dia antenaina hihaona ary,
[FIV.]}

{3.
Ary raha ory ka mafy manjo
Ka toa hamely ny famoizam-po,
Raiso Jesosy, hazony tokoa,
Fa izay manana Azy tsy mety ho voa.
[FIV.]}

{4.
Izao  re no anatra omena anareo
Eny, reketo mba ho lovanao
Raiso Jesosy, fikiro tokoa,
Na inon-kihatra, na inon-kanjo
[FIV.]}

{5.
O! ry Jesosy Mpiahy anay,
Miara-mangataka Anao izahay
Raiso ireto ho mpanomponao,
Eny, tezao sy karakarao
Jesosy ô! Ny herinao
Tano izahay ho
Mpanomponao mahafatra-po.}
', '#3-XVI-6', '#3') /end

insert into song values (53051, '305. Veloma re, ry havana rehetra !', NULL, NULL, '
{1.
Veloma re, ry havana rehetra !
Ny fitahian''I Jeso tsy misy fetra
No hampandositra ny mampisento.
Veloma dia veloma re izao,
Mba homba antsika
Anie ny Tompo soa
Velon''aina tafahaona indray,
Raha tonga indray ny
Andro toy izao.}

{2.
Veloma re, ry Havana malala!
Mba  tsy ho simban''olompankahala,
Na ho voafita-java-manadala.
Veloma dia veloma re izao,
Mba homba antsika
Anie ny Tompo soa
Velon''aina tafahaona indray,
Raha tonga indray ny
Andro toy izao.}

{3.
 Ny tànan''I Jehovah Zanahary
Hitondra antsika anie ka tsy hanary!
Ekeo ny fitondray lavorary
Veloma dia veloma re izao,
Mba homba antsika
Anie ny Tompo soa
Velon''aina tafahaona indray,
Raha tonga indray ny
Andro toy izao.}

{4.
Veloma indrindra n''drao tsy tafahaona
Raha tonga indray ny andro sy ny taona
Dia ao ambony isika no mihaona.
Veloma dia veloma re izao,
Mba homba antsika
Anie ny Tompo soa
Velon''aina tafahaona indray,
Raha tonga indray ny
Andro toy izao.}
', '#3-XVI-6', '#3') /end

insert into song values (53061, '306. Ao amin''ny tena izahay', NULL, NULL, '
{"Ao amin''ny tena izahay
Izahay izao ;
Fa aleo amin''I Kristy,
Ao amin''I Kristy
Fa ety no be fahotana,
Fa ao ambony tsy misy intsony;
Fa ety no be fahotana,
Fa ao ambony tsy misy intsony"}

{"Andro malaza izany
Lalan''ny mino aina;
Andro malaza izany.
Làlan''ny mino aina!
Andro malaza izany
Lalan''ny mino aina izany,
Ny tena avela ho lo,
Ny fanahy ho ravoravo"}

{"Veloma ny zavatra ety,
Veloma dia veloma,
Izay tsy mety maharitra!
Veloma, veloma ny zavatra ety;
Veloma dia veloma,
Izay tsy mety maharitra ;
Veloma izay tsy mety maharitra!
Ny lova tsy voaloto,
Tsy ho samba no nalaiko;
Ny lova tsy ho samba no nalaiko"}

{"O! aleo amin''I Kristy,
Fiainana mandrakizay!
 Jeso manoro làlana
Ny Azy tsy diso;
Tsy diso ny làlany"
Ny Azy tsy diso ny làlany}
', '#3-XVI-7', '#3') /end

insert into song values (53071, '307. Tsy mba misy zava-tsoa maharitra', NULL, NULL, '
{1.
"Tsy mba misy zava-tsoa maharitra
Eto amin''ity fiainana,
Fiainana mandalo : mandalo ity,
Mivadibadika, mora miova,
Tsy mateza, tsy maharitra ;
Fiaina-mandalo : mandalo ity"}

{2.
"Toy ny zavona tratry ny masoandro
tsy maharitra, fa mihelina
toy ny rahona tsofin-drivotra,
Ka mandositra sy miriotra,
Fa ny eto"}

{3.
"Na ny laza, na ny hery
Sy voninahitra ary bika,
Dia samy tsy haharitra:
Samy ho levona hody vovoka;
Tsy mifidy ny fahafatesana;
Tsy mampiera fa mianjera,
Tsy milaza, tsy mampiomana;
Tsy mba mangataka fa manapaka"}

{4.
"Ka moa ve mbola halaina
Tsy handositra hitady
Ny aina tsy mety ho rivotra,
Hitady ny aina tsy mety mandalo?
Tsy mety mandalo"}

{5.
"fa ny eto, toy ny zavona
Tratry ny masoandro
Tsy maharitra fa mihelina;
Toy ny rahona tsofin-drivotra
Ka mandositra sy miriotra"}
', '#3-XVI-7', '#3') /end

insert into song values (53081, '308. Ny fahasoavan''i Jesosy Kristy', NULL, NULL, '
{Ny fahasoavan''i Jesosy Kristy
Sy ny fitiavan''Andriamanitra
Ary ny firaisana amin''ny Fanahy Masina
Ho amintsika rehetra anie
Ho amintsika
Amena}
', '#3-XVI-8', '#3') /end

insert into song values (53091, '309. Voninahitra any amin''ny Avo Indrindra', NULL, NULL, '
{"Voninahitra any amin''ny
Avo Indrindra
Ho an''Andriamanitra"
"Fiadanana no tonga izao
Fiadanana mba ho antsika re"}

{"Voninahitra any amin''ny
 Avo indrindra
Ho an''Andriamanitra"}
', '#3-XVI-9', '#3') /end

insert into song values (53101, '310. Dera, laza, voninahitra', NULL, NULL, '
{"Dera, laza, voninahitra
Ho an''Andriamanitra !
Sy fiadanana,
Fankasitrahana
Ho an''ny olona"}

{"Voninahitra
Ary fankasitrahana
Ho an''ny olona"}
', '#3-XVI-9', '#3') /end

insert into song values (53111, '311. Saotra, dera, no atao', NULL, NULL, '
{1.
Saotra, dera, no atao
ry Andriamanitray,
izahay, ry Tompo ô !
feno fitahianao.}

{FIV.
Saotra no aterinay,
Ry Andriamanitray,
Tena sy fanahinay,
Ento ho Anao indray}

{2.
''Zay nianaranay taminao
Mba ho fandrosoanay
Hitozoanay Anao,
Amin''ny finona
[FIV.]}

{3.
Ry Mpanjaka tsara ô !
Ry Ampinga aronay,
Mba arovy ho Anao
''Zay rehetra anananay
[FIV.]}

{4.
Raha ho lavi-dalàna,
Mba tantano Tompo ô !
Raha ho lasa haingana,
Ento-mody Aminao
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53121, '312. Ny dera sy ny laza', NULL, NULL, '
{"Ny dera sy ny laza,
Ny voninahitra
Ho an''ny Tompo,
Tompo Masina sy To"}

{"Asandrato izao
Ny hirantsika soa,
Mba hanandratra  Azy
Mpanome ny soa lehibe"
Mba  hanandratra Azy
Mpanome ny soa lehibe}

{Torio ny famindram-pony
Mba hanandratra Azy
Mpanome ny soa lehibe}

{"Raisonao, ry Andriamanitra,
Fiderana atolotray!
 Aza lavinao, fa ento
Hiakatra any an-dapanao,
Ento ao an-dapanao
An-dapanao"}

{"Ry vahoaka, misaotra Azy
Miderà ny Tompo Tsitoha,
Ka misaora an''Andriamanitsika!
Eny ! misaora}

{Tsy mba misy hadinoinay
Ny fitahiana rehetra
Misaotra  Azy}
', '#3-XVI-9', '#3') /end

insert into song values (53131, '313. Isaoranay Zanahary', NULL, NULL, '
{Isaoranay Zanahary
Izy nitahy sy niaro
Izay matoky an''I Jeso
Velona tsy misy marary
Izay niakatra ho any an-danitra
Tsy mba misy milanja vatsy
Izay mahazo Paradisa
Velona mandrakizay}
', '#3-XVI-9', '#3') /end

insert into song values (53141, '314. Jesosy ro Hazomanga soa', NULL, NULL, '
{1.
Jesosy ro Hazomanga soa
Fitahiandrake firengeambe tokoa
Ny mino Azy olôn''afak''avoa
Ny devoly lavitr''azy moa}

{Ahiko Jeso bak''etoy
Ahiko jeso irak''eroy
Lavareake ngaiko izao
Manen''an''i Jeso anateko ao}

{2.
Baiboly ro havelona tokoa
Tano ka valio lah''ka mihemotse koa
Taomo ny longo mba ho afak''avoa
Fa Jeso ro hazomanga soa}

{Ahiko Jeso bak''etoy
Ahiko jeso irak''eroy
Lavareake ngaiko izao
Manen''an''i Jeso anateko ao}

{3.
Ndra inoino ny soa ety
Jeso tsy atakalo ndraka tsy azo afoe
Tsy ampirafesy amin-dratsy fiatao
Ny finoan''Azy anate ao}

{Ahiko Jeso bak''etoy
Ahiko jeso irak''eroy
Lavareake ngaiko izao
Manen''an''i Jeso anateko ao}
', '#3-XVI-9', '#3') /end

insert into song values (53151, '315. ''Ty tany itoerantsika ity', NULL, NULL, '
{1.
''Ty tany itoerantsika ity
Be loza be ady sy tafika eto
Ka miomàna hiala ety
Hamonjy  ny mandrakizay
Raha ianao no tamana ety
Dia hohitanao eto ny mafy manjo
Raha ianao no mody ary
Ny fiadanana mandrakizay
Izany no lova miandry anareo}

{2.
Izaho no Jeso Mpanjakanao
''Zay miadana ao an-dapan''ny mandrakizay
Vita ny ahy fa anao ny mandray
Ny soa nomeko anao
Raha ianao no mino tokoa
Dia raiso ny avotra omeko anao
Ny fiadanana mandrakizay
Izany no lova miandry anao re}

{3.
Mba sainonao ange ny nanjo anao
''Zay andevon''ny ota fahiny ianao
Jeso no tonga nanavotra anao
Anananao aina vaovao
Raha ianao no mino tokoa
Dia raiso ny avotra tao Kalvary
Ho fandresena mahery ho anao
Ny fiadanana mandrakizay
Izany no lova miandry anao re}

{4.
Veloma ry tany hafoiko ianao
Ilaozako eto ny sento manjo
Izaho hamonjy ny mandrakizay
An-danitra ambony ary
Veloma! Veloma indrindra ianao
Ry sitrapo manadala ety
Ny foko dia lasan''I Jeso velona
Ny fiadanana mandrakizay
Izany no lova}
', '#3-XVI-9', '#3') /end

insert into song values (53161, '316. Faingàna ajoroy ny faneva re', NULL, NULL, '
{1.
Faingàna ajoroy ny faneva re
Ry tanoran''i Jerosalema
Faingàna areheto ny jiro ô !
Hanazava ny lala-makao}

{FIV.
Faingàna, faingana
Faingàna
Faingàna ry namako ô
Andao é! Andao e!
Hanatona ny Tompo soa}

{2.
Ny dimy dia adala satria re
Tsy mba niomana solika soa
Nony tonga ilay hampakarina e!
Dia voahidy ny lala-mankao
[FIV.]}

{3.
Ny dimy dia Hendry satria re
Efa niomana solika soa
Tanteraka sy lavorary e!
''Zay rehetra kasainy atao
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53171, '317. Tsy taniko ety tsy taninao', NULL, NULL, '
{1.
Tsy taniko ety tsy taninao (in-2)
Jerosalema  vaovao no taninao
Aza sondriana ety (in-2)}

{2.
Indro Kanàna soa anilanao (in-2)
Kanàna lapa soa no taninao
Aza sondriana ety (in-2)}

{3.
Tsy taniko ety tsy taninao (in-2)
Holevona ity é! Tsy taninao
Aza sondriana ety (in-2)}

{4.
Mba raiso aho Tompo ho anilanao (in-2)
Tsy mendrika aho anefa mba mamelà
Aza sondriana ety (in-2)}

{5.
Tsy taniko ety tsy taninao (in-2)
An-danitra ao no misy ny taninao
Aza sondriana ety (in-2)}

{6.
Ny heloko Jeso mba avelao
ny fahotako koa mba avelao
Mba raiso aho ho anilanao fa (tsy taniko ety) (in-2)}
', '#3-XVI-9', '#3') /end

insert into song values (53181, '318. Hangina ve ny foko', NULL, NULL, '
{1.
Hangina ve ny foko
Ka tsy hiantso Anao ?
Andriamanitra ô ! (hianao)
Andriamanitra ô !}

{2.
Hangina ve ny foko
Ka tsy hidera Anao ?
Raha sendra sarotra (aho ety)
Raha sendra sarotra}

{3.
Hangina ve ny foko
Vesarin''eso izao ?
Ka tsy hiantso Anao (izao)
Andriamanitra ô !}

{4.
Ny foko tsy hangina
Fa mbola hiantso Anao
Andriamanitra ô ! (hianao)
Andriamanitra ô !}
', '#3-XVI-9', '#3') /end

insert into song values (53191, '319. Raha maneno ny trompetra ka ho re', NULL, NULL, '
{1.
Raha maneno ny trompetra ka ho re
Hisy horohoron-tany mafy be
Ka izao tontolo izao hangovitra
Fa ny mino ho faly sy finaritra}

{Miomàna miambena
Fa ho tampoka tokoa
Ny fotoana hisehoan''ny Tomponao
Toy ny tselatra mazava eny ambony
''Zay no andro farany}

{2.
Raha maneo ny trompetra ka ho re
Ny masoandro dia hanjary aizimbe
Ireo kintana hiraraka avokoa
 Hikorontana tsy hanampaharoa}

{3.
Raha maneno ny trompetra ka ho re
Dia hanafotra ny ranomasim-be
Ka ny tany tsy ho hita intsony re
Iny lanitra iny dia hiorona}

{4.
Raha maneno ny trompetra ka ho re
Ireo maty dia hitsangana avokoa
Dia ho avy ireo anjely marobe
Ka hanavaka ny mino
''zay manoa}

{5.
Raha maneno ny trompetra ka ho re
Dia ho avy Jeso Zanaky ny Ray
Ka hanao ny fitsarana lehibe
Hanontany ny asa izay natao tety.}
', '#3-XVI-9', '#3') /end

insert into song values (53201, '320. Izao ry Jesosy dia vonona izao', NULL, NULL, '
{1.
Izao ry Jesosy dia vonona izao
Vonon-kitsangana hanaraka Anao
Kanefa Jesosy malemy tokoa
K''avia re tantano dia ho tafita soa.}

{Jeso Malala ô!
Mpamonjy tia
Raiso re fa ondry mania
Jeso tantano,
Tantano izahay
Tantano Jesosy
Sao lavo eto indray}

{2.
Fa ny fahotako dia maro tokoa
Ka tsy mba mendrika
Anao Tompo soa
Nefa ny avotra tao Kalvary
Tokiko mandra-pialako ety}

{3.
Ny foko Jesosy atolotro Anao
Mbola maloto kanefa sasao
Eny, Jesosy, sasao ho madio
Ao re mitoetra manomboka anio.}

{4.
Ny tenako rehetra dia vonona izao
Vonon-kanefa izay sitrakao
Aina no fetra hanaraka Anao
Raiso aho Jesosy mba ho mendrika Anao}

{5.
Fanahy Masina ô! Tsinjovy izahay
Tsinjovy hahita ny lapan''ny Ray
Midina Jesosy ety aminay
Midina Jesosy ka raiso izahay}
', '#3-XVI-9', '#3') /end

insert into song values (53211, '321. Misy olon''ory sy mahantra ao', NULL, NULL, '
{1.
Misy olon''ory sy mahantra ao ;
Vonjeo fa namanao!
Ota no manimba sy mandratra ao;
Vonjeo fa namanao!
Misy azonao atao hamonjena azy ireo
Antenaina hianao!}

{FIV.
Vonjeo fa namanao!
He, mijaly ka ho faty,
Vonjeo fa namanao!
Mbola azo antenaina,
Mbola misy fofon''aina
Aoka hikelezan''aina maika,
Vonjeo fa namanao!}

{2.
Misy fo marary sy mijaly ao
Vonjeo fa namanao!
Ota no mandoza, mampijaly ao,
Vonjeo fa namanao!
''Zay talenta anananao,
Ento hamonjena ireo
O! faingàna hianao!
[FIV.]}

{3.
Misy olom-bery ka ho faty ao;
Vonjeo fa namanao!
Ota no mamono mahafaty ao;
Vonjeo fa namanao!
Vola sy harena koa misy
Eo an-tananao
Ento hamonjena ireo
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53221, '322. Endrey ''ty fitiavan-dehibe', NULL, NULL, '
{1.
Endrey ''ty fitiavan-dehibe
Nasehonao Jeso ho ahy re !
Ny ranao latsaka, ho fanavotana
Endrey ''ty fitiavan-dehibe}

{2.
Jeso ''lay tia tokoa no miantsoa anao
Milaza hoe: " Ny ota avelanao"
Ny làlanao hazava, ny aizina hisava
Jeso ''lay tia tokoa no miantso anao.}

{FIV.
O! mba hevero izao
''Lay Tompo tia
Mpamonjinao
Ny ainy natolony
Hamonjy ny olony}

{3.
Jeso raiso ny fiainako ho Anao
Ny foko tsy madio mba sasao
Ho lapanao soa, mamitratra tokoa
Jeso raiso ny fiainako ho Anao
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53231, '323. Nohenjehina ombieny tia ahy Jeso', NULL, NULL, '
{1.
Nohenjehina ombieny tia ahy Jeso
Notsiniana raha niteny tia ahy Jeso
Noharatsiana tsy mba tiana tia ahy Jeso
Tsy henoina fa esorina tia ahy Jeso.}

{2.
Na namidy telopolo tia ahy Jeso
Na dia nofatorana aza tia ahy Jeso
Na dia nokapohina aza tia ahy Jeso
Na natositosika aza tia ahy Jeso.}

{3.
Na nasiana tehamaina tia ahy Jeso
Na nasiana totohondry tia ahy Jeso
Nohosena notsiraina tia ahy Jeso
Noresena tsy nekena tia ahy Jeso}

{4.
Norarana notsaraina tia ahy Jeso
Noratraina nolotoina tia ahy Jeso
Na dia nohelohina aza tia ahy Jeso
Na dia nosatrahan-tsilo tia ahy Jeso.}

{5.
Fa na dia nampahoriana tia ahy Jeso
Nozogaina fijaliana tia ahy Jeso
Na dia nofantsihana aza tia ahy Jeso
Na dia noendahina aza tia ahy Jeso.}

{6.
Nohomboana nolatsaina tia ahy Jeso
Na dia nolefonina aza tia ahy Jeso
Novonoina latsak''aina tia ahy Jeso
Izy no nisolo ahy tia ahy Jeso}

{7.
Nobabena fahotana tia ahy Jeso
Nohampangaina ho mpanota tia ahy Jeso
Novonoina latsak''aina tia ahy Jeso
Maty tao Golgota tia ahy Jeso.}
', '#3-XVI-9', '#3') /end

insert into song values (53241, '324. Tsy misy soa ho ahy afatsy Jeso', NULL, NULL, '
{1.
Tsy misy soa ho ahy afatsy Jeso
Nafoiko ny haren''ny tany
Manankarena aho nahita Azy
Faly ao Aminy re aho
Faly aho miarak''Aminy
Jesosy tokana ihany
Na dia mahantra aza
Mbola manao hoe:
Faly ao Aminy re aho}

{2.
Fony tsy nisy avotra fisoloana
Sy ho fampanekena ny Ray
Dia nanolo-tena hanavotra ahy
Tamin''ny Ràny Jesosy
Faly aho miarak''Aminy
Jesosy tokana ihany
Na dia mahantra aza
Mbola manao hoe:
Faly ao Aminy re aho}

{3.
Raha meloka ao amin''ny famoizam-po
Tsy misy fahazavana
Jesosy kosa nanao ahy mpandova
Ny lapan''ny voninahitra
Faly aho miarak''Aminy
Jesosy tokana ihany
Na dia mahantra aza
Mbola manao hoe:
Faly ao Aminy re aho}
', '#3-XVI-9', '#3') /end

insert into song values (53251, '325. O ! Ry Rain''ny fifaliana', NULL, NULL, '
{1.
O ! Ry Rain''ny fifaliana
Mba todio ny zanakao
Mitatao ny fahoriana
Maizim-pito ny andro izao
O ! Ry Raiko
Moa ve izao no sitrakao}

{2.
Lalim-be ny lohasaha
Ilentehanay izao
 Tano Raiko soa mivaha
Ny finoan''ny zanakao
O ! Ry Raiko
Moa ve izao no sitrakao}

{3.
Toa nahintsanao ny raozy
Tsilo sy satana ao
Ny nanjo anay havaozy
Hody fifaliana indray
O ! Ry Raiko
Moa ve izao no sitrakao}

{4.
O! Ry Raiko ela loatra
Zato fitsapana anay
Efa samba tafahoatra
Ny finoan''ny zanakao
O ! Ry Raiko
Moa ve izao no sitrakao}

{5.
Eny Raiko fa ekeko
Ty kapoaka atolotrao
Sady hitako no reko
Tamin''ireo mpianatrao
Hosotroiko hosotroiko
Ty kapoaka atolotrao}

{6.
Ty kapoakanao mangidy
No aingako an-tanan-droa
Tsy izahay no misafidy
Fa ny sitrakao no soa
Hosotroiko hosotroiko
Ty kapoaka atolotrao}
', '#3-XVI-9', '#3') /end

insert into song values (53261, '326. Aza avela ho mandalo re ny Teny', NULL, NULL, '
{1.
Aza avela ho mandalo re ny Teny
Na hikimpiana koa ny hazavana
Aza manamafy ny fonao fa aoka
Hovonjena ''zao anio izao.}

{Fa nahoana no tsy izao anio izao
Te-hovonjena va hianao
Ka tsy izao anio izao}

{2.
Mety tsy haraina intsony ny ampitso
Hovoafitaka ny hevitrao
Zao no ra ankasitrahana ka aoka
Hovonjena ''zao anio izao.}

{3.
Tsy mandà na iza n''iza re ny Tompo
Zay te-hiray fanahy Aminy
Maneke ny asa vitany ka aoka
Hovonjena ''zao anio izao.}
', '#3-XVI-9', '#3') /end

insert into song values (53271, '327. Misy ondriko very ao an-efitra ao', NULL, NULL, '
{1.
Misy ondriko very ao an-efitra ao
Hoy Jeso ! hoy Jeso
Iza re no mba mety ho irako izao
Mba hitady azy ireo sy hiandry azy koa ?}

{FIV.
Andraso ny ondriko,hoy Jeso
Fahano ny ondriko,hoy Jeso
Fa na hoatrinona no laninao
Dia honerako rehefa miverina aho.}

{2.
Maro ''reo bibi-dia no manandratra
Hoy Jeso !hoy Jeso
Iza re no hamonjy hitsabo azy ireo ?
Ka hamory azy ireo mba ho vala iray.
[FIV.]}

{3.
Mandehana hianao ka vonjeo izy ireo
Hoy Jeso !hoy Jeso
Lala-mafy sy efitra no izoranao
Mba hitady azy ireo sy hamonjy azy koa.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53281, '328. Ny antsonao Andriamanitra ô !', NULL, NULL, '
{1.
Ny antsonao Andriamanitra ô !
Dia feno fitiavan-dehibe
Ny teninao mandresy lahatra
Mamolaka ny foko izay mpandà}

{FIV.
''Ty ahy Tompo fa manantona
Manolo-tena ho Anao tokoa
Hanompo Andriamanitra ô !
Ka hampandre ireo izay tsy nandre.}

{2.
Ny antsonao Andriamanitra ô!
Manambitamby fo mibebaka
Ny feonao mandresy lahatra
Mananatra ny foko ''zay mpandà
[FIV.]}

{3.
Ny antsonao Andriamanitra ô!
Manakan-dalana ireo mania
Ny fitiavanao maharitra
Itiavanao ny fo mibebaka
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53291, '329. Menatra aho Tompo raha manatona', NULL, NULL, '
{1.
Menatra aho Tompo raha manatona
Eo am-pototry ny hazofijalianao
Tsy mba mendrika aho fa maloto fo
Nefa sitrakao Jeso hamela heloka ahy}

{FIV.
Tano aho re
Ho eo am-pototry ny hazofijalianao
Eo ny foko no mitony
Sady mahatsiaro sambatra tokoa.}

{2.
Menatra aho tompo ka miondrika
Eo am-pototry ny hazofijalianao
Noho izaho mantsy nohomboana Hianao
Mamelà ny heloko raha sitrakao Jesoa
[FIV.]}


{3.
Menatra aho Tompo nefa sitrakao
Ny hamela irony ratsy maro vitako
Afaka aho Tompo fa navotanao
Afaka aho fa navelanao ny heloko.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53301, '330. Ny Mpamonjy Jesosy no miantso anao', NULL, NULL, '
{1.
Ny Mpamonjy Jesosy no miantso anao
Miovà fa havelako ny helokao re
Ny aretinao mafy hositraniko ka
Te hositrana va hianao(hoy Jeso)}

{2.
Fo madio sady tsara no omeko anao
Miovà fa avelako ny helokao be
Ny fanahinao osa hatanjahiko koa
Te hositrana va hianao(hoy Jeso)}

{3.
Ry mitady ho kivy sy ho reraka ô !
Mbola misy ny vonjy izay omeko anao
Manantona hianao ka mibebaha re izao
Te hositrana va hianao(hoy Jeso)}

{4.
Ry Jesosy Mpamonjy sy Mpanafaka ô !
Feonao va izay...fa ekeko Jeso
Fa mpanota aho mantsy sady menatra Anao
Te hositrana re Tompo ô.}
', '#3-XVI-9', '#3') /end

insert into song values (53311, '331. Havaozy ny foko ry Tompo malala', NULL, NULL, '
{1.
Havaozy ny foko ry Tompo malala
Ka aoka ny loto nanjaka hiala
Fa mangetaheta hahita madio
Ny foko rehetra manomboka anio.}

{2.
Soloy hazavana ''lay aizina ela
Tsy mahatamana fa mbola tavela
Mitsilopilopy ''lay foko efa lao
K''iriko hitopy ilay masoandronao}
', '#3-XVI-9', '#3') /end

insert into song values (53321, '332. Fony tonga tety fahizay Jeso', NULL, NULL, '
{1.
Fony tonga tety fahizay Jeso
Tsy mba nanana zavatra soa Jeso
Tany am-pahitra kely izay Jeso
Tany an''efitra koa Jeso.}

{2.
Indro miantso anao indray Jeso
Te hamonjy ny fanahinao Jeso
Dia ho lavinao va re izao Jeso ?
Raha hitondra ny otanao.}

{Ao am-ponao Jeso no mandona,Jeso
Ao am-ponao,Jeso ao tian''i Jeso
Ho any an''efitra indray va
Jesosy,sa horaisinao ao am-po.}
', '#3-XVI-9', '#3') /end

insert into song values (53331, '333. Moa ve Jeso dia mihevitra', NULL, NULL, '
{1.
Moa ve Jeso dia mihevitra
Ny momba ny fiainako
Moa Jesosy ve isan''andro
''Zay lalan-kizorako}

{FIV.
He fantatro!he fantatro!
Fa akaiky izy izao
Izy tena tia,anio,doria
Akaiky izy izao}

{2.
Moa ve Jeso dia mandre tokoa
Ny feo fitarainako
Moa henoiny ve ka valiany e !
''Zany antso sy ventsom-po
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53341, '334. Raha te ho sambatra ianao', NULL, NULL, '
{1.
Raha te ho sambatra ianao
Sambatra
Raiso ao am-po Jeso}

{FIV.
Ny otanao alainy
Omeny anao ny soa
Hanova anao tanteraka
Hanjakany ao am-po.}

{2.
Raha te hiadana ianao
Hiadana
Raiso ao am-po Jeso
[FIV.]}

{3.
Raha te hifankatia ianao
Hifankatia
Raiso ao am-po Jeso.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53351, '335. Tompo tokana no ahy ety', NULL, NULL, '
{1.
Tompo tokana no ahy ety
Hatraty ka ho doria
Tsy mba misy roa na telo hafa
Fa tokana no ahy ety.}

{2.
Dia Jesosy Kristy Zanaka lahy Tokana
Tokana no ahy
Jesosy no ahy,Izy no ahy
Toky sy Avo ho ahy.}
', '#3-XVI-9', '#3') /end

insert into song values (53361, '336. Jeso apetrako Aminao', NULL, NULL, '
{1.
Jeso apetrako Aminao
Ny fanahiako rehetra
Ny asa vita hatrizao
Anao tokoa tsy misy fetra
Jeso apetraka Aminao.}

{2.
Jeso apetrako Aminao
Ny asa fikelezan''aina
Tsy misy miafina Aminao
Ny hetaheta tsy hay lazaina
Jeso apetrako Aminao}

{3.
Jeso apetrako Aminao
Ny toky sy fanantenana
Ny sitrakao no aoka hatao
Na aiza làlana ombana
Jeso apetrako Aminao.}
', '#3-XVI-9', '#3') /end

insert into song values (53371, '337. Mamela heloka isan''andro', NULL, NULL, '
{1.
Mamela heloka isan''andro
Ianao Jesosy Zoky be fitia-Be fitia ka
Kanefa,nefa he,indrisy
Fa lotoiko(2) amin''otako}

{....Mh,mh,mh,mh,...
Miantsoantso aho hoe :
Ray malala ô
....Mh,mh,mh,mh,...
Atsangano aho e !
Fa efa trotraka.}

{2.
Tsaroako dia tsaroako eto
Ianao Jesosy Zoky be fitia-Be fitia ka
Kanefa,nefa he,indrisy
Fa lotoiko (2) amin''ny otako}

{Rotsirotsy ny Mpamonjy
Diboky ny hafaliana izahay
Novesaram-pahoriana
Nefa he,dia fiadanana
No natolotrao.}
', '#3-XVI-9', '#3') /end

insert into song values (53381, '338. Maro ireo tanora izay miraparapa', NULL, NULL, '
{1.
Maro ireo tanora izay miraparapa
Ao anatin''ny aizina ao
Mitambesatra ny ota ka tsy zaka
Fahoriana re no ao.}

{FIV.
Iza kosa no hilanja
Ny fanilo ho azy ireo
Izay hampahiratra tokoa ny jamba
Ka ho vonjy ho azy ireo ?}

{2.
Fahafatesana no toerana hitokiany
Lavitra ny fiainana
Tsy mba misy fanavotana itokianay
Manome fiadanana
[FIV.]}

{3.
Fa Jesosy no efa maty ho azy ireo
Ka mitondra fiainana
''Zay mandray dia sahy manandratra ny feo
Fa manam-piadanana.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53391, '339. He fantatro f''indray andro any', NULL, NULL, '
{1.
He fantatro f''indray andro any
Ho levona ity tenako ety
 Fa fiainan-danitra
Antenaiko hatrety
Ny masoko hahita an''i Jeso}

{2.
He fantatro f''indray andro any
Ho rava sy ho potika ny ety
Fa ny mino ihany re
No haharitra doria
Ny masoko hahita an''i Jeso}

{3.
He fantatro f''indray andro any
Hiverina ny Tompo Jesosy
Faly aho ho raisiny
Honina eo anilany
Ny masoko hahita an''i Jeso}
', '#3-XVI-9', '#3') /end

insert into song values (53401, '340. Velonay variake trake etoy', NULL, NULL, '
{Velonay variake trake etoy
Andron''i Jesosy Tompo soa
La misinisiny n''aiko izao
Tsy engako Jeso Tompo soa
Ndrao ho maty é
Ndrao ho velo
Jeso Hazomangako}

{''Reo pasitera tsy miroro
Hampandroso antsika masikoro
Olo-jiaby toy fa mandroso
Mimenara antsika longoko}
', '#3-XVI-9', '#3') /end

insert into song values (53411, '341. Ny fanoloran-tenanao ry sakaiza', NULL, NULL, '
{1.
Ny fanoloran-tenanao ry sakaiza
Mila fanavaozana
Fomba fitorianao ry sakaiza
Mila fanavaozana koa
Vavaka ataonao ry sakaiza
Mila fanavaozana}

{FIV.
Koa dia eritrereto ihany
Ary koa dinidiniho tsara
Ry sakaiza}

{2.Izato fiainanao ry sakaiza
Mba aoreno tsara
Faingafaingano ihany ry sakaiza
Sao ianao ho tara
Mba araraoty ihany ny fotoana
Sao nenina any am-para
[FIV.]}

{3.
Io fanavaozana io ranabavy
Tsoriko aminao
Io fanorenana io rahalahy
Ambarako aminao
Tsy vitan''ny herin-tena na ny saina
Fa Jeso no tena aina,
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53421, '342. Ali-mangina ny Getsemane', NULL, NULL, '
{Ali-mangina ny Getsemane
Indro Jesoa nandohalika teo
Izy nangataka tamin''ny Ray
Nisento nijaly Izy tamin''izay.}

{Moa mba tsapako ve ?
Moa mba tsapanao ve ?
Mora mivadika izaho sy ianao
Fa fitiavany no mandrakizay}
', '#3-XVI-9', '#3') /end

insert into song values (53431, '343. Tao anaty aizina aho no tadiavinao', NULL, NULL, '
{1.
Tao anaty aizina aho no tadiavinao
Tao anaty lalina aho nampiakarinao
Efa maty anie ny foko novelominao
Efa potika sy toro namboarinao.}

{2.
Fo mararin-tsento lava nositraninao
Saiko vizaky ny ota nohavaozinao
Teny an-dalan-tsitrapo no nosakananao
Tao anaty filibana no navoakanao.}

{3.
Indro Tompo fa omeko ho Anao anio
Aina,tena sy fanahy,foiko ho Anao
Ny fiainako rehetra no atolotro
Ho Anao tsisy fetra ho Anao doria.}
', '#3-XVI-9', '#3') /end

insert into song values (53441, '344. Tsy mba trano bongo akory', NULL, NULL, '
{Tsy mba trano bongo akory
No nahaterahanao Jesoa
Tsy mba tao an-dapasoa
Na trano tsara sy madio
Fa tranon''omby no nanaiky
Nandray Anao
Jesosy ô,o,o,o}

{Ny foko ratsy Tompo ô
No atolotro Anao
Io no solon-tranon''omby
Mba handraisako Anao}
', '#3-XVI-9', '#3') /end

insert into song values (53451, '345. Andriamanitra ô ! ô !', NULL, NULL, '
{1.
Andriamanitra ô ! ô !
Rainay Mpamorona
Mba henoy ange e ! e !
Ny fivavahanay}

{FIV.
Dera, laza e !
Atolotro ho Anao
Ho Anao(beso)
Ho Anao ry ray ô !
Ny voninahitra}

{2.
Raha mbola eto an-tany
Be fahoriana
Rehefa any an-danitra any
Dia ho finaritra
[FIV.]}

{3.
Raha eo anilanao Jeso
Miadana tokoa
Mandrakizay doria
Haleloia amena !
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53461, '346. Ry zanako mania !', NULL, NULL, '
{1.
Ry zanako mania !
Henoinao izao :
Izaho be fitia
Mangora-po anao ;
Ry zanko ! modia,
Sao maty hianao,
Fa be ny bibidia;
Modia re !}

{FIV.
"Aiza ho aiza ny lalanao"
Ry zanak''olombelona ?
Aiza ho aiza ny lalanao
Ho any an-danitra ?
Mpanjaka va  ianao
Sa mpanota maika hody ?
Ka mbola manontany
Tsy hody va ?}

{2.
Ny zavatry ny tany
Sao mahasimba anao
Harena mety lany
Tsy mahavonjy anao ;
Izaho irery ihany
No hiadananao;
Ka mbola manontany:
Tsy hody va ?
[FIV.]}

{3.
Mamiratra jerena
Ny lalan-tsitrapo
Fa ankoso-bolamena
Mamitaka ny fo;
Ny sitrapon''ny tena
Mamery ny ainao
Ario ka miverena
Modia re !
[FIV.]}

{4.
Izao Tompon''aina,
Nijaly ho anao
Ka, he, nanolotra aina
Mba hamonjena anao;
Ny rako mitaraina,
Modia hianao:
Ka aza mba malaina,
Modia re !
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53471, '347. Misy didy tena tsara ', NULL, NULL, '
{1.
Misy didy tena tsara
Izay nomen''ny Tompo hoe:
Mba mifankatiava tsara,
Hianareo izay mino hoe :}

{FIV.
Haleloia haleloia
Haleloia haleloia
Haleloia haleloia
Dera, laza Anao ry Jesoa !}

{2.
Tonga Jesoa mba hanompo
Sy mba hampihavana;
Tsy mpanaraka ny Tompo
Izay tsy tia namana.
[FIV.]}

{3.
Fa iray tokoa ny Loha,
Dia Jesosy Zanaka,
Endriky ny Ray Tsitoha,
Izay halaina tahaka.
[FIV.]}

{4.
Jeso no Ilay foto-kazo,
Tena voaloboka :
Ka ny sampana halazo,
Raha tafasaraka.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53481, '348. Vatolampy, tromba, tsiny', NULL, NULL, '
{1.
Vatolampy, tromba, tsiny
Tsy Zanahary
Kalanôro, razana e !
Tsy mahavita azy e !
Ialao daholo ireo;
Sampy, sampy e !
Zanahary arô manampy e !}

{FIV.
Ô ! avia
Avia zoky,avia zandry
Avia hivavaka e !
Avia zoky, avia zandry
Andao hivavaka e !}

{2.
Mba henoy ry havako ô !
Feo miantso anao ô !
Ialao ny ditranao,
Tsy tian-Janahary
Ialao daholo ireo;
Sampy, sampy e !
Zanahary arô manampy e !
[FIV.]}

{3.
Zanahary tsy mifidy
Tavan''olo-ratsy
Na mpandainga ; mpamono olo
Tsy tia toetra ratsy
Na mpimasy be malaza ;
Manatona Azy e !
Zanahary arô manampy e !
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53491, '349. Andriamanitra Fitiavana', NULL, NULL, '
{1.
Andriamanitra Fitiavana Hianao
Ka mihevitra ny momba anay aty
Raha mifoha maraina izahay
Dia tsy afaka am-bavanay ny hoe :
"Ampy ho anay ny fahasoavanao !"}

{FIV.
Raha maraina mibaliaka ny andronay ety
Ianao no masoandro niaviany.
Raha ny alin''ny ririnina no handalo ety
Ianao no kintana hitantana ny dia
Fa ny vavaka atao izay mipololotra am-ponay ao hoe :
"Izahay anie hatoky anao"}

{2.
Andriamanitra Fitiavana Hianao
Ka mitanatana ny dianay ety
Raha mandimby ny andro ny alina
Mbola tokana ny deranay hoe :
"Ampy ho anay ny fahasoavanao"
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53501, '350. Jeso sakaizanay', NULL, NULL, '
{1.
Jeso sakaizanay,
Jeso anay (3)
Sady tsy mahafoy,
Jeso anay;
Jeso Sakaizanay
Raha ny olona
Mety mandao anay,
Jeso no tsy mandao,

{FIV.
Jeso anay, Jeso anay
Jeso Sakaizanay}

{2.
Jeso Sakaizanay,
Jeso anay (3)
Izy no Avotray,
Jeso anay;
Jeso Sakaizanay,
Maty ny Tomponay
Noho ny helokay
Afaka izahay,
[FIV.]}

{3.
Jeso Sakaizanay,
Jeso anay (3)
Izy no Aronay,
Jeso anay;
Jeso Sakaizanay
Reraka izahay
Noho ny adinay,
Jeso no herinay,
[FIV.]}

{4.
Jeso Sakaizanay,
Jeso anay (3)
Izy no Lalanay,
Jeso anay;
Jeso Sakaizanay
Lalana marina
Hahatongavanay
Any an-danitra,
[FIV.]}

{5.
Jeso Sakaizanay,
Jeso anay(3)
Manana izahay,
Jeso anay;
Jeso Sakaizanay(2)
No hovantaninay
Any an-danitra,
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53511, '351. Izaho nentina nanatrika', NULL, NULL, '
{1.
Izaho nentina nanatrika
Ny Tompo sy ny fiangonana;
Ny ray sy reny no niantoka
Hitaiza sy hitari-dalana
Iô Zagnahary io ô(in-3)
Hitaiza sy hitari-dalana.}

{2.
Ka faly aho, fa noraisina
Natao batisa mbola zaza re
Nanaovam-panekena masina
Ho zanaky ny Tompo lehibe
Io ô Zagnahary io ô(in-3)
Ho zanaky ny Tompo lehibe.}

{3.
Ampio aho Zagnahary ô!
Hanaraka ny eny masigny e
Izay nataoko taminao ry To
Ka tsy mba hisy fivadihana
Io ô Zagnahary io ô(in-3)
Ka tsy mba hisy fivadihana}

{4.
Ry Tompoko,Izay manomboka
Ny asa tsara ao anatiko
Arovy aho ho tanteraka
Mba tsy ho faty ny finoako
Io ô Zagnahary io ô(in-3)
Mba tsy ho faty ny finoako.}
', '#3-XVI-9', '#3') /end

insert into song values (53521, '352. Iny lalanao iny Tompo', NULL, NULL, '
{1.
Iny lalanao iny Tompo
No mamiko
Iny lalanao iny Tompo
No tiako
Iny lalanao iny Tompo
Fiainako
Ka arahiko ô..ô..ô..}

{2.
Iny teninao iny Tompo
No mamiko
Iny teninao iny Tompo
No tiako
Iny teneinao iny Tompo
Fiainako
Ka arahiko ô..ô..ô..}
', '#3-XVI-9', '#3') /end

insert into song values (53531, '353. Miomàna , miomàna', NULL, NULL, '
{Miomàna , miomàna
Efa tapitra ny andro
Tsy maintsy handeha ôlo jiaby é, é
Handeha ho aiza é é é ?
Handeha hiatrika Zagnahary}

{1.
Telopolo taona nanterana eto
Tsisy raha tsy hainao
Hainao ny tsara sy ny ratsy
Kanefa anao misafidy
Olo jiaby mandeha leglizy
Anao indraiky mizaha tena izy
Izôvy eky tena izy zahanao
Ambonimbony Zagnahary}

{2.
Dimampolo taona nanteragna eto
Tsisy raha tsy hainao
Hainao ny tsara sy ny ratsy
Kanefa anao misafidy
Lakolosy magneno Alahady
Anao indraiky mizahazaha angady
Mahery é é andro araiky
Avian''ny "convocation"
Tsy mampatanjaka anao ô ô}

{3.
Fitopolo taona nanterana eto
Tsisy raha tsy hainao
Hainao ny tsara sy ny ratsy
Kanefa anao misafidy
Olo jiaby mamaky Baiboly
Anao indraiky mandeha an-tanimboly
Azôvy eky tsy hangorohoro
Raha hiatrika Zagnahary}
', '#3-XVI-9', '#3') /end

insert into song values (53541, '354. Noho ny sakafo nomenao', NULL, NULL, '
{Noho ny sakafo nomenao
Misaotra ry Ray (in-2)
Omeo anay ny fahendrena
Sy falifaly (in-2)
Noho ny sakafo nomenao
Misaotra ry Ray}

{Amena.}
', '#3-XVI-9', '#3') /end

insert into song values (53551, '355. Vavaka mihambahamba ny sasany', NULL, NULL, '
{1.
Vavaka mihambahamba ny sasany
Tsy mba tsaroany ny andro Alahady
Miasa tany, mibalady mandray angady
Ka izany ve no mivavaka}

{FIV.
Tsy izany e! (bis) velively
Aro mivavaka e!
Vakio Soratra Masina
Tsy misy aro mahita
Tavan''i Jesosy Tompo
Raha tsy amin''ny finoana}

{2.
Vavaka mihambahamba ny sasany
Tsy sitraky ny fony mandeha l''Eglizy
Miaraka amin''ny namany  no tena anton-diany
Ka izany ve no mivavaka
[FIV.]}

{3.
Vavaka mihambahamba ny sasany
Mamonjy fivoriana sy fandalinana
Mijery tovolahy sy tovovavy mipozipozy
Kristiana ve no anarany
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53561, '356. Raiko ô ! izaho misaotra Anao', NULL, 'Tokim-panompoana', '
{1.
Raiko ô ! izaho misaotra Anao
Fa mba nofidianao ho mpiasanao
Ka na dia tsy mendrika aza aho
Nomenao anjara hanao ny asanao.
Z''inona aho Andriamanitra ô !
Z''inona moa aho mba tinendrinao ?
Mpanota aho tsy mendrika tokoa
Nefa indro fa toloranao ny soa.}


{FIV.
Ka mba ambarako eto izao
Ny fahafoizantenako hanompo anao Raiko
Tsy handrarak''ilo mby an-doha
Fa tena hanao ny asanao tokoa.
Ka ny saiko, na ny aiko
Ny androko, ny hareko,ny fanahiko
Foiko ary ho entiko manao
Sy manefa izao fanompoako izao.}

{2.
Jeso ô!ekeko eto izao
Ny ho fitaovana eo am-pelatananao
Hitoriana ny famonjenao
Hanitarana ny ambain-dainao
Ho fadiko izany hoe hanao
Zava-mifanohitra ami-baikonao
Na hitondra ny Fiangonanao
Hanao ny fanaon''izao tontolo izao.
[FIV.]}

{3.
Ray Tsitoha ô!izayiriko
Dia ny Fanahinao hamomba ahy
Ka na Asa hatao na fivoriana
Tsy haroaroako faharatsiana
Jeso ô ! mba tovy amiko fa
Ianao tsy hahafoy na hahasaraka ahy
Raha Ianao no miaraka amiko tokoa
Tsy misy asa tsara zay tsy efako.
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53571, '357. O ! Mitsangana ka dia mihazavà', NULL, 'Hira Faneva faha-20 Taona SAFif FJKM', '
{1. O ! Mitsangana ka dia mihazavà
F''indro efa tonga Ilay Fahazavana o ! o !
Maika ity asa ka dia miaingà
Fa efa manjaka ny fahajambana}

{FIV.
Mameno tany ny aizina be
Efa mandrakotra ny firenena
Jamba ireo mponina ao ka endrey
Mila ity Fahazavana omena o ! o !}

{2.
O ! Mitsangàna ka dia mirosoa
Ny hery rehetra vononina tsara e ! e !
Fa mila olona tena mifoha
Hanaitra ireo matory sao tara
[FIV.]}

{3.
Jesoa no Fahazavana be
Efa miposaka eo aminao e! e!
Ary Izy ihany no lalan-kaleha
Ho amin''ny Ray amin''izao tany izao
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53581, '358. Indreto ''zahay Zañahary !', NULL, NULL, '
{Indreto ''zahay Zañahary e,
Avy mañatoño Anà ô!
Mitonga ny fô ravoravo
Mahatsaro ny tsara natànào
Letry ny êla (Letry ny êlabe) [2x]
Avake baroko niany e!}

{(Oà kalê),
Mankalaza Anà Zañahary e
Zahay jiaby e, vôry aketo niany e
Mankasitraka, mankateliñy  e,
Fõ tia ''zahay tatôboe Anà ô!}

{(Êfa very ''zay,)
''zay aombinào,
(Nitsakarahinào),
tañaty alan''ny fahotaña}

{Nandifasanào,
Tsimañaja mazôto ô,
Tsy mataho-drôtry e,
maha keki-panentry e.
Nitsaka dengy laliñy e,
salakady misy voay
Nitety karõka-bato malamatra,
Ndrêfa tsy lavo!}

{(Veloño ô!) 
Veloño ''zahay Zañahary e
Veloño ''zahay Zañahary e!}
', '#3-XVI-9', '#3') /end

insert into song values (53591, '359. Jesosy ô! Indreto ny olonao', NULL, NULL, '
{1.
Jesosy ô! Indreto ny olonao
Manatona Anao mba ho vonjena
Manatona Anao sao very
Kely finoana Jesoa ka mila hery}

{FIV.
Mamaky efitra mety solafaka
Raha tojo fiakarana dia mety tsy ho afaka
Herikereo ihany Jesoa fa mbola malefaka
Tazatazano ihany Jesoa fa sao dia reraka}

{2.
Jesosy ô ! Indreto ny olonao
Manatona Anao mba ho vonjena
Manatona Anao ka tohano
Raha sendra lavo aho Jesoa atsangano
[FIV.]}

{3.
Fanahy Masina ô midina
Jereo ny olonao fa sao variana
Izay matory mba fohazy
Ny Fanahy Masina Jesoa afafazo
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53601, '360. Isika tsy maintsy miasa ', NULL, 'Hira Faneva Jobily faha-25 Taona SAFIF/FJKM', '
{1.
Isika tsy maintsy miasa
Raha mbola atoandro koa  (in-2)
Baikon''i Jesoa izany
Ka tanteraho}

{FIV.
(in-2)
Miasa, milofosa tsara,
Aza mba mangatak''andro
Raha mbola atoandro koa ny andro
Dieny mbola mety izao}

{2.
Isika tsy maintsy mitory
Raha mbola velona koa (in-2)
Hanaitra ireo izay matory
Ho olona mifoha
[FIV.]}

{3.
Isika tsy maintsy mandeha
Mitondra ny mazava (in-2)
Hanaisotra ny aizina
Hampahiratra ny jamba
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53611, '361. Isika tsy maintsy manao ny asan''ny Tompo isan''andro', NULL, NULL, '
{1.
Isika tsy maintsy manao
Ny asan''ny Tompo isan''andro
Koa andeha ary izaho sy ianao
Hanefa raha mbola atoandro}

{FIV.
Mandroso hatrany,
Izay no tanjona !
Hatrany am-paran-tany
Ry Fifohazana.}

{2.
Haingàna ry Fifohazana e,
Fandrao ora farany izao
Hiasa, tsy ho mpitazana e,
No hilan''i Jesoa Tompo anao !
[FIV.]}

{3.
Na mafy aza ny ady atao
Ny sarotra koa hifanesy !
D''izao no aoka ho fantarinao :
Isika tsy maintsy mandresy !
[FIV.]}
', '#3-XVI-9', '#3') /end

insert into song values (53621, '362. Iny lalana iny e ! lalan-tsarotra', NULL, NULL, '
{Iny lalana iny e ! lalan-tsarotra
Miaraka amin''ny Tompo e!
Iny lalana iny e !
Iny lalana iny, iny lalana iny
Misy e ! misy hazo fijaliana
Ho lanjaina amin''iny lalana iny
Ry Jesosy Tompo ô ! mba tantanonao
Fa mideza loatra e !
Iny lalana iny
Iny lalana iny.sns
Hianao ry Tompo ô no angatahiko
Mba hitantana ahy amin''iny lalana iny
Iny lalana iny.}
', '#3-XVI-9', '#3') /end
